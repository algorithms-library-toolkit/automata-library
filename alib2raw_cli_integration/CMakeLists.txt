project(alt-libraw_cli_integration VERSION ${CMAKE_PROJECT_VERSION})
alt_library(alib2raw_cli_integration
	DEPENDS alib2raw alib2data alib2cli alib2common alib2abstraction alib2measure alib2std
)
