project(alt-libaux VERSION ${CMAKE_PROJECT_VERSION})
find_package(LibXml2 REQUIRED)
alt_library(alib2aux
	DEPENDS alib2str alib2graph_data alib2data alib2xml alib2common alib2abstraction alib2measure alib2std
	TEST_DEPENDS LibXml2::LibXml2
)
