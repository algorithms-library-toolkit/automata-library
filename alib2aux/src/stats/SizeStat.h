#pragma once

#include <alib/set>

namespace stats {

class SizeStat {
public:
    template <class T>
    static unsigned stat(const ext::set<T>& object);
};

template <class T>
unsigned SizeStat::stat(const ext::set<T>& object)
{
    return object.size();
}

}
