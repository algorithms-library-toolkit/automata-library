#include <registration/AlgoRegistration.hpp>
#include "StringCompare.h"

namespace {

auto StringCompareLinear2 = registration::AbstractRegister<compare::StringCompare, bool, const string::LinearString<>&, const string::LinearString<>&>(compare::StringCompare::compare);
auto StringCompareCyclic2 = registration::AbstractRegister<compare::StringCompare, bool, const string::CyclicString<>&, const string::CyclicString<>&>(compare::StringCompare::compare);

} /* namespace */
