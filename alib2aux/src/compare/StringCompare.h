#pragma once

#include "string/CyclicString.h"
#include "string/LinearString.h"

namespace compare {

class StringCompare {
public:
    template <class SymbolType>
    static bool compare(const string::LinearString<SymbolType>& a, const string::LinearString<SymbolType>& b);

    template <class SymbolType>
    static bool compare(const string::CyclicString<SymbolType>& a, const string::CyclicString<SymbolType>& b);
};

template <class SymbolType>
bool StringCompare::compare(const string::LinearString<SymbolType>& a, const string::LinearString<SymbolType>& b)
{
    return a.getContent() == b.getContent();
}

template <class SymbolType>
bool StringCompare::compare(const string::CyclicString<SymbolType>&, const string::CyclicString<SymbolType>&)
{
    throw "NYI";
}

} /* namespace compare */
