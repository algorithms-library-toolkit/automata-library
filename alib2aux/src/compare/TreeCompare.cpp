#include <common/DefaultSymbolType.h>
#include <registration/AlgoRegistration.hpp>
#include "TreeCompare.h"

namespace {

auto TreeCompareRankedTree = registration::AbstractRegister<compare::TreeCompare, bool, const tree::RankedTree<DefaultSymbolType>&, const tree::RankedTree<DefaultSymbolType>&>(compare::TreeCompare::compare);

} /* namespace */
