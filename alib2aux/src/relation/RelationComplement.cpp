#include <registration/AlgoRegistration.hpp>
#include "RelationComplement.h"

#include <object/Object.h>

namespace {

auto RelationComplement = registration::AbstractRegister<relation::RelationComplement, ext::set<ext::pair<object::Object, object::Object>>, const ext::set<ext::pair<object::Object, object::Object>>&, const ext::set<object::Object>&>(relation::RelationComplement::complement, "relation", "universe").setDocumentation("Computes the complement relation in given universe\n\
\n\
@param relation the original relation\n\
@param universe the universe of items participating in the relation\n\
\n\
return relation where items in relation are those which that were not in relation and otherwise");

} /* namespace */
