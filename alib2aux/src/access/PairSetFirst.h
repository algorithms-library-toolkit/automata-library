#pragma once

#include <alib/pair>
#include <alib/set>

namespace dataAccess {

class PairSetFirst {
public:
    template <class First, class Second>
    static ext::set<First> access(const ext::set<ext::pair<First, Second>>& pairSet)
    {
        ext::set<First> res;
        for (const ext::pair<First, Second>& pair : pairSet)
            res.insert(pair.first);

        return res;
    }
};

} /* namespace dataAccess */
