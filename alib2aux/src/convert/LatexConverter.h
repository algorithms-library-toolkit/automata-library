#pragma once

#include <ext/ostream>
#include <ext/sstream>

#include <string/String.h>

#include <automaton/FSM/DFA.h>
#include <automaton/FSM/EpsilonNFA.h>
#include <automaton/FSM/MultiInitialStateEpsilonNFA.h>
#include <automaton/FSM/MultiInitialStateNFA.h>
#include <automaton/FSM/NFA.h>

#include <grammar/RawRules.h>

#include "common/converterCommon.hpp"

#include <container/string/ObjectsVariant.h>
#include <factory/StringDataFactory.hpp>

namespace {
inline std::string escape(std::string str)
{
    for (const auto& [a, b] : {std::pair<std::string, std::string>{"\"", "\\\""}, {"{", "\\{"}, {"}", "\\}"}}) {
        str = convert::replace(std::move(str), a, b);
    }

    return str;
}
}

namespace convert {

class LatexConverter {
public:
    template <class FiniteAutomatonType>
    static void convertFSM(ext::ostream& out, const FiniteAutomatonType& automaton, bool wideTable = false);

    template <class FiniteAutomatonType>
    static std::string convertFSM(const FiniteAutomatonType& automaton)
    {
        return convertFSM(automaton, false);
    }

    template <class FiniteAutomatonType>
    static std::string convertFSM(const FiniteAutomatonType& automaton, bool wideTable)
    {
        ext::ostringstream ss;
        convertFSM(ss, automaton, wideTable);
        return ss.str();
    }

    /* --------------------------------------------------------------------- */


    template <class GrammarType>
    static void convertGrammar(ext::ostream& out, const GrammarType& grammar);

    template <class GrammarType>
    static std::string convertGrammar(const GrammarType& grammar)
    {
        ext::ostringstream ss;
        convertGrammar(ss, grammar);
        return ss.str();
    }

private:
    template <class SymbolType, class StateType>
    static constexpr const char* automatonType(const automaton::DFA<SymbolType, StateType>&)
    {
        return "DFA";
    }

    template <class SymbolType, class StateType>
    static constexpr const char* automatonType(const automaton::NFA<SymbolType, StateType>&)
    {
        return "NFA";
    }

    template <class SymbolType, class StateType>
    static constexpr const char* automatonType(const automaton::EpsilonNFA<SymbolType, StateType>&)
    {
        return "$\\varepsilon$-NFA";
    }

    template <class SymbolType, class StateType>
    static constexpr const char* automatonType(const automaton::MultiInitialStateNFA<SymbolType, StateType>&)
    {
        return "NFA";
    }

    template <class SymbolType, class StateType>
    static constexpr const char* automatonType(const automaton::MultiInitialStateEpsilonNFA<SymbolType, StateType>&)
    {
        return "$\\varepsilon$-NFA";
    }

    /* --------------------------------------------------------------------- */

    template <class FiniteAutomatonType, class StateType>
        requires automaton::isMultiInitialStateNFA<FiniteAutomatonType> || automaton::isMultiInitialStateEpsilonNFA<FiniteAutomatonType>
    static inline bool isInitialState(const FiniteAutomatonType& automaton, const StateType& state)
    {
        return automaton.getInitialStates().count(state);
    }

    template <class FiniteAutomatonType, class StateType>
        requires automaton::isDFA<FiniteAutomatonType> || automaton::isNFA<FiniteAutomatonType> || automaton::isEpsilonNFA<FiniteAutomatonType>
    static inline bool isInitialState(const FiniteAutomatonType& automaton, const StateType& state)
    {
        return automaton.getInitialState() == state;
    }

    /* --------------------------------------------------------------------- */

    template <class FiniteAutomatonType, class StateType>
        requires automaton::isDFA<FiniteAutomatonType>
    static void transitionsRow(ext::ostream& out, const FiniteAutomatonType& automaton, const StateType& state)
    {
        for (const auto& symbol : automaton.getInputAlphabet()) {
            out << " & ";

            auto transition = automaton.getTransitions().find(ext::make_pair(state, symbol));
            if (transition == automaton.getTransitions().end())
                out << "-";
            else
                out << escape(factory::StringDataFactory::toString(transition->second));
        }
    }

    template <class FiniteAutomatonType, class StateType>
        requires automaton::isNFA<FiniteAutomatonType> || automaton::isMultiInitialStateNFA<FiniteAutomatonType>
    static void transitionsRow(ext::ostream& out, const FiniteAutomatonType& automaton, const StateType& state)
    {
        for (const auto& symbol : automaton.getInputAlphabet()) {
            out << " & ";

            auto transitions = automaton.getTransitions().equal_range(ext::make_pair(state, symbol));

            for (auto it = transitions.begin(); it != transitions.end(); ++it) {
                if (it != transitions.begin())
                    out << ",";
                out << escape(factory::StringDataFactory::toString(it->second));
            }

            if (transitions.empty())
                out << "-";
        }
    }

    template <class FiniteAutomatonType, class StateType>
        requires automaton::isEpsilonNFA<FiniteAutomatonType> || automaton::isMultiInitialStateEpsilonNFA<FiniteAutomatonType>
    static void transitionsRow(ext::ostream& out, const FiniteAutomatonType& automaton, const StateType& state)
    {
        const auto symTransitions = automaton.getSymbolTransitions();
        const auto epsTransitions = automaton.getEpsilonTransitions();

        /* columns for symbols */
        for (const auto& symbol : automaton.getInputAlphabet()) {
            out << " & ";

            auto tr = symTransitions.equal_range(ext::make_pair(state, symbol));

            if (tr.empty())
                out << "-";

            for (auto it = tr.begin(); it != tr.end(); ++it) {
                if (it != tr.begin())
                    out << ",";
                out << escape(factory::StringDataFactory::toString(it->second));
            }
        }

        /* column for epsilon */
        out << " & ";
        auto tr = epsTransitions.equal_range(state);

        if (tr.empty())
            out << "-";

        for (auto it = tr.begin(); it != tr.end(); ++it) {
            if (it != tr.begin())
                out << ",";
            out << escape(factory::StringDataFactory::toString(it->second));
        }
    }
};

template <class FiniteAutomatonType>
void LatexConverter::convertFSM(ext::ostream& out, const FiniteAutomatonType& automaton, bool wideTable)
{
    constexpr bool isEpsilonType = automaton::isMultiInitialStateEpsilonNFA<FiniteAutomatonType> || automaton::isEpsilonNFA<FiniteAutomatonType>;

    if (wideTable)
        out << "\\begin{tabular*}{\\textwidth}{|rl||";
    else
        out << "\\begin{tabular}{|rl||";

    for (size_t i = 0; i < automaton.getInputAlphabet().size() + isEpsilonType; ++i)
        out << "c|";
    out << "}" << std::endl;

    out << "\\hline" << std::endl;
    out << "\\multicolumn{2}{|c||}{" << automatonType(automaton) << "}";

    for (const auto& symbol : automaton.getInputAlphabet())
        out << " & " << escape(factory::StringDataFactory::toString(symbol));

    if constexpr (isEpsilonType)
        out << " & $\\varepsilon$";

    // out << "\\hspace*{14cm}\\\\" << std::endl;
    out << "\\\\\\hline" << std::endl;

    for (const auto& state : automaton.getStates()) {
        if (automaton.getFinalStates().count(state) && isInitialState(automaton, state))
            out << "$\\leftrightarrow$ & ";
        else if (automaton.getFinalStates().count(state))
            out << "$\\leftarrow$      & ";
        else if (isInitialState(automaton, state))
            out << "$\\rightarrow$     & ";
        else
            out << "                  & ";

        out << escape(factory::StringDataFactory::toString(state));
        transitionsRow(out, automaton, state);
        out << " \\\\\\hline" << std::endl;
    }

    if (wideTable)
        out << "\\end{tabular*}" << std::endl;
    else
        out << "\\end{tabular}" << std::endl;
}

/* ----------------------------------------------------------------------------------------------------------------- */

namespace {

template <class NonterminalSymbolType, class TerminalSymbolType>
void grammarRules(ext::ostream& out, const ext::map<NonterminalSymbolType, ext::set<ext::vector<ext::variant<TerminalSymbolType, NonterminalSymbolType>>>>& rawRules)
{
    for (const auto& [lhs, rhsSet] : rawRules) {
        if (rhsSet.empty()) {
            continue;
        }

        out << escape(factory::StringDataFactory::toString(lhs)) << " \\rightarrow ";

        for (auto itRhs = rhsSet.begin(); itRhs != rhsSet.end(); ++itRhs) {
            if (itRhs != rhsSet.begin()) {
                out << " \\mid ";
            }

            for (const auto& symb : *itRhs) {
                out << escape(factory::StringDataFactory::toString(symb)) << " ";
            }

            if (itRhs->empty()) {
                out << "\\varepsilon";
            }
        }
        out << ", \\\\" << std::endl;
    }
}

}

template <class GrammarType>
void LatexConverter::convertGrammar(ext::ostream& out, const GrammarType& grammar)
{
    out << "";
    out << "$$G = (\\{";
    for (auto it = grammar.getNonterminalAlphabet().begin(); it != grammar.getNonterminalAlphabet().end(); ++it) {
        if (it != grammar.getNonterminalAlphabet().begin())
            out << ", ";
        out << escape(factory::StringDataFactory::toString(*it));
    }
    out << "\\}, \\{";
    for (auto it = grammar.getTerminalAlphabet().begin(); it != grammar.getTerminalAlphabet().end(); ++it) {
        if (it != grammar.getTerminalAlphabet().begin())
            out << ", ";
        out << escape(factory::StringDataFactory::toString(*it));
    }
    out << "\\}, P, ";
    out << escape(factory::StringDataFactory::toString(grammar.getInitialSymbol()));
    out << ")$$"
        << " \\\\" << std::endl
        << std::endl;

    out << "\\begin{array}{l}" << std::endl;
    out << "P = \\{\\\\" << std::endl;
    grammarRules(out, grammar::RawRules::getRawRules(grammar));
    out << "\\}" << std::endl;
    out << "\\end{array}" << std::endl;
}

} /* namespace convert */
