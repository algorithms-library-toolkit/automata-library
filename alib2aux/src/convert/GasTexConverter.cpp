#include <registration/AlgoRegistration.hpp>
#include "GasTexConverter.h"

namespace convert {

void GasTexConverter::printTransitionMap(const ext::map<std::pair<std::string, std::string>, std::string>& transitionMap, ext::ostream& out)
{
    for (const auto& transition : transitionMap) {
        std::string from = transition.first.first;
        std::string to = transition.first.second;
        std::string symbols = transition.second;

        if (from == to) {
            out << "\\drawloop(";
            out << from;
            out << "){";
            out << symbols;
            out << "}\n";

        } else {
            out << "\\drawedge(";
            out << from;
            out << ",";
            out << to;
            out << "){";
            out << symbols;
            out << "}\n";
        }
    }
}

} /* namespace convert */

namespace {

auto GasTexConverterEpsilonNFA = registration::AbstractRegister<convert::GasTexConverter, std::string, const automaton::EpsilonNFA<>&>(convert::GasTexConverter::convert);
auto GasTexConverterMultiInitialStateNFA = registration::AbstractRegister<convert::GasTexConverter, std::string, const automaton::MultiInitialStateNFA<>&>(convert::GasTexConverter::convert);
auto GasTexConverterNFA = registration::AbstractRegister<convert::GasTexConverter, std::string, const automaton::NFA<>&>(convert::GasTexConverter::convert);
auto GasTexConverterDFA = registration::AbstractRegister<convert::GasTexConverter, std::string, const automaton::DFA<>&>(convert::GasTexConverter::convert);
auto GasTexConverterExtendedNFA = registration::AbstractRegister<convert::GasTexConverter, std::string, const automaton::ExtendedNFA<>&>(convert::GasTexConverter::convert);
auto GasTexConverterCompactNFA = registration::AbstractRegister<convert::GasTexConverter, std::string, const automaton::CompactNFA<>&>(convert::GasTexConverter::convert);
auto GasTexConverterNFTA = registration::AbstractRegister<convert::GasTexConverter, std::string, const automaton::NFTA<>&>(convert::GasTexConverter::convert);
auto GasTexConverterDFTA = registration::AbstractRegister<convert::GasTexConverter, std::string, const automaton::DFTA<>&>(convert::GasTexConverter::convert);
auto GasTexConverterDPDA = registration::AbstractRegister<convert::GasTexConverter, std::string, const automaton::DPDA<>&>(convert::GasTexConverter::convert);
auto GasTexConverterSinglePopDPDA = registration::AbstractRegister<convert::GasTexConverter, std::string, const automaton::SinglePopDPDA<>&>(convert::GasTexConverter::convert);
auto GasTexConverterInputDrivenDPDA = registration::AbstractRegister<convert::GasTexConverter, std::string, const automaton::InputDrivenDPDA<>&>(convert::GasTexConverter::convert);
auto GasTexConverterInputDrivenNPDA = registration::AbstractRegister<convert::GasTexConverter, std::string, const automaton::InputDrivenNPDA<>&>(convert::GasTexConverter::convert);
auto GasTexConverterVisiblyPushdownDPDA = registration::AbstractRegister<convert::GasTexConverter, std::string, const automaton::VisiblyPushdownDPDA<>&>(convert::GasTexConverter::convert);
auto GasTexConverterVisiblyPushdownNPDA = registration::AbstractRegister<convert::GasTexConverter, std::string, const automaton::VisiblyPushdownNPDA<>&>(convert::GasTexConverter::convert);
auto GasTexConverterRealTimeHeightDeterministicDPDA = registration::AbstractRegister<convert::GasTexConverter, std::string, const automaton::RealTimeHeightDeterministicDPDA<>&>(convert::GasTexConverter::convert);
auto GasTexConverterRealTimeHeightDeterministicNPDA = registration::AbstractRegister<convert::GasTexConverter, std::string, const automaton::RealTimeHeightDeterministicNPDA<>&>(convert::GasTexConverter::convert);
auto GasTexConverterNPDA = registration::AbstractRegister<convert::GasTexConverter, std::string, const automaton::NPDA<>&>(convert::GasTexConverter::convert);
auto GasTexConverterSinglePopNPDA = registration::AbstractRegister<convert::GasTexConverter, std::string, const automaton::SinglePopNPDA<>&>(convert::GasTexConverter::convert);
auto GasTexConverterOneTapeDTM = registration::AbstractRegister<convert::GasTexConverter, std::string, const automaton::OneTapeDTM<>&>(convert::GasTexConverter::convert);

} /* namespace */
