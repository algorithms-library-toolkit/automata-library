#include <alib/vector>
#include <catch2/catch.hpp>
#include <ext/sstream>

#include "testing/TestFiles.hpp"
#include "testing/TimeoutAqlTest.hpp"

const unsigned RAND_STATES = 15;
const unsigned RAND_ALPHABET = 5;
const double RAND_DENSITY = 2.5;
const size_t ITERATIONS = 50;

std::string qGenNFA()
{
    return ext::concat("execute automaton::generate::RandomAutomatonFactory (size_t)", rand() % RAND_STATES + 1, " <(alphabet::generate::GenerateAlphabet (size_t)", rand() % RAND_ALPHABET + 1, " true true) (double)\"", RAND_DENSITY, "\"");
}

static const std::string qMinimize("automaton::simplify::efficient::EpsilonRemoverIncoming - | automaton::determinize::Determinize - | "
                                   "automaton::simplify::Trim - | automaton::simplify::Minimize - | automaton::simplify::Normalize -");

void fileTest(const std::string& pipeline)
{
    for (const std::string& inputFile : TestFiles::Get("/automaton/aconversion.test.*.xml$")) {
        ext::vector<std::string> qs = {
            ext::concat("execute < \"", inputFile, "\" > $gen"),
            ext::concat("quit compare::AutomatonCompare <( $gen | ", qMinimize, " ) <( $gen | ", pipeline, " | ", qMinimize, ")")};
        TimeoutAqlTest(10s, qs);
    }
}

void randomTest(const std::string& pipeline)
{
    for (size_t i = 0; i < ITERATIONS; i++) {
        ext::vector<std::string> qs = {
            ext::concat(qGenNFA(), " > $gen"),
            ext::concat("quit compare::AutomatonCompare <( $gen | ", qMinimize, " ) <( $gen | ", pipeline, " | ", qMinimize, ")")};
        TimeoutAqlTest(10s, qs);
    }
}

TEST_CASE("RG-RG conversions test", "[integration]")
{
    auto pipeline = GENERATE(as<std::string>(),
                             "automaton::convert::ToGrammarLeftRG  - | grammar::convert::ToGrammarRightRG - | grammar::convert::ToAutomaton -",
                             "automaton::convert::ToGrammarRightRG - | grammar::convert::ToGrammarLeftRG  - | grammar::convert::ToAutomaton -");

    SECTION("Files tests")
    {
        fileTest(pipeline);
    }

    SECTION("Random tests")
    {
        randomTest(pipeline);
    }
}

TEST_CASE("FA-RE conversions test I", "[integration]")
{
    auto pipeline = GENERATE(as<std::string>(),
                             "(FormalRegExp) automaton::convert::ToRegExpAlgebraic - | regexp::convert::ToAutomatonDerivation -",
                             "automaton::convert::ToRegExpAlgebraic                - | regexp::convert::ToAutomatonGlushkov   -",
                             "automaton::convert::ToRegExpAlgebraic                - | regexp::convert::ToAutomatonThompson   -");

    SECTION("Files tests")
    {
        fileTest(pipeline);
    }

    SECTION("Random tests")
    {
        randomTest(pipeline);
    }
}

TEST_CASE("FA-RE conversions test II", "[integration]")
{
    auto pipeline = GENERATE(as<std::string>(),
                             "(FormalRegExp) automaton::convert::ToRegExpStateElimination - | regexp::convert::ToAutomatonDerivation -",
                             "automaton::convert::ToRegExpStateElimination                - | regexp::convert::ToAutomatonGlushkov   -",
                             "automaton::convert::ToRegExpStateElimination                - | regexp::convert::ToAutomatonThompson   -");

    SECTION("Files tests")
    {
        fileTest(pipeline);
    }

    SECTION("Random tests")
    {
        randomTest(pipeline);
    }
}

TEST_CASE("FA-RE conversions test III", "[integration]")
{
    auto pipeline = GENERATE(as<std::string>(),
                             "automaton::convert::ToRegExpKleene           - | regexp::convert::ToAutomatonDerivation -",
                             "automaton::convert::ToRegExpKleene           - | regexp::convert::ToAutomatonGlushkov   -",
                             "automaton::convert::ToRegExpKleene           - | regexp::convert::ToAutomatonThompson   -");

    SECTION("Files tests")
    {
        fileTest(pipeline);
    }

    SECTION("Random tests")
    {
        randomTest(pipeline);
    }
}

TEST_CASE("RE-RG-RG-FA conversions test I", "[integration]")
{
    auto pipeline = GENERATE(as<std::string>(),
                             "automaton::convert::ToRegExpAlgebraic        - | regexp::convert::ToGrammarRightRGDerivation - | grammar::convert::ToGrammarLeftRG - | grammar::convert::ToAutomaton -",
                             "automaton::convert::ToRegExpAlgebraic        - | regexp::convert::ToGrammarRightRGGlushkov   - | grammar::convert::ToGrammarLeftRG - | grammar::convert::ToAutomaton -");

    SECTION("Files tests")
    {
        fileTest(pipeline);
    }

    SECTION("Random tests")
    {
        randomTest(pipeline);
    }
}

TEST_CASE("RE-RG-RG-FA conversions test II", "[integration]")
{
    auto pipeline = GENERATE(as<std::string>(),
                             "automaton::convert::ToRegExpStateElimination - | regexp::convert::ToGrammarRightRGDerivation - | grammar::convert::ToGrammarLeftRG - | grammar::convert::ToAutomaton -",
                             "automaton::convert::ToRegExpStateElimination - | regexp::convert::ToGrammarRightRGGlushkov   - | grammar::convert::ToGrammarLeftRG - | grammar::convert::ToAutomaton -");

    SECTION("Files tests")
    {
        fileTest(pipeline);
    }

    SECTION("Random tests")
    {
        randomTest(pipeline);
    }
}

TEST_CASE("RE-RG-RG-FA conversions test III", "[integration]")
{
    auto pipeline = GENERATE(as<std::string>(),
                             "automaton::convert::ToRegExpKleene           - | regexp::convert::ToGrammarRightRGDerivation - | grammar::convert::ToGrammarLeftRG - | grammar::convert::ToAutomaton -",
                             "automaton::convert::ToRegExpKleene           - | regexp::convert::ToGrammarRightRGGlushkov   - | grammar::convert::ToGrammarLeftRG - | grammar::convert::ToAutomaton -");

    SECTION("Files tests")
    {
        fileTest(pipeline);
    }

    SECTION("Random tests")
    {
        randomTest(pipeline);
    }
}

TEST_CASE("RG-RE-FA conversions test I", "[integration]")
{
    auto pipeline = GENERATE(as<std::string>(),
                             "automaton::convert::ToGrammarLeftRG  - | (FormalRegExp) grammar::convert::ToRegExp - | regexp::convert::ToAutomatonDerivation -",
                             "automaton::convert::ToGrammarRightRG - | grammar::convert::ToRegExp                - | regexp::convert::ToAutomatonDerivation -");

    SECTION("Files tests")
    {
        fileTest(pipeline);
    }

    SECTION("Random tests")
    {
        randomTest(pipeline);
    }
}

TEST_CASE("RG-RE-FA conversions test II", "[integration]")
{
    auto pipeline = GENERATE(as<std::string>(),
                             "automaton::convert::ToGrammarLeftRG  - | grammar::convert::ToRegExp - | regexp::convert::ToAutomatonThompson   -",
                             "automaton::convert::ToGrammarRightRG - | grammar::convert::ToRegExp - | regexp::convert::ToAutomatonThompson   -");

    SECTION("Files tests")
    {
        fileTest(pipeline);
    }

    SECTION("Random tests")
    {
        randomTest(pipeline);
    }
}

TEST_CASE("RG-RE-FA conversions test III", "[integration]")
{
    auto pipeline = GENERATE(as<std::string>(),
                             "automaton::convert::ToGrammarLeftRG  - | grammar::convert::ToRegExp - | regexp::convert::ToAutomatonGlushkov                -",
                             "automaton::convert::ToGrammarRightRG - | grammar::convert::ToRegExp - | regexp::convert::ToAutomatonGlushkov (FormalRegExp) -");

    SECTION("Files tests")
    {
        fileTest(pipeline);
    }

    SECTION("Random tests")
    {
        randomTest(pipeline);
    }
}
