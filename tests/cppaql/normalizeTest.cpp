#include <alib/vector>
#include <catch2/catch.hpp>
#include <extensions/container/string.hpp>

#include "testing/TestFiles.hpp"
#include "testing/TimeoutAqlTest.hpp"

static constexpr double RAND_DENSITY = 50;
static constexpr size_t RAND_NONTERMINALS = 2;
static constexpr size_t RAND_TERMINALS = 4;
static constexpr size_t STRING_LENGTHS = 6;
static constexpr size_t RANDOM_ITERATIONS = 50;


std::string qGenerateCfg(const std::string& var)
{
    return ext::concat("execute grammar::generate::RandomGrammarFactory <(alphabet::generate::GenerateAlphabet (int) ", rand() % RAND_NONTERMINALS + 1, " true false) <(alphabet::generate::GenerateAlphabet (int) ", rand() % RAND_TERMINALS + 1, " true true) (double) ", RAND_DENSITY, " | grammar::simplify::EpsilonRemover - | grammar::simplify::SimpleRulesRemover - > $", var);
}

TEST_CASE("Normalize test", "[integration]")
{
    auto algorithm = GENERATE(as<std::string>(), "grammar::simplify::ToCNF", "grammar::simplify::ToGNF");

    SECTION("Test Files")
    {
        for (const std::string& file : TestFiles::Get("/grammar/anormalization.test.*.xml$")) {
            ext::vector<std::string> qs = {
                ext::concat("execute < \"", file, "\" > $cfg"),
                ext::concat("execute ", algorithm, " $cfg > $cfg2"),
                ext::concat("execute grammar::generate::GenerateUpToLength $cfg ", STRING_LENGTHS, " > $str"),
                ext::concat("execute grammar::generate::GenerateUpToLength $cfg2 ", STRING_LENGTHS, " > $str2"),
                "quit compare::IsSame $str $str2",
            };

            TimeoutAqlTest(1s, qs);
        }
    }

    SECTION("Random tests")
    {
        for (size_t i = 0; i < RANDOM_ITERATIONS; i++) {
            ext::vector<std::string> qs = {
                qGenerateCfg("cfg"),
                ext::concat("execute ", algorithm, " $cfg > $cfg2"),
                ext::concat("execute grammar::generate::GenerateUpToLength $cfg ", STRING_LENGTHS, " > $str"),
                ext::concat("execute grammar::generate::GenerateUpToLength $cfg2 ", STRING_LENGTHS, " > $str2"),
                "quit compare::IsSame $str $str2",
            };

            TimeoutAqlTest(1s, qs);
        }
    }
}
