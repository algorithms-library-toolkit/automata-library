#include <alib/vector>
#include <catch2/catch.hpp>
#include <ext/sstream>

#include "testing/TestFiles.hpp"
#include "testing/TimeoutAqlTest.hpp"

const unsigned RAND_STATES = 15;
const unsigned RAND_ALPHABET = 5;
const double RAND_DENSITY = 2.5;
const size_t ITERATIONS = 100;

static std::string qGenNFA()
{
    return ext::concat("execute automaton::generate::RandomAutomatonFactory (size_t)", rand() % RAND_STATES + 1, " <(alphabet::generate::GenerateAlphabet (size_t)", rand() % RAND_ALPHABET + 1, " (bool)true true) (double)\"", RAND_DENSITY, "\"");
}

TEST_CASE("Minimization FA test", "[integration]")
{
    static const std::string qDet("automaton::determinize::Determinize - | automaton::simplify::Trim -");
    static const std::string qMinimizeHop = ext::concat(qDet, " | automaton::simplify::Minimize - | automaton::simplify::Normalize -");
    static const std::string qMinimizeBrz = ext::concat(qDet, " | automaton::simplify::MinimizeBrzozowski - | automaton::simplify::Trim - | automaton::simplify::Normalize -");
    static const std::string qMinimizeDis = ext::concat(qDet, " | automaton::simplify::MinimizeByPartitioning - <( $gen | ", qDet, " | automaton::properties::DistinguishableStates - | relation::RelationComplement - <( $gen | ", qDet, " | component::States::get - ) | relation::InducedEquivalence - ) | automaton::simplify::Normalize -");
    static const std::string qMinimizeUnd = ext::concat(qDet, " | automaton::simplify::MinimizeByPartitioning - <( $gen | ", qDet, " | automaton::properties::UndistinguishableStates - | relation::InducedEquivalence - ) | automaton::simplify::Normalize -");


    SECTION("Files tests")
    {
        for (const std::string& inputFile : TestFiles::Get("/automaton/aconversion.test.*.xml$")) {
            ext::vector<std::string> qs;

            qs = {
                ext::concat("execute < \"", inputFile, "\" > $gen"),
                ext::concat("quit compare::AutomatonCompare <( $gen | ", qMinimizeHop, " ) <( $gen | ", qMinimizeBrz, ")")};
            TimeoutAqlTest(2s, qs);

            qs = {
                ext::concat("execute < \"", inputFile, "\" > $gen"),
                ext::concat("quit compare::AutomatonCompare <( $gen | ", qMinimizeHop, " ) <( $gen | ", qMinimizeDis, ")")};
            TimeoutAqlTest(2s, qs);

            qs = {
                ext::concat("execute < \"", inputFile, "\" > $gen"),
                ext::concat("quit compare::AutomatonCompare <( $gen | ", qMinimizeHop, " ) <( $gen | ", qMinimizeUnd, ")")};
            TimeoutAqlTest(2s, qs);
        }
    }

    SECTION("Random tests")
    {
        for (size_t i = 0; i < ITERATIONS; i++) {
            ext::vector<std::string> qs;

            qs = {
                ext::concat(qGenNFA(), " > $gen"),
                ext::concat("quit compare::AutomatonCompare <( $gen | ", qMinimizeHop, " ) <( $gen | ", qMinimizeBrz, ")")};
            TimeoutAqlTest(2s, qs);

            qs = {
                ext::concat(qGenNFA(), " > $gen"),
                ext::concat("quit compare::AutomatonCompare <( $gen | ", qMinimizeHop, " ) <( $gen | ", qMinimizeDis, ")")};
            TimeoutAqlTest(2s, qs);

            qs = {
                ext::concat(qGenNFA(), " > $gen"),
                ext::concat("quit compare::AutomatonCompare <( $gen | ", qMinimizeHop, " ) <( $gen | ", qMinimizeUnd, ")")};
            TimeoutAqlTest(2s, qs);
        }
    }
}
