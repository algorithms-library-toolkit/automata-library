#include <alib/vector>
#include <catch2/catch.hpp>
#include <ext/sstream>

#include "testing/TestFiles.hpp"
#include "testing/TimeoutAqlTest.hpp"

enum class EGenerateType {
    PATTERN,
    SUBTREE,
    NONLINEAR_PATTERN,
    EXTENDED_PATTERN,
    NONLINEAR_PATTERN_SINGLE_VAR,
    UNORDERED_PATTERN,
    UNORDERED_SUBTREE,

    UNRANKED_PATTERN,
    UNRANKED_EXTENDED_PATTERN,
    UNRANKED_SUBJECT,
    SUBJECT,
};

std::ostream& operator<<(std::ostream& os, const EGenerateType& type)
{
    switch (type) {
    case EGenerateType::PATTERN:
        return (os << "PATTERN");
    case EGenerateType::SUBTREE:
        return (os << "SUBTREE");
    case EGenerateType::NONLINEAR_PATTERN:
        return (os << "NONLINEAR_PATTERN");
    case EGenerateType::EXTENDED_PATTERN:
        return (os << "EXTENDED_PATTERN");
    case EGenerateType::NONLINEAR_PATTERN_SINGLE_VAR:
        return (os << "NONLINEAR_PATTERN_SINGLE_VAR");
    case EGenerateType::SUBJECT:
        return (os << "SUBJECT");
    case EGenerateType::UNORDERED_PATTERN:
        return (os << "UNORDERED_PATTERN");
    case EGenerateType::UNORDERED_SUBTREE:
        return (os << "UNORDERED_SUBTREE");

    case EGenerateType::UNRANKED_PATTERN:
        return (os << "UNRANKED_PATTERN");
    case EGenerateType::UNRANKED_EXTENDED_PATTERN:
        return (os << "UNRANKED_EXTENDED_PATTERN");
    case EGenerateType::UNRANKED_SUBJECT:
        return (os << "UNRANKED_SUBJECT");
    default:
        return (os << "Unhandled EGenerateType");
    }
}

const size_t PATTERN_SIZE = 4;
const size_t ALPHABET_SIZE = 3;
const size_t SUBJECT_HEIGHT = 25;
const size_t PATTERN_HEIGHT = 2;
const size_t RANDOM_ITERATIONS = 20;
const size_t NODE_WILDCARD_PROBABILITY = 10;

static std::string qGen(const EGenerateType& type, size_t height, size_t nodes, size_t alphSize, const std::string& output)
{
    std::ostringstream oss;
    oss << "execute ";

    switch (type) {
    case EGenerateType::SUBJECT:
    case EGenerateType::SUBTREE:
        oss << "tree::generate::RandomRankedTreeFactory";
        break;
    case EGenerateType::UNRANKED_SUBJECT:
        oss << "tree::generate::RandomUnrankedTreeFactory";
        break;
    case EGenerateType::PATTERN:
        oss << "tree::generate::RandomRankedPatternFactory";
        break;
    case EGenerateType::EXTENDED_PATTERN:
        oss << "tree::generate::RandomRankedExtendedPatternFactory";
        break;
    case EGenerateType::NONLINEAR_PATTERN:
    case EGenerateType::NONLINEAR_PATTERN_SINGLE_VAR:
        oss << "tree::generate::RandomRankedNonlinearPatternFactory";
        break;
    case EGenerateType::UNORDERED_PATTERN:
        oss << "(UnorderedRankedPattern) tree::generate::RandomRankedPatternFactory";
        break;
    case EGenerateType::UNORDERED_SUBTREE:
        oss << "(UnorderedRankedTree) tree::generate::RandomRankedTreeFactory";
        break;
    case EGenerateType::UNRANKED_PATTERN:
        oss << "tree::generate::RandomUnrankedPatternFactory";
        break;
    case EGenerateType::UNRANKED_EXTENDED_PATTERN:
        oss << "tree::generate::RandomUnrankedExtendedPatternFactory";
        break;
    }

    oss << " (int)" << height;
    oss << " (int)" << nodes;
    oss << " (int)" << rand() % alphSize + 1;
    oss << " (bool) false"; /* alphRand */
    if (type == EGenerateType::NONLINEAR_PATTERN || type == EGenerateType::NONLINEAR_PATTERN_SINGLE_VAR)
        oss << " (bool)" << (type == EGenerateType::NONLINEAR_PATTERN_SINGLE_VAR);
    oss << " (int)" << 2; /* rank */
    if (type == EGenerateType::EXTENDED_PATTERN || type == EGenerateType::UNRANKED_EXTENDED_PATTERN)
        oss << " (double)" << NODE_WILDCARD_PROBABILITY;
    oss << "> $" << output;
    return oss.str();
}

static std::string search_replace(std::string text, const std::string& search, const std::string& replace)
{
    size_t pos = text.find(search);
    text.replace(pos, search.size(), replace);
    return text;
}

static std::vector<std::pair<std::string, std::string>> pair_pattern_subject(const std::vector<std::string>& files, const std::string& pattern, const std::string& subj)
{
    std::vector<std::pair<std::string, std::string>> res;
    res.reserve(files.size());
    for (const auto& file : files)
        res.emplace_back(file, search_replace(file, pattern, subj));

    return res;
}


void runTest(const std::string& exactPipeline, const std::string& pipeline, const std::string& pFile, const std::string& sFile)
{
    ext::vector<std::string> qs = {
        ext::concat("execute < \"", pFile, "\" > $pattern"),
        ext::concat("execute < \"", sFile, "\" > $subject"),
        ext::concat("execute ", exactPipeline, " > $res1"),
        ext::concat("execute ", pipeline, " > $res2"),
        "quit compare::IsSame <(stats::SizeStat $res1) <(stats::SizeStat $res2)",
    };

    TimeoutAqlTest(10s, qs);
}

void runRandom(const std::string& exactPipeline, const std::string& pipeline, const EGenerateType& subjectType, const EGenerateType& patternType, const size_t& subjSize)
{
    ext::vector<std::string> qs = {
        qGen(patternType, PATTERN_HEIGHT, PATTERN_SIZE, ALPHABET_SIZE, "pattern"),
        qGen(subjectType, SUBJECT_HEIGHT, subjSize, ALPHABET_SIZE, "subject"),
        ext::concat("execute ", exactPipeline, " > $res1"),
        ext::concat("execute ", pipeline, " > $res2"),
        "quit compare::IsSame <(stats::SizeStat $res1) <(stats::SizeStat $res2)",
    };

    TimeoutAqlTest(10s, qs);
}

TEST_CASE("Arbology tests | nonlinear pattern", "[integration]")
{
    auto definition = GENERATE(as<std::tuple<std::string, std::string, size_t>>(),
                               std::make_tuple("Exact Nonlinear Pattern Matching Using Compressed Bit Vectors (PrefixRankedBar)",
                                               "arbology::indexing::NonlinearCompressedBitParallelIndexConstruction (PrefixRankedBarTree)$subject | arbology::query::NonlinearCompressedBitParallelismPatterns - (PrefixRankedBarNonlinearPattern)$pattern",
                                               1000),
                               std::make_tuple("Exact Nonlinear Pattern Matching Using Full And Linear Index (PrefixRanked)",
                                               "arbology::indexing::NonlinearFullAndLinearIndexConstruction (PrefixRankedTree)$subject | arbology::query::NonlinearFullAndLinearIndexPatterns - (PrefixRankedNonlinearPattern) $pattern",
                                               1000),
                               std::make_tuple("Exact Nonlinear Pattern Matching Using Full And Linear Index (PrefixRankedBar)",
                                               "arbology::indexing::NonlinearFullAndLinearIndexConstruction (PrefixRankedBarTree)$subject | arbology::query::NonlinearFullAndLinearIndexPatterns - (PrefixRankedBarNonlinearPattern) $pattern",
                                               1000),
                               std::make_tuple("Exact Pattern Match (NonlinearPattern PrefixRankedBar)",
                                               "arbology::exact::ExactPatternMatch (PrefixRankedBarTree)$subject (PrefixRankedBarNonlinearPattern) <(component::GeneralAlphabet::add $pattern <(component::GeneralAlphabet::get $subject))",
                                               1000),
                               std::make_tuple("Exact Knuth Morris Pratt (NonlinearPattern PrefixRankedBar)",
                                               "arbology::exact::KnuthMorrisPratt (PrefixRankedBarTree)$subject (PrefixRankedBarNonlinearPattern)$pattern",
                                               1000),
                               std::make_tuple("Exact Knuth Morris Pratt (NonlinearPattern PrefixRanked)",
                                               "arbology::exact::KnuthMorrisPratt (PrefixRankedTree)$subject (PrefixRankedNonlinearPattern)$pattern",
                                               1000),
                               std::make_tuple("Exact Boyer Moore Horspool (NonlinearPattern PrefixRankedBar)",
                                               "arbology::exact::BoyerMooreHorspool (PrefixRankedBarTree)$subject (PrefixRankedBarNonlinearPattern) <(component::GeneralAlphabet::add $pattern <(component::GeneralAlphabet::get $subject))",
                                               1000),
                               std::make_tuple("Exact Reversed Boyer Moore Horspool (NonlinearPattern PrefixRankedBar)",
                                               "arbology::exact::ReversedBoyerMooreHorspool (PrefixRankedBarTree)$subject (PrefixRankedBarNonlinearPattern) <(component::GeneralAlphabet::add $pattern <(component::GeneralAlphabet::get $subject))",
                                               1000),
                               std::make_tuple("Exact Reversed Boyer Moore Horspool (NonlinearPattern PrefixRanked)",
                                               "arbology::exact::ReversedBoyerMooreHorspool (PrefixRankedTree)$subject (PrefixRankedNonlinearPattern) <(component::GeneralAlphabet::add $pattern <(component::GeneralAlphabet::get $subject))",
                                               1000));

    const char* exact = "arbology::exact::ExactPatternMatch $subject $pattern";
    auto pattern = EGenerateType::NONLINEAR_PATTERN;


    SECTION("Test files")
    {
        for (const std::pair<std::string, std::string>& files : pair_pattern_subject(TestFiles::Get("/tree/aarbology.test[0-9]+.nonlinear.pattern.xml$"), ".nonlinear.pattern.xml", ".subject.xml")) {
            CAPTURE(std::get<0>(definition), std::get<1>(definition), files.first, files.second);
            runTest(exact, std::get<1>(definition), files.first, files.second);
        }
    }

    SECTION("Random tests")
    {
        for (size_t i = 0; i < RANDOM_ITERATIONS; i++) {
            CAPTURE(std::get<0>(definition), std::get<1>(definition), pattern, std::get<2>(definition));
            runRandom(exact, std::get<1>(definition), EGenerateType::SUBJECT, pattern, std::get<2>(definition));
        }
    }
}

// --------------------------------------------------------------------------------------------------------------------

TEST_CASE("Arbology tests | ranked nonlinear pattern ends", "[integration]")
{
    auto definition = GENERATE(as<std::tuple<std::string, std::string, size_t>>(),
                               std::make_tuple("Exact Nonlinear Tree Pattern Automaton (PrefixRanked)",
                                               "arbology::exact::ExactNonlinearTreePatternAutomaton (PrefixRankedTree)$subject <(component::SubtreeWildcardSymbol::get $pattern) <(component::NonlinearAlphabet::get $pattern) | automaton::determinize::Determinize - |"
                                               "automaton::run::Result - (LinearString)(PrefixRankedNonlinearPattern) $pattern { } | dataAccess::PairSetSecond -",
                                               80));

    const char* exact = "arbology::exact::ExactPatternMatch $subject $pattern | arbology::transform::BeginToEndIndex (PrefixRankedTree) $subject -";
    auto pattern = EGenerateType::NONLINEAR_PATTERN_SINGLE_VAR;

    SECTION("Test files")
    {
        for (const std::pair<std::string, std::string>& files : pair_pattern_subject(TestFiles::Get("/tree/aarbology.test[0-9]+.nonlinear.pattern.xml$"), ".nonlinear.pattern.xml", ".subject.xml")) {
            CAPTURE(std::get<0>(definition), std::get<1>(definition), files.first, files.second);
            runTest(exact, std::get<1>(definition), files.first, files.second);
        }
    }

    SECTION("Random tests")
    {
        for (size_t i = 0; i < RANDOM_ITERATIONS; i++) {
            CAPTURE(std::get<0>(definition), std::get<1>(definition), pattern, std::get<2>(definition));
            runRandom(exact, std::get<1>(definition), EGenerateType::SUBJECT, pattern, std::get<2>(definition));
        }
    }
}

// --------------------------------------------------------------------------------------------------------------------

TEST_CASE("Arbology tests | nonlinear pattern ends", "[integration]")
{
    auto definition = GENERATE(as<std::tuple<std::string, std::string, size_t>>(),
                               std::make_tuple("Exact Nonlinear Tree Pattern Automaton (PrefixRankedBar)",
                                               "arbology::exact::ExactNonlinearTreePatternAutomaton (PrefixRankedBarTree)$subject <(component::SubtreeWildcardSymbol::get $pattern) <(component::NonlinearAlphabet::get $pattern) <(component::VariablesBarSymbol::get (PrefixRankedBarNonlinearPattern)$pattern) | "
                                               "automaton::determinize::Determinize - | automaton::run::Result - (LinearString)(PrefixRankedBarNonlinearPattern)$pattern { }",
                                               80));

    const char* exact = "arbology::exact::ExactPatternMatch $subject $pattern";
    auto pattern = EGenerateType::NONLINEAR_PATTERN_SINGLE_VAR;

    SECTION("Test files")
    {
        for (const std::pair<std::string, std::string>& files : pair_pattern_subject(TestFiles::Get("/tree/aarbology.test[0-9]+.nonlinear.pattern.xml$"), ".nonlinear.pattern.xml", ".subject.xml")) {
            CAPTURE(std::get<0>(definition), std::get<1>(definition), files.first, files.second);
            runTest(exact, std::get<1>(definition), files.first, files.second);
        }
    }

    SECTION("Random tests")
    {
        for (size_t i = 0; i < RANDOM_ITERATIONS; i++) {
            CAPTURE(std::get<0>(definition), std::get<1>(definition), pattern, std::get<2>(definition));
            runRandom(exact, std::get<1>(definition), EGenerateType::SUBJECT, pattern, std::get<2>(definition));
        }
    }
}

// --------------------------------------------------------------------------------------------------------------------

TEST_CASE("Arbology tests | pattern", "[integration]")
{
    auto definition = GENERATE(as<std::tuple<std::string, std::string, size_t>>(),
                               std::make_tuple("Exact Pattern Matching Using Full And Linear Index (PrefixRankedBar)",
                                               "arbology::indexing::FullAndLinearIndexConstruction (PrefixRankedBarTree) $subject | arbology::query::FullAndLinearIndexPatterns - (PrefixRankedBarPattern) $pattern",
                                               1000),
                               std::make_tuple("Exact Pattern Matching Using Full And Linear Index (PrefixRanked)",
                                               "arbology::indexing::FullAndLinearIndexConstruction (PrefixRankedTree) $subject | arbology::query::FullAndLinearIndexPatterns - (PrefixRankedPattern) $pattern",
                                               1000),
                               std::make_tuple("Exact Pattern Matching Using Compressed Bit Vectors (PrefixRankedBar)",
                                               "arbology::indexing::CompressedBitParallelIndexConstruction (PrefixRankedBarTree)$subject | arbology::query::CompressedBitParallelismPatterns - (PrefixRankedBarPattern)$pattern",
                                               1000),
                               std::make_tuple("Exact Knuth Morris Pratt (Pattern PrefixRankedBar)",
                                               "arbology::exact::KnuthMorrisPratt (PrefixRankedBarTree)$subject (PrefixRankedBarPattern)$pattern",
                                               1000),
                               std::make_tuple("Exact Knuth Morris Pratt (Pattern PrefixRanked)",
                                               "arbology::exact::KnuthMorrisPratt (PrefixRankedTree)$subject (PrefixRankedPattern)$pattern",
                                               1000),
                               std::make_tuple("Exact Boyer Moore Horspool (Pattern PrefixRankedBar)",
                                               "arbology::exact::BoyerMooreHorspool (PrefixRankedBarTree)$subject (PrefixRankedBarPattern) <(component::GeneralAlphabet::add $pattern <(component::GeneralAlphabet::get $subject))",
                                               1000),
                               std::make_tuple("Exact Reversed Boyer Moore Horspool (Pattern PrefixRankedBar)",
                                               "arbology::exact::ReversedBoyerMooreHorspool (PrefixRankedBarTree)$subject (PrefixRankedBarPattern) <(component::GeneralAlphabet::add $pattern <(component::GeneralAlphabet::get $subject))",
                                               1000),
                               std::make_tuple("Exact Reversed Boyer Moore Horspool (Pattern PrefixRanked)",
                                               "arbology::exact::ReversedBoyerMooreHorspool (PrefixRankedTree)$subject (PrefixRankedPattern) <(component::GeneralAlphabet::add $pattern <(component::GeneralAlphabet::get $subject))",
                                               1000),
                               std::make_tuple("Exact Quick Search (Pattern PrefixRankedBar)",
                                               "arbology::exact::QuickSearch (PrefixRankedBarTree)$subject (PrefixRankedBarPattern)<(component::GeneralAlphabet::add $pattern <(component::GeneralAlphabet::get $subject))",
                                               1000),
                               std::make_tuple("Exact Reversed Quick Search (Pattern PrefixRankedBar)",
                                               "arbology::exact::ReversedQuickSearch (PrefixRankedBarTree)$subject (PrefixRankedBarPattern)<(component::GeneralAlphabet::add $pattern <(component::GeneralAlphabet::get $subject))",
                                               1000),
                               std::make_tuple("Exact Reversed Quick Search (Pattern PrefixRanked)",
                                               "arbology::exact::ReversedQuickSearch (PrefixRankedTree)$subject (PrefixRankedPattern)<(component::GeneralAlphabet::add $pattern <(component::GeneralAlphabet::get $subject))",
                                               1000),
                               std::make_tuple("Exact Dead Zone Using Bad Character Shift And Border Array (Pattern PrefixRanked)",
                                               "arbology::exact::DeadZoneUsingBadCharacterShiftAndBorderArray (PrefixRankedTree)$subject (PrefixRankedPattern)<(component::GeneralAlphabet::add $pattern <(component::GeneralAlphabet::get $subject))",
                                               1000),
                               std::make_tuple("Exact Dead Zone Using Bad Character Shift And Border Array (Pattern PrefixRankedBar)",
                                               "arbology::exact::DeadZoneUsingBadCharacterShiftAndBorderArray (PrefixRankedBarTree)$subject (PrefixRankedBarPattern)<(component::GeneralAlphabet::add $pattern <(component::GeneralAlphabet::get $subject))",
                                               1000),
                               std::make_tuple("Exact Pattern Matching Automaton (Pattern Tree)",
                                               "automaton::run::Occurrences <(arbology::exact::ExactPatternMatchingAutomaton <(component::GeneralAlphabet::add $pattern <(component::GeneralAlphabet::get $subject)) | automaton::determinize::Determinize - ) $subject",
                                               1000),
                               std::make_tuple("Exact Pattern Matching Automaton (PrefixRankedBar)",
                                               "arbology::exact::ExactPatternMatchingAutomaton (PrefixRankedBarPattern) <(component::GeneralAlphabet::add $pattern <(component::GeneralAlphabet::get $subject)) | automaton::determinize::Determinize - | automaton::run::Occurrences - (LinearString)(PrefixRankedBarTree)$subject",
                                               1000));

    const char* exact = "arbology::exact::ExactPatternMatch $subject $pattern";
    auto pattern = EGenerateType::PATTERN;

    SECTION("Test files")
    {
        for (const std::pair<std::string, std::string>& files : pair_pattern_subject(TestFiles::Get("/tree/aarbology.test[0-9]+.pattern.xml$"), ".pattern.xml", ".subject.xml")) {
            CAPTURE(std::get<0>(definition), std::get<1>(definition), files.first, files.second);
            runTest(exact, std::get<1>(definition), files.first, files.second);
        }
    }

    SECTION("Random tests")
    {
        for (size_t i = 0; i < RANDOM_ITERATIONS; i++) {
            CAPTURE(std::get<0>(definition), std::get<1>(definition), pattern, std::get<2>(definition));
            runRandom(exact, std::get<1>(definition), EGenerateType::SUBJECT, pattern, std::get<2>(definition));
        }
    }
}

// --------------------------------------------------------------------------------------------------------------------

TEST_CASE("Arbology tests | unordered pattern", "[integration]")
{
    auto definition = GENERATE(as<std::tuple<std::string, std::string, size_t>>(),
                               std::make_tuple("Exact Pattern Matching Automaton (Pattern Tree)",
                                               "automaton::run::Occurrences <(arbology::exact::ExactPatternMatchingAutomaton <(component::GeneralAlphabet::add $pattern <(component::GeneralAlphabet::get $subject)) | automaton::determinize::Determinize - ) $subject",
                                               1000));

    const char* exact = "arbology::exact::ExactPatternMatch $subject $pattern";
    auto pattern = EGenerateType::UNORDERED_PATTERN;

    /*	SECTION ( "Test files" ) {
                    for ( const std::pair < std::string, std::string > & files : pair_pattern_subject ( TestFiles::Get ( "/tree/aarbology.test[0-9]+.pattern.xml$" ), ".pattern.xml", ".subject.xml" ) ) {
                            CAPTURE ( std::get < 0 > ( definition ), std::get < 1 > ( definition ), files.first, files.second );
                            runTest ( exact, std::get < 1 > ( definition ), files.first, files.second );
                    }
            }*/

    SECTION("Random tests")
    {
        for (size_t i = 0; i < RANDOM_ITERATIONS; i++) {
            CAPTURE(std::get<0>(definition), std::get<1>(definition), pattern, std::get<2>(definition));
            runRandom(exact, std::get<1>(definition), EGenerateType::SUBJECT, pattern, std::get<2>(definition));
        }
    }
}

// --------------------------------------------------------------------------------------------------------------------

TEST_CASE("Arbology tests | unranked pattern", "[integration]")
{
    auto definition = GENERATE(as<std::tuple<std::string, std::string, size_t>>(),
                               std::make_tuple("Exact Pattern Matching Automaton (Pattern Tree)",
                                               "automaton::run::Occurrences <(arbology::exact::ExactPatternMatchingAutomaton <(component::GeneralAlphabet::add $pattern <(component::GeneralAlphabet::get $subject)) | automaton::simplify::ToArcFactored - | automaton::determinize::Determinize - ) $subject",
                                               1000),
                               std::make_tuple("Exact Pattern Matching Automaton (As DPDA) (Pattern Tree)",
                                               "automaton::run::Occurrences <(arbology::exact::ExactPatternMatchingAutomaton <(component::GeneralAlphabet::add $pattern <(component::GeneralAlphabet::get $subject)) | automaton::simplify::ToArcFactored - | automaton::determinize::Determinize - | automaton::convert::ToPrefixPushdownAutomaton -) (PrefixBarTree) $subject",
                                               1000));

    const char* exact = "arbology::exact::ExactPatternMatch $subject $pattern";
    auto pattern = EGenerateType::UNRANKED_PATTERN;

    SECTION("Random tests")
    {
        for (size_t i = 0; i < RANDOM_ITERATIONS; i++) {
            CAPTURE(std::get<0>(definition), std::get<1>(definition), pattern, std::get<2>(definition));
            runRandom(exact, std::get<1>(definition), EGenerateType::UNRANKED_SUBJECT, pattern, std::get<2>(definition));
        }
    }
}

// --------------------------------------------------------------------------------------------------------------------

TEST_CASE("Arbology tests | unranked extended pattern", "[integration]")
{
    auto definition = GENERATE(as<std::tuple<std::string, std::string, size_t>>(),
                               std::make_tuple("Exact Pattern Matching Automaton (Pattern Tree)",
                                               "automaton::run::Occurrences <(arbology::exact::ExactPatternMatchingAutomaton <(component::GeneralAlphabet::add $pattern <(component::GeneralAlphabet::get $subject)) | automaton::simplify::ToArcFactored - | automaton::determinize::Determinize - ) $subject",
                                               1000));

    const char* exact = "arbology::exact::ExactPatternMatch $subject $pattern";
    auto pattern = EGenerateType::UNRANKED_EXTENDED_PATTERN;

    SECTION("Random tests")
    {
        for (size_t i = 0; i < RANDOM_ITERATIONS; i++) {
            CAPTURE(std::get<0>(definition), std::get<1>(definition), pattern, std::get<2>(definition));
            runRandom(exact, std::get<1>(definition), EGenerateType::UNRANKED_SUBJECT, pattern, std::get<2>(definition));
        }
    }
}

// --------------------------------------------------------------------------------------------------------------------

TEST_CASE("Arbology tests | ranked extended pattern", "[integration]")
{
    auto definition = GENERATE(as<std::tuple<std::string, std::string, size_t>>(),
                               std::make_tuple("Exact Knuth Morris Pratt (Pattern PrefixRankedExtended)",
                                               "arbology::exact::KnuthMorrisPratt (PrefixRankedTree)$subject (PrefixRankedExtendedPattern)$pattern",
                                               1000));

    const char* exact = "arbology::exact::ExactPatternMatch $subject $pattern";
    auto pattern = EGenerateType::EXTENDED_PATTERN;

    SECTION("Random tests")
    {
        for (size_t i = 0; i < RANDOM_ITERATIONS; i++) {
            CAPTURE(std::get<0>(definition), std::get<1>(definition), pattern, std::get<2>(definition));
            runRandom(exact, std::get<1>(definition), EGenerateType::SUBJECT, pattern, std::get<2>(definition));
        }
    }
}

// --------------------------------------------------------------------------------------------------------------------

TEST_CASE("Arbology tests | pattern ends ", "[integration]")
{
    auto definition = GENERATE(as<std::tuple<std::string, std::string, size_t>>(),
                               std::make_tuple("Exact Pattern Matching Using Compressed Bit Vectors (PrefixRanked)",
                                               "arbology::indexing::CompressedBitParallelIndexConstruction (PrefixRankedTree)$subject | arbology::query::CompressedBitParallelismPatterns - (PrefixRankedPattern)$pattern",
                                               1000),

                               std::make_tuple("Exact Pattern Matching Automaton (PrefixRanked)",
                                               "arbology::exact::ExactPatternMatchingAutomaton (PrefixRankedPattern) <(component::GeneralAlphabet::add $pattern <(component::GeneralAlphabet::get $subject) ) | automaton::determinize::Determinize - | "
                                               "automaton::run::Occurrences - (LinearString)(PrefixRankedTree) $subject",
                                               1000),

                               std::make_tuple("Exact Tree Pattern Automaton (PrefixRanked)",
                                               "arbology::exact::ExactTreePatternAutomaton (PrefixRankedTree)$subject <(component::SubtreeWildcardSymbol::get $pattern) | automaton::determinize::Determinize - | automaton::run::Result - (LinearString)(PrefixRankedPattern)$pattern { }",
                                               120));

    const char* exact = "arbology::exact::ExactPatternMatch (PrefixRankedTree)$subject (PrefixRankedPattern)$pattern | arbology::transform::BeginToEndIndex (PrefixRankedTree)$subject -";
    auto pattern = EGenerateType::PATTERN;

    SECTION("Test files")
    {
        for (const std::pair<std::string, std::string>& files : pair_pattern_subject(TestFiles::Get("/tree/aarbology.test[0-9]+.pattern.xml$"), ".pattern.xml", ".subject.xml")) {
            CAPTURE(std::get<0>(definition), std::get<1>(definition), files.first, files.second);
            runTest(exact, std::get<1>(definition), files.first, files.second);
        }
    }

    SECTION("Random tests")
    {
        for (size_t i = 0; i < RANDOM_ITERATIONS; i++) {
            CAPTURE(std::get<0>(definition), std::get<1>(definition), pattern, std::get<2>(definition));
            runRandom(exact, std::get<1>(definition), EGenerateType::SUBJECT, pattern, std::get<2>(definition));
        }
    }
}
// --------------------------------------------------------------------------------------------------------------------

TEST_CASE("Arbology tests | subtree", "[integration]")
{
    auto definition = GENERATE(as<std::tuple<std::string, std::string, size_t>>(),
                               std::make_tuple("Exact Subtree Automaton (Tree)",
                                               "automaton::run::Occurrences <(arbology::exact::ExactSubtreeMatchingAutomaton $pattern | automaton::determinize::Determinize -) $subject",
                                               1000),
                               std::make_tuple("Exact Minimized Subtree Automaton (Tree)",
                                               "automaton::run::Occurrences <(arbology::exact::ExactSubtreeMatchingAutomaton $pattern | automaton::determinize::Determinize - | "
                                               "automaton::simplify::Trim - | automaton::simplify::Minimize -) $subject",
                                               1000),
                               std::make_tuple("Exact Boyer Moore Horspool (Subtree PrefixRankedBar)",
                                               "arbology::exact::BoyerMooreHorspool (PrefixRankedBarTree)$subject (PrefixRankedBarTree) <(component::GeneralAlphabet::add $pattern <(component::GeneralAlphabet::get $subject))",
                                               1000));


    const char* exact = "arbology::exact::ExactSubtreeMatch $subject $pattern";
    auto pattern = EGenerateType::SUBTREE;

    SECTION("Test files")
    {
        for (const std::pair<std::string, std::string>& files : pair_pattern_subject(TestFiles::Get("/tree/aarbology.test[0-9]+.subtree.xml$"), ".subtree.xml", ".subject.xml")) {
            CAPTURE(std::get<0>(definition), std::get<1>(definition), files.first, files.second);
            runTest(exact, std::get<1>(definition), files.first, files.second);
        }
    }

    SECTION("Random tests")
    {
        for (size_t i = 0; i < RANDOM_ITERATIONS; i++) {
            CAPTURE(std::get<0>(definition), std::get<1>(definition), pattern, std::get<2>(definition));
            runRandom(exact, std::get<1>(definition), EGenerateType::SUBJECT, pattern, std::get<2>(definition));
        }
    }
}
