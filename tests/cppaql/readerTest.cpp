#include <alib/vector>
#include <catch2/catch.hpp>
#include <ext/sstream>

#include "testing/TestFiles.hpp"
#include "testing/TimeoutAqlTest.hpp"

TEST_CASE("ReaderTest | Xml", "[integration]")
{
    auto regexp = GENERATE(as<std::string>(), "/automaton/.*.xml$", "/grammar/.*.xml$", "/regexp/.*.xml$", "/string/.*.xml$", "/rte/.*.xml$");

    for (const std::string& file : TestFiles::Get(regexp)) {
        ext::vector<std::string> qs = {
            ext::concat("execute < \"", file, "\" > $object"),
            "execute $object > \"/dev/null\"",
        };


        TimeoutAqlTest(1s, qs);
    }
}

TEST_CASE("ReaderTest | Txt ", "[integration]")
{
    auto def = GENERATE(
        std::make_pair("automaton::Automaton", "/automaton/.*.txt$"),
        std::make_pair("regexp::RegExp", "/regexp/.*.txt$"),
        std::make_pair("grammar::Grammar", "/grammar/.*.txt$"),
        std::make_pair("string::String", "/string/.*.txt$"));

    for (const std::string& file : TestFiles::Get(def.second)) {
        ext::vector<std::string> qs = {
            ext::concat("execute cli::builtin::ReadFile \"", file, "\" | Move - | string::Parse @", def.first, " -  > $object"),
            "execute $object > \"/dev/null\"",
        };

        TimeoutAqlTest(1s, qs);
    }
}
