#pragma once

#include <istream>
#include <string>

#include <ext/exception>
#include <ext/sstream>

#include "common/ResultInterpret.h"
#include "environment/Environment.h"
#include "global/GlobalData.h"
#include "readline/IstreamLineInterface.h"
#include "readline/StringLineInterface.h"

struct AqlTestResult {
    int retcode;
    unsigned seed;
    std::string output;
};

/** @brief Runs AQL test with code stored in stream */
template <typename Stream>
AqlTestResult AqlTest(Stream& is, unsigned seed)
{
    try {
        cli::Environment environment;
        cli::CommandResult result;

        // Capture CLI output
        ext::ostringstream oss;
        common::Streams::out = ext::reference_wrapper<ext::ostream>(oss);
        common::Streams::err = ext::reference_wrapper<ext::ostream>(oss);

        // seed cli, run test file
        auto testSeed = "set seed " + ext::to_string(seed);
        result = environment.execute(testSeed);
        result = environment.execute(is);

        int returnValue;
        if (result == cli::CommandResult::QUIT || result == cli::CommandResult::RETURN) {
            returnValue = cli::ResultInterpret::cli(environment.getResult());
        } else if (result == cli::CommandResult::EOT || result == cli::CommandResult::OK) {
            returnValue = 0;
        } else {
            returnValue = 4;
        }

        return {returnValue, seed, oss.str()};
    } catch (const std::exception&) {
        ext::ostringstream oss;
        alib::ExceptionHandler::handle(oss);
        return {-1, seed, oss.str()};
    }
}
