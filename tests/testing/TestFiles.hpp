#pragma once

#include <filesystem>

#include <regex>
#include <string>
#include <vector>

class TestFiles {
    static std::filesystem::path TEST_FILES_BASEDIR;

public:
    static std::vector<std::string> Get(const std::regex& regex);
    static std::vector<std::string> Get(const std::string& regex);
    static std::string GetOne(const std::string& regex);
    static std::string GetOne(const std::regex& regex);
};
