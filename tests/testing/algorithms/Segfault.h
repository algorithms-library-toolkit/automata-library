/*
 * Segfault.h
 */

#pragma once

namespace debug {

class Segfault {
    static int* NULL_VALUE;

public:
    static int segfault();
};

} /* namespace debug */
