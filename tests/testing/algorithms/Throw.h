/*
 * Throw.h
 */

#pragma once

namespace debug {

class Throw {
public:
    static int throwme();
};

} /* namespace debug */
