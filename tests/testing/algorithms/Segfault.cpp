/*
 * Segfault.h
 */

#include <registration/AlgoRegistration.hpp>
#include "Segfault.h"

namespace debug {

int debug::Segfault::segfault()
{
    return *NULL_VALUE;
}

int* debug::Segfault::NULL_VALUE;

} /* namespace debug */

namespace {

auto SegfaultInt = registration::AbstractRegister<debug::Segfault, int>(debug::Segfault::segfault);

} /* namespace */
