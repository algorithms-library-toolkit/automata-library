#pragma once

#include <catch2/catch.hpp>

#define CHECK_IMPLY(x, y) CHECK((!(x) || (y)))
#define CHECK_EXCLUSIVE_OR(x, y) CHECK(((!(x) && (y)) || ((x) && !(y))))
#define REQUIRE_IMPLY(x, y) REQUIRE((!(x) || (y)))
#define REQUIRE_EXCLUSIVE_OR(x, y) REQUIRE(((!(x) && (y)) || ((x) && !(y))))
