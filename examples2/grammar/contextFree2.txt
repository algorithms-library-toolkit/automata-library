CFG (
{S, B, C, D, E},
{a, b},
{S -> D S | a | C D,
C -> D a B | b,
B -> S D |,
D -> B E,
E -> a | C D
},
D)
