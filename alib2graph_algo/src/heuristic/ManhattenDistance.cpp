// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#include "ManhattenDistance.hpp"

#include <common/DefaultTypes.hpp>
#include <registration/AlgoRegistration.hpp>

namespace {

auto ManhattenDistance = registration::AbstractRegister<graph::heuristic::ManhattenDistance,
                                                        std::function<DefaultCoordinateType(const ext::pair<
                                                                                                DefaultCoordinateType,
                                                                                                DefaultCoordinateType>&,
                                                                                            const ext::pair<
                                                                                                DefaultCoordinateType,
                                                                                                DefaultCoordinateType>&)>>(
    graph::heuristic::ManhattenDistance::manhattenDistanceFunction);

}
