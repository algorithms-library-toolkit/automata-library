// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#include "EuclideanDistance.hpp"

#include <common/DefaultTypes.hpp>
#include <registration/AlgoRegistration.hpp>

namespace {

auto EuclideanDistance = registration::AbstractRegister<graph::heuristic::EuclideanDistance,
                                                        std::function<DefaultWeightType(const ext::pair<
                                                                                            DefaultCoordinateType,
                                                                                            DefaultCoordinateType>&,
                                                                                        const ext::pair<
                                                                                            DefaultCoordinateType,
                                                                                            DefaultCoordinateType>&)>>(
    graph::heuristic::EuclideanDistance::euclideanDistanceFunction);

}
