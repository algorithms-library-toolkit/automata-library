// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#include "EuclideanDistanceNoSQRT.hpp"

#include <common/DefaultTypes.hpp>
#include <registration/AlgoRegistration.hpp>

namespace {

auto EuclideanDistanceNoSQRT = registration::AbstractRegister<graph::heuristic::EuclideanDistanceNoSQRT,
                                                              std::function<DefaultWeightType(const ext::pair<
                                                                                                  DefaultCoordinateType,
                                                                                                  DefaultCoordinateType>&,
                                                                                              const ext::pair<
                                                                                                  DefaultCoordinateType,
                                                                                                  DefaultCoordinateType>&)>>(
    graph::heuristic::EuclideanDistanceNoSQRT::euclideanDistanceNoSQRTFunction);

}
