// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#include <registration/AlgoRegistration.hpp>
#include "FixedUndirectedGraph.hpp"

graph::UndirectedGraph<int, ext::pair<int, int>> graph::generate::FixedUndirectedGraph::undirected()
{
    graph::UndirectedGraph<int, ext::pair<int, int>> graph;

    graph.addNode(1);
    graph.addNode(2);
    graph.addNode(3);
    graph.addNode(4);
    graph.addNode(5);
    graph.addNode(6);

    graph.addEdge(ext::make_pair(1, 2));
    graph.addEdge(2, 3);
    graph.addEdge(3, 4);
    graph.addEdge(5, 1);

    return graph;
}

// ---------------------------------------------------------------------------------------------------------------------

namespace {

auto FixedUndirectedGraph = registration::AbstractRegister<graph::generate::FixedUndirectedGraph, graph::UndirectedGraph<int, ext::pair<int, int>>>(
    graph::generate::FixedUndirectedGraph::undirected);

}

// ---------------------------------------------------------------------------------------------------------------------
