// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#pragma once

#include <graph/GraphClasses.hpp>

namespace graph {

namespace generate {

class FixedUndirectedGraph {
    // ---------------------------------------------------------------------------------------------------------------------

public:
    static graph::UndirectedGraph<int, ext::pair<int, int>> undirected();

    // ---------------------------------------------------------------------------------------------------------------------
};

// =====================================================================================================================


// ---------------------------------------------------------------------------------------------------------------------

} // namespace generate

} // namespace graph
