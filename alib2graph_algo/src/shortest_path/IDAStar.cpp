// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#include "IDAStar.hpp"

#include <registration/AlgoRegistration.hpp>

namespace {

// ---------------------------------------------------------------------------------------------------------------------

auto IDAStar1 = registration::AbstractRegister<graph::shortest_path::IDAStar,
                                               ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                               const graph::WeightedUndirectedGraph<>&,
                                               const DefaultNodeType&,
                                               const DefaultNodeType&,
                                               std::function<DefaultWeightType(const DefaultNodeType&,
                                                                               const DefaultNodeType&)>>(
    graph::shortest_path::IDAStar::findPathRegistration);

auto IDAStar2 = registration::AbstractRegister<graph::shortest_path::IDAStar,
                                               ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                               const graph::WeightedUndirectedMultiGraph<>&,
                                               const DefaultNodeType&,
                                               const DefaultNodeType&,
                                               std::function<DefaultWeightType(const DefaultNodeType&,
                                                                               const DefaultNodeType&)>>(
    graph::shortest_path::IDAStar::findPathRegistration);

auto IDAStar3 = registration::AbstractRegister<graph::shortest_path::IDAStar,
                                               ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                               const graph::WeightedDirectedGraph<>&,
                                               const DefaultNodeType&,
                                               const DefaultNodeType&,
                                               std::function<DefaultWeightType(const DefaultNodeType&,
                                                                               const DefaultNodeType&)>>(
    graph::shortest_path::IDAStar::findPathRegistration);

auto IDAStar4 = registration::AbstractRegister<graph::shortest_path::IDAStar,
                                               ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                               const graph::WeightedDirectedMultiGraph<>&,
                                               const DefaultNodeType&,
                                               const DefaultNodeType&,
                                               std::function<DefaultWeightType(const DefaultNodeType&,
                                                                               const DefaultNodeType&)>>(
    graph::shortest_path::IDAStar::findPathRegistration);

auto IDAStar5 = registration::AbstractRegister<graph::shortest_path::IDAStar,
                                               ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                               const graph::WeightedMixedGraph<>&,
                                               const DefaultNodeType&,
                                               const DefaultNodeType&,
                                               std::function<DefaultWeightType(const DefaultNodeType&,
                                                                               const DefaultNodeType&)>>(
    graph::shortest_path::IDAStar::findPathRegistration);

auto IDAStar6 = registration::AbstractRegister<graph::shortest_path::IDAStar,
                                               ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                               const graph::WeightedMixedMultiGraph<>&,
                                               const DefaultNodeType&,
                                               const DefaultNodeType&,
                                               std::function<DefaultWeightType(const DefaultNodeType&,
                                                                               const DefaultNodeType&)>>(
    graph::shortest_path::IDAStar::findPathRegistration);

auto IDAStarGrid1 = registration::AbstractRegister<graph::shortest_path::IDAStar,
                                                   ext::pair<ext::vector<DefaultSquareGridNodeType>, DefaultWeightType>,
                                                   const grid::WeightedSquareGrid4<>&,
                                                   const DefaultSquareGridNodeType&,
                                                   const DefaultSquareGridNodeType&,
                                                   std::function<DefaultWeightType(const DefaultSquareGridNodeType&,
                                                                                   const DefaultSquareGridNodeType&)>>(
    graph::shortest_path::IDAStar::findPathRegistration);

auto IDAStarGrid2 = registration::AbstractRegister<graph::shortest_path::IDAStar,
                                                   ext::pair<ext::vector<DefaultSquareGridNodeType>, DefaultWeightType>,
                                                   const grid::WeightedSquareGrid8<>&,
                                                   const DefaultSquareGridNodeType&,
                                                   const DefaultSquareGridNodeType&,
                                                   std::function<DefaultWeightType(const DefaultSquareGridNodeType&,
                                                                                   const DefaultSquareGridNodeType&)>>(
    graph::shortest_path::IDAStar::findPathRegistration);

// ---------------------------------------------------------------------------------------------------------------------

}
