// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#include "MM.hpp"

#include <registration/AlgoRegistration.hpp>

namespace {

// ---------------------------------------------------------------------------------------------------------------------

auto MM1 = registration::AbstractRegister<graph::shortest_path::MM,
                                          ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                          const graph::WeightedUndirectedGraph<>&,
                                          const DefaultNodeType&,
                                          const DefaultNodeType&,
                                          std::function<DefaultWeightType(const DefaultNodeType&,
                                                                          const DefaultNodeType&)>>(
    graph::shortest_path::MM::findPathBidirectionalRegistration);

auto MM2 = registration::AbstractRegister<graph::shortest_path::MM,
                                          ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                          const graph::WeightedUndirectedMultiGraph<>&,
                                          const DefaultNodeType&,
                                          const DefaultNodeType&,
                                          std::function<DefaultWeightType(const DefaultNodeType&,
                                                                          const DefaultNodeType&)>>(
    graph::shortest_path::MM::findPathBidirectionalRegistration);

auto MM3 = registration::AbstractRegister<graph::shortest_path::MM,
                                          ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                          const graph::WeightedDirectedGraph<>&,
                                          const DefaultNodeType&,
                                          const DefaultNodeType&,
                                          std::function<DefaultWeightType(const DefaultNodeType&,
                                                                          const DefaultNodeType&)>>(
    graph::shortest_path::MM::findPathBidirectionalRegistration);

auto MM4 = registration::AbstractRegister<graph::shortest_path::MM,
                                          ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                          const graph::WeightedDirectedMultiGraph<>&,
                                          const DefaultNodeType&,
                                          const DefaultNodeType&,
                                          std::function<DefaultWeightType(const DefaultNodeType&,
                                                                          const DefaultNodeType&)>>(
    graph::shortest_path::MM::findPathBidirectionalRegistration);

auto MM5 = registration::AbstractRegister<graph::shortest_path::MM,
                                          ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                          const graph::WeightedMixedGraph<>&,
                                          const DefaultNodeType&,
                                          const DefaultNodeType&,
                                          std::function<DefaultWeightType(const DefaultNodeType&,
                                                                          const DefaultNodeType&)>>(
    graph::shortest_path::MM::findPathBidirectionalRegistration);

auto MM6 = registration::AbstractRegister<graph::shortest_path::MM,
                                          ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                          const graph::WeightedMixedMultiGraph<>&,
                                          const DefaultNodeType&,
                                          const DefaultNodeType&,
                                          std::function<DefaultWeightType(const DefaultNodeType&,
                                                                          const DefaultNodeType&)>>(
    graph::shortest_path::MM::findPathBidirectionalRegistration);

auto MMGrid1 = registration::AbstractRegister<graph::shortest_path::MM,
                                              ext::pair<ext::vector<DefaultSquareGridNodeType>, DefaultWeightType>,
                                              const grid::WeightedSquareGrid4<>&,
                                              const DefaultSquareGridNodeType&,
                                              const DefaultSquareGridNodeType&,
                                              std::function<DefaultWeightType(const DefaultSquareGridNodeType&,
                                                                              const DefaultSquareGridNodeType&)>>(
    graph::shortest_path::MM::findPathBidirectionalRegistration);

auto MMGrid2 = registration::AbstractRegister<graph::shortest_path::MM,
                                              ext::pair<ext::vector<DefaultSquareGridNodeType>, DefaultWeightType>,
                                              const grid::WeightedSquareGrid8<>&,
                                              const DefaultSquareGridNodeType&,
                                              const DefaultSquareGridNodeType&,
                                              std::function<DefaultWeightType(const DefaultSquareGridNodeType&,
                                                                              const DefaultSquareGridNodeType&)>>(
    graph::shortest_path::MM::findPathBidirectionalRegistration);

// ---------------------------------------------------------------------------------------------------------------------

}
