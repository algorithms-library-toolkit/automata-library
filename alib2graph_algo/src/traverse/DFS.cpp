// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#include "DFS.hpp"

#include <graph/GraphClasses.hpp>
#include <grid/GridClasses.hpp>
#include <registration/AlgoRegistration.hpp>

namespace {

// ---------------------------------------------------------------------------------------------------------------------
// Normal Graph

auto DFS1 = registration::AbstractRegister<graph::traverse::DFS,
                                           ext::vector<DefaultNodeType>,
                                           const graph::UndirectedGraph<>&,
                                           const DefaultNodeType&,
                                           const DefaultNodeType&>(graph::traverse::DFS::findPathRegistration);

auto DFS2 = registration::AbstractRegister<graph::traverse::DFS,
                                           ext::vector<DefaultNodeType>,
                                           const graph::UndirectedMultiGraph<>&,
                                           const DefaultNodeType&,
                                           const DefaultNodeType&>(graph::traverse::DFS::findPathRegistration);

auto DFS3 = registration::AbstractRegister<graph::traverse::DFS,
                                           ext::vector<DefaultNodeType>,
                                           const graph::DirectedGraph<>&,
                                           const DefaultNodeType&,
                                           const DefaultNodeType&>(graph::traverse::DFS::findPathRegistration);

auto DFS4 = registration::AbstractRegister<graph::traverse::DFS,
                                           ext::vector<DefaultNodeType>,
                                           const graph::DirectedMultiGraph<>&,
                                           const DefaultNodeType&,
                                           const DefaultNodeType&>(graph::traverse::DFS::findPathRegistration);

auto DFS5 = registration::AbstractRegister<graph::traverse::DFS,
                                           ext::vector<DefaultNodeType>,
                                           const graph::MixedGraph<>&,
                                           const DefaultNodeType&,
                                           const DefaultNodeType&>(graph::traverse::DFS::findPathRegistration);

auto DFS6 = registration::AbstractRegister<graph::traverse::DFS,
                                           ext::vector<DefaultNodeType>,
                                           const graph::MixedMultiGraph<>&,
                                           const DefaultNodeType&,
                                           const DefaultNodeType&>(graph::traverse::DFS::findPathRegistration);

auto DFSGrid1 = registration::AbstractRegister<graph::traverse::DFS,
                                               ext::vector<DefaultSquareGridNodeType>,
                                               const grid::SquareGrid4<>&,
                                               const DefaultSquareGridNodeType&,
                                               const DefaultSquareGridNodeType&>(graph::traverse::DFS::findPathRegistration);

auto DFSGrid2 = registration::AbstractRegister<graph::traverse::DFS,
                                               ext::vector<DefaultSquareGridNodeType>,
                                               const grid::SquareGrid8<>&,
                                               const DefaultSquareGridNodeType&,
                                               const DefaultSquareGridNodeType&>(graph::traverse::DFS::findPathRegistration);

// ---------------------------------------------------------------------------------------------------------------------
// Weighted Graph

auto WDFS1 = registration::AbstractRegister<graph::traverse::DFS,
                                            ext::vector<DefaultNodeType>,
                                            const graph::WeightedUndirectedGraph<>&,
                                            const DefaultNodeType&,
                                            const DefaultNodeType&>(graph::traverse::DFS::findPathRegistration);

auto WDFS2 = registration::AbstractRegister<graph::traverse::DFS,
                                            ext::vector<DefaultNodeType>,
                                            const graph::WeightedUndirectedMultiGraph<>&,
                                            const DefaultNodeType&,
                                            const DefaultNodeType&>(graph::traverse::DFS::findPathRegistration);

auto WDFS3 = registration::AbstractRegister<graph::traverse::DFS,
                                            ext::vector<DefaultNodeType>,
                                            const graph::WeightedDirectedGraph<>&,
                                            const DefaultNodeType&,
                                            const DefaultNodeType&>(graph::traverse::DFS::findPathRegistration);

auto WDFS4 = registration::AbstractRegister<graph::traverse::DFS,
                                            ext::vector<DefaultNodeType>,
                                            const graph::WeightedDirectedMultiGraph<>&,
                                            const DefaultNodeType&,
                                            const DefaultNodeType&>(graph::traverse::DFS::findPathRegistration);

auto WDFS5 = registration::AbstractRegister<graph::traverse::DFS,
                                            ext::vector<DefaultNodeType>,
                                            const graph::WeightedMixedGraph<>&,
                                            const DefaultNodeType&,
                                            const DefaultNodeType&>(graph::traverse::DFS::findPathRegistration);

auto WDFS6 = registration::AbstractRegister<graph::traverse::DFS,
                                            ext::vector<DefaultNodeType>,
                                            const graph::WeightedMixedMultiGraph<>&,
                                            const DefaultNodeType&,
                                            const DefaultNodeType&>(graph::traverse::DFS::findPathRegistration);

auto WDFSGrid1 = registration::AbstractRegister<graph::traverse::DFS,
                                                ext::vector<DefaultSquareGridNodeType>,
                                                const grid::WeightedSquareGrid4<>&,
                                                const DefaultSquareGridNodeType&,
                                                const DefaultSquareGridNodeType&>(graph::traverse::DFS::findPathRegistration);

auto WDFSGrid2 = registration::AbstractRegister<graph::traverse::DFS,
                                                ext::vector<DefaultSquareGridNodeType>,
                                                const grid::WeightedSquareGrid8<>&,
                                                const DefaultSquareGridNodeType&,
                                                const DefaultSquareGridNodeType&>(graph::traverse::DFS::findPathRegistration);

// ---------------------------------------------------------------------------------------------------------------------

}
