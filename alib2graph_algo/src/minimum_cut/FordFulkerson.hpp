// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#pragma once

#include <alib/unordered_set>

#include <edge/EdgeClasses.hpp>
#include <graph/GraphClasses.hpp>
#include <node/NodeClasses.hpp>

namespace std {
template <>
struct hash<ext::pair<node::Node, node::Node>> {
    std::size_t operator()(const ext::pair<node::Node, node::Node>& p) const
    {
        return std::hash<std::string>()(ext::to_string(p.first) + ext::to_string(p.second));
    }
};
}

// =====================================================================================================================

namespace graph {

namespace minimum_cut {

typedef ext::unordered_set<ext::pair<node::Node, node::Node>> Cut;

// Old implementation without templates
using UndirectedGraph = graph::UndirectedGraph<node::Node, edge::CapacityEdge<node::Node, int>>;
using DirectedGraph = graph::DirectedGraph<node::Node, edge::CapacityEdge<node::Node, int>>;

class FordFulkerson {
public:
    // ---------------------------------------------------------------------------------------------------------------------

    static Cut findMinimumCut(const DirectedGraph& graph,
                              const node::Node& source,
                              const node::Node& sink);

    static Cut findMinimumCut(const UndirectedGraph& graph,
                              const node::Node& source,
                              const node::Node& sink);

    // ---------------------------------------------------------------------------------------------------------------------
};

// =====================================================================================================================


// ---------------------------------------------------------------------------------------------------------------------

} // namespace minimum_cut

} // namespace graph
