#include <catch2/catch.hpp>

#include <ext/iostream>

#include <alib/string>

#include "sax/SaxComposeInterface.h"
#include "sax/SaxParseInterface.h"

TEST_CASE("Sax", "[unit][sax][xml]")
{
    const std::string tmp = "<?xml version=\"1.0\"?>\n<aa bb=\"cc\"><xx/>dd<ee ff=\"gg\"/></aa>\n";

    ext::deque<sax::Token> tokens;
    sax::SaxParseInterface::parseMemory(tmp, tokens);

    CAPTURE(tokens);

    std::string tmp2;
    sax::SaxComposeInterface::composeMemory(tmp2, tokens);

    REQUIRE(tmp == tmp2);

    ext::deque<sax::Token> tokens2;
    sax::SaxParseInterface::parseMemory(tmp2, tokens2);

    CHECK(tokens == tokens2);
}
