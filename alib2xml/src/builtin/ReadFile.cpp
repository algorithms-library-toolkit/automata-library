#include <factory/XmlDataFactory.hpp>
#include <registration/AlgoRegistration.hpp>
#include "ReadFile.h"

namespace xml::builtin {

object::Object ReadFile::read(const std::string& filename)
{
    return factory::XmlDataFactory::fromFile(filename).operator object::Object();
}

auto ReadFileString = registration::AbstractRegister<ReadFile, object::Object, const std::string&>(ReadFile::read, "filename").setDocumentation("Reads the content of a file into a string.\n\
\n\
@param filename the name of read file\n\
@return the content of the file");

} /* namespace xml::builtin */
