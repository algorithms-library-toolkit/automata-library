#pragma once

#include <alib/string>
#include <object/Object.h>

namespace xml {

namespace builtin {

/**
 * File reader command.
 */
class ReadFile {
public:
    /**
     * Reads the content of a file into a string.
     *
     * \param filename the name of read file
     *
     * \return the content of the file
     */
    static object::Object read(const std::string& filename);
};

} /* namespace builtin */

} /* namespace xml */
