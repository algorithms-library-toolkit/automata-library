#pragma once

#include <alib/deque>
#include <core/xmlApi.hpp>

namespace core {

template <typename T>
struct xmlApi<ext::deque<T>> {
    static ext::deque<T> parse(ext::deque<sax::Token>::iterator& input);
    static bool first(const ext::deque<sax::Token>::const_iterator& input);
    static std::string xmlTagName();
    static void compose(ext::deque<sax::Token>& output, const ext::deque<T>& input);
};

template <typename T>
ext::deque<T> xmlApi<ext::deque<T>>::parse(ext::deque<sax::Token>::iterator& input)
{
    sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());

    ext::deque<T> set;

    while (sax::FromXMLParserHelper::isTokenType(input, sax::Token::TokenType::START_ELEMENT))
        set.push_back(core::xmlApi<T>::parse(input));

    sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::END_ELEMENT, xmlTagName());
    return set;
}

template <typename T>
bool xmlApi<ext::deque<T>>::first(const ext::deque<sax::Token>::const_iterator& input)
{
    return sax::FromXMLParserHelper::isToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());
}

template <typename T>
std::string xmlApi<ext::deque<T>>::xmlTagName()
{
    return "Deque";
}

template <typename T>
void xmlApi<ext::deque<T>>::compose(ext::deque<sax::Token>& output, const ext::deque<T>& input)
{
    output.emplace_back(xmlTagName(), sax::Token::TokenType::START_ELEMENT);

    for (const T& item : input)
        core::xmlApi<T>::compose(output, item);

    output.emplace_back(xmlTagName(), sax::Token::TokenType::END_ELEMENT);
}

} /* namespace core */
