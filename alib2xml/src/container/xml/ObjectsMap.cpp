#include "ObjectsMap.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<ext::map<object::Object, object::Object>>();
auto xmlRead = registration::XmlReaderRegister<ext::map<object::Object, object::Object>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, ext::map<object::Object, object::Object>>();

} /* namespace */
