#pragma once

#include <alib/bitset>
#include <core/xmlApi.hpp>

#include <primitive/xml/Bool.h>

namespace core {

template <size_t N>
struct xmlApi<ext::bitset<N>> {
    static ext::bitset<N> parse(ext::deque<sax::Token>::iterator& input);
    static bool first(const ext::deque<sax::Token>::const_iterator& input);
    static std::string xmlTagName();
    static void compose(ext::deque<sax::Token>& output, const ext::bitset<N>& input);
};

template <size_t N>
ext::bitset<N> xmlApi<ext::bitset<N>>::parse(ext::deque<sax::Token>::iterator& input)
{
    sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());

    ext::bitset<N> res;

    for (size_t i = 0; i < N; ++i)
        res[i] = core::xmlApi<bool>::parse(input);

    sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::END_ELEMENT, xmlTagName());

    return res;
}

template <size_t N>
bool xmlApi<ext::bitset<N>>::first(const ext::deque<sax::Token>::const_iterator& input)
{
    return sax::FromXMLParserHelper::isToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());
}

template <size_t N>
std::string xmlApi<ext::bitset<N>>::xmlTagName()
{
    return "Bitset";
}

template <size_t N>
void xmlApi<ext::bitset<N>>::compose(ext::deque<sax::Token>& output, const ext::bitset<N>& input)
{
    output.emplace_back(xmlTagName(), sax::Token::TokenType::START_ELEMENT);

    for (size_t i = 0; i < N; ++i)
        core::xmlApi<bool>::compose(output, input[i]);

    output.emplace_back(xmlTagName(), sax::Token::TokenType::END_ELEMENT);
}

} /* namespace core */
