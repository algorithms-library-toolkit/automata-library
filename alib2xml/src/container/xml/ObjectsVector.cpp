#include "ObjectsVector.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<ext::vector<object::Object>>();
auto xmlRead = registration::XmlReaderRegister<ext::vector<object::Object>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, ext::vector<object::Object>>();

} /* namespace */
