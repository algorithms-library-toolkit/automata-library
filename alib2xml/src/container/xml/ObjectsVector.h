#pragma once

#include <alib/vector>
#include <core/xmlApi.hpp>

namespace core {

template <typename T>
struct xmlApi<ext::vector<T>> {
    static ext::vector<T> parse(ext::deque<sax::Token>::iterator& input);
    static bool first(const ext::deque<sax::Token>::const_iterator& input);
    static std::string xmlTagName();
    static void compose(ext::deque<sax::Token>& output, const ext::vector<T>& input);
};

template <typename T>
ext::vector<T> xmlApi<ext::vector<T>>::parse(ext::deque<sax::Token>::iterator& input)
{
    sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());

    ext::vector<T> vector;

    while (sax::FromXMLParserHelper::isTokenType(input, sax::Token::TokenType::START_ELEMENT))
        vector.push_back(core::xmlApi<T>::parse(input));

    sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::END_ELEMENT, xmlTagName());
    return vector;
}

template <typename T>
bool xmlApi<ext::vector<T>>::first(const ext::deque<sax::Token>::const_iterator& input)
{
    return sax::FromXMLParserHelper::isToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());
}

template <typename T>
std::string xmlApi<ext::vector<T>>::xmlTagName()
{
    return "Vector";
}

template <typename T>
void xmlApi<ext::vector<T>>::compose(ext::deque<sax::Token>& output, const ext::vector<T>& input)
{
    output.emplace_back(xmlTagName(), sax::Token::TokenType::START_ELEMENT);

    for (const T& item : input)
        core::xmlApi<T>::compose(output, item);

    output.emplace_back(xmlTagName(), sax::Token::TokenType::END_ELEMENT);
}

} /* namespace core */
