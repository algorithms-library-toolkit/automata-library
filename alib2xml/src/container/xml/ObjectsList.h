#pragma once

#include <alib/list>
#include <core/xmlApi.hpp>

namespace core {

template <typename T>
struct xmlApi<ext::list<T>> {
    static ext::list<T> parse(ext::deque<sax::Token>::iterator& input);
    static bool first(const ext::deque<sax::Token>::const_iterator& input);
    static std::string xmlTagName();
    static void compose(ext::deque<sax::Token>& output, const ext::list<T>& input);
};

template <typename T>
ext::list<T> xmlApi<ext::list<T>>::parse(ext::deque<sax::Token>::iterator& input)
{
    sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());

    ext::list<T> set;

    while (sax::FromXMLParserHelper::isTokenType(input, sax::Token::TokenType::START_ELEMENT))
        set.push_back(core::xmlApi<T>::parse(input));

    sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::END_ELEMENT, xmlTagName());
    return set;
}

template <typename T>
bool xmlApi<ext::list<T>>::first(const ext::deque<sax::Token>::const_iterator& input)
{
    return sax::FromXMLParserHelper::isToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());
}

template <typename T>
std::string xmlApi<ext::list<T>>::xmlTagName()
{
    return "List";
}

template <typename T>
void xmlApi<ext::list<T>>::compose(ext::deque<sax::Token>& output, const ext::list<T>& input)
{
    output.emplace_back(xmlTagName(), sax::Token::TokenType::START_ELEMENT);

    for (const T& item : input)
        core::xmlApi<T>::compose(output, item);

    output.emplace_back(xmlTagName(), sax::Token::TokenType::END_ELEMENT);
}

} /* namespace core */
