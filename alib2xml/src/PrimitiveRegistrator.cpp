#include <registration/XmlRegistration.hpp>
#include <registry/ContainerRegistry.hpp>

#include <primitive/xml/Bool.h>
#include <primitive/xml/Character.h>
#include <primitive/xml/Double.h>
#include <primitive/xml/Integer.h>
#include <primitive/xml/String.h>
#include <primitive/xml/Unsigned.h>
#include <primitive/xml/UnsignedLong.h>

#include <container/xml/ObjectsMap.h>
#include <container/xml/ObjectsPair.h>
#include <container/xml/ObjectsSet.h>
#include <container/xml/ObjectsVariant.h>
#include <container/xml/ObjectsVector.h>

namespace {

class PrimitiveRegistrator {
    registration::XmlWriterRegister<object::Object> member8;

public:
    PrimitiveRegistrator()
    {
        abstraction::XmlParserRegistry::registerXmlParser<object::Object>("Object");
    }

    ~PrimitiveRegistrator()
    {
        abstraction::XmlParserRegistry::unregisterXmlParser("Object");
    }
};

auto primitiveRegistrator = PrimitiveRegistrator();

} /* namespace */
