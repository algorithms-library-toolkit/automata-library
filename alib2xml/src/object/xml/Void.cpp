#include <object/Object.h>
#include "Void.h"

#include <registration/XmlRegistration.hpp>

namespace core {

void xmlApi<void>::parse(ext::deque<sax::Token>::iterator& input)
{
    sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());
    sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::END_ELEMENT, xmlTagName());
}

bool xmlApi<void>::first(const ext::deque<sax::Token>::const_iterator& input)
{
    return sax::FromXMLParserHelper::isToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());
}

std::string xmlApi<void>::xmlTagName()
{
    return "Void";
}

void xmlApi<void>::compose(ext::deque<sax::Token>& output)
{
    output.emplace_back(xmlTagName(), sax::Token::TokenType::START_ELEMENT);
    output.emplace_back(xmlTagName(), sax::Token::TokenType::END_ELEMENT);
}

object::Void xmlApi<object::Void>::parse(ext::deque<sax::Token>::iterator& input)
{
    xmlApi<void>::parse(input);
    return object::Void();
}

bool xmlApi<object::Void>::first(const ext::deque<sax::Token>::const_iterator& input)
{
    return sax::FromXMLParserHelper::isToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());
}

std::string xmlApi<object::Void>::xmlTagName()
{
    return xmlApi<void>::xmlTagName();
}

void xmlApi<object::Void>::compose(ext::deque<sax::Token>& output, const object::Void&)
{
    xmlApi<void>::compose(output);
}

} /* namespace core */

namespace {

auto xmlWrite = registration::XmlWriterRegister<object::Void>();
auto xmlRead = registration::XmlReaderRegister<object::Void>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, object::Void>();

} /* namespace */
