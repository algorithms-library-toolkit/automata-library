#pragma once

#include <core/xmlApi.hpp>
#include <object/Void.h>

namespace core {

template <>
struct xmlApi<void> {
    static void parse(ext::deque<sax::Token>::iterator& input);
    static bool first(const ext::deque<sax::Token>::const_iterator& input);
    static std::string xmlTagName();
    static void compose(ext::deque<sax::Token>& output);
};

template <>
struct xmlApi<object::Void> {
    static object::Void parse(ext::deque<sax::Token>::iterator& input);
    static bool first(const ext::deque<sax::Token>::const_iterator& input);
    static std::string xmlTagName();
    static void compose(ext::deque<sax::Token>& output, const object::Void& data);
};

} /* namespace core */
