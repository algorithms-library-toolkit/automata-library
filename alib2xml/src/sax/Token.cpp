#include <ext/iostream>
#include <ext/typeinfo>
#include "Token.h"

#include <object/ObjectFactory.h>

namespace sax {

Token::Token(std::string tokenData, Token::TokenType tokenType)
    : data(std::move(tokenData))
    , type(tokenType)
{
}

const std::string& Token::getData() const&
{
    return data;
}

std::string&& Token::getData() &&
{
    return std::move(data);
}

Token::TokenType Token::getType() const
{
    return type;
}

bool Token::operator==(const Token& other) const
{
    return data == other.data && type == other.type;
}

std::ostream& operator<<(std::ostream& os, const Token& token)
{
    os << "(Token ";
    switch (token.type) {
    case Token::TokenType::START_ELEMENT:
        os << "START_ELEMENT ";
        break;
    case Token::TokenType::END_ELEMENT:
        os << "END_ELEMENT ";
        break;
    case Token::TokenType::START_ATTRIBUTE:
        os << "START_ATTRIBUTE ";
        break;
    case Token::TokenType::END_ATTRIBUTE:
        os << "END_ATTRIBUTE ";
        break;
    case Token::TokenType::CHARACTER:
        os << "CHARACTER ";
        break;
    }
    os << token.data;
    os << ")";
    return os;
}

} /* namespace sax */

namespace core {

sax::Token type_util<sax::Token>::denormalize(sax::Token&& arg)
{
    return std::move(arg);
}

sax::Token type_util<sax::Token>::normalize(sax::Token&& arg)
{
    return std::move(arg);
}

std::unique_ptr<type_details_base> type_util<sax::Token>::type(const sax::Token&)
{
    return std::make_unique<type_details_type>("sax::Token");
}

std::unique_ptr<type_details_base> type_details_retriever<sax::Token>::get()
{
    return std::make_unique<type_details_type>("sax::Token");
}

} /* namespace core */
