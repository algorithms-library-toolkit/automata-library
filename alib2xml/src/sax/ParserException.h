#pragma once

#include <exception/CommonException.h>
#include "Token.h"

namespace sax {

/**
 * Exception thrown by XML parser when is expected different tag than the one which is read.
 */
class ParserException : public exception::CommonException {
    Token m_expected;
    Token m_read;

public:
    ParserException(const Token& expected, const Token& read);
};

} /* namespace sax */
