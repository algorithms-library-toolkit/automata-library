#include <object/Object.h>
#include "Character.h"

#include <registration/XmlRegistration.hpp>

namespace core {

char xmlApi<char>::parse(ext::deque<sax::Token>::iterator& input)
{
    sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());
    int data = ext::from_string<int>(sax::FromXMLParserHelper::popTokenData(input, sax::Token::TokenType::CHARACTER));
    sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::END_ELEMENT, xmlTagName());
    return static_cast<char>(data);
}

bool xmlApi<char>::first(const ext::deque<sax::Token>::const_iterator& input)
{
    return sax::FromXMLParserHelper::isToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());
}

std::string xmlApi<char>::xmlTagName()
{
    return "Character";
}

void xmlApi<char>::compose(ext::deque<sax::Token>& output, char data)
{
    output.emplace_back(xmlTagName(), sax::Token::TokenType::START_ELEMENT);
    output.emplace_back(ext::to_string(static_cast<int>(data)), sax::Token::TokenType::CHARACTER);
    output.emplace_back(xmlTagName(), sax::Token::TokenType::END_ELEMENT);
}

} /* namespace core */

namespace {

auto xmlWrite = registration::XmlWriterRegister<char>();
auto xmlRead = registration::XmlReaderRegister<char>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, char>();

} /* namespace */
