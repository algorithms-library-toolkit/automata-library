#include <object/Object.h>
#include "Double.h"

#include <registration/XmlRegistration.hpp>

namespace core {

double xmlApi<double>::parse(ext::deque<sax::Token>::iterator& input)
{
    sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());
    double data = ext::from_string<double>(sax::FromXMLParserHelper::popTokenData(input, sax::Token::TokenType::CHARACTER));
    sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::END_ELEMENT, xmlTagName());
    return data;
}

bool xmlApi<double>::first(const ext::deque<sax::Token>::const_iterator& input)
{
    return sax::FromXMLParserHelper::isToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());
}

std::string xmlApi<double>::xmlTagName()
{
    return "Double";
}

void xmlApi<double>::compose(ext::deque<sax::Token>& output, double data)
{
    output.emplace_back(xmlTagName(), sax::Token::TokenType::START_ELEMENT);
    output.emplace_back(ext::to_string(data), sax::Token::TokenType::CHARACTER);
    output.emplace_back(xmlTagName(), sax::Token::TokenType::END_ELEMENT);
}

} /* namespace core */

namespace {

auto xmlWrite = registration::XmlWriterRegister<double>();
auto xmlRead = registration::XmlReaderRegister<double>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, double>();

} /* namespace */
