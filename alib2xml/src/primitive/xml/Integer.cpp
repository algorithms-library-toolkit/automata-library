#include <object/Object.h>
#include "Integer.h"

#include <registration/XmlRegistration.hpp>

namespace core {

int xmlApi<int>::parse(ext::deque<sax::Token>::iterator& input)
{
    sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());
    int data = ext::from_string<int>(sax::FromXMLParserHelper::popTokenData(input, sax::Token::TokenType::CHARACTER));
    sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::END_ELEMENT, xmlTagName());
    return data;
}

bool xmlApi<int>::first(const ext::deque<sax::Token>::const_iterator& input)
{
    return sax::FromXMLParserHelper::isToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());
}

std::string xmlApi<int>::xmlTagName()
{
    return "Integer";
}

void xmlApi<int>::compose(ext::deque<sax::Token>& output, int data)
{
    output.emplace_back(xmlTagName(), sax::Token::TokenType::START_ELEMENT);
    output.emplace_back(ext::to_string(data), sax::Token::TokenType::CHARACTER);
    output.emplace_back(xmlTagName(), sax::Token::TokenType::END_ELEMENT);
}

} /* namespace core */

namespace {

auto xmlWrite = registration::XmlWriterRegister<int>();
auto xmlRead = registration::XmlReaderRegister<int>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, int>();

} /* namespace */
