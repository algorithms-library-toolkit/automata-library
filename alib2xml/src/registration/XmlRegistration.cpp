#include <registration/AlgoRegistration.hpp>
#include "XmlRegistration.hpp"

namespace {

auto xmlParse = registration::WrapperRegister<xml::Parse, abstraction::UnspecifiedType, ext::deque<sax::Token>&&>(xml::Parse::abstractionFromTokens, "arg0").setDocumentation("Xml parsing of datatype.\n\
\n\
@param arg0 the parsed sequence of xml tokens\n\
@return value parsed from @p arg0");

}
