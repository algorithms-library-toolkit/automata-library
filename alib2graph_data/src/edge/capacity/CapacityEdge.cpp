// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#include <registration/ValuePrinterRegistration.hpp>
#include "CapacityEdge.hpp"

namespace {

auto valuePrinter = registration::ValuePrinterRegister<edge::CapacityEdge<>>();

}
