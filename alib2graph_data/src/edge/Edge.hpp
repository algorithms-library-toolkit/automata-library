// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#pragma once

#include <alib/pair>
#include <alib/tuple>
#include <object/Object.h>
#include <sstream>

#include "EdgeBase.hpp"
#include "EdgeFeatures.hpp"

#include <core/type_details_base.hpp>

namespace edge {

template <typename TNode>
class Edge : public ext::pair<TNode, TNode>, public EdgeBase {
    // ---------------------------------------------------------------------------------------------------------------------
public:
    using node_type = TNode;
    using normalized_type = Edge<>;

    // ---------------------------------------------------------------------------------------------------------------------
    // =====================================================================================================================
    // Constructor, Destructor, Operators

    explicit Edge(TNode _first, TNode _second);

    // =====================================================================================================================
    // EdgeBase interface

    auto operator<=>(const Edge& other) const
    {
        return ext::tie(this->first, this->second) <=> ext::tie(other.first, other.second);
    }

    bool operator==(const Edge& other) const
    {
        return ext::tie(this->first, this->second) == ext::tie(other.first, other.second);
    }

    void operator>>(ext::ostream& ostream) const override;

    // =====================================================================================================================

    virtual std::string name() const;

    // ---------------------------------------------------------------------------------------------------------------------
};
// =====================================================================================================================

template <typename TNode>
Edge<TNode>::Edge(TNode _first, TNode _second)
    : ext::pair<TNode, TNode>(_first, _second)
{
}

// ---------------------------------------------------------------------------------------------------------------------

template <typename TNode>
std::string Edge<TNode>::name() const
{
    return "Edge";
}

// ---------------------------------------------------------------------------------------------------------------------

template <typename TNode>
void Edge<TNode>::operator>>(ext::ostream& ostream) const
{
    ostream << "(" << name() << "(first=" << this->first << ", second=" << this->second << "))";
}

// ---------------------------------------------------------------------------------------------------------------------

} // namespace edge

// =====================================================================================================================

namespace core {

template <class TNode>
struct type_details_retriever<edge::Edge<TNode>> {
    static std::unique_ptr<type_details_base> get()
    {
        std::vector<std::unique_ptr<type_details_base>> sub_types_vec;
        sub_types_vec.push_back(type_details_retriever<TNode>::get());
        return std::make_unique<type_details_template>("edge::Edge", std::move(sub_types_vec));
    }
};

} /* namespace core */
