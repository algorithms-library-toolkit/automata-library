// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#pragma once

#include <alib/pair>
#include <alib/set>
#include <alib/vector>
#include <functional>

#include "GridBase.hpp"

namespace grid {

template <typename TCoordinate, typename TEdge>
class GridInterface : public GridBase {
    // ---------------------------------------------------------------------------------------------------------------------
public:
    using coordinate_type = TCoordinate;
    using edge_type = TEdge;
    using node_type = ext::pair<TCoordinate, TCoordinate>;

    // ---------------------------------------------------------------------------------------------------------------------

    virtual size_t nodeCount() const = 0;
    virtual size_t edgeCount() const = 0;

    // ---------------------------------------------------------------------------------------------------------------------

    virtual ext::set<node_type> getNodes() const = 0;
    virtual ext::vector<TEdge> getEdges() const = 0;

    // ---------------------------------------------------------------------------------------------------------------------

    virtual ext::set<node_type> successors(const node_type& n) const = 0;
    virtual ext::vector<TEdge> successorEdges(const node_type& n) const = 0;
    virtual ext::set<node_type> predecessors(const node_type& n) const = 0;
    virtual ext::vector<TEdge> predecessorEdges(const node_type& n) const = 0;

    // ---------------------------------------------------------------------------------------------------------------------

    virtual std::string name() const = 0;

    // ---------------------------------------------------------------------------------------------------------------------
};

// =====================================================================================================================

}
