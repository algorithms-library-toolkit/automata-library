// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#include "GridFunction.hpp"

// ---------------------------------------------------------------------------------------------------------------------

namespace grid {

ext::set<grid::SquareGridDirections>
GridFunction::squareGridDirectionDecompose(grid::SquareGridDirections direction)
{
    using direction_type = grid::SquareGridDirections;

    ext::set<direction_type> decomposition;

    if (direction == direction_type::north_west) {
        decomposition.insert(direction_type::north);
        decomposition.insert(direction_type::west);
    } else if (direction == direction_type::north_east) {
        decomposition.insert(direction_type::north);
        decomposition.insert(direction_type::east);
    } else if (direction == direction_type::south_east) {
        decomposition.insert(direction_type::south);
        decomposition.insert(direction_type::east);
    } else if (direction == direction_type::south_west) {
        decomposition.insert(direction_type::south);
        decomposition.insert(direction_type::west);
    }

    return decomposition;
}

// ---------------------------------------------------------------------------------------------------------------------

bool GridFunction::sqaureGridDirectionIsDiagonal(grid::SquareGridDirections direction)
{
    using direction_type = grid::SquareGridDirections;

    return direction == direction_type::north_west || direction == direction_type::south_west || direction == direction_type::north_east || direction == direction_type::south_east;
}

bool GridFunction::sqaureGridDirectionIsCardinal(grid::SquareGridDirections direction)
{
    using direction_type = grid::SquareGridDirections;

    return direction == direction_type::north || direction == direction_type::south || direction == direction_type::east || direction == direction_type::west;
}

// ---------------------------------------------------------------------------------------------------------------------

} // namespace grid
