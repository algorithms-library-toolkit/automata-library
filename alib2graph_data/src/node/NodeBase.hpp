// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#pragma once

#include <iostream>
#include <string>

namespace node {

/**
 * Represents node in graph.
 */
class NodeBase {
public:
    virtual ~NodeBase() noexcept = default;

    // ---------------------------------------------------------------------------------------------------------------------

    friend ext::ostream& operator<<(ext::ostream& os, const NodeBase& instance)
    {
        instance >> os;
        return os;
    }

    virtual void operator>>(ext::ostream& os) const = 0;
};

} // namespace node
