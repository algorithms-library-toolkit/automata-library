// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#pragma once

#include <ostream>
#include <string>

namespace graph {

/**
 * Represents graph.
 */
class GraphBase {
public:
    virtual ~GraphBase() noexcept = default;

    // ---------------------------------------------------------------------------------------------------------------------

    // ---------------------------------------------------------------------------------------------------------------------
    friend ext::ostream& operator<<(ext::ostream& os, const GraphBase& instance)
    {
        instance >> os;
        return os;
    }

    virtual void operator>>(ext::ostream& os) const = 0;
};

} // namespace graph
