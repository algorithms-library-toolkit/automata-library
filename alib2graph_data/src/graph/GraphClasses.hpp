// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#pragma once

// Graphs
#include "directed/DirectedGraph.hpp"
#include "directed/DirectedMultiGraph.hpp"
#include "mixed/MixedGraph.hpp"
#include "mixed/MixedMultiGraph.hpp"
#include "undirected/UndirectedGraph.hpp"
#include "undirected/UndirectedMultiGraph.hpp"

// Weighted graphs
#include "weighted/WeightedGraphClasses.hpp"
