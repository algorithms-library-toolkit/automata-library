// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#pragma once

#include <alib/pair>
#include "DefaultSquareGridNodeType.hpp"

typedef ext::pair<DefaultSquareGridNodeType, DefaultSquareGridNodeType> DefaultSquareGridEdgeType;
