// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.

#pragma once

#include <common/default_type/DefaultCapacityType.hpp>
#include <common/default_type/DefaultNodeType.hpp>
#include <edge/capacity/CapacityEdge.hpp>

typedef edge::CapacityEdge<DefaultNodeType, DefaultCapacityType> DefaultCapacityEdgeType;
