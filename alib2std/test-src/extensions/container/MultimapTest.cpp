#include <catch2/catch.hpp>
#include <ext/algorithm>
#include <ext/multimap>
#include <ext/variant>

namespace {
class Moveable {
    int& m_moves;
    int& m_copies;

public:
    Moveable(int& moves, int& copies)
        : m_moves(moves)
        , m_copies(copies)
    {
        m_moves = 0;
        m_copies = 0;
    }

    Moveable(const Moveable& src)
        : m_moves(src.m_moves)
        , m_copies(src.m_copies)
    {
        m_copies++;
    }

    Moveable(Moveable&& src)
        : m_moves(src.m_moves)
        , m_copies(src.m_copies)
    {
        m_moves++;
    }

    Moveable& operator=(const Moveable&)
    {
        m_copies++;
        return *this;
    }

    Moveable& operator=(Moveable&&)
    {
        m_moves++;
        return *this;
    }

    bool operator<(const Moveable&) const
    {
        return false;
    }
};
}
TEST_CASE("Multimap", "[unit][std][container]")
{
    SECTION("Basic")
    {
        int moves;
        int copies;

        ext::multimap<Moveable, Moveable> multimap;
        multimap.insert(Moveable(moves, copies), Moveable(moves, copies));
        ext::multimap<Moveable, Moveable> map2;

        for (std::pair<Moveable, Moveable> moveablePair : ext::make_mover(multimap)) {
            map2.insert(std::move(moveablePair));
        }

        CHECK(copies == 0);
    }

    SECTION("Variant as key")
    {
        CHECK(1 < ext::variant<int, ext::pair<int, int>>(ext::make_pair(1, 1)));
        CHECK(ext::variant<int, ext::pair<int, int>>(1) < ext::variant<int, ext::pair<int, int>>(ext::make_pair(1, 1)));

        ext::multimap<ext::variant<int, ext::pair<int, int>>, int> map;
        map.insert(std::make_pair(ext::variant<int, ext::pair<int, int>>(1), 1));
        map.insert(std::make_pair(ext::variant<int, ext::pair<int, int>>(ext::make_pair(1, 1)), 11));

        auto range = map.equal_range(1);
        CHECK(std::distance(range.begin(), range.end()) == 1);
    }
}
