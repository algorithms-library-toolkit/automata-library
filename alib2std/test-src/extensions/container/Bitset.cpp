#include <catch2/catch.hpp>
#include <compare>
#include <ext/bitset>

TEST_CASE("Bitset", "[unit][std][container]")
{
    SECTION("Basic")
    {
        ext::bitset<64> bitset1;
        ext::bitset<64> bitset2;

        bool res = bitset1 <=> bitset2 == 0;
        CHECK(res);
        CHECK(bitset1 == bitset2);
    }
}
