#include <catch2/catch.hpp>
#include <exception>

#include <ext/managed_linear_set>

TEST_CASE("ManagedLinearSet", "[unit][std][container]")
{
    SECTION("ManagedLinearSet")
    {
        ext::managed_linear_set<int> mod2;
        mod2.addInsertCallback([](const int& value) { if ( value % 2 != 0 ) throw value; });
        CHECK_NOTHROW(mod2.insert(12));
        CHECK_THROWS_AS(mod2.insert(13), int);

        ext::managed_linear_set<int> mod6;
        mod6.addInsertCallback([](const int& value) { if ( value % 6 != 0 ) throw value; });
        CHECK_NOTHROW(mod6.insert(6));
        CHECK_THROWS_AS(mod6.insert(4), int);

        CHECK_NOTHROW(std::swap(mod2, mod6));

        mod2.insert(4);

        mod2.addInsertCallback([](const int& value) { INFO("mod2 added: " << value); });
        mod2.addRemoveCallback([](const int& value) { INFO("mod2 removed: " << value); });
        mod6.addInsertCallback([](const int& value) { INFO("mod6 added: " << value); });
        mod6.addRemoveCallback([](const int& value) { INFO("mod6 removed: " << value); });

        INFO("mod2: " << mod2);
        INFO("mod6: " << mod6);
        CHECK_THROWS_AS(std::swap(mod2, mod6), int);
        INFO("mod2: " << mod2);
        INFO("mod6: " << mod6);
        CHECK_THROWS_AS(swap(mod2, mod6), int);
        INFO("mod2: " << mod2);
        INFO("mod6: " << mod6);

        CHECK_NOTHROW(mod2.insert(mod6.begin(), mod6.end()));
        CHECK_THROWS_AS(mod2.insert({8, 9}), int);

        INFO("mod2: " << mod2);
        INFO("mod6: " << mod6);

        mod2 = {4, 6, 8};
    }
}
