#include <catch2/catch.hpp>

#include <ext/tuple>

namespace {
class Foo {
public:
    Foo(int i)
        : m_I(i)
    {
    }
    int m_I;
};
}

static int tupleTestApply1(Foo a, Foo b)
{
    return a.m_I * b.m_I;
}

static int tupleTestApply2(int a, int b)
{
    return a + b;
}

TEST_CASE("Apply Test", "[unit][std][bits]")
{
    SECTION("Apply")
    {
        std::tuple<Foo, Foo> t1(1, 2);
        std::tuple<int, int> t2(1, 2);
        CHECK(std::apply(tupleTestApply1, t1) == 2);
        CHECK(std::apply(tupleTestApply2, t2) == 3);
    }
}
