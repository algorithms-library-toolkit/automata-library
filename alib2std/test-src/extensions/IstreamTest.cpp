#include <catch2/catch.hpp>

#include <ext/istream>
#include <ext/pair>
#include <ext/set>
#include <ext/sstream>
#include <ext/string>
#include <ext/vector>

TEST_CASE("Istream", "[unit][std][bits]")
{
    SECTION("istream")
    {
        ext::istringstream ss("TEST");

        CHECK((static_cast<bool>(ss >> std::string("TEST"))) == true);

        ss.str("TEST");

        CHECK((static_cast<bool>(ss >> std::string("TESS"))) == false);
        CHECK(ss.str() == "TEST");


        CHECK((static_cast<bool>(ss >> std::string("TESTS"))) == false);
        CHECK(ss.str() == "TEST");
    }
}
