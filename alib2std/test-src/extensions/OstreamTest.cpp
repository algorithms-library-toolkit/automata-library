#include <catch2/catch.hpp>

#include <ext/pair>
#include <ext/set>
#include <ext/sstream>
#include <ext/vector>

TEST_CASE("Ostream", "[unit][std][bits]")
{
    SECTION("Test Ostream")
    {
        ext::vector<int> vector1{1, 2, 3};
        ext::vector<int> vector2{2, 3, 4};

        ext::ostringstream ss1;
        ss1 << vector1;
        CAPTURE(ss1.str());
        CHECK("[1, 2, 3]" == ss1.str());

        ext::set<ext::vector<int>> set1{vector1};
        ext::set<ext::vector<int>> set2{vector2};

        ext::ostringstream ss2;
        ss2 << set1;
        CAPTURE(ss2.str());
        CHECK("{[1, 2, 3]}" == ss2.str());

        ext::pair<ext::set<ext::vector<int>>, ext::set<ext::vector<int>>> pair1{set1, set2};
        ext::pair<ext::set<ext::vector<int>>, ext::set<ext::vector<int>>> pair2{set2, set1};

        ext::ostringstream ss3;
        ss3 << pair1;
        CAPTURE(ss3.str());
        CHECK("({[1, 2, 3]}, {[2, 3, 4]})" == ss3.str());
    }
}
