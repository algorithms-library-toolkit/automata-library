#include <catch2/catch.hpp>
#include <ext/compare>

#include <ext/map>
#include <ext/pair>
#include <ext/set>
#include <ext/tuple>
#include <ext/vector>

TEST_CASE("Compare Test", "[unit][std][bits]")
{
    SECTION("Compare")
    {
        ext::vector<int> vector1{1, 2, 3};
        ext::vector<int> vector2{2, 3, 4};

        auto v1v2 = vector1 <=> vector2 < 0;
        CHECK(v1v2);

        ext::set<ext::vector<int>> set1{vector1};
        ext::set<ext::vector<int>> set2{vector2};

        auto s1s2 = set1 <=> set2 < 0;
        CHECK(s1s2);

        ext::pair<ext::set<ext::vector<int>>, ext::set<ext::vector<int>>> pair1{set1, set2};
        ext::pair<ext::set<ext::vector<int>>, ext::set<ext::vector<int>>> pair2{set2, set1};

        auto p1p2 = pair1 <=> pair2 < 0;
        CHECK(p1p2);

        int two = 2;
        int three = 3;
        int four = 4;

        ext::map<int, const int*> map1 = {{1, &two}, {2, &three}};
        ext::map<int, const int*> map2 = {{2, &three}, {3, &four}};

        auto m1m2 = map1 <=> map2 < 0;
        CHECK(m1m2);

        auto tuple1 = ext::tie(vector1, set1, pair1, map1);
        auto tuple2 = ext::tie(vector2, set2, pair2, map2);

        auto t1t2 = tuple1 <=> tuple2 < 0;
        CHECK(t1t2);
    }
}

TEST_CASE("Dry Comparison Test", "[unit][std][bits]")
{
    SECTION("Dry Compare each other")
    {
        CHECK(ext::none_of{4, 5, 3} <= ext::any_of(1, 2));
        CHECK(ext::any_of{4, 5, 3} == 3);
    }
}
