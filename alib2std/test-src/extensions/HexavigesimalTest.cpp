#include <catch2/catch.hpp>

#include <ext/hexavigesimal>

TEST_CASE("Hexavigesimal", "[unit][std][bits]")
{
    SECTION("Hexavigesimal")
    {
        CHECK(std::string("A") == ext::toBase26(0));
        CHECK(std::string("B") == ext::toBase26(1));
        CHECK(std::string("Z") == ext::toBase26(25));
        CHECK(std::string("BA") == ext::toBase26(26));
        CHECK(std::string("BB") == ext::toBase26(27));
        CHECK(std::string("ZZ") == ext::toBase26(675));
        CHECK(std::string("BAA") == ext::toBase26(676));

        CHECK(0u == ext::fromBase26("A"));
        CHECK(1u == ext::fromBase26("B"));
        CHECK(25u == ext::fromBase26("Z"));
        CHECK(26u == ext::fromBase26("BA"));
        CHECK(27u == ext::fromBase26("BB"));
        CHECK(675u == ext::fromBase26("ZZ"));
        CHECK(676u == ext::fromBase26("BAA"));

        CHECK(std::string("") == ext::bijectiveToBase26(0));
        CHECK(std::string("A") == ext::bijectiveToBase26(1));
        CHECK(std::string("Z") == ext::bijectiveToBase26(26));
        CHECK(std::string("AA") == ext::bijectiveToBase26(27));
        CHECK(std::string("AB") == ext::bijectiveToBase26(28));
        CHECK(std::string("ZZ") == ext::bijectiveToBase26(702));
        CHECK(std::string("AAA") == ext::bijectiveToBase26(703));

        CHECK(0u == ext::bijectiveFromBase26(""));
        CHECK(1u == ext::bijectiveFromBase26("A"));
        CHECK(26u == ext::bijectiveFromBase26("Z"));
        CHECK(27u == ext::bijectiveFromBase26("AA"));
        CHECK(28u == ext::bijectiveFromBase26("AB"));
        CHECK(702u == ext::bijectiveFromBase26("ZZ"));
        CHECK(703u == ext::bijectiveFromBase26("AAA"));
    }
}
