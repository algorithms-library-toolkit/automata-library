/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <iostream>
#include <string>
#include <typeindex>

namespace ext {

/**
 * Class extending the type_index class from the standard library. Original reason is to allow printing of the container with overloaded operator <<.
 *
 * The class mimics the behavior of the type_index from the standatd library.
 */
class type_index : public std::type_index {
public:
    using std::type_index::type_index;
    using std::type_index::operator=;
};

/**
 * \brief
 * Implementation of three way comparison helper.
 *
 * \return -1 if the first is smaller than the second, 0 if they are the same, 1 if the the first is bigger than the second
 */
inline int operator-(const ext::type_index& first, const ext::type_index& second)
{
    return (first < second) ? -1 : (first > second) ? 1
                                                    : 0;
}

/**
 * \brief
 * Operator to print the type_index to the output stream.
 *
 * \param out the output stream
 * \param type the type_index to print
 *
 * \return the output stream from the \p out
 */
std::ostream& operator<<(std::ostream& os, const ext::type_index& type);

} /* namespace ext */
