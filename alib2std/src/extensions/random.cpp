#include "random.hpp"

namespace ext {

std::random_device& random_devices::random = getRandom();
random_devices::semirandom_device& random_devices::semirandom = getSemirandom();

} /* namespace ext */
