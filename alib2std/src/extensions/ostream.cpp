#include "ostream.h"

namespace ext {

ostream::ostream(std::basic_streambuf<char>* sb)
    : m_os(std::make_unique<std::ostream>(sb))
{
}

ostream::~ostream() noexcept = default;

ostream::ostream(ostream&& rhs) noexcept
    : m_os(std::move(rhs.m_os))
{
}

ostream& ostream::operator=(ostream&& rhs) noexcept
{
    m_os = std::move(rhs.m_os);
    return *this;
}

ostream& operator<<(ostream& os, std::ostream& (*fn)(std::ostream&))
{
    static_cast<std::ostream&>(os) << fn;
    return os;
}

ostream::operator std::ostream&() &
{
    return *m_os;
}

ostream::operator const std::ostream&() const&
{
    return *m_os;
}

ostream& ostream::put(char ch)
{
    m_os->put(ch);
    return *this;
}

ostream& ostream::write(const char* s, std::streamsize count)
{
    m_os->write(s, count);
    return *this;
}

std::streampos ostream::tellp()
{
    return m_os->tellp();
}

ostream& ostream::seekp(std::streampos pos)
{
    m_os->seekp(pos);
    return *this;
}

ostream& ostream::seekp(std::streamoff off, std::ios_base::seekdir dir)
{
    m_os->seekp(off, dir);
    return *this;
}

ostream& ostream::flush()
{
    m_os->flush();
    return *this;
}

std::streambuf* ostream::rdbuf() const
{
    return m_os->rdbuf();
}

std::streambuf* ostream::rdbuf(std::streambuf* buf)
{
    return m_os->rdbuf(buf);
}

void ostream::setstate(std::ios_base::iostate state)
{
    m_os->setstate(state);
}

void ostream::clear(std::ios_base::iostate state)
{
    m_os->clear(state);
}

} /* namespace ext */
