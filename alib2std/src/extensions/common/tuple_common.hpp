#pragma once

#include <tuple>
#include <utility>

namespace ext {

template <class Tuple, class Callable, std::size_t... Is>
static void foreach_fn(Tuple&& t, Callable callback, std::index_sequence<Is...>)
{
    (callback(std::forward<std::tuple_element_t<Is, std::remove_reference_t<Tuple>>>(t.template get<Is>())), ...);
}

template <class Tuple, class Callable>
void foreach (Tuple&& t, Callable callback)
{
    return foreach_fn(std::forward<Tuple>(t), callback, std::make_index_sequence<std::tuple_size_v<std::remove_reference_t<Tuple>>>());
}

} /* namespace ext */
