#pragma once

#include <concepts>
#include <memory>
#include <ostream>

namespace ext {

class ostream {
    std::unique_ptr<std::ostream> m_os;

public:
    ostream(std::basic_streambuf<char>* sb);

    virtual ~ostream() noexcept;

protected:
    ostream(ostream&& rhs) noexcept;

    ostream& operator=(ostream&& rhs) noexcept;

public:
    template <class T>
        requires requires { std::declval<std::ostream>() << std::declval<T>(); }
    friend ostream& operator<<(ostream& os, const T& val)
    {
        static_cast<std::ostream&>(os) << val;
        return os;
    }

    friend ostream& operator<<(ostream& os, std::ostream& (*fn)(std::ostream&));

    /*	does no exist in std, note it should not
            friend ostream & operator << ( ostream & os, std::ios_base & ( * fn ) ( std::ios_base & ) ) {
                    static_cast < std::ostream & > ( os ) >> fn;
                    return os;
            }*/

protected:
    operator std::ostream&() &;

    operator const std::ostream&() const&;

public:
    ostream& put(char ch);

    ostream& write(const char* s, std::streamsize count);

    std::streampos tellp();

    ostream& seekp(std::streampos pos);

    ostream& seekp(std::streamoff off, std::ios_base::seekdir dir);

    ostream& flush();

    std::streambuf* rdbuf() const;

    std::streambuf* rdbuf(std::streambuf* buf);

    void setstate(std::ios_base::iostate state);

    void clear(std::ios_base::iostate state = std::ios_base::goodbit);
};

} /* namespace ext */
