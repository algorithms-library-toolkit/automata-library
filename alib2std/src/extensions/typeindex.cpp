#include "typeindex.h"

#include <cstdlib>
#include <cxxabi.h>

#include <functional>
#include <memory>

namespace ext {

std::ostream& operator<<(std::ostream& os, const ext::type_index& type)
{
    int status;

    // TODO switch to using lambda function in the place of std::function once swithing to clang++-13
    std::unique_ptr<char, std::function<void(char*)>> demangled(abi::__cxa_demangle(type.name(), nullptr, nullptr, &status), [](char* ptr) { free(ptr); });
    os << demangled;
    return os;
}

} /* namespace ext */
