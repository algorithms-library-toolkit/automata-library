#pragma once

#include <sstream>

#include <ext/istream>
#include <ext/ostream>

namespace ext {

class ostringstream : public ostream {
public:
    ostringstream();

    ostringstream(ostringstream&& rhs) noexcept;

    ~ostringstream() noexcept override;

    ostringstream& operator=(ostringstream&&) noexcept;

    void swap(ostringstream& other);

    std::string str() const&;

    void str(const std::string& str);

    std::stringbuf* rdbuf();

    const std::stringbuf* rdbuf() const;
};

class istringstream : public istream {
public:
    istringstream(const std::string& data = "");

    istringstream(istringstream&& rhs) noexcept;

    ~istringstream() noexcept override;

    istringstream& operator=(istringstream&&) noexcept;

    void swap(istringstream& other);

    std::string str() const&;

    void str(const std::string& str);

    std::stringbuf* rdbuf();

    const std::stringbuf* rdbuf() const;
};

/**
 * Interface based on P1228 (http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2019/p1228r1.html), if it ever gets to C++
 */
template <typename... Types>
std::string concat(const Types&... values)
{
    ext::ostringstream os;
    (os << ... << values);
    return os.str();
}

} /* namespace ext */
