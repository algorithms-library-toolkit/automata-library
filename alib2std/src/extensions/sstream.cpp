#include "sstream.h"

namespace ext {

ostringstream::ostringstream()
    : ostream(new std::stringbuf())
{
}

ostringstream::ostringstream(ostringstream&& rhs) noexcept
    : ostream(std::move(rhs))
{
    rhs = ostringstream();
}

ostringstream::~ostringstream() noexcept
{
    delete rdbuf();
}

ostringstream& ostringstream::operator=(ostringstream&&) noexcept = default;

void ostringstream::swap(ostringstream& other)
{
    ostringstream tmp(std::move(*this));
    *this = std::move(other);
    other = std::move(tmp);
}

std::string ostringstream::str() const&
{
    return rdbuf()->str();
}

void ostringstream::str(const std::string& str)
{
    return rdbuf()->str(str);
}

std::stringbuf* ostringstream::rdbuf()
{
    return static_cast<std::stringbuf*>(ostream::rdbuf());
}

const std::stringbuf* ostringstream::rdbuf() const
{
    return static_cast<const std::stringbuf*>(ostream::rdbuf());
}

istringstream::istringstream(const std::string& data)
    : istream(new std::stringbuf(data))
{
}

istringstream::istringstream(istringstream&& rhs) noexcept
    : istream(std::move(rhs))
{
    rhs = istringstream();
}

istringstream::~istringstream() noexcept
{
    delete rdbuf();
}

istringstream& istringstream::operator=(istringstream&&) noexcept = default;

void istringstream::swap(istringstream& other)
{
    istringstream tmp(std::move(*this));
    *this = std::move(other);
    other = std::move(tmp);
}

std::string istringstream::str() const&
{
    return rdbuf()->str();
}

void istringstream::str(const std::string& str)
{
    return rdbuf()->str(str);
}

std::stringbuf* istringstream::rdbuf()
{
    return static_cast<std::stringbuf*>(istream::rdbuf());
}

const std::stringbuf* istringstream::rdbuf() const
{
    return static_cast<const std::stringbuf*>(istream::rdbuf());
}

} /* namespace ext */
