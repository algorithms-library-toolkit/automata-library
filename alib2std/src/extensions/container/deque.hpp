/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <deque>

#include <ext/ostream>

#include <extensions/range.hpp>

namespace ext {

/**
 * \brief
 * Class extending the deque class from the standard library. Original reason is to allow printing of the container with overloaded operator <<.
 *
 * The class mimics the behavior of the deque from the standard library.
 *
 * \tparam T the type of values inside the deque
 * \tparam Alloc the allocator of values of type T
 */
template <class T, class Alloc = std::allocator<T>>
class deque : public std::deque<T, Alloc> {
public:
    /**
     * Inherit constructors of the standard deque
     */
    using std::deque<T, Alloc>::deque; // NOLINT(modernize-use-equals-default)

    /**
     * Inherit operator = of the standard deque
     */
    using std::deque<T, Alloc>::operator=;
#ifndef __clang__

    /**
     * Default constructor needed by g++ since it is not inherited
     */
    deque() = default;

    /**
     * Copy constructor needed by g++ since it is not inherited
     */
    deque(const deque& other) = default;

    /**
     * Move constructor needed by g++ since it is not inherited
     */
    deque(deque&& other) = default;

    /**
     * Copy operator = needed by g++ since it is not inherited
     */
    deque& operator=(deque&& other) = default;

    /**
     * Move operator = needed by g++ since it is not inherited
     */
    deque& operator=(const deque& other) = default;
#endif

    /**
     * Constructor from range of values.
     *
     * \tparam Iterator the type of range iterator
     *
     * \param range the source range
     */
    template <class Iterator>
    explicit deque(const ext::iterator_range<Iterator>& range)
        : deque(range.begin(), range.end())
    {
    }

    /**
     * \brief
     * Inherited behavior of begin for non-const instance.
     *
     * \return iterator the first element of deque
     */
    auto begin() &
    {
        return std::deque<T, Alloc>::begin();
    }

    /**
     * \brief
     * Inherited behavior of begin for const instance.
     *
     * \return const_iterator the first element of deque
     */
    auto begin() const&
    {
        return std::deque<T, Alloc>::begin();
    }

    /**
     * \brief
     * Inherited behavior of begin for rvalues.
     *
     * \return move_iterator the first element of deque
     */
    auto begin() &&
    {
        return make_move_iterator(this->begin());
    }

    /**
     * \brief
     * Inherited behavior of end for non-const instance.
     *
     * \return iterator to one after the last element of deque
     */
    auto end() &
    {
        return std::deque<T, Alloc>::end();
    }

    /**
     * \brief
     * Inherited behavior of end for const instance.
     *
     * \return const_iterator to one after the last element of deque
     */
    auto end() const&
    {
        return std::deque<T, Alloc>::end();
    }

    /**
     * \brief
     * Inherited behavior of end for rvalues.
     *
     * \return move_iterator to one after the last element of deque
     */
    auto end() &&
    {
        return make_move_iterator(this->end());
    }

    /**
     * \brief
     * Make range of non-const begin to end iterators.
     *
     * \return full range over container values
     */
    auto range() &
    {
        auto endIter = end();
        auto beginIter = begin();
        return ext::iterator_range<decltype(endIter)>(beginIter, endIter);
    }

    /**
     * \brief
     * Make range of non-const begin to end iterators.
     *
     * \return full range over container values
     */
    auto range() const&
    {
        auto endIter = end();
        auto beginIter = begin();
        return ext::iterator_range<decltype(endIter)>(beginIter, endIter);
    }

    /**
     * \brief
     * Make range of move begin to end iterators.
     *
     * \return full range over container values
     */
    auto range() &&
    {
        auto endIter = std::move(*this).end();
        auto beginIter = std::move(*this).begin();
        return ext::iterator_range<decltype(endIter)>(beginIter, endIter);
    }
};

/**
 * \brief
 * Operator to print the deque to the output stream.
 *
 * \param out the output stream
 * \param deque the deque to print
 *
 * \tparam T the type of values inside the deque
 * \tparam Ts ... remaining unimportant template parameters of the deque
 *
 * \return the output stream from the \p out
 */
template <class T, class... Ts>
ext::ostream& operator<<(ext::ostream& out, const ext::deque<T, Ts...>& list)
{
    out << "[";

    bool first = true;

    for (const T& item : list) {
        if (!first)
            out << ", ";

        first = false;
        out << item;
    }

    out << "]";
    return out;
}

} /* namespace ext */
