/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <algorithm>
#include <stdexcept>

#include <ext/sstream>

#include "vector.hpp"

namespace ext {

/**
 * \brief
 * To string method designated for objects that can be casted to string.
 *
 * \tparam T type of the casted instance
 *
 * \param value the casted instance
 *
 * \return string representation of the instance
 */
template <typename T>
std::string to_string(const T& value)
{
    ext::ostringstream ss;
    ss << value;
    return ss.str();
}

/**
 * \brief
 * Predeclaration of from string method. The from string methods are supposed to convert string representation of a given type to that type.
 *
 * \tparam the requested result type
 *
 * \param the string representation
 *
 * \return value converted from string
 */
template <typename T>
T from_string(const std::string&);

/**
 * \brief
 * Specialisation of from string method for string result to make the interface complete.
 *
 * \param the string representation
 *
 * \return string copy
 */
template <>
std::string from_string(const std::string& value);

/**
 * \brief
 * Specialisation of from string method for integer result.
 *
 * \param the string representation
 *
 * \return integer value converted from string
 */
template <>
int from_string(const std::string& value);

/**
 * \brief
 * Specialisation of from string method for booleans result.
 *
 * \param the string representation
 *
 * \return boolean value converted from string
 */
template <>
bool from_string(const std::string& value);

/**
 * \brief
 * Specialisation of from string method for long result.
 *
 * \param the string representation
 *
 * \return long value converted from string
 */
template <>
long from_string(const std::string& value);

/**
 * \brief
 * Specialisation of from string method for long long result.
 *
 * \param the string representation
 *
 * \return long long value converted from string
 */
template <>
long long from_string(const std::string& value);

/**
 * \brief
 * Specialisation of from string method for unsigned result.
 *
 * \param the string representation
 *
 * \return unsigned value converted from string
 */
template <>
unsigned from_string(const std::string& value);

/**
 * \brief
 * Specialisation of from string method for unsigned long result.
 *
 * \param the string representation
 *
 * \return unsigned long value converted from string
 */
template <>
unsigned long from_string(const std::string& value);

/**
 * \brief
 * Specialisation of from string method for unsigned long long result.
 *
 * \param the string representation
 *
 * \return unsigned long long value converted from string
 */
template <>
unsigned long long from_string(const std::string& value);

/**
 * \brief
 * Specialisation of from string method for double result.
 *
 * \param the string representation
 *
 * \return double value converted from string
 */
template <>
double from_string(const std::string& value);

/**
 * \brief
 * Splits the string to substrings based on delimiter.
 *
 * \param source the string to split
 * \param delimiter the string to split by
 *
 * \return vector of substrings
 */
ext::vector<std::string> explode(const std::string& source, const std::string& delimiter);

/**
 * \brief
 * Merges strings using the delimiter.
 *
 * \param source the vector of strings to merge
 * \param delimiter the connection string
 *
 * \return the merged string
 */
std::string implode(const std::vector<std::string>& source, const std::string& delimiter);

/**
 * \brief
 * isspace method.
 *
 * \param ch the tested character
 *
 * \return true if the character is whitespace, false othervise
 */
inline bool isspace(int ch)
{
    return std::isspace(ch) != 0;
}

/**
 * \brief
 * Inverse of isspace method.
 *
 * \param ch the tested character
 *
 * \return true if the character is not whitespace, false othervise
 */
inline bool not_isspace(int ch)
{
    return std::isspace(ch) == 0;
}

/**
 * \brief
 * Trims spaces inside the string from the left.
 *
 * \param s the trimed string
 *
 * \return string without spaces at front of it
 */
inline std::string_view ltrim(std::string_view s)
{
    const auto* endPos = std::find_if(s.begin(), s.end(), not_isspace);
    s.remove_prefix(std::distance(s.begin(), endPos));
    return s;
}

/**
 * \brief
 * Trims spaces inside the string from the right.
 *
 * \param s the trimed string
 *
 * \return string without spaces at back of it
 */
inline std::string_view rtrim(std::string_view s)
{
    const auto* beginPos = std::find_if(s.rbegin(), s.rend(), not_isspace).base();
    s.remove_suffix(std::distance(beginPos, s.end()));
    return s;
}

/**
 * \brief
 * Trims spaces inside the string from both sides.
 *
 * \param s the trimed string
 *
 * \return string without spaces at both sides of it
 */
inline std::string_view trim(std::string_view s)
{
    return rtrim(ltrim(s));
}

// Creates a string view from a pair of iterators
//  http://stackoverflow.com/q/33750600/882436
inline std::string_view make_string_view(std::string::const_iterator begin, std::string::const_iterator end)
{
    return std::string_view{(begin != end) ? &*begin : nullptr, static_cast<std::string_view::size_type>(std::max(std::distance(begin, end), static_cast<typename std::string_view::difference_type>(0)))};
} // make_string_view

} /* namespace ext */
