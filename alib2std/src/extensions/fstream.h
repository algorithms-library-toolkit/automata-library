#pragma once

#include <fstream>

#include <ext/istream>
#include <ext/ostream>

namespace ext {

class ofstream : public ostream {
public:
    ofstream(const char* filename)
        : ostream(new std::filebuf())
    {
        open(filename);
    }

    ofstream(const std::string& filename)
        : ofstream(filename.c_str())
    {
    }

    ofstream(ofstream&& rhs) noexcept
        : ostream(std::move(rhs))
    {
        rhs = ofstream("");
    }

    ofstream& operator=(ofstream&&) noexcept = default;

    void swap(ofstream& other)
    {
        ofstream tmp(std::move(*this));
        *this = std::move(other);
        other = std::move(tmp);
    }

    void open(const char* filename, std::ios_base::openmode mode = std::ios_base::out)
    {
        std::ostream& os = static_cast<std::ostream&>(*this);
        if (!rdbuf()->open(filename, mode)) {
            os.setstate(std::ios_base::failbit);
        } else {
            os.clear();
        }
    }

    void close()
    {
        std::ostream& os = static_cast<std::ostream&>(*this);
        if (rdbuf()->close())
            os.setstate(std::ios_base::failbit);
    }

    bool is_open()
    {
        return rdbuf()->is_open();
    }

    bool is_open() const
    {
        return rdbuf()->is_open();
    }

    std::filebuf* rdbuf()
    {
        return static_cast<std::filebuf*>(ostream::rdbuf());
    }

    const std::filebuf* rdbuf() const
    {
        return static_cast<const std::filebuf*>(ostream::rdbuf());
    }
};

class ifstream : public istream {
public:
    ifstream(const char* filename)
        : istream(new std::filebuf())
    {
        open(filename);
    }

    ifstream(const std::string& filename)
        : ifstream(filename.c_str())
    {
    }

    ifstream(ifstream&& rhs) noexcept
        : istream(std::move(rhs))
    {
        rhs = ifstream("");
    }

    ifstream& operator=(ifstream&&) noexcept = default;

    void swap(ifstream& other)
    {
        ifstream tmp(std::move(*this));
        *this = std::move(other);
        other = std::move(tmp);
    }

    void open(const char* filename, std::ios_base::openmode mode = std::ios_base::in)
    {
        std::istream& os = static_cast<std::istream&>(*this);
        if (!rdbuf()->open(filename, mode)) {
            os.setstate(std::ios_base::failbit);
        } else {
            os.clear();
        }
    }

    void close()
    {
        std::istream& os = static_cast<std::istream&>(*this);
        if (rdbuf()->close())
            os.setstate(std::ios_base::failbit);
    }

    bool is_open()
    {
        return rdbuf()->is_open();
    }

    bool is_open() const
    {
        return rdbuf()->is_open();
    }

    std::filebuf* rdbuf()
    {
        return static_cast<std::filebuf*>(istream::rdbuf());
    }

    const std::filebuf* rdbuf() const
    {
        return static_cast<const std::filebuf*>(istream::rdbuf());
    }
};

} /* namespace ext */
