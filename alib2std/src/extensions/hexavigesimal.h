/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <string>

namespace ext {

/**
 * \brief
 * Function to convert a number to sequence of uppercase letters.
 *
 * The following translation is implemented:
 *
 * Maps 0 -> A, 1 -> B, ..., 25 -> Z, 26 -> AB, 27 -> AC, ..., 675 -> ZZ, 676 -> BAA, ...
 * http://en.wikipedia.org/wiki/Hexavigesimal
 *
 * \param n the number to convert
 *
 * \return string sequence corresponding to the value in \p n
 */
std::string toBase26(unsigned n);

/**
 * \brief
 * Function to convert sequence of uppercase letters to a number.
 *
 * The following translation is implemented:
 *
 * Maps A -> 0, B -> 0, ..., Z -> 25, AB -> 26, AC -> 27, ..., ZZ -> 675, BAA -> 676, ...
 * http://en.wikipedia.org/wiki/Hexavigesimal
 *
 * \param rep string sequence of uppercase letters
 *
 * \return the number corresponding to the value in \p rep
 */
unsigned fromBase26(const std::string& rep);

/**
 * \brief
 * Function to convert a number to sequence of uppercase letters.
 *
 * The following translation is implemented:
 *
 * Maps 1 -> A, 2 -> B, ..., 26 -> Z, 27 -> AA, 28 -> AB, ..., 676 -> ZZ, 677 -> AAA, ...
 * http://en.wikipedia.org/wiki/Hexavigesimal
 *
 * \param n the number to convert
 *
 * \return string sequence corresponding to the value in \p n
 */
std::string bijectiveToBase26(unsigned n);

/**
 * \brief
 * Function to convert sequence of uppercase letters to a number.
 *
 * The following translation is implemented:
 *
 * Maps 1 -> A, 2 -> B, ..., 26 -> Z, 27 -> AA, AB -> 28, ..., ZZ -> 676, AAA -> 677, ...
 * http://en.wikipedia.org/wiki/Hexavigesimal
 *
 * \param rep string sequence of uppercase letters
 *
 * \return the number corresponding to the value in \p rep
 */
unsigned bijectiveFromBase26(const std::string& rep);

} /* namespace ext */
