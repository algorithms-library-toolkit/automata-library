#include <catch2/catch.hpp>

#include <alphabet/Bar.h>

namespace alphabet {
template <>
inline int Bar::instance<int>()
{
    return -1;
}
} /* namespace alphabet */

TEST_CASE("Alphabet symbols test", "[unit][data][alphabet]")
{
    SECTION("Order")
    {
        common::ranked_symbol<> rs1(object::ObjectFactory<>::construct(alphabet::Bar{}), 0);
        common::ranked_symbol<> rs2(object::ObjectFactory<>::construct(alphabet::Bar{}), 0);

        CHECK(rs1 == rs2);
    }

    SECTION("Default values")
    {
        CHECK(alphabet::Bar::instance<int>() == -1);
        CHECK(alphabet::Bar::instance<char>() == '|');
        INFO(alphabet::Bar::instance<std::string>());
        CHECK(alphabet::Bar::instance<std::string>() == "|");
        CHECK(alphabet::Bar::instance<alphabet::Bar>() == alphabet::Bar());
        CHECK(alphabet::Bar::instance<object::Object>() == object::ObjectFactory<>::construct(alphabet::Bar()));
    }
}
