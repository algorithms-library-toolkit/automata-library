#include "GrammarException.h"

namespace grammar {

GrammarException::GrammarException(const std::string& cause)
    : CommonException(cause)
{
}

} /* namespace grammar */
