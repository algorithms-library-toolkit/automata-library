#include <grammar/Grammar.h>
#include <object/Object.h>
#include "GNF.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<grammar::GNF<>>();
auto xmlRead = registration::XmlReaderRegister<grammar::GNF<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, grammar::GNF<>>();

} /* namespace */
