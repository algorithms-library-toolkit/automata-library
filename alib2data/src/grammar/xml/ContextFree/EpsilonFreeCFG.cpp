#include <grammar/Grammar.h>
#include <object/Object.h>
#include "EpsilonFreeCFG.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<grammar::EpsilonFreeCFG<>>();
auto xmlRead = registration::XmlReaderRegister<grammar::EpsilonFreeCFG<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, grammar::EpsilonFreeCFG<>>();

} /* namespace */
