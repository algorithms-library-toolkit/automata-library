#include <grammar/Grammar.h>
#include <object/Object.h>
#include "CNF.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<grammar::CNF<>>();
auto xmlRead = registration::XmlReaderRegister<grammar::CNF<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, grammar::CNF<>>();

} /* namespace */
