#include <grammar/Grammar.h>
#include <object/Object.h>
#include "NonContractingGrammar.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<grammar::NonContractingGrammar<>>();
auto xmlRead = registration::XmlReaderRegister<grammar::NonContractingGrammar<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, grammar::NonContractingGrammar<>>();

} /* namespace */
