#include <grammar/Grammar.h>
#include <object/Object.h>
#include "ContextPreservingUnrestrictedGrammar.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<grammar::ContextPreservingUnrestrictedGrammar<>>();
auto xmlRead = registration::XmlReaderRegister<grammar::ContextPreservingUnrestrictedGrammar<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, grammar::ContextPreservingUnrestrictedGrammar<>>();

} /* namespace */
