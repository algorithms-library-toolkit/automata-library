#include <grammar/Grammar.h>
#include <object/Object.h>
#include "UnrestrictedGrammar.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<grammar::UnrestrictedGrammar<>>();
auto xmlRead = registration::XmlReaderRegister<grammar::UnrestrictedGrammar<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, grammar::UnrestrictedGrammar<>>();

} /* namespace */
