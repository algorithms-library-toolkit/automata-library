#include <grammar/Grammar.h>
#include <object/Object.h>
#include "RightRG.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<grammar::RightRG<>>();
auto xmlRead = registration::XmlReaderRegister<grammar::RightRG<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, grammar::RightRG<>>();

} /* namespace */
