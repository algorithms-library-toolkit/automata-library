#include <grammar/Grammar.h>
#include <object/Object.h>
#include "LeftRG.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<grammar::LeftRG<>>();
auto xmlRead = registration::XmlReaderRegister<grammar::LeftRG<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, grammar::LeftRG<>>();

} /* namespace */
