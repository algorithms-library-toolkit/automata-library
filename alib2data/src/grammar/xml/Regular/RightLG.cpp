#include <grammar/Grammar.h>
#include <object/Object.h>
#include "RightLG.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<grammar::RightLG<>>();
auto xmlRead = registration::XmlReaderRegister<grammar::RightLG<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, grammar::RightLG<>>();

} /* namespace */
