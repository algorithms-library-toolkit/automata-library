#include "LeftRG.h"

#include <registration/CastRegistration.hpp>
#include <registration/ValuePrinterRegistration.hpp>

template class grammar::LeftRG<>;
template class abstraction::ValueHolder<grammar::LeftRG<>>;
template const grammar::LeftRG<>& abstraction::retrieveValue<const grammar::LeftRG<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const grammar::LeftRG<>&>;
template class registration::NormalizationRegisterImpl<grammar::LeftRG<>>;

namespace {

auto valuePrinter = registration::ValuePrinterRegister<grammar::LeftRG<>>();

} /* namespace */
