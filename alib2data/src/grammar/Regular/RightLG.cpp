#include "RightLG.h"

#include <registration/CastRegistration.hpp>
#include <registration/ValuePrinterRegistration.hpp>

template class grammar::RightLG<>;
template class abstraction::ValueHolder<grammar::RightLG<>>;
template const grammar::RightLG<>& abstraction::retrieveValue<const grammar::RightLG<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const grammar::RightLG<>&>;
template class registration::NormalizationRegisterImpl<grammar::RightLG<>>;

namespace {

auto valuePrinter = registration::ValuePrinterRegister<grammar::RightLG<>>();

} /* namespace */
