#include "UnrestrictedGrammar.h"

#include <registration/CastRegistration.hpp>
#include <registration/ValuePrinterRegistration.hpp>

template class grammar::UnrestrictedGrammar<>;
template class abstraction::ValueHolder<grammar::UnrestrictedGrammar<>>;
template const grammar::UnrestrictedGrammar<>& abstraction::retrieveValue<const grammar::UnrestrictedGrammar<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const grammar::UnrestrictedGrammar<>&>;
template class registration::NormalizationRegisterImpl<grammar::UnrestrictedGrammar<>>;

namespace {

auto valuePrinter = registration::ValuePrinterRegister<grammar::UnrestrictedGrammar<>>();

} /* namespace */
