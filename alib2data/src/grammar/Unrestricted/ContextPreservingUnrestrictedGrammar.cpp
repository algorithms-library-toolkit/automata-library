#include "ContextPreservingUnrestrictedGrammar.h"

#include <registration/CastRegistration.hpp>
#include <registration/ValuePrinterRegistration.hpp>

template class grammar::ContextPreservingUnrestrictedGrammar<>;
template class abstraction::ValueHolder<grammar::ContextPreservingUnrestrictedGrammar<>>;
template const grammar::ContextPreservingUnrestrictedGrammar<>& abstraction::retrieveValue<const grammar::ContextPreservingUnrestrictedGrammar<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const grammar::ContextPreservingUnrestrictedGrammar<>&>;
template class registration::NormalizationRegisterImpl<grammar::ContextPreservingUnrestrictedGrammar<>>;

namespace {

auto valuePrinter = registration::ValuePrinterRegister<grammar::ContextPreservingUnrestrictedGrammar<>>();

} /* namespace */
