#include "CFG.h"

#include <registration/CastRegistration.hpp>
#include <registration/ValuePrinterRegistration.hpp>

template class grammar::CFG<>;
template class abstraction::ValueHolder<grammar::CFG<>>;
template const grammar::CFG<>& abstraction::retrieveValue<const grammar::CFG<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const grammar::CFG<>&>;
template class registration::NormalizationRegisterImpl<grammar::CFG<>>;

namespace {

auto CFGEpsilonFreeCFG = registration::CastRegister<grammar::CFG<>, grammar::EpsilonFreeCFG<>>();

auto valuePrinter = registration::ValuePrinterRegister<grammar::CFG<>>();

} /* namespace */
