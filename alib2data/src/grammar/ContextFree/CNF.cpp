#include "CNF.h"

#include <registration/CastRegistration.hpp>
#include <registration/ValuePrinterRegistration.hpp>

template class grammar::CNF<>;
template class abstraction::ValueHolder<grammar::CNF<>>;
template const grammar::CNF<>& abstraction::retrieveValue<const grammar::CNF<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const grammar::CNF<>&>;
template class registration::NormalizationRegisterImpl<grammar::CNF<>>;

namespace {

auto valuePrinter = registration::ValuePrinterRegister<grammar::CNF<>>();

} /* namespace */
