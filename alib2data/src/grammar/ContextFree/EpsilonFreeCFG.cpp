#include "EpsilonFreeCFG.h"

#include <registration/CastRegistration.hpp>
#include <registration/ValuePrinterRegistration.hpp>

template class grammar::EpsilonFreeCFG<>;
template class abstraction::ValueHolder<grammar::EpsilonFreeCFG<>>;
template const grammar::EpsilonFreeCFG<>& abstraction::retrieveValue<const grammar::EpsilonFreeCFG<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const grammar::EpsilonFreeCFG<>&>;
template class registration::NormalizationRegisterImpl<grammar::EpsilonFreeCFG<>>;

namespace {

auto valuePrinter = registration::ValuePrinterRegister<grammar::EpsilonFreeCFG<>>();

} /* namespace */
