/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <ext/algorithm>

#include <alib/map>
#include <alib/set>
#include <alib/vector>

#include <core/modules.hpp>

#include <common/DefaultSymbolType.h>

#include <grammar/GrammarException.h>

#include <alphabet/common/SymbolDenormalize.h>
#include <alphabet/common/SymbolNormalize.h>
#include <core/type_details_base.hpp>
#include <core/type_util.hpp>

#include <registration/DenormalizationRegistration.hpp>
#include <registration/NormalizationRegistration.hpp>

namespace grammar {

/**
 * \brief
 * Context free grammar without epsilon rules in Chomsky hierarchy or type 2 in Chomsky hierarchy. Generates context free languages.

 * \details
 * Definition is similar to all common definitions of context free grammars.
 * G = (N, T, P, S),
 * N (NonterminalAlphabet) = nonempty finite set of nonterminal symbols,
 * T (TerminalAlphabet) = finite set of terminal symbols - having this empty won't let grammar do much though,
 * P = set of production rules of the form A -> B, where A \in N and B \in ( N \cup T )+,
 * S (InitialSymbol) = initial nonterminal symbol,
 *
 * \tparam TerminalSymbolType used for the terminal alphabet of the grammar.
 * \tparam NonterminalSymbolType used for the nonterminal alphabet, and the initial symbol of the grammar.
 */
template <class TerminalSymbolType = DefaultSymbolType, class NonterminalSymbolType = DefaultSymbolType>
class EpsilonFreeCFG final : public core::Components<EpsilonFreeCFG<TerminalSymbolType, NonterminalSymbolType>, ext::set<TerminalSymbolType>, module::Set, component::TerminalAlphabet, ext::set<NonterminalSymbolType>, module::Set, component::NonterminalAlphabet, NonterminalSymbolType, module::Value, component::InitialSymbol> {
    /**
     * Rules function as mapping from nonterminal symbol on the left hand side to a set of sequences of terminal and nonterminal symbols.
     */
    ext::map<NonterminalSymbolType, ext::set<ext::vector<ext::variant<TerminalSymbolType, NonterminalSymbolType>>>> rules;

    /**
     * Boolean signaling whether grammar generates empty string or don't.
     */
    bool generatesEpsilon;

public:
    /**
     * \brief Creates a new instance of the grammar with a concrete initial symbol.
     *
     * \param initialSymbol the initial symbol of the grammar
     */
    explicit EpsilonFreeCFG(NonterminalSymbolType initialSymbol);

    /**
     * \brief Creates a new instance of the grammar with a concrete nonterminal alphabet, terminal alphabet and initial symbol.
     *
     * \param nonTerminalSymbols the initial nonterminal alphabet
     * \param terminalSymbols the initial terminal alphabet
     * \param initialSymbol the initial symbol of the grammar
     */
    explicit EpsilonFreeCFG(ext::set<NonterminalSymbolType> nonterminalAlphabet, ext::set<TerminalSymbolType> terminalAlphabet, NonterminalSymbolType initialSymbol);

    /**
     * \brief Add a new rule of a grammar.
     *
     * \details The rule is in a form of A -> B, where A, \in N and B \in (N \cup T )+.
     *
     * \param leftHandSide the left hand side of the rule
     * \param rightHandSide the right hand side of the rule
     *
     * \returns true if the rule was indeed added, false othervise
     */
    bool addRule(NonterminalSymbolType leftHandSide, ext::vector<ext::variant<TerminalSymbolType, NonterminalSymbolType>> rightHandSide);

    /**
     * \brief Add new rules of a grammar.
     *
     * \details The rules are in form of A -> B | C | ..., where A \in N and B, C ... \in (N \cup T)+.
     *
     * \param leftHandSide the left hand side of the rule
     * \param rightHandSide a set of right hand sides of the rule
     */
    void addRules(NonterminalSymbolType leftHandSide, ext::set<ext::vector<ext::variant<TerminalSymbolType, NonterminalSymbolType>>> rightHandSide);

    /**
     * Get rules of the grammar.
     *
     * \returns rules of the grammar
     */
    const ext::map<NonterminalSymbolType, ext::set<ext::vector<ext::variant<TerminalSymbolType, NonterminalSymbolType>>>>& getRules() const&;

    /**
     * Get rules of the grammar.
     *
     * \returns rules of the grammar
     */
    ext::map<NonterminalSymbolType, ext::set<ext::vector<ext::variant<TerminalSymbolType, NonterminalSymbolType>>>>&& getRules() &&;

    /**
     * Remove a rule of a grammar in form of A -> B, where A \in N and B \in (N \cup T)+.
     *
     * \param leftHandSide the left hand side of the rule
     * \param rightHandSide the right hand side of the rule
     *
     * \returns true if the rule was indeed removed, false othervise
     */
    bool removeRule(const NonterminalSymbolType& leftHandSide, const ext::vector<ext::variant<TerminalSymbolType, NonterminalSymbolType>>& rightHandSide);

    /**
     * Getter of initial symbol.
     *
     * \returns the initial symbol of the grammar
     */
    const NonterminalSymbolType& getInitialSymbol() const&
    {
        return this->template accessComponent<component::InitialSymbol>().get();
    }

    /**
     * Getter of initial symbol.
     *
     * \returns the initial symbol of the grammar
     */
    NonterminalSymbolType&& getInitialSymbol() &&
    {
        return std::move(this->template accessComponent<component::InitialSymbol>().get());
    }

    /**
     * Setter of initial symbol.
     *
     * \param symbol new initial symbol of the grammar
     *
     * \returns true if the initial symbol was indeed changed
     */
    bool setInitialSymbol(NonterminalSymbolType symbol)
    {
        return this->template accessComponent<component::InitialSymbol>().set(std::move(symbol));
    }

    /**
     * Getter of nonterminal alphabet.
     *
     * \returns the nonterminal alphabet of the grammar
     */
    const ext::set<NonterminalSymbolType>& getNonterminalAlphabet() const&
    {
        return this->template accessComponent<component::NonterminalAlphabet>().get();
    }

    /**
     * Getter of nonterminal alphabet.
     *
     * \returns the nonterminal alphabet of the grammar
     */
    ext::set<NonterminalSymbolType>&& getNonterminalAlphabet() &&
    {
        return std::move(this->template accessComponent<component::NonterminalAlphabet>().get());
    }

    /**
     * Adder of nonterminal symbol.
     *
     * \param symbol the new symbol to be added to nonterminal alphabet
     *
     * \returns true if the symbol was indeed added
     */
    bool addNonterminalSymbol(NonterminalSymbolType symbol)
    {
        return this->template accessComponent<component::NonterminalAlphabet>().add(std::move(symbol));
    }

    /**
     * Setter of nonterminal alphabet.
     *
     * \param symbols completely new nonterminal alphabet
     */
    void setNonterminalAlphabet(ext::set<NonterminalSymbolType> symbols)
    {
        this->template accessComponent<component::NonterminalAlphabet>().set(std::move(symbols));
    }

    /**
     * Getter of terminal alphabet.
     *
     * \returns the terminal alphabet of the grammar
     */
    const ext::set<TerminalSymbolType>& getTerminalAlphabet() const&
    {
        return this->template accessComponent<component::TerminalAlphabet>().get();
    }

    /**
     * Getter of terminal alphabet.
     *
     * \returns the terminal alphabet of the grammar
     */
    ext::set<TerminalSymbolType>&& getTerminalAlphabet() &&
    {
        return std::move(this->template accessComponent<component::TerminalAlphabet>().get());
    }

    /**
     * Adder of terminal symbol.
     *
     * \param symbol the new symbol tuo be added to nonterminal alphabet
     *
     * \returns true if the symbol was indeed added
     */
    bool addTerminalSymbol(TerminalSymbolType symbol)
    {
        return this->template accessComponent<component::TerminalAlphabet>().add(std::move(symbol));
    }

    /**
     * Setter of terminal alphabet.
     *
     * \param symbol completely new nontemrinal alphabet
     */
    void setTerminalAlphabet(ext::set<TerminalSymbolType> symbols)
    {
        this->template accessComponent<component::TerminalAlphabet>().set(std::move(symbols));
    }

    /**
     * Sets sign representing that grammar generates or doesn't generate empty word.
     *
     * \param genEps sign representing the posibility of generating empty string from the grammar
     */
    void setGeneratesEpsilon(bool genEps);

    /**
     * Gets sign representing that grammar generates or doesn't generate empty word.
     *
     * \returns sign representing the posibility of generating empty string from the grammar
     */
    bool getGeneratesEpsilon() const;

    /**
     * The three way comparison implementation
     *
     * \param other the other instance
     *
     * \returns the ordering between this object and the @p other.
     */
    auto operator<=>(const EpsilonFreeCFG& other) const
    {
        return std::tie(getTerminalAlphabet(), getNonterminalAlphabet(), getInitialSymbol(), rules) <=> std::tie(other.getTerminalAlphabet(), other.getNonterminalAlphabet(), other.getInitialSymbol(), other.rules);
    }

    /**
     * The equality comparison implementation.
     *
     * \param other the other object to compare with.
     *
     * \returns true if this and other objects are equal, false othervise
     */
    bool operator==(const EpsilonFreeCFG& other) const
    {
        return std::tie(getTerminalAlphabet(), getNonterminalAlphabet(), getInitialSymbol(), rules) == std::tie(other.getTerminalAlphabet(), other.getNonterminalAlphabet(), other.getInitialSymbol(), other.rules);
    }

    /**
     * Print this object as raw representation to ostream.
     *
     * \param out ostream where to print
     * \param instance object to print
     *
     * \returns modified output stream
     */
    friend ext::ostream& operator<<(ext::ostream& out, const EpsilonFreeCFG& instance)
    {
        return out << "(EpsilonFreeCFG"
                   << " nonterminalAlphabet = " << instance.getNonterminalAlphabet()
                   << " terminalAlphabet = " << instance.getTerminalAlphabet()
                   << " initialSymbol = " << instance.getInitialSymbol()
                   << " rules = " << instance.getRules()
                   << " generatesEpsilon = " << instance.getGeneratesEpsilon()
                   << ")";
    }
};

template <class TerminalSymbolType, class NonterminalSymbolType>
EpsilonFreeCFG<TerminalSymbolType, NonterminalSymbolType>::EpsilonFreeCFG(NonterminalSymbolType initialSymbol)
    : EpsilonFreeCFG(ext::set<NonterminalSymbolType>{initialSymbol}, ext::set<TerminalSymbolType>(), initialSymbol)
{
}

template <class TerminalSymbolType, class NonterminalSymbolType>
EpsilonFreeCFG<TerminalSymbolType, NonterminalSymbolType>::EpsilonFreeCFG(ext::set<NonterminalSymbolType> nonterminalAlphabet, ext::set<TerminalSymbolType> terminalAlphabet, NonterminalSymbolType initialSymbol)
    : core::Components<EpsilonFreeCFG, ext::set<TerminalSymbolType>, module::Set, component::TerminalAlphabet, ext::set<NonterminalSymbolType>, module::Set, component::NonterminalAlphabet, NonterminalSymbolType, module::Value, component::InitialSymbol>(std::move(terminalAlphabet), std::move(nonterminalAlphabet), std::move(initialSymbol))
    , generatesEpsilon(false)
{
}

template <class TerminalSymbolType, class NonterminalSymbolType>
bool EpsilonFreeCFG<TerminalSymbolType, NonterminalSymbolType>::addRule(NonterminalSymbolType leftHandSide, ext::vector<ext::variant<TerminalSymbolType, NonterminalSymbolType>> rightHandSide)
{
    if (rightHandSide.empty())
        throw GrammarException("Epsilon rule is not allowed");

    if (!getNonterminalAlphabet().count(leftHandSide))
        throw GrammarException("Rule must rewrite nonterminal symbol");

    for (const ext::variant<TerminalSymbolType, NonterminalSymbolType>& symbol : rightHandSide)
        if (!getTerminalAlphabet().count(symbol) && !getNonterminalAlphabet().count(symbol))
            throw GrammarException("Symbol \"" + ext::to_string(symbol) + "\" is not neither terminal nor nonterminal symbol");

    return rules[std::move(leftHandSide)].insert(std::move(rightHandSide)).second;
}

template <class TerminalSymbolType, class NonterminalSymbolType>
void EpsilonFreeCFG<TerminalSymbolType, NonterminalSymbolType>::addRules(NonterminalSymbolType leftHandSide, ext::set<ext::vector<ext::variant<TerminalSymbolType, NonterminalSymbolType>>> rightHandSide)
{
    if (!getNonterminalAlphabet().count(leftHandSide))
        throw GrammarException("Rule must rewrite nonterminal symbol");

    for (const ext::vector<ext::variant<TerminalSymbolType, NonterminalSymbolType>>& rhs : rightHandSide) {
        if (rhs.empty())
            throw GrammarException("Epsilon rule is not allowed");

        for (const ext::variant<TerminalSymbolType, NonterminalSymbolType>& symbol : rhs)
            if (!getTerminalAlphabet().count(symbol) && !getNonterminalAlphabet().count(symbol))
                throw GrammarException("Symbol \"" + ext::to_string(symbol) + "\" is not neither terminal nor nonterminal symbol");
    }

    rules[std::move(leftHandSide)].insert(ext::make_mover(rightHandSide).begin(), ext::make_mover(rightHandSide).end());
}

template <class TerminalSymbolType, class NonterminalSymbolType>
const ext::map<NonterminalSymbolType, ext::set<ext::vector<ext::variant<TerminalSymbolType, NonterminalSymbolType>>>>& EpsilonFreeCFG<TerminalSymbolType, NonterminalSymbolType>::getRules() const&
{
    return rules;
}

template <class TerminalSymbolType, class NonterminalSymbolType>
ext::map<NonterminalSymbolType, ext::set<ext::vector<ext::variant<TerminalSymbolType, NonterminalSymbolType>>>>&& EpsilonFreeCFG<TerminalSymbolType, NonterminalSymbolType>::getRules() &&
{
    return std::move(rules);
}

template <class TerminalSymbolType, class NonterminalSymbolType>
bool EpsilonFreeCFG<TerminalSymbolType, NonterminalSymbolType>::removeRule(const NonterminalSymbolType& leftHandSide, const ext::vector<ext::variant<TerminalSymbolType, NonterminalSymbolType>>& rightHandSide)
{
    return rules[leftHandSide].erase(rightHandSide);
}

template <class TerminalSymbolType, class NonterminalSymbolType>
void EpsilonFreeCFG<TerminalSymbolType, NonterminalSymbolType>::setGeneratesEpsilon(bool genEps)
{
    generatesEpsilon = genEps;
}

template <class TerminalSymbolType, class NonterminalSymbolType>
bool EpsilonFreeCFG<TerminalSymbolType, NonterminalSymbolType>::getGeneratesEpsilon() const
{
    return generatesEpsilon;
}

} /* namespace grammar */

namespace core {

/**
 * Helper class specifying constraints for the grammar's internal terminal alphabet component.
 *
 * \tparam TerminalSymbolType used for the terminal alphabet of the grammar.
 * \tparam NonterminalSymbolType used for the nonterminal alphabet, and the initial symbol of the grammar.
 */
template <class TerminalSymbolType, class NonterminalSymbolType>
class SetConstraint<grammar::EpsilonFreeCFG<TerminalSymbolType, NonterminalSymbolType>, TerminalSymbolType, component::TerminalAlphabet> {
public:
    /**
     * Returns true if the terminal symbol is still used in some rule of the grammar.
     *
     * \param grammar the tested grammar
     * \param symbol the tested symbol
     *
     * \returns true if the symbol is used, false othervise
     */
    static bool used(const grammar::EpsilonFreeCFG<TerminalSymbolType, NonterminalSymbolType>& grammar, const TerminalSymbolType& symbol)
    {
        for (const std::pair<const NonterminalSymbolType, ext::set<ext::vector<ext::variant<TerminalSymbolType, NonterminalSymbolType>>>>& rule : grammar.getRules())
            for (const ext::vector<ext::variant<TerminalSymbolType, NonterminalSymbolType>>& rhs : rule.second)
                if (std::find(rhs.begin(), rhs.end(), symbol) != rhs.end())
                    return true;

        return false;
    }

    /**
     * Returns true as all terminal symbols are possibly available to be terminal symbols.
     *
     * \param grammar the tested grammar
     * \param symbol the tested symbol
     *
     * \returns true
     */
    static bool available(const grammar::EpsilonFreeCFG<TerminalSymbolType, NonterminalSymbolType>&, const TerminalSymbolType&)
    {
        return true;
    }

    /**
     * Throws runtime exception if the symbol requested to be terminal symbol is already in nonterminal alphabet.
     *
     * \param grammar the tested grammar
     * \param symbol the tested symbol
     *
     * \throws grammar::GrammarException of the tested symbol is in nonterminal alphabet
     */
    static void valid(const grammar::EpsilonFreeCFG<TerminalSymbolType, NonterminalSymbolType>& grammar, const TerminalSymbolType& symbol)
    {
        if (grammar.template accessComponent<component::NonterminalAlphabet>().get().count(ext::poly_comp(symbol)))
            throw grammar::GrammarException("Symbol " + ext::to_string(symbol) + " cannot be in the terminal alphabet since it is already in the nonterminal alphabet.");
    }
};

/**
 * Helper class specifying constraints for the grammar's internal nonterminal alphabet component.
 *
 * \tparam TerminalSymbolType used for the terminal alphabet of the grammar.
 * \tparam NonterminalSymbolType used for the nonterminal alphabet, and the initial symbol of the grammar.
 */
template <class TerminalSymbolType, class NonterminalSymbolType>
class SetConstraint<grammar::EpsilonFreeCFG<TerminalSymbolType, NonterminalSymbolType>, NonterminalSymbolType, component::NonterminalAlphabet> {
public:
    /**
     * Returns true if the nonterminal symbol is still used in some rule of the grammar or if it is the initial symbol of the grammar.
     *
     * \param grammar the tested grammar
     * \param symbol the tested symbol
     *
     * \returns true if the symbol is used, false othervise
     */
    static bool used(const grammar::EpsilonFreeCFG<TerminalSymbolType, NonterminalSymbolType>& grammar, const NonterminalSymbolType& symbol)
    {
        for (const std::pair<const NonterminalSymbolType, ext::set<ext::vector<ext::variant<TerminalSymbolType, NonterminalSymbolType>>>>& rule : grammar.getRules()) {
            if (rule.first == symbol)
                return true;

            for (const ext::vector<ext::variant<TerminalSymbolType, NonterminalSymbolType>>& rhs : rule.second)
                if (std::find(rhs.begin(), rhs.end(), symbol) != rhs.end())
                    return true;
        }

        return grammar.template accessComponent<component::InitialSymbol>().get() == symbol;
    }

    /**
     * Returns true as all terminal symbols are possibly available to be nonterminal symbols.
     *
     * \param grammar the tested grammar
     * \param symbol the tested symbol
     *
     * \returns true
     */
    static bool available(const grammar::EpsilonFreeCFG<TerminalSymbolType, NonterminalSymbolType>&, const NonterminalSymbolType&)
    {
        return true;
    }

    /**
     * Throws runtime exception if the symbol requested to be nonterminal symbol is already in terminal alphabet.
     *
     * \param grammar the tested grammar
     * \param symbol the tested symbol
     *
     * \throws grammar::GrammarException of the tested symbol is in nonterminal alphabet
     */
    static void valid(const grammar::EpsilonFreeCFG<TerminalSymbolType, NonterminalSymbolType>& grammar, const NonterminalSymbolType& symbol)
    {
        if (grammar.template accessComponent<component::TerminalAlphabet>().get().count(ext::poly_comp(symbol)))
            throw grammar::GrammarException("Symbol " + ext::to_string(symbol) + " cannot be in the nonterminal alphabet since it is already in the terminal alphabet.");
    }
};

/**
 * Helper class specifying constraints for the grammar's internal initial symbol element.
 *
 * \tparam TerminalSymbolType used for the terminal alphabet of the grammar.
 * \tparam NonterminalSymbolType used for the nonterminal alphabet, and the initial symbol of the grammar.
 */
template <class TerminalSymbolType, class NonterminalSymbolType>
class ElementConstraint<grammar::EpsilonFreeCFG<TerminalSymbolType, NonterminalSymbolType>, NonterminalSymbolType, component::InitialSymbol> {
public:
    /**
     * Returns true if the symbol requested to be initial is available in nonterminal alphabet.
     *
     * \param grammar the tested grammar
     * \param symbol the tested symbol
     *
     * \returns true if the tested symbol is in nonterminal alphabet
     */
    static bool available(const grammar::EpsilonFreeCFG<TerminalSymbolType, NonterminalSymbolType>& grammar, const NonterminalSymbolType& symbol)
    {
        return grammar.template accessComponent<component::NonterminalAlphabet>().get().count(symbol);
    }

    /**
     * All symbols are valid as initial symbols.
     *
     * \param grammar the tested grammar
     * \param symbol the tested symbol
     */
    static void valid(const grammar::EpsilonFreeCFG<TerminalSymbolType, NonterminalSymbolType>&, const NonterminalSymbolType&)
    {
    }
};

/**
 * Helper for normalisation of types specified by templates used as internal datatypes of symbols.
 *
 * \returns new instance of the grammar with default template parameters or unmodified instance if the template parameters were already default ones
 */
template <class TerminalSymbolType, class NonterminalSymbolType>
struct type_util<grammar::EpsilonFreeCFG<TerminalSymbolType, NonterminalSymbolType>> {
    static grammar::EpsilonFreeCFG<TerminalSymbolType, NonterminalSymbolType> denormalize(grammar::EpsilonFreeCFG<>&& value)
    {
        ext::set<NonterminalSymbolType> nonterminals = alphabet::SymbolDenormalize::denormalizeAlphabet<NonterminalSymbolType>(std::move(value).getNonterminalAlphabet());
        ext::set<TerminalSymbolType> terminals = alphabet::SymbolDenormalize::denormalizeAlphabet<TerminalSymbolType>(std::move(value).getTerminalAlphabet());
        NonterminalSymbolType initialSymbol = alphabet::SymbolDenormalize::denormalizeSymbol<NonterminalSymbolType>(std::move(value).getInitialSymbol());

        grammar::EpsilonFreeCFG<TerminalSymbolType, NonterminalSymbolType> res(std::move(nonterminals), std::move(terminals), std::move(initialSymbol));

        for (std::pair<NonterminalSymbolType, ext::set<ext::vector<ext::variant<TerminalSymbolType, NonterminalSymbolType>>>>&& rule : ext::make_mover(std::move(value).getRules())) {

            ext::set<ext::vector<ext::variant<DefaultSymbolType, DefaultSymbolType>>> rhs;
            for (ext::vector<ext::variant<TerminalSymbolType, NonterminalSymbolType>>&& target : ext::make_mover(rule.second))
                rhs.insert(alphabet::SymbolDenormalize::denormalizeVariantSymbols<TerminalSymbolType, NonterminalSymbolType>(std::move(target)));

            DefaultSymbolType lhs = alphabet::SymbolDenormalize::denormalizeSymbol<NonterminalSymbolType>(std::move(rule.first));

            res.addRules(std::move(lhs), std::move(rhs));
        }

        res.setGeneratesEpsilon(value.getGeneratesEpsilon());

        return res;
    }

    static grammar::EpsilonFreeCFG<> normalize(grammar::EpsilonFreeCFG<TerminalSymbolType, NonterminalSymbolType>&& value)
    {
        ext::set<DefaultSymbolType> nonterminals = alphabet::SymbolNormalize::normalizeAlphabet(std::move(value).getNonterminalAlphabet());
        ext::set<DefaultSymbolType> terminals = alphabet::SymbolNormalize::normalizeAlphabet(std::move(value).getTerminalAlphabet());
        DefaultSymbolType initialSymbol = alphabet::SymbolNormalize::normalizeSymbol(std::move(value).getInitialSymbol());

        grammar::EpsilonFreeCFG<> res(std::move(nonterminals), std::move(terminals), std::move(initialSymbol));

        for (std::pair<NonterminalSymbolType, ext::set<ext::vector<ext::variant<TerminalSymbolType, NonterminalSymbolType>>>>&& rule : ext::make_mover(std::move(value).getRules())) {

            ext::set<ext::vector<ext::variant<DefaultSymbolType, DefaultSymbolType>>> rhs;
            for (ext::vector<ext::variant<TerminalSymbolType, NonterminalSymbolType>>&& target : ext::make_mover(rule.second))
                rhs.insert(alphabet::SymbolNormalize::normalizeVariantSymbols(std::move(target)));

            DefaultSymbolType lhs = alphabet::SymbolNormalize::normalizeSymbol(std::move(rule.first));

            res.addRules(std::move(lhs), std::move(rhs));
        }

        res.setGeneratesEpsilon(value.getGeneratesEpsilon());

        return res;
    }

    static std::unique_ptr<type_details_base> type(const grammar::EpsilonFreeCFG<TerminalSymbolType, NonterminalSymbolType>& arg)
    {
        core::unique_ptr_set<type_details_base> subTypesTerminals;
        for (const TerminalSymbolType& item : arg.getTerminalAlphabet())
            subTypesTerminals.insert(type_util<TerminalSymbolType>::type(item));

        core::unique_ptr_set<type_details_base> subTypesNonterminals;
        for (const NonterminalSymbolType& item : arg.getNonterminalAlphabet())
            subTypesNonterminals.insert(type_util<NonterminalSymbolType>::type(item));

        std::vector<std::unique_ptr<type_details_base>> sub_types_vec;
        sub_types_vec.push_back(type_details_variant_type::make_variant(std::move(subTypesTerminals)));
        sub_types_vec.push_back(type_details_variant_type::make_variant(std::move(subTypesNonterminals)));
        return std::make_unique<type_details_template>("grammar::EpsilonFreeCFG", std::move(sub_types_vec));
    }
};

template <class TerminalSymbolType, class NonterminalSymbolType>
struct type_details_retriever<grammar::EpsilonFreeCFG<TerminalSymbolType, NonterminalSymbolType>> {
    static std::unique_ptr<type_details_base> get()
    {
        std::vector<std::unique_ptr<type_details_base>> sub_types_vec;
        sub_types_vec.push_back(type_details_retriever<TerminalSymbolType>::get());
        sub_types_vec.push_back(type_details_retriever<NonterminalSymbolType>::get());
        return std::make_unique<type_details_template>("grammar::EpsilonFreeCFG", std::move(sub_types_vec));
    }
};

} /* namespace core */

extern template class grammar::EpsilonFreeCFG<>;
extern template class abstraction::ValueHolder<grammar::EpsilonFreeCFG<>>;
extern template const grammar::EpsilonFreeCFG<>& abstraction::retrieveValue<const grammar::EpsilonFreeCFG<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
extern template class registration::DenormalizationRegisterImpl<const grammar::EpsilonFreeCFG<>&>;
extern template class registration::NormalizationRegisterImpl<grammar::EpsilonFreeCFG<>>;
