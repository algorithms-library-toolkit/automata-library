#include "LG.h"

#include <registration/CastRegistration.hpp>
#include <registration/ValuePrinterRegistration.hpp>

template class grammar::LG<>;
template class abstraction::ValueHolder<grammar::LG<>>;
template const grammar::LG<>& abstraction::retrieveValue<const grammar::LG<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const grammar::LG<>&>;
template class registration::NormalizationRegisterImpl<grammar::LG<>>;

namespace {

auto valuePrinter = registration::ValuePrinterRegister<grammar::LG<>>();

} /* namespace */
