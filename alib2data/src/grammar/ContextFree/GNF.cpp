#include "GNF.h"

#include <registration/CastRegistration.hpp>
#include <registration/ValuePrinterRegistration.hpp>

template class grammar::GNF<>;
template class abstraction::ValueHolder<grammar::GNF<>>;
template const grammar::GNF<>& abstraction::retrieveValue<const grammar::GNF<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const grammar::GNF<>&>;
template class registration::NormalizationRegisterImpl<grammar::GNF<>>;

namespace {

auto valuePrinter = registration::ValuePrinterRegister<grammar::GNF<>>();

} /* namespace */
