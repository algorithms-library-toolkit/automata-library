#include <registration/AlgoRegistration.hpp>
#include "AddRawRule.h"

namespace {

auto AddRawRuleCFG = registration::AbstractRegister<grammar::AddRawRule, bool, grammar::CFG<>&, DefaultSymbolType, ext::vector<ext::variant<DefaultSymbolType, DefaultSymbolType>>>(grammar::AddRawRule::addRawRule);
auto AddRawRuleEpsilonFreeCFG = registration::AbstractRegister<grammar::AddRawRule, bool, grammar::EpsilonFreeCFG<>&, DefaultSymbolType, ext::vector<ext::variant<DefaultSymbolType, DefaultSymbolType>>>(grammar::AddRawRule::addRawRule);
auto AddRawRuleGNF = registration::AbstractRegister<grammar::AddRawRule, bool, grammar::GNF<>&, DefaultSymbolType, ext::vector<ext::variant<DefaultSymbolType, DefaultSymbolType>>>(grammar::AddRawRule::addRawRule);
auto AddRawRuleCNF = registration::AbstractRegister<grammar::AddRawRule, bool, grammar::CNF<>&, DefaultSymbolType, ext::vector<ext::variant<DefaultSymbolType, DefaultSymbolType>>>(grammar::AddRawRule::addRawRule);
auto AddRawRuleLG = registration::AbstractRegister<grammar::AddRawRule, bool, grammar::LG<>&, DefaultSymbolType, ext::vector<ext::variant<DefaultSymbolType, DefaultSymbolType>>>(grammar::AddRawRule::addRawRule);
auto AddRawRuleLeftLG = registration::AbstractRegister<grammar::AddRawRule, bool, grammar::LeftLG<>&, DefaultSymbolType, ext::vector<ext::variant<DefaultSymbolType, DefaultSymbolType>>>(grammar::AddRawRule::addRawRule);
auto AddRawRuleLeftRG = registration::AbstractRegister<grammar::AddRawRule, bool, grammar::LeftRG<>&, DefaultSymbolType, ext::vector<ext::variant<DefaultSymbolType, DefaultSymbolType>>>(grammar::AddRawRule::addRawRule);
auto AddRawRuleRightLG = registration::AbstractRegister<grammar::AddRawRule, bool, grammar::RightLG<>&, DefaultSymbolType, ext::vector<ext::variant<DefaultSymbolType, DefaultSymbolType>>>(grammar::AddRawRule::addRawRule);
auto AddRawRuleRightRG = registration::AbstractRegister<grammar::AddRawRule, bool, grammar::RightRG<>&, DefaultSymbolType, ext::vector<ext::variant<DefaultSymbolType, DefaultSymbolType>>>(grammar::AddRawRule::addRawRule);

} /* namespace */
