#include <object/Object.h>
#include "UnrankedTree.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<tree::UnrankedTree<>>();
auto xmlRead = registration::XmlReaderRegister<tree::UnrankedTree<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, tree::UnrankedTree<>>();

} /* namespace */
