#include <object/Object.h>
#include "UnrankedNonlinearPattern.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<tree::UnrankedNonlinearPattern<>>();
auto xmlRead = registration::XmlReaderRegister<tree::UnrankedNonlinearPattern<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, tree::UnrankedNonlinearPattern<>>();

} /* namespace */
