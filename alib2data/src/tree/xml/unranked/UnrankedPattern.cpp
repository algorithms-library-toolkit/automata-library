#include <object/Object.h>
#include "UnrankedPattern.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<tree::UnrankedPattern<>>();
auto xmlRead = registration::XmlReaderRegister<tree::UnrankedPattern<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, tree::UnrankedPattern<>>();

} /* namespace */
