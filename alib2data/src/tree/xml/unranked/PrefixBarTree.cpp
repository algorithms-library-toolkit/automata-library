#include <object/Object.h>
#include "PrefixBarTree.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<tree::PrefixBarTree<>>();
auto xmlRead = registration::XmlReaderRegister<tree::PrefixBarTree<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, tree::PrefixBarTree<>>();

} /* namespace */
