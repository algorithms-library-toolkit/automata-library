#include <object/Object.h>
#include "PostfixRankedTree.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<tree::PostfixRankedTree<>>();
auto xmlRead = registration::XmlReaderRegister<tree::PostfixRankedTree<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, tree::PostfixRankedTree<>>();

} /* namespace */
