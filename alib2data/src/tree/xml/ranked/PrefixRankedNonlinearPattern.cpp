#include <object/Object.h>
#include "PrefixRankedNonlinearPattern.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<tree::PrefixRankedNonlinearPattern<>>();
auto xmlRead = registration::XmlReaderRegister<tree::PrefixRankedNonlinearPattern<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, tree::PrefixRankedNonlinearPattern<>>();

} /* namespace */
