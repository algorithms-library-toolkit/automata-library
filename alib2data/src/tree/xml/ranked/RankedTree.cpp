#include <object/Object.h>
#include "RankedTree.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<tree::RankedTree<>>();
auto xmlRead = registration::XmlReaderRegister<tree::RankedTree<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, tree::RankedTree<>>();

} /* namespace */
