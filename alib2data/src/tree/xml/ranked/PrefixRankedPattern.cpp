#include <object/Object.h>
#include "PrefixRankedPattern.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<tree::PrefixRankedPattern<>>();
auto xmlRead = registration::XmlReaderRegister<tree::PrefixRankedPattern<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, tree::PrefixRankedPattern<>>();

} /* namespace */
