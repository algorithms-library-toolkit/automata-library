#include <object/Object.h>
#include "PrefixRankedBarPattern.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<tree::PrefixRankedBarPattern<>>();
auto xmlRead = registration::XmlReaderRegister<tree::PrefixRankedBarPattern<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, tree::PrefixRankedBarPattern<>>();

} /* namespace */
