#include <object/Object.h>
#include "RankedNonlinearPattern.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<tree::RankedNonlinearPattern<>>();
auto xmlRead = registration::XmlReaderRegister<tree::RankedNonlinearPattern<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, tree::RankedNonlinearPattern<>>();

} /* namespace */
