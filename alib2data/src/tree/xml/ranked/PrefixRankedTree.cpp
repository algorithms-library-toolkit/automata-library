#include <object/Object.h>
#include "PrefixRankedTree.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<tree::PrefixRankedTree<>>();
auto xmlRead = registration::XmlReaderRegister<tree::PrefixRankedTree<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, tree::PrefixRankedTree<>>();

} /* namespace */
