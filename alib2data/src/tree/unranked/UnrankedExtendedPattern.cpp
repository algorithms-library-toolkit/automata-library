#include "UnrankedExtendedPattern.h"

#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>
#include <registration/ValuePrinterRegistration.hpp>

template class tree::UnrankedExtendedPattern<>;
template class abstraction::ValueHolder<tree::UnrankedExtendedPattern<>>;
template const tree::UnrankedExtendedPattern<>& abstraction::retrieveValue<const tree::UnrankedExtendedPattern<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const tree::UnrankedExtendedPattern<>&>;
template class registration::NormalizationRegisterImpl<tree::UnrankedExtendedPattern<>>;

namespace {

auto components = registration::ComponentRegister<tree::UnrankedExtendedPattern<>>();

auto valuePrinter = registration::ValuePrinterRegister<tree::UnrankedExtendedPattern<>>();

auto UnrankedPatternFromRankedPattern = registration::CastRegister<tree::UnrankedExtendedPattern<>, tree::UnrankedPattern<>>();

} /* namespace */
