#include "UnorderedUnrankedPattern.h"

#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>
#include <registration/ValuePrinterRegistration.hpp>

template class tree::UnorderedUnrankedPattern<>;
template class abstraction::ValueHolder<tree::UnorderedUnrankedPattern<>>;
template const tree::UnorderedUnrankedPattern<>& abstraction::retrieveValue<const tree::UnorderedUnrankedPattern<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const tree::UnorderedUnrankedPattern<>&>;
template class registration::NormalizationRegisterImpl<tree::UnorderedUnrankedPattern<>>;

namespace {

auto components = registration::ComponentRegister<tree::UnorderedUnrankedPattern<>>();

auto valuePrinter = registration::ValuePrinterRegister<tree::UnorderedUnrankedPattern<>>();

auto UnrankedUnorderedPatternFromUnrankedPattern = registration::CastRegister<tree::UnorderedUnrankedPattern<>, tree::UnrankedPattern<>>();

} /* namespace */
