#include "UnrankedNonlinearPattern.h"

#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>
#include <registration/ValuePrinterRegistration.hpp>

template class tree::UnrankedNonlinearPattern<>;
template class abstraction::ValueHolder<tree::UnrankedNonlinearPattern<>>;
template const tree::UnrankedNonlinearPattern<>& abstraction::retrieveValue<const tree::UnrankedNonlinearPattern<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const tree::UnrankedNonlinearPattern<>&>;
template class registration::NormalizationRegisterImpl<tree::UnrankedNonlinearPattern<>>;

namespace {

auto components = registration::ComponentRegister<tree::UnrankedNonlinearPattern<>>();

auto valuePrinter = registration::ValuePrinterRegister<tree::UnrankedNonlinearPattern<>>();

} /* namespace */
