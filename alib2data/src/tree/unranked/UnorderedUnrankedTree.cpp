#include "UnorderedUnrankedTree.h"

#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>
#include <registration/ValuePrinterRegistration.hpp>

template class tree::UnorderedUnrankedTree<>;
template class abstraction::ValueHolder<tree::UnorderedUnrankedTree<>>;
template const tree::UnorderedUnrankedTree<>& abstraction::retrieveValue<const tree::UnorderedUnrankedTree<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const tree::UnorderedUnrankedTree<>&>;
template class registration::NormalizationRegisterImpl<tree::UnorderedUnrankedTree<>>;

namespace {

auto components = registration::ComponentRegister<tree::UnorderedUnrankedTree<>>();

auto valuePrinter = registration::ValuePrinterRegister<tree::UnorderedUnrankedTree<>>();

auto UnrankedUnorderedTreeFromUnrankedTree = registration::CastRegister<tree::UnorderedUnrankedTree<>, tree::UnrankedTree<>>();

} /* namespace */
