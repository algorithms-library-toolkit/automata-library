#include "PrefixBarTree.h"

#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>
#include <registration/ValuePrinterRegistration.hpp>

template class tree::PrefixBarTree<>;
template class abstraction::ValueHolder<tree::PrefixBarTree<>>;
template const tree::PrefixBarTree<>& abstraction::retrieveValue<const tree::PrefixBarTree<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const tree::PrefixBarTree<>&>;
template class registration::NormalizationRegisterImpl<tree::PrefixBarTree<>>;

namespace {

auto components = registration::ComponentRegister<tree::PrefixBarTree<>>();

auto valuePrinter = registration::ValuePrinterRegister<tree::PrefixBarTree<>>();

auto PrefixBarTreeFromUnrankedTree = registration::CastRegister<tree::PrefixBarTree<>, tree::UnrankedTree<>>();

auto LinearStringFromPrefixBarTree = registration::CastRegister<string::LinearString<>, tree::PrefixBarTree<>>();

} /* namespace */
