/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <common/DefaultSymbolType.h>

namespace tree {

template <class SymbolType = DefaultSymbolType>
class UnrankedExtendedPattern;

} /* namespace tree */


#include <ext/algorithm>
#include <ext/iostream>

#include <alib/set>
#include <alib/string>
#include <alib/tree>

#include <core/modules.hpp>

#include <tree/TreeException.h>
#include <tree/common/TreeAuxiliary.h>

#include <alphabet/common/SymbolDenormalize.h>
#include <alphabet/common/SymbolNormalize.h>
#include <core/type_details_base.hpp>
#include <core/type_util.hpp>
#include <tree/common/TreeDenormalize.h>
#include <tree/common/TreeNormalize.h>

#include <alphabet/Gap.h>
#include <alphabet/NodeWildcard.h>
#include "UnrankedPattern.h"

#include <registration/DenormalizationRegistration.hpp>
#include <registration/NormalizationRegistration.hpp>

namespace tree {

/**
 * \brief
 * Extended tree pattern represented in its natural representation. The representation is so called unranked, therefore it consists of unranked symbols. Additionally the pattern contains a special wildcard symbol representing any subtree and other special wildcard symbols representing any label at the corresponding node in the match.
 *
 * \details
 * T = ( A, C, W \in A, N \in A ),
 * A (Alphabet) = finite set of symbols,
 * C (Content) = pattern in its natural representation
 * W (Wildcard) = special symbol representing any subtree
 * N (Node Wildcard) = special symbol representing any label
 *
 * \tparam SymbolType used for the symbol of the alphabet
 */
template <class SymbolType>
class UnrankedExtendedPattern final : public core::Components<UnrankedExtendedPattern<SymbolType>, ext::set<SymbolType>, module::Set, component::GeneralAlphabet, SymbolType, module::Value, std::tuple<component::NodeWildcardSymbol, component::SubtreeWildcardSymbol, component::SubtreeGapSymbol>> {
    /**
     * Natural representation of the pattern content.
     */
    ext::tree<SymbolType> m_content;

    /**
     * Checks that symbols of the pattern are present in the alphabet.
     *
     * \throws TreeException when some symbols of the pattern representation are not present in the alphabet
     *
     * \param data the pattern in its natural representation
     */
    void checkAlphabet(const ext::tree<SymbolType>& data) const;

public:
    /**
     * \brief Creates a new instance of the pattern with concrete alphabet, content, and wildcard.
     *
     * \param subtreeWildcard the wildcard symbol
     * \param subtreeGap effectively wildcard^*
     * \param alphabet the initial alphabet of the pattern
     * \param pattern the initial content in it's natural representation
     */
    explicit UnrankedExtendedPattern(SymbolType subtreeWildcard, SymbolType subtreeGap, SymbolType nodeWildcard, ext::set<SymbolType> alphabet, ext::tree<SymbolType> pattern);

    /**
     * \brief Creates a new instance of the pattern with concrete content and wildcard. The alphabet is deduced from the content.
     *
     * \param subtreeWildcard the wildcard symbol
     * \param subtreeGap effectively wildcard^*
     * \param pattern the initial content in it's natural representation
     */
    explicit UnrankedExtendedPattern(SymbolType subtreeWildcard, SymbolType subtreeGap, SymbolType nodeWildcard, ext::tree<SymbolType> pattern);

    /**
     * \brief Creates a new instance of the pattern based on RankedPattern, the alphabet is created from the content of the RankedPattern.
     *
     * \param other the pattern represented as RankedNonlinearPattern
     */
    explicit UnrankedExtendedPattern(const UnrankedPattern<SymbolType>& other);

    /**
     * Getter of the alphabet.
     *
     * \returns the alphabet of the pattern
     */
    const ext::set<SymbolType>& getAlphabet() const&
    {
        return this->template accessComponent<component::GeneralAlphabet>().get();
    }

    /**
     * Getter of the alphabet.
     *
     * \returns the alphabet of the pattern
     */
    ext::set<SymbolType>&& getAlphabet() &&
    {
        return std::move(this->template accessComponent<component::GeneralAlphabet>().get());
    }

    /**
     * Adder of an alphabet symbols.
     *
     * \param symbols the new symbols to be added to the alphabet
     */
    void extendAlphabet(const ext::set<SymbolType>& symbols)
    {
        this->template accessComponent<component::GeneralAlphabet>().add(symbols);
    }

    /**
     * Getter of the node wildcards.
     *
     * \returns the node wildcards of the pattern
     */
    const SymbolType& getNodeWildcard() const&
    {
        return this->template accessComponent<component::NodeWildcardSymbol>().get();
    }

    /**
     * Getter of the node wildcards.
     *
     * \returns the node wildcards of the pattern
     */
    SymbolType&& getNodeWildcard() &&
    {
        return std::move(this->template accessComponent<component::NodeWildcardSymbol>().get());
    }

    /**
     * Getter of the wildcard symbol.
     *
     * \returns the wildcard symbol of the pattern
     */
    const SymbolType& getSubtreeWildcard() const&
    {
        return this->template accessComponent<component::SubtreeWildcardSymbol>().get();
    }

    /**
     * Getter of the wildcard symbol.
     *
     * \returns the wildcard symbol of the pattern
     */
    SymbolType&& getSubtreeWildcard() &&
    {
        return std::move(this->template accessComponent<component::SubtreeWildcardSymbol>().get());
    }

    /**
     * Getter of the wildcard symbol.
     *
     * \returns the wildcard symbol of the pattern
     */
    const SymbolType& getSubtreeGap() const&
    {
        return this->template accessComponent<component::SubtreeGapSymbol>().get();
    }

    /**
     * Getter of the wildcard symbol.
     *
     * \returns the wildcard symbol of the pattern
     */
    SymbolType&& getSubtreeGap() &&
    {
        return std::move(this->template accessComponent<component::SubtreeGapSymbol>().get());
    }

    /**
     * Getter of the pattern representation.
     *
     * \return the natural representation of the pattern.
     */
    const ext::tree<SymbolType>& getContent() const&;

    /**
     * Getter of the pattern representation.
     *
     * \return the natural representation of the pattern.
     */
    ext::tree<SymbolType>&& getContent() &&;

    /**
     * Setter of the representation of the pattern.
     *
     * \throws TreeException in same situations as checkAlphabet
     *
     * \param pattern new representation of the pattern.
     */
    void setTree(ext::tree<SymbolType> pattern);

    /**
     * The three way comparison implementation
     *
     * \param other the other instance
     *
     * \returns the ordering between this object and the @p other.
     */
    auto operator<=>(const UnrankedExtendedPattern& other) const
    {
        return std::tie(m_content, getAlphabet(), getSubtreeWildcard(), getNodeWildcard()) <=> std::tie(other.m_content, other.getAlphabet(), other.getSubtreeWildcard(), other.getNodeWildcard());
    }

    /**
     * The equality comparison implementation.
     *
     * \param other the other object to compare with.
     *
     * \returns true if this and other objects are equal, false othervise
     */
    bool operator==(const UnrankedExtendedPattern& other) const
    {
        return std::tie(m_content, getAlphabet(), getSubtreeWildcard(), getNodeWildcard()) == std::tie(other.m_content, other.getAlphabet(), other.getSubtreeWildcard(), other.getNodeWildcard());
    }

    /**
     * Print this object as raw representation to ostream.
     *
     * \param out ostream where to print
     * \param instance object to print
     *
     * \returns modified output stream
     */
    friend ext::ostream& operator<<(ext::ostream& out, const UnrankedExtendedPattern& instance)
    {
        out << "(UnrankedExtendedPattern ";
        out << " alphabet = " << instance.getAlphabet();
        out << " content = " << instance.getContent();
        out << " subtreeWildcard = " << instance.getSubtreeWildcard();
        out << " nodeWildcard = " << instance.getNodeWildcard();
        out << " subtreeGap = " << instance.getSubtreeGap();
        out << ")";
        return out;
    }

    /**
     * Nice printer of the tree natural representation
     *
     * \param os the output stream to print to
     */
    void nicePrint(ext::ostream& os) const;
};

template <class SymbolType>
UnrankedExtendedPattern<SymbolType>::UnrankedExtendedPattern(SymbolType subtreeWildcard, SymbolType subtreeGap, SymbolType nodeWildcard, ext::set<SymbolType> alphabet, ext::tree<SymbolType> pattern)
    : core::Components<UnrankedExtendedPattern, ext::set<SymbolType>, module::Set, component::GeneralAlphabet, SymbolType, module::Value, std::tuple<component::NodeWildcardSymbol, component::SubtreeWildcardSymbol, component::SubtreeGapSymbol>>(std::move(alphabet), std::move(nodeWildcard), std::move(subtreeWildcard), std::move(subtreeGap))
    , m_content(std::move(pattern))
{
    checkAlphabet(m_content);
}

template <class SymbolType>
UnrankedExtendedPattern<SymbolType>::UnrankedExtendedPattern(SymbolType subtreeWildcard, SymbolType subtreeGap, SymbolType nodeWildcard, ext::tree<SymbolType> pattern)
    : UnrankedExtendedPattern(subtreeWildcard, subtreeGap, nodeWildcard, ext::set<SymbolType>(pattern.prefix_begin(), pattern.prefix_end()) + ext::set<SymbolType>{subtreeWildcard, subtreeGap, nodeWildcard}, pattern)
{
}

template <class SymbolType>
UnrankedExtendedPattern<SymbolType>::UnrankedExtendedPattern(const UnrankedPattern<SymbolType>& other)
    : UnrankedExtendedPattern(other.getSubtreeWildcard(), alphabet::Gap::template instance<SymbolType>(), alphabet::NodeWildcard::template instance<SymbolType>(), other.getAlphabet() + ext::set<SymbolType>{alphabet::Gap::template instance<SymbolType>(), alphabet::NodeWildcard::template instance<SymbolType>()}, other.getContent())
{
}

template <class SymbolType>
const ext::tree<SymbolType>& UnrankedExtendedPattern<SymbolType>::getContent() const&
{
    return m_content;
}

template <class SymbolType>
ext::tree<SymbolType>&& UnrankedExtendedPattern<SymbolType>::getContent() &&
{
    return std::move(m_content);
}

template <class SymbolType>
void UnrankedExtendedPattern<SymbolType>::checkAlphabet(const ext::tree<SymbolType>& data) const
{
    ext::set<SymbolType> minimalAlphabet(data.prefix_begin(), data.prefix_end());
    std::set_difference(minimalAlphabet.begin(), minimalAlphabet.end(), getAlphabet().begin(), getAlphabet().end(), ext::callback_iterator([](const SymbolType&) {
                            throw TreeException("Input symbols not in the alphabet.");
                        }));
}

template <class SymbolType>
void UnrankedExtendedPattern<SymbolType>::setTree(ext::tree<SymbolType> pattern)
{
    checkAlphabet(pattern);

    this->m_content = std::move(pattern);
}

template <class SymbolType>
void UnrankedExtendedPattern<SymbolType>::nicePrint(ext::ostream& os) const
{
    m_content.nicePrint(os);
}

} /* namespace tree */

namespace core {

/**
 * Helper class specifying constraints for the pattern's internal alphabet component.
 *
 * \tparam SymbolType used for the symbol of the alphabet
 */
template <class SymbolType>
class SetConstraint<tree::UnrankedExtendedPattern<SymbolType>, SymbolType, component::GeneralAlphabet> {
public:
    /**
     * Returns true if the symbol is still used in the pattern.
     *
     * \param pattern the tested pattern
     * \param symbol the tested symbol
     *
     * \returns true if the symbol is used, false othervise
     */
    static bool used(const tree::UnrankedExtendedPattern<SymbolType>& pattern, const SymbolType& symbol)
    {
        const ext::tree<SymbolType>& m_content = pattern.getContent();
        return std::find(m_content.prefix_begin(), m_content.prefix_end(), symbol) != m_content.prefix_end() || pattern.template accessComponent<component::SubtreeWildcardSymbol>().get() == symbol || pattern.template accessComponent<component::NodeWildcardSymbol>().get() == symbol;
    }

    /**
     * Returns true as all symbols are possibly available to be in an alphabet.
     *
     * \param pattern the tested pattern
     * \param symbol the tested symbol
     *
     * \returns true
     */
    static bool available(const tree::UnrankedExtendedPattern<SymbolType>&, const SymbolType&)
    {
        return true;
    }

    /**
     * All symbols are valid as symbols of an alphabet.
     *
     * \param pattern the tested pattern
     * \param symbol the tested symbol
     */
    static void valid(const tree::UnrankedExtendedPattern<SymbolType>&, const SymbolType&)
    {
    }
};

/**
 * Helper class specifying constraints for the pattern's internal node wildcards component.
 *
 * \tparam SymbolType used for the symbol part of the ranked symbols of the node wildcards of the pattern.
 */
template <class SymbolType>
class ElementConstraint<tree::UnrankedExtendedPattern<SymbolType>, SymbolType, component::NodeWildcardSymbol> {
public:
    /**
     * Node wildcards must already be in the alphabet.
     *
     * \param pattern the tested pattern
     * \param symbol the tested symbol
     *
     * \returns true if the symbol is in the alphabet of the pattern
     */
    static bool available(const tree::UnrankedExtendedPattern<SymbolType>& pattern, const SymbolType& symbol)
    {
        return pattern.template accessComponent<component::GeneralAlphabet>().get().contains(symbol);
    }

    /**
     * Node wildcard cannot be a subtree wildcard at the same time.
     *
     * \param pattern the tested pattern
     * \param symbol the tested symbol
     *
     * \throws TreeException if the symbol is already a subtree wildcard
     */
    static void valid(const tree::UnrankedExtendedPattern<SymbolType>& pattern, const SymbolType& symbol)
    {
        if (pattern.template accessComponent<component::SubtreeWildcardSymbol>().get() == symbol)
            throw tree::TreeException("NodeWildcard is already a SubtreeWildcard");
    }
};

/**
 * Helper class specifying constraints for the pattern's internal subtree wildcard element.
 *
 * \tparam SymbolType used for the symbol of the alphabet
 */
template <class SymbolType>
class ElementConstraint<tree::UnrankedExtendedPattern<SymbolType>, SymbolType, component::SubtreeWildcardSymbol> {
public:
    /**
     * Determines whether the symbol is available in the pattern's alphabet.
     *
     * \param pattern the tested pattern
     * \param symbol the tested symbol
     *
     * \returns true if the symbol is already in the alphabet of the pattern
     */
    static bool available(const tree::UnrankedExtendedPattern<SymbolType>& pattern, const SymbolType& symbol)
    {
        return pattern.template accessComponent<component::GeneralAlphabet>().get().count(symbol);
    }

    /**
     * Subtree wildcard may not already be a node wildcard.
     *
     * \param pattern the tested pattern
     * \param symbol the tested symbol
     *
     * \throws TreeException if the symbol is a node wildcard
     */
    static void valid(const tree::UnrankedExtendedPattern<SymbolType>& pattern, const SymbolType& symbol)
    {
        if (pattern.template accessComponent<component::NodeWildcardSymbol>().get() == symbol)
            throw tree::TreeException("SubtreeWildcard is already a NodeWildcard");
    }
};

/**
 * Helper class specifying constraints for the pattern's internal subtree wildcard element.
 *
 * \tparam SymbolType used for the symbol of the alphabet
 */
template <class SymbolType>
class ElementConstraint<tree::UnrankedExtendedPattern<SymbolType>, SymbolType, component::SubtreeGapSymbol> {
public:
    /**
     * Determines whether the symbol is available in the pattern's alphabet.
     *
     * \param pattern the tested pattern
     * \param symbol the tested symbol
     *
     * \returns true if the symbol is already in the alphabet of the pattern
     */
    static bool available(const tree::UnrankedExtendedPattern<SymbolType>& pattern, const SymbolType& symbol)
    {
        return pattern.template accessComponent<component::GeneralAlphabet>().get().count(symbol);
    }

    /**
     * Subtree wildcard needs to have zero arity and it needs to be different from any nonlinear variable of the tree.
     *
     * \param pattern the tested pattern
     * \param symbol the tested symbol
     *
     * \throws TreeException if the symbol does not have zero arity
     */
    static void valid(const tree::UnrankedExtendedPattern<SymbolType>&, const SymbolType&)
    {
    }
};

template <class SymbolType>
struct type_util<tree::UnrankedExtendedPattern<SymbolType>> {
    static tree::UnrankedExtendedPattern<SymbolType> denormalize(tree::UnrankedExtendedPattern<>&& value)
    {
        SymbolType wildcard = alphabet::SymbolDenormalize::denormalizeSymbol<SymbolType>(std::move(value).getSubtreeWildcard());
        SymbolType gap = alphabet::SymbolDenormalize::denormalizeSymbol<SymbolType>(std::move(value).getSubtreeGap());
        SymbolType nodeWildcard = alphabet::SymbolDenormalize::denormalizeSymbol<SymbolType>(std::move(value).getNodeWildcard());
        ext::set<SymbolType> alphabet = alphabet::SymbolDenormalize::denormalizeAlphabet<SymbolType>(std::move(value).getAlphabet());
        ext::tree<SymbolType> content = tree::TreeDenormalize::denormalizeTree<SymbolType>(std::move(value).getContent());
        return tree::UnrankedExtendedPattern<SymbolType>(std::move(wildcard), std::move(gap), std::move(nodeWildcard), std::move(alphabet), std::move(content));
    }

    static tree::UnrankedExtendedPattern<> normalize(tree::UnrankedExtendedPattern<SymbolType>&& value)
    {
        DefaultSymbolType wildcard = alphabet::SymbolNormalize::normalizeSymbol(std::move(value).getSubtreeWildcard());
        DefaultSymbolType gap = alphabet::SymbolNormalize::normalizeSymbol(std::move(value).getSubtreeGap());
        DefaultSymbolType nodeWildcard = alphabet::SymbolNormalize::normalizeSymbol(std::move(value).getNodeWildcard());
        ext::set<DefaultSymbolType> alphabet = alphabet::SymbolNormalize::normalizeAlphabet(std::move(value).getAlphabet());
        ext::tree<DefaultSymbolType> content = tree::TreeNormalize::normalizeTree(std::move(value).getContent());
        return tree::UnrankedExtendedPattern<>(std::move(wildcard), std::move(gap), std::move(nodeWildcard), std::move(alphabet), std::move(content));
    }

    static std::unique_ptr<type_details_base> type(const tree::UnrankedExtendedPattern<SymbolType>& arg)
    {
        core::unique_ptr_set<type_details_base> subTypesSymbol;
        for (const SymbolType& item : arg.getAlphabet())
            subTypesSymbol.insert(type_util<SymbolType>::type(item));

        std::vector<std::unique_ptr<type_details_base>> sub_types_vec;
        sub_types_vec.push_back(type_details_variant_type::make_variant(std::move(subTypesSymbol)));
        return std::make_unique<type_details_template>("tree::UnrankedExtendedPattern", std::move(sub_types_vec));
    }
};

template <class SymbolType>
struct type_details_retriever<tree::UnrankedExtendedPattern<SymbolType>> {
    static std::unique_ptr<type_details_base> get()
    {
        std::vector<std::unique_ptr<type_details_base>> sub_types_vec;
        sub_types_vec.push_back(type_details_retriever<SymbolType>::get());
        return std::make_unique<type_details_template>("tree::UnrankedExtendedPattern", std::move(sub_types_vec));
    }
};

} /* namespace core */

extern template class tree::UnrankedExtendedPattern<>;
extern template class abstraction::ValueHolder<tree::UnrankedExtendedPattern<>>;
extern template const tree::UnrankedExtendedPattern<>& abstraction::retrieveValue<const tree::UnrankedExtendedPattern<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
extern template class registration::DenormalizationRegisterImpl<const tree::UnrankedExtendedPattern<>&>;
extern template class registration::NormalizationRegisterImpl<tree::UnrankedExtendedPattern<>>;
