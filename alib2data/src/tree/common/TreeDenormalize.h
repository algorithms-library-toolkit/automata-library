#pragma once

#include <alib/tree>

#include <alphabet/common/SymbolDenormalize.h>
#include <common/ranked_symbol.hpp>

namespace tree {

/**
 * This class contains methods to print XML representation of automata to the output stream.
 */
class TreeDenormalize {
public:
    template <class SymbolType>
    static ext::tree<SymbolType> denormalizeTree(ext::tree<DefaultSymbolType>&& tree);

    template <class SymbolType>
    static ext::tree<common::ranked_symbol<SymbolType>> denormalizeRankedTree(ext::tree<common::ranked_symbol<DefaultSymbolType>>&& tree);
};

template <class SymbolType>
ext::tree<SymbolType> TreeDenormalize::denormalizeTree(ext::tree<DefaultSymbolType>&& tree)
{
    ext::vector<ext::tree<SymbolType>> children;

    for (ext::tree<DefaultSymbolType>& child : tree.getChildren()) {
        children.push_back(denormalizeTree<SymbolType>(std::move(child)));
    }

    return ext::tree<SymbolType>(alphabet::SymbolDenormalize::denormalizeSymbol<SymbolType>(std::move(tree.getData())), std::move(children));
}

template <class SymbolType>
ext::tree<common::ranked_symbol<SymbolType>> TreeDenormalize::denormalizeRankedTree(ext::tree<common::ranked_symbol<DefaultSymbolType>>&& tree)
{
    ext::vector<ext::tree<common::ranked_symbol<SymbolType>>> children;

    for (ext::tree<common::ranked_symbol<DefaultSymbolType>>& child : tree.getChildren()) {
        children.push_back(denormalizeRankedTree<SymbolType>(std::move(child)));
    }

    return ext::tree<common::ranked_symbol<SymbolType>>(alphabet::SymbolDenormalize::denormalizeRankedSymbol<SymbolType>(std::move(tree.getData())), std::move(children));
}

} /* namespace tree */
