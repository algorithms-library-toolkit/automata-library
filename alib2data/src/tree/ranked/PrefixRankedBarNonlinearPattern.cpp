#include "PrefixRankedBarNonlinearPattern.h"

#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>
#include <registration/ValuePrinterRegistration.hpp>

template class tree::PrefixRankedBarNonlinearPattern<>;
template class abstraction::ValueHolder<tree::PrefixRankedBarNonlinearPattern<>>;
template const tree::PrefixRankedBarNonlinearPattern<>& abstraction::retrieveValue<const tree::PrefixRankedBarNonlinearPattern<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const tree::PrefixRankedBarNonlinearPattern<>&>;
template class registration::NormalizationRegisterImpl<tree::PrefixRankedBarNonlinearPattern<>>;

namespace {

auto components = registration::ComponentRegister<tree::PrefixRankedBarNonlinearPattern<>>();

auto valuePrinter = registration::ValuePrinterRegister<tree::PrefixRankedBarNonlinearPattern<>>();

auto PrefixRankedBarNonlinearPatternFromRankedTree = registration::CastRegister<tree::PrefixRankedBarNonlinearPattern<>, tree::RankedTree<>>();
auto PrefixRankedBarNonlinearPatternFromRankedPattern = registration::CastRegister<tree::PrefixRankedBarNonlinearPattern<>, tree::RankedPattern<>>();
auto PrefixRankedBarNonlinearPatternFromRankedNonlinearPattern = registration::CastRegister<tree::PrefixRankedBarNonlinearPattern<>, tree::RankedNonlinearPattern<>>();
auto PrefixRankedBarNonlinearPatternFromPrefixRankedBarTree = registration::CastRegister<tree::PrefixRankedBarNonlinearPattern<>, tree::PrefixRankedBarTree<>>();
auto PrefixRankedBarNonlinearPatternFromPrefixRankedBarPattern = registration::CastRegister<tree::PrefixRankedBarNonlinearPattern<>, tree::PrefixRankedBarPattern<>>();

auto LinearStringFromPrefixRankedBarNonlinearPattern = registration::CastRegister<string::LinearString<common::ranked_symbol<>>, tree::PrefixRankedBarNonlinearPattern<>>();

} /* namespace */
