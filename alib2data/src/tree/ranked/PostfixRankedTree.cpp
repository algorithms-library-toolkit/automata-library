#include "PostfixRankedTree.h"

#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>
#include <registration/ValuePrinterRegistration.hpp>

template class tree::PostfixRankedTree<>;
template class abstraction::ValueHolder<tree::PostfixRankedTree<>>;
template const tree::PostfixRankedTree<>& abstraction::retrieveValue<const tree::PostfixRankedTree<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const tree::PostfixRankedTree<>&>;
template class registration::NormalizationRegisterImpl<tree::PostfixRankedTree<>>;

namespace {

auto components = registration::ComponentRegister<tree::PostfixRankedTree<>>();

auto valuePrinter = registration::ValuePrinterRegister<tree::PostfixRankedTree<>>();

auto PostfixRankedTreeFromRankedTree = registration::CastRegister<tree::PostfixRankedTree<>, tree::RankedTree<>>();

auto LinearStringFromPostfixRankedTree = registration::CastRegister<string::LinearString<common::ranked_symbol<>>, tree::PostfixRankedTree<>>();

} /* namespace */
