/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <common/DefaultSymbolType.h>

namespace tree {

template <class SymbolType = DefaultSymbolType>
class UnorderedRankedTree;

} /* namespace tree */


#include <ext/algorithm>
#include <ext/iostream>

#include <alib/set>
#include <alib/string>
#include <alib/tree>

#include <common/ranked_symbol.hpp>
#include <core/modules.hpp>

#include <tree/TreeException.h>
#include <tree/common/TreeAuxiliary.h>

#include <alphabet/common/SymbolDenormalize.h>
#include <alphabet/common/SymbolNormalize.h>
#include <core/type_details_base.hpp>
#include <core/type_util.hpp>
#include <tree/common/TreeDenormalize.h>
#include <tree/common/TreeNormalize.h>

#include <tree/ranked/RankedTree.h>

#include <registration/DenormalizationRegistration.hpp>
#include <registration/NormalizationRegistration.hpp>

namespace tree {

/**
 * \brief
 * Tree structure represented in its natural representation. The representation is so called ranked, therefore it consists of ranked symbols. The rank of the ranked symbol needs to be compatible with unsigned integer.

 * \details
 * T = (A, C),
 * A (Alphabet) = finite set of ranked symbols,
 * C (Content) = tree in its natural representation
 *
 * \tparam SymbolType used for the symbol part of the ranked symbol
 */
template <class SymbolType>
class UnorderedRankedTree final : public core::Components<UnorderedRankedTree<SymbolType>, ext::set<common::ranked_symbol<SymbolType>>, module::Set, component::GeneralAlphabet> {
    /**
     * Natural representation of the tree content.
     */
    ext::tree<common::ranked_symbol<SymbolType>> m_content;

    /**
     * Checks that symbols of the tree are present in the alphabet.
     *
     * \throws TreeException when some symbols of the tree representation are not present in the alphabet
     *
     * \param data the tree in its natural representation
     */
    void checkAlphabet(const ext::tree<common::ranked_symbol<SymbolType>>& data) const;

    /**
     * Checks that the rank of each symbol of a tree node corresponds to the number of child nodes of that same node.
     *
     * \throws TreeException when some nodes of a tree have different number of children than the rank of their label states
     *
     * \param data the tree in its natural representation
     */
    void checkArities(const ext::tree<common::ranked_symbol<SymbolType>>& data) const;

public:
    /**
     * \brief Creates a new instance of the tree with concrete alphabet and content.
     *
     * \param alphabet the initial alphabet of the tree
     * \param tree the initial tree in its natural representation
     */
    explicit UnorderedRankedTree(ext::set<common::ranked_symbol<SymbolType>> alphabet, ext::tree<common::ranked_symbol<SymbolType>> tree);

    /**
     * \brief Creates a new instance of the tree based on the content, the alphabet is implicitly created from the content.
     *
     * \param tree the initial tree in its natural representation
     */
    explicit UnorderedRankedTree(ext::tree<common::ranked_symbol<SymbolType>> tree);

    /**
     * \brief Creates a new instance of the tree based on the Ranked Tree.
     *
     * \param tree the ranked tree to use.
     */
    explicit UnorderedRankedTree(const RankedTree<SymbolType>& tree);

    /**
     * Getter of the alphabet.
     *
     * \returns the alphabet of the tree
     */
    const ext::set<common::ranked_symbol<SymbolType>>& getAlphabet() const&
    {
        return this->template accessComponent<component::GeneralAlphabet>().get();
    }

    /**
     * Getter of the alphabet.
     *
     * \returns the alphabet of the tree
     */
    ext::set<common::ranked_symbol<SymbolType>>&& getAlphabet() &&
    {
        return std::move(this->template accessComponent<component::GeneralAlphabet>().get());
    }

    /**
     * Adder of an alphabet symbols.
     *
     * \param symbols the new symbols to be added to the alphabet
     */
    void extendAlphabet(const ext::set<common::ranked_symbol<SymbolType>>& symbols)
    {
        this->template accessComponent<component::GeneralAlphabet>().add(symbols);
    }

    /**
     * Getter of the tree representation.
     *
     * \return the natural representation of the tree.
     */
    const ext::tree<common::ranked_symbol<SymbolType>>& getContent() const&;

    /**
     * Getter of the tree representation.
     *
     * \return the natural representation of the tree.
     */
    ext::tree<common::ranked_symbol<SymbolType>>&& getContent() &&;

    /**
     * Setter of the representation of the tree.
     *
     * \throws TreeException in same situations as checkAlphabet and checkArities
     *
     * \param tree new representation of the tree.
     */
    void setTree(ext::tree<common::ranked_symbol<SymbolType>> tree);

    /**
     * The three way comparison implementation
     *
     * \param other the other instance
     *
     * \returns the ordering between this object and the @p other.
     */
    auto operator<=>(const UnorderedRankedTree& other) const
    {
        return std::tie(m_content, getAlphabet()) <=> std::tie(other.m_content, other.getAlphabet());
    }

    /**
     * The equality comparison implementation.
     *
     * \param other the other object to compare with.
     *
     * \returns true if this and other objects are equal, false othervise
     */
    bool operator==(const UnorderedRankedTree& other) const
    {
        return std::tie(m_content, getAlphabet()) == std::tie(other.m_content, other.getAlphabet());
    }

    /**
     * Print this object as raw representation to ostream.
     *
     * \param out ostream where to print
     * \param instance object to print
     *
     * \returns modified output stream
     */
    friend ext::ostream& operator<<(ext::ostream& out, const UnorderedRankedTree& instance)
    {
        out << "(UnorderedRankedTree";
        out << " alphabet = " << instance.getAlphabet();
        out << " content = " << instance.getContent();
        out << ")";
        return out;
    }

    /**
     * Hierarchical printer of the tree.
     *
     * \param os the output stream destination of the print
     */
    void nicePrint(ext::ostream& os) const;
};

template <class SymbolType>
UnorderedRankedTree<SymbolType>::UnorderedRankedTree(ext::set<common::ranked_symbol<SymbolType>> alphabet, ext::tree<common::ranked_symbol<SymbolType>> tree)
    : core::Components<UnorderedRankedTree, ext::set<common::ranked_symbol<SymbolType>>, module::Set, component::GeneralAlphabet>(std::move(alphabet))
    , m_content(TreeAuxiliary::sort(std::move(tree)))
{
    checkAlphabet(m_content);
    checkArities(m_content);
}

template <class SymbolType>
UnorderedRankedTree<SymbolType>::UnorderedRankedTree(ext::tree<common::ranked_symbol<SymbolType>> tree)
    : UnorderedRankedTree(ext::set<common::ranked_symbol<SymbolType>>(tree.prefix_begin(), tree.prefix_end()), tree)
{
}

template <class SymbolType>
UnorderedRankedTree<SymbolType>::UnorderedRankedTree(const RankedTree<SymbolType>& tree)
    : UnorderedRankedTree(tree.getAlphabet(), tree.getContent())
{
}

template <class SymbolType>
const ext::tree<common::ranked_symbol<SymbolType>>& UnorderedRankedTree<SymbolType>::getContent() const&
{
    return m_content;
}

template <class SymbolType>
ext::tree<common::ranked_symbol<SymbolType>>&& UnorderedRankedTree<SymbolType>::getContent() &&
{
    return std::move(m_content);
}

template <class SymbolType>
void UnorderedRankedTree<SymbolType>::checkAlphabet(const ext::tree<common::ranked_symbol<SymbolType>>& data) const
{
    ext::set<common::ranked_symbol<SymbolType>> minimalAlphabet(data.prefix_begin(), data.prefix_end());
    std::set_difference(minimalAlphabet.begin(), minimalAlphabet.end(), getAlphabet().begin(), getAlphabet().end(), ext::callback_iterator([](const common::ranked_symbol<SymbolType>&) {
                            throw TreeException("Input symbols not in the alphabet.");
                        }));
}

template <class SymbolType>
void UnorderedRankedTree<SymbolType>::checkArities(const ext::tree<common::ranked_symbol<SymbolType>>& data) const
{
    if (data.getData().getRank() != data.getChildren().size())
        throw exception::CommonException("Invalid rank.");

    for (const ext::tree<common::ranked_symbol<SymbolType>>& child : data)
        checkArities(child);
}

template <class SymbolType>
void UnorderedRankedTree<SymbolType>::setTree(ext::tree<common::ranked_symbol<SymbolType>> tree)
{
    checkAlphabet(tree);
    checkArities(tree);

    this->m_content = TreeAuxiliary::sort(std::move(tree));
}

template <class SymbolType>
void UnorderedRankedTree<SymbolType>::nicePrint(ext::ostream& os) const
{
    m_content.nicePrint(os);
}

} /* namespace tree */

namespace core {

/**
 * Helper class specifying constraints for the tree's internal alphabet component.
 *
 * \tparam SymbolType used for the symbol part of the ranked symbols of the alphabet of the pattern.
 */
template <class SymbolType>
class SetConstraint<tree::UnorderedRankedTree<SymbolType>, common::ranked_symbol<SymbolType>, component::GeneralAlphabet> {
public:
    /**
     * Returns true if the symbol is still used in the tree.
     *
     * \param tree the tested tree
     * \param symbol the tested symbol
     *
     * \returns true if the symbol is used, false othervise
     */
    static bool used(const tree::UnorderedRankedTree<SymbolType>& tree, const common::ranked_symbol<SymbolType>& symbol)
    {
        const ext::tree<common::ranked_symbol<SymbolType>>& m_content = tree.getContent();
        return std::find(m_content.prefix_begin(), m_content.prefix_end(), symbol) != m_content.prefix_end();
    }

    /**
     * Returns true as all symbols are possibly available to be in an alphabet.
     *
     * \param tree the tested tree
     * \param symbol the tested symbol
     *
     * \returns true
     */
    static bool available(const tree::UnorderedRankedTree<SymbolType>&, const common::ranked_symbol<SymbolType>&)
    {
        return true;
    }

    /**
     * All symbols are valid as symbols of an alphabet.
     *
     * \param tree the tested tree
     * \param symbol the tested symbol
     */
    static void valid(const tree::UnorderedRankedTree<SymbolType>&, const common::ranked_symbol<SymbolType>&)
    {
    }
};

template <class SymbolType>
struct type_util<tree::UnorderedRankedTree<SymbolType>> {
    static tree::UnorderedRankedTree<SymbolType> denormalize(tree::UnorderedRankedTree<>&& value)
    {
        ext::set<common::ranked_symbol<SymbolType>> alphabet = alphabet::SymbolDenormalize::denormalizeRankedAlphabet<SymbolType>(std::move(value).getAlphabet());
        ext::tree<common::ranked_symbol<SymbolType>> content = tree::TreeDenormalize::denormalizeRankedTree<SymbolType>(std::move(value).getContent());
        return tree::UnorderedRankedTree<SymbolType>(std::move(alphabet), std::move(content));
    }

    static tree::UnorderedRankedTree<> normalize(tree::UnorderedRankedTree<SymbolType>&& value)
    {
        ext::set<common::ranked_symbol<DefaultSymbolType>> alphabet = alphabet::SymbolNormalize::normalizeRankedAlphabet(std::move(value).getAlphabet());
        ext::tree<common::ranked_symbol<DefaultSymbolType>> content = tree::TreeNormalize::normalizeRankedTree(std::move(value).getContent());
        return tree::UnorderedRankedTree<>(std::move(alphabet), std::move(content));
    }

    static std::unique_ptr<type_details_base> type(const tree::UnorderedRankedTree<SymbolType>& arg)
    {
        core::unique_ptr_set<type_details_base> subTypesSymbol;
        for (const common::ranked_symbol<SymbolType>& item : arg.getAlphabet())
            subTypesSymbol.insert(type_util<SymbolType>::type(item.getSymbol()));

        std::vector<std::unique_ptr<type_details_base>> sub_types_vec;
        sub_types_vec.push_back(type_details_variant_type::make_variant(std::move(subTypesSymbol)));
        return std::make_unique<type_details_template>("tree::UnorderedRankedTree", std::move(sub_types_vec));
    }
};

template <class SymbolType>
struct type_details_retriever<tree::UnorderedRankedTree<SymbolType>> {
    static std::unique_ptr<type_details_base> get()
    {
        std::vector<std::unique_ptr<type_details_base>> sub_types_vec;
        sub_types_vec.push_back(type_details_retriever<SymbolType>::get());
        return std::make_unique<type_details_template>("tree::UnorderedRankedTree", std::move(sub_types_vec));
    }
};

} /* namespace core */

extern template class tree::UnorderedRankedTree<>;
extern template class abstraction::ValueHolder<tree::UnorderedRankedTree<>>;
extern template const tree::UnorderedRankedTree<>& abstraction::retrieveValue<const tree::UnorderedRankedTree<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
extern template class registration::DenormalizationRegisterImpl<const tree::UnorderedRankedTree<>&>;
extern template class registration::NormalizationRegisterImpl<tree::UnorderedRankedTree<>>;
