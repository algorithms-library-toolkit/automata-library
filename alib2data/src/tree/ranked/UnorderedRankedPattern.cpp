#include "UnorderedRankedPattern.h"

#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>
#include <registration/ValuePrinterRegistration.hpp>

template class tree::UnorderedRankedPattern<>;
template class abstraction::ValueHolder<tree::UnorderedRankedPattern<>>;
template const tree::UnorderedRankedPattern<>& abstraction::retrieveValue<const tree::UnorderedRankedPattern<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const tree::UnorderedRankedPattern<>&>;
template class registration::NormalizationRegisterImpl<tree::UnorderedRankedPattern<>>;

namespace {

auto components = registration::ComponentRegister<tree::UnorderedRankedPattern<>>();

auto valuePrinter = registration::ValuePrinterRegister<tree::UnorderedRankedPattern<>>();

auto UnorderedRankedPatternFromRankedPattern = registration::CastRegister<tree::UnorderedRankedPattern<>, tree::RankedPattern<>>();

} /* namespace */
