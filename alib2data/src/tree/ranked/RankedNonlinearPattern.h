/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <common/DefaultSymbolType.h>

namespace tree {

template <class SymbolType = DefaultSymbolType>
class RankedNonlinearPattern;

} /* namespace tree */


#include <ext/algorithm>
#include <ext/iostream>

#include <alib/set>
#include <alib/string>
#include <alib/tree>

#include <common/ranked_symbol.hpp>
#include <core/modules.hpp>

#include <tree/TreeException.h>
#include <tree/common/TreeAuxiliary.h>

#include <alphabet/common/SymbolDenormalize.h>
#include <alphabet/common/SymbolNormalize.h>
#include <core/type_details_base.hpp>
#include <core/type_util.hpp>
#include <tree/common/TreeDenormalize.h>
#include <tree/common/TreeNormalize.h>

#include "../unranked/UnrankedNonlinearPattern.h"

#include <registration/DenormalizationRegistration.hpp>
#include <registration/NormalizationRegistration.hpp>

namespace tree {

/**
 * \brief
 * Nonlinear tree pattern represented in its natural representation. The representation is so called ranked, therefore it consists of ranked symbols. The rank of the ranked symbol needs to be compatible with unsigned integer. Additionally the pattern contains a special wildcard symbol representing any subtree and nonlinear variables each to represent same subtree (in the particular occurrence in a tree).
 *
 * \details
 * T = (A, C, W \in ( A \minus B ), V \in ( A \minus B) ),
 * A (Alphabet) = finite set of ranked symbols,
 * C (Content) = pattern in its natural representation
 * W (Wildcard) = special symbol representing any subtree
 * V (Variables) = finite set of special symbols each representing same subtree
 *
 * \tparam SymbolType used for the symbol part of the ranked symbol
 */
template <class SymbolType>
class RankedNonlinearPattern final : public core::Components<RankedNonlinearPattern<SymbolType>, ext::set<common::ranked_symbol<SymbolType>>, module::Set, std::tuple<component::GeneralAlphabet, component::NonlinearAlphabet>, common::ranked_symbol<SymbolType>, module::Value, component::SubtreeWildcardSymbol> {
    /**
     * Natural representation of the pattern content.
     */
    ext::tree<common::ranked_symbol<SymbolType>> m_content;

    /**
     * Checks that symbols of the pattern are present in the alphabet.
     *
     * \throws TreeException when some symbols of the pattern representation are not present in the alphabet
     *
     * \param data the pattern in its natural representation
     */
    void checkAlphabet(const ext::tree<common::ranked_symbol<SymbolType>>& data) const;

    /**
     * Checks that the rank of each symbol of a pattern node corresponds to the number of child nodes of that same node.
     *
     * \throws TreeException when some nodes of a pattern have different number of children than the rank of their label states
     *
     * \param data the pattern in its natural representation
     */
    void checkArities(const ext::tree<common::ranked_symbol<SymbolType>>& data) const;

public:
    /**
     * \brief Creates a new instance of the pattern with concrete alphabet, content, wildcard, and nonlinear variables.
     *
     * \param subtreeWildcard the wildcard symbol
     * \param nonlinearVariables the set of nonlinear variables
     * \param alphabet the initial alphabet of the pattern
     * \param pattern the initial content in it's natural representation
     */
    explicit RankedNonlinearPattern(common::ranked_symbol<SymbolType> subtreeWildcard, ext::set<common::ranked_symbol<SymbolType>> nonlinearVariables, ext::set<common::ranked_symbol<SymbolType>> alphabet, ext::tree<common::ranked_symbol<SymbolType>> pattern);

    /**
     * \brief Creates a new instance of the pattern with concrete content, wildcard, and nonlinear variables. The alphabet is deduced from the content.
     *
     * \param subtreeWildcard the wildcard symbol
     * \param nonlinearVariables the set of nonlinear variables
     * \param pattern the initial content in it's natural representation
     */
    explicit RankedNonlinearPattern(common::ranked_symbol<SymbolType> subtreeWildcard, ext::set<common::ranked_symbol<SymbolType>> nonlinearVariables, ext::tree<common::ranked_symbol<SymbolType>> pattern);

    /**
     * \brief Creates a new instance of the pattern with concrete content and wildcard. The alphabet is deduced from the content. Nonlinear variables are defaultly constructed to empty set.
     *
     * \param subtreeWildcard the wildcard symbol
     * \param pattern the initial content in it's natural representation
     */
    explicit RankedNonlinearPattern(common::ranked_symbol<SymbolType> subtreeWildcard, ext::tree<common::ranked_symbol<SymbolType>> pattern);

    /**
     * \brief Creates a new instance of the pattern based on UnrankedNonlinearPattern, the alphabet is created from the content of the UnrankedNonlinearPattern. The alphabet is created since ranks cannot be deduced.
     *
     * \param other the pattern represented as UnrankedNonlinearPattern
     */
    explicit RankedNonlinearPattern(const UnrankedNonlinearPattern<SymbolType>& other);

    /**
     * Getter of the alphabet.
     *
     * \returns the alphabet of the pattern
     */
    const ext::set<common::ranked_symbol<SymbolType>>& getAlphabet() const&
    {
        return this->template accessComponent<component::GeneralAlphabet>().get();
    }

    /**
     * Getter of the alphabet.
     *
     * \returns the alphabet of the pattern
     */
    ext::set<common::ranked_symbol<SymbolType>>&& getAlphabet() &&
    {
        return std::move(this->template accessComponent<component::GeneralAlphabet>().get());
    }

    /**
     * Adder of an alphabet symbols.
     *
     * \param symbols the new symbols to be added to the alphabet
     */
    void extendAlphabet(const ext::set<common::ranked_symbol<SymbolType>>& symbols)
    {
        this->template accessComponent<component::GeneralAlphabet>().add(symbols);
    }

    /**
     * Getter of the wildcard symbol.
     *
     * \returns the wildcard symbol of the pattern
     */
    const common::ranked_symbol<SymbolType>& getSubtreeWildcard() const&
    {
        return this->template accessComponent<component::SubtreeWildcardSymbol>().get();
    }

    /**
     * Getter of the wildcard symbol.
     *
     * \returns the wildcard symbol of the pattern
     */
    common::ranked_symbol<SymbolType>&& getSubtreeWildcard() &&
    {
        return std::move(this->template accessComponent<component::SubtreeWildcardSymbol>().get());
    }

    /**
     * Getter of the nonlinear variables.
     *
     * \returns the nonlinear variables of the pattern
     */
    const ext::set<common::ranked_symbol<SymbolType>>& getNonlinearVariables() const&
    {
        return this->template accessComponent<component::NonlinearAlphabet>().get();
    }

    /**
     * Getter of the nonlinear variables.
     *
     * \returns the nonlinear variables of the pattern
     */
    ext::set<common::ranked_symbol<SymbolType>>&& getNonlinearVariables() &&
    {
        return std::move(this->template accessComponent<component::NonlinearAlphabet>().get());
    }

    /**
     * Getter of the pattern representation.
     *
     * \return the natural representation of the pattern.
     */
    const ext::tree<common::ranked_symbol<SymbolType>>& getContent() const&;

    /**
     * Getter of the pattern representation.
     *
     * \return the natural representation of the pattern.
     */
    ext::tree<common::ranked_symbol<SymbolType>>&& getContent() &&;

    /**
     * Setter of the representation of the pattern.
     *
     * \throws TreeException in same situations as checkAlphabet and checkArities
     *
     * \param pattern new representation of the pattern.
     */
    void setTree(ext::tree<common::ranked_symbol<SymbolType>> pattern);

    /**
     * The three way comparison implementation
     *
     * \param other the other instance
     *
     * \returns the ordering between this object and the @p other.
     */
    auto operator<=>(const RankedNonlinearPattern& other) const
    {
        return std::tie(m_content, getAlphabet(), getSubtreeWildcard(), getNonlinearVariables()) <=> std::tie(other.m_content, other.getAlphabet(), other.getSubtreeWildcard(), getNonlinearVariables());
    }

    /**
     * The equality comparison implementation.
     *
     * \param other the other object to compare with.
     *
     * \returns true if this and other objects are equal, false othervise
     */
    bool operator==(const RankedNonlinearPattern& other) const
    {
        return std::tie(m_content, getAlphabet(), getSubtreeWildcard(), getNonlinearVariables()) == std::tie(other.m_content, other.getAlphabet(), other.getSubtreeWildcard(), getNonlinearVariables());
    }

    /**
     * Print this object as raw representation to ostream.
     *
     * \param out ostream where to print
     * \param instance object to print
     *
     * \returns modified output stream
     */
    friend ext::ostream& operator<<(ext::ostream& out, const RankedNonlinearPattern& instance)
    {
        out << "(RankedNonlinearPattern";
        out << " alphabet = " << instance.getAlphabet();
        out << " content = " << instance.getContent();
        out << " nonlinearVariables = " << instance.getNonlinearVariables();
        out << " subtreeWildcard = " << instance.getSubtreeWildcard();
        out << ")";
        return out;
    }

    /**
     * Nice printer of the tree natural representation
     *
     * \param os the output stream to print to
     */
    void nicePrint(ext::ostream& os) const;
};

template <class SymbolType>
RankedNonlinearPattern<SymbolType>::RankedNonlinearPattern(common::ranked_symbol<SymbolType> subtreeWildcard, ext::set<common::ranked_symbol<SymbolType>> nonlinearVariables, ext::set<common::ranked_symbol<SymbolType>> alphabet, ext::tree<common::ranked_symbol<SymbolType>> pattern)
    : core::Components<RankedNonlinearPattern, ext::set<common::ranked_symbol<SymbolType>>, module::Set, std::tuple<component::GeneralAlphabet, component::NonlinearAlphabet>, common::ranked_symbol<SymbolType>, module::Value, component::SubtreeWildcardSymbol>(std::move(alphabet), std::move(nonlinearVariables), std::move(subtreeWildcard))
    , m_content(std::move(pattern))
{
    checkAlphabet(m_content);
    checkArities(m_content);
}

template <class SymbolType>
RankedNonlinearPattern<SymbolType>::RankedNonlinearPattern(common::ranked_symbol<SymbolType> subtreeWildcard, ext::set<common::ranked_symbol<SymbolType>> nonlinearVariables, ext::tree<common::ranked_symbol<SymbolType>> pattern)
    : RankedNonlinearPattern(subtreeWildcard, nonlinearVariables, ext::set<common::ranked_symbol<SymbolType>>(pattern.prefix_begin(), pattern.prefix_end()) + nonlinearVariables + ext::set<common::ranked_symbol<SymbolType>>{subtreeWildcard}, pattern)
{
}

template <class SymbolType>
RankedNonlinearPattern<SymbolType>::RankedNonlinearPattern(common::ranked_symbol<SymbolType> subtreeWildcard, ext::tree<common::ranked_symbol<SymbolType>> pattern)
    : RankedNonlinearPattern(subtreeWildcard, ext::set<common::ranked_symbol<SymbolType>>{}, pattern)
{
}

template <class SymbolType>
RankedNonlinearPattern<SymbolType>::RankedNonlinearPattern(const UnrankedNonlinearPattern<SymbolType>& other)
    : RankedNonlinearPattern(common::ranked_symbol<SymbolType>(other.getSubtreeWildcard(), 0), TreeAuxiliary::unrankedToRanked<SymbolType>(other.getContent()))
{
}

template <class SymbolType>
const ext::tree<common::ranked_symbol<SymbolType>>& RankedNonlinearPattern<SymbolType>::getContent() const&
{
    return m_content;
}

template <class SymbolType>
ext::tree<common::ranked_symbol<SymbolType>>&& RankedNonlinearPattern<SymbolType>::getContent() &&
{
    return std::move(m_content);
}

template <class SymbolType>
void RankedNonlinearPattern<SymbolType>::checkAlphabet(const ext::tree<common::ranked_symbol<SymbolType>>& data) const
{
    ext::set<common::ranked_symbol<SymbolType>> minimalAlphabet(data.prefix_begin(), data.prefix_end());
    std::set_difference(minimalAlphabet.begin(), minimalAlphabet.end(), getAlphabet().begin(), getAlphabet().end(), ext::callback_iterator([](const common::ranked_symbol<SymbolType>&) {
                            throw TreeException("Input symbols not in the alphabet.");
                        }));
}

template <class SymbolType>
void RankedNonlinearPattern<SymbolType>::checkArities(const ext::tree<common::ranked_symbol<SymbolType>>& data) const
{
    if (data.getData().getRank() != data.getChildren().size())
        throw exception::CommonException("Invalid rank.");

    for (const ext::tree<common::ranked_symbol<SymbolType>>& child : data)
        checkArities(child);
}

template <class SymbolType>
void RankedNonlinearPattern<SymbolType>::setTree(ext::tree<common::ranked_symbol<SymbolType>> pattern)
{
    checkAlphabet(pattern);
    checkArities(pattern);

    this->m_content = std::move(pattern);
}

template <class SymbolType>
void RankedNonlinearPattern<SymbolType>::nicePrint(ext::ostream& os) const
{
    m_content.nicePrint(os);
}

} /* namespace tree */

namespace core {

/**
 * Helper class specifying constraints for the pattern's internal alphabet component.
 *
 * \tparam SymbolType used for the symbol part of the ranked symbols of the alphabet of the pattern.
 */
template <class SymbolType>
class SetConstraint<tree::RankedNonlinearPattern<SymbolType>, common::ranked_symbol<SymbolType>, component::GeneralAlphabet> {
public:
    /**
     * Returns true if the symbol is still used in the pattern.
     *
     * \param pattern the tested pattern
     * \param symbol the tested symbol
     *
     * \returns true if the symbol is used, false othervise
     */
    static bool used(const tree::RankedNonlinearPattern<SymbolType>& pattern, const common::ranked_symbol<SymbolType>& symbol)
    {
        const ext::tree<common::ranked_symbol<SymbolType>>& m_content = pattern.getContent();
        return std::find(m_content.prefix_begin(), m_content.prefix_end(), symbol) != m_content.prefix_end() || pattern.template accessComponent<component::SubtreeWildcardSymbol>().get() == symbol || pattern.template accessComponent<component::NonlinearAlphabet>().get().count(symbol);
    }

    /**
     * Returns true as all symbols are possibly available to be in an alphabet.
     *
     * \param pattern the tested pattern
     * \param symbol the tested symbol
     *
     * \returns true
     */
    static bool available(const tree::RankedNonlinearPattern<SymbolType>&, const common::ranked_symbol<SymbolType>&)
    {
        return true;
    }

    /**
     * All symbols are valid as symbols of an alphabet.
     *
     * \param pattern the tested pattern
     * \param symbol the tested symbol
     */
    static void valid(const tree::RankedNonlinearPattern<SymbolType>&, const common::ranked_symbol<SymbolType>&)
    {
    }
};

/**
 * Helper class specifying constraints for the pattern's internal nonlinear variables component.
 *
 * \tparam SymbolType used for the symbol part of the ranked symbols of the alphabet of the pattern.
 */
template <class SymbolType>
class SetConstraint<tree::RankedNonlinearPattern<SymbolType>, common::ranked_symbol<SymbolType>, component::NonlinearAlphabet> {
public:
    /**
     * Returns false. Nonlinear symbol is only a mark that the pattern itself does require further.
     *
     * \param pattern the tested pattern
     * \param symbol the tested symbol
     *
     * \returns true if the symbol is used, false othervise
     */
    static bool used(const tree::RankedNonlinearPattern<SymbolType>&, const common::ranked_symbol<SymbolType>&)
    {
        return false;
    }

    /**
     * Determines whether the symbol is available in the pattern's alphabet.
     *
     * \param pattern the tested pattern
     * \param symbol the tested symbol
     *
     * \returns true
     */
    static bool available(const tree::RankedNonlinearPattern<SymbolType>& pattern, const common::ranked_symbol<SymbolType>& symbol)
    {
        return pattern.template accessComponent<component::GeneralAlphabet>().get().count(symbol);
    }

    /**
     * Nonlinear variable needs to have zero arity and needs to be different from subtree wildcard.
     *
     * \param pattern the tested pattern
     * \param symbol the tested symbol
     *
     * \throws TreeException if the symbol does not have zero arity
     */
    static void valid(const tree::RankedNonlinearPattern<SymbolType>& pattern, const common::ranked_symbol<SymbolType>& symbol)
    {
        if (symbol.getRank() != 0)
            throw tree::TreeException("Nonlinear variable symbol has nonzero arity");

        if (pattern.template accessComponent<component::SubtreeWildcardSymbol>().get() == symbol)
            throw tree::TreeException("Symbol " + ext::to_string(symbol) + "cannot be set as nonlinear variable since it is already subtree wildcard");
    }
};

/**
 * Helper class specifying constraints for the pattern's internal subtree wildcard element.
 *
 * \tparam SymbolType used for the symbol part of the ranked symbols of the alphabet of the pattern.
 */
template <class SymbolType>
class ElementConstraint<tree::RankedNonlinearPattern<SymbolType>, common::ranked_symbol<SymbolType>, component::SubtreeWildcardSymbol> {
public:
    /**
     * Determines whether the symbol is available in the pattern's alphabet.
     *
     * \param pattern the tested pattern
     * \param symbol the tested symbol
     *
     * \returns true if the symbol is already in the alphabet of the pattern
     */
    static bool available(const tree::RankedNonlinearPattern<SymbolType>& pattern, const common::ranked_symbol<SymbolType>& symbol)
    {
        return pattern.template accessComponent<component::GeneralAlphabet>().get().count(symbol);
    }

    /**
     * Subtree wildcard needs to have zero arity and it needs to be different from any nonlinear variable of the tree.
     *
     * \param pattern the tested pattern
     * \param symbol the tested symbol
     *
     * \throws TreeException if the symbol does not have zero arity
     */
    static void valid(const tree::RankedNonlinearPattern<SymbolType>& pattern, const common::ranked_symbol<SymbolType>& symbol)
    {
        if (symbol.getRank() != 0)
            throw tree::TreeException("SubtreeWildcard symbol has nonzero arity");

        if (pattern.template accessComponent<component::NonlinearAlphabet>().get().count(symbol))
            throw tree::TreeException("Symbol " + ext::to_string(symbol) + "cannot be set as subtree wildcard since it is already nonlinear variable");
    }
};

template <class SymbolType>
struct type_util<tree::RankedNonlinearPattern<SymbolType>> {
    static tree::RankedNonlinearPattern<SymbolType> denormalize(tree::RankedNonlinearPattern<>&& value)
    {
        common::ranked_symbol<SymbolType> wildcard = alphabet::SymbolDenormalize::denormalizeRankedSymbol<SymbolType>(std::move(value).getSubtreeWildcard());
        ext::set<common::ranked_symbol<SymbolType>> nonlinearAlphabet = alphabet::SymbolDenormalize::denormalizeRankedAlphabet<SymbolType>(std::move(value).getNonlinearVariables());
        ext::set<common::ranked_symbol<SymbolType>> alphabet = alphabet::SymbolDenormalize::denormalizeRankedAlphabet<SymbolType>(std::move(value).getAlphabet());
        ext::tree<common::ranked_symbol<SymbolType>> content = tree::TreeDenormalize::denormalizeRankedTree<SymbolType>(std::move(value).getContent());
        return tree::RankedNonlinearPattern<SymbolType>(std::move(wildcard), std::move(nonlinearAlphabet), std::move(alphabet), std::move(content));
    }

    static tree::RankedNonlinearPattern<> normalize(tree::RankedNonlinearPattern<SymbolType>&& value)
    {
        common::ranked_symbol<DefaultSymbolType> wildcard = alphabet::SymbolNormalize::normalizeRankedSymbol(std::move(value).getSubtreeWildcard());
        ext::set<common::ranked_symbol<DefaultSymbolType>> nonlinearAlphabet = alphabet::SymbolNormalize::normalizeRankedAlphabet(std::move(value).getNonlinearVariables());
        ext::set<common::ranked_symbol<DefaultSymbolType>> alphabet = alphabet::SymbolNormalize::normalizeRankedAlphabet(std::move(value).getAlphabet());
        ext::tree<common::ranked_symbol<DefaultSymbolType>> content = tree::TreeNormalize::normalizeRankedTree(std::move(value).getContent());
        return tree::RankedNonlinearPattern<>(std::move(wildcard), std::move(nonlinearAlphabet), std::move(alphabet), std::move(content));
    }

    static std::unique_ptr<type_details_base> type(const tree::RankedNonlinearPattern<SymbolType>& arg)
    {
        core::unique_ptr_set<type_details_base> subTypesSymbol;
        for (const common::ranked_symbol<SymbolType>& item : arg.getAlphabet())
            subTypesSymbol.insert(type_util<SymbolType>::type(item.getSymbol()));

        std::vector<std::unique_ptr<type_details_base>> sub_types_vec;
        sub_types_vec.push_back(type_details_variant_type::make_variant(std::move(subTypesSymbol)));
        return std::make_unique<type_details_template>("tree::RankedNonlinearPattern", std::move(sub_types_vec));
    }
};

template <class SymbolType>
struct type_details_retriever<tree::RankedNonlinearPattern<SymbolType>> {
    static std::unique_ptr<type_details_base> get()
    {
        std::vector<std::unique_ptr<type_details_base>> sub_types_vec;
        sub_types_vec.push_back(type_details_retriever<SymbolType>::get());
        return std::make_unique<type_details_template>("tree::RankedNonlinearPattern", std::move(sub_types_vec));
    }
};

} /* namespace core */

extern template class tree::RankedNonlinearPattern<>;
extern template class abstraction::ValueHolder<tree::RankedNonlinearPattern<>>;
extern template const tree::RankedNonlinearPattern<>& abstraction::retrieveValue<const tree::RankedNonlinearPattern<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
extern template class registration::DenormalizationRegisterImpl<const tree::RankedNonlinearPattern<>&>;
extern template class registration::NormalizationRegisterImpl<tree::RankedNonlinearPattern<>>;
