#include "PrefixRankedTree.h"

#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>
#include <registration/ValuePrinterRegistration.hpp>

template class tree::PrefixRankedTree<>;
template class abstraction::ValueHolder<tree::PrefixRankedTree<>>;
template const tree::PrefixRankedTree<>& abstraction::retrieveValue<const tree::PrefixRankedTree<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const tree::PrefixRankedTree<>&>;
template class registration::NormalizationRegisterImpl<tree::PrefixRankedTree<>>;

namespace {

auto components = registration::ComponentRegister<tree::PrefixRankedTree<>>();

auto valuePrinter = registration::ValuePrinterRegister<tree::PrefixRankedTree<>>();

auto PrefixRankedTreeFromRankedTree = registration::CastRegister<tree::PrefixRankedTree<>, tree::RankedTree<>>();
auto PrefixRankedTreeFromPostfixRankedTree = registration::CastRegister<tree::PrefixRankedTree<>, tree::PostfixRankedTree<>>();

auto LinearStringFromPrefixRankedTree = registration::CastRegister<string::LinearString<common::ranked_symbol<>>, tree::PrefixRankedTree<>>();

} /* namespace */
