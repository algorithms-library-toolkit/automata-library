#include "UnorderedRankedTree.h"

#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>
#include <registration/ValuePrinterRegistration.hpp>

template class tree::UnorderedRankedTree<>;
template class abstraction::ValueHolder<tree::UnorderedRankedTree<>>;
template const tree::UnorderedRankedTree<>& abstraction::retrieveValue<const tree::UnorderedRankedTree<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const tree::UnorderedRankedTree<>&>;
template class registration::NormalizationRegisterImpl<tree::UnorderedRankedTree<>>;

namespace {

auto components = registration::ComponentRegister<tree::UnorderedRankedTree<>>();

auto valuePrinter = registration::ValuePrinterRegister<tree::UnorderedRankedTree<>>();

auto UnorderedRankedTreeFromRankedTree = registration::CastRegister<tree::UnorderedRankedTree<>, tree::RankedTree<>>();

} /* namespace */
