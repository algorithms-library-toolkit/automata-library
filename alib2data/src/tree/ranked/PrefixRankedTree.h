/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <common/DefaultSymbolType.h>

namespace tree {

template <class SymbolType = DefaultSymbolType>
class PrefixRankedTree;

} /* namespace tree */

#include <ext/algorithm>

#include <alib/deque>
#include <alib/set>
#include <alib/tree>
#include <alib/vector>

#include <common/ranked_symbol.hpp>
#include <core/modules.hpp>

#include <tree/TreeException.h>

#include <alphabet/common/SymbolDenormalize.h>
#include <alphabet/common/SymbolNormalize.h>
#include <core/type_details_base.hpp>
#include <core/type_util.hpp>
#include <tree/common/TreeDenormalize.h>
#include <tree/common/TreeNormalize.h>

#include <string/LinearString.h>

#include "RankedTree.h"

#include <registration/DenormalizationRegistration.hpp>
#include <registration/NormalizationRegistration.hpp>

namespace tree {

/**
 * \brief
 * Tree structure represented as linear sequece as result of preorder traversal. The representation is so called ranked, therefore it consists of ranked symbols. The rank of the ranked symbol needs to be compatible with unsigned integer.

 * \details
 * T = (A, C),
 * A (Alphabet) = finite set of ranked symbols,
 * C (Content) = linear representation of the tree content
 *
 * \tparam SymbolType used for the symbol part of the ranked symbol
 */
template <class SymbolType>
class PrefixRankedTree final : public core::Components<PrefixRankedTree<SymbolType>, ext::set<common::ranked_symbol<SymbolType>>, module::Set, component::GeneralAlphabet> {
    /**
     * Linear representation of the tree content.
     */
    ext::vector<common::ranked_symbol<SymbolType>> m_Data;

    /**
     * Checker of validity of the representation of the tree
     *
     * \throws TreeException when new tree representation is not valid
     */
    void arityChecksum(const ext::vector<common::ranked_symbol<SymbolType>>& data);

public:
    /**
     * \brief Creates a new instance of the tree with concrete alphabet and content.
     *
     * \param alphabet the initial alphabet of the tree
     * \param data the initial tree in linear representation
     */
    explicit PrefixRankedTree(ext::set<common::ranked_symbol<SymbolType>> alphabet, ext::vector<common::ranked_symbol<SymbolType>> data);

    /**
     * \brief Creates a new instance of the tree based on the content, the alphabet is implicitly created from the content.
     *
     * \param data the initial tree in linear representation
     */
    explicit PrefixRankedTree(ext::vector<common::ranked_symbol<SymbolType>> data);

    /**
     * \brief Creates a new instance of the tree based on the RankedTree. The linear representation is constructed by preorder traversal on the tree parameter.
     *
     * \param tree RankedTree representation of a tree.
     */
    explicit PrefixRankedTree(const RankedTree<SymbolType>& tree);

    /**
     * \brief Creates a new instance of the tree based on the PostfixRankedTree. The linear representation is constructed by transforming postfix notation to prefix notation.
     *
     * \param tree RankedTree representation of a tree.
     */
    explicit PrefixRankedTree(const PostfixRankedTree<SymbolType>& other);

    /**
     * Getter of the alphabet.
     *
     * \returns the alphabet of the tree
     */
    const ext::set<common::ranked_symbol<SymbolType>>& getAlphabet() const&
    {
        return this->template accessComponent<component::GeneralAlphabet>().get();
    }

    /**
     * Getter of the alphabet.
     *
     * \returns the alphabet of the tree
     */
    ext::set<common::ranked_symbol<SymbolType>>&& getAlphabet() &&
    {
        return std::move(this->template accessComponent<component::GeneralAlphabet>().get());
    }

    /**
     * Adder of an alphabet symbols.
     *
     * \param symbols the new symbols to be added to the alphabet
     */
    void extendAlphabet(const ext::set<common::ranked_symbol<SymbolType>>& symbols)
    {
        this->template accessComponent<component::GeneralAlphabet>().add(symbols);
    }

    /**
     * Getter of the tree representation.
     *
     * \return List of symbols forming the linear representation of the tree.
     */
    const ext::vector<common::ranked_symbol<SymbolType>>& getContent() const&;

    /**
     * Getter of the tree representation.
     *
     * \return List of symbols forming the linear representation of the tree.
     */
    ext::vector<common::ranked_symbol<SymbolType>>&& getContent() &&;

    /**
     * Setter of the representation of the tree.
     *
     * \throws TreeException when new tree representation is not valid or when symbol of the representation are not present in the alphabet
     *
     * \param data new List of symbols forming the representation of the tree.
     */
    void setContent(ext::vector<common::ranked_symbol<SymbolType>> data);

    /**
     * @return true if tree is an empty word (vector length is 0). The method is present to allow compatibility with strings. Tree is never empty in this datatype.
     */
    bool isEmpty() const;

    /**
     * The three way comparison implementation
     *
     * \param other the other instance
     *
     * \returns the ordering between this object and the @p other.
     */
    auto operator<=>(const PrefixRankedTree& other) const
    {
        return std::tie(m_Data, getAlphabet()) <=> std::tie(other.m_Data, other.getAlphabet());
    }

    /**
     * The equality comparison implementation.
     *
     * \param other the other object to compare with.
     *
     * \returns true if this and other objects are equal, false othervise
     */
    bool operator==(const PrefixRankedTree& other) const
    {
        return std::tie(m_Data, getAlphabet()) == std::tie(other.m_Data, other.getAlphabet());
    }

    /**
     * Print this object as raw representation to ostream.
     *
     * \param out ostream where to print
     * \param instance object to print
     *
     * \returns modified output stream
     */
    friend ext::ostream& operator<<(ext::ostream& out, const PrefixRankedTree& instance)
    {
        out << "(PrefixRankedTree";
        out << " alphabet = " << instance.getAlphabet();
        out << " content = " << instance.getContent();
        out << ")";
        return out;
    }

    /**
     * \brief Creates a new instance of the string from a linear representation of a tree
     *
     * \returns tree casted to string
     */
    explicit operator string::LinearString<common::ranked_symbol<SymbolType>>() const
    {
        return string::LinearString<common::ranked_symbol<SymbolType>>(getAlphabet(), getContent());
    }
};

template <class SymbolType>
PrefixRankedTree<SymbolType>::PrefixRankedTree(ext::set<common::ranked_symbol<SymbolType>> alphabet, ext::vector<common::ranked_symbol<SymbolType>> data)
    : core::Components<PrefixRankedTree, ext::set<common::ranked_symbol<SymbolType>>, module::Set, component::GeneralAlphabet>(std::move(alphabet))
{
    setContent(std::move(data));
}

template <class SymbolType>
PrefixRankedTree<SymbolType>::PrefixRankedTree(ext::vector<common::ranked_symbol<SymbolType>> data)
    : PrefixRankedTree(ext::set<common::ranked_symbol<SymbolType>>(data.begin(), data.end()), data)
{
}

template <class SymbolType>
PrefixRankedTree<SymbolType>::PrefixRankedTree(const RankedTree<SymbolType>& tree)
    : PrefixRankedTree(tree.getAlphabet(), TreeAuxiliary::treeToPrefix(tree.getContent()))
{
}

template <class SymbolType>
PrefixRankedTree<SymbolType>::PrefixRankedTree(const PostfixRankedTree<SymbolType>& other)
    : PrefixRankedTree(TreeAuxiliary::postfixToPrefix(other.getContent()))
{
}

template <class SymbolType>
const ext::vector<common::ranked_symbol<SymbolType>>& PrefixRankedTree<SymbolType>::getContent() const&
{
    return this->m_Data;
}

template <class SymbolType>
ext::vector<common::ranked_symbol<SymbolType>>&& PrefixRankedTree<SymbolType>::getContent() &&
{
    return std::move(this->m_Data);
}

template <class SymbolType>
void PrefixRankedTree<SymbolType>::setContent(ext::vector<common::ranked_symbol<SymbolType>> data)
{
    arityChecksum(data);

    ext::set<common::ranked_symbol<SymbolType>> minimalAlphabet(data.begin(), data.end());
    std::set_difference(minimalAlphabet.begin(), minimalAlphabet.end(), getAlphabet().begin(), getAlphabet().end(), ext::callback_iterator([](const common::ranked_symbol<SymbolType>&) {
                            throw TreeException("Input symbols not in the alphabet.");
                        }));

    this->m_Data = std::move(data);
}

template <class SymbolType>
void PrefixRankedTree<SymbolType>::arityChecksum(const ext::vector<common::ranked_symbol<SymbolType>>& data)
{
    if (std::accumulate(data.begin(), data.end(), 1, [](int current, const common::ranked_symbol<SymbolType>& symbol) {
            return current + symbol.getRank() - 1;
        })
        != 0)
        throw TreeException("The string does not form a tree");
}

template <class SymbolType>
bool PrefixRankedTree<SymbolType>::isEmpty() const
{
    return this->m_Data.empty();
}

} /* namespace tree */

namespace core {

/**
 * Helper class specifying constraints for the tree's internal alphabet component.
 *
 * \tparam SymbolType used for the symbol part of the ranked symbols of the alphabet of the pattern.
 */
template <class SymbolType>
class SetConstraint<tree::PrefixRankedTree<SymbolType>, common::ranked_symbol<SymbolType>, component::GeneralAlphabet> {
public:
    /**
     * Returns true if the symbol is still used in the tree.
     *
     * \param tree the tested tree
     * \param symbol the tested symbol
     *
     * \returns true if the symbol is used, false othervise
     */
    static bool used(const tree::PrefixRankedTree<SymbolType>& tree, const common::ranked_symbol<SymbolType>& symbol)
    {
        const ext::vector<common::ranked_symbol<SymbolType>>& content = tree.getContent();

        return std::find(content.begin(), content.end(), symbol) != content.end();
    }

    /**
     * Returns true as all symbols are possibly available to be in an alphabet.
     *
     * \param tree the tested tree
     * \param symbol the tested symbol
     *
     * \returns true
     */
    static bool available(const tree::PrefixRankedTree<SymbolType>&, const common::ranked_symbol<SymbolType>&)
    {
        return true;
    }

    /**
     * All symbols are valid as symbols of an alphabet.
     *
     * \param tree the tested tree
     * \param symbol the tested symbol
     */
    static void valid(const tree::PrefixRankedTree<SymbolType>&, const common::ranked_symbol<SymbolType>&)
    {
    }
};

template <class SymbolType>
struct type_util<tree::PrefixRankedTree<SymbolType>> {
    static tree::PrefixRankedTree<SymbolType> denormalize(tree::PrefixRankedTree<>&& value)
    {
        ext::set<common::ranked_symbol<SymbolType>> alphabet = alphabet::SymbolDenormalize::denormalizeRankedAlphabet<SymbolType>(std::move(value).getAlphabet());
        ext::vector<common::ranked_symbol<SymbolType>> content = alphabet::SymbolDenormalize::denormalizeRankedSymbols<SymbolType>(std::move(value).getContent());
        return tree::PrefixRankedTree<SymbolType>(std::move(alphabet), std::move(content));
    }

    static tree::PrefixRankedTree<> normalize(tree::PrefixRankedTree<SymbolType>&& value)
    {
        ext::set<common::ranked_symbol<DefaultSymbolType>> alphabet = alphabet::SymbolNormalize::normalizeRankedAlphabet(std::move(value).getAlphabet());
        ext::vector<common::ranked_symbol<DefaultSymbolType>> content = alphabet::SymbolNormalize::normalizeRankedSymbols(std::move(value).getContent());
        return tree::PrefixRankedTree<>(std::move(alphabet), std::move(content));
    }

    static std::unique_ptr<type_details_base> type(const tree::PrefixRankedTree<SymbolType>& arg)
    {
        core::unique_ptr_set<type_details_base> subTypesSymbol;
        for (const common::ranked_symbol<SymbolType>& item : arg.getAlphabet())
            subTypesSymbol.insert(type_util<SymbolType>::type(item.getSymbol()));

        std::vector<std::unique_ptr<type_details_base>> sub_types_vec;
        sub_types_vec.push_back(type_details_variant_type::make_variant(std::move(subTypesSymbol)));
        return std::make_unique<type_details_template>("tree::PrefixRankedTree", std::move(sub_types_vec));
    }
};

template <class SymbolType>
struct type_details_retriever<tree::PrefixRankedTree<SymbolType>> {
    static std::unique_ptr<type_details_base> get()
    {
        std::vector<std::unique_ptr<type_details_base>> sub_types_vec;
        sub_types_vec.push_back(type_details_retriever<SymbolType>::get());
        return std::make_unique<type_details_template>("tree::PrefixRankedTree", std::move(sub_types_vec));
    }
};

} /* namespace core */

extern template class tree::PrefixRankedTree<>;
extern template class abstraction::ValueHolder<tree::PrefixRankedTree<>>;
extern template const tree::PrefixRankedTree<>& abstraction::retrieveValue<const tree::PrefixRankedTree<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
extern template class registration::DenormalizationRegisterImpl<const tree::PrefixRankedTree<>&>;
extern template class registration::NormalizationRegisterImpl<tree::PrefixRankedTree<>>;
