#include "PrefixRankedNonlinearPattern.h"

#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>
#include <registration/ValuePrinterRegistration.hpp>

template class tree::PrefixRankedNonlinearPattern<>;
template class abstraction::ValueHolder<tree::PrefixRankedNonlinearPattern<>>;
template const tree::PrefixRankedNonlinearPattern<>& abstraction::retrieveValue<const tree::PrefixRankedNonlinearPattern<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const tree::PrefixRankedNonlinearPattern<>&>;
template class registration::NormalizationRegisterImpl<tree::PrefixRankedNonlinearPattern<>>;

namespace {

auto components = registration::ComponentRegister<tree::PrefixRankedNonlinearPattern<>>();

auto valuePrinter = registration::ValuePrinterRegister<tree::PrefixRankedNonlinearPattern<>>();

auto PrefixRankedNonlinearPatternFromRankedTree = registration::CastRegister<tree::PrefixRankedNonlinearPattern<>, tree::RankedTree<>>();
auto PrefixRankedNonlinearPatternFromRankedPattern = registration::CastRegister<tree::PrefixRankedNonlinearPattern<>, tree::RankedPattern<>>();
auto PrefixRankedNonlinearPatternFromRankedNonlinearPattern = registration::CastRegister<tree::PrefixRankedNonlinearPattern<>, tree::RankedNonlinearPattern<>>();
auto PrefixRankedNonlinearPatternFromPrefixRankedTree = registration::CastRegister<tree::PrefixRankedNonlinearPattern<>, tree::PrefixRankedTree<>>();
auto PrefixRankedNonlinearPatternFromPrefixRankedPattern = registration::CastRegister<tree::PrefixRankedNonlinearPattern<>, tree::PrefixRankedPattern<>>();

auto LinearStringFromPrefixRankedNonlinearPattern = registration::CastRegister<string::LinearString<common::ranked_symbol<>>, tree::PrefixRankedNonlinearPattern<>>();

} /* namespace */
