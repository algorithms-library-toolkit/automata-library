#include "PrefixRankedPattern.h"

#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>
#include <registration/ValuePrinterRegistration.hpp>

template class tree::PrefixRankedPattern<>;
template class abstraction::ValueHolder<tree::PrefixRankedPattern<>>;
template const tree::PrefixRankedPattern<>& abstraction::retrieveValue<const tree::PrefixRankedPattern<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const tree::PrefixRankedPattern<>&>;
template class registration::NormalizationRegisterImpl<tree::PrefixRankedPattern<>>;

namespace {

auto components = registration::ComponentRegister<tree::PrefixRankedPattern<>>();

auto valuePrinter = registration::ValuePrinterRegister<tree::PrefixRankedPattern<>>();

auto PrefixRankedPatternFromRankedPattern = registration::CastRegister<tree::PrefixRankedPattern<>, tree::RankedPattern<>>();
auto PrefixRankedPatternFromPrefixRankedTree = registration::CastRegister<tree::PrefixRankedPattern<>, tree::PrefixRankedTree<>>();

auto LinearStringFromPrefixRankedPattern = registration::CastRegister<string::LinearString<common::ranked_symbol<>>, tree::PrefixRankedPattern<>>();

} /* namespace */
