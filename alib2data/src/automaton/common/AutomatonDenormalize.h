#pragma once

#include <exception>

#include <alib/deque>
#include <alib/map>
#include <alib/multiset>
#include <alib/set>
#include <alib/vector>

#include <alphabet/common/SymbolDenormalize.h>
#include <regexp/common/RegExpDenormalize.h>
#include <regexp/unbounded/UnboundedRegExpStructure.h>
#include <rte/common/RTEDenormalizeVariant.h>

#include <common/DefaultStateType.h>
#include <common/DefaultSymbolType.h>
#include <common/symbol_or_epsilon.hpp>

namespace automaton {

/**
 * This class contains methods to print XML representation of automata to the output stream.
 */
class AutomatonDenormalize {
public:
    template <class StateType>
    static ext::multiset<StateType> denormalizeStates(ext::multiset<DefaultStateType>&& states);

    template <class StateType>
    static ext::set<StateType> denormalizeStates(ext::set<DefaultStateType>&& states);

    template <class StateType>
    static ext::vector<StateType> denormalizeStates(ext::vector<DefaultStateType>&& states);

    template <class StateType>
    static StateType denormalizeState(DefaultStateType&& state);

    template <class SymbolType>
    static common::symbol_or_epsilon<SymbolType> denormalizeSymbolEpsilon(common::symbol_or_epsilon<DefaultSymbolType>&& symbol);

    template <class SymbolType>
    static regexp::UnboundedRegExpStructure<SymbolType> denormalizeRegExp(regexp::UnboundedRegExpStructure<DefaultSymbolType>&& regexp);

    template <class SymbolType, class StateType>
    static rte::FormalRTEStructure<ext::variant<SymbolType, StateType>> denormalizeRTE(rte::FormalRTEStructure<ext::variant<DefaultSymbolType, DefaultStateType>>&& rte);
};

template <class StateType>
ext::multiset<StateType> AutomatonDenormalize::denormalizeStates(ext::multiset<DefaultStateType>&& states)
{
    ext::multiset<StateType> res;
    for (DefaultStateType&& state : ext::make_mover(states))
        res.insert(denormalizeState<StateType>(std::move(state)));

    return res;
}

template <class StateType>
ext::set<StateType> AutomatonDenormalize::denormalizeStates(ext::set<DefaultStateType>&& states)
{
    ext::set<StateType> res;
    for (DefaultStateType&& state : ext::make_mover(states))
        res.insert(denormalizeState<StateType>(std::move(state)));

    return res;
}

template <class StateType>
ext::vector<StateType> AutomatonDenormalize::denormalizeStates(ext::vector<DefaultStateType>&& states)
{
    ext::vector<StateType> res;
    for (DefaultStateType& state : states)
        res.push_back(denormalizeState<StateType>(std::move(state)));

    return res;
}

template <class StateType>
StateType AutomatonDenormalize::denormalizeState(DefaultStateType&& state)
{
    return factory::NormalizeFactory::denormalize<StateType>(std::move(state));
}

template <class SymbolType> // FIXME why is it here? and not SymbolDenormalize?
common::symbol_or_epsilon<SymbolType> AutomatonDenormalize::denormalizeSymbolEpsilon(common::symbol_or_epsilon<DefaultSymbolType>&& symbol)
{
    if (symbol.is_epsilon())
        return common::symbol_or_epsilon<SymbolType>();
    else
        return common::symbol_or_epsilon<SymbolType>(alphabet::SymbolDenormalize::denormalizeSymbol<SymbolType>(std::move(symbol).getSymbol()));
}

template <class SymbolType>
regexp::UnboundedRegExpStructure<SymbolType> AutomatonDenormalize::denormalizeRegExp(regexp::UnboundedRegExpStructure<DefaultSymbolType>&& regexp)
{
    return regexp::UnboundedRegExpStructure<SymbolType>(std::move(*regexp::RegExpDenormalize::denormalize<SymbolType>(std::move(regexp).getStructure())));
}

template <class SymbolType, class StateType>
rte::FormalRTEStructure<ext::variant<SymbolType, StateType>> AutomatonDenormalize::denormalizeRTE(rte::FormalRTEStructure<ext::variant<DefaultSymbolType, DefaultStateType>>&& rte)
{
    return rte::FormalRTEStructure<ext::variant<SymbolType, StateType>>(std::move(*rte::RTEDenormalizeVariant::denormalize<SymbolType, StateType>(std::move(rte).getStructure())));
}

} /* namespace automaton */
