#include "NPDTA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::NPDTA<>;
template class abstraction::ValueHolder<automaton::NPDTA<>>;
template const automaton::NPDTA<>& abstraction::retrieveValue<const automaton::NPDTA<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const automaton::NPDTA<>&>;
template class registration::NormalizationRegisterImpl<automaton::NPDTA<>>;

namespace {

auto valuePrinter = registration::ValuePrinterRegister<automaton::NPDTA<>>();

} /* namespace */
