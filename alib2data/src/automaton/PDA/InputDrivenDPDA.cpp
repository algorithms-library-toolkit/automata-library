#include "InputDrivenDPDA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::InputDrivenDPDA<>;
template class abstraction::ValueHolder<automaton::InputDrivenDPDA<>>;
template const automaton::InputDrivenDPDA<>& abstraction::retrieveValue<const automaton::InputDrivenDPDA<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const automaton::InputDrivenDPDA<>&>;
template class registration::NormalizationRegisterImpl<automaton::InputDrivenDPDA<>>;

namespace {

auto valuePrinter = registration::ValuePrinterRegister<automaton::InputDrivenDPDA<>>();

} /* namespace */
