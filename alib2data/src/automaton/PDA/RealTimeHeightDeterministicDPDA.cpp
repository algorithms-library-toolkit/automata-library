#include "RealTimeHeightDeterministicDPDA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::RealTimeHeightDeterministicDPDA<>;
template class abstraction::ValueHolder<automaton::RealTimeHeightDeterministicDPDA<>>;
template const automaton::RealTimeHeightDeterministicDPDA<>& abstraction::retrieveValue<const automaton::RealTimeHeightDeterministicDPDA<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const automaton::RealTimeHeightDeterministicDPDA<>&>;
template class registration::NormalizationRegisterImpl<automaton::RealTimeHeightDeterministicDPDA<>>;

namespace {

auto valuePrinter = registration::ValuePrinterRegister<automaton::RealTimeHeightDeterministicDPDA<>>();

} /* namespace */
