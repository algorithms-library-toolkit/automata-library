/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <ext/algorithm>

#include <alib/multimap>
#include <alib/set>
#include <alib/vector>

#include <core/modules.hpp>

#include <common/DefaultStateType.h>
#include <common/DefaultSymbolType.h>

#include <automaton/AutomatonException.h>

#include <alphabet/common/SymbolDenormalize.h>
#include <alphabet/common/SymbolNormalize.h>
#include <automaton/common/AutomatonDenormalize.h>
#include <automaton/common/AutomatonNormalize.h>
#include <core/type_details_base.hpp>
#include <core/type_util.hpp>

#include <registration/DenormalizationRegistration.hpp>
#include <registration/NormalizationRegistration.hpp>

namespace automaton {

/**
 * \brief
 * Nondeterministic visibly pushdown automaton. Accepts subset of context free languages.

 * \details
 * Definition
 * A = (Q, T = C \cup R \cup L, G, I, Z, \delta, F),
 * Q (States) = nonempty finite set of states,
 * T (TerminalAlphabet) = finite set of terminal symbols split to three disjoint parts
 *   - C (CallAlphabet) = symbols used with transitions increasing the pushdown store,
 *   - R (ReturnAlphabet) = symbols used with transitions decreasing the pushdown store,
 *   - L (LocalAlphabet) = symbols used with transitions leaving the height of the pushdown store unchanged
 * G (PushdownStoreAlphabet) = finite set of pushdown store symbol
 * I (InitialStates) = set of initial states,
 * Z (BottomOfTheStackSymbol) = initial pushdown store symbol
 * \delta is split to three disjoint parts
 *   - \delta_{call} of the form A \times c -> B \times g, where A, B \in Q, a \in C, and g \in G
 *   - \delta_{return} of the form A \times r \times g -> B, where A, B \in Q, r \in C, and g \in G
 *   - \delta_{local} of the form A \times l -> B, where A, B \in Q, a \in C
 * F (FinalStates) = set of final states
 *
 * \tparam InputSymbolTypeT used for the terminal alphabet
 * \tparam PushdownSymbolTypeT used for the pushdown store alphabet
 * \tparam StateTypeT used to the states, and the initial state of the automaton.
 */
template <class InputSymbolTypeT = DefaultSymbolType, class PushdownStoreSymbolTypeT = DefaultSymbolType, class StateTypeT = DefaultStateType>
class VisiblyPushdownNPDA final : public core::Components<VisiblyPushdownNPDA<InputSymbolTypeT, PushdownStoreSymbolTypeT, StateTypeT>, ext::set<InputSymbolTypeT>, module::Set, std::tuple<component::CallAlphabet, component::ReturnAlphabet, component::LocalAlphabet>, ext::set<PushdownStoreSymbolTypeT>, module::Set, component::PushdownStoreAlphabet, PushdownStoreSymbolTypeT, module::Value, component::BottomOfTheStackSymbol, ext::set<StateTypeT>, module::Set, std::tuple<component::States, component::InitialStates, component::FinalStates>> {
public:
    typedef InputSymbolTypeT InputSymbolType;
    typedef PushdownStoreSymbolTypeT PushdownStoreSymbolType;
    typedef StateTypeT StateType;

private:
    /**
     * Call transition function as mapping from a state times an input symbol on the left hand side to a state times pushdown store symbol on the right hand side.
     */
    ext::multimap<ext::pair<StateType, InputSymbolType>, ext::pair<StateType, PushdownStoreSymbolType>> callTransitions;

    /**
     * Return transition function as mapping from a state times an input symbol times pushdown store symbol on the left hand side to a state on the right hand side.
     */
    ext::multimap<ext::tuple<StateType, InputSymbolType, PushdownStoreSymbolType>, StateType> returnTransitions;

    /**
     * Local transition function as mapping from a state times an input symbol on the left hand side to a state on the right hand side.
     */
    ext::multimap<ext::pair<StateType, InputSymbolType>, StateType> localTransitions;

public:
    /**
     * \brief Creates a new instance of the automaton with a concrete set of states, call, return, and local alphabets, pushdown store alphabet, initial state, bottom of the stack symbol, and a set of final states.
     *
     * \param states the initial set of states of the automaton
     * \param callAlphabet the initial input alphabet
     * \param returnAlphabet the initial input alphabet
     * \param localAlphabet the initial input alphabet
     * \param pushdownStoreAlphabet the initial set of symbols used in the pushdown store by the automaton
     * \param initialState the initial state of the automaton
     * \param bottomOfTheStackSymbol the initial pushdown symbol of the automaton
     * \param finalStates the initial set of final states of the automaton
     */
    explicit VisiblyPushdownNPDA(ext::set<StateType> states, ext::set<InputSymbolType> callAlphabet, ext::set<InputSymbolType> returnAlphabet, ext::set<InputSymbolType> localAlphabet, ext::set<PushdownStoreSymbolType> pushdownStoreAlphabet, ext::set<StateType> initialStates, PushdownStoreSymbolType bottomOfTheStackSymbol, ext::set<StateType> finalStates);

    /**
     * \brief Creates a new instance of the automaton with a concrete initial state and bottom of the stack symbol.
     *
     * \param initialState the initial state of the automaton
     * \param bottomOfTheStackSymbol the bottom of the stack symbol of the automaton
     */
    explicit VisiblyPushdownNPDA(PushdownStoreSymbolType bottomOfTheStackSymbol);

    /**
     * Getter of initial states.
     *
     * \returns the initial states of the automaton
     */
    const ext::set<StateType>& getInitialStates() const&
    {
        return this->template accessComponent<component::InitialStates>().get();
    }

    /**
     * Getter of initial states.
     *
     * \returns the initial states of the automaton
     */
    ext::set<StateType>&& getInitialStates() &&
    {
        return std::move(this->template accessComponent<component::InitialStates>().get());
    }

    /**
     * Adder of a initial state.
     *
     * \param state the new state to be added to a set of initial states
     *
     * \returns true if the state was indeed added
     */
    bool addInitialState(StateType state)
    {
        return this->template accessComponent<component::InitialStates>().add(std::move(state));
    }

    /**
     * Setter of initial states.
     *
     * \param states completely new set of initial states
     */
    void setInitialStates(ext::set<StateType> states)
    {
        this->template accessComponent<component::InitialStates>().set(std::move(states));
    }

    /**
     * Remover of a initial state.
     *
     * \param state a state to be removed from a set of initial states
     *
     * \returns true if the state was indeed removed
     */
    bool removeInitialState(const StateType& state)
    {
        return this->template accessComponent<component::InitialStates>().remove(state);
    }

    /**
     * Getter of states.
     *
     * \returns the states of the automaton
     */
    const ext::set<StateType>& getStates() const&
    {
        return this->template accessComponent<component::States>().get();
    }

    /**
     * Getter of states.
     *
     * \returns the states of the automaton
     */
    ext::set<StateType>&& getStates() &&
    {
        return std::move(this->template accessComponent<component::States>().get());
    }

    /**
     * Adder of a state.
     *
     * \param state the new state to be added to a set of states
     *
     * \returns true if the state was indeed added
     */
    bool addState(StateType state)
    {
        return this->template accessComponent<component::States>().add(std::move(state));
    }

    /**
     * Setter of states.
     *
     * \param states completely new set of states
     */
    void setStates(ext::set<StateType> states)
    {
        this->template accessComponent<component::States>().set(std::move(states));
    }

    /**
     * Remover of a state.
     *
     * \param state a state to be removed from a set of states
     *
     * \returns true if the state was indeed removed
     */
    bool removeState(const StateType& state)
    {
        return this->template accessComponent<component::States>().remove(state);
    }

    /**
     * Getter of final states.
     *
     * \returns the final states of the automaton
     */
    const ext::set<StateType>& getFinalStates() const&
    {
        return this->template accessComponent<component::FinalStates>().get();
    }

    /**
     * Getter of final states.
     *
     * \returns the final states of the automaton
     */
    ext::set<StateType>&& getFinalStates() &&
    {
        return std::move(this->template accessComponent<component::FinalStates>().get());
    }

    /**
     * Adder of a final state.
     *
     * \param state the new state to be added to a set of final states
     *
     * \returns true if the state was indeed added
     */
    bool addFinalState(StateType state)
    {
        return this->template accessComponent<component::FinalStates>().add(std::move(state));
    }

    /**
     * Setter of final states.
     *
     * \param states completely new set of final states
     */
    void setFinalStates(ext::set<StateType> states)
    {
        this->template accessComponent<component::FinalStates>().set(std::move(states));
    }

    /**
     * Remover of a final state.
     *
     * \param state a state to be removed from a set of final states
     *
     * \returns true if the state was indeed removed
     */
    bool removeFinalState(const StateType& state)
    {
        return this->template accessComponent<component::FinalStates>().remove(state);
    }

    /**
     * Getter of the pushdown store alphabet.
     *
     * \returns the pushdown store alphabet of the automaton
     */
    const ext::set<PushdownStoreSymbolType>& getPushdownStoreAlphabet() const&
    {
        return this->template accessComponent<component::PushdownStoreAlphabet>().get();
    }

    /**
     * Getter of the pushdown store alphabet.
     *
     * \returns the pushdown store alphabet of the automaton
     */
    ext::set<PushdownStoreSymbolType>&& getPushdownStoreAlphabet() &&
    {
        return std::move(this->template accessComponent<component::PushdownStoreAlphabet>().get());
    }

    /**
     * Adder of a pushdown store symbol.
     *
     * \param symbol the new symbol to be added to a pushdown store alphabet
     *
     * \returns true if the symbol was indeed added
     */
    bool addPushdownStoreSymbol(PushdownStoreSymbolType symbol)
    {
        return this->template accessComponent<component::PushdownStoreAlphabet>().add(std::move(symbol));
    }

    /**
     * Adder of pushdown store symbols.
     *
     * \param symbols new symbols to be added to a pushdown store alphabet
     */
    void addPushdownStoreSymbols(ext::set<PushdownStoreSymbolType> symbols)
    {
        this->template accessComponent<component::PushdownStoreAlphabet>().add(std::move(symbols));
    }

    /**
     * Setter of a pushdown store alphabet.
     *
     * \param symbols completely new pushdown store alphabet
     */
    void setPushdownStoreAlphabet(ext::set<PushdownStoreSymbolType> symbols)
    {
        this->template accessComponent<component::PushdownStoreAlphabet>().set(std::move(symbols));
    }

    /**
     * Remover of an pushdown store symbol.
     *
     * \param symbol a symbol to be removed from a pushdown store alphabet
     *
     * \returns true if the symbol was indeed removed
     */
    bool removePushdownStoreSymbol(const PushdownStoreSymbolType& symbol)
    {
        return this->template accessComponent<component::PushdownStoreAlphabet>().remove(symbol);
    }

    /**
     * Getter of the bottom of the stack symbol.
     *
     * \returns the bottom of the stack symbol of the automaton
     */
    const PushdownStoreSymbolType& getBottomOfTheStackSymbol() const&
    {
        return this->template accessComponent<component::BottomOfTheStackSymbol>().get();
    }

    /**
     * Getter of the bottom of the stack symbol.
     *
     * \returns the bottom of the stack symbol of the automaton
     */
    PushdownStoreSymbolType&& getBottomOfTheStackSymbol() &&
    {
        return std::move(this->template accessComponent<component::BottomOfTheStackSymbol>().get());
    }

    /**
     * Setter of the bottom of the stack symbol.
     *
     * \param symbol new bottom of the stack symbol of the automaton
     *
     * \returns true if the bottom of the stack symbol was indeed changed
     */
    bool setBottomOfTheStackSymbol(PushdownStoreSymbolType symbol)
    {
        return this->template accessComponent<component::BottomOfTheStackSymbol>().set(std::move(symbol));
    }

    /**
     * Getter of the call part of the input alphabet.
     *
     * \returns the call part of the input alphabet of the automaton
     */
    const ext::set<InputSymbolType>& getCallInputAlphabet() const&
    {
        return this->template accessComponent<component::CallAlphabet>().get();
    }

    /**
     * Getter of the call part of the input alphabet.
     *
     * \returns the the call part of the input alphabet of the automaton
     */
    ext::set<InputSymbolType>&& getCallInputAlphabet() &&
    {
        return std::move(this->template accessComponent<component::CallAlphabet>().get());
    }

    /**
     * Adder of a symbol to call part of the input alphabet.
     *
     * \param symbol the new symbol to be added to a call part of the input alphabet
     *
     * \returns true if the symbol was indeed added
     */
    bool addCallInputSymbol(InputSymbolType symbol)
    {
        return this->template accessComponent<component::CallAlphabet>().add(std::move(symbol));
    }

    /**
     * Adder of a symbol to a call part of the input alphabet.
     *
     * \param symbols new symbols to be added to a call part of the input alphabet
     */
    void addCallInputSymbols(ext::set<InputSymbolType> symbols)
    {
        this->template accessComponent<component::CallAlphabet>().add(std::move(symbols));
    }

    /**
     * Setter of a call part of the input alphabet.
     *
     * \param symbols completely new call part of the input alphabet
     */
    void setCallInputAlphabet(ext::set<InputSymbolType> symbols)
    {
        this->template accessComponent<component::CallAlphabet>().set(std::move(symbols));
    }

    /**
     * Remover of a call input symbol.
     *
     * \param symbol a symbol to be removed from a call part of the input alphabet
     *
     * \returns true if the symbol was indeed removed
     */
    bool removeCallInputSymbol(const InputSymbolType& symbol)
    {
        return this->template accessComponent<component::CallAlphabet>().remove(symbol);
    }

    /**
     * Getter of the call part of the input alphabet.
     *
     * \returns the call part of the input alphabet of the automaton
     */
    const ext::set<InputSymbolType>& getReturnInputAlphabet() const&
    {
        return this->template accessComponent<component::ReturnAlphabet>().get();
    }

    /**
     * Getter of the call part of the input alphabet.
     *
     * \returns the the call part of the input alphabet of the automaton
     */
    ext::set<InputSymbolType>&& getReturnInputAlphabet() &&
    {
        return std::move(this->template accessComponent<component::ReturnAlphabet>().get());
    }

    /**
     * Adder of a symbol to call part of the input alphabet.
     *
     * \param symbol the new symbol to be added to a call part of the input alphabet
     *
     * \returns true if the symbol was indeed added
     */
    bool addReturnInputSymbol(InputSymbolType symbol)
    {
        return this->template accessComponent<component::ReturnAlphabet>().add(std::move(symbol));
    }

    /**
     * Adder of a symbol to a call part of the input alphabet.
     *
     * \param symbols new symbols to be added to a call part of the input alphabet
     */
    void addReturnInputSymbols(ext::set<InputSymbolType> symbols)
    {
        this->template accessComponent<component::ReturnAlphabet>().add(std::move(symbols));
    }

    /**
     * Setter of a call part of the input alphabet.
     *
     * \param symbols completely new call part of the input alphabet
     */
    void setReturnInputAlphabet(ext::set<InputSymbolType> symbols)
    {
        this->template accessComponent<component::ReturnAlphabet>().set(std::move(symbols));
    }

    /**
     * Remover of a call input symbol.
     *
     * \param symbol a symbol to be removed from a call part of the input alphabet
     *
     * \returns true if the symbol was indeed removed
     */
    bool removeReturnInputSymbol(const InputSymbolType& symbol)
    {
        return this->template accessComponent<component::ReturnAlphabet>().remove(symbol);
    }

    /**
     * Getter of the call part of the input alphabet.
     *
     * \returns the call part of the input alphabet of the automaton
     */
    const ext::set<InputSymbolType>& getLocalInputAlphabet() const&
    {
        return this->template accessComponent<component::LocalAlphabet>().get();
    }

    /**
     * Getter of the call part of the input alphabet.
     *
     * \returns the the call part of the input alphabet of the automaton
     */
    ext::set<InputSymbolType>&& getLocalInputAlphabet() &&
    {
        return std::move(this->template accessComponent<component::LocalAlphabet>().get());
    }

    /**
     * Adder of a symbol to call part of the input alphabet.
     *
     * \param symbol the new symbol to be added to a call part of the input alphabet
     *
     * \returns true if the symbol was indeed added
     */
    bool addLocalInputSymbol(InputSymbolType symbol)
    {
        return this->template accessComponent<component::LocalAlphabet>().add(std::move(symbol));
    }

    /**
     * Adder of a symbol to a call part of the input alphabet.
     *
     * \param symbols new symbols to be added to a call part of the input alphabet
     */
    void addLocalInputSymbols(ext::set<InputSymbolType> symbols)
    {
        this->template accessComponent<component::LocalAlphabet>().add(std::move(symbols));
    }

    /**
     * Setter of a call part of the input alphabet.
     *
     * \param symbols completely new call part of the input alphabet
     */
    void setLocalInputAlphabet(ext::set<InputSymbolType> symbols)
    {
        this->template accessComponent<component::LocalAlphabet>().set(std::move(symbols));
    }

    /**
     * Remover of a call input symbol.
     *
     * \param symbol a symbol to be removed from a call part of the input alphabet
     *
     * \returns true if the symbol was indeed removed
     */
    bool removeLocalInputSymbol(const InputSymbolType& symbol)
    {
        return this->template accessComponent<component::LocalAlphabet>().remove(symbol);
    }

    /**
     * Remover of a input symbol.
     *
     * \param symbol a symbol to be removed from the input alphabet
     *
     * \returns true if the symbol was indeed removed
     */
    bool removeInputSymbol(const InputSymbolType& symbol)
    {
        return removeCallInputSymbol(symbol) || removeReturnInputSymbol(symbol) || removeLocalInputSymbol(symbol);
    }

    /**
     * \brief Adds a call transition to the automaton.
     *
     * \details The transition is in a form A \times a -> B \times g, where A, B \in Q, a \in C, and g \in G
     *
     * \param from the source state (A)
     * \param input the call input symbol (a)
     * \param to the target state (B)
     * \param push symbol to be pushed to the pushdown store on the transition use (g)
     *
     * \throws AutomatonException when transition contains state or symbol not present in the automaton components
     *
     * \returns true if the transition was indeed added
     */
    bool addCallTransition(StateType from, InputSymbolType input, StateType to, PushdownStoreSymbolType push);

    /**
     * \brief Adds a return transition to the automaton.
     *
     * \details The transition is in a form A \times a \times g -> B, where A, B \in Q, a \in C, and g \in G
     *
     * \param from the source state (A)
     * \param input the return input symbol (a)
     * \param pop symbol to be poped to the pushdown store on the transition use (g)
     * \param to the target state (B)
     *
     * \throws AutomatonException when transition contains state or symbol not present in the automaton components
     *
     * \returns true if the transition was indeed added
     */
    bool addReturnTransition(StateType from, InputSymbolType input, PushdownStoreSymbolType pop, StateType to);

    /**
     * \brief Adds a local transition to the automaton.
     *
     * \details The transition is in a form A \times a -> B, where A, B \in Q and a \in C
     *
     * \param from the source state (A)
     * \param input the local input symbol (a)
     * \param to the target state (B)
     *
     * \throws AutomatonException when transition contains state or symbol not present in the automaton components
     *
     * \returns true if the transition was indeed added
     */
    bool addLocalTransition(StateType from, InputSymbolType input, StateType to);

    /**
     * \brief Removes a call transition from the automaton.
     *
     * \details The transition is in a form A \times a -> B \times g, where A, B \in Q, a \in C, and g \in G
     *
     * \param from the source state (A)
     * \param input the call input symbol (a)
     * \param to the target state (B)
     * \param push symbol to be pushed to the pushdown store on the transition use (g)
     *
     * \throws AutomatonException when removed transition left hand side was found but the right hand side did not match.
     *
     * \returns true if the transition was indeed removed
     */
    bool removeCallTransition(const StateType& from, const InputSymbolType& input, const StateType& to, const PushdownStoreSymbolType& push);

    /**
     * \brief Removes a return transition from the automaton.
     *
     * \details The transition is in a form A \times a \times g -> B, where A, B \in Q, a \in C, and g \in G
     *
     * \param from the source state (A)
     * \param input the call input symbol (a)
     * \param pop symbol to be poped to the pushdown store on the transition use (g)
     * \param to the target state (B)
     *
     * \throws AutomatonException when removed transition left hand side was found but the right hand side did not match.
     *
     * \returns true if the transition was indeed removed
     */
    bool removeReturnTransition(const StateType& from, const InputSymbolType& input, const PushdownStoreSymbolType& pop, const StateType& to);

    /**
     * \brief Removes a local transition from the automaton.
     *
     * \details The transition is in a form A \times a -> B, where A, B \in Q and a \in C
     *
     * \param from the source state (A)
     * \param input the call input symbol (a)
     * \param to the target state (B)
     *
     * \throws AutomatonException when removed transition left hand side was found but the right hand side did not match.
     *
     * \returns true if the transition was indeed removed
     */
    bool removeLocalTransition(const StateType& from, const InputSymbolType& input, const StateType& to);

    /**
     * Get the call transition function of the automaton in its natural form.
     *
     * \returns call transition function of the automaton
     */
    const ext::multimap<ext::pair<StateType, InputSymbolType>, ext::pair<StateType, PushdownStoreSymbolType>>& getCallTransitions() const&;

    /**
     * Get the call transition function of the automaton in its natural form.
     *
     * \returns call transition function of the automaton
     */
    ext::multimap<ext::pair<StateType, InputSymbolType>, ext::pair<StateType, PushdownStoreSymbolType>>&& getCallTransitions() &&;

    /**
     * Get the return transition function of the automaton in its natural form.
     *
     * \returns return transition function of the automaton
     */
    const ext::multimap<ext::tuple<StateType, InputSymbolType, PushdownStoreSymbolType>, StateType>& getReturnTransitions() const&;

    /**
     * Get the return transition function of the automaton in its natural form.
     *
     * \returns return transition function of the automaton
     */
    ext::multimap<ext::tuple<StateType, InputSymbolType, PushdownStoreSymbolType>, StateType>&& getReturnTransitions() &&;

    /**
     * Get the local transition function of the automaton in its natural form.
     *
     * \returns local transition function of the automaton
     */
    const ext::multimap<ext::pair<StateType, InputSymbolType>, StateType>& getLocalTransitions() const&;

    /**
     * Get the local transition function of the automaton in its natural form.
     *
     * \returns local transition function of the automaton
     */
    ext::multimap<ext::pair<StateType, InputSymbolType>, StateType>&& getLocalTransitions() &&;

    /**
     * The three way comparison implementation
     *
     * \param other the other instance
     *
     * \returns the ordering between this object and the @p other.
     */
    auto operator<=>(const VisiblyPushdownNPDA& other) const
    {
        return std::tie(getStates(), getCallInputAlphabet(), getReturnInputAlphabet(), getLocalInputAlphabet(), getInitialStates(), getFinalStates(), getPushdownStoreAlphabet(), getBottomOfTheStackSymbol(), callTransitions, returnTransitions, localTransitions) <=> std::tie(other.getStates(), other.getCallInputAlphabet(), other.getReturnInputAlphabet(), other.getLocalInputAlphabet(), other.getInitialStates(), other.getFinalStates(), other.getPushdownStoreAlphabet(), other.getBottomOfTheStackSymbol(), other.callTransitions, other.returnTransitions, other.localTransitions);
    }

    /**
     * The equality comparison implementation.
     *
     * \param other the other object to compare with.
     *
     * \returns true if this and other objects are equal, false othervise
     */
    bool operator==(const VisiblyPushdownNPDA& other) const
    {
        return std::tie(getStates(), getCallInputAlphabet(), getReturnInputAlphabet(), getLocalInputAlphabet(), getInitialStates(), getFinalStates(), getPushdownStoreAlphabet(), getBottomOfTheStackSymbol(), callTransitions, returnTransitions, localTransitions) == std::tie(other.getStates(), other.getCallInputAlphabet(), other.getReturnInputAlphabet(), other.getLocalInputAlphabet(), other.getInitialStates(), other.getFinalStates(), other.getPushdownStoreAlphabet(), other.getBottomOfTheStackSymbol(), other.callTransitions, other.returnTransitions, other.localTransitions);
    }

    /**
     * Print this object as raw representation to ostream.
     *
     * \param out ostream where to print
     * \param instance object to print
     *
     * \returns modified output stream
     */
    friend ext::ostream& operator<<(ext::ostream& out, const VisiblyPushdownNPDA& instance)
    {
        return out << "(VisiblyPushdownNPDA"
                   << " states = " << instance.getStates()
                   << " callAlphabet = " << instance.getCallInputAlphabet()
                   << " returnAlphabet = " << instance.getReturnInputAlphabet()
                   << " localAlphabet = " << instance.getLocalInputAlphabet()
                   << " initialStates = " << instance.getInitialStates()
                   << " finalStates = " << instance.getFinalStates()
                   << " pushdownStoreAlphabet = " << instance.getPushdownStoreAlphabet()
                   << " bottomOfTheStackSymbol = " << instance.getBottomOfTheStackSymbol()
                   << " callTransitions = " << instance.getCallTransitions()
                   << " returnTransitions = " << instance.getReturnTransitions()
                   << " localTransitions = " << instance.getLocalTransitions()
                   << ")";
    }
};

template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>::VisiblyPushdownNPDA(ext::set<StateType> states, ext::set<InputSymbolType> callAlphabet, ext::set<InputSymbolType> returnAlphabet, ext::set<InputSymbolType> localAlphabet, ext::set<PushdownStoreSymbolType> pushdownStoreAlphabet, ext::set<StateType> initialStates, PushdownStoreSymbolType bottomOfTheStackSymbol, ext::set<StateType> finalStates)
    : core::Components<VisiblyPushdownNPDA, ext::set<InputSymbolType>, module::Set, std::tuple<component::CallAlphabet, component::ReturnAlphabet, component::LocalAlphabet>, ext::set<PushdownStoreSymbolType>, module::Set, component::PushdownStoreAlphabet, PushdownStoreSymbolType, module::Value, component::BottomOfTheStackSymbol, ext::set<StateType>, module::Set, std::tuple<component::States, component::InitialStates, component::FinalStates>>(std::move(callAlphabet), std::move(returnAlphabet), std::move(localAlphabet), std::move(pushdownStoreAlphabet), std::move(bottomOfTheStackSymbol), std::move(states), std::move(initialStates), std::move(finalStates))
{
}

template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>::VisiblyPushdownNPDA(PushdownStoreSymbolType bottomOfTheStackSymbol)
    : VisiblyPushdownNPDA(ext::set<StateType>{}, ext::set<InputSymbolType>{}, ext::set<InputSymbolType>{}, ext::set<InputSymbolType>{}, ext::set<PushdownStoreSymbolType>{bottomOfTheStackSymbol}, ext::set<StateType>{}, bottomOfTheStackSymbol, ext::set<StateType>{})
{
}

template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
bool VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>::addCallTransition(StateType from, InputSymbolType input, StateType to, PushdownStoreSymbolType push)
{
    if (!getStates().count(from)) {
        throw AutomatonException("State \"" + ext::to_string(from) + "\" doesn't exist.");
    }

    if (!getCallInputAlphabet().count(input)) {
        throw AutomatonException("Input symbol \"" + ext::to_string(input) + "\" doesn't exist.");
    }

    if (!getStates().count(to)) {
        throw AutomatonException("State \"" + ext::to_string(to) + "\" doesn't exist.");
    }

    if (!getPushdownStoreAlphabet().count(push)) {
        throw AutomatonException("Pushdown store symbol \"" + ext::to_string(push) + "\" doesn't exist.");
    }

    auto upper_bound = callTransitions.upper_bound(ext::tie(from, input));
    auto lower_bound = callTransitions.lower_bound(ext::tie(from, input));

    ext::pair<StateType, PushdownStoreSymbolType> value = ext::make_pair(std::move(to), std::move(push));

    auto iter = std::lower_bound(lower_bound, upper_bound, value, [](const auto& transition, const auto& target) { return transition.second < target; });
    if (iter != upper_bound && value >= iter->second)
        return false;

    ext::pair<StateType, InputSymbolType> key(std::move(from), std::move(input));

    callTransitions.insert(iter, std::make_pair(std::move(key), std::move(value)));
    return true;
}

template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
bool VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>::addReturnTransition(StateType from, InputSymbolType input, PushdownStoreSymbolType pop, StateType to)
{
    if (!getStates().count(from)) {
        throw AutomatonException("State \"" + ext::to_string(from) + "\" doesn't exist.");
    }

    if (!getReturnInputAlphabet().count(input)) {
        throw AutomatonException("Input symbol \"" + ext::to_string(input) + "\" doesn't exist.");
    }

    if (!getStates().count(to)) {
        throw AutomatonException("State \"" + ext::to_string(to) + "\" doesn't exist.");
    }

    if (!getPushdownStoreAlphabet().count(pop)) {
        throw AutomatonException("Pushdown store symbol \"" + ext::to_string(pop) + "\" doesn't exist.");
    }

    auto upper_bound = returnTransitions.upper_bound(ext::tie(from, input, pop));
    auto lower_bound = returnTransitions.lower_bound(ext::tie(from, input, pop));

    auto iter = std::lower_bound(lower_bound, upper_bound, to, [](const auto& transition, const auto& target) { return transition.second < target; });
    if (iter != upper_bound && to >= iter->second)
        return false;

    ext::tuple<StateType, InputSymbolType, PushdownStoreSymbolType> key(std::move(from), std::move(input), std::move(pop));

    returnTransitions.insert(iter, std::make_pair(std::move(key), std::move(to)));
    return true;
}

template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
bool VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>::addLocalTransition(StateType from, InputSymbolType input, StateType to)
{
    if (!getStates().count(from)) {
        throw AutomatonException("State \"" + ext::to_string(from) + "\" doesn't exist.");
    }

    if (!getLocalInputAlphabet().count(input)) {
        throw AutomatonException("Input symbol \"" + ext::to_string(input) + "\" doesn't exist.");
    }

    if (!getStates().count(to)) {
        throw AutomatonException("State \"" + ext::to_string(to) + "\" doesn't exist.");
    }

    auto upper_bound = localTransitions.upper_bound(ext::tie(from, input));
    auto lower_bound = localTransitions.lower_bound(ext::tie(from, input));

    auto iter = std::lower_bound(lower_bound, upper_bound, to, [](const auto& transition, const auto& target) { return transition.second < target; });
    if (iter != upper_bound && to >= iter->second)
        return false;

    ext::pair<StateType, InputSymbolType> key(std::move(from), std::move(input));

    localTransitions.insert(iter, std::make_pair(std::move(key), std::move(to)));
    return true;
}

template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
bool VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>::removeCallTransition(const StateType& from, const InputSymbolType& input, const StateType& to, const PushdownStoreSymbolType& push)
{
    auto upper_bound = callTransitions.upper_bound(ext::tie(from, input));
    auto lower_bound = callTransitions.lower_bound(ext::tie(from, input));
    auto iter = std::find_if(lower_bound, upper_bound, [&](const auto& transition) { return transition.second.first == to && transition.second.second == push; });
    if (iter == upper_bound)
        return false;

    callTransitions.erase(iter);
    return true;
}

template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
bool VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>::removeReturnTransition(const StateType& from, const InputSymbolType& input, const PushdownStoreSymbolType& pop, const StateType& to)
{
    auto upper_bound = returnTransitions.upper_bound(ext::tie(from, input, pop));
    auto lower_bound = returnTransitions.lower_bound(ext::tie(from, input, pop));
    auto iter = std::find_if(lower_bound, upper_bound, [&](const auto& transition) { return transition.second == to; });
    if (iter == upper_bound)
        return false;

    returnTransitions.erase(iter);
    return true;
}

template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
bool VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>::removeLocalTransition(const StateType& from, const InputSymbolType& input, const StateType& to)
{
    auto upper_bound = localTransitions.upper_bound(ext::tie(from, input));
    auto lower_bound = localTransitions.lower_bound(ext::tie(from, input));
    auto iter = std::find_if(lower_bound, upper_bound, [&](const auto& transition) { return transition.second == to; });
    if (iter == upper_bound)
        return false;

    localTransitions.erase(iter);
    return true;
}

template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
const ext::multimap<ext::pair<StateType, InputSymbolType>, ext::pair<StateType, PushdownStoreSymbolType>>& VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>::getCallTransitions() const&
{
    return callTransitions;
}

template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
ext::multimap<ext::pair<StateType, InputSymbolType>, ext::pair<StateType, PushdownStoreSymbolType>>&& VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>::getCallTransitions() &&
{
    return std::move(callTransitions);
}

template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
const ext::multimap<ext::tuple<StateType, InputSymbolType, PushdownStoreSymbolType>, StateType>& VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>::getReturnTransitions() const&
{
    return returnTransitions;
}

template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
ext::multimap<ext::tuple<StateType, InputSymbolType, PushdownStoreSymbolType>, StateType>&& VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>::getReturnTransitions() &&
{
    return std::move(returnTransitions);
}

template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
const ext::multimap<ext::pair<StateType, InputSymbolType>, StateType>& VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>::getLocalTransitions() const&
{
    return localTransitions;
}

template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
ext::multimap<ext::pair<StateType, InputSymbolType>, StateType>&& VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>::getLocalTransitions() &&
{
    return std::move(localTransitions);
}

} /* namespace automaton */

namespace core {

/**
 * Helper class specifying constraints for the automaton's internal call input alphabet component.
 *
 * \tparam InputSymbolType used for the terminal alphabet of the automaton.
 * \tparam PushdownSymbolType used for the pushdown store alphabet of the automaton.
 * \tparam StateType used for the terminal alphabet of the automaton.
 */
template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
class SetConstraint<automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>, InputSymbolType, component::CallAlphabet> {
public:
    /**
     * Returns true if the symbol is still used in some transition of the automaton.
     *
     * \param automaton the tested automaton
     * \param symbol the tested symbol
     *
     * \returns true if the symbol is used, false othervise
     */
    static bool used(const automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>& automaton, const InputSymbolType& symbol)
    {
        for (const std::pair<const ext::pair<StateType, InputSymbolType>, ext::pair<StateType, PushdownStoreSymbolType>>& callTransition : automaton.getCallTransitions())
            if (symbol == callTransition.first.second)
                return true;

        return false;
    }

    /**
     * Returns true as all symbols are possibly available to be elements of the call input alphabet.
     *
     * \param automaton the tested automaton
     * \param symbol the tested symbol
     *
     * \returns true
     */
    static bool available(const automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>&, const InputSymbolType&)
    {
        return true;
    }

    /**
     * All symbols are valid as call input symbols.
     *
     * \param automaton the tested automaton
     * \param symbol the tested symbol
     */
    static void valid(const automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>& automaton, const InputSymbolType& symbol)
    {
        if (automaton.getLocalInputAlphabet().count(symbol))
            throw automaton::AutomatonException("Input symbol " + ext::to_string(symbol) + " already in local alphabet");
        if (automaton.getReturnInputAlphabet().count(symbol))
            throw automaton::AutomatonException("Input symbol " + ext::to_string(symbol) + " already in return alphabet");
    }
};

/**
 * Helper class specifying constraints for the automaton's internal return input alphabet component.
 *
 * \tparam InputSymbolType used for the terminal alphabet of the automaton.
 * \tparam PushdownSymbolType used for the pushdown store alphabet of the automaton.
 * \tparam StateType used for the terminal alphabet of the automaton.
 */
template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
class SetConstraint<automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>, InputSymbolType, component::ReturnAlphabet> {
public:
    /**
     * Returns true if the symbol is still used in some transition of the automaton.
     *
     * \param automaton the tested automaton
     * \param symbol the tested symbol
     *
     * \returns true if the symbol is used, false othervise
     */
    static bool used(const automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>& automaton, const InputSymbolType& symbol)
    {
        for (const std::pair<const ext::tuple<StateType, InputSymbolType, PushdownStoreSymbolType>, StateType>& returnTransition : automaton.getReturnTransitions())
            if (symbol == std::get<1>(returnTransition.first))
                return true;

        return false;
    }

    /**
     * Returns true as all symbols are possibly available to be elements of the return input alphabet.
     *
     * \param automaton the tested automaton
     * \param symbol the tested symbol
     *
     * \returns true
     */
    static bool available(const automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>&, const InputSymbolType&)
    {
        return true;
    }

    /**
     * All symbols are valid as return input symbols.
     *
     * \param automaton the tested automaton
     * \param symbol the tested symbol
     */
    static void valid(const automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>& automaton, const InputSymbolType& symbol)
    {
        if (automaton.getLocalInputAlphabet().count(symbol))
            throw automaton::AutomatonException("Input symbol " + ext::to_string(symbol) + " already in local alphabet");
        if (automaton.getCallInputAlphabet().count(symbol))
            throw automaton::AutomatonException("Input symbol " + ext::to_string(symbol) + " already in call alphabet");
    }
};

/**
 * Helper class specifying constraints for the automaton's internal local input alphabet component.
 *
 * \tparam InputSymbolType used for the terminal alphabet of the automaton.
 * \tparam PushdownSymbolType used for the pushdown store alphabet of the automaton.
 * \tparam StateType used for the terminal alphabet of the automaton.
 */
template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
class SetConstraint<automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>, InputSymbolType, component::LocalAlphabet> {
public:
    /**
     * Returns true if the symbol is still used in some transition of the automaton.
     *
     * \param automaton the tested automaton
     * \param symbol the tested symbol
     *
     * \returns true if the symbol is used, false othervise
     */
    static bool used(const automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>& automaton, const InputSymbolType& symbol)
    {
        for (const std::pair<const ext::pair<StateType, InputSymbolType>, StateType>& localTransition : automaton.getLocalTransitions())
            if (symbol == localTransition.first.second)
                return true;

        return false;
    }

    /**
     * Returns true as all symbols are possibly available to be elements of the local input alphabet.
     *
     * \param automaton the tested automaton
     * \param symbol the tested symbol
     *
     * \returns true
     */
    static bool available(const automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>&, const InputSymbolType&)
    {
        return true;
    }

    /**
     * All symbols are valid as local input symbols.
     *
     * \param automaton the tested automaton
     * \param symbol the tested symbol
     */
    static void valid(const automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>& automaton, const InputSymbolType& symbol)
    {
        if (automaton.getReturnInputAlphabet().count(symbol))
            throw automaton::AutomatonException("Input symbol " + ext::to_string(symbol) + " already in return alphabet");
        if (automaton.getCallInputAlphabet().count(symbol))
            throw automaton::AutomatonException("Input symbol " + ext::to_string(symbol) + " already in call alphabet");
    }
};

/**
 * Helper class specifying constraints for the automaton's internal pushdown store alphabet component.
 *
 * \tparam InputSymbolType used for the terminal alphabet of the automaton.
 * \tparam PushdownSymbolType used for the pushdown store alphabet of the automaton.
 * \tparam StateType used for the terminal alphabet of the automaton.
 */
template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
class SetConstraint<automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>, PushdownStoreSymbolType, component::PushdownStoreAlphabet> {
public:
    /**
     * Returns true if the symbol is still used in some transition of the automaton.
     *
     * \param automaton the tested automaton
     * \param symbol the tested symbol
     *
     * \returns true if the symbol is used, false othervise
     */
    static bool used(const automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>& automaton, const PushdownStoreSymbolType& symbol)
    {
        for (const std::pair<const ext::pair<StateType, InputSymbolType>, ext::pair<StateType, PushdownStoreSymbolType>>& transition : automaton.getCallTransitions())
            if (symbol == transition.second.second)
                return true;

        for (const std::pair<const ext::tuple<StateType, InputSymbolType, PushdownStoreSymbolType>, StateType>& returnTransition : automaton.getReturnTransitions())
            if (symbol == std::get<2>(returnTransition.first))
                return true;

        if (automaton.getBottomOfTheStackSymbol() == symbol)
            return true;

        return false;
    }

    /**
     * Returns true as all symbols are possibly available to be elements of the pushdown store alphabet.
     *
     * \param automaton the tested automaton
     * \param symbol the tested symbol
     *
     * \returns true
     */
    static bool available(const automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>&, const PushdownStoreSymbolType&)
    {
        return true;
    }

    /**
     * All symbols are valid as pushdown store symbols.
     *
     * \param automaton the tested automaton
     * \param symbol the tested symbol
     */
    static void valid(const automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>&, const PushdownStoreSymbolType&)
    {
    }
};

/**
 * Helper class specifying constraints for the automaton's internal bottom of the stack symbol element.
 *
 * \tparam InputSymbolType used for the terminal alphabet of the automaton.
 * \tparam PushdownSymbolType used for the pushdown store alphabet of the automaton.
 * \tparam StateType used for the terminal alphabet of the automaton.
 */
template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
class ElementConstraint<automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>, PushdownStoreSymbolType, component::BottomOfTheStackSymbol> {
public:
    /**
     * Determines whether the bottom of the stack symbol is available in the automaton's pushdown store alphabet.
     *
     * \param automaton the tested automaton
     * \param symbol the tested symbol
     *
     * \returns true if the pushdown store symbol is already in the pushdown store alphabet of the automaton
     */
    static bool available(const automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>& automaton, const PushdownStoreSymbolType& symbol)
    {
        return automaton.template accessComponent<component::PushdownStoreAlphabet>().get().count(symbol);
    }

    /**
     * All pushdown store symbols are valid as a bottom of the stack symbol symbol of the automaton.
     *
     * \param automaton the tested automaton
     * \param symbol the tested symbol
     */
    static void valid(const automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>&, const PushdownStoreSymbolType&)
    {
    }
};

/**
 * Helper class specifying constraints for the automaton's internal states component.
 *
 * \tparam InputSymbolType used for the terminal alphabet of the automaton.
 * \tparam PushdownSymbolType used for the pushdown store alphabet of the automaton.
 * \tparam StateType used for the terminal alphabet of the automaton.
 */
template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
class SetConstraint<automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>, StateType, component::States> {
public:
    /**
     * Returns true if the state is still used in some transition of the automaton.
     *
     * \param automaton the tested automaton
     * \param state the tested state
     *
     * \returns true if the state is used, false othervise
     */
    static bool used(const automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>& automaton, const StateType& state)
    {
        if (automaton.getInitialStates().count(state))
            return true;

        if (automaton.getFinalStates().count(state))
            return true;

        for (const std::pair<const ext::pair<StateType, InputSymbolType>, ext::pair<StateType, PushdownStoreSymbolType>>& transition : automaton.getCallTransitions()) {
            if (state == transition.first.first)
                return true;
            if (transition.second.first == state)
                return true;
        }

        for (const std::pair<const ext::tuple<StateType, InputSymbolType, PushdownStoreSymbolType>, StateType>& transition : automaton.getReturnTransitions()) {
            if (state == std::get<0>(transition.first))
                return true;
            if (transition.second == state)
                return true;
        }

        for (const std::pair<const ext::pair<StateType, InputSymbolType>, StateType>& transition : automaton.getLocalTransitions()) {
            if (state == transition.first.first)
                return true;
            if (transition.second == state)
                return true;
        }

        return false;
    }

    /**
     * Returns true as all states are possibly available to be elements of the states.
     *
     * \param automaton the tested automaton
     * \param state the tested state
     *
     * \returns true
     */
    static bool available(const automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>&, const StateType&)
    {
        return true;
    }

    /**
     * All states are valid as a state of the automaton.
     *
     * \param automaton the tested automaton
     * \param state the tested state
     */
    static void valid(const automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>&, const StateType&)
    {
    }
};

/**
 * Helper class specifying constraints for the automaton's internal final states component.
 *
 * \tparam InputSymbolType used for the terminal alphabet of the automaton.
 * \tparam PushdownSymbolType used for the pushdown store alphabet of the automaton.
 * \tparam StateType used for the terminal alphabet of the automaton.
 */
template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
class SetConstraint<automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>, StateType, component::FinalStates> {
public:
    /**
     * Returns false. Final state is only a mark that the automaton itself does require further.
     *
     * \param automaton the tested automaton
     * \param state the tested state
     *
     * \returns false
     */
    static bool used(const automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>&, const StateType&)
    {
        return false;
    }

    /**
     * Determines whether the state is available in the automaton's states set.
     *
     * \param automaton the tested automaton
     * \param state the tested state
     *
     * \returns true if the state is already in the set of states of the automaton
     */
    static bool available(const automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>& automaton, const StateType& state)
    {
        return automaton.template accessComponent<component::States>().get().count(state);
    }

    /**
     * All states are valid as a final state of the automaton.
     *
     * \param automaton the tested automaton
     * \param state the tested state
     */
    static void valid(const automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>&, const StateType&)
    {
    }
};

/**
 * Helper class specifying constraints for the automaton's internal initial state component.
 *
 * \tparam InputSymbolType used for the terminal alphabet of the automaton.
 * \tparam PushdownSymbolType used for the pushdown store alphabet of the automaton.
 * \tparam StateType used for the terminal alphabet of the automaton.
 */
template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
class SetConstraint<automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>, StateType, component::InitialStates> {
public:
    /**
     * Returns false. Initial state is only a mark that the automaton itself does require further.
     *
     * \param automaton the tested automaton
     * \param state the tested state
     *
     * \returns false
     */
    static bool used(const automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>&, const StateType&)
    {
        return false;
    }

    /**
     * Determines whether the state is available in the automaton's states set.
     *
     * \param automaton the tested automaton
     * \param state the tested state
     *
     * \returns true if the state is already in the set of states of the automaton
     */
    static bool available(const automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>& automaton, const StateType& state)
    {
        return automaton.template accessComponent<component::States>().get().count(state);
    }

    /**
     * All states are valid as a initial state of the automaton.
     *
     * \param automaton the tested automaton
     * \param state the tested state
     */
    static void valid(const automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>&, const StateType&)
    {
    }
};

template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
struct type_util<automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>> {
    static automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType> denormalize(automaton::VisiblyPushdownNPDA<>&& value)
    {
        ext::set<InputSymbolType> call_alphabet = alphabet::SymbolDenormalize::denormalizeAlphabet<InputSymbolType>(std::move(value).getCallInputAlphabet());
        ext::set<InputSymbolType> return_alphabet = alphabet::SymbolDenormalize::denormalizeAlphabet<InputSymbolType>(std::move(value).getReturnInputAlphabet());
        ext::set<InputSymbolType> local_alphabet = alphabet::SymbolDenormalize::denormalizeAlphabet<InputSymbolType>(std::move(value).getLocalInputAlphabet());
        ext::set<PushdownStoreSymbolType> pushdownAlphabet = alphabet::SymbolDenormalize::denormalizeAlphabet<PushdownStoreSymbolType>(std::move(value).getPushdownStoreAlphabet());
        PushdownStoreSymbolType initialSymbol = alphabet::SymbolDenormalize::denormalizeSymbol<PushdownStoreSymbolType>(std::move(value).getBottomOfTheStackSymbol());
        ext::set<StateType> states = automaton::AutomatonDenormalize::denormalizeStates<StateType>(std::move(value).getStates());
        ext::set<StateType> initialStates = automaton::AutomatonDenormalize::denormalizeStates<StateType>(std::move(value).getInitialStates());
        ext::set<StateType> finalStates = automaton::AutomatonDenormalize::denormalizeStates<StateType>(std::move(value).getFinalStates());

        automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType> res(std::move(states), std::move(call_alphabet), std::move(return_alphabet), std::move(local_alphabet), std::move(pushdownAlphabet), std::move(initialStates), std::move(initialSymbol), std::move(finalStates));

        for (std::pair<ext::pair<DefaultStateType, DefaultSymbolType>, ext::pair<DefaultStateType, DefaultSymbolType>>&& transition : ext::make_mover(std::move(value).getCallTransitions())) {
            StateType to = automaton::AutomatonDenormalize::denormalizeState<StateType>(std::move(transition.second.first));
            PushdownStoreSymbolType push = alphabet::SymbolDenormalize::denormalizeSymbol<PushdownStoreSymbolType>(std::move(transition.second.second));

            StateType from = automaton::AutomatonDenormalize::denormalizeState<StateType>(std::move(transition.first.first));
            InputSymbolType input = alphabet::SymbolDenormalize::denormalizeSymbol<InputSymbolType>(std::move(transition.first.second));

            res.addCallTransition(std::move(from), std::move(input), std::move(to), std::move(push));
        }

        for (std::pair<ext::tuple<DefaultStateType, DefaultSymbolType, DefaultSymbolType>, DefaultStateType>&& transition : ext::make_mover(std::move(value).getReturnTransitions())) {
            StateType to = automaton::AutomatonDenormalize::denormalizeState<StateType>(std::move(transition.second));

            StateType from = automaton::AutomatonDenormalize::denormalizeState<StateType>(std::move(std::get<0>(transition.first)));
            InputSymbolType input = alphabet::SymbolDenormalize::denormalizeSymbol<InputSymbolType>(std::move(std::get<1>(transition.first)));
            PushdownStoreSymbolType pop = alphabet::SymbolDenormalize::denormalizeSymbol<PushdownStoreSymbolType>(std::move(std::get<2>(transition.first)));

            res.addReturnTransition(std::move(from), std::move(input), std::move(pop), std::move(to));
        }

        for (std::pair<ext::pair<DefaultStateType, DefaultSymbolType>, DefaultStateType>&& transition : ext::make_mover(std::move(value).getLocalTransitions())) {
            StateType to = automaton::AutomatonDenormalize::denormalizeState<StateType>(std::move(transition.second));

            StateType from = automaton::AutomatonDenormalize::denormalizeState<StateType>(std::move(transition.first.first));
            InputSymbolType input = alphabet::SymbolDenormalize::denormalizeSymbol<InputSymbolType>(std::move(transition.first.second));

            res.addLocalTransition(std::move(from), std::move(input), std::move(to));
        }

        return res;
    }

    static automaton::VisiblyPushdownNPDA<> normalize(automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>&& value)
    {
        ext::set<DefaultSymbolType> call_alphabet = alphabet::SymbolNormalize::normalizeAlphabet(std::move(value).getCallInputAlphabet());
        ext::set<DefaultSymbolType> return_alphabet = alphabet::SymbolNormalize::normalizeAlphabet(std::move(value).getReturnInputAlphabet());
        ext::set<DefaultSymbolType> local_alphabet = alphabet::SymbolNormalize::normalizeAlphabet(std::move(value).getLocalInputAlphabet());
        ext::set<DefaultSymbolType> pushdownAlphabet = alphabet::SymbolNormalize::normalizeAlphabet(std::move(value).getPushdownStoreAlphabet());
        DefaultSymbolType initialSymbol = alphabet::SymbolNormalize::normalizeSymbol(std::move(value).getBottomOfTheStackSymbol());
        ext::set<DefaultStateType> states = automaton::AutomatonNormalize::normalizeStates(std::move(value).getStates());
        ext::set<DefaultStateType> initialStates = automaton::AutomatonNormalize::normalizeStates(std::move(value).getInitialStates());
        ext::set<DefaultStateType> finalStates = automaton::AutomatonNormalize::normalizeStates(std::move(value).getFinalStates());

        automaton::VisiblyPushdownNPDA<> res(std::move(states), std::move(call_alphabet), std::move(return_alphabet), std::move(local_alphabet), std::move(pushdownAlphabet), std::move(initialStates), std::move(initialSymbol), std::move(finalStates));

        for (std::pair<ext::pair<StateType, InputSymbolType>, ext::pair<StateType, PushdownStoreSymbolType>>&& transition : ext::make_mover(std::move(value).getCallTransitions())) {
            DefaultStateType to = automaton::AutomatonNormalize::normalizeState(std::move(transition.second.first));
            DefaultSymbolType push = alphabet::SymbolNormalize::normalizeSymbol(std::move(transition.second.second));

            DefaultStateType from = automaton::AutomatonNormalize::normalizeState(std::move(transition.first.first));
            DefaultSymbolType input = alphabet::SymbolNormalize::normalizeSymbol(std::move(transition.first.second));

            res.addCallTransition(std::move(from), std::move(input), std::move(to), std::move(push));
        }

        for (std::pair<ext::tuple<StateType, InputSymbolType, PushdownStoreSymbolType>, StateType>&& transition : ext::make_mover(std::move(value).getReturnTransitions())) {
            DefaultStateType to = automaton::AutomatonNormalize::normalizeState(std::move(transition.second));

            DefaultStateType from = automaton::AutomatonNormalize::normalizeState(std::move(std::get<0>(transition.first)));
            DefaultSymbolType input = alphabet::SymbolNormalize::normalizeSymbol(std::move(std::get<1>(transition.first)));
            DefaultSymbolType pop = alphabet::SymbolNormalize::normalizeSymbol(std::move(std::get<2>(transition.first)));

            res.addReturnTransition(std::move(from), std::move(input), std::move(pop), std::move(to));
        }

        for (std::pair<ext::pair<StateType, InputSymbolType>, StateType>&& transition : ext::make_mover(std::move(value).getLocalTransitions())) {
            DefaultStateType to = automaton::AutomatonNormalize::normalizeState(std::move(transition.second));

            DefaultStateType from = automaton::AutomatonNormalize::normalizeState(std::move(transition.first.first));
            DefaultSymbolType input = alphabet::SymbolNormalize::normalizeSymbol(std::move(transition.first.second));

            res.addLocalTransition(std::move(from), std::move(input), std::move(to));
        }

        return res;
    }

    static std::unique_ptr<type_details_base> type(const automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>& arg)
    {
        core::unique_ptr_set<type_details_base> subTypesInputSymbol;
        for (const InputSymbolType& item : arg.getCallInputAlphabet())
            subTypesInputSymbol.insert(type_util<InputSymbolType>::type(item));
        for (const InputSymbolType& item : arg.getReturnInputAlphabet())
            subTypesInputSymbol.insert(type_util<InputSymbolType>::type(item));
        for (const InputSymbolType& item : arg.getLocalInputAlphabet())
            subTypesInputSymbol.insert(type_util<InputSymbolType>::type(item));

        core::unique_ptr_set<type_details_base> subTypesPushdownStoreSymbol;
        for (const PushdownStoreSymbolType& item : arg.getPushdownStoreAlphabet())
            subTypesPushdownStoreSymbol.insert(type_util<PushdownStoreSymbolType>::type(item));

        core::unique_ptr_set<type_details_base> subTypesState;
        for (const StateType& item : arg.getStates())
            subTypesState.insert(type_util<StateType>::type(item));

        std::vector<std::unique_ptr<type_details_base>> sub_types_vec;
        sub_types_vec.push_back(type_details_variant_type::make_variant(std::move(subTypesInputSymbol)));
        sub_types_vec.push_back(type_details_variant_type::make_variant(std::move(subTypesPushdownStoreSymbol)));
        sub_types_vec.push_back(type_details_variant_type::make_variant(std::move(subTypesState)));
        return std::make_unique<type_details_template>("automaton::VisiblyPushdownNPDA", std::move(sub_types_vec));
    }
};

template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
struct type_details_retriever<automaton::VisiblyPushdownNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>> {
    static std::unique_ptr<type_details_base> get()
    {
        std::vector<std::unique_ptr<type_details_base>> sub_types_vec;
        sub_types_vec.push_back(type_details_retriever<InputSymbolType>::get());
        sub_types_vec.push_back(type_details_retriever<PushdownStoreSymbolType>::get());
        sub_types_vec.push_back(type_details_retriever<StateType>::get());
        return std::make_unique<type_details_template>("automaton::VisiblyPushdownNPDA", std::move(sub_types_vec));
    }
};

} /* namespace core */

extern template class automaton::VisiblyPushdownNPDA<>;
extern template class abstraction::ValueHolder<automaton::VisiblyPushdownNPDA<>>;
extern template const automaton::VisiblyPushdownNPDA<>& abstraction::retrieveValue<const automaton::VisiblyPushdownNPDA<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
extern template class registration::DenormalizationRegisterImpl<const automaton::VisiblyPushdownNPDA<>&>;
extern template class registration::NormalizationRegisterImpl<automaton::VisiblyPushdownNPDA<>>;
