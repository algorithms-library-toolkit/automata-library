#include "VisiblyPushdownDPDA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::VisiblyPushdownDPDA<>;
template class abstraction::ValueHolder<automaton::VisiblyPushdownDPDA<>>;
template const automaton::VisiblyPushdownDPDA<>& abstraction::retrieveValue<const automaton::VisiblyPushdownDPDA<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const automaton::VisiblyPushdownDPDA<>&>;
template class registration::NormalizationRegisterImpl<automaton::VisiblyPushdownDPDA<>>;

namespace {

auto valuePrinter = registration::ValuePrinterRegister<automaton::VisiblyPushdownDPDA<>>();

} /* namespace */
