#include "DPDA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::DPDA<>;
template class abstraction::ValueHolder<automaton::DPDA<>>;
template const automaton::DPDA<>& abstraction::retrieveValue<const automaton::DPDA<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const automaton::DPDA<>&>;
template class registration::NormalizationRegisterImpl<automaton::DPDA<>>;

namespace {

auto valuePrinter = registration::ValuePrinterRegister<automaton::DPDA<>>();

} /* namespace */
