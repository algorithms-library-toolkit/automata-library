/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <ext/algorithm>

#include <alib/multimap>
#include <alib/set>
#include <alib/vector>

#include <core/modules.hpp>

#include <common/DefaultStateType.h>
#include <common/DefaultSymbolType.h>

#include <automaton/AutomatonException.h>

#include <alphabet/common/SymbolDenormalize.h>
#include <alphabet/common/SymbolNormalize.h>
#include <automaton/common/AutomatonDenormalize.h>
#include <automaton/common/AutomatonNormalize.h>
#include <core/type_details_base.hpp>
#include <core/type_util.hpp>

#include <registration/DenormalizationRegistration.hpp>
#include <registration/NormalizationRegistration.hpp>

namespace automaton {

/**
 * Nondeterministic pushdown automaton requiring a symbol pop from pushdown store on each transition use. Accepts subset of context free languages.
 *
 * \details
 * Definition
 * A = (Q, T, G, I, Z, \delta, F),
 * Q (States) = nonempty finite set of states,
 * T (TerminalAlphabet) = finite set of terminal symbols - having this empty won't let automaton do much though,
 * G (PushdownStoreAlphabet) = finite set of pushdown store symbol
 * I (InitialState) = initial state,
 * Z (InitialPushdownStoreSymbol) = initial pushdown store symbol
 * \delta = transition function of the form A \times a \times g -> B \times \beta, where A, B \in Q, a \in T \cup { \eps }, g \in G, and \beta \in G*,
 * F (FinalStates) = set of final states
 *
 * \tparam InputSymbolTypeT used for the terminal alphabet
 * \tparam PushdownSymbolTypeT used for the pushdown store alphabet
 * \tparam StateTypeT used to the states, and the initial state of the automaton.
 */
template <class InputSymbolTypeT = DefaultSymbolType, class PushdownStoreSymbolTypeT = DefaultSymbolType, class StateTypeT = DefaultStateType>
class SinglePopNPDA final : public core::Components<SinglePopNPDA<InputSymbolTypeT, PushdownStoreSymbolTypeT, StateTypeT>, ext::set<InputSymbolTypeT>, module::Set, component::InputAlphabet, ext::set<PushdownStoreSymbolTypeT>, module::Set, component::PushdownStoreAlphabet, PushdownStoreSymbolTypeT, module::Value, component::InitialSymbol, ext::set<StateTypeT>, module::Set, std::tuple<component::States, component::FinalStates>, StateTypeT, module::Value, component::InitialState> {
public:
    typedef InputSymbolTypeT InputSymbolType;
    typedef PushdownStoreSymbolTypeT PushdownStoreSymbolType;
    typedef StateTypeT StateType;

private:
    /**
     * Transition function as mapping from a state times an input symbol or epsilon times pushdown store symbol on the left hand side to a state times string of pushdown store symbols.
     */
    ext::multimap<ext::tuple<StateType, common::symbol_or_epsilon<InputSymbolType>, PushdownStoreSymbolType>, ext::pair<StateType, ext::vector<PushdownStoreSymbolType>>> transitions;

public:
    /**
     * \brief Creates a new instance of the automaton with a concrete set of states, input alphabet, pushdown store alphabet, initial state, initial pushdown symbol and a set of final states.
     *
     * \param states the initial set of states of the automaton
     * \param inputAlphabet the initial input alphabet
     * \param pushdownStoreAlphabet the initial set of symbols used in the pushdown store by the automaton
     * \param initialState the initial state of the automaton
     * \param initialPushdownSymbol the initial pushdown symbol of the automaton
     * \param finalStates the initial set of final states of the automaton
     */
    explicit SinglePopNPDA(ext::set<StateType> states, ext::set<InputSymbolType> inputAlphabet, ext::set<PushdownStoreSymbolType> pushdownStoreAlphabet, StateType initialState, PushdownStoreSymbolType initialSymbol, ext::set<StateType> finalStates);

    /**
     * \brief Creates a new instance of the automaton with a concrete initial state and initial pushdown store symbol.
     *
     * \param initialState the initial state of the automaton
     * \param initialPushdownSymbol the initial pushdown symbol of the automaton
     */
    explicit SinglePopNPDA(StateType initialState, PushdownStoreSymbolType initialPushdownSymbol);

    /**
     * Getter of the initial state.
     *
     * \returns the initial state of the automaton
     */
    const StateType& getInitialState() const&
    {
        return this->template accessComponent<component::InitialState>().get();
    }

    /**
     * Getter of the initial state.
     *
     * \returns the initial state of the automaton
     */
    StateType&& getInitialState() &&
    {
        return std::move(this->template accessComponent<component::InitialState>().get());
    }

    /**
     * Setter of the initial state.
     *
     * \param state new initial state of the automaton
     *
     * \returns true if the initial state was indeed changed
     */
    bool setInitialState(StateType state)
    {
        return this->template accessComponent<component::InitialState>().set(std::move(state));
    }

    /**
     * Getter of states.
     *
     * \returns the states of the automaton
     */
    const ext::set<StateType>& getStates() const&
    {
        return this->template accessComponent<component::States>().get();
    }

    /**
     * Getter of states.
     *
     * \returns the states of the automaton
     */
    ext::set<StateType>&& getStates() &&
    {
        return std::move(this->template accessComponent<component::States>().get());
    }

    /**
     * Adder of a state.
     *
     * \param state the new state to be added to a set of states
     *
     * \returns true if the state was indeed added
     */
    bool addState(StateType state)
    {
        return this->template accessComponent<component::States>().add(std::move(state));
    }

    /**
     * Setter of states.
     *
     * \param states completely new set of states
     */
    void setStates(ext::set<StateType> states)
    {
        this->template accessComponent<component::States>().set(std::move(states));
    }

    /**
     * Remover of a state.
     *
     * \param state a state to be removed from a set of states
     *
     * \returns true if the state was indeed removed
     */
    void removeState(const StateType& state)
    {
        this->template accessComponent<component::States>().remove(state);
    }

    /**
     * Getter of final states.
     *
     * \returns the final states of the automaton
     */
    const ext::set<StateType>& getFinalStates() const&
    {
        return this->template accessComponent<component::FinalStates>().get();
    }

    /**
     * Getter of final states.
     *
     * \returns the final states of the automaton
     */
    ext::set<StateType>&& getFinalStates() &&
    {
        return std::move(this->template accessComponent<component::FinalStates>().get());
    }

    /**
     * Adder of a final state.
     *
     * \param state the new state to be added to a set of final states
     *
     * \returns true if the state was indeed added
     */
    bool addFinalState(StateType state)
    {
        return this->template accessComponent<component::FinalStates>().add(std::move(state));
    }

    /**
     * Setter of final states.
     *
     * \param states completely new set of final states
     */
    void setFinalStates(ext::set<StateType> states)
    {
        this->template accessComponent<component::FinalStates>().set(std::move(states));
    }

    /**
     * Remover of a final state.
     *
     * \param state a state to be removed from a set of final states
     *
     * \returns true if the state was indeed removed
     */
    void removeFinalState(const StateType& state)
    {
        this->template accessComponent<component::FinalStates>().remove(state);
    }

    /**
     * Getter of the pushdown store alphabet.
     *
     * \returns the pushdown store alphabet of the automaton
     */
    const ext::set<PushdownStoreSymbolType>& getPushdownStoreAlphabet() const&
    {
        return this->template accessComponent<component::PushdownStoreAlphabet>().get();
    }

    /**
     * Getter of the pushdown store alphabet.
     *
     * \returns the pushdown store alphabet of the automaton
     */
    ext::set<PushdownStoreSymbolType>&& getPushdownStoreAlphabet() &&
    {
        return std::move(this->template accessComponent<component::PushdownStoreAlphabet>().get());
    }

    /**
     * Adder of a pushdown store symbol.
     *
     * \param symbol the new symbol to be added to a pushdown store alphabet
     *
     * \returns true if the symbol was indeed added
     */
    bool addPushdownStoreSymbol(PushdownStoreSymbolType symbol)
    {
        return this->template accessComponent<component::PushdownStoreAlphabet>().add(std::move(symbol));
    }

    /**
     * Adder of pushdown store symbols.
     *
     * \param symbols new symbols to be added to a pushdown store alphabet
     */
    void addPushdownStoreSymbols(ext::set<PushdownStoreSymbolType> symbols)
    {
        this->template accessComponent<component::PushdownStoreAlphabet>().add(std::move(symbols));
    }

    /**
     * Setter of a pushdown store alphabet.
     *
     * \param symbols completely new pushdown store alphabet
     */
    void setPushdownStoreAlphabet(ext::set<PushdownStoreSymbolType> symbols)
    {
        this->template accessComponent<component::PushdownStoreAlphabet>().set(std::move(symbols));
    }

    /**
     * Remover of an pushdown store symbol.
     *
     * \param symbol a symbol to be removed from a pushdown store alphabet
     *
     * \returns true if the symbol was indeed removed
     */
    void removePushdownStoreSymbol(const PushdownStoreSymbolType& symbol)
    {
        this->template accessComponent<component::PushdownStoreAlphabet>().remove(symbol);
    }

    /**
     * Getter of the initial pushdown store symbol.
     *
     * \returns the initial pushdown store symbol of the automaton
     */
    const PushdownStoreSymbolType& getInitialSymbol() const&
    {
        return this->template accessComponent<component::InitialSymbol>().get();
    }

    /**
     * Getter of the initial pushdown store symbol.
     *
     * \returns the initial pushdown store symbol of the automaton
     */
    PushdownStoreSymbolType&& getInitialSymbol() &&
    {
        return std::move(this->template accessComponent<component::InitialSymbol>().get());
    }

    /**
     * Setter of the initial pushdown store symbol.
     *
     * \param symbol new initial pushdown store symbol of the automaton
     *
     * \returns true if the initial pushdown store symbol was indeed changed
     */
    bool setInitialSymbol(PushdownStoreSymbolType symbol)
    {
        return this->template accessComponent<component::InitialSymbol>().set(std::move(symbol));
    }

    /**
     * Getter of the input alphabet.
     *
     * \returns the input alphabet of the automaton
     */
    const ext::set<InputSymbolType>& getInputAlphabet() const&
    {
        return this->template accessComponent<component::InputAlphabet>().get();
    }

    /**
     * Getter of the input alphabet.
     *
     * \returns the input alphabet of the automaton
     */
    ext::set<InputSymbolType>&& getInputAlphabet() &&
    {
        return std::move(this->template accessComponent<component::InputAlphabet>().get());
    }

    /**
     * Adder of a input symbol.
     *
     * \param symbol the new symbol to be added to an input alphabet
     *
     * \returns true if the symbol was indeed added
     */
    bool addInputSymbol(InputSymbolType symbol)
    {
        return this->template accessComponent<component::InputAlphabet>().add(std::move(symbol));
    }

    /**
     * Adder of input symbols.
     *
     * \param symbols new symbols to be added to an input alphabet
     */
    void addInputSymbols(ext::set<InputSymbolType> symbols)
    {
        this->template accessComponent<component::InputAlphabet>().add(std::move(symbols));
    }

    /**
     * Setter of input alphabet.
     *
     * \param symbols completely new input alphabet
     */
    void setInputAlphabet(ext::set<InputSymbolType> symbols)
    {
        this->template accessComponent<component::InputAlphabet>().set(std::move(symbols));
    }

    /**
     * Remover of an input symbol.
     *
     * \param symbol a symbol to be removed from an input alphabet
     *
     * \returns true if the symbol was indeed removed
     */
    void removeInputSymbol(const InputSymbolType& symbol)
    {
        this->template accessComponent<component::InputAlphabet>().remove(symbol);
    }

    /**
     * \brief Adds a transition to the automaton.
     *
     * \details The transition is in a form A \times a \times g -> B \times \beta, where A, B \in Q, a \in T \cup { \eps }, g \in G, and \beta \in G*
     *
     * \param from the source state (A)
     * \param input the input symbol or epsilon (a)
     * \param pop symbols to be poped from pushdown store on the transition use (g)
     * \param to the target state (B)
     * \param push symbols to be pushed to the pushdown store on the transition use (\beta)
     *
     * \throws AutomatonException when transition contains state or symbol not present in the automaton components
     *
     * \returns true if the transition was indeed added
     */
    bool addTransition(StateType from, common::symbol_or_epsilon<InputSymbolType> input, PushdownStoreSymbolType pop, StateType to, ext::vector<PushdownStoreSymbolType> push);

    /**
     * \brief Adds a transition to the automaton.
     *
     * \details The transition is in a form A \times a \times g -> B \times \beta, where A, B \in Q, a \in T, g \in G, and \beta \in G*
     *
     * \param from the source state (A)
     * \param input the input symbol (a)
     * \param pop symbols to be poped from pushdown store on the transition use (g)
     * \param to the target state (B)
     * \param push symbols to be pushed to the pushdown store on the transition use (\beta)
     *
     * \throws AutomatonException when transition contains state or symbol not present in the automaton components
     *
     * \returns true if the transition was indeed added
     */
    bool addTransition(StateType from, InputSymbolType input, PushdownStoreSymbolType pop, StateType to, ext::vector<PushdownStoreSymbolType> push);

    /**
     * \brief Adds a transition to the automaton.
     *
     * \details The transition is in a form A \times \eps \times g -> B \times \beta, where A, B \in Q, g \in G, and \beta \in G*
     *
     * \param from the source state (A)
     * \param pop symbols to be poped from pushdown store on the transition use (g)
     * \param to the target state (B)
     * \param push symbols to be pushed to the pushdown store on the transition use (\beta)
     *
     * \throws AutomatonException when transition contains state or symbol not present in the automaton components
     *
     * \returns true if the transition was indeed added
     */
    bool addTransition(StateType from, PushdownStoreSymbolType pop, StateType to, ext::vector<PushdownStoreSymbolType> push);

    /**
     * \brief Removes a transition from the automaton.
     *
     * \details The transition is in a form A \times a \times g -> B \times \beta, where A, B \in Q, a \in T \cup { \eps }, g \in G, and \beta \in G*
     *
     * \param from the source state (A)
     * \param input the input symbol or epsilon (a)
     * \param pop symbols poped from pushdown store on the transition use (\alpha)
     * \param to the target state (B)
     * \param push symbols pushed to the pushdown store on the transition use (\beta)
     *
     * \throws AutomatonException when removed transition left hand side was found but the right hand side did not match.
     *
     * \returns true if the transition was indeed removed
     */
    bool removeTransition(const StateType& from, const common::symbol_or_epsilon<InputSymbolType>& input, const PushdownStoreSymbolType& pop, const StateType& to, const ext::vector<PushdownStoreSymbolType>& push);

    /**
     * \brief Removes a transition from the automaton.
     *
     * \details The transition is in a form A \times a \times g -> B \times \beta, where A, B \in Q, a \in T, g \in G, and \beta \in G*
     *
     * \param from the source state (A)
     * \param input the input symbol (a)
     * \param pop symbols poped from pushdown store on the transition use (g)
     * \param to the target state (B)
     * \param push symbols pushed to the pushdown store on the transition use (\beta)
     *
     * \throws AutomatonException when removed transition left hand side was found but the right hand side did not match.
     *
     * \returns true if the transition was indeed removed
     */
    bool removeTransition(const StateType& from, const InputSymbolType& input, const PushdownStoreSymbolType& pop, const StateType& to, const ext::vector<PushdownStoreSymbolType>& push);

    /**
     * \brief Removes a transition from the automaton.
     *
     * \details The transition is in a form A \times \eps \times g -> B \times \beta, where A, B \in Q, g \in G, and \beta \in G*
     *
     * \param from the source state (A)
     * \param pop symbols poped from pushdown store on the transition use (g)
     * \param to the target state (B)
     * \param push symbols pushed to the pushdown store on the transition use (\beta)
     *
     * \throws AutomatonException when removed transition left hand side was found but the right hand side did not match.
     *
     * \returns true if the transition was indeed removed
     */
    bool removeTransition(const StateType& from, const PushdownStoreSymbolType& pop, const StateType& to, const ext::vector<PushdownStoreSymbolType>& push);

    /**
     * Get the transition function of the automaton in its natural form.
     *
     * \returns transition function of the automaton
     */
    const ext::multimap<ext::tuple<StateType, common::symbol_or_epsilon<InputSymbolType>, PushdownStoreSymbolType>, ext::pair<StateType, ext::vector<PushdownStoreSymbolType>>>& getTransitions() const&;

    /**
     * Get the transition function of the automaton in its natural form.
     *
     * \returns transition function of the automaton
     */
    ext::multimap<ext::tuple<StateType, common::symbol_or_epsilon<InputSymbolType>, PushdownStoreSymbolType>, ext::pair<StateType, ext::vector<PushdownStoreSymbolType>>>&& getTransitions() &&;

    /**
     * The three way comparison implementation
     *
     * \param other the other instance
     *
     * \returns the ordering between this object and the @p other.
     */
    auto operator<=>(const SinglePopNPDA& other) const
    {
        return std::tie(getStates(), getInputAlphabet(), getInitialState(), getFinalStates(), getPushdownStoreAlphabet(), getInitialSymbol(), transitions) <=> std::tie(other.getStates(), other.getInputAlphabet(), other.getInitialState(), other.getFinalStates(), other.getPushdownStoreAlphabet(), other.getInitialSymbol(), other.transitions);
    }

    /**
     * The equality comparison implementation.
     *
     * \param other the other object to compare with.
     *
     * \returns true if this and other objects are equal, false othervise
     */
    bool operator==(const SinglePopNPDA& other) const
    {
        return std::tie(getStates(), getInputAlphabet(), getInitialState(), getFinalStates(), getPushdownStoreAlphabet(), getInitialSymbol(), transitions) == std::tie(other.getStates(), other.getInputAlphabet(), other.getInitialState(), other.getFinalStates(), other.getPushdownStoreAlphabet(), other.getInitialSymbol(), other.transitions);
    }

    /**
     * Print this object as raw representation to ostream.
     *
     * \param out ostream where to print
     * \param instance object to print
     *
     * \returns modified output stream
     */
    friend ext::ostream& operator<<(ext::ostream& out, const SinglePopNPDA& instance)
    {
        return out << "(SinglePopNPDA"
                   << " states = " << instance.getStates()
                   << " inputAlphabet = " << instance.getInputAlphabet()
                   << " initialState = " << instance.getInitialState()
                   << " finalStates = " << instance.getFinalStates()
                   << " pushdownStoreAlphabet = " << instance.getPushdownStoreAlphabet()
                   << " initialSymbol = " << instance.getInitialSymbol()
                   << " transitions = " << instance.getTransitions()
                   << ")";
    }
};

template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>::SinglePopNPDA(ext::set<StateType> states, ext::set<InputSymbolType> inputAlphabet, ext::set<PushdownStoreSymbolType> pushdownStoreAlphabet, StateType initialState, PushdownStoreSymbolType initialSymbol, ext::set<StateType> finalStates)
    : core::Components<SinglePopNPDA, ext::set<InputSymbolType>, module::Set, component::InputAlphabet, ext::set<PushdownStoreSymbolType>, module::Set, component::PushdownStoreAlphabet, PushdownStoreSymbolType, module::Value, component::InitialSymbol, ext::set<StateType>, module::Set, std::tuple<component::States, component::FinalStates>, StateType, module::Value, component::InitialState>(std::move(inputAlphabet), std::move(pushdownStoreAlphabet), std::move(initialSymbol), std::move(states), std::move(finalStates), std::move(initialState))
{
}

template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>::SinglePopNPDA(StateType initialState, PushdownStoreSymbolType initialPushdownSymbol)
    : SinglePopNPDA(ext::set<StateType>{initialState}, ext::set<InputSymbolType>{}, ext::set<PushdownStoreSymbolType>{initialPushdownSymbol}, initialState, initialPushdownSymbol, ext::set<StateType>{})
{
}

template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
bool SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>::addTransition(StateType from, common::symbol_or_epsilon<InputSymbolType> input, PushdownStoreSymbolType pop, StateType to, ext::vector<PushdownStoreSymbolType> push)
{
    if (!getStates().count(from)) {
        throw AutomatonException("State \"" + ext::to_string(from) + "\" doesn't exist.");
    }

    if (!input.is_epsilon() && !getInputAlphabet().count(input.getSymbol())) {
        throw AutomatonException("Input symbol \"" + ext::to_string(input) + "\" doesn't exist.");
    }

    if (!getStates().count(to)) {
        throw AutomatonException("State \"" + ext::to_string(to) + "\" doesn't exist.");
    }

    if (!getPushdownStoreAlphabet().count(pop)) {
        throw AutomatonException("Pushdown store symbol \"" + ext::to_string(pop) + "\" doesn't exist.");
    }

    for (const PushdownStoreSymbolType& pushSymbol : push) {
        if (!getPushdownStoreAlphabet().count(pushSymbol)) {
            throw AutomatonException("Pushdown store symbol \"" + ext::to_string(pushSymbol) + "\" doesn't exist.");
        }
    }

    auto upper_bound = transitions.upper_bound(ext::tie(from, input, pop));
    auto lower_bound = transitions.lower_bound(ext::tie(from, input, pop));

    ext::pair<StateType, ext::vector<PushdownStoreSymbolType>> value = ext::make_pair(std::move(to), std::move(push));

    auto iter = std::lower_bound(lower_bound, upper_bound, value, [](const auto& transition, const auto& target) { return transition.second < target; });
    if (iter != upper_bound && value >= iter->second)
        return false;

    ext::tuple<StateType, common::symbol_or_epsilon<InputSymbolType>, PushdownStoreSymbolType> key(std::move(from), std::move(input), std::move(pop));
    transitions.insert(iter, std::make_pair(std::move(key), std::move(value)));
    return true;
}

template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
bool SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>::addTransition(StateType from, InputSymbolType input, PushdownStoreSymbolType pop, StateType to, ext::vector<PushdownStoreSymbolType> push)
{
    common::symbol_or_epsilon<InputSymbolType> inputVariant(std::move(input));
    return addTransition(std::move(from), std::move(inputVariant), std::move(pop), std::move(to), std::move(push));
}

template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
bool SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>::addTransition(StateType from, PushdownStoreSymbolType pop, StateType to, ext::vector<PushdownStoreSymbolType> push)
{
    auto inputVariant = common::symbol_or_epsilon<InputSymbolType>();
    return addTransition(std::move(from), std::move(inputVariant), std::move(pop), std::move(to), std::move(push));
}

template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
bool SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>::removeTransition(const StateType& from, const common::symbol_or_epsilon<InputSymbolType>& input, const PushdownStoreSymbolType& pop, const StateType& to, const ext::vector<PushdownStoreSymbolType>& push)
{
    auto upper_bound = transitions.upper_bound(ext::tie(from, input, pop));
    auto lower_bound = transitions.lower_bound(ext::tie(from, input, pop));
    auto iter = std::find_if(lower_bound, upper_bound, [&](const auto& transition) { return transition.second.first == to && transition.second.second == push; });
    if (iter == upper_bound)
        return false;

    transitions.erase(iter);
    return true;
}

template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
bool SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>::removeTransition(const StateType& from, const InputSymbolType& input, const PushdownStoreSymbolType& pop, const StateType& to, const ext::vector<PushdownStoreSymbolType>& push)
{
    common::symbol_or_epsilon<InputSymbolType> inputVariant(input);
    return removeTransition(from, inputVariant, pop, to, push);
}

template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
bool SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>::removeTransition(const StateType& from, const PushdownStoreSymbolType& pop, const StateType& to, const ext::vector<PushdownStoreSymbolType>& push)
{
    auto inputVariant = common::symbol_or_epsilon<InputSymbolType>();
    return removeTransition(from, inputVariant, pop, to, push);
}

template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
const ext::multimap<ext::tuple<StateType, common::symbol_or_epsilon<InputSymbolType>, PushdownStoreSymbolType>, ext::pair<StateType, ext::vector<PushdownStoreSymbolType>>>& SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>::getTransitions() const&
{
    return transitions;
}

template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
ext::multimap<ext::tuple<StateType, common::symbol_or_epsilon<InputSymbolType>, PushdownStoreSymbolType>, ext::pair<StateType, ext::vector<PushdownStoreSymbolType>>>&& SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>::getTransitions() &&
{
    return std::move(transitions);
}

} /* namespace automaton */

namespace core {

/**
 * Helper class specifying constraints for the automaton's internal input alphabet component.
 *
 * \tparam InputSymbolType used for the terminal alphabet of the automaton.
 * \tparam PushdownSymbolType used for the pushdown store alphabet of the automaton.
 * \tparam StateType used for the terminal alphabet of the automaton.
 */
template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
class SetConstraint<automaton::SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>, InputSymbolType, component::InputAlphabet> {
public:
    /**
     * Returns true if the symbol is still used in some transition of the automaton.
     *
     * \param automaton the tested automaton
     * \param symbol the tested symbol
     *
     * \returns true if the symbol is used, false othervise
     */
    static bool used(const automaton::SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>& automaton, const InputSymbolType& symbol)
    {
        for (const std::pair<const ext::tuple<StateType, common::symbol_or_epsilon<InputSymbolType>, PushdownStoreSymbolType>, ext::pair<StateType, ext::vector<PushdownStoreSymbolType>>>& transition : automaton.getTransitions())
            if (!std::get<1>(transition.first).is_epsilon() && symbol == std::get<1>(transition.first).getSymbol())
                return true;

        return false;
    }

    /**
     * Returns true as all symbols are possibly available to be elements of the input alphabet.
     *
     * \param automaton the tested automaton
     * \param symbol the tested symbol
     *
     * \returns true
     */
    static bool available(const automaton::SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>&, const InputSymbolType&)
    {
        return true;
    }

    /**
     * All symbols are valid as input symbols.
     *
     * \param automaton the tested automaton
     * \param symbol the tested symbol
     */
    static void valid(const automaton::SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>&, const InputSymbolType&)
    {
    }
};

/**
 * Helper class specifying constraints for the automaton's internal pushdown store alphabet component.
 *
 * \tparam InputSymbolType used for the terminal alphabet of the automaton.
 * \tparam PushdownSymbolType used for the pushdown store alphabet of the automaton.
 * \tparam StateType used for the terminal alphabet of the automaton.
 */
template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
class SetConstraint<automaton::SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>, PushdownStoreSymbolType, component::PushdownStoreAlphabet> {
public:
    /**
     * Returns true if the symbol is still used in some transition of the automaton.
     *
     * \param automaton the tested automaton
     * \param symbol the tested symbol
     *
     * \returns true if the symbol is used, false othervise
     */
    static bool used(const automaton::SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>& automaton, const PushdownStoreSymbolType& symbol)
    {
        if (automaton.getInitialSymbol() == symbol)
            return true;

        for (const std::pair<const ext::tuple<StateType, common::symbol_or_epsilon<InputSymbolType>, PushdownStoreSymbolType>, ext::pair<StateType, ext::vector<PushdownStoreSymbolType>>>& transition : automaton.getTransitions()) {
            if (symbol == std::get<2>(transition.first))
                return true;

            if (std::find(transition.second.second.begin(), transition.second.second.end(), symbol) != transition.second.second.end())
                return true;
        }

        return false;
    }

    /**
     * Returns true as all symbols are possibly available to be elements of the pushdown store alphabet.
     *
     * \param automaton the tested automaton
     * \param symbol the tested symbol
     *
     * \returns true
     */
    static bool available(const automaton::SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>&, const PushdownStoreSymbolType&)
    {
        return true;
    }

    /**
     * All symbols are valid as pushdown store symbols.
     *
     * \param automaton the tested automaton
     * \param symbol the tested symbol
     */
    static void valid(const automaton::SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>&, const PushdownStoreSymbolType&)
    {
    }
};

/**
 * Helper class specifying constraints for the automaton's internal pushdown store initial element.
 *
 * \tparam InputSymbolType used for the terminal alphabet of the automaton.
 * \tparam PushdownSymbolType used for the pushdown store alphabet of the automaton.
 * \tparam StateType used for the terminal alphabet of the automaton.
 */
template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
class ElementConstraint<automaton::SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>, PushdownStoreSymbolType, component::InitialSymbol> {
public:
    /**
     * Determines whether the initial pushdown store symbol is available in the automaton's pushdown store alphabet.
     *
     * \param automaton the tested automaton
     * \param symbol the tested symbol
     *
     * \returns true if the pushdown store symbol is already in the pushdown store alphabet of the automaton
     */
    static bool available(const automaton::SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>& automaton, const PushdownStoreSymbolType& symbol)
    {
        return automaton.template accessComponent<component::PushdownStoreAlphabet>().get().count(symbol);
    }

    /**
     * All pushdown store symbols are valid as an initial pusdown store symbol of the automaton.
     *
     * \param automaton the tested automaton
     * \param symbol the tested symbol
     */
    static void valid(const automaton::SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>&, const PushdownStoreSymbolType&)
    {
    }
};

/**
 * Helper class specifying constraints for the automaton's internal states component.
 *
 * \tparam InputSymbolType used for the terminal alphabet of the automaton.
 * \tparam PushdownSymbolType used for the pushdown store alphabet of the automaton.
 * \tparam StateType used for the terminal alphabet of the automaton.
 */
template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
class SetConstraint<automaton::SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>, StateType, component::States> {
public:
    /**
     * Returns true if the state is still used in some transition of the automaton.
     *
     * \param automaton the tested automaton
     * \param state the tested state
     *
     * \returns true if the state is used, false othervise
     */
    static bool used(const automaton::SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>& automaton, const StateType& state)
    {
        if (automaton.getInitialState() == state)
            return true;

        if (automaton.getFinalStates().count(state))
            return true;

        for (const std::pair<const ext::tuple<StateType, common::symbol_or_epsilon<InputSymbolType>, PushdownStoreSymbolType>, ext::pair<StateType, ext::vector<PushdownStoreSymbolType>>>& transition : automaton.getTransitions()) {
            if (state == std::get<0>(transition.first))
                return true;

            if (transition.second.first == state)
                return true;
        }

        return false;
    }

    /**
     * Returns true as all states are possibly available to be elements of the states.
     *
     * \param automaton the tested automaton
     * \param state the tested state
     *
     * \returns true
     */
    static bool available(const automaton::SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>&, const StateType&)
    {
        return true;
    }

    /**
     * All states are valid as a state of the automaton.
     *
     * \param automaton the tested automaton
     * \param state the tested state
     */
    static void valid(const automaton::SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>&, const StateType&)
    {
    }
};

/**
 * Helper class specifying constraints for the automaton's internal final states component.
 *
 * \tparam InputSymbolType used for the terminal alphabet of the automaton.
 * \tparam PushdownSymbolType used for the pushdown store alphabet of the automaton.
 * \tparam StateType used for the terminal alphabet of the automaton.
 */
template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
class SetConstraint<automaton::SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>, StateType, component::FinalStates> {
public:
    /**
     * Returns false. Final state is only a mark that the automaton itself does require further.
     *
     * \param automaton the tested automaton
     * \param state the tested state
     *
     * \returns false
     */
    static bool used(const automaton::SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>&, const StateType&)
    {
        return false;
    }

    /**
     * Determines whether the state is available in the automaton's states set.
     *
     * \param automaton the tested automaton
     * \param state the tested state
     *
     * \returns true if the state is already in the set of states of the automaton
     */
    static bool available(const automaton::SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>& automaton, const StateType& state)
    {
        return automaton.template accessComponent<component::States>().get().count(state);
    }

    /**
     * All states are valid as a final state of the automaton.
     *
     * \param automaton the tested automaton
     * \param state the tested state
     */
    static void valid(const automaton::SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>&, const StateType&)
    {
    }
};

/**
 * Helper class specifying constraints for the automaton's internal initial state element.
 *
 * \tparam InputSymbolType used for the terminal alphabet of the automaton.
 * \tparam PushdownSymbolType used for the pushdown store alphabet of the automaton.
 * \tparam StateType used for the terminal alphabet of the automaton.
 */
template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
class ElementConstraint<automaton::SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>, StateType, component::InitialState> {
public:
    /**
     * Determines whether the state is available in the automaton's states set.
     *
     * \param automaton the tested automaton
     * \param state the tested state
     *
     * \returns true if the state is already in the set of states of the automaton
     */
    static bool available(const automaton::SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>& automaton, const StateType& state)
    {
        return automaton.template accessComponent<component::States>().get().count(state);
    }

    /**
     * All states are valid as an initial state of the automaton.
     *
     * \param automaton the tested automaton
     * \param state the tested state
     */
    static void valid(const automaton::SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>&, const StateType&)
    {
    }
};

template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
struct type_util<automaton::SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>> {
    static automaton::SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType> denormalize(automaton::SinglePopNPDA<>&& value)
    {
        ext::set<InputSymbolType> alphabet = alphabet::SymbolDenormalize::denormalizeAlphabet<InputSymbolType>(std::move(value).getInputAlphabet());
        ext::set<PushdownStoreSymbolType> pushdownAlphabet = alphabet::SymbolDenormalize::denormalizeAlphabet<PushdownStoreSymbolType>(std::move(value).getPushdownStoreAlphabet());
        PushdownStoreSymbolType initialSymbol = alphabet::SymbolDenormalize::denormalizeSymbol<PushdownStoreSymbolType>(std::move(value).getInitialSymbol());
        ext::set<StateType> states = automaton::AutomatonDenormalize::denormalizeStates<StateType>(std::move(value).getStates());
        StateType initialState = automaton::AutomatonDenormalize::denormalizeState<StateType>(std::move(value).getInitialState());
        ext::set<StateType> finalStates = automaton::AutomatonDenormalize::denormalizeStates<StateType>(std::move(value).getFinalStates());

        automaton::SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType> res(std::move(states), std::move(alphabet), std::move(pushdownAlphabet), std::move(initialState), std::move(initialSymbol), std::move(finalStates));

        for (std::pair<ext::tuple<DefaultStateType, common::symbol_or_epsilon<DefaultSymbolType>, DefaultSymbolType>, ext::pair<DefaultStateType, ext::vector<DefaultSymbolType>>>&& transition : ext::make_mover(std::move(value).getTransitions())) {
            PushdownStoreSymbolType pop = alphabet::SymbolDenormalize::denormalizeSymbol<PushdownStoreSymbolType>(std::move(std::get<2>(transition.first)));
            StateType from = automaton::AutomatonDenormalize::denormalizeState<StateType>(std::move(std::get<0>(transition.first)));
            common::symbol_or_epsilon<InputSymbolType> input = automaton::AutomatonDenormalize::denormalizeSymbolEpsilon<InputSymbolType>(std::move(std::get<1>(transition.first)));

            StateType to = automaton::AutomatonDenormalize::denormalizeState<StateType>(std::move(transition.second.first));
            ext::vector<PushdownStoreSymbolType> push = alphabet::SymbolDenormalize::denormalizeSymbols<PushdownStoreSymbolType>(std::move(transition.second.second));

            res.addTransition(std::move(from), std::move(input), std::move(pop), std::move(to), std::move(push));
        }

        return res;
    }

    static automaton::SinglePopNPDA<> normalize(automaton::SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>&& value)
    {
        ext::set<DefaultSymbolType> alphabet = alphabet::SymbolNormalize::normalizeAlphabet(std::move(value).getInputAlphabet());
        ext::set<DefaultSymbolType> pushdownAlphabet = alphabet::SymbolNormalize::normalizeAlphabet(std::move(value).getPushdownStoreAlphabet());
        DefaultSymbolType initialSymbol = alphabet::SymbolNormalize::normalizeSymbol(std::move(value).getInitialSymbol());
        ext::set<DefaultStateType> states = automaton::AutomatonNormalize::normalizeStates(std::move(value).getStates());
        DefaultStateType initialState = automaton::AutomatonNormalize::normalizeState(std::move(value).getInitialState());
        ext::set<DefaultStateType> finalStates = automaton::AutomatonNormalize::normalizeStates(std::move(value).getFinalStates());

        automaton::SinglePopNPDA<> res(std::move(states), std::move(alphabet), std::move(pushdownAlphabet), std::move(initialState), std::move(initialSymbol), std::move(finalStates));

        for (std::pair<ext::tuple<StateType, common::symbol_or_epsilon<InputSymbolType>, PushdownStoreSymbolType>, ext::pair<StateType, ext::vector<PushdownStoreSymbolType>>>&& transition : ext::make_mover(std::move(value).getTransitions())) {
            DefaultSymbolType pop = alphabet::SymbolNormalize::normalizeSymbol(std::move(std::get<2>(transition.first)));
            DefaultStateType from = automaton::AutomatonNormalize::normalizeState(std::move(std::get<0>(transition.first)));
            common::symbol_or_epsilon<DefaultSymbolType> input = automaton::AutomatonNormalize::normalizeSymbolEpsilon(std::move(std::get<1>(transition.first)));

            DefaultStateType to = automaton::AutomatonNormalize::normalizeState(std::move(transition.second.first));
            ext::vector<DefaultSymbolType> push = alphabet::SymbolNormalize::normalizeSymbols(std::move(transition.second.second));

            res.addTransition(std::move(from), std::move(input), std::move(pop), std::move(to), std::move(push));
        }

        return res;
    }

    static std::unique_ptr<type_details_base> type(const automaton::SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>& arg)
    {
        core::unique_ptr_set<type_details_base> subTypesInputSymbol;
        for (const InputSymbolType& item : arg.getInputAlphabet())
            subTypesInputSymbol.insert(type_util<InputSymbolType>::type(item));

        core::unique_ptr_set<type_details_base> subTypesPushdownStoreSymbol;
        for (const PushdownStoreSymbolType& item : arg.getPushdownStoreAlphabet())
            subTypesPushdownStoreSymbol.insert(type_util<PushdownStoreSymbolType>::type(item));

        core::unique_ptr_set<type_details_base> subTypesState;
        for (const StateType& item : arg.getStates())
            subTypesState.insert(type_util<StateType>::type(item));

        std::vector<std::unique_ptr<type_details_base>> sub_types_vec;
        sub_types_vec.push_back(type_details_variant_type::make_variant(std::move(subTypesInputSymbol)));
        sub_types_vec.push_back(type_details_variant_type::make_variant(std::move(subTypesPushdownStoreSymbol)));
        sub_types_vec.push_back(type_details_variant_type::make_variant(std::move(subTypesState)));
        return std::make_unique<type_details_template>("automaton::SinglePopNPDA", std::move(sub_types_vec));
    }
};

template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
struct type_details_retriever<automaton::SinglePopNPDA<InputSymbolType, PushdownStoreSymbolType, StateType>> {
    static std::unique_ptr<type_details_base> get()
    {
        std::vector<std::unique_ptr<type_details_base>> sub_types_vec;
        sub_types_vec.push_back(type_details_retriever<InputSymbolType>::get());
        sub_types_vec.push_back(type_details_retriever<PushdownStoreSymbolType>::get());
        sub_types_vec.push_back(type_details_retriever<StateType>::get());
        return std::make_unique<type_details_template>("automaton::SinglePopNPDA", std::move(sub_types_vec));
    }
};

} /* namespace core */

extern template class automaton::SinglePopNPDA<>;
extern template class abstraction::ValueHolder<automaton::SinglePopNPDA<>>;
extern template const automaton::SinglePopNPDA<>& abstraction::retrieveValue<const automaton::SinglePopNPDA<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
extern template class registration::DenormalizationRegisterImpl<const automaton::SinglePopNPDA<>&>;
extern template class registration::NormalizationRegisterImpl<automaton::SinglePopNPDA<>>;
