#include "VisiblyPushdownNPDA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::VisiblyPushdownNPDA<>;
template class abstraction::ValueHolder<automaton::VisiblyPushdownNPDA<>>;
template const automaton::VisiblyPushdownNPDA<>& abstraction::retrieveValue<const automaton::VisiblyPushdownNPDA<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const automaton::VisiblyPushdownNPDA<>&>;
template class registration::NormalizationRegisterImpl<automaton::VisiblyPushdownNPDA<>>;

namespace {

auto valuePrinter = registration::ValuePrinterRegister<automaton::VisiblyPushdownNPDA<>>();

} /* namespace */
