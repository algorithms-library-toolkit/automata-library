#include "SinglePopDPDA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::SinglePopDPDA<>;
template class abstraction::ValueHolder<automaton::SinglePopDPDA<>>;
template const automaton::SinglePopDPDA<>& abstraction::retrieveValue<const automaton::SinglePopDPDA<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const automaton::SinglePopDPDA<>&>;
template class registration::NormalizationRegisterImpl<automaton::SinglePopDPDA<>>;

namespace {

auto valuePrinter = registration::ValuePrinterRegister<automaton::SinglePopDPDA<>>();

} /* namespace */
