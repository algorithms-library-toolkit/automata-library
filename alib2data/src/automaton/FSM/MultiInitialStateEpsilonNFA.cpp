#include "MultiInitialStateEpsilonNFA.h"

#include <registration/CastRegistration.hpp>
#include <registration/ValuePrinterRegistration.hpp>

template class automaton::MultiInitialStateEpsilonNFA<>;
template class abstraction::ValueHolder<automaton::MultiInitialStateEpsilonNFA<>>;
template const automaton::MultiInitialStateEpsilonNFA<>& abstraction::retrieveValue<const automaton::MultiInitialStateEpsilonNFA<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const automaton::MultiInitialStateEpsilonNFA<>&>;
template class registration::NormalizationRegisterImpl<automaton::MultiInitialStateEpsilonNFA<>>;

namespace {

auto valuePrinter = registration::ValuePrinterRegister<automaton::MultiInitialStateEpsilonNFA<>>();

auto multiInitialStateEpsilonNFAFromDFA = registration::CastRegister<automaton::MultiInitialStateEpsilonNFA<>, automaton::DFA<>>();
auto multiInitialStateEpsilonNFAFromNFA = registration::CastRegister<automaton::MultiInitialStateEpsilonNFA<>, automaton::NFA<>>();
auto multiInitialStateEpsilonNFAFromMultiInitialStateNFA = registration::CastRegister<automaton::MultiInitialStateEpsilonNFA<>, automaton::MultiInitialStateNFA<>>();
auto multiInitialStateEpsilonNFAFromEpsilonNFA = registration::CastRegister<automaton::MultiInitialStateEpsilonNFA<>, automaton::EpsilonNFA<>>();

} /* namespace */
