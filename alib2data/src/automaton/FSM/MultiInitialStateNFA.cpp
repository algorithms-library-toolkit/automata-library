#include "MultiInitialStateNFA.h"

#include <registration/CastRegistration.hpp>
#include <registration/ValuePrinterRegistration.hpp>

template class automaton::MultiInitialStateNFA<>;
template class abstraction::ValueHolder<automaton::MultiInitialStateNFA<>>;
template const automaton::MultiInitialStateNFA<>& abstraction::retrieveValue<const automaton::MultiInitialStateNFA<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const automaton::MultiInitialStateNFA<>&>;
template class registration::NormalizationRegisterImpl<automaton::MultiInitialStateNFA<>>;

namespace {

auto valuePrinter = registration::ValuePrinterRegister<automaton::MultiInitialStateNFA<>>();

auto multiInitialStateNFAFromDFA = registration::CastRegister<automaton::MultiInitialStateNFA<>, automaton::DFA<>>();
auto multiInitialStateNFAFromNFA = registration::CastRegister<automaton::MultiInitialStateNFA<>, automaton::NFA<>>();

} /* namespace */
