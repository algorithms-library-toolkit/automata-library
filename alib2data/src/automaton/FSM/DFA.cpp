#include "DFA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::DFA<>;
template class abstraction::ValueHolder<automaton::DFA<>>;
template const automaton::DFA<>& abstraction::retrieveValue<const automaton::DFA<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const automaton::DFA<>&>;
template class registration::NormalizationRegisterImpl<automaton::DFA<>>;

namespace {

auto valuePrinter = registration::ValuePrinterRegister<automaton::DFA<>>();

} /* namespace */
