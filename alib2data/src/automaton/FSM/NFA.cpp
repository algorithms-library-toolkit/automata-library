#include "NFA.h"

#include <registration/CastRegistration.hpp>
#include <registration/ValuePrinterRegistration.hpp>

template class automaton::NFA<>;
template class abstraction::ValueHolder<automaton::NFA<>>;
template const automaton::NFA<>& abstraction::retrieveValue<const automaton::NFA<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const automaton::NFA<>&>;
template class registration::NormalizationRegisterImpl<automaton::NFA<>>;

namespace {

auto valuePrinter = registration::ValuePrinterRegister<automaton::NFA<>>();

auto NFAFromDFA = registration::CastRegister<automaton::NFA<>, automaton::DFA<>>();

} /* namespace */
