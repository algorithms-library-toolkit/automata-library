#include <object/Object.h>
#include "DFTA.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<automaton::DFTA<>>();
auto xmlRead = registration::XmlReaderRegister<automaton::DFTA<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, automaton::DFTA<>>();

} /* namespace */
