#include <object/Object.h>
#include "NondeterministicZAutomaton.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<automaton::NondeterministicZAutomaton<>>();
auto xmlRead = registration::XmlReaderRegister<automaton::NondeterministicZAutomaton<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, automaton::NondeterministicZAutomaton<>>();

} /* namespace */
