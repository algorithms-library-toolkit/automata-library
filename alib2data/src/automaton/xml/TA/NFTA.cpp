#include <object/Object.h>
#include "NFTA.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<automaton::NFTA<>>();
auto xmlRead = registration::XmlReaderRegister<automaton::NFTA<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, automaton::NFTA<>>();

} /* namespace */
