#include <object/Object.h>
#include "CompactNFA.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<automaton::CompactNFA<>>();
auto xmlRead = registration::XmlReaderRegister<automaton::CompactNFA<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, automaton::CompactNFA<>>();

} /* namespace */
