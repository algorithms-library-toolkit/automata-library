#include <object/Object.h>
#include "MultiInitialStateNFA.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<automaton::MultiInitialStateNFA<>>();
auto xmlRead = registration::XmlReaderRegister<automaton::MultiInitialStateNFA<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, automaton::MultiInitialStateNFA<>>();

} /* namespace */
