#include <object/Object.h>
#include "MultiInitialStateEpsilonNFA.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<automaton::MultiInitialStateEpsilonNFA<>>();
auto xmlRead = registration::XmlReaderRegister<automaton::MultiInitialStateEpsilonNFA<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, automaton::MultiInitialStateEpsilonNFA<>>();

} /* namespace */
