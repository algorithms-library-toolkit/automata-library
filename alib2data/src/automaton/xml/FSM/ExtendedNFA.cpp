#include <object/Object.h>
#include "ExtendedNFA.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<automaton::ExtendedNFA<>>();
auto xmlRead = registration::XmlReaderRegister<automaton::ExtendedNFA<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, automaton::ExtendedNFA<>>();

} /* namespace */
