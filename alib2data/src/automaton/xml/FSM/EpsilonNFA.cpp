#include <object/Object.h>
#include "EpsilonNFA.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<automaton::EpsilonNFA<>>();
auto xmlRead = registration::XmlReaderRegister<automaton::EpsilonNFA<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, automaton::EpsilonNFA<>>();

} /* namespace */
