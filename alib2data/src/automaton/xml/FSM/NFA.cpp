#include <object/Object.h>
#include "NFA.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<automaton::NFA<>>();
auto xmlRead = registration::XmlReaderRegister<automaton::NFA<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, automaton::NFA<>>();

} /* namespace */
