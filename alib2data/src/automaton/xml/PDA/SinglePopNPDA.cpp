#include <object/Object.h>
#include "SinglePopNPDA.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<automaton::SinglePopNPDA<>>();
auto xmlRead = registration::XmlReaderRegister<automaton::SinglePopNPDA<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, automaton::SinglePopNPDA<>>();

} /* namespace */
