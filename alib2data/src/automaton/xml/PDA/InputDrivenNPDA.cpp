#include <object/Object.h>
#include "InputDrivenNPDA.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<automaton::InputDrivenNPDA<>>();
auto xmlRead = registration::XmlReaderRegister<automaton::InputDrivenNPDA<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, automaton::InputDrivenNPDA<>>();

} /* namespace */
