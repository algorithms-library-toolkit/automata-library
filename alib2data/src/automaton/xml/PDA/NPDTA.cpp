#include <object/Object.h>
#include "NPDTA.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<automaton::NPDTA<>>();
auto xmlRead = registration::XmlReaderRegister<automaton::NPDTA<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, automaton::NPDTA<>>();

} /* namespace */
