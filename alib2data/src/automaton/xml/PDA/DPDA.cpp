#include <object/Object.h>
#include "DPDA.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<automaton::DPDA<>>();
auto xmlRead = registration::XmlReaderRegister<automaton::DPDA<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, automaton::DPDA<>>();

} /* namespace */
