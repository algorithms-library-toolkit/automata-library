#include <object/Object.h>
#include "InputDrivenDPDA.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<automaton::InputDrivenDPDA<>>();
auto xmlRead = registration::XmlReaderRegister<automaton::InputDrivenDPDA<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, automaton::InputDrivenDPDA<>>();

} /* namespace */
