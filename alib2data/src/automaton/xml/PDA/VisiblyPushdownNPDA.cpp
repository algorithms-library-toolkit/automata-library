#include <object/Object.h>
#include "VisiblyPushdownNPDA.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<automaton::VisiblyPushdownNPDA<>>();
auto xmlRead = registration::XmlReaderRegister<automaton::VisiblyPushdownNPDA<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, automaton::VisiblyPushdownNPDA<>>();

} /* namespace */
