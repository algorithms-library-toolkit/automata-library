#include <object/Object.h>
#include "RealTimeHeightDeterministicNPDA.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<automaton::RealTimeHeightDeterministicNPDA<>>();
auto xmlRead = registration::XmlReaderRegister<automaton::RealTimeHeightDeterministicNPDA<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, automaton::RealTimeHeightDeterministicNPDA<>>();

} /* namespace */
