#include "AutomatonException.h"

namespace automaton {

AutomatonException::AutomatonException(const std::string& cause)
    : CommonException(cause)
{
}

} /* namespace automaton */
