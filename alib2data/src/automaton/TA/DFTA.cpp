#include "DFTA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::DFTA<>;
template class abstraction::ValueHolder<automaton::DFTA<>>;
template const automaton::DFTA<>& abstraction::retrieveValue<const automaton::DFTA<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const automaton::DFTA<>&>;
template class registration::NormalizationRegisterImpl<automaton::DFTA<>>;

namespace {

auto valuePrinter = registration::ValuePrinterRegister<automaton::DFTA<>>();

} /* namespace */
