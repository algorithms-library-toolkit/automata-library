/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <ostream>

#include <alib/multimap>
#include <alib/set>
#include <alib/vector>

#include <core/modules.hpp>

#include <common/DefaultStateType.h>
#include <common/DefaultSymbolType.h>

#include <automaton/AutomatonException.h>

#include <alphabet/common/SymbolDenormalize.h>
#include <alphabet/common/SymbolNormalize.h>
#include <automaton/common/AutomatonDenormalize.h>
#include <automaton/common/AutomatonNormalize.h>
#include <core/type_details_base.hpp>
#include <core/type_util.hpp>

#include <registration/DenormalizationRegistration.hpp>
#include <registration/NormalizationRegistration.hpp>

namespace automaton {

/**
 * \brief
 * Nondeterministic Z-Automaton. Computation model for unranked regular tree languages.
 *
 * \details
 * Defined in  Björklund, Drewes, Satta: Z-Automata for Compact and Direct Representation of Unranked Tree Languages
 * https://doi.org/10.1007/978-3-030-23679-3_7
 *
 * Nondeterministic Z-Automaton is a quadruple
 * A = (\Sigma, Q, R, F), where
 * \Sigma is an input alphabet;
 * Q is the finite set of states, Q \cap T = \emptyset;
 * R is the set of transition rules, each of the form s -> q, q \in Q, s is a tree over \Sigma \cup Q;
 * F is a set of final states
 *
 * \tparam SymbolTypeT used for the symbols of the tree the automaton is executed on.
 * \tparam StateTypeT used for the states, and the initial state of the automaton.
 */
template <class SymbolTypeT = DefaultSymbolType, class StateTypeT = DefaultStateType>
class NondeterministicZAutomaton final : public core::Components<NondeterministicZAutomaton<SymbolTypeT, StateTypeT>, ext::set<SymbolTypeT>, module::Set, component::InputAlphabet, ext::set<StateTypeT>, module::Set, std::tuple<component::States, component::FinalStates>> {
public:
    typedef SymbolTypeT SymbolType;
    typedef StateTypeT StateType;

private:
    /**
     * Transition function as mapping from a list of states times an input symbol on the left hand side to a state.
     */
    ext::multimap<ext::pair<ext::variant<SymbolType, StateType>, ext::vector<ext::variant<SymbolType, StateType>>>, StateType> transitions;

public:
    /**
     * \brief Creates a new instance of the automaton.
     */
    explicit NondeterministicZAutomaton();

    /**
     * \brief Creates a new instance of the automaton with a concrete set of states, input alphabet, and  a set of final states.
     *
     * \param states the initial set of states of the automaton
     * \param inputAlphabet the initial input alphabet
     * \param finalStates the initial set of final states of the automaton
     */
    explicit NondeterministicZAutomaton(ext::set<StateType> states, ext::set<SymbolType> inputAlphabet, ext::set<StateType> finalStates);

    /**
     * Getter of states.
     *
     * \returns the states of the automaton
     */
    const ext::set<StateType>& getStates() const&
    {
        return this->template accessComponent<component::States>().get();
    }

    /**
     * Getter of states.
     *
     * \returns the states of the automaton
     */
    ext::set<StateType>&& getStates() &&
    {
        return std::move(this->template accessComponent<component::States>().get());
    }

    /**
     * Adder of a state.
     *
     * \param state the new state to be added to a set of states
     *
     * \returns true if the state was indeed added
     */
    bool addState(StateType state)
    {
        return this->template accessComponent<component::States>().add(std::move(state));
    }

    /**
     * Setter of states.
     *
     * \param states completely new set of states
     */
    void setStates(ext::set<StateType> states)
    {
        this->template accessComponent<component::States>().set(std::move(states));
    }

    /**
     * Remover of a state.
     *
     * \param state a state to be removed from a set of states
     *
     * \returns true if the state was indeed removed
     */
    void removeState(const StateType& state)
    {
        this->template accessComponent<component::States>().remove(state);
    }

    /**
     * Getter of final states.
     *
     * \returns the final states of the automaton
     */
    const ext::set<StateType>& getFinalStates() const&
    {
        return this->template accessComponent<component::FinalStates>().get();
    }

    /**
     * Getter of final states.
     *
     * \returns the final states of the automaton
     */
    ext::set<StateType>&& getFinalStates() &&
    {
        return std::move(this->template accessComponent<component::FinalStates>().get());
    }

    /**
     * Adder of a final state.
     *
     * \param state the new state to be added to a set of final states
     *
     * \returns true if the state was indeed added
     */
    bool addFinalState(StateType state)
    {
        return this->template accessComponent<component::FinalStates>().add(std::move(state));
    }

    /**
     * Setter of final states.
     *
     * \param states completely new set of final states
     */
    void setFinalStates(ext::set<StateType> states)
    {
        this->template accessComponent<component::FinalStates>().set(std::move(states));
    }

    /**
     * Remover of a final state.
     *
     * \param state a state to be removed from a set of final states
     *
     * \returns true if the state was indeed removed
     */
    void removeFinalState(const StateType& state)
    {
        this->template accessComponent<component::FinalStates>().remove(state);
    }

    /**
     * Getter of the input alphabet.
     *
     * \returns the input alphabet of the automaton
     */
    const ext::set<SymbolType>& getInputAlphabet() const&
    {
        return this->template accessComponent<component::InputAlphabet>().get();
    }

    /**
     * Getter of the input alphabet.
     *
     * \returns the input alphabet of the automaton
     */
    ext::set<SymbolType>&& getInputAlphabet() &&
    {
        return std::move(this->template accessComponent<component::InputAlphabet>().get());
    }

    /**
     * Adder of a input symbol.
     *
     * \param symbol the new symbol to be added to an input alphabet
     *
     * \returns true if the symbol was indeed added
     */
    bool addInputSymbol(SymbolType symbol)
    {
        return this->template accessComponent<component::InputAlphabet>().add(std::move(symbol));
    }

    /**
     * Adder of input symbols.
     *
     * \param symbols new symbols to be added to an input alphabet
     */
    void addInputSymbols(ext::set<SymbolType> symbols)
    {
        this->template accessComponent<component::InputAlphabet>().add(std::move(symbols));
    }

    /**
     * Setter of input alphabet.
     *
     * \param symbols completely new input alphabet
     */
    void setInputAlphabet(ext::set<SymbolType> symbols)
    {
        this->template accessComponent<component::InputAlphabet>().set(std::move(symbols));
    }

    /**
     * Remover of an input symbol.
     *
     * \param symbol a symbol to be removed from an input alphabet
     *
     * \returns true if the symbol was indeed removed
     */
    void removeInputSymbol(const SymbolType& symbol)
    {
        this->template accessComponent<component::InputAlphabet>().remove(symbol);
    }

    /**
     * \brief Add a transition to the automaton.
     *
     * \details The transition is in a form ( A, B, C, ... ) \times a -> X, where A, B, C, ..., X \in Q and a \in T
     *
     * \param children the source states (A, B, C, ...)
     * \param current the input symbol (a)
     * \param next the target state (B)
     *
     * \throws AutomatonException when transition contains state or symbol not present in the automaton components
     *
     * \returns true if the transition was indeed added
     */
    bool addTransition(ext::variant<SymbolType, StateType> symbol, ext::vector<ext::variant<SymbolType, StateType>> prevStates, StateType next);

    /**
     * \brief Removes a transition from the automaton.
     *
     * \details The transition is in a form ( A, B, C, ... ) \times a -> X, where A, B, C, ..., X \in Q and a \in T
     *
     * \param children the source states (A, B, C, ...)
     * \param current the input symbol (a)
     * \param next the target state (B)
     *
     * \returns true if the transition was indeed removed
     */
    bool removeTransition(const ext::variant<SymbolType, StateType>& symbol, const ext::vector<ext::variant<SymbolType, StateType>>& states, const StateType& next);

    /**
     * Get the transition function of the automaton in its natural form.
     *
     * \returns transition function of the automaton
     */
    const ext::multimap<ext::pair<ext::variant<SymbolType, StateType>, ext::vector<ext::variant<SymbolType, StateType>>>, StateType>& getTransitions() const&
    {
        return transitions;
    }

    /**
     * Get the transition function of the automaton in its natural form.
     *
     * \returns transition function of the automaton
     */
    ext::multimap<ext::pair<ext::variant<SymbolType, StateType>, ext::vector<ext::variant<SymbolType, StateType>>>, StateType>&& getTransitions() &&
    {
        return std::move(transitions);
    }

    /**
     * \returns transitions to state @p q
     */
    ext::multimap<ext::pair<ext::variant<SymbolType, StateType>, ext::vector<ext::variant<SymbolType, StateType>>>, StateType> getTransitionsToState(const StateType& q) const;

    /**
     * \returns transitions from state @p q
     */
    ext::multimap<ext::pair<ext::variant<SymbolType, StateType>, ext::vector<ext::variant<SymbolType, StateType>>>, StateType> getTransitionsFromState(const StateType& q) const;

    /**
     * The three way comparison implementation
     *
     * \param other the other instance
     *
     * \returns the ordering between this object and the @p other.
     */
    auto operator<=>(const NondeterministicZAutomaton& other) const
    {
        return std::tie(getStates(), getInputAlphabet(), getFinalStates(), transitions) <=> std::tie(other.getStates(), other.getInputAlphabet(), other.getFinalStates(), other.transitions);
    }

    /**
     * The equality comparison implementation.
     *
     * \param other the other object to compare with.
     *
     * \returns true if this and other objects are equal, false othervise
     */
    bool operator==(const NondeterministicZAutomaton& other) const
    {
        return std::tie(getStates(), getInputAlphabet(), getFinalStates(), transitions) == std::tie(other.getStates(), other.getInputAlphabet(), other.getFinalStates(), other.transitions);
    }

    /**
     * Print this object as raw representation to ostream.
     *
     * \param out ostream where to print
     * \param instance object to print
     *
     * \returns modified output stream
     */
    friend ext::ostream& operator<<(ext::ostream& out, const NondeterministicZAutomaton& instance)
    {
        return out << "(NondeterministicZAutomaton "
                   << " states = " << instance.getStates()
                   << " inputAlphabet = " << instance.getInputAlphabet()
                   << " finalStates = " << instance.getFinalStates()
                   << " transitions = " << instance.getTransitions()
                   << ")";
    }
};

template <class SymbolType, class StateType>
NondeterministicZAutomaton<SymbolType, StateType>::NondeterministicZAutomaton(ext::set<StateType> states, ext::set<SymbolType> inputAlphabet, ext::set<StateType> finalStates)
    : core::Components<NondeterministicZAutomaton, ext::set<SymbolType>, module::Set, component::InputAlphabet, ext::set<StateType>, module::Set, std::tuple<component::States, component::FinalStates>>(std::move(inputAlphabet), std::move(states), std::move(finalStates))
{
}

template <class SymbolType, class StateType>
NondeterministicZAutomaton<SymbolType, StateType>::NondeterministicZAutomaton()
    : NondeterministicZAutomaton(ext::set<StateType>{}, ext::set<SymbolType>{}, ext::set<StateType>{})
{
}

template <class SymbolType, class StateType>
bool NondeterministicZAutomaton<SymbolType, StateType>::addTransition(ext::variant<SymbolType, StateType> symbol, ext::vector<ext::variant<SymbolType, StateType>> prevStates, StateType next)
{
    if (!getInputAlphabet().count(symbol) && !getStates().count(symbol))
        throw AutomatonException("Input symbol or state \"" + ext::to_string(symbol) + "\" doesn't exist.");

    if (!getStates().count(next))
        throw AutomatonException("State \"" + ext::to_string(next) + "\" doesn't exist.");

    for (const ext::variant<SymbolType, StateType>& it : prevStates) {
        if (!getInputAlphabet().count(it) && !getStates().count(it))
            throw AutomatonException("Input symbol or state \"" + ext::to_string(it) + "\" doesn't exist.");
    }

    auto upper_bound = transitions.upper_bound(ext::tie(symbol, prevStates));
    auto lower_bound = transitions.lower_bound(ext::tie(symbol, prevStates));
    auto iter = std::lower_bound(lower_bound, upper_bound, next, [](const auto& transition, const auto& target) { return transition.second < target; });
    if (iter != upper_bound && next >= iter->second)
        return false;

    ext::pair<ext::variant<SymbolType, StateType>, ext::vector<ext::variant<SymbolType, StateType>>> key = ext::make_pair(std::move(symbol), std::move(prevStates));
    transitions.insert(iter, std::make_pair(std::move(key), std::move(next)));
    return true;
}

template <class SymbolType, class StateType>
bool NondeterministicZAutomaton<SymbolType, StateType>::removeTransition(const ext::variant<SymbolType, StateType>& symbol, const ext::vector<ext::variant<SymbolType, StateType>>& states, const StateType& next)
{
    auto upper_bound = transitions.upper_bound(ext::tie(symbol, states));
    auto lower_bound = transitions.lower_bound(ext::tie(symbol, states));
    auto iter = std::find_if(lower_bound, upper_bound, [&](const auto& transition) { return transition.second == next; });
    if (iter == upper_bound)
        return false;

    transitions.erase(iter);
    return true;
}

template <class SymbolType, class StateType>
ext::multimap<ext::pair<ext::variant<SymbolType, StateType>, ext::vector<ext::variant<SymbolType, StateType>>>, StateType> NondeterministicZAutomaton<SymbolType, StateType>::getTransitionsToState(const StateType& q) const
{
    ext::multimap<ext::pair<ext::variant<SymbolType, StateType>, ext::vector<ext::variant<SymbolType, StateType>>>, StateType> res;

    for (const auto& transition : getTransitions()) {
        if (transition.second == q)
            res.insert(std::make_pair(transition.first, q));
    }

    return res;
}

template <class SymbolType, class StateType>
ext::multimap<ext::pair<ext::variant<SymbolType, StateType>, ext::vector<ext::variant<SymbolType, StateType>>>, StateType> NondeterministicZAutomaton<SymbolType, StateType>::getTransitionsFromState(const StateType& q) const
{
    ext::multimap<ext::pair<ext::variant<SymbolType, StateType>, ext::vector<ext::variant<SymbolType, StateType>>>, StateType> res;

    for (const auto& transition : getTransitions()) {
        if (std::find(transition.first.second.begin(), transition.first.second.end(), q) != transition.first.second.end())
            res.insert(transition);
    }

    return res;
}

} /* namespace automaton */

namespace core {

/**
 * Helper class specifying constraints for the automaton's internal input alphabet component.
 *
 * \tparam SymbolType used for the symbols of the tree the automaton is executed on.
 * \tparam StateType used for the states, and the initial state of the automaton.
 */
template <class SymbolType, class StateType>
class SetConstraint<automaton::NondeterministicZAutomaton<SymbolType, StateType>, SymbolType, component::InputAlphabet> {
public:
    /**
     * Returns true if the symbol is still used in some transition of the automaton.
     *
     * \param automaton the tested automaton
     * \param symbol the tested symbol
     *
     * \returns true if the symbol is used, false othervise
     */
    static bool used(const automaton::NondeterministicZAutomaton<SymbolType, StateType>& automaton, const SymbolType& symbol)
    {
        for (const std::pair<const ext::pair<ext::variant<SymbolType, StateType>, ext::vector<ext::variant<SymbolType, StateType>>>, StateType>& t : automaton.getTransitions())
            if (t.first.first == symbol || ext::contains(t.first.second.begin(), t.first.second.end(), symbol))
                return true;

        return false;
    }

    /**
     * Returns true as all symbols are possibly available to be elements of the input alphabet.
     *
     * \param automaton the tested automaton
     * \param symbol the tested symbol
     *
     * \returns true
     */
    static bool available(const automaton::NondeterministicZAutomaton<SymbolType, StateType>&, const SymbolType&)
    {
        return true;
    }

    /**
     * All symbols are valid as input symbols.
     *
     * \param automaton the tested automaton
     * \param symbol the tested symbol
     */
    static void valid(const automaton::NondeterministicZAutomaton<SymbolType, StateType>& automaton, const SymbolType& symbol)
    {
        if (automaton.template accessComponent<component::States>().get().count(ext::poly_comp(symbol)))
            throw automaton::AutomatonException("Symbol " + ext::to_string(symbol) + " cannot be in the input alphabet since it is already in the states set.");
    }
};

/**
 * Helper class specifying constraints for the automaton's internal states component.
 *
 * \tparam SymbolType used for the symbols of the tree the automaton is executed on.
 * \tparam StateType used for the states, and the initial state of the automaton.
 */
template <class SymbolType, class StateType>
class SetConstraint<automaton::NondeterministicZAutomaton<SymbolType, StateType>, StateType, component::States> {
public:
    /**
     * Returns true if the state is still used in some transition of the automaton.
     *
     * \param automaton the tested automaton
     * \param state the tested state
     *
     * \returns true if the state is used, false othervise
     */
    static bool used(const automaton::NondeterministicZAutomaton<SymbolType, StateType>& automaton, const StateType& state)
    {
        if (automaton.getFinalStates().count(state))
            return true;

        for (const std::pair<const ext::pair<ext::variant<SymbolType, StateType>, ext::vector<ext::variant<SymbolType, StateType>>>, StateType>& t : automaton.getTransitions())
            if (t.first.first == state || ext::contains(t.first.second.begin(), t.first.second.end(), state) || t.second == state)
                return true;

        return false;
    }

    /**
     * Returns true as all states are possibly available to be elements of the states.
     *
     * \param automaton the tested automaton
     * \param state the tested state
     *
     * \returns true
     */
    static bool available(const automaton::NondeterministicZAutomaton<SymbolType, StateType>&, const StateType&)
    {
        return true;
    }

    /**
     * All states are valid as a state of the automaton.
     *
     * \param automaton the tested automaton
     * \param state the tested state
     */
    static void valid(const automaton::NondeterministicZAutomaton<SymbolType, StateType>& automaton, const StateType& state)
    {
        if (automaton.template accessComponent<component::InputAlphabet>().get().count(ext::poly_comp(state)))
            throw automaton::AutomatonException("State " + ext::to_string(state) + " cannot be in the states set since it is already in the input alphabet.");
    }
};

/**
 * Helper class specifying constraints for the automaton's internal final states component.
 *
 * \tparam SymbolType used for the symbols of the tree the automaton is executed on.
 * \tparam StateType used for the states, and the initial state of the automaton.
 */
template <class SymbolType, class StateType>
class SetConstraint<automaton::NondeterministicZAutomaton<SymbolType, StateType>, StateType, component::FinalStates> {
public:
    /**
     * Returns false. Final state is only a mark that the automaton itself does require further.
     *
     * \param automaton the tested automaton
     * \param state the tested state
     *
     * \returns false
     */
    static bool used(const automaton::NondeterministicZAutomaton<SymbolType, StateType>&, const StateType&)
    {
        return false;
    }

    /**
     * Nondetermines whether the state is available in the automaton's states set.
     *
     * \param automaton the tested automaton
     * \param state the tested state
     *
     * \returns true if the state is already in the set of states of the automaton
     */
    static bool available(const automaton::NondeterministicZAutomaton<SymbolType, StateType>& automaton, const StateType& state)
    {
        return automaton.template accessComponent<component::States>().get().count(state);
    }

    /**
     * All states are valid as a final state of the automaton.
     *
     * \param automaton the tested automaton
     * \param state the tested state
     */
    static void valid(const automaton::NondeterministicZAutomaton<SymbolType, StateType>&, const StateType&)
    {
    }
};

template <class SymbolType, class StateType>
struct type_util<automaton::NondeterministicZAutomaton<SymbolType, StateType>> {
    static automaton::NondeterministicZAutomaton<SymbolType, StateType> denormalize(automaton::NondeterministicZAutomaton<>&& value)
    {
        ext::set<SymbolType> alphabet = alphabet::SymbolDenormalize::denormalizeAlphabet<SymbolType>(std::move(value).getInputAlphabet());
        ext::set<StateType> states = automaton::AutomatonDenormalize::denormalizeStates<StateType>(std::move(value).getStates());
        ext::set<StateType> finalStates = automaton::AutomatonDenormalize::denormalizeStates<StateType>(std::move(value).getFinalStates());

        automaton::NondeterministicZAutomaton<SymbolType, StateType> res(std::move(states), std::move(alphabet), std::move(finalStates));

        for (std::pair<ext::pair<ext::variant<DefaultSymbolType, DefaultStateType>, ext::vector<ext::variant<DefaultSymbolType, DefaultStateType>>>, StateType>&& transition : ext::make_mover(std::move(value).getTransitions())) {
            ext::variant<SymbolType, StateType> input = alphabet::SymbolDenormalize::denormalizeVariantSymbol<SymbolType, StateType>(std::move(transition.first.first));
            ext::vector<ext::variant<SymbolType, StateType>> from = alphabet::SymbolDenormalize::denormalizeVariantSymbols<SymbolType, StateType>(std::move(transition.first.second));
            StateType to = automaton::AutomatonDenormalize::denormalizeState<StateType>(std::move(transition.second));

            res.addTransition(std::move(input), std::move(from), std::move(to));
        }

        return res;
    }

    static automaton::NondeterministicZAutomaton<> normalize(automaton::NondeterministicZAutomaton<SymbolType, StateType>&& value)
    {
        ext::set<DefaultSymbolType> alphabet = alphabet::SymbolNormalize::normalizeAlphabet(std::move(value).getInputAlphabet());
        ext::set<DefaultStateType> states = automaton::AutomatonNormalize::normalizeStates(std::move(value).getStates());
        ext::set<DefaultStateType> finalStates = automaton::AutomatonNormalize::normalizeStates(std::move(value).getFinalStates());

        automaton::NondeterministicZAutomaton<> res(std::move(states), std::move(alphabet), std::move(finalStates));

        for (std::pair<ext::pair<ext::variant<SymbolType, StateType>, ext::vector<ext::variant<SymbolType, StateType>>>, StateType>&& transition : ext::make_mover(std::move(value).getTransitions())) {
            ext::variant<DefaultSymbolType, DefaultStateType> input = alphabet::SymbolNormalize::normalizeVariantSymbol(std::move(transition.first.first));
            ext::vector<ext::variant<DefaultSymbolType, DefaultStateType>> from = alphabet::SymbolNormalize::normalizeVariantSymbols(std::move(transition.first.second));
            DefaultStateType to = automaton::AutomatonNormalize::normalizeState(std::move(transition.second));

            res.addTransition(std::move(input), std::move(from), std::move(to));
        }

        return res;
    }

    static std::unique_ptr<type_details_base> type(const automaton::NondeterministicZAutomaton<SymbolType, StateType>& arg)
    {
        core::unique_ptr_set<type_details_base> subTypesSymbol;
        for (const SymbolType& item : arg.getInputAlphabet())
            subTypesSymbol.insert(type_util<SymbolType>::type(item));

        core::unique_ptr_set<type_details_base> subTypesState;
        for (const StateType& item : arg.getStates())
            subTypesState.insert(type_util<StateType>::type(item));

        std::vector<std::unique_ptr<type_details_base>> sub_types_vec;
        sub_types_vec.push_back(type_details_variant_type::make_variant(std::move(subTypesSymbol)));
        sub_types_vec.push_back(type_details_variant_type::make_variant(std::move(subTypesState)));
        return std::make_unique<type_details_template>("automaton::NondeterministicZAutomaton", std::move(sub_types_vec));
    }
};

template <class SymbolType, class StateType>
struct type_details_retriever<automaton::NondeterministicZAutomaton<SymbolType, StateType>> {
    static std::unique_ptr<type_details_base> get()
    {
        std::vector<std::unique_ptr<type_details_base>> sub_types_vec;
        sub_types_vec.push_back(type_details_retriever<SymbolType>::get());
        sub_types_vec.push_back(type_details_retriever<StateType>::get());
        return std::make_unique<type_details_template>("automaton::NondeterministicZAutomaton", std::move(sub_types_vec));
    }
};

} /* namespace core */

extern template class automaton::NondeterministicZAutomaton<>;
extern template class abstraction::ValueHolder<automaton::NondeterministicZAutomaton<>>;
extern template const automaton::NondeterministicZAutomaton<>& abstraction::retrieveValue<const automaton::NondeterministicZAutomaton<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
extern template class registration::DenormalizationRegisterImpl<const automaton::NondeterministicZAutomaton<>&>;
extern template class registration::NormalizationRegisterImpl<automaton::NondeterministicZAutomaton<>>;
