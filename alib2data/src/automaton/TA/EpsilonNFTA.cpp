#include "EpsilonNFTA.h"

#include <registration/CastRegistration.hpp>
#include <registration/ValuePrinterRegistration.hpp>

template class automaton::EpsilonNFTA<>;
template class abstraction::ValueHolder<automaton::EpsilonNFTA<>>;
template const automaton::EpsilonNFTA<>& abstraction::retrieveValue<const automaton::EpsilonNFTA<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const automaton::EpsilonNFTA<>&>;
template class registration::NormalizationRegisterImpl<automaton::EpsilonNFTA<>>;

namespace {

auto valuePrinter = registration::ValuePrinterRegister<automaton::EpsilonNFTA<>>();

auto EpsilonNFTAFromDFTA = registration::CastRegister<automaton::EpsilonNFTA<>, automaton::DFTA<>>();
auto EpsilonNFTAFromNFTA = registration::CastRegister<automaton::EpsilonNFTA<>, automaton::NFTA<>>();

} /* namespace */
