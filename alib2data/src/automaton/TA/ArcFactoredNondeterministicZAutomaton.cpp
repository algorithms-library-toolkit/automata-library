#include "ArcFactoredNondeterministicZAutomaton.h"

#include <registration/CastRegistration.hpp>
#include <registration/ValuePrinterRegistration.hpp>

template class automaton::ArcFactoredNondeterministicZAutomaton<>;
template class abstraction::ValueHolder<automaton::ArcFactoredNondeterministicZAutomaton<>>;
template const automaton::ArcFactoredNondeterministicZAutomaton<>& abstraction::retrieveValue<const automaton::ArcFactoredNondeterministicZAutomaton<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const automaton::ArcFactoredNondeterministicZAutomaton<>&>;
template class registration::NormalizationRegisterImpl<automaton::ArcFactoredNondeterministicZAutomaton<>>;

namespace {

auto valuePrinter = registration::ValuePrinterRegister<automaton::ArcFactoredNondeterministicZAutomaton<>>();

auto ArcFactoredNondeterministicZAutomatonFromArcFactoredDeterministicZAutomaton = registration::CastRegister<automaton::ArcFactoredNondeterministicZAutomaton<>, automaton::ArcFactoredDeterministicZAutomaton<>>();

} /* namespace */
