#include "UnorderedDFTA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::UnorderedDFTA<>;
template class abstraction::ValueHolder<automaton::UnorderedDFTA<>>;
template const automaton::UnorderedDFTA<>& abstraction::retrieveValue<const automaton::UnorderedDFTA<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const automaton::UnorderedDFTA<>&>;
template class registration::NormalizationRegisterImpl<automaton::UnorderedDFTA<>>;

namespace {

auto valuePrinter = registration::ValuePrinterRegister<automaton::UnorderedDFTA<>>();

} /* namespace */
