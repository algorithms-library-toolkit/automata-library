#include "BottomOfTheStack.h"

#include <registration/ValuePrinterRegistration.hpp>

namespace alphabet {

BottomOfTheStack::BottomOfTheStack() = default;

ext::ostream& operator<<(ext::ostream& out, const BottomOfTheStack&)
{
    return out << "(BottomOfTheStack)";
}

} /* namespace alphabet */

namespace core {

alphabet::BottomOfTheStack type_util<alphabet::BottomOfTheStack>::denormalize(alphabet::BottomOfTheStack&& arg)
{
    return arg;
}

alphabet::BottomOfTheStack type_util<alphabet::BottomOfTheStack>::normalize(alphabet::BottomOfTheStack&& arg)
{
    return arg;
}

std::unique_ptr<type_details_base> type_util<alphabet::BottomOfTheStack>::type(const alphabet::BottomOfTheStack&)
{
    return std::make_unique<type_details_type>("alphabet::BottomOfTheStack");
}

std::unique_ptr<type_details_base> type_details_retriever<alphabet::BottomOfTheStack>::get()
{
    return std::make_unique<type_details_type>("alphabet::BottomOfTheStack");
}

} /* namespace core */

namespace {

auto valuePrinter = registration::ValuePrinterRegister<alphabet::BottomOfTheStack>();

} /* namespace */
