#pragma once

#include <alphabet/Wildcard.h>
#include <core/xmlApi.hpp>

namespace core {

template <>
struct xmlApi<alphabet::Wildcard> {
    static alphabet::Wildcard parse(ext::deque<sax::Token>::iterator& input);
    static bool first(const ext::deque<sax::Token>::const_iterator& input);
    static std::string xmlTagName();
    static void compose(ext::deque<sax::Token>& output, const alphabet::Wildcard& data);
};

} /* namespace core */
