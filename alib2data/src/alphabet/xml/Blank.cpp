#include <object/Object.h>
#include "Blank.h"

#include <registration/XmlRegistration.hpp>

namespace core {

alphabet::Blank xmlApi<alphabet::Blank>::parse(ext::deque<sax::Token>::iterator& input)
{
    sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());
    sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::END_ELEMENT, xmlTagName());
    return alphabet::Blank();
}

bool xmlApi<alphabet::Blank>::first(const ext::deque<sax::Token>::const_iterator& input)
{
    return sax::FromXMLParserHelper::isToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());
}

std::string xmlApi<alphabet::Blank>::xmlTagName()
{
    return "Blank";
}

void xmlApi<alphabet::Blank>::compose(ext::deque<sax::Token>& output, const alphabet::Blank&)
{
    output.emplace_back(xmlTagName(), sax::Token::TokenType::START_ELEMENT);
    output.emplace_back(xmlTagName(), sax::Token::TokenType::END_ELEMENT);
}

} /* namespace core */

namespace {

auto xmlWrite = registration::XmlWriterRegister<alphabet::Blank>();
auto xmlRead = registration::XmlReaderRegister<alphabet::Blank>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, alphabet::Blank>();

} /* namespace */
