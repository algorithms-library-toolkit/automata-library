#include <object/Object.h>
#include "VariablesBar.h"

#include <registration/XmlRegistration.hpp>

namespace core {

alphabet::VariablesBar xmlApi<alphabet::VariablesBar>::parse(ext::deque<sax::Token>::iterator& input)
{
    sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());
    sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::END_ELEMENT, xmlTagName());
    return alphabet::VariablesBar();
}

bool xmlApi<alphabet::VariablesBar>::first(const ext::deque<sax::Token>::const_iterator& input)
{
    return sax::FromXMLParserHelper::isToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());
}

std::string xmlApi<alphabet::VariablesBar>::xmlTagName()
{
    return "VariablesBar";
}

void xmlApi<alphabet::VariablesBar>::compose(ext::deque<sax::Token>& output, const alphabet::VariablesBar&)
{
    output.emplace_back(xmlTagName(), sax::Token::TokenType::START_ELEMENT);
    output.emplace_back(xmlTagName(), sax::Token::TokenType::END_ELEMENT);
}

} /* namespace core */

namespace {

auto xmlWrite = registration::XmlWriterRegister<alphabet::VariablesBar>();
auto xmlRead = registration::XmlReaderRegister<alphabet::VariablesBar>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, alphabet::VariablesBar>();

} /* namespace */
