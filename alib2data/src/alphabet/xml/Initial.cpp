#include <object/Object.h>
#include "Initial.h"

#include <registration/XmlRegistration.hpp>

namespace core {

alphabet::Initial xmlApi<alphabet::Initial>::parse(ext::deque<sax::Token>::iterator& input)
{
    sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());
    sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::END_ELEMENT, xmlTagName());
    return alphabet::Initial();
}

bool xmlApi<alphabet::Initial>::first(const ext::deque<sax::Token>::const_iterator& input)
{
    return sax::FromXMLParserHelper::isToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());
}

std::string xmlApi<alphabet::Initial>::xmlTagName()
{
    return "Initial";
}

void xmlApi<alphabet::Initial>::compose(ext::deque<sax::Token>& output, const alphabet::Initial&)
{
    output.emplace_back(xmlTagName(), sax::Token::TokenType::START_ELEMENT);
    output.emplace_back(xmlTagName(), sax::Token::TokenType::END_ELEMENT);
}

} /* namespace core */

namespace {

auto xmlWrite = registration::XmlWriterRegister<alphabet::Initial>();
auto xmlRead = registration::XmlReaderRegister<alphabet::Initial>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, alphabet::Initial>();

} /* namespace */
