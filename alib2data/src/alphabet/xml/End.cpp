#include <object/Object.h>
#include "End.h"

#include <registration/XmlRegistration.hpp>

namespace core {

alphabet::End xmlApi<alphabet::End>::parse(ext::deque<sax::Token>::iterator& input)
{
    sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());
    sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::END_ELEMENT, xmlTagName());
    return alphabet::End();
}

bool xmlApi<alphabet::End>::first(const ext::deque<sax::Token>::const_iterator& input)
{
    return sax::FromXMLParserHelper::isToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());
}

std::string xmlApi<alphabet::End>::xmlTagName()
{
    return "End";
}

void xmlApi<alphabet::End>::compose(ext::deque<sax::Token>& output, const alphabet::End&)
{
    output.emplace_back(xmlTagName(), sax::Token::TokenType::START_ELEMENT);
    output.emplace_back(xmlTagName(), sax::Token::TokenType::END_ELEMENT);
}

} /* namespace core */

namespace {

auto xmlWrite = registration::XmlWriterRegister<alphabet::End>();
auto xmlRead = registration::XmlReaderRegister<alphabet::End>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, alphabet::End>();

} /* namespace */
