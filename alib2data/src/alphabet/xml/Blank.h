#pragma once

#include <alphabet/Blank.h>
#include <core/xmlApi.hpp>

namespace core {

template <>
struct xmlApi<alphabet::Blank> {
    static alphabet::Blank parse(ext::deque<sax::Token>::iterator& input);
    static bool first(const ext::deque<sax::Token>::const_iterator& input);
    static std::string xmlTagName();
    static void compose(ext::deque<sax::Token>& output, const alphabet::Blank& data);
};

} /* namespace core */
