#include <object/Object.h>
#include "Gap.h"

#include <registration/XmlRegistration.hpp>

namespace core {

alphabet::Gap xmlApi<alphabet::Gap>::parse(ext::deque<sax::Token>::iterator& input)
{
    sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());
    sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::END_ELEMENT, xmlTagName());
    return alphabet::Gap();
}

bool xmlApi<alphabet::Gap>::first(const ext::deque<sax::Token>::const_iterator& input)
{
    return sax::FromXMLParserHelper::isToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());
}

std::string xmlApi<alphabet::Gap>::xmlTagName()
{
    return "Gap";
}

void xmlApi<alphabet::Gap>::compose(ext::deque<sax::Token>& output, const alphabet::Gap&)
{
    output.emplace_back(xmlTagName(), sax::Token::TokenType::START_ELEMENT);
    output.emplace_back(xmlTagName(), sax::Token::TokenType::END_ELEMENT);
}

} /* namespace core */

namespace {

auto xmlWrite = registration::XmlWriterRegister<alphabet::Gap>();
auto xmlRead = registration::XmlReaderRegister<alphabet::Gap>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, alphabet::Gap>();

} /* namespace */
