#pragma once

#include <common/ranked_symbol.hpp>
#include <core/xmlApi.hpp>
#include <primitive/xml/UnsignedLong.h>

namespace core {

template <typename T>
struct xmlApi<common::ranked_symbol<T>> {
    static common::ranked_symbol<T> parse(ext::deque<sax::Token>::iterator& input);
    static bool first(const ext::deque<sax::Token>::const_iterator& input);
    static std::string xmlTagName();
    static void compose(ext::deque<sax::Token>& output, const common::ranked_symbol<T>& input);
};

template <typename T>
common::ranked_symbol<T> xmlApi<common::ranked_symbol<T>>::parse(ext::deque<sax::Token>::iterator& input)
{
    sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());
    T data = core::xmlApi<T>::parse(input);
    size_t rank = core::xmlApi<size_t>::parse(input);
    sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::END_ELEMENT, xmlTagName());
    return common::ranked_symbol<T>(std::move(data), rank);
}

template <typename T>
bool xmlApi<common::ranked_symbol<T>>::first(const ext::deque<sax::Token>::const_iterator& input)
{
    return sax::FromXMLParserHelper::isToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());
}

template <typename T>
std::string xmlApi<common::ranked_symbol<T>>::xmlTagName()
{
    return "RankedSymbol";
}

template <typename T>
void xmlApi<common::ranked_symbol<T>>::compose(ext::deque<sax::Token>& output, const common::ranked_symbol<T>& input)
{
    output.emplace_back(xmlTagName(), sax::Token::TokenType::START_ELEMENT);
    core::xmlApi<T>::compose(output, input.getSymbol());
    core::xmlApi<size_t>::compose(output, input.getRank());
    output.emplace_back(xmlTagName(), sax::Token::TokenType::END_ELEMENT);
}

} /* namespace core */
