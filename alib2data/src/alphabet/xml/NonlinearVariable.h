#pragma once

#include <alphabet/NonlinearVariable.h>
#include <core/xmlApi.hpp>

namespace core {

template <class T>
struct xmlApi<alphabet::NonlinearVariable<T>> {
    static alphabet::NonlinearVariable<T> parse(ext::deque<sax::Token>::iterator& input);
    static bool first(const ext::deque<sax::Token>::const_iterator& input);
    static std::string xmlTagName();
    static void compose(ext::deque<sax::Token>& output, const alphabet::NonlinearVariable<T>& input);
};

template <typename T>
alphabet::NonlinearVariable<T> xmlApi<alphabet::NonlinearVariable<T>>::parse(ext::deque<sax::Token>::iterator& input)
{
    sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());
    T data = core::xmlApi<T>::parse(input);
    sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::END_ELEMENT, xmlTagName());

    return alphabet::NonlinearVariable<T>(data);
}

template <typename T>
bool xmlApi<alphabet::NonlinearVariable<T>>::first(const ext::deque<sax::Token>::const_iterator& input)
{
    return sax::FromXMLParserHelper::isToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());
}

template <typename T>
std::string xmlApi<alphabet::NonlinearVariable<T>>::xmlTagName()
{
    return "NonlinearVariable";
}

template <typename T>
void xmlApi<alphabet::NonlinearVariable<T>>::compose(ext::deque<sax::Token>& output, const alphabet::NonlinearVariable<T>& input)
{
    output.emplace_back(xmlTagName(), sax::Token::TokenType::START_ELEMENT);
    core::xmlApi<T>::compose(output, input.getSymbol());
    output.emplace_back(xmlTagName(), sax::Token::TokenType::END_ELEMENT);
}

} /* namespace core */
