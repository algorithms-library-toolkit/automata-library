#include <object/Object.h>
#include "Start.h"

#include <registration/XmlRegistration.hpp>

namespace core {

alphabet::Start xmlApi<alphabet::Start>::parse(ext::deque<sax::Token>::iterator& input)
{
    sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());
    sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::END_ELEMENT, xmlTagName());
    return alphabet::Start();
}

bool xmlApi<alphabet::Start>::first(const ext::deque<sax::Token>::const_iterator& input)
{
    return sax::FromXMLParserHelper::isToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());
}

std::string xmlApi<alphabet::Start>::xmlTagName()
{
    return "Start";
}

void xmlApi<alphabet::Start>::compose(ext::deque<sax::Token>& output, const alphabet::Start&)
{
    output.emplace_back(xmlTagName(), sax::Token::TokenType::START_ELEMENT);
    output.emplace_back(xmlTagName(), sax::Token::TokenType::END_ELEMENT);
}

} /* namespace core */

namespace {

auto xmlWrite = registration::XmlWriterRegister<alphabet::Start>();
auto xmlRead = registration::XmlReaderRegister<alphabet::Start>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, alphabet::Start>();

} /* namespace */
