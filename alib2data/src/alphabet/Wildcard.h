/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/type_details_base.hpp>
#include <core/type_util.hpp>

#include <object/Object.h>
#include <object/ObjectFactory.h>

#include <common/ranked_symbol.hpp>

namespace alphabet {

/**
 * \brief
 * Represents a wildcard used as representation of anything here in a string or a tree.
 */
class Wildcard {
public:
    /**
     * \brief
     * Creates a new instance of the symbol.
     */
    explicit Wildcard();

    /**
     * The three way comparison implementation
     *
     * \param other the other instance
     *
     * \returns the ordering between this object and the @p other.
     */
    std::strong_ordering operator<=>(const Wildcard&) const
    {
        return std::strong_ordering::equal;
    }

    /**
     * The equality comparison implementation.
     *
     * \param other the other object to compare with.
     *
     * \returns true if this and other objects are equal, false othervise
     */
    bool operator==(const Wildcard&) const
    {
        return true;
    }

    /**
     * Print this object as raw representation to ostream.
     *
     * \param os ostream where to print
     * \param instance object to print
     *
     * \returns modified output stream
     */
    friend ext::ostream& operator<<(ext::ostream& out, const Wildcard& instance);

    /**
     * \brief Factory for the symbol construction of the symbol based on given type.
     */
    template <typename Base>
    static inline Base instance();
};

template <typename Base>
inline Base Wildcard::instance()
{
    if constexpr (std::is_integral_v<Base>) {
        return '?';
    } else if constexpr (std::is_same_v<Base, std::string>) {
        return std::string(1, Wildcard::instance<char>());
    } else if constexpr (std::is_same_v<Base, Wildcard>) {
        return Wildcard();
    } else if constexpr (std::is_same_v<Base, object::Object>) {
        return object::ObjectFactory<>::construct(Wildcard());
    } else if constexpr (std::is_same_v<Base, common::ranked_symbol<>>) {
        // TODO make partially specialised for different kinds of ranked_symbols
        return common::ranked_symbol<>(Wildcard::instance<DefaultSymbolType>(), 0u);
    } else {
        static_assert(!std::is_same_v<Base, Base>, "Unsupported type of instance");
    }
}

} /* namespace alphabet */

namespace core {

template <>
struct type_util<alphabet::Wildcard> {
    static alphabet::Wildcard denormalize(alphabet::Wildcard&& arg);

    static alphabet::Wildcard normalize(alphabet::Wildcard&& arg);

    static std::unique_ptr<type_details_base> type(const alphabet::Wildcard& arg);
};

template <>
struct type_details_retriever<alphabet::Wildcard> {
    static std::unique_ptr<type_details_base> get();
};

} /* namespace core */
