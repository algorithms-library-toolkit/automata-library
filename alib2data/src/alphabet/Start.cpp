#include "Start.h"

#include <registration/ValuePrinterRegistration.hpp>

namespace alphabet {

Start::Start() = default;

ext::ostream& operator<<(ext::ostream& out, const Start&)
{
    return out << "(Start)";
}

} /* namespace alphabet */

namespace core {

alphabet::Start type_util<alphabet::Start>::denormalize(alphabet::Start&& arg)
{
    return arg;
}

alphabet::Start type_util<alphabet::Start>::normalize(alphabet::Start&& arg)
{
    return arg;
}

std::unique_ptr<type_details_base> type_util<alphabet::Start>::type(const alphabet::Start&)
{
    return std::make_unique<type_details_type>("alphabet::Start");
}

std::unique_ptr<type_details_base> type_details_retriever<alphabet::Start>::get()
{
    return std::make_unique<type_details_type>("alphabet::Start");
}

} /* namespace core */

namespace {

auto valuePrinter = registration::ValuePrinterRegister<alphabet::Start>();

} /* namespace */
