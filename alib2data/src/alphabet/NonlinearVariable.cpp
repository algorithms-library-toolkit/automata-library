#include <registration/ValuePrinterRegistration.hpp>
#include "NonlinearVariable.h"

namespace {

auto valuePrinter = registration::ValuePrinterRegister<alphabet::NonlinearVariable<>>();

} /* namespace */
