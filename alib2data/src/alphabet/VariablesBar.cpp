#include "VariablesBar.h"

#include <registration/ValuePrinterRegistration.hpp>

namespace alphabet {

VariablesBar::VariablesBar() = default;

ext::ostream& operator<<(ext::ostream& out, const VariablesBar&)
{
    return out << "(Variables bar symbol)";
}

} /* namespace alphabet */

namespace core {

alphabet::VariablesBar type_util<alphabet::VariablesBar>::denormalize(alphabet::VariablesBar&& arg)
{
    return arg;
}

alphabet::VariablesBar type_util<alphabet::VariablesBar>::normalize(alphabet::VariablesBar&& arg)
{
    return arg;
}

std::unique_ptr<type_details_base> type_util<alphabet::VariablesBar>::type(const alphabet::VariablesBar&)
{
    return std::make_unique<type_details_type>("alphabet::VariablesBar");
}

std::unique_ptr<type_details_base> type_details_retriever<alphabet::VariablesBar>::get()
{
    return std::make_unique<type_details_type>("alphabet::VariablesBar");
}

} /* namespace core */

namespace {

auto valuePrinter = registration::ValuePrinterRegister<alphabet::VariablesBar>();

} /* namespace */
