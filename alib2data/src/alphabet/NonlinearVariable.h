/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <core/type_details_base.hpp>
#include <core/type_util.hpp>

#include <factory/NormalizeFactory.hpp>

#include <common/DefaultSymbolType.h>

namespace alphabet {

/**
 * \brief
 * Represents the nonlinear variable symbol used in a nonlinear tree pattern.
 */
template <class SymbolType = DefaultSymbolType>
class NonlinearVariable {
    /**
     * \brief the symbol of the nonlinear variable.
     */
    SymbolType m_symbol;

public:
    /**
     * \brief
     * Creates a new instance of the nonlinear variable with some underlying base symbol.
     */
    explicit NonlinearVariable(SymbolType symbol);

    /**
     * Getter of the nonlinear variable's symbol
     *
     * \return the symbol of the nonlinear variable
     */
    const SymbolType& getSymbol() const&;

    /**
     * Getter of the nonlinear variable's symbol
     *
     * \return the symbol of the nonlinear variable
     */
    SymbolType&& getSymbol() &&;

    /**
     * The three way comparison implementation
     *
     * \param other the other instance
     *
     * \returns the ordering between this object and the @p other.
     */
    auto operator<=>(const NonlinearVariable& other) const
    {
        return m_symbol <=> other.m_symbol;
    }

    /**
     * The equality comparison implementation.
     *
     * \param other the other object to compare with.
     *
     * \returns true if this and other objects are equal, false othervise
     */
    bool operator==(const NonlinearVariable& other) const
    {
        return m_symbol == other.m_symbol;
    }

    /**
     * Print this object as raw representation to ostream.
     *
     * \param out ostream where to print
     * \param instance object to print
     *
     * \returns modified output stream
     */
    friend ext::ostream& operator<<(ext::ostream& out, const NonlinearVariable<SymbolType>& instance)
    {
        return out << "(NonlinearVariable " << instance.getSymbol() << ")";
    }
};

template <class SymbolType>
NonlinearVariable<SymbolType>::NonlinearVariable(SymbolType symbol)
    : m_symbol(std::move(symbol))
{
}

template <class SymbolType>
const SymbolType& NonlinearVariable<SymbolType>::getSymbol() const&
{
    return m_symbol;
}

template <class SymbolType>
SymbolType&& NonlinearVariable<SymbolType>::getSymbol() &&
{
    return std::move(m_symbol);
}

} /* namespace alphabet */

namespace core {

template <class SymbolType>
struct type_util<alphabet::NonlinearVariable<SymbolType>> {
    static alphabet::NonlinearVariable<SymbolType> denormalize(alphabet::NonlinearVariable<object::Object>&& arg)
    {
        return alphabet::NonlinearVariable<SymbolType>(factory::NormalizeFactory::denormalize<SymbolType>(std::move(arg).getSymbol()));
    }

    static alphabet::NonlinearVariable<object::Object> normalize(alphabet::NonlinearVariable<SymbolType>&& arg)
    {
        return alphabet::NonlinearVariable<object::Object>(factory::NormalizeFactory::normalize<SymbolType>(std::move(arg).getSymbol()));
    }

    static std::unique_ptr<type_details_base> type(const alphabet::NonlinearVariable<SymbolType>& arg)
    {
        core::unique_ptr_set<type_details_base> subTypes;

        subTypes.insert(type_util<SymbolType>::type(arg.getSymbol()));

        std::vector<std::unique_ptr<type_details_base>> sub_types_vec;
        sub_types_vec.push_back(type_details_variant_type::make_variant(std::move(subTypes)));
        return std::make_unique<type_details_template>("alphabet::NonlinearVariable", std::move(sub_types_vec));
    }
};

template <class SymbolType>
struct type_details_retriever<alphabet::NonlinearVariable<SymbolType>> {
    static std::unique_ptr<type_details_base> get()
    {
        std::vector<std::unique_ptr<type_details_base>> sub_types_vec;
        sub_types_vec.push_back(type_details_retriever<SymbolType>::get());
        return std::make_unique<type_details_template>("alphabet::NonlinearVariable", std::move(sub_types_vec));
    }
};

} /* namespace core */
