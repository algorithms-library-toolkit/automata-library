#pragma once

#include <alib/multiset>
#include <alib/set>
#include <alib/vector>

#include <common/ranked_symbol.hpp>

#include <object/ObjectFactory.h>

namespace alphabet {

/**
 * This class contains methods to print XML representation of automata to the output stream.
 */
class SymbolDenormalize {
public:
    template <class SymbolType>
    static ext::set<SymbolType> denormalizeAlphabet(ext::set<DefaultSymbolType>&& symbols);

    template <class SymbolType>
    static ext::set<common::ranked_symbol<SymbolType>> denormalizeRankedAlphabet(ext::set<common::ranked_symbol<DefaultSymbolType>>&& symbols);

    template <class SymbolType>
    static SymbolType denormalizeSymbol(DefaultSymbolType&& symbol);

    template <class FirstSymbolType, class SecondSymbolType>
    static ext::variant<FirstSymbolType, SecondSymbolType> denormalizeVariantSymbol(ext::variant<DefaultSymbolType, DefaultSymbolType>&& symbol);

    template <class FirstSymbolType, class SecondSymbolType>
    static common::ranked_symbol<ext::variant<FirstSymbolType, SecondSymbolType>> denormalizeRankedVariantSymbol(common::ranked_symbol<ext::variant<DefaultSymbolType, DefaultSymbolType>>&& symbol);

    template <class SymbolType>
    static common::ranked_symbol<SymbolType> denormalizeRankedSymbol(common::ranked_symbol<DefaultSymbolType>&& symbol);

    template <class SymbolType>
    static ext::vector<SymbolType> denormalizeSymbols(ext::vector<DefaultSymbolType>&& symbols);

    template <class FirstSymbolType, class SecondSymbolType>
    static ext::vector<ext::variant<FirstSymbolType, SecondSymbolType>> denormalizeVariantSymbols(ext::vector<ext::variant<DefaultSymbolType, DefaultSymbolType>>&& symbols);

    template <class FirstSymbolType, class SecondSymbolType>
    static ext::multiset<ext::variant<FirstSymbolType, SecondSymbolType>> denormalizeVariantSymbols(ext::multiset<ext::variant<DefaultSymbolType, DefaultSymbolType>>&& symbols);

    template <class SymbolType>
    static ext::vector<common::ranked_symbol<SymbolType>> denormalizeRankedSymbols(ext::vector<common::ranked_symbol<DefaultSymbolType>>&& symbols);
};

template <class SymbolType>
ext::set<SymbolType> SymbolDenormalize::denormalizeAlphabet(ext::set<DefaultSymbolType>&& symbols)
{
    ext::set<SymbolType> res;
    for (DefaultSymbolType&& symbol : ext::make_mover(symbols)) {
        res.insert(denormalizeSymbol<SymbolType>(std::move(symbol)));
    }
    return res;
}

template <class SymbolType>
ext::set<common::ranked_symbol<SymbolType>> SymbolDenormalize::denormalizeRankedAlphabet(ext::set<common::ranked_symbol<DefaultSymbolType>>&& symbols)
{
    ext::set<common::ranked_symbol<SymbolType>> res;
    for (common::ranked_symbol<DefaultSymbolType>&& symbol : ext::make_mover(symbols)) {
        res.insert(denormalizeRankedSymbol<SymbolType>(std::move(symbol)));
    }
    return res;
}

template <class SymbolType>
SymbolType SymbolDenormalize::denormalizeSymbol(DefaultSymbolType&& symbol)
{
    return factory::NormalizeFactory::denormalize<SymbolType>(std::move(symbol));
}

template <class FirstSymbolType, class SecondSymbolType>
ext::variant<FirstSymbolType, SecondSymbolType> SymbolDenormalize::denormalizeVariantSymbol(ext::variant<DefaultSymbolType, DefaultSymbolType>&& symbol)
{
    core::type_details targetType = core::type_details::get<FirstSymbolType>();

    DefaultSymbolType rawSymbol = std::move(symbol.template get<DefaultSymbolType>());
    core::type_details type = core::type_details::get(rawSymbol);

    if (type.compatible_with(targetType))
        return ext::variant<FirstSymbolType, SecondSymbolType>(factory::NormalizeFactory::denormalize<FirstSymbolType>(std::move(rawSymbol)));
    else
        return ext::variant<FirstSymbolType, SecondSymbolType>(factory::NormalizeFactory::denormalize<SecondSymbolType>(std::move(rawSymbol)));
}

template <class FirstSymbolType, class SecondSymbolType>
common::ranked_symbol<ext::variant<FirstSymbolType, SecondSymbolType>> SymbolDenormalize::denormalizeRankedVariantSymbol(common::ranked_symbol<ext::variant<DefaultSymbolType, DefaultSymbolType>>&& symbol)
{
    return common::ranked_symbol<ext::variant<FirstSymbolType, SecondSymbolType>>(denormalizeVariantSymbol<FirstSymbolType, SecondSymbolType>(std::move(symbol).getSymbol()), symbol.getRank());
}

template <class SymbolType>
common::ranked_symbol<SymbolType> SymbolDenormalize::denormalizeRankedSymbol(common::ranked_symbol<DefaultSymbolType>&& symbol)
{
    return common::ranked_symbol<SymbolType>(denormalizeSymbol<SymbolType>(std::move(symbol).getSymbol()), symbol.getRank());
}

template <class SymbolType>
ext::vector<SymbolType> SymbolDenormalize::denormalizeSymbols(ext::vector<DefaultSymbolType>&& symbols)
{
    ext::vector<SymbolType> res;
    for (DefaultSymbolType& symbol : symbols) {
        res.push_back(denormalizeSymbol<SymbolType>(std::move(symbol)));
    }
    return res;
}

template <class FirstSymbolType, class SecondSymbolType>
ext::vector<ext::variant<FirstSymbolType, SecondSymbolType>> SymbolDenormalize::denormalizeVariantSymbols(ext::vector<ext::variant<DefaultSymbolType, DefaultSymbolType>>&& symbols)
{
    ext::vector<ext::variant<FirstSymbolType, SecondSymbolType>> res;
    for (ext::variant<DefaultSymbolType, DefaultSymbolType>& symbol : symbols) {
        res.push_back(denormalizeVariantSymbol<FirstSymbolType, SecondSymbolType>(std::move(symbol)));
    }
    return res;
}

template <class FirstSymbolType, class SecondSymbolType>
ext::multiset<ext::variant<FirstSymbolType, SecondSymbolType>> SymbolDenormalize::denormalizeVariantSymbols(ext::multiset<ext::variant<DefaultSymbolType, DefaultSymbolType>>&& symbols)
{
    ext::multiset<ext::variant<FirstSymbolType, SecondSymbolType>> res;
    for (ext::variant<DefaultSymbolType, DefaultSymbolType>&& symbol : ext::make_mover(symbols)) {
        res.insert(denormalizeVariantSymbol<FirstSymbolType, SecondSymbolType>(std::move(symbol)));
    }
    return res;
}

template <class SymbolType>
ext::vector<common::ranked_symbol<SymbolType>> SymbolDenormalize::denormalizeRankedSymbols(ext::vector<common::ranked_symbol<DefaultSymbolType>>&& symbols)
{
    ext::vector<common::ranked_symbol<SymbolType>> res;
    for (common::ranked_symbol<DefaultSymbolType>& symbol : symbols) {
        res.push_back(denormalizeRankedSymbol<SymbolType>(std::move(symbol)));
    }
    return res;
}

} /* namespace alphabet */
