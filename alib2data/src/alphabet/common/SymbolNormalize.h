#pragma once

#include <alib/multiset>
#include <alib/set>
#include <alib/vector>

#include <common/ranked_symbol.hpp>

#include <object/ObjectFactory.h>

namespace alphabet {

/**
 * This class contains methods to print XML representation of automata to the output stream.
 */
class SymbolNormalize {
public:
    template <class SymbolType>
    static ext::set<DefaultSymbolType> normalizeAlphabet(ext::set<SymbolType>&& symbols);

    template <class SymbolType>
    static ext::set<common::ranked_symbol<DefaultSymbolType>> normalizeRankedAlphabet(ext::set<common::ranked_symbol<SymbolType>>&& symbols);

    template <class SymbolType>
    static DefaultSymbolType normalizeSymbol(SymbolType&& symbol);

    template <class FirstSymbolType, class SecondSymbolType>
    static ext::variant<DefaultSymbolType, DefaultSymbolType> normalizeVariantSymbol(ext::variant<FirstSymbolType, SecondSymbolType>&& symbol);

    template <class FirstSymbolType, class SecondSymbolType>
    static common::ranked_symbol<ext::variant<DefaultSymbolType, DefaultSymbolType>> normalizeRankedVariantSymbol(common::ranked_symbol<ext::variant<FirstSymbolType, SecondSymbolType>>&& symbol);

    template <class SymbolType>
    static common::ranked_symbol<DefaultSymbolType> normalizeRankedSymbol(common::ranked_symbol<SymbolType>&& symbol);

    template <class SymbolType>
    static ext::vector<DefaultSymbolType> normalizeSymbols(ext::vector<SymbolType>&& symbols);

    template <class FirstSymbolType, class SecondSymbolType>
    static ext::vector<ext::variant<DefaultSymbolType, DefaultSymbolType>> normalizeVariantSymbols(ext::vector<ext::variant<FirstSymbolType, SecondSymbolType>>&& symbols);

    template <class FirstSymbolType, class SecondSymbolType>
    static ext::multiset<ext::variant<DefaultSymbolType, DefaultSymbolType>> normalizeVariantSymbols(ext::multiset<ext::variant<FirstSymbolType, SecondSymbolType>>&& symbols);

    template <class SymbolType>
    static ext::vector<common::ranked_symbol<DefaultSymbolType>> normalizeRankedSymbols(ext::vector<common::ranked_symbol<SymbolType>>&& symbols);
};

template <class SymbolType>
ext::set<DefaultSymbolType> SymbolNormalize::normalizeAlphabet(ext::set<SymbolType>&& symbols)
{
    ext::set<DefaultSymbolType> res;
    for (SymbolType&& symbol : ext::make_mover(symbols)) {
        res.insert(normalizeSymbol(std::move(symbol)));
    }
    return res;
}

template <class SymbolType>
ext::set<common::ranked_symbol<DefaultSymbolType>> SymbolNormalize::normalizeRankedAlphabet(ext::set<common::ranked_symbol<SymbolType>>&& symbols)
{
    ext::set<common::ranked_symbol<DefaultSymbolType>> res;
    for (common::ranked_symbol<SymbolType>&& symbol : ext::make_mover(symbols)) {
        res.insert(normalizeRankedSymbol(std::move(symbol)));
    }
    return res;
}

template <class SymbolType>
DefaultSymbolType SymbolNormalize::normalizeSymbol(SymbolType&& symbol)
{
    return factory::NormalizeFactory::normalize<SymbolType>(std::forward<SymbolType>(symbol));
}

template <class FirstSymbolType, class SecondSymbolType>
ext::variant<DefaultSymbolType, DefaultSymbolType> SymbolNormalize::normalizeVariantSymbol(ext::variant<FirstSymbolType, SecondSymbolType>&& symbol)
{
    if (symbol.template is<FirstSymbolType>())
        return ext::variant<DefaultSymbolType, DefaultSymbolType>(normalizeSymbol(std::move(symbol.template get<FirstSymbolType>())));
    else
        return ext::variant<DefaultSymbolType, DefaultSymbolType>(normalizeSymbol(std::move(symbol.template get<SecondSymbolType>())));
}

template <class FirstSymbolType, class SecondSymbolType>
common::ranked_symbol<ext::variant<DefaultSymbolType, DefaultSymbolType>> SymbolNormalize::normalizeRankedVariantSymbol(common::ranked_symbol<ext::variant<FirstSymbolType, SecondSymbolType>>&& symbol)
{
    return common::ranked_symbol<ext::variant<DefaultSymbolType, DefaultSymbolType>>(normalizeSymbol(std::move(symbol).getSymbol()), symbol.getRank());
}

template <class SymbolType>
common::ranked_symbol<DefaultSymbolType> SymbolNormalize::normalizeRankedSymbol(common::ranked_symbol<SymbolType>&& symbol)
{
    return common::ranked_symbol<DefaultSymbolType>(normalizeSymbol(std::move(symbol).getSymbol()), symbol.getRank());
}

template <class SymbolType>
ext::vector<DefaultSymbolType> SymbolNormalize::normalizeSymbols(ext::vector<SymbolType>&& symbols)
{
    ext::vector<DefaultSymbolType> res;
    for (SymbolType& symbol : symbols) {
        res.push_back(normalizeSymbol(std::move(symbol)));
    }
    return res;
}

template <class FirstSymbolType, class SecondSymbolType>
ext::vector<ext::variant<DefaultSymbolType, DefaultSymbolType>> SymbolNormalize::normalizeVariantSymbols(ext::vector<ext::variant<FirstSymbolType, SecondSymbolType>>&& symbols)
{
    ext::vector<ext::variant<DefaultSymbolType, DefaultSymbolType>> res;
    for (ext::variant<FirstSymbolType, SecondSymbolType>& symbol : symbols) {
        res.push_back(normalizeVariantSymbol(std::move(symbol)));
    }
    return res;
}

template <class FirstSymbolType, class SecondSymbolType>
ext::multiset<ext::variant<DefaultSymbolType, DefaultSymbolType>> SymbolNormalize::normalizeVariantSymbols(ext::multiset<ext::variant<FirstSymbolType, SecondSymbolType>>&& symbols)
{
    ext::multiset<ext::variant<DefaultSymbolType, DefaultSymbolType>> res;
    for (ext::variant<FirstSymbolType, SecondSymbolType>&& symbol : ext::make_mover(symbols)) {
        res.insert(normalizeVariantSymbol(std::move(symbol)));
    }
    return res;
}

template <class SymbolType>
ext::vector<common::ranked_symbol<DefaultSymbolType>> SymbolNormalize::normalizeRankedSymbols(ext::vector<common::ranked_symbol<SymbolType>>&& symbols)
{
    ext::vector<common::ranked_symbol<DefaultSymbolType>> res;
    for (common::ranked_symbol<SymbolType>& symbol : symbols) {
        res.push_back(normalizeRankedSymbol(std::move(symbol)));
    }
    return res;
}

} /* namespace alphabet */
