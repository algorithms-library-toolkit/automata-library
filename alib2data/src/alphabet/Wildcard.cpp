#include "Wildcard.h"

#include <registration/ValuePrinterRegistration.hpp>

namespace alphabet {

Wildcard::Wildcard() = default;

ext::ostream& operator<<(ext::ostream& out, const Wildcard&)
{
    return out << "(Wildcard)";
}

} /* namespace alphabet */

namespace core {

alphabet::Wildcard type_util<alphabet::Wildcard>::denormalize(alphabet::Wildcard&& arg)
{
    return arg;
}

alphabet::Wildcard type_util<alphabet::Wildcard>::normalize(alphabet::Wildcard&& arg)
{
    return arg;
}

std::unique_ptr<type_details_base> type_util<alphabet::Wildcard>::type(const alphabet::Wildcard&)
{
    return std::make_unique<type_details_type>("alphabet::Wildcard");
}

std::unique_ptr<type_details_base> type_details_retriever<alphabet::Wildcard>::get()
{
    return std::make_unique<type_details_type>("alphabet::Wildcard");
}

} /* namespace core */

namespace {

auto valuePrinter = registration::ValuePrinterRegister<alphabet::Wildcard>();

} /* namespace */
