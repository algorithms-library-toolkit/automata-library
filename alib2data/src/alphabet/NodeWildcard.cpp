#include "NodeWildcard.h"

#include <registration/ValuePrinterRegistration.hpp>

namespace alphabet {

NodeWildcard::NodeWildcard() = default;

ext::ostream& operator<<(ext::ostream& out, const NodeWildcard&)
{
    return out << "(NodeWildcard)";
}

} /* namespace alphabet */

namespace core {

alphabet::NodeWildcard type_util<alphabet::NodeWildcard>::denormalize(alphabet::NodeWildcard&& arg)
{
    return arg;
}

alphabet::NodeWildcard type_util<alphabet::NodeWildcard>::normalize(alphabet::NodeWildcard&& arg)
{
    return arg;
}

std::unique_ptr<type_details_base> type_util<alphabet::NodeWildcard>::type(const alphabet::NodeWildcard&)
{
    return std::make_unique<type_details_type>("alphabet::NodeWildcard");
}

std::unique_ptr<type_details_base> type_details_retriever<alphabet::NodeWildcard>::get()
{
    return std::make_unique<type_details_type>("alphabet::NodeWildcard");
}

} /* namespace core */

namespace {

auto valuePrinter = registration::ValuePrinterRegister<alphabet::NodeWildcard>();

} /* namespace */
