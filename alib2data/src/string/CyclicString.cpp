#include "CyclicString.h"

#include <registration/CastRegistration.hpp>
#include <registration/ValuePrinterRegistration.hpp>

template class string::CyclicString<>;
template class abstraction::ValueHolder<string::CyclicString<>>;
template const string::CyclicString<>& abstraction::retrieveValue<const string::CyclicString<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const string::CyclicString<>&>;
template class registration::NormalizationRegisterImpl<string::CyclicString<>>;

namespace {

auto valuePrinter = registration::ValuePrinterRegister<string::CyclicString<>>();

} /* namespace */
