#pragma once

#include <core/xmlApi.hpp>
#include <string/LinearString.h>

#include <string/xml/common/StringFromXmlParserCommon.h>
#include <string/xml/common/StringToXmlComposerCommon.h>

namespace core {

template <typename SymbolType>
struct xmlApi<string::LinearString<SymbolType>> {
    static string::LinearString<SymbolType> parse(ext::deque<sax::Token>::iterator& input);
    static bool first(const ext::deque<sax::Token>::const_iterator& input);
    static std::string xmlTagName();
    static void compose(ext::deque<sax::Token>& output, const string::LinearString<SymbolType>& input);
};

template <typename SymbolType>
string::LinearString<SymbolType> xmlApi<string::LinearString<SymbolType>>::parse(ext::deque<sax::Token>::iterator& input)
{
    sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());
    ext::set<SymbolType> alphabet = string::StringFromXmlParserCommon::parseAlphabet<SymbolType>(input);
    ext::vector<SymbolType> content = string::StringFromXmlParserCommon::parseContent<SymbolType>(input);
    sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::END_ELEMENT, xmlTagName());
    return string::LinearString<SymbolType>(std::move(alphabet), std::move(content));
}

template <typename SymbolType>
bool xmlApi<string::LinearString<SymbolType>>::first(const ext::deque<sax::Token>::const_iterator& input)
{
    return sax::FromXMLParserHelper::isToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());
}

template <typename SymbolType>
std::string xmlApi<string::LinearString<SymbolType>>::xmlTagName()
{
    return "LinearString";
}

template <typename SymbolType>
void xmlApi<string::LinearString<SymbolType>>::compose(ext::deque<sax::Token>& output, const string::LinearString<SymbolType>& input)
{
    output.emplace_back(xmlTagName(), sax::Token::TokenType::START_ELEMENT);
    string::StringToXmlComposerCommon::composeAlphabet(output, input.getAlphabet());
    string::StringToXmlComposerCommon::composeContent(output, input.getContent());
    output.emplace_back(xmlTagName(), sax::Token::TokenType::END_ELEMENT);
}

} /* namespace core */
