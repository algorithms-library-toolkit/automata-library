/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once


#include <ext/algorithm>

#include <alib/set>
#include <alib/vector>

#include <object/ObjectFactory.h>

#include <core/modules.hpp>

#include <common/DefaultSymbolType.h>

#include <exception/CommonException.h>

#include <alphabet/common/SymbolDenormalize.h>
#include <alphabet/common/SymbolNormalize.h>
#include <core/type_details_base.hpp>
#include <core/type_util.hpp>

#include <registration/DenormalizationRegistration.hpp>
#include <registration/NormalizationRegistration.hpp>

namespace string {

/**
 * \brief
 * Cyclic string.

 * \details
 * Definition is similar to a linear string however cyclic.
 * S = (A, C),
 * A (Alphabet) = finite set of symbols,
 * C (Content) = linear representation of cyclic string in some rotation
 *
 * Note that two same cyclic strings compare not equal in different rotation. In order to either normalize rotation or compare without influence of particular rotation, use respective algoritm.
 *
 * \tparam SymbolType used for the terminal alphabet
 */
template <class SymbolType = DefaultSymbolType>
class CyclicString final : public core::Components<CyclicString<SymbolType>, ext::set<SymbolType>, module::Set, component::GeneralAlphabet> {
    /**
     * Representation of the string content.
     */
    ext::vector<SymbolType> m_Data;

    ext::vector<SymbolType> asVector(const std::string& str)
    {
        ext::vector<SymbolType> res;
        for (char symbol : str) {
            res.push_back(object::ObjectFactory<SymbolType>::construct(symbol));
        }
        return res;
    }

public:
    /**
     * \brief Creates a new instance of the string with an empty content.
     */
    explicit CyclicString();

    /**
     * \brief Creates a new instance of the string with a concrete alphabet and content.
     *
     * \param alphabet the initial alphabet of the string
     * \param str the initial content of the string
     */
    explicit CyclicString(ext::set<SymbolType> alphabet, ext::vector<SymbolType> str);

    /**
     * \brief Creates a new instance of the string based on content, the alphabet is implicitly created from the content.
     *
     * \param str the initial content of the string
     */
    explicit CyclicString(ext::vector<SymbolType> str);

    /**
     * \brief Creates a new instance of the string from the standard string. The alphabet is deduced from the content. The constructor expects SymbolType of the string is constructible from char.
     *
     * \param str the initial content of the string
     */
    explicit CyclicString(const std::string& str);

    /**
     * \brief Creates a new instance of the string from c-string. The alphabet is deduced from the content. The constructor expects SymbolType of the string is constructible from char.
     *
     * \param str the initial content of the string
     */
    explicit CyclicString(const char* str);

    /**
     * Getter of the alphabet.
     *
     * \returns the alphabet of the string
     */
    const ext::set<SymbolType>& getAlphabet() const&
    {
        return this->template accessComponent<component::GeneralAlphabet>().get();
    }

    /**
     * Getter of the alphabet.
     *
     * \returns the alphabet of the string
     */
    ext::set<SymbolType>&& getAlphabet() &&
    {
        return std::move(this->template accessComponent<component::GeneralAlphabet>().get());
    }

    /**
     * Adder of an alphabet symbols.
     *
     * \param symbols the new symbols to be added to the alphabet
     */
    void extendAlphabet(ext::set<SymbolType> symbols)
    {
        this->template accessComponent<component::GeneralAlphabet>().add(std::move(symbols));
    }

    /**
     * Getter of the string content.
     *
     * \return List of symbols forming string.
     */
    const ext::vector<SymbolType>& getContent() const&;

    /**
     * Getter of the string content.
     *
     * \return List of symbols forming string.
     */
    ext::vector<SymbolType>&& getContent() &&;

    /**
     * Setter of the string content.
     *
     * \throws CommonException when new string contains symbols not present in the alphabet
     *
     * \param new List of symbols forming string.
     */
    void setContent(ext::vector<SymbolType> str);

    /**
     * Test function to determine whether the cyclic string is empty
     *
     * \return true if string is an empty word (vector length is 0)
     */
    bool isEmpty() const;

    /**
     * The three way comparison implementation
     *
     * \param other the other instance
     *
     * \returns the ordering between this object and the @p other.
     */
    auto operator<=>(const CyclicString& other) const
    {
        return std::tie(m_Data, getAlphabet()) <=> std::tie(other.m_Data, other.getAlphabet());
    }

    /**
     * The equality comparison implementation.
     *
     * \param other the other object to compare with.
     *
     * \returns true if this and other objects are equal, false othervise
     */
    bool operator==(const CyclicString& other) const
    {
        return std::tie(m_Data, getAlphabet()) == std::tie(other.m_Data, other.getAlphabet());
    }

    /**
     * Print this object as raw representation to ostream.
     *
     * \param out ostream where to print
     * \param instance object to print
     *
     * \returns modified output stream
     */
    friend ext::ostream& operator<<(ext::ostream& out, const CyclicString& instance)
    {
        out << "(CyclicString";
        out << " content = " << instance.getContent();
        out << " alphabet = " << instance.getAlphabet();
        out << ")";
        return out;
    }
};

template <class SymbolType>
CyclicString<SymbolType>::CyclicString(ext::set<SymbolType> alphabet, ext::vector<SymbolType> str)
    : core::Components<CyclicString, ext::set<SymbolType>, module::Set, component::GeneralAlphabet>(std::move(alphabet))
{
    setContent(std::move(str));
}

template <class SymbolType>
CyclicString<SymbolType>::CyclicString()
    : CyclicString(ext::set<SymbolType>(), ext::vector<SymbolType>())
{
}

template <class SymbolType>
CyclicString<SymbolType>::CyclicString(ext::vector<SymbolType> str)
    : CyclicString(ext::set<SymbolType>(str.begin(), str.end()), str)
{
}

template <class SymbolType>
CyclicString<SymbolType>::CyclicString(const std::string& str)
    : CyclicString(asVector(str))
{
}

template <class SymbolType>
CyclicString<SymbolType>::CyclicString(const char* str)
    : CyclicString(std::string(str))
{
}

template <class SymbolType>
const ext::vector<SymbolType>& CyclicString<SymbolType>::getContent() const&
{
    return this->m_Data;
}

template <class SymbolType>
ext::vector<SymbolType>&& CyclicString<SymbolType>::getContent() &&
{
    return std::move(this->m_Data);
}

// serves as both move and copy content setter
template <class SymbolType>
void CyclicString<SymbolType>::setContent(ext::vector<SymbolType> str)
{
    ext::set<SymbolType> minimalAlphabet(str.begin(), str.end());
    std::set_difference(minimalAlphabet.begin(), minimalAlphabet.end(), getAlphabet().begin(), getAlphabet().end(), ext::callback_iterator([](const SymbolType&) {
                            throw exception::CommonException("Input symbols not in the alphabet.");
                        }));

    m_Data = std::move(str);
}

template <class SymbolType>
bool CyclicString<SymbolType>::isEmpty() const
{
    return this->m_Data.empty();
}

} /* namespace string */

namespace core {

/**
 * Helper class specifying constraints for the string's internal alphabet component.
 *
 * \tparam SymbolType used for the alphabet of the string.
 */
template <class SymbolType>
class SetConstraint<string::CyclicString<SymbolType>, SymbolType, component::GeneralAlphabet> {
public:
    /**
     * Returns true if the symbol is still used in the string.
     *
     * \param string the tested string
     * \param symbol the tested symbol
     *
     * \returns true if the symbol is used, false othervise
     */
    static bool used(const string::CyclicString<SymbolType>& str, const SymbolType& symbol)
    {
        const ext::vector<SymbolType>& content = str.getContent();
        return std::find(content.begin(), content.end(), symbol) != content.end();
    }

    /**
     * Returns true as all symbols are possibly available to be in an alphabet.
     *
     * \param string the tested string
     * \param symbol the tested symbol
     *
     * \returns true
     */
    static bool available(const string::CyclicString<SymbolType>&, const SymbolType&)
    {
        return true;
    }

    /**
     * All symbols are valid as symbols of an alphabet.
     *
     * \param string the tested string
     * \param symbol the tested symbol
     */
    static void valid(const string::CyclicString<SymbolType>&, const SymbolType&)
    {
    }
};

template <class SymbolType>
struct type_util<string::CyclicString<SymbolType>> {
    static string::CyclicString<SymbolType> denormalize(string::CyclicString<>&& value)
    {
        ext::set<SymbolType> alphabet = alphabet::SymbolDenormalize::denormalizeAlphabet<SymbolType>(std::move(value).getAlphabet());
        ext::vector<SymbolType> content = alphabet::SymbolDenormalize::denormalizeSymbols<SymbolType>(std::move(value).getContent());

        return string::CyclicString<SymbolType>(std::move(alphabet), std::move(content));
    }

    static string::CyclicString<> normalize(string::CyclicString<SymbolType>&& value)
    {
        ext::set<DefaultSymbolType> alphabet = alphabet::SymbolNormalize::normalizeAlphabet(std::move(value).getAlphabet());
        ext::vector<DefaultSymbolType> content = alphabet::SymbolNormalize::normalizeSymbols(std::move(value).getContent());
        return string::CyclicString<>(std::move(alphabet), std::move(content));
    }

    static std::unique_ptr<type_details_base> type(const string::CyclicString<SymbolType>& arg)
    {
        core::unique_ptr_set<type_details_base> subTypesSymbol;
        for (const SymbolType& item : arg.getAlphabet())
            subTypesSymbol.insert(type_util<SymbolType>::type(item));

        std::vector<std::unique_ptr<type_details_base>> sub_types_vec;
        sub_types_vec.push_back(type_details_variant_type::make_variant(std::move(subTypesSymbol)));
        return std::make_unique<type_details_template>("string::CyclicString", std::move(sub_types_vec));
    }
};

template <class SymbolType>
struct type_details_retriever<string::CyclicString<SymbolType>> {
    static std::unique_ptr<type_details_base> get()
    {
        std::vector<std::unique_ptr<type_details_base>> sub_types_vec;
        sub_types_vec.push_back(type_details_retriever<SymbolType>::get());
        return std::make_unique<type_details_template>("string::CyclicString", std::move(sub_types_vec));
    }
};

} /* namespace core */

extern template class string::CyclicString<>;
extern template class abstraction::ValueHolder<string::CyclicString<>>;
extern template const string::CyclicString<>& abstraction::retrieveValue<const string::CyclicString<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
extern template class registration::DenormalizationRegisterImpl<const string::CyclicString<>&>;
extern template class registration::NormalizationRegisterImpl<string::CyclicString<>>;
