#include "WildcardLinearString.h"

#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>
#include <registration/ValuePrinterRegistration.hpp>

template class string::WildcardLinearString<>;
template class abstraction::ValueHolder<string::WildcardLinearString<>>;
template const string::WildcardLinearString<>& abstraction::retrieveValue<const string::WildcardLinearString<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const string::WildcardLinearString<>&>;
template class registration::NormalizationRegisterImpl<string::WildcardLinearString<>>;

namespace {

auto components = registration::ComponentRegister<string::WildcardLinearString<>>();

auto WildcardLinearStringFromString = registration::CastRegister<string::WildcardLinearString<char>, std::string>();
auto WildcardLinearStringFromLinearString = registration::CastRegister<string::WildcardLinearString<>, string::LinearString<>>();

auto valuePrinter = registration::ValuePrinterRegister<string::WildcardLinearString<>>();

} /* namespace */
