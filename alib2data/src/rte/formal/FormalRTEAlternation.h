/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <ext/utility>

#include "FormalRTEElement.h"

#include <exception/CommonException.h>

namespace rte {

/**
 * \brief Represents the alternation operator in the regular tree expression. The node must have exactly two children.
 *
 * The structure is derived from BinaryNode that provides the children accessors.
 *
 * The node can be visited by the FormalRTEElement < SymbolType >::Visitor
 *
 * \tparam SymbolType used for the symbol part of the ranked symbol
 */
template <class SymbolType>
class FormalRTEAlternation : public ext::BinaryNode<FormalRTEElement<SymbolType>> {
    /**
     * @copydoc rte::FormalRTEElement < SymbolType >::accept ( ) const &
     */
    void accept(typename FormalRTEElement<SymbolType>::ConstVisitor& visitor) const& override
    {
        visitor.visit(*this);
    }

    /**
     * @copydoc rte::FormalRTEElement < SymbolType >::accept ( ) &
     */
    void accept(typename FormalRTEElement<SymbolType>::Visitor& visitor) & override
    {
        visitor.visit(*this);
    }

    /**
     * @copydoc rte::FormalRTEElement < SymbolType >::accept ( ) &&
     */
    void accept(typename FormalRTEElement<SymbolType>::RValueVisitor& visitor) && override
    {
        visitor.visit(std::move(*this));
    }

public:
    /**
     * \brief Creates a new instance of the alternation node with explicit alternated elements
     *
     * \param left the first alternated element
     * \param right the second alternated element
     */
    explicit FormalRTEAlternation(FormalRTEElement<SymbolType>&& left, FormalRTEElement<SymbolType>&& right);

    /**
     * \brief Creates a new instance of the alternation node with explicit alternated elements
     *
     * \param left the first alternated element
     * \param right the second alternated element
     */
    explicit FormalRTEAlternation(const FormalRTEElement<SymbolType>& left, const FormalRTEElement<SymbolType>& right);

    /**
     * @copydoc FormalRTEElement::clone ( ) const &
     */
    FormalRTEAlternation<SymbolType>* clone() const& override;

    /**
     * @copydoc FormalRTEElement::clone ( ) &&
     */
    FormalRTEAlternation<SymbolType>* clone() && override;

    /**
     * @copydoc FormalRTEElement::testSymbol ( const common::ranked_symbol < SymbolType > & ) const
     */
    bool testSymbol(const common::ranked_symbol<SymbolType>& symbol) const override;

    /**
     * @copydoc FormalRTEElement::computeMinimalAlphabet ( ext::set < common::ranked_symbol < SymbolType > > &, ext::set < common::ranked_symbol < SymbolType > > & ) const
     */
    void computeMinimalAlphabet(ext::set<common::ranked_symbol<SymbolType>>& alphabetF, ext::set<common::ranked_symbol<SymbolType>>& alphabetK) const override;

    /**
     * @copydoc FormalRTEElement::checkAlphabet ( const ext::set < common::ranked_symbol < SymbolType > > &, const ext::set < common::ranked_symbol < SymbolType > > & ) const
     */
    bool checkAlphabet(const ext::set<common::ranked_symbol<SymbolType>>& alphabetF, const ext::set<common::ranked_symbol<SymbolType>>& alphabetK) const override;

    /**
     * Getter of the left alternated element
     *
     * \return left alternated element
     */
    const FormalRTEElement<SymbolType>& getLeftElement() const;

    /**
     * Getter of the right alternated element
     *
     * \return right alternated element
     */
    const FormalRTEElement<SymbolType>& getRightElement() const;

    /**
     * Getter of the left alternated element
     *
     * \return left alternated element
     */
    FormalRTEElement<SymbolType>& getLeftElement();

    /**
     * Getter of the right alternated element
     *
     * \return right alternated element
     */
    FormalRTEElement<SymbolType>& getRightElement();

    /**
     * Setter of the left alternated element
     *
     * \param element left alternated element
     */
    void setLeftElement(FormalRTEElement<SymbolType>&& element);

    /**
     * Setter of the left alternated element
     *
     * \param element left alternated element
     */
    void setLeftElement(const FormalRTEElement<SymbolType>& element);

    /**
     * Setter of the right alternated element
     *
     * \param element right alternated element
     */
    void setRightElement(FormalRTEElement<SymbolType>&& element);

    /**
     * Setter of the right alternated element
     *
     * \param element right alternated element
     */
    void setRightElement(const FormalRTEElement<SymbolType>& element);

    /**
     * @copydoc FormalRTEElement < SymbolType >::operator <=> ( const FormalRTEElement < SymbolType > & other ) const;
     */
    std::strong_ordering operator<=>(const FormalRTEElement<SymbolType>& other) const override
    {
        if (ext::type_index(typeid(*this)) == ext::type_index(typeid(other)))
            return *this <=> static_cast<decltype((*this))>(other);

        return ext::type_index(typeid(*this)) <=> ext::type_index(typeid(other));
    }

    /**
     * The three way comparison implementation
     *
     * \param other the other instance
     *
     * \returns the ordering between this object and the @p other.
     */
    std::strong_ordering operator<=>(const FormalRTEAlternation<SymbolType>&) const;

    /**
     * @copydoc FormalRTEElement < SymbolType >::operator == ( const FormalRTEElement < SymbolType > & other ) const;
     */
    bool operator==(const FormalRTEElement<SymbolType>& other) const override
    {
        if (ext::type_index(typeid(*this)) == ext::type_index(typeid(other)))
            return *this == static_cast<decltype((*this))>(other);

        return false;
    }

    /**
     * The equality comparison implementation.
     *
     * \param other the other object to compare with.
     *
     * \returns true if this and other objects are equal, false othervise
     */
    bool operator==(const FormalRTEAlternation<SymbolType>&) const;

    /**
     * @copydoc FormalRTEElement < SymbolType >::operator == ( const FormalRTEElement < SymbolType > & other ) const;
     */
    void operator>>(ext::ostream& out) const override;
};

template <class SymbolType>
FormalRTEAlternation<SymbolType>::FormalRTEAlternation(FormalRTEElement<SymbolType>&& left, FormalRTEElement<SymbolType>&& right)
    : ext::BinaryNode<FormalRTEElement<SymbolType>>(std::move(left), std::move(right))
{
}

template <class SymbolType>
FormalRTEAlternation<SymbolType>::FormalRTEAlternation(const FormalRTEElement<SymbolType>& left, const FormalRTEElement<SymbolType>& right)
    : FormalRTEAlternation(ext::move_copy(left), ext::move_copy(right))
{
}

template <class SymbolType>
const FormalRTEElement<SymbolType>& FormalRTEAlternation<SymbolType>::getLeftElement() const
{
    return this->getLeft();
}

template <class SymbolType>
const FormalRTEElement<SymbolType>& FormalRTEAlternation<SymbolType>::getRightElement() const
{
    return this->getRight();
}

template <class SymbolType>
FormalRTEElement<SymbolType>& FormalRTEAlternation<SymbolType>::getLeftElement()
{
    return this->getLeft();
}

template <class SymbolType>
FormalRTEElement<SymbolType>& FormalRTEAlternation<SymbolType>::getRightElement()
{
    return this->getRight();
}

template <class SymbolType>
void FormalRTEAlternation<SymbolType>::setLeftElement(const FormalRTEElement<SymbolType>& element)
{
    setLeftElement(ext::move_copy(element));
}

template <class SymbolType>
void FormalRTEAlternation<SymbolType>::setLeftElement(FormalRTEElement<SymbolType>&& element)
{
    this->setLeft(std::move(element));
}

template <class SymbolType>
void FormalRTEAlternation<SymbolType>::setRightElement(const FormalRTEElement<SymbolType>& element)
{
    setRightElement(ext::move_copy(element));
}

template <class SymbolType>
void FormalRTEAlternation<SymbolType>::setRightElement(FormalRTEElement<SymbolType>&& element)
{
    this->setRight(std::move(element));
}

template <class SymbolType>
FormalRTEAlternation<SymbolType>* FormalRTEAlternation<SymbolType>::clone() const&
{
    return new FormalRTEAlternation(*this);
}

template <class SymbolType>
FormalRTEAlternation<SymbolType>* FormalRTEAlternation<SymbolType>::clone() &&
{
    return new FormalRTEAlternation(std::move(*this));
}

template <class SymbolType>
std::strong_ordering FormalRTEAlternation<SymbolType>::operator<=>(const FormalRTEAlternation<SymbolType>& other) const
{
    return std::tie(getLeftElement(), getRightElement()) <=> std::tie(other.getLeftElement(), other.getRightElement());
}

template <class SymbolType>
bool FormalRTEAlternation<SymbolType>::operator==(const FormalRTEAlternation<SymbolType>& other) const
{
    return std::tie(getLeftElement(), getRightElement()) == std::tie(other.getLeftElement(), other.getRightElement());
}

template <class SymbolType>
void FormalRTEAlternation<SymbolType>::operator>>(ext::ostream& out) const
{
    out << "(FormalRTEAlternation";
    out << " " << getLeftElement();
    out << " " << getRightElement();
    out << ")";
}

template <class SymbolType>
bool FormalRTEAlternation<SymbolType>::testSymbol(const common::ranked_symbol<SymbolType>& symbol) const
{
    if (getLeftElement().testSymbol(symbol))
        return true;

    if (getRightElement().testSymbol(symbol))
        return true;

    return false;
}

template <class SymbolType>
void FormalRTEAlternation<SymbolType>::computeMinimalAlphabet(ext::set<common::ranked_symbol<SymbolType>>& alphabetF, ext::set<common::ranked_symbol<SymbolType>>& alphabetK) const
{
    getLeftElement().computeMinimalAlphabet(alphabetF, alphabetK);
    getRightElement().computeMinimalAlphabet(alphabetF, alphabetK);
}

template <class SymbolType>
bool FormalRTEAlternation<SymbolType>::checkAlphabet(const ext::set<common::ranked_symbol<SymbolType>>& alphabetF, const ext::set<common::ranked_symbol<SymbolType>>& alphabetK) const
{
    return getLeftElement().checkAlphabet(alphabetF, alphabetK) && getRightElement().checkAlphabet(alphabetF, alphabetK);
}

} /* namespace rte */

extern template class rte::FormalRTEAlternation<DefaultSymbolType>;
