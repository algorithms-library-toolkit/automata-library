#pragma once

#include "FormalRTEAlternation.h"
#include "FormalRTEElement.h"
#include "FormalRTEEmpty.h"
#include "FormalRTEIteration.h"
#include "FormalRTESubstitution.h"
#include "FormalRTESymbol.h"
#include "FormalRTESymbolAlphabet.h"
#include "FormalRTESymbolSubst.h"
