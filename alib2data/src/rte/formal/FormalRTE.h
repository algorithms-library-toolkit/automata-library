/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <ext/algorithm>
#include <ext/iostream>

#include <alib/set>
#include <alib/string>

#include <core/modules.hpp>
#include <exception/CommonException.h>

#include <common/DefaultSymbolType.h>

#include "FormalRTEStructure.h"

#include <alphabet/common/SymbolDenormalize.h>
#include <alphabet/common/SymbolNormalize.h>
#include <core/type_details_base.hpp>
#include <core/type_util.hpp>
#include <rte/common/RTEDenormalize.h>
#include <rte/common/RTENormalize.h>

#include <registration/DenormalizationRegistration.hpp>
#include <registration/NormalizationRegistration.hpp>

namespace rte {

/**
 * \brief
 * Formal regular tree expression represents regular tree expression. It describes regular tree languages. The expression consists of the following nodes:
 *   - Alternation - the representation of + in the regular tree expression
 *   - Substitution - the representation of &sdot; □ in the regular tree expression
 *   - Iteration - the representation of *, □ in the regular tree expression
 *   - Empty - the representation of empty regular tree expression
 *   - SymbolAlphabet - the representation of a single symbol from symbol alphabet
 *   - SymbolSubst - the representation of a single symbol from the substitution alphabet
 *
 * The formal representation allows nodes of alternation to have exactly two children.
 *
 * The structure of the regular tree expression is wrapped in a structure class to allow separate the alphabets from the structure

 * \details
 * Definition is unbounded definition of regular expressions.
 * E = (T, S, C),
 * T (TerminalAlphabet) = finite set of terminal symbols
 * S (SubstitutionAlphabet) = finite set of substitution symbols
 * C (Content) = representation of the regular expression
 *
 * \tparam SymbolType used for the symbol part of the ranked symbol
 */
template <class SymbolType = DefaultSymbolType>
class FormalRTE final : public core::Components<FormalRTE<SymbolType>, ext::set<common::ranked_symbol<SymbolType>>, module::Set, std::tuple<component::GeneralAlphabet, component::ConstantAlphabet>> {
    /**
     * The structure of the regular tree expression.
     */
    FormalRTEStructure<SymbolType> m_rte;

public:
    /**
     * \brief Creates a new instance of the expression. The default constructor creates expression describing empty language.
     */
    explicit FormalRTE();

    /**
     * \brief Creates a new instance of the expression with a concrete terminal alphabet, substitution alphabet, and initial content.
     *
     * \param alphabetF the initial terminal alphabet
     * \param alphabetK the initial substitution alphabet
     * \param rte the initial rte content
     */
    explicit FormalRTE(ext::set<common::ranked_symbol<SymbolType>> alphabetF, ext::set<common::ranked_symbol<SymbolType>> alphabetK, FormalRTEStructure<SymbolType> rte);

    /**
     * \brief Creates a new instance of the expression with a concrete terminal alphabet, substitution alphabet, and initial content.
     *
     * \param alphabets the pair of terminal alphabet and substitution alphabet
     * \param rte the initial rte content
     */
    explicit FormalRTE(std::pair<ext::set<common::ranked_symbol<SymbolType>>, ext::set<common::ranked_symbol<SymbolType>>> alphabets, FormalRTEStructure<SymbolType> rte);

    /**
     * \brief Creates a new instance of the expression based on the initial content. Alphabets are deduced from the content.
     *
     * \param rte the initial rte content
     */
    explicit FormalRTE(FormalRTEStructure<SymbolType> rte);

    /**
     * Getter of the terminal alphabet.
     *
     * \returns the terminal alphabet of the expression
     */
    const ext::set<common::ranked_symbol<SymbolType>>& getAlphabet() const&
    {
        return this->template accessComponent<component::GeneralAlphabet>().get();
    }

    /**
     * Getter of the terminal alphabet.
     *
     * \returns the terminal alphabet of the expression
     */
    ext::set<common::ranked_symbol<SymbolType>>&& getAlphabet() &&
    {
        return std::move(this->template accessComponent<component::GeneralAlphabet>().get());
    }

    /**
     * Adder of a terminal symbol.
     *
     * \param symbol the new symbol to be added to a terminal alphabet
     *
     * \returns true if the symbol was indeed added
     */
    void addAlphabetSymbols(common::ranked_symbol<SymbolType> symbol)
    {
        this->template accessComponent<component::GeneralAlphabet>().add(std::move(symbol));
    }

    /**
     * Adder of terminal symbols.
     *
     * \param symbols new symbols to be added to a terminal alphabet
     */
    void addAlphabetSymbols(ext::set<common::ranked_symbol<SymbolType>> symbols)
    {
        this->template accessComponent<component::GeneralAlphabet>().add(std::move(symbols));
    }

    /**
     * Setter of terminal alphabet.
     *
     * \param symbols completely new terminal alphabet
     */
    void setAlphabetSymbols(ext::set<common::ranked_symbol<SymbolType>> symbols)
    {
        this->template accessComponent<component::GeneralAlphabet>().set(std::move(symbols));
    }

    /**
     * Remover of a terminal symbol.
     *
     * \param symbol a symbol to be removed from a terminal alphabet
     *
     * \returns true if the symbol was indeed removed
     */
    void removeAlphabetSymbol(const common::ranked_symbol<SymbolType>& symbol)
    {
        this->template accessComponent<component::GeneralAlphabet>().remove(symbol);
    }

    /**
     * Getter of the substitution alphabet.
     *
     * \returns the substitution alphabet of the expression
     */
    const ext::set<common::ranked_symbol<SymbolType>>& getSubstitutionAlphabet() const&
    {
        return this->template accessComponent<component::ConstantAlphabet>().get();
    }

    /**
     * Getter of the substitution alphabet.
     *
     * \returns the substitution alphabet of the expression
     */
    ext::set<common::ranked_symbol<SymbolType>>&& getSubstitutionAlphabet() &&
    {
        return std::move(this->template accessComponent<component::ConstantAlphabet>().get());
    }

    /**
     * Adder of a substitution symbol.
     *
     * \param symbol the new symbol to be added to a substitution alphabet
     *
     * \returns true if the symbol was indeed added
     */
    void addConstantSymbol(common::ranked_symbol<SymbolType> symbol)
    {
        this->template accessComponent<component::ConstantAlphabet>().add(std::move(symbol));
    }

    /**
     * Adder of substitution symbols.
     *
     * \param symbols new symbols to be added to a substitution alphabet
     */
    void addConstantSymbols(ext::set<common::ranked_symbol<SymbolType>> symbols)
    {
        this->template accessComponent<component::ConstantAlphabet>().add(std::move(symbols));
    }

    /**
     * Setter of substitution alphabet.
     *
     * \param symbols completely new substitution alphabet
     */
    void setConstantSymbols(ext::set<common::ranked_symbol<SymbolType>> symbols)
    {
        this->template accessComponent<component::ConstantAlphabet>().set(std::move(symbols));
    }

    /**
     * Remover of a substitution symbol.
     *
     * \param symbol a symbol to be removed from a substitution alphabet
     *
     * \returns true if the symbol was indeed removed
     */
    void removeConstantSymbol(const common::ranked_symbol<SymbolType>& symbol)
    {
        this->template accessComponent<component::ConstantAlphabet>().remove(symbol);
    }

    /**
     * Get the structure of the expression.
     *
     * \returns the structure of the expression.
     */
    const FormalRTEStructure<SymbolType>& getRTE() const&;

    /**
     * Get the structure of the expression.
     *
     * \returns the structure of the expression.
     */
    FormalRTEStructure<SymbolType>&& getRTE() &&;

    /**
     * Set the structure of the expression.
     *
     * \param regExp the new structure of the expression.
     */
    void setRTE(FormalRTEStructure<SymbolType> param);

    /**
     * The three way comparison implementation
     *
     * \param other the other instance
     *
     * \returns the ordering between this object and the @p other.
     */
    auto operator<=>(const FormalRTE& other) const
    {
        return std::tie(m_rte, getAlphabet(), getSubstitutionAlphabet()) <=> std::tie(m_rte, other.getAlphabet(), getSubstitutionAlphabet());
    }

    /**
     * The equality comparison implementation.
     *
     * \param other the other object to compare with.
     *
     * \returns true if this and other objects are equal, false othervise
     */
    bool operator==(const FormalRTE& other) const
    {
        return std::tie(m_rte, getAlphabet(), getSubstitutionAlphabet()) == std::tie(m_rte, other.getAlphabet(), getSubstitutionAlphabet());
    }

    /**
     * Print this object as raw representation to ostream.
     *
     * \param out ostream where to print
     * \param instance object to print
     *
     * \returns modified output stream
     */
    friend ext::ostream& operator<<(ext::ostream& out, const FormalRTE& instance)
    {
        return out << "(FormalRTE " << instance.getRTE() << ")";
    }
};

template <class SymbolType>
FormalRTE<SymbolType>::FormalRTE(ext::set<common::ranked_symbol<SymbolType>> alphabetF, ext::set<common::ranked_symbol<SymbolType>> alphabetK, FormalRTEStructure<SymbolType> rte)
    : core::Components<FormalRTE, ext::set<common::ranked_symbol<SymbolType>>, module::Set, std::tuple<component::GeneralAlphabet, component::ConstantAlphabet>>(std::move(alphabetF), std::move(alphabetK))
    , m_rte(std::move(rte))
{
    if (!this->m_rte.getStructure().checkAlphabet(getAlphabet(), getSubstitutionAlphabet()))
        throw exception::CommonException("Input symbols not in the alphabet.");
}

template <class SymbolType>
FormalRTE<SymbolType>::FormalRTE(std::pair<ext::set<common::ranked_symbol<SymbolType>>, ext::set<common::ranked_symbol<SymbolType>>> alphabets, FormalRTEStructure<SymbolType> rte)
    : FormalRTE(std::move(alphabets.first), std::move(alphabets.second), std::move(rte))
{
}

template <class SymbolType>
FormalRTE<SymbolType>::FormalRTE()
    : FormalRTE(ext::set<common::ranked_symbol<SymbolType>>(), ext::set<common::ranked_symbol<SymbolType>>(), FormalRTEStructure<SymbolType>())
{
}

template <class SymbolType>
FormalRTE<SymbolType>::FormalRTE(FormalRTEStructure<SymbolType> rte)
    : FormalRTE(rte.getStructure().computeMinimalAlphabets(), rte)
{
}

template <class SymbolType>
const FormalRTEStructure<SymbolType>& FormalRTE<SymbolType>::getRTE() const&
{
    return m_rte;
}

template <class SymbolType>
FormalRTEStructure<SymbolType>&& FormalRTE<SymbolType>::getRTE() &&
{
    return std::move(m_rte);
}

template <class SymbolType>
void FormalRTE<SymbolType>::setRTE(FormalRTEStructure<SymbolType> param)
{
    if (!param.getStructure().checkAlphabet(getAlphabet(), getSubstitutionAlphabet()))
        throw exception::CommonException("Symbols not matching alphabets.");

    this->m_rte = std::move(param);
}

} /* namespace rte */

namespace core {

/**
 * Helper class specifying constraints for the expression's internal terminal alphabet component.
 *
 * \tparam SymbolType used for the symbol part of the ranked symbol
 */
template <class SymbolType>
class SetConstraint<rte::FormalRTE<SymbolType>, common::ranked_symbol<SymbolType>, component::GeneralAlphabet> {
public:
    /**
     * Returns true if the symbol is still used somewhere in the structure of the expression.
     *
     * \param rte the tested expresion
     * \param symbol the tested symbol
     *
     * \returns true if the symbol is used, false othervise
     */
    static bool used(const rte::FormalRTE<SymbolType>& rte, const common::ranked_symbol<SymbolType>& symbol)
    {
        return rte.getRTE().getStructure().testSymbol(symbol);
    }

    /**
     * Returns true as all symbols are possibly available to be elements of the terminal alphabet.
     *
     * \param rte the tested expresion
     * \param symbol the tested state
     *
     * \returns true
     */
    static bool available(const rte::FormalRTE<SymbolType>&, const common::ranked_symbol<SymbolType>&)
    {
        return true;
    }

    /**
     * Throws exception if the symbol is already a part of the substitution alphabet.
     *
     * \param rte the tested expresion
     * \param symbol the tested state
     */
    static void valid(const rte::FormalRTE<SymbolType>& rte, const common::ranked_symbol<SymbolType>& symbol)
    {
        if (rte.template accessComponent<component::ConstantAlphabet>().get().count(symbol))
            throw ::exception::CommonException("Symbol " + ext::to_string(symbol) + " cannot be in general alphabet since it is already in constant alphabet");
    }
};

/**
 * Helper class specifying constraints for the expression's internal substitution alphabet component.
 *
 * \tparam SymbolType used for the symbol part of the ranked symbol
 */
template <class SymbolType>
class SetConstraint<rte::FormalRTE<SymbolType>, common::ranked_symbol<SymbolType>, component::ConstantAlphabet> {
public:
    /**
     * Returns true if the symbol is still used somewhere in the structure of the expression.
     *
     * \param rte the tested expresion
     * \param symbol the tested symbol
     *
     * \returns true if the symbol is used, false othervise
     */
    static bool used(const rte::FormalRTE<SymbolType>& rte, const common::ranked_symbol<SymbolType>& symbol)
    {
        return rte.getRTE().getStructure().testSymbol(symbol);
    }

    /**
     * Returns true as all symbols are possibly available to be elements of the substitution alphabet.
     *
     * \param rte the tested expresion
     * \param symbol the tested state
     *
     * \returns true
     */
    static bool available(const rte::FormalRTE<SymbolType>&, const common::ranked_symbol<SymbolType>&)
    {
        return true;
    }

    /**
     * Throws exception if the symbol is already a part of the terminal alphabet.
     *
     * \param rte the tested expresion
     * \param symbol the tested state
     */
    static void valid(const rte::FormalRTE<SymbolType>& rte, const common::ranked_symbol<SymbolType>& symbol)
    {
        if (rte.template accessComponent<component::GeneralAlphabet>().get().count(symbol))
            throw ::exception::CommonException("Symbol " + ext::to_string(symbol) + " cannot be in constant alphabet since it is already in general alphabet");
    }
};

template <class SymbolType>
struct type_util<rte::FormalRTE<SymbolType>> {
    static rte::FormalRTE<SymbolType> denormalize(rte::FormalRTE<>&& value)
    {
        ext::set<common::ranked_symbol<DefaultSymbolType>> alphabet = alphabet::SymbolDenormalize::denormalizeRankedAlphabet<SymbolType>(std::move(value).getAlphabet());
        ext::set<common::ranked_symbol<DefaultSymbolType>> constants = alphabet::SymbolDenormalize::denormalizeRankedAlphabet<SymbolType>(std::move(value).getSubstitutionAlphabet());
        rte::FormalRTEStructure<DefaultSymbolType> structure(std::move(*rte::RTEDenormalize::denormalize<SymbolType>(std::move(value).getRTE().getStructure())));
        return rte::FormalRTE<SymbolType>(std::move(alphabet), std::move(constants), std::move(structure));
    }

    static rte::FormalRTE<> normalize(rte::FormalRTE<SymbolType>&& value)
    {
        ext::set<common::ranked_symbol<DefaultSymbolType>> alphabet = alphabet::SymbolNormalize::normalizeRankedAlphabet(std::move(value).getAlphabet());
        ext::set<common::ranked_symbol<DefaultSymbolType>> constants = alphabet::SymbolNormalize::normalizeRankedAlphabet(std::move(value).getSubstitutionAlphabet());
        rte::FormalRTEStructure<DefaultSymbolType> structure(std::move(*rte::RTENormalize::normalize(std::move(value).getRTE().getStructure())));
        return rte::FormalRTE<>(std::move(alphabet), std::move(constants), std::move(structure));
    }

    static std::unique_ptr<type_details_base> type(const rte::FormalRTE<SymbolType>& arg)
    {
        core::unique_ptr_set<type_details_base> subTypesSymbol;
        for (const common::ranked_symbol<SymbolType>& item : arg.getAlphabet())
            subTypesSymbol.insert(type_util<SymbolType>::type(item.getSymbol()));

        std::vector<std::unique_ptr<type_details_base>> sub_types_vec;
        sub_types_vec.push_back(type_details_variant_type::make_variant(std::move(subTypesSymbol)));
        return std::make_unique<type_details_template>("rte::FormalRTE", std::move(sub_types_vec));
    }
};

template <class SymbolType>
struct type_details_retriever<rte::FormalRTE<SymbolType>> {
    static std::unique_ptr<type_details_base> get()
    {
        std::vector<std::unique_ptr<type_details_base>> sub_types_vec;
        sub_types_vec.push_back(type_details_retriever<SymbolType>::get());
        return std::make_unique<type_details_template>("rte::FormalRTE", std::move(sub_types_vec));
    }
};

} /* namespace core */

extern template class rte::FormalRTE<>;
extern template class abstraction::ValueHolder<rte::FormalRTE<>>;
extern template const rte::FormalRTE<>& abstraction::retrieveValue<const rte::FormalRTE<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
extern template class registration::DenormalizationRegisterImpl<const rte::FormalRTE<>&>;
extern template class registration::NormalizationRegisterImpl<rte::FormalRTE<>>;
