#pragma once

#include <rte/formal/FormalRTEElements.h>
#include <rte/formal/FormalRTEStructure.h>

#include <alphabet/common/SymbolDenormalize.h>

namespace rte {

/**
 * Determines whether regular expression (or its subtree) describes an empty language (rte == \0)
 *
 */
class RTEDenormalizeVariant {
public:
    /**
     * Determines whether regular expression is describes an empty language (rte == \0)
     *
     * \tparam SymbolType the type of symbol in the tested regular expression
     *
     * \param rte the rte to test
     *
     * \return true of the language described by the regular expression is empty
     */
    template <class... Types>
    static ext::smart_ptr<FormalRTEElement<ext::variant<Types...>>> denormalize(rte::FormalRTEElement<ext::variant<ext::second_type<Types, object::Object>...>>&& rte);

    template <class... Types>
    class Formal {
    public:
        static ext::smart_ptr<FormalRTEElement<ext::variant<Types...>>> visit(rte::FormalRTEAlternation<ext::variant<ext::second_type<Types, object::Object>...>>&& alternation);
        static ext::smart_ptr<FormalRTEElement<ext::variant<Types...>>> visit(rte::FormalRTESubstitution<ext::variant<ext::second_type<Types, object::Object>...>>&& substitution);
        static ext::smart_ptr<FormalRTEElement<ext::variant<Types...>>> visit(rte::FormalRTEIteration<ext::variant<ext::second_type<Types, object::Object>...>>&& iteration);
        static ext::smart_ptr<FormalRTEElement<ext::variant<Types...>>> visit(rte::FormalRTESymbolAlphabet<ext::variant<ext::second_type<Types, object::Object>...>>&& symbol);
        static ext::smart_ptr<FormalRTEElement<ext::variant<Types...>>> visit(rte::FormalRTEEmpty<ext::variant<ext::second_type<Types, object::Object>...>>&& empty);
        static ext::smart_ptr<FormalRTEElement<ext::variant<Types...>>> visit(rte::FormalRTESymbolSubst<ext::variant<ext::second_type<Types, object::Object>...>>&& symbol);
    };
};

// ----------------------------------------------------------------------------

template <class... Types>
ext::smart_ptr<FormalRTEElement<ext::variant<Types...>>> RTEDenormalizeVariant::denormalize(rte::FormalRTEElement<ext::variant<ext::second_type<Types, object::Object>...>>&& rte)
{
    return std::move(rte).template accept<ext::smart_ptr<FormalRTEElement<ext::variant<Types...>>>, RTEDenormalizeVariant::Formal<Types...>>();
}

// ----------------------------------------------------------------------------

template <class... Types>
ext::smart_ptr<FormalRTEElement<ext::variant<Types...>>> RTEDenormalizeVariant::Formal<Types...>::visit(rte::FormalRTEAlternation<ext::variant<ext::second_type<Types, object::Object>...>>&& alternation)
{
    return ext::smart_ptr<FormalRTEElement<ext::variant<Types...>>>(
        new FormalRTEAlternation<ext::variant<Types...>>(
            std::move(*std::move(std::move(alternation).getLeftElement()).template accept<ext::smart_ptr<FormalRTEElement<ext::variant<Types...>>>, RTEDenormalizeVariant::Formal<Types...>>()),
            std::move(*std::move(std::move(alternation).getRightElement()).template accept<ext::smart_ptr<FormalRTEElement<ext::variant<Types...>>>, RTEDenormalizeVariant::Formal<Types...>>())));
}

template <class... Types>
ext::smart_ptr<FormalRTEElement<ext::variant<Types...>>> RTEDenormalizeVariant::Formal<Types...>::visit(rte::FormalRTESubstitution<ext::variant<ext::second_type<Types, object::Object>...>>&& substitution)
{
    FormalRTESymbolSubst<ext::variant<Types...>> subst(alphabet::SymbolDenormalize::denormalizeRankedVariantSymbol<Types...>(std::move(substitution).getSubstitutionSymbol().getSymbol()));
    return ext::smart_ptr<FormalRTEElement<ext::variant<Types...>>>(
        new FormalRTESubstitution<ext::variant<Types...>>(
            std::move(*std::move(std::move(substitution).getLeftElement()).template accept<ext::smart_ptr<FormalRTEElement<ext::variant<Types...>>>, RTEDenormalizeVariant::Formal<Types...>>()),
            std::move(*std::move(std::move(substitution).getRightElement()).template accept<ext::smart_ptr<FormalRTEElement<ext::variant<Types...>>>, RTEDenormalizeVariant::Formal<Types...>>()),
            std::move(subst)));
}

template <class... Types>
ext::smart_ptr<FormalRTEElement<ext::variant<Types...>>> RTEDenormalizeVariant::Formal<Types...>::visit(rte::FormalRTEIteration<ext::variant<ext::second_type<Types, object::Object>...>>&& iteration)
{
    FormalRTESymbolSubst<ext::variant<Types...>> subst(alphabet::SymbolDenormalize::denormalizeRankedVariantSymbol<Types...>(std::move(iteration).getSubstitutionSymbol().getSymbol()));
    return ext::smart_ptr<FormalRTEElement<ext::variant<Types...>>>(
        new FormalRTEIteration<ext::variant<Types...>>(
            std::move(*std::move(std::move(iteration).getElement()).template accept<ext::smart_ptr<FormalRTEElement<ext::variant<Types...>>>, RTEDenormalizeVariant::Formal<Types...>>()),
            std::move(subst)));
}

template <class... Types>
ext::smart_ptr<FormalRTEElement<ext::variant<Types...>>> RTEDenormalizeVariant::Formal<Types...>::visit(rte::FormalRTESymbolAlphabet<ext::variant<ext::second_type<Types, object::Object>...>>&& symbol)
{
    ext::ptr_vector<FormalRTEElement<ext::variant<Types...>>> children;
    for (FormalRTEElement<ext::variant<ext::second_type<Types, object::Object>...>>&& element : ext::make_mover(std::move(symbol).getChildren()))
        children.push_back(std::move(*std::move(element).template accept<ext::smart_ptr<FormalRTEElement<ext::variant<Types...>>>, RTEDenormalizeVariant::Formal<Types...>>()));

    return ext::smart_ptr<FormalRTEElement<ext::variant<Types...>>>(
        new FormalRTESymbolAlphabet<ext::variant<Types...>>(
            alphabet::SymbolDenormalize::denormalizeRankedVariantSymbol<Types...>(std::move(symbol).getSymbol()),
            std::move(children)));
}

template <class... Types>
ext::smart_ptr<FormalRTEElement<ext::variant<Types...>>> RTEDenormalizeVariant::Formal<Types...>::visit(rte::FormalRTEEmpty<ext::variant<ext::second_type<Types, object::Object>...>>&&)
{
    return ext::smart_ptr<FormalRTEElement<ext::variant<Types...>>>(new FormalRTEEmpty<ext::variant<Types...>>());
}

template <class... Types>
ext::smart_ptr<FormalRTEElement<ext::variant<Types...>>> RTEDenormalizeVariant::Formal<Types...>::visit(rte::FormalRTESymbolSubst<ext::variant<ext::second_type<Types, object::Object>...>>&& symbol)
{
    FormalRTESymbolSubst<ext::variant<Types...>> res(alphabet::SymbolDenormalize::denormalizeRankedVariantSymbol<Types...>(std::move(symbol).getSymbol()));
    return ext::smart_ptr<FormalRTEElement<ext::variant<Types...>>>(std::move(res).clone());
}

} /* namespace rte */
