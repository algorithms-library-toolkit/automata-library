#pragma once

#include <rte/formal/FormalRTEElements.h>
#include <rte/formal/FormalRTEStructure.h>

#include <alphabet/common/SymbolNormalize.h>

namespace rte {

/**
 * Determines whether regular expression (or its subtree) describes an empty language (rte == \0)
 *
 */
class RTENormalize {
public:
    /**
     * Determines whether regular expression is describes an empty language (rte == \0)
     *
     * \tparam DefaultSymbolType the type of symbol in the tested regular expression
     *
     * \param rte the rte to test
     *
     * \return true of the language described by the regular expression is empty
     */
    template <class SymbolType>
    static ext::smart_ptr<FormalRTEElement<DefaultSymbolType>> normalize(rte::FormalRTEElement<SymbolType>&& rte);

    template <class SymbolType>
    class Formal {
    public:
        static ext::smart_ptr<FormalRTEElement<DefaultSymbolType>> visit(rte::FormalRTEAlternation<SymbolType>&& alternation);
        static ext::smart_ptr<FormalRTEElement<DefaultSymbolType>> visit(rte::FormalRTESubstitution<SymbolType>&& substitution);
        static ext::smart_ptr<FormalRTEElement<DefaultSymbolType>> visit(rte::FormalRTEIteration<SymbolType>&& iteration);
        static ext::smart_ptr<FormalRTEElement<DefaultSymbolType>> visit(rte::FormalRTESymbolAlphabet<SymbolType>&& symbol);
        static ext::smart_ptr<FormalRTEElement<DefaultSymbolType>> visit(rte::FormalRTEEmpty<SymbolType>&& empty);
        static ext::smart_ptr<FormalRTEElement<DefaultSymbolType>> visit(rte::FormalRTESymbolSubst<SymbolType>&& symbol);
    };
};

// ----------------------------------------------------------------------------

template <class SymbolType>
ext::smart_ptr<FormalRTEElement<DefaultSymbolType>> RTENormalize::normalize(rte::FormalRTEElement<SymbolType>&& rte)
{
    return std::move(rte).template accept<ext::smart_ptr<FormalRTEElement<DefaultSymbolType>>, RTENormalize::Formal<SymbolType>>();
}

// ----------------------------------------------------------------------------

template <class SymbolType>
ext::smart_ptr<FormalRTEElement<DefaultSymbolType>> RTENormalize::Formal<SymbolType>::visit(rte::FormalRTEAlternation<SymbolType>&& alternation)
{
    return ext::smart_ptr<FormalRTEElement<DefaultSymbolType>>(
        new FormalRTEAlternation<DefaultSymbolType>(
            std::move(*std::move(std::move(alternation).getLeftElement()).template accept<ext::smart_ptr<FormalRTEElement<DefaultSymbolType>>, RTENormalize::Formal<SymbolType>>()),
            std::move(*std::move(std::move(alternation).getRightElement()).template accept<ext::smart_ptr<FormalRTEElement<DefaultSymbolType>>, RTENormalize::Formal<SymbolType>>())));
}

template <class SymbolType>
ext::smart_ptr<FormalRTEElement<DefaultSymbolType>> RTENormalize::Formal<SymbolType>::visit(rte::FormalRTESubstitution<SymbolType>&& substitution)
{
    FormalRTESymbolSubst<DefaultSymbolType> subst(alphabet::SymbolNormalize::normalizeRankedSymbol(std::move(substitution).getSubstitutionSymbol().getSymbol()));
    return ext::smart_ptr<FormalRTEElement<DefaultSymbolType>>(
        new FormalRTESubstitution<DefaultSymbolType>(
            std::move(*std::move(std::move(substitution).getLeftElement()).template accept<ext::smart_ptr<FormalRTEElement<DefaultSymbolType>>, RTENormalize::Formal<SymbolType>>()),
            std::move(*std::move(std::move(substitution).getRightElement()).template accept<ext::smart_ptr<FormalRTEElement<DefaultSymbolType>>, RTENormalize::Formal<SymbolType>>()),
            std::move(subst)));
}

template <class SymbolType>
ext::smart_ptr<FormalRTEElement<DefaultSymbolType>> RTENormalize::Formal<SymbolType>::visit(rte::FormalRTEIteration<SymbolType>&& iteration)
{
    FormalRTESymbolSubst<DefaultSymbolType> subst(alphabet::SymbolNormalize::normalizeRankedSymbol(std::move(iteration).getSubstitutionSymbol().getSymbol()));
    return ext::smart_ptr<FormalRTEElement<DefaultSymbolType>>(
        new FormalRTEIteration<DefaultSymbolType>(
            std::move(*std::move(std::move(iteration).getElement()).template accept<ext::smart_ptr<FormalRTEElement<DefaultSymbolType>>, RTENormalize::Formal<SymbolType>>()),
            std::move(subst)));
}

template <class SymbolType>
ext::smart_ptr<FormalRTEElement<DefaultSymbolType>> RTENormalize::Formal<SymbolType>::visit(rte::FormalRTESymbolAlphabet<SymbolType>&& symbol)
{
    ext::ptr_vector<FormalRTEElement<DefaultSymbolType>> children;
    for (FormalRTEElement<SymbolType>&& element : ext::make_mover(std::move(symbol).getChildren()))
        children.push_back(std::move(*std::move(element).template accept<ext::smart_ptr<FormalRTEElement<DefaultSymbolType>>, RTENormalize::Formal<SymbolType>>()));

    return ext::smart_ptr<FormalRTEElement<DefaultSymbolType>>(
        new FormalRTESymbolAlphabet<DefaultSymbolType>(
            alphabet::SymbolNormalize::normalizeRankedSymbol(std::move(symbol).getSymbol()),
            std::move(children)));
}

template <class SymbolType>
ext::smart_ptr<FormalRTEElement<DefaultSymbolType>> RTENormalize::Formal<SymbolType>::visit(rte::FormalRTEEmpty<SymbolType>&&)
{
    return ext::smart_ptr<FormalRTEElement<DefaultSymbolType>>(new FormalRTEEmpty<DefaultSymbolType>());
}

template <class SymbolType>
ext::smart_ptr<FormalRTEElement<DefaultSymbolType>> RTENormalize::Formal<SymbolType>::visit(rte::FormalRTESymbolSubst<SymbolType>&& symbol)
{
    FormalRTESymbolSubst<DefaultSymbolType> res(alphabet::SymbolNormalize::normalizeRankedSymbol(std::move(symbol).getSymbol()));
    return ext::smart_ptr<FormalRTEElement<DefaultSymbolType>>(std::move(res).clone());
}

} /* namespace rte */
