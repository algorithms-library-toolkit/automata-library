#include "CompressedBitParallelIndex.h"

#include <registration/ValuePrinterRegistration.hpp>

namespace {

auto valuePrinter = registration::ValuePrinterRegister<indexes::stringology::CompressedBitParallelIndex<>>();

} /* namespace */
