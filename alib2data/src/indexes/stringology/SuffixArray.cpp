#include "SuffixArray.h"

#include <registration/ValuePrinterRegistration.hpp>

namespace {

auto valuePrinter = registration::ValuePrinterRegister<indexes::stringology::SuffixArray<>>();

} /* namespace */
