#include "BitParallelIndex.h"

#include <registration/ValuePrinterRegistration.hpp>

namespace {

auto valuePrinter = registration::ValuePrinterRegister<indexes::stringology::BitParallelIndex<>>();

} /* namespace */
