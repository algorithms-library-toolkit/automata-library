/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <ext/algorithm>
#include <ext/iostream>

#include <alib/optional>
#include <alib/set>
#include <alib/string>
#include <alib/trie>

#include <common/DefaultSymbolType.h>

#include <core/modules.hpp>
#include <exception/CommonException.h>

#include <core/type_details_base.hpp>
// #include <alphabet/common/SymbolNormalize.h>
// #include <indexes/common/IndexesNormalize.h>

namespace indexes {

namespace stringology {

/**
 * \brief Suffix trie string index. Tree like representation of all suffixes. Nodes of the trie are optionally containing index of the suffix. The parent child relationship of nodes is represented by single symbol. The class does not checks whether the trie actually is suffix trie.
 *
 * \tparam SymbolType type of symbols of indexed string
 */
template <class SymbolType = DefaultSymbolType>
class SuffixTrie final : public core::Components<SuffixTrie<SymbolType>, ext::set<SymbolType>, module::Set, component::GeneralAlphabet> {
    /**
     * Representation of the suffix trie.
     */
    ext::trie<SymbolType, std::optional<unsigned>> m_trie;

    /**
     * \brief Checks edges of the raw suffix trie, whether they are over the specified alphabet.
     *
     * \throws exception::CommonException if there is a symbol on trie edges not present in the alphabet.
     */
    void checkTrie(const ext::trie<SymbolType, std::optional<unsigned>>& trie);

public:
    /**
     * Creates a new instance of the index over concrete alphabet and content initialized to empty root.
     *
     * \param edgeAlphabet the alphabet of symbols on node to node edges.
     */
    explicit SuffixTrie(ext::set<SymbolType> edgeAlphabet);

    /**
     * Creates a new instance of the index over concrete alphabet and raw suffix trie.
     *
     * \param edgeAlphabet the alphabet of symbols on node to node edges.
     * \param trie the representation of suffix trie.
     */
    explicit SuffixTrie(ext::set<SymbolType> edgeAlphabet, ext::trie<SymbolType, std::optional<unsigned>> trie);

    /**
     * Creates a new instance of the index based on raw suffix trie.
     *
     * \param trie the representation of suffix trie.
     */
    explicit SuffixTrie(ext::trie<SymbolType, std::optional<unsigned>> trie);

    /**
     * Getter of the raw suffix trie.
     *
     * \return root node of the trie
     */
    const ext::trie<SymbolType, std::optional<unsigned>>& getRoot() const&;

    /**
     * Getter of the raw suffix trie.
     *
     * \return root node of the trie
     */
    ext::trie<SymbolType, std::optional<unsigned>>&& getRoot() &&;

    /**
     * Getter of the alphabet of the indexed string.
     *
     * \returns the alphabet of the indexed string
     */
    const ext::set<SymbolType>& getAlphabet() const&
    {
        return this->template accessComponent<component::GeneralAlphabet>().get();
    }

    /**
     * Getter of the alphabet of the indexed string.
     *
     * \returns the alphabet of the indexed string
     */
    ext::set<SymbolType>&& getAlphabet() &&
    {
        return std::move(this->template accessComponent<component::GeneralAlphabet>().get());
    }

    /**
     * Sets the root node of the suffix trie
     *
     * \param tree root node to set
     */
    void setTree(ext::trie<SymbolType, std::optional<unsigned>> trie);

    /**
     * Remover of a symbol from the alphabet.
     *
     * \param symbol a symbol to remove.
     */
    bool removeSymbolFromEdgeAlphabet(const SymbolType& symbol)
    {
        return this->template accessComponent<component::GeneralAlphabet>().remove(symbol);
    }

    /**
     * The three way comparison implementation
     *
     * \param other the other instance
     *
     * \returns the ordering between this object and the @p other.
     */
    auto operator<=>(const SuffixTrie& other) const
    {
        return std::tie(getRoot(), getAlphabet()) <=> std::tie(other.getRoot(), other.getAlphabet());
    }

    /**
     * The equality comparison implementation.
     *
     * \param other the other object to compare with.
     *
     * \returns true if this and other objects are equal, false othervise
     */
    bool operator==(const SuffixTrie& other) const
    {
        return std::tie(getRoot(), getAlphabet()) == std::tie(other.getRoot(), other.getAlphabet());
    }

    /**
     * Print this object as raw representation to ostream.
     *
     * \param out ostream where to print
     * \param instance object to print
     *
     * \returns modified output stream
     */
    friend ext::ostream& operator<<(ext::ostream& out, const SuffixTrie& instance)
    {
        return out << "(SuffixTrie " << instance.m_trie << ")";
    }
};

} /* namespace stringology */

} /* namespace indexes */

namespace indexes {

namespace stringology {

template <class SymbolType>
SuffixTrie<SymbolType>::SuffixTrie(ext::set<SymbolType> edgeAlphabet)
    : SuffixTrie(std::move(edgeAlphabet), ext::trie<SymbolType, std::optional<unsigned>>(std::optional<unsigned>()))
{
}

template <class SymbolType>
SuffixTrie<SymbolType>::SuffixTrie(ext::set<SymbolType> edgeAlphabet, ext::trie<SymbolType, std::optional<unsigned>> trie)
    : core::Components<SuffixTrie, ext::set<SymbolType>, module::Set, component::GeneralAlphabet>(std::move(edgeAlphabet))
    , m_trie(std::move(trie))
{
    checkTrie(this->m_trie);
}

template <class SymbolType>
SuffixTrie<SymbolType>::SuffixTrie(ext::trie<SymbolType, std::optional<unsigned>> trie)
    : SuffixTrie(computeMinimalEdgeAlphabet(trie), trie)
{
}

template <class SymbolType>
void SuffixTrie<SymbolType>::checkTrie(const ext::trie<SymbolType, std::optional<unsigned>>& trie)
{
    for (const std::pair<const SymbolType, ext::trie<SymbolType, std::optional<unsigned>>>& child : trie.getChildren()) {
        if (!getAlphabet().count(child.first))
            throw exception::CommonException("Symbol " + ext::to_string(child.first) + "not in the alphabet.");
        checkTrie(child.second);
    }
}

template <class SymbolType>
const ext::trie<SymbolType, std::optional<unsigned>>& SuffixTrie<SymbolType>::getRoot() const&
{
    return m_trie;
}

template <class SymbolType>
ext::trie<SymbolType, std::optional<unsigned>>&& SuffixTrie<SymbolType>::getRoot() &&
{
    return std::move(m_trie);
}

template <class SymbolType>
void SuffixTrie<SymbolType>::setTree(ext::trie<SymbolType, std::optional<unsigned>> trie)
{
    checkTrie(trie);
    this->m_trie = std::move(trie).clone();
}

} /* namespace stringology */

} /* namespace indexes */

namespace core {

/**
 * Helper class specifying constraints for the internal alphabet component of the index.
 *
 * \tparam SymbolType type of symbols of indexed string
 */
template <class SymbolType>
class SetConstraint<indexes::stringology::SuffixTrie<SymbolType>, SymbolType, component::GeneralAlphabet> {

    /**
     * Recursive implementation of the used constraint checker. Returns true if the symbol is still used on edges of the suffix trie bellow the node specified by parameter.
     *
     * \param trie the tested subtrie
     * \param symbol the tested symbol
     *
     * \returns true if the symbol is used, false othervise
     */
    static bool used(const ext::trie<SymbolType, std::optional<unsigned>>& trie, const SymbolType& symbol)
    {
        for (const std::pair<const SymbolType, ext::trie<SymbolType, std::optional<unsigned>>>& child : trie.getChildren()) {
            if (symbol == child.first || checkTrie(trie, child.second))
                return true;
        }
        return false;
    }

public:
    /**
     * Returns true if the symbol is still used on edges of the suffix trie.
     *
     * \param index the tested index
     * \param symbol the tested symbol
     *
     * \returns true if the symbol is used, false othervise
     */
    static bool used(const indexes::stringology::SuffixTrie<SymbolType>& index, const SymbolType& symbol)
    {
        return used(index.getRoot(), symbol);
    }

    /**
     * Returns true as all symbols are possibly available to be elements of the alphabet.
     *
     * \param index the tested index
     * \param symbol the tested symbol
     *
     * \returns true
     */
    static bool available(const indexes::stringology::SuffixTrie<SymbolType>&, const SymbolType&)
    {
        return true;
    }

    /**
     * All symbols are valid as symbols of the alphabet.
     *
     * \param index the tested index
     * \param symbol the tested symbol
     */
    static void valid(const indexes::stringology::SuffixTrie<SymbolType>&, const SymbolType&)
    {
    }
};

/**
 * Helper for normalisation of types specified by templates used as internal datatypes of symbols.
 *
 * \returns new instance of the index with default template parameters or unmodified instance if the template parameters were already the default ones
 */
/*template < class SymbolType >
struct normalize < indexes::stringology::SuffixTrie < SymbolType > > {
        static indexes::stringology::SuffixTrie < > eval ( indexes::stringology::SuffixTrie < SymbolType > && value ) {
                ext::set < DefaultSymbolType > alphabet = alphabet::SymbolNormalize::normalizeAlphabet ( std::move ( value ).getAlpahet ( ) );
                ext::trie < DefaultSymbolType, std::optional < unsigned > > trie = indexes::IndexesNormalize::normalizeTrie ( std::move ( value ).getRoot ( ) );

                return indexes::stringology::SuffixTrie < > ( std::move ( alphabet ), std::move ( trie ) );
        }
};*/

template <class SymbolType>
struct type_details_retriever<indexes::stringology::SuffixTrie<SymbolType>> {
    static std::unique_ptr<type_details_base> get()
    {
        std::vector<std::unique_ptr<type_details_base>> sub_types_vec;
        sub_types_vec.push_back(type_details_retriever<SymbolType>::get());
        return std::make_unique<type_details_template>("indexes::stringology::SuffixTrie", std::move(sub_types_vec));
    }
};

} /* namespace core */
