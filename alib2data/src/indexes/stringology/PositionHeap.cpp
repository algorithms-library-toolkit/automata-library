#include "PositionHeap.h"

#include <registration/ValuePrinterRegistration.hpp>

namespace {

auto valuePrinter = registration::ValuePrinterRegister<indexes::stringology::PositionHeap<>>();

} /* namespace */
