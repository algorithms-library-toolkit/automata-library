#include "FactorOracleAutomaton.h"

#include <registration/CastRegistration.hpp>
#include <registration/ValuePrinterRegistration.hpp>

namespace {

auto DFAFromOracleAutomaton = registration::CastRegister<automaton::DFA<DefaultSymbolType, unsigned>, indexes::stringology::FactorOracleAutomaton<>>();

auto valuePrinter = registration::ValuePrinterRegister<indexes::stringology::FactorOracleAutomaton<>>();

} /* namespace */
