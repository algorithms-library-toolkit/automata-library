#include "FullAndLinearIndex.h"

#include <registration/ValuePrinterRegistration.hpp>

namespace {

auto valuePrinter = registration::ValuePrinterRegister<indexes::arbology::FullAndLinearIndex<>>();

} /* namespace */
