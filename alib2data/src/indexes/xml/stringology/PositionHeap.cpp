#include "PositionHeap.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<indexes::stringology::PositionHeap<>>();
auto xmlRead = registration::XmlReaderRegister<indexes::stringology::PositionHeap<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, indexes::stringology::PositionHeap<>>();

} /* namespace */
