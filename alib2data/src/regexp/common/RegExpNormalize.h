#pragma once

#include <regexp/formal/FormalRegExpElements.h>
#include <regexp/formal/FormalRegExpStructure.h>

#include <regexp/unbounded/UnboundedRegExpElements.h>
#include <regexp/unbounded/UnboundedRegExpStructure.h>

#include <alphabet/common/SymbolNormalize.h>

namespace regexp {

/**
 * Determines whether regular expression (or its subtree) describes an empty language (regexp == \0)
 *
 */
class RegExpNormalize {
public:
    /**
     * Determines whether regular expression is describes an empty language (regexp == \0)
     *
     * \tparam DefaultSymbolType the type of symbol in the tested regular expression
     *
     * \param regexp the regexp to test
     *
     * \return true of the language described by the regular expression is empty
     */
    template <class SymbolType>
    static ext::smart_ptr<FormalRegExpElement<DefaultSymbolType>> normalize(regexp::FormalRegExpElement<SymbolType>&& regexp);

    /**
     * \override
     */
    template <class SymbolType>
    static ext::smart_ptr<UnboundedRegExpElement<DefaultSymbolType>> normalize(regexp::UnboundedRegExpElement<SymbolType>&& regexp);

    template <class SymbolType>
    class Unbounded {
    public:
        static ext::smart_ptr<UnboundedRegExpElement<DefaultSymbolType>> visit(regexp::UnboundedRegExpAlternation<SymbolType>&& alternation);
        static ext::smart_ptr<UnboundedRegExpElement<DefaultSymbolType>> visit(regexp::UnboundedRegExpConcatenation<SymbolType>&& concatenation);
        static ext::smart_ptr<UnboundedRegExpElement<DefaultSymbolType>> visit(regexp::UnboundedRegExpIteration<SymbolType>&& iteration);
        static ext::smart_ptr<UnboundedRegExpElement<DefaultSymbolType>> visit(regexp::UnboundedRegExpSymbol<SymbolType>&& symbol);
        static ext::smart_ptr<UnboundedRegExpElement<DefaultSymbolType>> visit(regexp::UnboundedRegExpEmpty<SymbolType>&& empty);
        static ext::smart_ptr<UnboundedRegExpElement<DefaultSymbolType>> visit(regexp::UnboundedRegExpEpsilon<SymbolType>&& epsilon);
    };

    template <class SymbolType>
    class Formal {
    public:
        static ext::smart_ptr<FormalRegExpElement<DefaultSymbolType>> visit(regexp::FormalRegExpAlternation<SymbolType>&& alternation);
        static ext::smart_ptr<FormalRegExpElement<DefaultSymbolType>> visit(regexp::FormalRegExpConcatenation<SymbolType>&& concatenation);
        static ext::smart_ptr<FormalRegExpElement<DefaultSymbolType>> visit(regexp::FormalRegExpIteration<SymbolType>&& iteration);
        static ext::smart_ptr<FormalRegExpElement<DefaultSymbolType>> visit(regexp::FormalRegExpSymbol<SymbolType>&& symbol);
        static ext::smart_ptr<FormalRegExpElement<DefaultSymbolType>> visit(regexp::FormalRegExpEmpty<SymbolType>&& empty);
        static ext::smart_ptr<FormalRegExpElement<DefaultSymbolType>> visit(regexp::FormalRegExpEpsilon<SymbolType>&& epsilon);
    };
};

// ----------------------------------------------------------------------------

template <class SymbolType>
ext::smart_ptr<FormalRegExpElement<DefaultSymbolType>> RegExpNormalize::normalize(regexp::FormalRegExpElement<SymbolType>&& regexp)
{
    return std::move(regexp).template accept<ext::smart_ptr<FormalRegExpElement<DefaultSymbolType>>, RegExpNormalize::Formal<SymbolType>>();
}

// ----------------------------------------------------------------------------

template <class SymbolType>
ext::smart_ptr<UnboundedRegExpElement<DefaultSymbolType>> RegExpNormalize::normalize(regexp::UnboundedRegExpElement<SymbolType>&& regexp)
{
    return std::move(regexp).template accept<ext::smart_ptr<UnboundedRegExpElement<DefaultSymbolType>>, RegExpNormalize::Unbounded<SymbolType>>();
}

// ----------------------------------------------------------------------------

template <class SymbolType>
ext::smart_ptr<UnboundedRegExpElement<DefaultSymbolType>> RegExpNormalize::Unbounded<SymbolType>::visit(regexp::UnboundedRegExpAlternation<SymbolType>&& alternation)
{
    UnboundedRegExpAlternation<DefaultSymbolType>* res = new UnboundedRegExpAlternation<DefaultSymbolType>();

    for (UnboundedRegExpElement<SymbolType>&& element : ext::make_mover(std::move(alternation).getChildren()))
        res->appendElement(std::move(*std::move(element).template accept<ext::smart_ptr<UnboundedRegExpElement<DefaultSymbolType>>, RegExpNormalize::Unbounded<SymbolType>>()));

    return ext::smart_ptr<UnboundedRegExpElement<DefaultSymbolType>>(res);
}

template <class SymbolType>
ext::smart_ptr<UnboundedRegExpElement<DefaultSymbolType>> RegExpNormalize::Unbounded<SymbolType>::visit(regexp::UnboundedRegExpConcatenation<SymbolType>&& concatenation)
{
    UnboundedRegExpConcatenation<DefaultSymbolType>* res = new UnboundedRegExpConcatenation<DefaultSymbolType>();

    for (UnboundedRegExpElement<SymbolType>&& element : ext::make_mover(std::move(concatenation).getChildren()))
        res->appendElement(std::move(*std::move(element).template accept<ext::smart_ptr<UnboundedRegExpElement<DefaultSymbolType>>, RegExpNormalize::Unbounded<SymbolType>>()));

    return ext::smart_ptr<UnboundedRegExpElement<DefaultSymbolType>>(res);
}

template <class SymbolType>
ext::smart_ptr<UnboundedRegExpElement<DefaultSymbolType>> RegExpNormalize::Unbounded<SymbolType>::visit(regexp::UnboundedRegExpIteration<SymbolType>&& iteration)
{
    return ext::smart_ptr<UnboundedRegExpElement<DefaultSymbolType>>(
        new UnboundedRegExpIteration<DefaultSymbolType>(
            std::move(*std::move(std::move(iteration).getElement()).template accept<ext::smart_ptr<UnboundedRegExpElement<DefaultSymbolType>>, RegExpNormalize::Unbounded<SymbolType>>())));
}

template <class SymbolType>
ext::smart_ptr<UnboundedRegExpElement<DefaultSymbolType>> RegExpNormalize::Unbounded<SymbolType>::visit(regexp::UnboundedRegExpSymbol<SymbolType>&& symbol)
{
    return ext::smart_ptr<UnboundedRegExpElement<DefaultSymbolType>>(new UnboundedRegExpSymbol<DefaultSymbolType>(alphabet::SymbolNormalize::normalizeSymbol(std::move(symbol).getSymbol())));
}

template <class SymbolType>
ext::smart_ptr<UnboundedRegExpElement<DefaultSymbolType>> RegExpNormalize::Unbounded<SymbolType>::visit(regexp::UnboundedRegExpEmpty<SymbolType>&&)
{
    return ext::smart_ptr<UnboundedRegExpElement<DefaultSymbolType>>(new UnboundedRegExpEmpty<DefaultSymbolType>());
}

template <class SymbolType>
ext::smart_ptr<UnboundedRegExpElement<DefaultSymbolType>> RegExpNormalize::Unbounded<SymbolType>::visit(regexp::UnboundedRegExpEpsilon<SymbolType>&&)
{
    return ext::smart_ptr<UnboundedRegExpElement<DefaultSymbolType>>(new UnboundedRegExpEpsilon<DefaultSymbolType>());
}

// ----------------------------------------------------------------------------

template <class SymbolType>
ext::smart_ptr<FormalRegExpElement<DefaultSymbolType>> RegExpNormalize::Formal<SymbolType>::visit(regexp::FormalRegExpAlternation<SymbolType>&& alternation)
{
    return ext::smart_ptr<FormalRegExpElement<DefaultSymbolType>>(
        new FormalRegExpAlternation<DefaultSymbolType>(
            std::move(*std::move(std::move(alternation).getLeftElement()).template accept<ext::smart_ptr<FormalRegExpElement<SymbolType>>, RegExpNormalize::Formal<DefaultSymbolType>>()),
            std::move(*std::move(std::move(alternation).getRightElement()).template accept<ext::smart_ptr<FormalRegExpElement<SymbolType>>, RegExpNormalize::Formal<DefaultSymbolType>>())));
}

template <class SymbolType>
ext::smart_ptr<FormalRegExpElement<DefaultSymbolType>> RegExpNormalize::Formal<SymbolType>::visit(regexp::FormalRegExpConcatenation<SymbolType>&& concatenation)
{
    return ext::smart_ptr<FormalRegExpElement<DefaultSymbolType>>(
        new FormalRegExpConcatenation<DefaultSymbolType>(
            std::move(*std::move(std::move(concatenation).getLeftElement()).template accept<ext::smart_ptr<FormalRegExpElement<SymbolType>>, RegExpNormalize::Formal<DefaultSymbolType>>()),
            std::move(*std::move(std::move(concatenation).getRightElement()).template accept<ext::smart_ptr<FormalRegExpElement<SymbolType>>, RegExpNormalize::Formal<DefaultSymbolType>>())));
}

template <class SymbolType>
ext::smart_ptr<FormalRegExpElement<DefaultSymbolType>> RegExpNormalize::Formal<SymbolType>::visit(regexp::FormalRegExpIteration<SymbolType>&& iteration)
{
    return ext::smart_ptr<FormalRegExpElement<DefaultSymbolType>>(
        new FormalRegExpIteration<DefaultSymbolType>(
            std::move(*std::move(std::move(iteration).getElement()).template accept<ext::smart_ptr<FormalRegExpElement<SymbolType>>, RegExpNormalize::Formal<DefaultSymbolType>>())));
}

template <class SymbolType>
ext::smart_ptr<FormalRegExpElement<DefaultSymbolType>> RegExpNormalize::Formal<SymbolType>::visit(regexp::FormalRegExpSymbol<SymbolType>&& symbol)
{
    return ext::smart_ptr<FormalRegExpElement<DefaultSymbolType>>(new FormalRegExpSymbol<DefaultSymbolType>(alphabet::SymbolNormalize::normalizeSymbol(std::move(symbol).getSymbol())));
}

template <class SymbolType>
ext::smart_ptr<FormalRegExpElement<DefaultSymbolType>> RegExpNormalize::Formal<SymbolType>::visit(regexp::FormalRegExpEmpty<SymbolType>&&)
{
    return ext::smart_ptr<FormalRegExpElement<DefaultSymbolType>>(new FormalRegExpEmpty<DefaultSymbolType>());
}

template <class SymbolType>
ext::smart_ptr<FormalRegExpElement<DefaultSymbolType>> RegExpNormalize::Formal<SymbolType>::visit(regexp::FormalRegExpEpsilon<SymbolType>&&)
{
    return ext::smart_ptr<FormalRegExpElement<DefaultSymbolType>>(new FormalRegExpEpsilon<DefaultSymbolType>());
}

} /* namespace regexp */
