#pragma once

#include <regexp/formal/FormalRegExpElements.h>
#include <regexp/formal/FormalRegExpStructure.h>

#include <regexp/unbounded/UnboundedRegExpElements.h>
#include <regexp/unbounded/UnboundedRegExpStructure.h>

#include <alphabet/common/SymbolDenormalize.h>

namespace regexp {

/**
 * Determines whether regular expression (or its subtree) describes an empty language (regexp == \0)
 *
 */
class RegExpDenormalize {
public:
    /**
     * Determines whether regular expression is describes an empty language (regexp == \0)
     *
     * \tparam SymbolType the type of symbol in the tested regular expression
     *
     * \param regexp the regexp to test
     *
     * \return true of the language described by the regular expression is empty
     */
    template <class SymbolType>
    static ext::smart_ptr<FormalRegExpElement<SymbolType>> denormalize(regexp::FormalRegExpElement<DefaultSymbolType>&& regexp);

    /**
     * \override
     */
    template <class SymbolType>
    static ext::smart_ptr<UnboundedRegExpElement<SymbolType>> denormalize(regexp::UnboundedRegExpElement<DefaultSymbolType>&& regexp);

    template <class SymbolType>
    class Unbounded {
    public:
        static ext::smart_ptr<UnboundedRegExpElement<SymbolType>> visit(regexp::UnboundedRegExpAlternation<DefaultSymbolType>&& alternation);
        static ext::smart_ptr<UnboundedRegExpElement<SymbolType>> visit(regexp::UnboundedRegExpConcatenation<DefaultSymbolType>&& concatenation);
        static ext::smart_ptr<UnboundedRegExpElement<SymbolType>> visit(regexp::UnboundedRegExpIteration<DefaultSymbolType>&& iteration);
        static ext::smart_ptr<UnboundedRegExpElement<SymbolType>> visit(regexp::UnboundedRegExpSymbol<DefaultSymbolType>&& symbol);
        static ext::smart_ptr<UnboundedRegExpElement<SymbolType>> visit(regexp::UnboundedRegExpEmpty<DefaultSymbolType>&& empty);
        static ext::smart_ptr<UnboundedRegExpElement<SymbolType>> visit(regexp::UnboundedRegExpEpsilon<DefaultSymbolType>&& epsilon);
    };

    template <class SymbolType>
    class Formal {
    public:
        static ext::smart_ptr<FormalRegExpElement<SymbolType>> visit(regexp::FormalRegExpAlternation<DefaultSymbolType>&& alternation);
        static ext::smart_ptr<FormalRegExpElement<SymbolType>> visit(regexp::FormalRegExpConcatenation<DefaultSymbolType>&& concatenation);
        static ext::smart_ptr<FormalRegExpElement<SymbolType>> visit(regexp::FormalRegExpIteration<DefaultSymbolType>&& iteration);
        static ext::smart_ptr<FormalRegExpElement<SymbolType>> visit(regexp::FormalRegExpSymbol<DefaultSymbolType>&& symbol);
        static ext::smart_ptr<FormalRegExpElement<SymbolType>> visit(regexp::FormalRegExpEmpty<DefaultSymbolType>&& empty);
        static ext::smart_ptr<FormalRegExpElement<SymbolType>> visit(regexp::FormalRegExpEpsilon<DefaultSymbolType>&& epsilon);
    };
};

// ----------------------------------------------------------------------------

template <class SymbolType>
ext::smart_ptr<FormalRegExpElement<SymbolType>> RegExpDenormalize::denormalize(regexp::FormalRegExpElement<DefaultSymbolType>&& regexp)
{
    return std::move(regexp).template accept<ext::smart_ptr<FormalRegExpElement<SymbolType>>, RegExpDenormalize::Formal<SymbolType>>();
}

// ----------------------------------------------------------------------------

template <class SymbolType>
ext::smart_ptr<UnboundedRegExpElement<SymbolType>> RegExpDenormalize::denormalize(regexp::UnboundedRegExpElement<DefaultSymbolType>&& regexp)
{
    return std::move(regexp).template accept<ext::smart_ptr<UnboundedRegExpElement<SymbolType>>, RegExpDenormalize::Unbounded<SymbolType>>();
}

// ----------------------------------------------------------------------------

template <class SymbolType>
ext::smart_ptr<UnboundedRegExpElement<SymbolType>> RegExpDenormalize::Unbounded<SymbolType>::visit(regexp::UnboundedRegExpAlternation<DefaultSymbolType>&& alternation)
{
    UnboundedRegExpAlternation<SymbolType>* res = new UnboundedRegExpAlternation<SymbolType>();

    for (UnboundedRegExpElement<SymbolType>&& element : ext::make_mover(std::move(alternation).getChildren()))
        res->appendElement(std::move(*std::move(element).template accept<ext::smart_ptr<UnboundedRegExpElement<SymbolType>>, RegExpDenormalize::Unbounded<SymbolType>>()));

    return ext::smart_ptr<UnboundedRegExpElement<DefaultSymbolType>>(res);
}

template <class SymbolType>
ext::smart_ptr<UnboundedRegExpElement<SymbolType>> RegExpDenormalize::Unbounded<SymbolType>::visit(regexp::UnboundedRegExpConcatenation<DefaultSymbolType>&& concatenation)
{
    UnboundedRegExpConcatenation<SymbolType>* res = new UnboundedRegExpConcatenation<SymbolType>();

    for (UnboundedRegExpElement<SymbolType>&& element : ext::make_mover(std::move(concatenation).getChildren()))
        res->appendElement(std::move(*std::move(element).template accept<ext::smart_ptr<UnboundedRegExpElement<SymbolType>>, RegExpDenormalize::Unbounded<SymbolType>>()));

    return ext::smart_ptr<UnboundedRegExpElement<DefaultSymbolType>>(res);
}

template <class SymbolType>
ext::smart_ptr<UnboundedRegExpElement<SymbolType>> RegExpDenormalize::Unbounded<SymbolType>::visit(regexp::UnboundedRegExpIteration<DefaultSymbolType>&& iteration)
{
    return ext::smart_ptr<UnboundedRegExpElement<SymbolType>>(
        new UnboundedRegExpIteration<SymbolType>(
            std::move(*std::move(std::move(iteration).getElement()).template accept<ext::smart_ptr<UnboundedRegExpElement<SymbolType>>, RegExpDenormalize::Unbounded<SymbolType>>())));
}

template <class SymbolType>
ext::smart_ptr<UnboundedRegExpElement<SymbolType>> RegExpDenormalize::Unbounded<SymbolType>::visit(regexp::UnboundedRegExpSymbol<DefaultSymbolType>&& symbol)
{
    return ext::smart_ptr<UnboundedRegExpElement<SymbolType>>(new UnboundedRegExpSymbol<SymbolType>(alphabet::SymbolDenormalize::denormalizeSymbol<SymbolType>(std::move(symbol).getSymbol())));
}

template <class SymbolType>
ext::smart_ptr<UnboundedRegExpElement<SymbolType>> RegExpDenormalize::Unbounded<SymbolType>::visit(regexp::UnboundedRegExpEmpty<DefaultSymbolType>&&)
{
    return ext::smart_ptr<UnboundedRegExpElement<SymbolType>>(new UnboundedRegExpEmpty<SymbolType>());
}

template <class SymbolType>
ext::smart_ptr<UnboundedRegExpElement<SymbolType>> RegExpDenormalize::Unbounded<SymbolType>::visit(regexp::UnboundedRegExpEpsilon<DefaultSymbolType>&&)
{
    return ext::smart_ptr<UnboundedRegExpElement<SymbolType>>(new UnboundedRegExpEpsilon<SymbolType>());
}

// ----------------------------------------------------------------------------

template <class SymbolType>
ext::smart_ptr<FormalRegExpElement<SymbolType>> RegExpDenormalize::Formal<SymbolType>::visit(regexp::FormalRegExpAlternation<DefaultSymbolType>&& alternation)
{
    return ext::smart_ptr<FormalRegExpElement<SymbolType>>(
        new FormalRegExpAlternation<SymbolType>(
            std::move(*std::move(std::move(alternation).getLeftElement()).template accept<ext::smart_ptr<FormalRegExpElement<SymbolType>>, RegExpDenormalize::Formal<SymbolType>>()),
            std::move(*std::move(std::move(alternation).getRightElement()).template accept<ext::smart_ptr<FormalRegExpElement<SymbolType>>, RegExpDenormalize::Formal<SymbolType>>())));
}

template <class SymbolType>
ext::smart_ptr<FormalRegExpElement<SymbolType>> RegExpDenormalize::Formal<SymbolType>::visit(regexp::FormalRegExpConcatenation<DefaultSymbolType>&& concatenation)
{
    return ext::smart_ptr<FormalRegExpElement<SymbolType>>(
        new FormalRegExpConcatenation<SymbolType>(
            std::move(*std::move(std::move(concatenation).getLeftElement()).template accept<ext::smart_ptr<FormalRegExpElement<SymbolType>>, RegExpDenormalize::Formal<SymbolType>>()),
            std::move(*std::move(std::move(concatenation).getRightElement()).template accept<ext::smart_ptr<FormalRegExpElement<SymbolType>>, RegExpDenormalize::Formal<SymbolType>>())));
}

template <class SymbolType>
ext::smart_ptr<FormalRegExpElement<SymbolType>> RegExpDenormalize::Formal<SymbolType>::visit(regexp::FormalRegExpIteration<DefaultSymbolType>&& iteration)
{
    return ext::smart_ptr<FormalRegExpElement<SymbolType>>(
        new FormalRegExpIteration<SymbolType>(
            std::move(*std::move(std::move(iteration).getElement()).template accept<ext::smart_ptr<FormalRegExpElement<SymbolType>>, RegExpDenormalize::Formal<SymbolType>>())));
}

template <class SymbolType>
ext::smart_ptr<FormalRegExpElement<SymbolType>> RegExpDenormalize::Formal<SymbolType>::visit(regexp::FormalRegExpSymbol<DefaultSymbolType>&& symbol)
{
    return ext::smart_ptr<FormalRegExpElement<SymbolType>>(new FormalRegExpSymbol<SymbolType>(alphabet::SymbolDenormalize::denormalizeSymbol<SymbolType>(std::move(symbol).getSymbol())));
}

template <class SymbolType>
ext::smart_ptr<FormalRegExpElement<SymbolType>> RegExpDenormalize::Formal<SymbolType>::visit(regexp::FormalRegExpEmpty<DefaultSymbolType>&&)
{
    return ext::smart_ptr<FormalRegExpElement<SymbolType>>(new FormalRegExpEmpty<SymbolType>());
}

template <class SymbolType>
ext::smart_ptr<FormalRegExpElement<SymbolType>> RegExpDenormalize::Formal<SymbolType>::visit(regexp::FormalRegExpEpsilon<DefaultSymbolType>&&)
{
    return ext::smart_ptr<FormalRegExpElement<SymbolType>>(new FormalRegExpEpsilon<SymbolType>());
}

} /* namespace regexp */
