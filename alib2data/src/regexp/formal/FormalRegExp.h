/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <common/DefaultSymbolType.h>

namespace regexp {

template <class SymbolType = DefaultSymbolType>
class FormalRegExp;

} /* namespace regexp */

#include <ext/algorithm>
#include <ext/iostream>

#include <alib/set>
#include <alib/string>

#include <core/modules.hpp>
#include <exception/CommonException.h>

#include "../unbounded/UnboundedRegExp.h"
#include "FormalRegExpStructure.h"

#include <core/type_details_base.hpp>
#include <core/type_util.hpp>
#include <regexp/common/RegExpDenormalize.h>
#include <regexp/common/RegExpNormalize.h>

#include <registration/DenormalizationRegistration.hpp>
#include <registration/NormalizationRegistration.hpp>

namespace regexp {

/**
 * \brief
 * Formal regular expression represents regular expression. It describes regular languages. The expression consists of the following nodes:
 *   - Alternation - the representation of + in the regular expression
 *   - Concatenation - the representation of &sdot; in the regular expression
 *   - Iteration - the representation of * (Kleene star)
 *   - Epsilon - the representation of empty word
 *   - Empty - the representation of empty regular expression
 *   - Symbol - the representation of single symbol
 *
 * The formal representation allows nodes of concatenation and alternation to have exactly two children.
 *
 * The structure of the regular expression is wrapped in a structure class to allow separate the alphabet from the structure for example in Extended NFA

 * \details
 * Definition is unbounded definition of regular expressions.
 * E = (T, C),
 * T (Alphabet) = finite set of terminal symbols
 * C (Content) = representation of the regular expression
 *
 * \tparam SymbolType used for the terminal alphabet
 */
template <class SymbolType>
class FormalRegExp final : public core::Components<FormalRegExp<SymbolType>, ext::set<SymbolType>, module::Set, component::GeneralAlphabet> {
    /**
     * The structure of the regular expression.
     */
    FormalRegExpStructure<SymbolType> m_regExp;

public:
    /**
     * The exposed SymbolType template parameter.
     */
    using symbol_type = SymbolType;

    /**
     * \brief Creates a new instance of the expression. The default constructor creates expression describing empty language.
     */
    explicit FormalRegExp();

    /**
     * \brief Creates a new instance of the expression with a concrete alphabet and initial content.
     *
     * \param alphabet the initial input alphabet
     * \param regExp the initial regexp content
     */
    explicit FormalRegExp(ext::set<SymbolType> alphabet, FormalRegExpStructure<SymbolType> regExp);

    /**
     * \brief Creates a new instance of the expression based on initial content. The alphabet is derived from the content.
     *
     * \param regExp the initial regexp content
     */
    explicit FormalRegExp(FormalRegExpStructure<SymbolType> regExp);

    /**
     * \brief Created a new instance of the expression based on the UnboundedRegExp representation.
     */
    explicit FormalRegExp(const UnboundedRegExp<SymbolType>& other);

    /**
     * Getter of the alphabet.
     *
     * \returns the alphabet of the expression
     */
    const ext::set<SymbolType>& getAlphabet() const&
    {
        return this->template accessComponent<component::GeneralAlphabet>().get();
    }

    /**
     * Getter of the alphabet.
     *
     * \returns the alphabet of the expression
     */
    ext::set<SymbolType>&& getAlphabet() &&
    {
        return std::move(this->template accessComponent<component::GeneralAlphabet>().get());
    }

    /**
     * Get the structure of the expression.
     *
     * \returns the structure of the expression.
     */
    const FormalRegExpStructure<SymbolType>& getRegExp() const&;

    /**
     * Get the structure of the expression.
     *
     * \returns the structure of the expression.
     */
    FormalRegExpStructure<SymbolType>&& getRegExp() &&;

    /**
     * Set the structure of the expression.
     *
     * \param regExp the new structure of the expression.
     */
    void setRegExp(FormalRegExpStructure<SymbolType> param);

    /**
     * The three way comparison implementation
     *
     * \param other the other instance
     *
     * \returns the ordering between this object and the @p other.
     */
    auto operator<=>(const FormalRegExp& other) const
    {
        return std::tie(m_regExp.getStructure(), getAlphabet()) <=> std::tie(other.m_regExp.getStructure(), other.getAlphabet());
    }

    /**
     * The equality comparison implementation.
     *
     * \param other the other object to compare with.
     *
     * \returns true if this and other objects are equal, false othervise
     */
    bool operator==(const FormalRegExp& other) const
    {
        return std::tie(m_regExp.getStructure(), getAlphabet()) == std::tie(other.m_regExp.getStructure(), other.getAlphabet());
    }

    /**
     * Print this object as raw representation to ostream.
     *
     * \param out ostream where to print
     * \param instance object to print
     *
     * \returns modified output stream
     */
    friend ext::ostream& operator<<(ext::ostream& out, const FormalRegExp& instance)
    {
        return out << "(FormalRegExp " << instance.getRegExp().getStructure() << ")";
    }
};

template <class SymbolType>
FormalRegExp<SymbolType>::FormalRegExp(ext::set<SymbolType> alphabet, FormalRegExpStructure<SymbolType> regExp)
    : core::Components<FormalRegExp, ext::set<SymbolType>, module::Set, component::GeneralAlphabet>(std::move(alphabet))
    , m_regExp(std::move(regExp))
{
    if (!this->m_regExp.getStructure().checkAlphabet(getAlphabet()))
        throw exception::CommonException("Input symbols not in the alphabet.");
}

template <class SymbolType>
FormalRegExp<SymbolType>::FormalRegExp()
    : FormalRegExp(ext::set<SymbolType>(), FormalRegExpStructure<SymbolType>())
{
}

template <class SymbolType>
FormalRegExp<SymbolType>::FormalRegExp(FormalRegExpStructure<SymbolType> regExp)
    : FormalRegExp(regExp.getStructure().computeMinimalAlphabet(), regExp)
{
}

template <class SymbolType>
FormalRegExp<SymbolType>::FormalRegExp(const UnboundedRegExp<SymbolType>& other)
    : FormalRegExp(other.getAlphabet(), FormalRegExpStructure<SymbolType>(other.getRegExp()))
{
}

template <class SymbolType>
const FormalRegExpStructure<SymbolType>& FormalRegExp<SymbolType>::getRegExp() const&
{
    return m_regExp;
}

template <class SymbolType>
FormalRegExpStructure<SymbolType>&& FormalRegExp<SymbolType>::getRegExp() &&
{
    return std::move(m_regExp);
}

template <class SymbolType>
void FormalRegExp<SymbolType>::setRegExp(FormalRegExpStructure<SymbolType> param)
{
    if (!param.getStructure().checkAlphabet(getAlphabet()))
        throw exception::CommonException("Input symbols not in the alphabet.");

    this->m_regExp = std::move(param);
}

} /* namespace regexp */

namespace core {

/**
 * Helper class specifying constraints for the expression's internal alphabet component.
 *
 * \tparam SymbolType used for the terminal alphabet of the expression.
 */
template <class SymbolType>
class SetConstraint<regexp::FormalRegExp<SymbolType>, SymbolType, component::GeneralAlphabet> {
public:
    /**
     * Returns true if the symbol is still used somewhere in the structure of the expression.
     *
     * \param regexp the tested expresion
     * \param symbol the tested symbol
     *
     * \returns true if the symbol is used, false othervise
     */
    static bool used(const regexp::FormalRegExp<SymbolType>& regexp, const SymbolType& symbol)
    {
        return regexp.getRegExp().getStructure().testSymbol(symbol);
    }

    /**
     * Returns true as all symbols are possibly available to be elements of the alphabet.
     *
     * \param regexp the tested expresion
     * \param symbol the tested state
     *
     * \returns true
     */
    static bool available(const regexp::FormalRegExp<SymbolType>&, const SymbolType&)
    {
        return true;
    }

    /**
     * All symbols are valid as symbols of the alphabet.
     *
     * \param regexp the tested expresion
     * \param symbol the tested state
     */
    static void valid(const regexp::FormalRegExp<SymbolType>&, const SymbolType&)
    {
    }
};

template <class SymbolType>
struct type_util<regexp::FormalRegExp<SymbolType>> {
    static regexp::FormalRegExp<SymbolType> denormalize(regexp::FormalRegExp<object::Object>&& value)
    {
        ext::set<SymbolType> alphabet = alphabet::SymbolDenormalize::denormalizeAlphabet<SymbolType>(std::move(value).getAlphabet());
        regexp::FormalRegExpStructure<SymbolType> structure(std::move(*regexp::RegExpDenormalize::denormalize<SymbolType>(std::move(value).getRegExp().getStructure())));
        return regexp::FormalRegExp<SymbolType>(std::move(alphabet), std::move(structure));
    }

    static regexp::FormalRegExp<object::Object> normalize(regexp::FormalRegExp<SymbolType>&& value)
    {
        ext::set<DefaultSymbolType> alphabet = alphabet::SymbolNormalize::normalizeAlphabet(std::move(value).getAlphabet());
        regexp::FormalRegExpStructure<DefaultSymbolType> structure(std::move(*regexp::RegExpNormalize::normalize<SymbolType>(std::move(value).getRegExp().getStructure())));
        return regexp::FormalRegExp<DefaultSymbolType>(std::move(alphabet), std::move(structure));
    }

    static std::unique_ptr<type_details_base> type(const regexp::FormalRegExp<SymbolType>& arg)
    {
        core::unique_ptr_set<type_details_base> subTypesSymbol;
        for (const SymbolType& item : arg.getAlphabet())
            subTypesSymbol.insert(type_util<SymbolType>::type(item));

        std::vector<std::unique_ptr<type_details_base>> sub_types_vec;
        sub_types_vec.push_back(type_details_variant_type::make_variant(std::move(subTypesSymbol)));
        return std::make_unique<type_details_template>("regexp::FormalRegExp", std::move(sub_types_vec));
    }
};

template <class SymbolType>
struct type_details_retriever<regexp::FormalRegExp<SymbolType>> {
    static std::unique_ptr<type_details_base> get()
    {
        std::vector<std::unique_ptr<type_details_base>> sub_types_vec;
        sub_types_vec.push_back(type_details_retriever<SymbolType>::get());
        return std::make_unique<type_details_template>("regexp::FormalRegExp", std::move(sub_types_vec));
    }
};

} /* namespace core */

extern template class regexp::FormalRegExp<>;
extern template class abstraction::ValueHolder<regexp::FormalRegExp<>>;
extern template const regexp::FormalRegExp<>& abstraction::retrieveValue<const regexp::FormalRegExp<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
extern template class registration::DenormalizationRegisterImpl<const regexp::FormalRegExp<>&>;
extern template class registration::NormalizationRegisterImpl<regexp::FormalRegExp<>>;
