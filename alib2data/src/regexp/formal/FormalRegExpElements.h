#pragma once

#include "FormalRegExpAlternation.h"
#include "FormalRegExpConcatenation.h"
#include "FormalRegExpElement.h"
#include "FormalRegExpEmpty.h"
#include "FormalRegExpEpsilon.h"
#include "FormalRegExpIteration.h"
#include "FormalRegExpSymbol.h"
