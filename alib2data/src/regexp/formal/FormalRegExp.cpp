#include "FormalRegExp.h"
#include "FormalRegExpElements.h"

#include <registration/CastRegistration.hpp>
#include <registration/ValuePrinterRegistration.hpp>

template class regexp::FormalRegExp<>;
template class abstraction::ValueHolder<regexp::FormalRegExp<>>;
template const regexp::FormalRegExp<>& abstraction::retrieveValue<const regexp::FormalRegExp<>&>(const std::shared_ptr<abstraction::Value>& param, bool move);
template class registration::DenormalizationRegisterImpl<const regexp::FormalRegExp<>&>;
template class registration::NormalizationRegisterImpl<regexp::FormalRegExp<>>;
template class regexp::FormalRegExpStructure<DefaultSymbolType>;
template class regexp::FormalRegExpElement<DefaultSymbolType>;
template class regexp::FormalRegExpAlternation<DefaultSymbolType>;
template class regexp::FormalRegExpConcatenation<DefaultSymbolType>;
template class regexp::FormalRegExpIteration<DefaultSymbolType>;
template class regexp::FormalRegExpEpsilon<DefaultSymbolType>;
template class regexp::FormalRegExpEmpty<DefaultSymbolType>;
template class regexp::FormalRegExpSymbol<DefaultSymbolType>;

namespace {

auto formalRegExpFromUnboundedRegExp = registration::CastRegister<regexp::FormalRegExp<>, regexp::UnboundedRegExp<>>();

auto valuePrinter = registration::ValuePrinterRegister<regexp::FormalRegExp<>>();

} /* namespace */
