#pragma once

#include <core/xmlApi.hpp>
#include <regexp/formal/FormalRegExp.h>
#include <regexp/xml/FormalRegExpStructure.h>

#include <regexp/xml/common/RegExpFromXmlParser.h>
#include <regexp/xml/common/RegExpToXmlComposer.h>

namespace core {

template <typename SymbolType>
struct xmlApi<regexp::FormalRegExp<SymbolType>> {
    static regexp::FormalRegExp<SymbolType> parse(ext::deque<sax::Token>::iterator& input);
    static bool first(const ext::deque<sax::Token>::const_iterator& input);
    static std::string xmlTagName();
    static void compose(ext::deque<sax::Token>& output, const regexp::FormalRegExp<SymbolType>& input);
};

template <typename SymbolType>
regexp::FormalRegExp<SymbolType> xmlApi<regexp::FormalRegExp<SymbolType>>::parse(ext::deque<sax::Token>::iterator& input)
{
    sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());

    ext::set<SymbolType> alphabet = regexp::RegExpFromXmlParser::parseAlphabet<SymbolType>(input);

    regexp::FormalRegExpStructure<SymbolType> element(core::xmlApi<regexp::FormalRegExpStructure<SymbolType>>::parse(input));
    regexp::FormalRegExp<SymbolType> regexp(std::move(alphabet), std::move(element));

    sax::FromXMLParserHelper::popToken(input, sax::Token::TokenType::END_ELEMENT, xmlTagName());

    return regexp;
}

template <typename SymbolType>
bool xmlApi<regexp::FormalRegExp<SymbolType>>::first(const ext::deque<sax::Token>::const_iterator& input)
{
    return sax::FromXMLParserHelper::isToken(input, sax::Token::TokenType::START_ELEMENT, xmlTagName());
}

template <typename SymbolType>
std::string xmlApi<regexp::FormalRegExp<SymbolType>>::xmlTagName()
{
    return "FormalRegExp";
}

template <typename SymbolType>
void xmlApi<regexp::FormalRegExp<SymbolType>>::compose(ext::deque<sax::Token>& output, const regexp::FormalRegExp<SymbolType>& input)
{
    output.emplace_back(xmlTagName(), sax::Token::TokenType::START_ELEMENT);
    regexp::RegExpToXmlComposer::composeAlphabet(output, input.getAlphabet());
    core::xmlApi<regexp::FormalRegExpStructure<SymbolType>>::compose(output, input.getRegExp());
    output.emplace_back(xmlTagName(), sax::Token::TokenType::END_ELEMENT);
}

} /* namespace core */
