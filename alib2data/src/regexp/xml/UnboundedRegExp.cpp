#include <regexp/xml/UnboundedRegExp.h>

#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister<regexp::UnboundedRegExp<>>();
auto xmlRead = registration::XmlReaderRegister<regexp::UnboundedRegExp<>>();

auto xmlGroup = registration::XmlRegisterTypeInGroup<object::Object, regexp::UnboundedRegExp<>>();

} /* namespace */
