#pragma once

#include <optional>

#include <core/type_details_base.hpp>
#include <core/type_util.hpp>
#include <ext/typeinfo>
#include <object/Object.h>

#include <alib/tuple>

#include <common/DefaultSymbolType.h>

#include <exception/CommonException.h>

namespace common {

/**
 * Represents symbol in an std.
 */
template <class SymbolType = DefaultSymbolType>
class symbol_or_epsilon {
    std::optional<SymbolType> m_symbol;

public:
    /**
     * Creates new symbol with given name and rank.
     * @param symbol name of the symbol
     * @param rank of the symbol
     */
    explicit symbol_or_epsilon(SymbolType symbol)
        : m_symbol(std::move(symbol))
    {
    }

    explicit symbol_or_epsilon() = default;

    /**
     * @return name of the symbol
     */
    const SymbolType& getSymbol() const&
    {
        if (!m_symbol)
            throw exception::CommonException("The symbol is epsilon");
        else
            return m_symbol.value();
    }

    /**
     * @return name of the symbol
     */
    SymbolType&& getSymbol() &&
    {
        if (!m_symbol)
            throw exception::CommonException("The symbol is epsilon");
        else
            return std::move(m_symbol.value());
    }

    bool is_epsilon() const
    {
        return !m_symbol;
    }

    auto operator<=>(const symbol_or_epsilon& other) const
    {
        return m_symbol <=> other.m_symbol;
    }

    bool operator==(const symbol_or_epsilon& other) const
    {
        return m_symbol == other.m_symbol;
    }

    auto operator<=>(const SymbolType& other) const
    {
        return m_symbol <=> other;
    }

    bool operator==(const SymbolType& other) const
    {
        return m_symbol == other;
    }

    symbol_or_epsilon<SymbolType>& operator++()
    {
        ++m_symbol.value();

        return *this;
    }
};

template <class SymbolType>
ext::ostream& operator<<(ext::ostream& out, const common::symbol_or_epsilon<SymbolType>& symbol)
{
    out << "(symbol_or_epsilon ";
    if (symbol.is_epsilon())
        out << "#E";
    else
        out << symbol.getSymbol();
    out << ")";
    return out;
}

} /* namespace common */

namespace core {

template <class SymbolType>
struct type_util<common::symbol_or_epsilon<SymbolType>> {
    static common::symbol_or_epsilon<SymbolType> denormalize(common::symbol_or_epsilon<object::Object>&& object)
    {
        if (object.is_epsilon())
            return common::symbol_or_epsilon<SymbolType>();
        else
            return common::symbol_or_epsilon<SymbolType>(factory::NormalizeFactory::denormalize<SymbolType>(std::move(std::move(object).getSymbol())));
    }

    static common::symbol_or_epsilon<object::Object> normalize(common::symbol_or_epsilon<SymbolType>&& object)
    {
        if (object.is_epsilon())
            return common::symbol_or_epsilon<object::Object>();
        else
            return common::symbol_or_epsilon<object::Object>(factory::NormalizeFactory::normalize<SymbolType>(std::move(std::move(object).getSymbol())));
    }

    static std::unique_ptr<type_details_base> type(const common::symbol_or_epsilon<SymbolType>& arg)
    {
        core::unique_ptr_set<type_details_base> subTypes;

        if (!arg.is_epsilon())
            subTypes.insert(type_util<SymbolType>::type(arg.getSymbol()));

        std::vector<std::unique_ptr<type_details_base>> sub_types_vec;
        sub_types_vec.push_back(type_details_variant_type::make_variant(std::move(subTypes)));
        return std::make_unique<type_details_template>("common::symbol_or_epsilon", std::move(sub_types_vec));
    }
};

template <class SymbolType>
struct type_details_retriever<common::symbol_or_epsilon<SymbolType>> {
    static std::unique_ptr<type_details_base> get()
    {
        std::vector<std::unique_ptr<type_details_base>> sub_types_vec;
        sub_types_vec.push_back(type_details_retriever<SymbolType>::get());
        return std::make_unique<type_details_template>("common::symbol_or_epsilon", std::move(sub_types_vec));
    }
};

} /* namespace core */
