#pragma once

#include <ext/typeinfo>

#include <alib/tuple>

#include <core/type_details_base.hpp>
#include <core/type_util.hpp>
#include <object/AnyObject.h>
#include <object/Object.h>
#include <object/ObjectFactory.h>

#include <common/DefaultSymbolType.h>

namespace common {

/**
 * Represents symbol in an std.
 */
template <class SymbolType = DefaultSymbolType>
class ranked_symbol {
    SymbolType m_symbol;
    size_t m_rank;

public:
    /**
     * Creates new symbol with given name and rank.
     * @param symbol name of the symbol
     * @param rank of the symbol
     */
    explicit ranked_symbol(SymbolType symbol, size_t rank);

    /**
     * @return name of the symbol
     */
    const SymbolType& getSymbol() const&;

    /**
     * @return name of the symbol
     */
    SymbolType&& getSymbol() &&;

    /**
     * @return rank of the symbol
     */
    const size_t& getRank() const&;

    /**
     * @return rank of the symbol
     */
    size_t getRank() &&;

    auto operator<=>(const ranked_symbol& other) const
    {
        return ext::tie(m_symbol, m_rank) <=> ext::tie(other.m_symbol, other.m_rank);
    }

    bool operator==(const ranked_symbol& other) const
    {
        return ext::tie(m_symbol, m_rank) == ext::tie(other.m_symbol, other.m_rank);
    }

    ranked_symbol<SymbolType>& operator++()
    {
        ++m_symbol;

        return *this;
    }
};

template <class SymbolType>
ranked_symbol<SymbolType>::ranked_symbol(SymbolType symbol, size_t rank)
    : m_symbol(std::move(symbol))
    , m_rank(rank)
{
}

template <class SymbolType>
const SymbolType& ranked_symbol<SymbolType>::getSymbol() const&
{
    return m_symbol;
}

template <class SymbolType>
SymbolType&& ranked_symbol<SymbolType>::getSymbol() &&
{
    return std::move(m_symbol);
}

template <class SymbolType>
const size_t& ranked_symbol<SymbolType>::getRank() const&
{
    return m_rank;
}

template <class SymbolType>
size_t ranked_symbol<SymbolType>::getRank() &&
{
    return m_rank;
}

template <class SymbolType>
ext::ostream& operator<<(ext::ostream& out, const common::ranked_symbol<SymbolType>& symbol)
{
    out << "(ranked_symbol " << symbol.getSymbol() << " #" << symbol.getRank() << ")";
    return out;
}

} /* namespace common */

namespace core {

template <class SymbolType>
struct type_util<common::ranked_symbol<SymbolType>> {
    static common::ranked_symbol<SymbolType> denormalize(common::ranked_symbol<object::Object>&& arg)
    {
        return common::ranked_symbol<SymbolType>(factory::NormalizeFactory::denormalize<SymbolType>(std::move(std::move(arg).getSymbol())), arg.getRank());
    }

    static common::ranked_symbol<object::Object> normalize(common::ranked_symbol<SymbolType>&& arg)
    {
        return common::ranked_symbol<object::Object>(factory::NormalizeFactory::normalize<SymbolType>(std::move(std::move(arg).getSymbol())), arg.getRank());
    }

    static std::unique_ptr<type_details_base> type(const common::ranked_symbol<SymbolType>& arg)
    {
        core::unique_ptr_set<type_details_base> subTypes;

        subTypes.insert(type_util<SymbolType>::type(arg.getSymbol()));

        std::vector<std::unique_ptr<type_details_base>> sub_types_vec;
        sub_types_vec.push_back(type_details_variant_type::make_variant(std::move(subTypes)));
        return std::make_unique<type_details_template>("common::ranked_symbol", std::move(sub_types_vec));
    }
};

template <class SymbolType>
struct type_details_retriever<common::ranked_symbol<SymbolType>> {
    static std::unique_ptr<type_details_base> get()
    {
        std::vector<std::unique_ptr<type_details_base>> sub_types_vec;
        sub_types_vec.push_back(type_details_retriever<SymbolType>::get());
        return std::make_unique<type_details_template>("common::ranked_symbol", std::move(sub_types_vec));
    }
};

} /* namespace core */
