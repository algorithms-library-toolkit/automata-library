#pragma once

#include <core/xmlApi.hpp>
#include <label/FailStateLabel.h>

namespace core {

template <>
struct xmlApi<label::FailStateLabel> {
    static label::FailStateLabel parse(ext::deque<sax::Token>::iterator& input);
    static bool first(const ext::deque<sax::Token>::const_iterator& input);
    static std::string xmlTagName();
    static void compose(ext::deque<sax::Token>& output, const label::FailStateLabel& data);
};

} /* namespace core */
