#pragma once

#include <core/xmlApi.hpp>
#include <label/InitialStateLabel.h>

namespace core {

template <>
struct xmlApi<label::InitialStateLabel> {
    static label::InitialStateLabel parse(ext::deque<sax::Token>::iterator& input);
    static bool first(const ext::deque<sax::Token>::const_iterator& input);
    static std::string xmlTagName();
    static void compose(ext::deque<sax::Token>& output, const label::InitialStateLabel& data);
};

} /* namespace core */
