#include "FinalStateLabel.h"

#include <registration/ValuePrinterRegistration.hpp>

namespace label {

FinalStateLabel::FinalStateLabel() = default;

ext::ostream& operator<<(ext::ostream& out, const FinalStateLabel&)
{
    return out << "(FinalStateLabel)";
}

} /* namespace label */

namespace core {

label::FinalStateLabel type_util<label::FinalStateLabel>::denormalize(label::FinalStateLabel&& arg)
{
    return arg;
}

label::FinalStateLabel type_util<label::FinalStateLabel>::normalize(label::FinalStateLabel&& arg)
{
    return arg;
}

std::unique_ptr<type_details_base> type_util<label::FinalStateLabel>::type(const label::FinalStateLabel&)
{
    return std::make_unique<type_details_type>("label::FinalStateLabel");
}

std::unique_ptr<type_details_base> type_details_retriever<label::FinalStateLabel>::get()
{
    return std::make_unique<type_details_type>("label::FinalStateLabel");
}

} /* namespace core */

namespace {

auto valuePrinter = registration::ValuePrinterRegister<label::FinalStateLabel>();

} /* namespace */
