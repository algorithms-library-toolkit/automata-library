#!/usr/bin/env bash

if [ $# -ne 1 ]; then
	echo "./$0 CONTAINER"
	exit 1
fi

docker run -it \
  --user $(id -u) \
  -e DISPLAY=$DISPLAY \
  -e XDG_RUNTIME_DIR=$XDG_RUNTIME_DIR \
  --volume="/etc/passwd:/etc/passwd:ro" \
  --volume="$XDG_RUNTIME_DIR:$XDG_RUNTIME_DIR" \
  -v /home/$USER/.local/:/home/$USER/.local/:ro \
  -v /usr/share/X11/xkb:/usr/share/X11/xkb \
  -v /tmp/.X11-unix:/tmp/.X11-unix \
  $1 agui2
