#include <catch2/catch.hpp>

#include <alib/list>

#include "sax/SaxComposeInterface.h"
#include "sax/SaxParseInterface.h"

#include "tree/string/ranked/RankedPattern.h"
#include "tree/string/ranked/RankedTree.h"

#include "tree/string/unranked/UnrankedPattern.h"
#include "tree/string/unranked/UnrankedTree.h"

#include "factory/StringDataFactory.hpp"

TEST_CASE("Tree Test", "[unit][str][tree]")
{
    SECTION("Equal")
    {
        {
            std::string input = "RANKED_TREE a 0";
            tree::RankedTree<> tree = factory::StringDataFactory::fromString(input);

            std::string output = factory::StringDataFactory::toString(tree);

            CAPTURE(input, output);
            CHECK(input == output);

            tree::RankedTree<> tree2 = factory::StringDataFactory::fromString(output);

            CHECK(tree == tree2);
        }
        {
            std::string input = "UNRANKED_TREE a |";
            tree::UnrankedTree<> tree = factory::StringDataFactory::fromString(input);

            std::string output = factory::StringDataFactory::toString(tree);

            CAPTURE(input, output);
            CHECK(input == output);

            tree::UnrankedTree<> tree2 = factory::StringDataFactory::fromString(output);

            CHECK(tree == tree2);
        }
        {
            std::string input = "RANKED_TREE a 2 a 0 a 1 a 0";
            tree::RankedTree<> tree = factory::StringDataFactory::fromString(input);

            std::string output = factory::StringDataFactory::toString(tree);

            CAPTURE(input, output);
            CHECK(input == output);

            tree::RankedTree<> tree2 = factory::StringDataFactory::fromString(output);

            CHECK(tree == tree2);
        }
        {
            std::string input = "UNRANKED_TREE a a | a a | | |";
            tree::UnrankedTree<> tree = factory::StringDataFactory::fromString(input);

            std::string output = factory::StringDataFactory::toString(tree);

            CAPTURE(input, output);
            CHECK(input == output);

            tree::UnrankedTree<> tree2 = factory::StringDataFactory::fromString(output);

            CHECK(tree == tree2);
        }
        {
            std::string input = "RANKED_PATTERN a 2 #S a 1 a 0";
            tree::RankedPattern<> tree = factory::StringDataFactory::fromString(input);

            std::string output = factory::StringDataFactory::toString(tree);

            CAPTURE(input, output);
            CHECK(input == output);

            tree::RankedPattern<> tree2 = factory::StringDataFactory::fromString(output);

            CHECK(tree == tree2);
        }
        {
            std::string input = "UNRANKED_PATTERN a #S | a a | | |";
            tree::UnrankedPattern<> tree = factory::StringDataFactory::fromString(input);

            std::string output = factory::StringDataFactory::toString(tree);

            CAPTURE(input, output);
            CHECK(input == output);

            tree::UnrankedPattern<> tree2 = factory::StringDataFactory::fromString(output);

            CHECK(tree == tree2);
        }
    }
}
