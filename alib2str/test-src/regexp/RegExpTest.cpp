#include <catch2/catch.hpp>

#include <alib/list>

#include "sax/SaxComposeInterface.h"
#include "sax/SaxParseInterface.h"

#include "regexp/string/FormalRegExp.h"
#include "regexp/string/UnboundedRegExp.h"

#include "factory/StringDataFactory.hpp"

TEST_CASE("RegExp Test", "[unit][str][regexp]")
{
    SECTION("Equal")
    {
        {
            std::string input = "#E* #0*";
            regexp::UnboundedRegExp<> regexp = factory::StringDataFactory::fromString(input);

            std::string output = factory::StringDataFactory::toString(regexp);

            CHECK(input == output);

            regexp::UnboundedRegExp<> regexp2 = factory::StringDataFactory::fromString(output);

            CHECK(regexp == regexp2);
        }
        {
            std::string input = "a+a";
            regexp::FormalRegExp<> regexp = factory::StringDataFactory::fromString(input);

            std::string output = factory::StringDataFactory::toString(regexp);

            CHECK(input == output);

            regexp::FormalRegExp<> regexp2 = factory::StringDataFactory::fromString(output);

            CHECK(regexp == regexp2);
        }
        {
            std::string input = "a+a (b+a)*";
            regexp::UnboundedRegExp<> regexp = factory::StringDataFactory::fromString(input);

            std::string output = factory::StringDataFactory::toString(regexp);

            CHECK(input == output);

            regexp::UnboundedRegExp<> regexp2 = factory::StringDataFactory::fromString(output);

            CHECK(regexp == regexp2);
        }
    }
}
