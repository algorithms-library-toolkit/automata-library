#include <catch2/catch.hpp>

#include "object/Object.h"

#include "factory/StringDataFactory.hpp"

TEST_CASE("Symbol Test", "[unit][object][alphabet]")
{
    SECTION("Equal")
    {
        {
            std::string input = "a";
            object::Object symbol = factory::StringDataFactory::fromString(input);

            std::string output = factory::StringDataFactory::toString(symbol);
            CHECK(input == output);

            object::Object symbol2 = factory::StringDataFactory::fromString(output);
            CHECK(symbol == symbol2);
        }
        {
            std::string input = "aaaa";
            object::Object symbol = factory::StringDataFactory::fromString(input);

            std::string output = factory::StringDataFactory::toString(symbol);
            CHECK(input == output);

            object::Object symbol2 = factory::StringDataFactory::fromString(output);
            CHECK(symbol == symbol2);
        }
    }
}
