#include <catch2/catch.hpp>

#include "factory/StringDataFactory.hpp"

#include <container/string/ObjectsVariant.h>
#include <primitive/string/Integer.h>
#include <primitive/string/String.h>

TEST_CASE("Container Test", "[unit][str][container]")
{
    SECTION("Variant")
    {
        std::string input1 = "1";
        ext::variant<int, std::string> variant1 = factory::StringDataFactory::fromString(input1);

        std::string output1 = factory::StringDataFactory::toString(variant1);

        CAPTURE(input1, output1);
        CHECK(input1 == output1);

        ext::variant<int, std::string> variant2 = factory::StringDataFactory::fromString(output1);

        CHECK(variant1 == variant2);
        CHECK(variant2.template get<int>() == 1);

        std::string input2 = "aa";
        ext::variant<int, std::string> variant3 = factory::StringDataFactory::fromString(input2);

        CHECK(variant3.template get<std::string>() == "aa");
    }
}
