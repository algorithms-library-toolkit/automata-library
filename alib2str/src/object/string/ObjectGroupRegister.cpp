#include <object/Object.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringReaderGroup = registration::StringReaderGroupRegister<object::Object>();

} /* namespace */
