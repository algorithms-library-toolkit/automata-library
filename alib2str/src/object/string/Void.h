#pragma once

#include <core/stringApi.hpp>
#include <object/Void.h>

namespace core {

template <>
struct stringApi<object::Void> {
    static object::Void parse(ext::istream& input);
    static bool first(ext::istream& input);
    static void compose(ext::ostream& output, const object::Void& primitive);
};

} /* namespace core */
