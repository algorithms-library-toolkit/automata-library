#include <grammar/Grammar.h>
#include "GNF.h"

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister<grammar::GNF<>>();
auto stringReader = registration::StringReaderRegister<grammar::Grammar, grammar::GNF<>>();

} /* namespace */
