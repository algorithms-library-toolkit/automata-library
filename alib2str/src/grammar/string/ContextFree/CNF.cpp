#include <grammar/Grammar.h>
#include "CNF.h"

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister<grammar::CNF<>>();
auto stringReader = registration::StringReaderRegister<grammar::Grammar, grammar::CNF<>>();

} /* namespace */
