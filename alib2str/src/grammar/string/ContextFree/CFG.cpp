#include <grammar/Grammar.h>
#include "CFG.h"

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister<grammar::CFG<>>();
auto stringReader = registration::StringReaderRegister<grammar::Grammar, grammar::CFG<>>();

} /* namespace */
