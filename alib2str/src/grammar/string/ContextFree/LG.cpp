#include <grammar/Grammar.h>
#include "LG.h"

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister<grammar::LG<>>();
auto stringReader = registration::StringReaderRegister<grammar::Grammar, grammar::LG<>>();

} /* namespace */
