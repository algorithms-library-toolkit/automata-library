#pragma once

#include <core/stringApi.hpp>
#include <grammar/ContextFree/GNF.h>

#include <grammar/GrammarFromStringLexer.h>

#include <grammar/string/common/GrammarFromStringParserCommon.h>
#include <grammar/string/common/GrammarToStringComposerCommon.h>

#include <grammar/properties/IsFITDefinition.h>
#include <grammar/simplify/MakeFITDefinition.h>

namespace core {

template <class TerminalSymbolType, class NonterminalSymbolType>
struct stringApi<grammar::GNF<TerminalSymbolType, NonterminalSymbolType>> {
    static grammar::GNF<TerminalSymbolType, NonterminalSymbolType> parse(ext::istream& input);
    static bool first(ext::istream& input);
    static void compose(ext::ostream& output, const grammar::GNF<TerminalSymbolType, NonterminalSymbolType>& grammar);
};

template <class TerminalSymbolType, class NonterminalSymbolType>
grammar::GNF<TerminalSymbolType, NonterminalSymbolType> stringApi<grammar::GNF<TerminalSymbolType, NonterminalSymbolType>>::parse(ext::istream& input)
{
    grammar::GrammarFromStringLexer::Token token = grammar::GrammarFromStringLexer::next(input);
    if (token.type != grammar::GrammarFromStringLexer::TokenType::GNF)
        throw exception::CommonException("Unrecognised GNF token.");

    grammar::GNF<TerminalSymbolType, NonterminalSymbolType> grammar = grammar::GrammarFromStringParserCommon::parseCFLikeGrammar<grammar::GNF<TerminalSymbolType, NonterminalSymbolType>>(input);

    if (!grammar::properties::IsFITDefinition::isFITDefinition(grammar))
        throw exception::CommonException("Initial symbol can not be on right hand side of any rule when generating empty word (epsilon)");

    return grammar;
}

template <class TerminalSymbolType, class NonterminalSymbolType>
bool stringApi<grammar::GNF<TerminalSymbolType, NonterminalSymbolType>>::first(ext::istream& input)
{
    grammar::GrammarFromStringLexer::Token token = grammar::GrammarFromStringLexer::next(input);
    bool res = token.type == grammar::GrammarFromStringLexer::TokenType::GNF;
    grammar::GrammarFromStringLexer::putback(input, token);
    return res;
}

template <class TerminalSymbolType, class NonterminalSymbolType>
void stringApi<grammar::GNF<TerminalSymbolType, NonterminalSymbolType>>::compose(ext::ostream& output, const grammar::GNF<TerminalSymbolType, NonterminalSymbolType>& grammar)
{
    output << "GNF";
    grammar::GrammarToStringComposerCommon::composeCFLikeGrammar(output, grammar::simplify::MakeFITDefinition::makeFITDefinition(grammar));
}

} /* namespace core */
