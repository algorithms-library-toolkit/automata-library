#include <grammar/Grammar.h>
#include "UnrestrictedGrammar.h"

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister<grammar::UnrestrictedGrammar<>>();
auto stringReader = registration::StringReaderRegister<grammar::Grammar, grammar::UnrestrictedGrammar<>>();

} /* namespace */
