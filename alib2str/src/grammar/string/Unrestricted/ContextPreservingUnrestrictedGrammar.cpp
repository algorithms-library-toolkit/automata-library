#include <grammar/Grammar.h>
#include "ContextPreservingUnrestrictedGrammar.h"

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister<grammar::ContextPreservingUnrestrictedGrammar<>>();
auto stringReader = registration::StringReaderRegister<grammar::Grammar, grammar::ContextPreservingUnrestrictedGrammar<>>();

} /* namespace */
