#pragma once

#include <core/stringApi.hpp>
#include <grammar/Unrestricted/UnrestrictedGrammar.h>

#include <grammar/GrammarFromStringLexer.h>

#include <grammar/string/common/GrammarFromStringParserCommon.h>
#include <grammar/string/common/GrammarToStringComposerCommon.h>

namespace core {

template <class SymbolType>
struct stringApi<grammar::UnrestrictedGrammar<SymbolType>> {
    static grammar::UnrestrictedGrammar<SymbolType> parse(ext::istream& input);
    static bool first(ext::istream& input);
    static void compose(ext::ostream& output, const grammar::UnrestrictedGrammar<SymbolType>& grammar);
};

template <class SymbolType>
grammar::UnrestrictedGrammar<SymbolType> stringApi<grammar::UnrestrictedGrammar<SymbolType>>::parse(ext::istream& input)
{
    grammar::GrammarFromStringLexer::Token token = grammar::GrammarFromStringLexer::next(input);
    if (token.type != grammar::GrammarFromStringLexer::TokenType::UNRESTRICTED_GRAMMAR)
        throw exception::CommonException("Unrecognised UnrestrictedGrammar token.");

    return grammar::GrammarFromStringParserCommon::parseCSLikeGrammar<grammar::UnrestrictedGrammar<SymbolType>>(input);
}

template <class SymbolType>
bool stringApi<grammar::UnrestrictedGrammar<SymbolType>>::first(ext::istream& input)
{
    grammar::GrammarFromStringLexer::Token token = grammar::GrammarFromStringLexer::next(input);
    bool res = token.type == grammar::GrammarFromStringLexer::TokenType::UNRESTRICTED_GRAMMAR;
    grammar::GrammarFromStringLexer::putback(input, token);
    return res;
}

template <class SymbolType>
void stringApi<grammar::UnrestrictedGrammar<SymbolType>>::compose(ext::ostream& output, const grammar::UnrestrictedGrammar<SymbolType>& grammar)
{
    output << "UNRESTRICTED_GRAMMAR";
    grammar::GrammarToStringComposerCommon::composeCSLikeGrammar(output, grammar);
}

} /* namespace core */
