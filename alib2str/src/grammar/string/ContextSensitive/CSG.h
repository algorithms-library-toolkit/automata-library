#pragma once

#include <core/stringApi.hpp>
#include <grammar/ContextSensitive/CSG.h>

#include <grammar/GrammarFromStringLexer.h>

#include <grammar/string/common/GrammarFromStringParserCommon.h>
#include <grammar/string/common/GrammarToStringComposerCommon.h>

namespace core {

template <class SymbolType>
struct stringApi<grammar::CSG<SymbolType>> {
    static grammar::CSG<SymbolType> parse(ext::istream& input);
    static bool first(ext::istream& input);
    static void compose(ext::ostream& output, const grammar::CSG<SymbolType>& grammar);
};

template <class SymbolType>
grammar::CSG<SymbolType> stringApi<grammar::CSG<SymbolType>>::parse(ext::istream& input)
{
    grammar::GrammarFromStringLexer::Token token = grammar::GrammarFromStringLexer::next(input);
    if (token.type != grammar::GrammarFromStringLexer::TokenType::CSG)
        throw exception::CommonException("Unrecognised CSG token.");

    return grammar::GrammarFromStringParserCommon::parsePreservingCSLikeGrammar<grammar::CSG<SymbolType>>(input);
}

template <class SymbolType>
bool stringApi<grammar::CSG<SymbolType>>::first(ext::istream& input)
{
    grammar::GrammarFromStringLexer::Token token = grammar::GrammarFromStringLexer::next(input);
    bool res = token.type == grammar::GrammarFromStringLexer::TokenType::CSG;
    grammar::GrammarFromStringLexer::putback(input, token);
    return res;
}

template <class SymbolType>
void stringApi<grammar::CSG<SymbolType>>::compose(ext::ostream& output, const grammar::CSG<SymbolType>& grammar)
{
    output << "CSG";
    grammar::GrammarToStringComposerCommon::composePreservingCSLikeGrammar(output, grammar);
}

} /* namespace core */
