#include <grammar/Grammar.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringReaderGroup = registration::StringReaderGroupRegister<grammar::Grammar>();

} /* namespace */
