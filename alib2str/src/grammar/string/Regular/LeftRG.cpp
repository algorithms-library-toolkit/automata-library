#include <grammar/Grammar.h>
#include "LeftRG.h"

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister<grammar::LeftRG<>>();
auto stringReader = registration::StringReaderRegister<grammar::Grammar, grammar::LeftRG<>>();

} /* namespace */
