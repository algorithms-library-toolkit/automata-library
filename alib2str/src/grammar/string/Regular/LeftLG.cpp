#include <grammar/Grammar.h>
#include "LeftLG.h"

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister<grammar::LeftLG<>>();
auto stringReader = registration::StringReaderRegister<grammar::Grammar, grammar::LeftLG<>>();

} /* namespace */
