#pragma once

#include <core/stringApi.hpp>
#include <grammar/Regular/LeftLG.h>

#include <grammar/GrammarFromStringLexer.h>

#include <grammar/string/common/GrammarFromStringParserCommon.h>
#include <grammar/string/common/GrammarToStringComposerCommon.h>

namespace core {

template <class TerminalSymbolType, class NonterminalSymbolType>
struct stringApi<grammar::LeftLG<TerminalSymbolType, NonterminalSymbolType>> {
    static grammar::LeftLG<TerminalSymbolType, NonterminalSymbolType> parse(ext::istream& input);
    static bool first(ext::istream& input);
    static void compose(ext::ostream& output, const grammar::LeftLG<TerminalSymbolType, NonterminalSymbolType>& grammar);
};

template <class TerminalSymbolType, class NonterminalSymbolType>
grammar::LeftLG<TerminalSymbolType, NonterminalSymbolType> stringApi<grammar::LeftLG<TerminalSymbolType, NonterminalSymbolType>>::parse(ext::istream& input)
{
    grammar::GrammarFromStringLexer::Token token = grammar::GrammarFromStringLexer::next(input);
    if (token.type != grammar::GrammarFromStringLexer::TokenType::LEFT_LG)
        throw exception::CommonException("Unrecognised LeftLG token.");

    return grammar::GrammarFromStringParserCommon::parseCFLikeGrammar<grammar::LeftLG<TerminalSymbolType, NonterminalSymbolType>>(input);
}

template <class TerminalSymbolType, class NonterminalSymbolType>
bool stringApi<grammar::LeftLG<TerminalSymbolType, NonterminalSymbolType>>::first(ext::istream& input)
{
    grammar::GrammarFromStringLexer::Token token = grammar::GrammarFromStringLexer::next(input);
    bool res = token.type == grammar::GrammarFromStringLexer::TokenType::LEFT_LG;
    grammar::GrammarFromStringLexer::putback(input, token);
    return res;
}

template <class TerminalSymbolType, class NonterminalSymbolType>
void stringApi<grammar::LeftLG<TerminalSymbolType, NonterminalSymbolType>>::compose(ext::ostream& output, const grammar::LeftLG<TerminalSymbolType, NonterminalSymbolType>& grammar)
{
    output << "LEFT_LG";
    grammar::GrammarToStringComposerCommon::composeCFLikeGrammar(output, grammar);
}

} /* namespace core */
