#pragma once

#include <core/stringApi.hpp>
#include <grammar/Regular/LeftRG.h>

#include <grammar/GrammarFromStringLexer.h>

#include <grammar/string/common/GrammarFromStringParserCommon.h>
#include <grammar/string/common/GrammarToStringComposerCommon.h>

#include <grammar/properties/IsFITDefinition.h>
#include <grammar/simplify/MakeFITDefinition.h>

namespace core {

template <class TerminalSymbolType, class NonterminalSymbolType>
struct stringApi<grammar::LeftRG<TerminalSymbolType, NonterminalSymbolType>> {
    static grammar::LeftRG<TerminalSymbolType, NonterminalSymbolType> parse(ext::istream& input);
    static bool first(ext::istream& input);
    static void compose(ext::ostream& output, const grammar::LeftRG<TerminalSymbolType, NonterminalSymbolType>& grammar);
};

template <class TerminalSymbolType, class NonterminalSymbolType>
grammar::LeftRG<TerminalSymbolType, NonterminalSymbolType> stringApi<grammar::LeftRG<TerminalSymbolType, NonterminalSymbolType>>::parse(ext::istream& input)
{
    grammar::GrammarFromStringLexer::Token token = grammar::GrammarFromStringLexer::next(input);
    if (token.type != grammar::GrammarFromStringLexer::TokenType::LEFT_RG)
        throw exception::CommonException("Unrecognised LeftRG token.");

    grammar::LeftRG<TerminalSymbolType, NonterminalSymbolType> grammar = grammar::GrammarFromStringParserCommon::parseCFLikeGrammar<grammar::LeftRG<TerminalSymbolType, NonterminalSymbolType>>(input);

    if (!grammar::properties::IsFITDefinition::isFITDefinition(grammar))
        throw exception::CommonException("Initial symbol can not be on right hand side of any rule when generating empty word (epsilon)");

    return grammar;
}

template <class TerminalSymbolType, class NonterminalSymbolType>
bool stringApi<grammar::LeftRG<TerminalSymbolType, NonterminalSymbolType>>::first(ext::istream& input)
{
    grammar::GrammarFromStringLexer::Token token = grammar::GrammarFromStringLexer::next(input);
    bool res = token.type == grammar::GrammarFromStringLexer::TokenType::LEFT_RG;
    grammar::GrammarFromStringLexer::putback(input, token);
    return res;
}

template <class TerminalSymbolType, class NonterminalSymbolType>
void stringApi<grammar::LeftRG<TerminalSymbolType, NonterminalSymbolType>>::compose(ext::ostream& output, const grammar::LeftRG<TerminalSymbolType, NonterminalSymbolType>& grammar)
{
    output << "LEFT_RG";
    grammar::GrammarToStringComposerCommon::composeCFLikeGrammar(output, grammar::simplify::MakeFITDefinition::makeFITDefinition(grammar));
}

} /* namespace core */
