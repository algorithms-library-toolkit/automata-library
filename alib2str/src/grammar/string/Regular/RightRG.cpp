#include <grammar/Grammar.h>
#include "RightRG.h"

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister<grammar::RightRG<>>();
auto stringReader = registration::StringReaderRegister<grammar::Grammar, grammar::RightRG<>>();

} /* namespace */
