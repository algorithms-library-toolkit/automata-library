#pragma once

#include <core/stringApi.hpp>

#include <primitive/PrimitiveFromStringLexer.h>

namespace core {

template <>
struct stringApi<bool> {
    static bool parse(ext::istream& input);
    static bool first(ext::istream& input);
    static void compose(ext::ostream& output, bool primitive);
};

} /* namespace core */
