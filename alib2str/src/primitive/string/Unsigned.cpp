#include <object/Object.h>
#include "Unsigned.h"

#include <registration/StringRegistration.hpp>

namespace core {

unsigned stringApi<unsigned>::parse(ext::istream&)
{
    throw exception::CommonException("parsing unsigned from string not implemented");
}

bool stringApi<unsigned>::first(ext::istream&)
{
    return false;
}

void stringApi<unsigned>::compose(ext::ostream& output, unsigned primitive)
{
    output << primitive;
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister<unsigned>();

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup<object::Object, unsigned>();

} /* namespace */
