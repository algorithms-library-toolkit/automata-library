#include <object/Object.h>
#include "Bool.h"

#include <registration/StringRegistration.hpp>

namespace core {

bool stringApi<bool>::parse(ext::istream&)
{
    throw exception::CommonException("parsing bool from string not implemented");
}

bool stringApi<bool>::first(ext::istream&)
{
    return false;
}

void stringApi<bool>::compose(ext::ostream& output, bool primitive)
{
    output << (primitive ? "true" : "false");
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister<bool>();

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup<object::Object, bool>();

} /* namespace */
