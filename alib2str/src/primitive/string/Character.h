#pragma once

#include <core/stringApi.hpp>

#include <primitive/PrimitiveFromStringLexer.h>

namespace core {

template <>
struct stringApi<char> {
    static char parse(ext::istream& input);
    static bool first(ext::istream& input);
    static void compose(ext::ostream& output, char primitive);
};

} /* namespace core */
