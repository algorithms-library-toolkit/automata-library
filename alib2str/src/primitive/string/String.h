#pragma once

#include <core/stringApi.hpp>

#include <primitive/PrimitiveFromStringLexer.h>

namespace core {

template <>
struct stringApi<std::string> {
    static std::string parse(ext::istream& input);
    static bool first(ext::istream& input);
    static void compose(ext::ostream& output, const std::string& primitive);
};

} /* namespace core */
