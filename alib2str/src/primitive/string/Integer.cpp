#include <object/Object.h>
#include "Integer.h"

#include <registration/StringRegistration.hpp>

namespace core {

int stringApi<int>::parse(ext::istream& input)
{
    primitive::PrimitiveFromStringLexer::Token token = primitive::PrimitiveFromStringLexer::next(input);
    if (token.type != primitive::PrimitiveFromStringLexer::TokenType::INTEGER)
        throw exception::CommonException("Unrecognised INTEGER token.");

    return ext::from_string<int>(token.value);
}

bool stringApi<int>::first(ext::istream& input)
{
    primitive::PrimitiveFromStringLexer::Token token = primitive::PrimitiveFromStringLexer::next(input);
    bool res = token.type == primitive::PrimitiveFromStringLexer::TokenType::INTEGER;
    primitive::PrimitiveFromStringLexer::putback(input, token);
    return res;
}

void stringApi<int>::compose(ext::ostream& output, int primitive)
{
    output << primitive;
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister<int>();
auto stringReader = registration::StringReaderRegister<object::Object, int>();

auto stringReaderGroup = registration::StringReaderRegisterTypeInGroup<object::Object, int>();
auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup<object::Object, int>();

} /* namespace */
