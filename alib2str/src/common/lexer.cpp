#include "lexer.hpp"

#include <ext/istream>
#include <ext/iterator>

#include <alib/string>

namespace ext {

void BasicLexer::putback(ext::istream& input, const std::string& data)
{
    input.clear();

    for (char character : ext::make_reverse(data))
        input.putback(character);
}

bool BasicLexer::test(ext::istream& input, const std::string& value)
{
    if (testAndConsume(input, value)) {
        putback(input, value);
        return true;
    } else {
        return false;
    }
}

void BasicLexer::consume(ext::istream& input, const std::string& value)
{
    if (!testAndConsume(input, value))
        throw std::runtime_error("Can't consume " + value + " from input stream.");
}

bool BasicLexer::testAndConsume(ext::istream& input, const std::string& value)
{
    return static_cast<bool>(input >> std::string(value));
}

} /* namespace ext */
