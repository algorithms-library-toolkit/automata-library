#pragma once

#include <ext/fstream>
#include <ext/sstream>

#include <exception/CommonException.h>
#include <global/GlobalData.h>

#include <core/stringApi.hpp>

namespace factory {

class StringDataFactory {
public:
    class fromFile {
        const std::string& filename;

    public:
        fromFile(const std::string& file)
            : filename(file)
        {
        }

        template <class T>
        operator T()
        {
            ext::ifstream fileStream(filename);

            return fromStream(fileStream);
        }
    };

    class fromString {
        const std::string& string;

    public:
        fromString(const std::string& str)
            : string(str)
        {
        }

        template <class T>
        operator T()
        {
            ext::istringstream stringStream(string);

            return fromStream(stringStream);
        }
    };

    class fromStdin {
    public:
        template <class T>
        operator T()
        {
            return fromStream(common::Streams::in);
        }
    };

    class fromStream {
        ext::istream& in;

    public:
        fromStream(ext::istream& i)
            : in(i)
        {
        }

        template <class T>
        operator T()
        {
            if (in.peek() == EOF)
                throw exception::CommonException("Empty stream");

            T res = core::stringApi<T>::parse(in);

            while (ext::isspace(in.peek()))
                in.get();

            if (in.peek() != EOF)
                throw exception::CommonException(std::string("Unexpected characters at the end of the stream (") + static_cast<char>(in.peek()) + ", code: " + ext::to_string(in.peek()) + ")");

            return res;
        }
    };

    template <class T>
    static void toFile(const T& data, const std::string& filename)
    {
        ext::ofstream fileStream(filename);
        toStream<T>(data, fileStream);
    }

    template <class T>
    static std::string toString(const T& data)
    {
        ext::ostringstream stringStream;
        toStream<T>(data, stringStream);
        return stringStream.str();
    }

    template <class T>
    static void toStdout(const T& data)
    {
        toStream<T>(data, common::Streams::out);
    }

    template <class T>
    static void toStream(const T& data, ext::ostream& out)
    {
        core::stringApi<T>::compose(out, data);
    }
};

} /* namespace factory */
