#include <alphabet/Bar.h>
#include <object/Object.h>
#include "Bar.h"

#include <registration/StringRegistration.hpp>

namespace core {

alphabet::Bar stringApi<alphabet::Bar>::parse(ext::istream&)
{
    throw exception::CommonException("parsing Bar from string not implemented");
}

bool stringApi<alphabet::Bar>::first(ext::istream&)
{
    return false;
}

void stringApi<alphabet::Bar>::compose(ext::ostream& output, const alphabet::Bar&)
{
    output << "#|";
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister<alphabet::Bar>();

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup<object::Object, alphabet::Bar>();

} /* namespace */
