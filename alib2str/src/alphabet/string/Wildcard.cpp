#include <alphabet/Wildcard.h>
#include <object/Object.h>
#include "Wildcard.h"

#include <registration/StringRegistration.hpp>

namespace core {

alphabet::Wildcard stringApi<alphabet::Wildcard>::parse(ext::istream&)
{
    throw exception::CommonException("parsing Wildcard from string not implemented");
}

bool stringApi<alphabet::Wildcard>::first(ext::istream&)
{
    return false;
}

void stringApi<alphabet::Wildcard>::compose(ext::ostream& output, const alphabet::Wildcard&)
{
    output << "#S";
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister<alphabet::Wildcard>();

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup<object::Object, alphabet::Wildcard>();

} /* namespace */
