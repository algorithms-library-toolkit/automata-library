#include <alphabet/BottomOfTheStack.h>
#include <object/Object.h>
#include "BottomOfTheStack.h"

#include <registration/StringRegistration.hpp>

namespace core {

alphabet::BottomOfTheStack stringApi<alphabet::BottomOfTheStack>::parse(ext::istream&)
{
    throw exception::CommonException("parsing BottomOfTheStack from string not implemented");
}

bool stringApi<alphabet::BottomOfTheStack>::first(ext::istream&)
{
    return false;
}

void stringApi<alphabet::BottomOfTheStack>::compose(ext::ostream& output, const alphabet::BottomOfTheStack&)
{
    output << "#T";
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister<alphabet::BottomOfTheStack>();

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup<object::Object, alphabet::BottomOfTheStack>();

} /* namespace */
