#pragma once

#include <alphabet/Blank.h>
#include <core/stringApi.hpp>

namespace core {

template <>
struct stringApi<alphabet::Blank> {
    static alphabet::Blank parse(ext::istream& input);
    static bool first(ext::istream& input);
    static void compose(ext::ostream& output, const alphabet::Blank& symbol);
};

} /* namespace core */
