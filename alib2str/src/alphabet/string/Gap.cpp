#include <alphabet/Gap.h>
#include <object/Object.h>
#include "Gap.h"

#include <registration/StringRegistration.hpp>

namespace core {

alphabet::Gap stringApi<alphabet::Gap>::parse(ext::istream&)
{
    throw exception::CommonException("parsing Gap from string not implemented");
}

bool stringApi<alphabet::Gap>::first(ext::istream&)
{
    return false;
}

void stringApi<alphabet::Gap>::compose(ext::ostream& output, const alphabet::Gap&)
{
    output << "#G";
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister<alphabet::Gap>();

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup<object::Object, alphabet::Gap>();

} /* namespace */
