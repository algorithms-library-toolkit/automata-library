#pragma once

#include <alphabet/BottomOfTheStack.h>
#include <core/stringApi.hpp>

namespace core {

template <>
struct stringApi<alphabet::BottomOfTheStack> {
    static alphabet::BottomOfTheStack parse(ext::istream& input);
    static bool first(ext::istream& input);
    static void compose(ext::ostream& output, const alphabet::BottomOfTheStack& symbol);
};

} /* namespace core */
