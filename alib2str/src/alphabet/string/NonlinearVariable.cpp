#include <object/Object.h>
#include "NonlinearVariable.h"

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister<alphabet::NonlinearVariable<>>();

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup<object::Object, alphabet::NonlinearVariable<>>();

} /* namespace */
