#include <alphabet/Blank.h>
#include <object/Object.h>
#include "Blank.h"

#include <registration/StringRegistration.hpp>

namespace core {

alphabet::Blank stringApi<alphabet::Blank>::parse(ext::istream&)
{
    throw exception::CommonException("parsing Blank from string not implemented");
}

bool stringApi<alphabet::Blank>::first(ext::istream&)
{
    return false;
}

void stringApi<alphabet::Blank>::compose(ext::ostream& output, const alphabet::Blank&)
{
    output << "#B";
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister<alphabet::Blank>();

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup<object::Object, alphabet::Blank>();

} /* namespace */
