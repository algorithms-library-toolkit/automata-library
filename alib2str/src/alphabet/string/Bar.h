#pragma once

#include <alphabet/Bar.h>
#include <core/stringApi.hpp>

namespace core {

template <>
struct stringApi<alphabet::Bar> {
    static alphabet::Bar parse(ext::istream& input);
    static bool first(ext::istream& input);
    static void compose(ext::ostream& output, const alphabet::Bar& symbol);
};

} /* namespace core */
