#pragma once

#include <alphabet/NonlinearVariable.h>
#include <core/stringApi.hpp>

namespace core {

template <class SymbolType>
struct stringApi<alphabet::NonlinearVariable<SymbolType>> {
    static alphabet::NonlinearVariable<SymbolType> parse(ext::istream& input);
    static bool first(ext::istream& input);
    static void compose(ext::ostream& output, const alphabet::NonlinearVariable<SymbolType>& symbol);
};

template <class SymbolType>
alphabet::NonlinearVariable<SymbolType> stringApi<alphabet::NonlinearVariable<SymbolType>>::parse(ext::istream&)
{
    throw exception::CommonException("parsing NonlinearVariable from string not implemented");
}

template <class SymbolType>
bool stringApi<alphabet::NonlinearVariable<SymbolType>>::first(ext::istream&)
{
    return false;
}

template <class SymbolType>
void stringApi<alphabet::NonlinearVariable<SymbolType>>::compose(ext::ostream& output, const alphabet::NonlinearVariable<SymbolType>& symbol)
{
    output << "$";
    core::stringApi<SymbolType>::compose(output, symbol.getSymbol());
}

} /* namespace core */
