#include <label/FailStateLabel.h>
#include <object/Object.h>
#include <primitive/string/String.h>
#include "FailStateLabel.h"

#include <registration/StringRegistration.hpp>

namespace core {

label::FailStateLabel stringApi<label::FailStateLabel>::parse(ext::istream&)
{
    throw exception::CommonException("parsing FailStateLabel from string not implemented");
}

bool stringApi<label::FailStateLabel>::first(ext::istream&)
{
    return false;
}

void stringApi<label::FailStateLabel>::compose(ext::ostream& output, const label::FailStateLabel& label)
{
    stringApi<std::string>::compose(output, ext::to_string(label));
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister<label::FailStateLabel>();

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup<object::Object, label::FailStateLabel>();

} /* namespace */
