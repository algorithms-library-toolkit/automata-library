#pragma once

#include <core/stringApi.hpp>
#include <label/FinalStateLabel.h>

namespace core {

template <>
struct stringApi<label::FinalStateLabel> {
    static label::FinalStateLabel parse(ext::istream& input);
    static bool first(ext::istream& input);
    static void compose(ext::ostream& output, const label::FinalStateLabel& label);
};

} /* namespace core */
