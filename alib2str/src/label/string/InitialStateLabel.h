#pragma once

#include <core/stringApi.hpp>
#include <label/InitialStateLabel.h>

namespace core {

template <>
struct stringApi<label::InitialStateLabel> {
    static label::InitialStateLabel parse(ext::istream& input);
    static bool first(ext::istream& input);
    static void compose(ext::ostream& output, const label::InitialStateLabel& label);
};

} /* namespace core */
