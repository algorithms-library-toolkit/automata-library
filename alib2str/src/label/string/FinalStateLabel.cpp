#include <label/FinalStateLabel.h>
#include <object/Object.h>
#include <primitive/string/String.h>
#include "FinalStateLabel.h"

#include <registration/StringRegistration.hpp>

namespace core {

label::FinalStateLabel stringApi<label::FinalStateLabel>::parse(ext::istream&)
{
    throw exception::CommonException("parsing FinalStateLabel from string not implemented");
}

bool stringApi<label::FinalStateLabel>::first(ext::istream&)
{
    return false;
}

void stringApi<label::FinalStateLabel>::compose(ext::ostream& output, const label::FinalStateLabel& label)
{
    stringApi<std::string>::compose(output, ext::to_string(label));
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister<label::FinalStateLabel>();

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup<object::Object, label::FinalStateLabel>();

} /* namespace */
