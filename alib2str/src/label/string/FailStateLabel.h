#pragma once

#include <core/stringApi.hpp>
#include <label/FailStateLabel.h>

namespace core {

template <>
struct stringApi<label::FailStateLabel> {
    static label::FailStateLabel parse(ext::istream& input);
    static bool first(ext::istream& input);
    static void compose(ext::ostream& output, const label::FailStateLabel& label);
};

} /* namespace core */
