#include <label/InitialStateLabel.h>
#include <object/Object.h>
#include <primitive/string/String.h>
#include "InitialStateLabel.h"

#include <registration/StringRegistration.hpp>

namespace core {

label::InitialStateLabel stringApi<label::InitialStateLabel>::parse(ext::istream&)
{
    throw exception::CommonException("parsing InitialStateLabel from string not implemented");
}

bool stringApi<label::InitialStateLabel>::first(ext::istream&)
{
    return false;
}

void stringApi<label::InitialStateLabel>::compose(ext::ostream& output, const label::InitialStateLabel& label)
{
    stringApi<std::string>::compose(output, ext::to_string(label));
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister<label::InitialStateLabel>();

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup<object::Object, label::InitialStateLabel>();

} /* namespace */
