#pragma once

#include <ext/istream>

#include <alib/string>

#include <common/lexer.hpp>

namespace automaton {

class AutomatonFromStringLexer : public ext::Lexer<AutomatonFromStringLexer> {
public:
    enum class TokenType {
        EPSILON_NFA,
        MULTI_INITIAL_STATE_NFA,
        MULTI_INITIAL_STATE_EPSILON_NFA,
        NFA,
        DFA,
        NFTA,
        DFTA,
        OUT,
        IN,
        EPSILON,
        SEPARATOR,
        LEFT_BRACKET,
        RIGHT_BRACKET,
        NONE,
        COMMA,
        NEW_LINE,
        RANK,
        TEOF,
        ERROR
    };

    static Token next(ext::istream& input);
};

} /* namepsace automaton */
