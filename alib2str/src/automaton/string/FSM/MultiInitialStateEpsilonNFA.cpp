#include <automaton/Automaton.h>
#include "MultiInitialStateEpsilonNFA.h"

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister<automaton::MultiInitialStateEpsilonNFA<>>();
auto stringReader = registration::StringReaderRegister<automaton::Automaton, automaton::MultiInitialStateEpsilonNFA<>>();

} /* namespace */
