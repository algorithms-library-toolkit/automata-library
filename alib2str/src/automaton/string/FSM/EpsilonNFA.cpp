#include <automaton/Automaton.h>
#include "EpsilonNFA.h"

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister<automaton::EpsilonNFA<>>();
auto stringReader = registration::StringReaderRegister<automaton::Automaton, automaton::EpsilonNFA<>>();

} /* namespace */
