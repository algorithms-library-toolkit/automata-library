#include <automaton/Automaton.h>
#include "DFA.h"

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister<automaton::DFA<>>();
auto stringReader = registration::StringReaderRegister<automaton::Automaton, automaton::DFA<>>();

} /* namespace */
