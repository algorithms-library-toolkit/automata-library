#include <automaton/Automaton.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringReaderGroup = registration::StringReaderGroupRegister<automaton::Automaton>();

} /* namespace */
