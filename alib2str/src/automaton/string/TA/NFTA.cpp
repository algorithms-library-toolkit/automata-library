#include <automaton/Automaton.h>
#include "NFTA.h"

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister<automaton::NFTA<>>();
auto stringReader = registration::StringReaderRegister<automaton::Automaton, automaton::NFTA<>>();

} /* namespace */
