#include "TreeFromStringLexer.h"

namespace tree {

TreeFromStringLexer::Token TreeFromStringLexer::next(ext::istream& input)
{
    TreeFromStringLexer::Token token;

    token.type = TokenType::ERROR;
    token.value = "";
    token.raw = "";
    char character;

L0:
    character = static_cast<char>(input.get());
    if (input.eof() || character == static_cast<char>(EOF)) {
        token.type = TokenType::TEOF;
        return token;
    } else if (ext::isspace(character)) {
        token.raw += character;
        goto L0;
    } else if (character == '#') {
        token.value += character;
        token.raw += character;
        goto L1;
    } else if (character == '$') {
        token.raw += character;
        goto L3;
    } else if (character == '|') {
        token.type = TokenType::BAR;
        token.value += character;
        token.raw += character;
        return token;
    } else if ((character >= '0') && (character <= '9')) {
        token.type = TokenType::RANK;
        token.value += character;
        token.raw += character;
        goto L2;
    } else if (input.clear(), input.unget(), input >> std::string("RANKED_TREE")) {
        token.type = TokenType::RANKED_TREE;
        token.value = "RANKED_TREE";
        token.raw += "RANKED_TREE";
        return token;
    } else if (input.clear(), input >> std::string("RANKED_PATTERN")) {
        token.type = TokenType::RANKED_PATTERN;
        token.value = "RANKED_PATTERN";
        token.raw += "RANKED_PATTERN";
        return token;
    } else if (input.clear(), input >> std::string("RANKED_EXTENDED_PATTERN")) {
        token.type = TokenType::RANKED_EXTENDED_PATTERN;
        token.value = "RANKED_EXTENDED_PATTERN";
        token.raw += "RANKED_EXTENDED_PATTERN";
        return token;
    } else if (input.clear(), input >> std::string("RANKED_NONLINEAR_PATTERN")) {
        token.type = TokenType::RANKED_NONLINEAR_PATTERN;
        token.value = "RANKED_NONLINEAR_PATTERN";
        token.raw += "RANKED_NONLINEAR_PATTERN";
        return token;
    } else if (input.clear(), input >> std::string("UNRANKED_TREE")) {
        token.type = TokenType::UNRANKED_TREE;
        token.value = "UNRANKED_TREE";
        token.raw += "UNRANKED_TREE";
        return token;
    } else if (input.clear(), input >> std::string("UNRANKED_PATTERN")) {
        token.type = TokenType::UNRANKED_PATTERN;
        token.value = "UNRANKED_PATTERN";
        token.raw += "UNRANKED_PATTERN";
        return token;
    } else if (input.clear(), input >> std::string("UNRANKED_NONLINEAR_PATTERN")) {
        token.type = TokenType::UNRANKED_NONLINEAR_PATTERN;
        token.value = "UNRANKED_NONLINEAR_PATTERN";
        token.raw += "UNRANKED_NONLINEAR_PATTERN";
        return token;
    } else if (input.clear(), input >> std::string("UNRANKED_EXTENDED_PATTERN")) {
        token.type = TokenType::UNRANKED_EXTENDED_PATTERN;
        token.value = "UNRANKED_EXTENDED_PATTERN";
        token.raw += "UNRANKED_EXTENDED_PATTERN";
        return token;
    } else {
        input.clear();
        putback(input, token);
        token.raw = "";
        token.type = TokenType::ERROR;
        return token;
    }

L1:
    character = static_cast<char>(input.get());
    if (input.eof()) {
        token.type = TokenType::TEOF;
        return token;
    } else if (character == 'S') {
        token.type = TokenType::SUBTREE_WILDCARD;
        token.value += character;
        token.raw += character;
        return token;
    } else if (character == 'G') {
        token.type = TokenType::SUBTREE_GAP;
        token.value += character;
        token.raw += character;
        return token;
    } else if (character == 'N') {
        token.type = TokenType::NODE_WILDCARD;
        token.value += character;
        token.raw += character;
        return token;
    } else {
        input.clear();
        input.unget();
        putback(input, token);
        token.raw = "";
        token.type = TokenType::ERROR;
        return token;
    }

L2:
    character = static_cast<char>(input.get());
    if (input.eof()) {
        return token;
    } else if ((character >= '0') && (character <= '9')) {
        token.value += character;
        token.raw += character;
        goto L2;
    } else {
        input.clear();
        input.unget();
        return token;
    }

L3:
    character = static_cast<char>(input.get());
    if (input.eof()) {
        token.type = TokenType::TEOF;
        return token;
    } else if ((character >= 'A') && (character <= 'Z')) {
        token.type = TokenType::NONLINEAR_VARIABLE;
        token.value += character;
        token.raw += character;
        return token;
    } else {
        input.clear();
        input.unget();
        putback(input, token);
        token.raw = "";
        token.type = TokenType::ERROR;
        return token;
    }
}

} /* namespace tree */
