#include <tree/Tree.h>
#include "UnrankedNonlinearPattern.h"

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister<tree::UnrankedNonlinearPattern<>>();
auto stringReader = registration::StringReaderRegister<tree::Tree, tree::UnrankedNonlinearPattern<>>();

} /* namespace */
