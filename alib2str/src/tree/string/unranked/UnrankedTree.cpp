#include <tree/Tree.h>
#include "UnrankedTree.h"

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister<tree::UnrankedTree<>>();
auto stringReader = registration::StringReaderRegister<tree::Tree, tree::UnrankedTree<>>();

} /* namespace */
