#pragma once

#include <core/stringApi.hpp>
#include <tree/unranked/UnrankedTree.h>

#include <tree/TreeFromStringLexer.h>

#include <tree/string/common/TreeFromStringParserCommon.h>
#include <tree/string/common/TreeToStringComposerCommon.h>

namespace core {

template <class SymbolType>
struct stringApi<tree::UnrankedTree<SymbolType>> {
    static tree::UnrankedTree<SymbolType> parse(ext::istream& input);
    static bool first(ext::istream& input);
    static void compose(ext::ostream& output, const tree::UnrankedTree<SymbolType>& tree);
};

template <class SymbolType>
tree::UnrankedTree<SymbolType> stringApi<tree::UnrankedTree<SymbolType>>::parse(ext::istream& input)
{
    tree::TreeFromStringLexer::Token token = tree::TreeFromStringLexer::next(input);
    if (token.type != tree::TreeFromStringLexer::TokenType::UNRANKED_TREE)
        throw exception::CommonException("Unrecognised UNRANKED_TREE token.");

    ext::set<SymbolType> nonlinearVariables;
    bool isPattern = false;
    bool isExtendedPattern = false;

    ext::tree<SymbolType> content = tree::TreeFromStringParserCommon::parseUnrankedContent<SymbolType>(input, isPattern, isExtendedPattern, nonlinearVariables);
    if (isPattern)
        throw exception::CommonException("Unexpected subtree wildcard recognised");
    if (!nonlinearVariables.empty())
        throw exception::CommonException("Unexpected variables recognised");
    if (isExtendedPattern)
        throw exception::CommonException("Unexpected node wildcards recognised");

    return tree::UnrankedTree<SymbolType>(content);
}

template <class SymbolType>
bool stringApi<tree::UnrankedTree<SymbolType>>::first(ext::istream& input)
{
    tree::TreeFromStringLexer::Token token = tree::TreeFromStringLexer::next(input);
    bool res = token.type == tree::TreeFromStringLexer::TokenType::UNRANKED_TREE;
    tree::TreeFromStringLexer::putback(input, token);
    return res;
}

template <class SymbolType>
void stringApi<tree::UnrankedTree<SymbolType>>::compose(ext::ostream& output, const tree::UnrankedTree<SymbolType>& tree)
{
    output << "UNRANKED_TREE ";
    tree::TreeToStringComposerCommon::compose(output, tree.getContent());
}

} /* namespace core */
