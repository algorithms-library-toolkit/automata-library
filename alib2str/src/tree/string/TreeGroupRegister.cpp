#include <tree/Tree.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringReaderGroup = registration::StringReaderGroupRegister<tree::Tree>();

} /* namespace */
