#include <tree/Tree.h>
#include "RankedPattern.h"

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister<tree::RankedPattern<>>();
auto stringReader = registration::StringReaderRegister<tree::Tree, tree::RankedPattern<>>();

} /* namespace */
