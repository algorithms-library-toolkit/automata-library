#pragma once

#include <core/stringApi.hpp>
#include <tree/ranked/RankedExtendedPattern.h>

#include <tree/TreeFromStringLexer.h>

#include <tree/string/common/TreeFromStringParserCommon.h>
#include <tree/string/common/TreeToStringComposerCommon.h>

namespace core {

template <class SymbolType>
struct stringApi<tree::RankedExtendedPattern<SymbolType>> {
    static tree::RankedExtendedPattern<SymbolType> parse(ext::istream& input);
    static bool first(ext::istream& input);
    static void compose(ext::ostream& output, const tree::RankedExtendedPattern<SymbolType>& tree);
};

template <class SymbolType>
tree::RankedExtendedPattern<SymbolType> stringApi<tree::RankedExtendedPattern<SymbolType>>::parse(ext::istream& input)
{
    tree::TreeFromStringLexer::Token token = tree::TreeFromStringLexer::next(input);
    if (token.type != tree::TreeFromStringLexer::TokenType::RANKED_EXTENDED_PATTERN)
        throw exception::CommonException("Unrecognised RANKED_EXTENDED_PATTERN token.");

    ext::set<common::ranked_symbol<SymbolType>> nonlinearVariables;
    ext::set<common::ranked_symbol<SymbolType>> nodeWildcards;
    bool isPattern = false;

    ext::tree<common::ranked_symbol<SymbolType>> content = tree::TreeFromStringParserCommon::parseRankedContent<SymbolType>(input, isPattern, nonlinearVariables, nodeWildcards);

    if (!nonlinearVariables.empty())
        throw exception::CommonException("Unexpected variables recognised");

    return tree::RankedExtendedPattern<SymbolType>(alphabet::Wildcard::instance<common::ranked_symbol<SymbolType>>(), nodeWildcards, content);
}

template <class SymbolType>
bool stringApi<tree::RankedExtendedPattern<SymbolType>>::first(ext::istream& input)
{
    tree::TreeFromStringLexer::Token token = tree::TreeFromStringLexer::next(input);
    bool res = token.type == tree::TreeFromStringLexer::TokenType::RANKED_EXTENDED_PATTERN;
    tree::TreeFromStringLexer::putback(input, token);
    return res;
}

template <class SymbolType>
void stringApi<tree::RankedExtendedPattern<SymbolType>>::compose(ext::ostream&, const tree::RankedExtendedPattern<SymbolType>&)
{
    throw exception::CommonException("Unimplemented.");
}

} /* namespace core */
