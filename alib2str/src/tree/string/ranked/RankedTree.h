#pragma once

#include <core/stringApi.hpp>
#include <tree/ranked/RankedTree.h>

#include <tree/TreeFromStringLexer.h>

#include <tree/string/common/TreeFromStringParserCommon.h>
#include <tree/string/common/TreeToStringComposerCommon.h>

namespace core {

template <class SymbolType>
struct stringApi<tree::RankedTree<SymbolType>> {
    static tree::RankedTree<SymbolType> parse(ext::istream& input);
    static bool first(ext::istream& input);
    static void compose(ext::ostream& output, const tree::RankedTree<SymbolType>& tree);
};

template <class SymbolType>
tree::RankedTree<SymbolType> stringApi<tree::RankedTree<SymbolType>>::parse(ext::istream& input)
{
    tree::TreeFromStringLexer::Token token = tree::TreeFromStringLexer::next(input);
    if (token.type != tree::TreeFromStringLexer::TokenType::RANKED_TREE)
        throw exception::CommonException("Unrecognised RANKED_TREE token.");

    ext::set<common::ranked_symbol<SymbolType>> nonlinearVariables;
    ext::set<common::ranked_symbol<SymbolType>> nodeWildcards;
    bool isPattern = false;

    ext::tree<common::ranked_symbol<SymbolType>> content = tree::TreeFromStringParserCommon::parseRankedContent<SymbolType>(input, isPattern, nonlinearVariables, nodeWildcards);

    if (isPattern)
        throw exception::CommonException("Unexpected subtree wildcard recognised");
    if (!nonlinearVariables.empty())
        throw exception::CommonException("Unexpected variables recognised");
    if (!nodeWildcards.empty())
        throw exception::CommonException("Unexpected node wildcards recognised");

    return tree::RankedTree<SymbolType>(content);
}

template <class SymbolType>
bool stringApi<tree::RankedTree<SymbolType>>::first(ext::istream& input)
{
    tree::TreeFromStringLexer::Token token = tree::TreeFromStringLexer::next(input);
    bool res = token.type == tree::TreeFromStringLexer::TokenType::RANKED_TREE;
    tree::TreeFromStringLexer::putback(input, token);
    return res;
}

template <class SymbolType>
void stringApi<tree::RankedTree<SymbolType>>::compose(ext::ostream& output, const tree::RankedTree<SymbolType>& tree)
{
    output << "RANKED_TREE ";
    tree::TreeToStringComposerCommon::compose(output, tree.getContent());
}

} /* namespace core */
