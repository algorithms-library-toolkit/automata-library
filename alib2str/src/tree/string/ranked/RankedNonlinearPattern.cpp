#include <tree/Tree.h>
#include "RankedNonlinearPattern.h"

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister<tree::RankedNonlinearPattern<>>();
auto stringReader = registration::StringReaderRegister<tree::Tree, tree::RankedNonlinearPattern<>>();

} /* namespace */
