#pragma once

#include <alib/vector>

#include <core/stringApi.hpp>

#include <string/StringFromStringLexer.h>

#include <alphabet/Gap.h>
#include <alphabet/NodeWildcard.h>
#include <alphabet/NonlinearVariable.h>
#include <alphabet/Wildcard.h>

namespace tree {

class TreeFromStringParserCommon {
public:
    template <class SymbolType>
    static ext::tree<common::ranked_symbol<SymbolType>> parseRankedContent(ext::istream& input, bool& isPattern, ext::set<common::ranked_symbol<SymbolType>>& nonlinearVariables, ext::set<common::ranked_symbol<SymbolType>>& nodeWildcards);
    template <class SymbolType>
    static ext::tree<SymbolType> parseUnrankedContent(ext::istream& input, bool& isPattern, bool& isExtendedPattern, ext::set<SymbolType>& nonlinearVariables);
};

template <class SymbolType>
ext::tree<common::ranked_symbol<SymbolType>> TreeFromStringParserCommon::parseRankedContent(ext::istream& input, bool& isPattern, ext::set<common::ranked_symbol<SymbolType>>& nonlinearVariables, ext::set<common::ranked_symbol<SymbolType>>& nodeWildcards)
{
    TreeFromStringLexer::Token token = TreeFromStringLexer::next(input);

    if (token.type == TreeFromStringLexer::TokenType::SUBTREE_WILDCARD) {
        isPattern = true;
        return ext::tree<common::ranked_symbol<SymbolType>>(alphabet::Wildcard::instance<common::ranked_symbol<SymbolType>>(), {});
    } else if (token.type == TreeFromStringLexer::TokenType::NONLINEAR_VARIABLE) {
        isPattern = true;
        common::ranked_symbol<SymbolType> nonlinearVariable(object::ObjectFactory<SymbolType>::construct(alphabet::NonlinearVariable<SymbolType>(object::ObjectFactory<SymbolType>::construct(token.value))), 0);
        nonlinearVariables.insert(nonlinearVariable);
        return ext::tree<common::ranked_symbol<SymbolType>>(std::move(nonlinearVariable), {});
    } else {
        bool isWildcard = false;
        SymbolType symbol = [&]() {
            if (token.type == TreeFromStringLexer::TokenType::NODE_WILDCARD) {
                isWildcard = true;
                return alphabet::NodeWildcard::instance<SymbolType>();
            } else {
                TreeFromStringLexer::putback(input, token);
                return core::stringApi<SymbolType>::parse(input);
            }
        }();

        token = TreeFromStringLexer::next(input);
        if (token.type != TreeFromStringLexer::TokenType::RANK)
            throw exception::CommonException("Missing rank");

        unsigned rank = ext::from_string<unsigned>(token.value);
        if (isWildcard) {
            nodeWildcards.insert(common::ranked_symbol<SymbolType>(symbol, rank));
            isPattern = true;
        }

        ext::vector<ext::tree<common::ranked_symbol<SymbolType>>> childs;
        for (unsigned i = 0; i < rank; i++)
            childs.emplace_back(ext::tree<common::ranked_symbol<SymbolType>>(parseRankedContent<SymbolType>(input, isPattern, nonlinearVariables, nodeWildcards)));

        return ext::tree<common::ranked_symbol<SymbolType>>(common::ranked_symbol<SymbolType>(std::move(symbol), rank), std::move(childs));
    }
}

template <class SymbolType>
ext::tree<SymbolType> TreeFromStringParserCommon::parseUnrankedContent(ext::istream& input, bool& isPattern, bool& isExtendedPattern, ext::set<SymbolType>& nonlinearVariables)
{
    TreeFromStringLexer::Token token = TreeFromStringLexer::next(input);

    if (token.type == TreeFromStringLexer::TokenType::SUBTREE_WILDCARD) {
        token = TreeFromStringLexer::next(input);

        if (token.type != TreeFromStringLexer::TokenType::BAR)
            throw exception::CommonException("Missing bar");

        isPattern = true;
        return ext::tree<SymbolType>(alphabet::Wildcard::instance<SymbolType>(), {});
    } else if (token.type == TreeFromStringLexer::TokenType::SUBTREE_GAP) {
        token = TreeFromStringLexer::next(input);

        if (token.type != TreeFromStringLexer::TokenType::BAR)
            throw exception::CommonException("Missing bar");

        isPattern = true;
        return ext::tree<SymbolType>(alphabet::Gap::instance<SymbolType>(), {});
    } else if (token.type == TreeFromStringLexer::TokenType::NONLINEAR_VARIABLE) {
        token = TreeFromStringLexer::next(input);

        if (token.type != TreeFromStringLexer::TokenType::BAR)
            throw exception::CommonException("Missing bar");

        isPattern = true;
        SymbolType nonlinearVariable = object::ObjectFactory<SymbolType>::construct(alphabet::NonlinearVariable<SymbolType>(object::ObjectFactory<SymbolType>::construct(token.value)));
        nonlinearVariables.insert(nonlinearVariable);
        return ext::tree<SymbolType>(std::move(nonlinearVariable), {});
    } else {
        SymbolType symbol = [&]() {
            if (token.type == TreeFromStringLexer::TokenType::NODE_WILDCARD) {
                isExtendedPattern = true;
                isPattern = true;
                return alphabet::NodeWildcard::instance<SymbolType>();
            } else {
                TreeFromStringLexer::putback(input, token);
                return core::stringApi<SymbolType>::parse(input);
            }
        }();

        ext::vector<ext::tree<SymbolType>> childs;

        token = TreeFromStringLexer::next(input);

        while (token.type != TreeFromStringLexer::TokenType::BAR) {
            TreeFromStringLexer::putback(input, token);
            childs.emplace_back(parseUnrankedContent<SymbolType>(input, isPattern, isExtendedPattern, nonlinearVariables));
            token = TreeFromStringLexer::next(input);
        }

        if (token.type != TreeFromStringLexer::TokenType::BAR)
            throw exception::CommonException("Missing bar");

        return ext::tree<SymbolType>(std::move(symbol), std::move(childs));
    }
}

} /* namespace tree */
