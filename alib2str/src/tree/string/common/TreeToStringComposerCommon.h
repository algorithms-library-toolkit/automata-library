#pragma once

#include <alib/vector>
#include <ostream>

#include <core/stringApi.hpp>

namespace tree {

class TreeToStringComposerCommon {
public:
    template <class SymbolType>
    static void compose(ext::ostream&, const ext::tree<common::ranked_symbol<SymbolType>>& node);
    template <class SymbolType>
    static void compose(ext::ostream&, const common::ranked_symbol<SymbolType>& subtreeWildcard, const ext::tree<common::ranked_symbol<SymbolType>>& node);
    template <class SymbolType>
    static void compose(ext::ostream&, const ext::tree<SymbolType>& node);
    template <class SymbolType>
    static void compose(ext::ostream&, const SymbolType& subtreeWildcard, const SymbolType& subtreeGap, const ext::tree<SymbolType>& node);
};

template <class SymbolType>
void TreeToStringComposerCommon::compose(ext::ostream& out, const ext::tree<common::ranked_symbol<SymbolType>>& node)
{
    core::stringApi<SymbolType>::compose(out, node.getData().getSymbol());

    out << " " << ext::to_string(node.getData().getRank());

    for (const ext::tree<common::ranked_symbol<SymbolType>>& child : node) {
        out << " ";
        compose(out, child);
    }
}

template <class SymbolType>
void TreeToStringComposerCommon::compose(ext::ostream& out, const common::ranked_symbol<SymbolType>& subtreeWildcard, const ext::tree<common::ranked_symbol<SymbolType>>& node)
{
    if (node.getData() == subtreeWildcard) {
        out << "#S";
    } else {
        core::stringApi<SymbolType>::compose(out, node.getData().getSymbol());

        out << " " << ext::to_string(node.getData().getRank());

        for (const ext::tree<common::ranked_symbol<SymbolType>>& child : node) {
            out << " ";
            compose(out, subtreeWildcard, child);
        }
    }
}

template <class SymbolType>
void TreeToStringComposerCommon::compose(ext::ostream& out, const ext::tree<SymbolType>& node)
{
    core::stringApi<SymbolType>::compose(out, node.getData());

    for (const ext::tree<SymbolType>& child : node) {
        out << " ";
        compose(out, child);
    }

    out << " |";
}

template <class SymbolType>
void TreeToStringComposerCommon::compose(ext::ostream& out, const SymbolType& subtreeWildcard, const SymbolType& subtreeGap, const ext::tree<SymbolType>& node)
{
    if (node.getData() == subtreeWildcard) {
        out << "#S |";
    } else if (node.getData() == subtreeWildcard) {
        out << "#G |";
    } else {
        core::stringApi<SymbolType>::compose(out, node.getData());

        for (const ext::tree<SymbolType>& child : node) {
            out << " ";
            compose(out, subtreeWildcard, subtreeGap, child);
        }

        out << " |";
    }
}

} /* namespace tree */
