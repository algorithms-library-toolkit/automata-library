#pragma once

#include <ext/memory>
#include <ext/typeinfo>

#include <alib/map>
#include <alib/set>
#include <alib/string>

#include <abstraction/OperationAbstraction.hpp>

namespace abstraction {

class StringWriterRegistry {
public:
    class Entry {
    public:
        virtual std::unique_ptr<abstraction::OperationAbstraction> getAbstraction() const = 0;

        virtual ~Entry() = default;
    };

private:
    template <class Param>
    class EntryImpl : public Entry {
    public:
        EntryImpl() = default;

        std::unique_ptr<abstraction::OperationAbstraction> getAbstraction() const override;
    };

    static ext::map<core::type_details, std::unique_ptr<Entry>>& getEntries();

public:
    static void unregisterStringWriter(const core::type_details& param);

    template <class ParamType>
    static void unregisterStringWriter()
    {
        core::type_details param = core::type_details::get<ParamType>();
        unregisterStringWriter(param);
    }

    static void registerStringWriter(core::type_details param, std::unique_ptr<Entry> entry);

    template <class ParamType>
    static void registerStringWriter(core::type_details param)
    {
        registerStringWriter(std::move(param), std::unique_ptr<Entry>(new EntryImpl<ParamType>()));
    }

    template <class ParamType>
    static void registerStringWriter()
    {
        core::type_details param = core::type_details::get<ParamType>();
        registerStringWriter<ParamType>(std::move(param));
    }

    static std::unique_ptr<abstraction::OperationAbstraction> getAbstraction(const core::type_details& param);
};

} /* namespace abstraction */

#include <abstraction/StringWriterAbstraction.hpp>

namespace abstraction {

template <class Param>
std::unique_ptr<abstraction::OperationAbstraction> StringWriterRegistry::EntryImpl<Param>::getAbstraction() const
{
    return std::make_unique<abstraction::StringWriterAbstraction<const Param&>>();
}


} /* namespace abstraction */
