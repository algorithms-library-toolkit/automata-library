#pragma once

#include <functional>

#include <ext/algorithm>
#include <ext/istream>
#include <ext/memory>
#include <ext/typeinfo>

#include <alib/list>
#include <alib/map>
#include <alib/string>

#include <abstraction/OperationAbstraction.hpp>

#include <core/stringApi.hpp>

namespace abstraction {

class StringReaderRegistry {
public:
    class Entry {
    public:
        virtual std::unique_ptr<abstraction::OperationAbstraction> getAbstraction() const = 0;

        virtual ~Entry() = default;
    };

private:
    template <class Return>
    class EntryImpl : public Entry {
    public:
        EntryImpl() = default;

        std::unique_ptr<abstraction::OperationAbstraction> getAbstraction() const override;
    };

    static ext::map<core::type_details, ext::list<std::pair<std::function<bool(ext::istream&)>, std::unique_ptr<Entry>>>>& getEntries();

public:
    static void unregisterStringReader(const core::type_details& group, ext::list<std::pair<std::function<bool(ext::istream&)>, std::unique_ptr<Entry>>>::const_iterator iter);

    template <class Group>
    static void unregisterStringReader(ext::list<std::pair<std::function<bool(ext::istream&)>, std::unique_ptr<Entry>>>::const_iterator iter)
    {
        core::type_details group = core::type_details::as_type(ext::to_string<Group>());
        unregisterStringReader(group, iter);
    }

    static ext::list<std::pair<std::function<bool(ext::istream&)>, std::unique_ptr<Entry>>>::const_iterator registerStringReader(core::type_details group, std::function<bool(ext::istream&)> first, std::unique_ptr<Entry> entry);

    template <class Group, class ReturnType>
    static ext::list<std::pair<std::function<bool(ext::istream&)>, std::unique_ptr<Entry>>>::const_iterator registerStringReader()
    {
        return registerStringReader(core::type_details::as_type(ext::to_string<Group>()), core::stringApi<ReturnType>::first, std::unique_ptr<Entry>(new EntryImpl<ReturnType>()));
    }

    static std::unique_ptr<abstraction::OperationAbstraction> getAbstraction(const core::type_details& group, const std::string& str);
};

} /* namespace abstraction */

#include <abstraction/StringReaderAbstraction.hpp>

namespace abstraction {

template <class Return>
std::unique_ptr<abstraction::OperationAbstraction> StringReaderRegistry::EntryImpl<Return>::getAbstraction() const
{
    return std::make_unique<abstraction::StringReaderAbstraction<Return>>();
}

} /* namespace abstraction */
