#include "ObjectsVector.h"

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister<ext::vector<object::Object>>();
auto stringReaded = registration::StringReaderRegister<object::Object, ext::vector<object::Object>>();

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup<object::Object, ext::vector<object::Object>>();
auto stringReadedGroup = registration::StringReaderRegisterTypeInGroup<object::Object, ext::vector<object::Object>>();

} /* namespace */
