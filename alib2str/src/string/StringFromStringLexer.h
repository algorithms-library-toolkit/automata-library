#pragma once

#include <ext/istream>

#include <alib/string>

#include <common/lexer.hpp>

namespace string {

class StringFromStringLexer : public ext::Lexer<StringFromStringLexer> {
public:
    enum class TokenType {
        LESS,
        GREATER,
        QUOTE,
        TERM,
        TEOF,
        ERROR
    };

    static Token next(ext::istream& input);
};

} /* namespace string */
