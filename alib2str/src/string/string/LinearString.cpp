#include <string/String.h>
#include "LinearString.h"

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister<string::LinearString<>>();
auto stringReader = registration::StringReaderRegister<string::String, string::LinearString<>>();

} /* namespace */
