project(alt-libalgo VERSION ${CMAKE_PROJECT_VERSION})
find_package(LibXml2 REQUIRED)
alt_library(alib2algo
	DEPENDS alib2str alib2std alib2common alib2data alib2xml LibXml2::LibXml2
	TEST_DEPENDS LibXml2::LibXml2
)
