#include <registration/AlgoRegistration.hpp>
#include "ExactFactorMatch.h"

namespace {

auto ExactFactorMatchLinearString = registration::AbstractRegister<stringology::exact::ExactFactorMatch, ext::set<unsigned>, const string::LinearString<>&, const string::LinearString<>&>(stringology::exact::ExactFactorMatch::match);

} /* namespace */
