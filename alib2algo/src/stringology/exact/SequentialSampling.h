#pragma once

#include <alib/measure>

#include <alib/set>
#include <alib/vector>

#include <stringology/common/CommonAlgorithm.h>

#include <string/LinearString.h>
#include <string/properties/PeriodicPrefix.h>

namespace stringology::exact {

namespace {

template <class SymbolType>
ext::set<unsigned> SimpleTextSearching(const string::LinearString<SymbolType>& subject, const string::LinearString<SymbolType>& pattern)
{
    ext::set<unsigned> occ;
    const auto& text = subject.getContent();
    const auto& pat = pattern.getContent();
    size_t n = text.size();
    size_t m = pat.size();

    size_t i = 0;
    while (i + m <= n) {
        size_t j = 0;
        while (j < m && text[i + j] == pat[j])
            ++j;

        if (j == m)
            occ.insert(i);

        i += common::div_up(j + 1, 2);
    }
    return occ;
}

// find first occurence i and return it, or text length
template <class SymbolType>
size_t SimpleTextSearchingFirst(const ext::vector<SymbolType>& text, const ext::vector<SymbolType>& pat, size_t start)
{
    size_t n = text.size();
    size_t m = pat.size();

    size_t i = start;
    while (i + m <= n) {
        size_t j = 0;
        while (j < m && text[i + j] == pat[j])
            ++j;

        if (j == m)
            return i;

        i += common::div_up(j + 1, 2);
    }
    return n;
}

template <class SymbolType>
ext::set<unsigned> SeqSampling(const string::LinearString<SymbolType>& subject, const string::LinearString<SymbolType>& pattern, size_t p, size_t q)
{
    ext::set<unsigned> occ;
    const auto& text = subject.getContent();
    const auto& pat = pattern.getContent();
    size_t n = text.size();
    size_t m = pat.size();

    size_t i = 0;
    while (i + m <= n) {
        if (text[i + p] != pat[p] || text[i + q] != pat[q])
            ++i;
        else {
            size_t j = 0;
            while (j < m && text[i + j] == pat[j])
                ++j;

            if (j == m)
                occ.insert(i);
            if (j + 1 < q)
                i += p;
            else
                i += common::div_up(j + 1, 2);
        }
    }
    return occ;
}

// finds the first occurence and return it
template <class SymbolType>
size_t SeqSamplingFirst(const ext::vector<SymbolType>& text, const ext::vector<SymbolType>& pat, size_t p, size_t q, size_t start)
{
    size_t n = text.size();
    size_t m = pat.size();

    size_t i = start;
    while (i <= n - m) {
        if (text[i + p] != pat[p] || text[i + q] != pat[q])
            ++i;
        else {
            size_t j = 0;
            while (j < m && text[i + j] == pat[j])
                ++j;

            if (j == m)
                return i;
            if (j < q - 1)
                i += p;
            else
                i += common::div_up(j + 1, 2);
        }
    }
    return n;
}

template <class SymbolType>
ext::set<unsigned> Mix(const string::LinearString<SymbolType>& subject, const string::LinearString<SymbolType>& pattern, const size_t& per)
{
    ext::set<unsigned> occ;
    const auto& text = subject.getContent();
    const auto& pat = pattern.getContent();
    size_t n = text.size();
    size_t m = pat.size();

    const ext::vector vv_ = ext::vector<SymbolType>(pat.begin(), pat.begin() + 2 * per - 1);
    size_t periodic_prefix;
    size_t period;
    ext::tie(periodic_prefix, period) = string::properties::PeriodicPrefix::construct(string::LinearString(vv_));

    size_t i = 0;
    while (i + m <= n) {
        if (period == vv_.size()) {
            i = SimpleTextSearchingFirst(text, vv_, i);
        } else {
            i = SeqSamplingFirst(text, vv_, periodic_prefix - period, periodic_prefix, i);
        }
        if (i == n || i + m > n)
            return occ;
        size_t j = 2 * per - 1;
        while (j < m && text[i + j] == pat[j])
            ++j;
        if (j == m) {
            occ.insert(i);
            i += per;
        } else {
            i += std::max<size_t>(j - per, 1);
        }
    }
    return occ;
}

} // namespace

/**
 * Implementation of the SequentialSampling algorithm from article “Constant-space string matching with smaller number of comparisons: sequential sampling
 * by Leszek Gnasieniec and Wojciech Plandowski and Wojciech Rytter
 */
class SequentialSampling {
public:
    /**
     * Search for pattern in linear string.
     * @return set set of occurences
     */
    template <class SymbolType>
    static ext::set<unsigned> match(const string::LinearString<SymbolType>& subject, const string::LinearString<SymbolType>& pattern);
};

template <class SymbolType>
ext::set<unsigned> SequentialSampling::match(const string::LinearString<SymbolType>& subject, const string::LinearString<SymbolType>& pattern)
{
    size_t periodic_prefix;
    size_t period;
    measurements::start("Preprocess", measurements::Type::PREPROCESS);
    ext::tie(periodic_prefix, period) = string::properties::PeriodicPrefix::construct(pattern);
    measurements::end();

    measurements::start("Algorithm", measurements::Type::ALGORITHM);
    ext::set<unsigned> res;
    if (period == pattern.getContent().size()) {
        res = SimpleTextSearching(subject, pattern);
    } else if (periodic_prefix < pattern.getContent().size()) {
        res = SeqSampling(subject, pattern, periodic_prefix - period, periodic_prefix);
    } else {
        res = Mix(subject, pattern, period);
    }

    measurements::end();
    return res;
}

} /* namespace stringology::exact */
