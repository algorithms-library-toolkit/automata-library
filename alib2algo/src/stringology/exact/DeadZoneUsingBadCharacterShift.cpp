#include <registration/AlgoRegistration.hpp>
#include "DeadZoneUsingBadCharacterShift.h"

namespace {

auto DeadZoneUsingBadCharacterShiftLinearStringLinearString = registration::AbstractRegister<stringology::exact::DeadZoneUsingBadCharacterShift, ext::set<unsigned>, const string::LinearString<>&, const string::LinearString<>&>(stringology::exact::DeadZoneUsingBadCharacterShift::match);

} /* namespace */
