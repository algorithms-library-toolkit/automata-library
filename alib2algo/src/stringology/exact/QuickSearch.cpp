#include <registration/AlgoRegistration.hpp>
#include "QuickSearch.h"

namespace {

auto QuickSearchLinearString = registration::AbstractRegister<stringology::exact::QuickSearch, ext::set<unsigned>, const string::LinearString<>&, const string::LinearString<>&>(stringology::exact::QuickSearch::match);

} /* namespace */
