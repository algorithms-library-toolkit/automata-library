#include <registration/AlgoRegistration.hpp>
#include "BoyerMooreHorspool.h"

namespace {

auto BoyerMooreHorspoolLinearStringLinearString = registration::AbstractRegister<stringology::exact::BoyerMooreHorspool, ext::set<unsigned>, const string::LinearString<>&, const string::LinearString<>&>(stringology::exact::BoyerMooreHorspool::match);

} /* namespace */
