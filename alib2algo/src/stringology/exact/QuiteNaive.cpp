#include <registration/AlgoRegistration.hpp>
#include "QuiteNaive.h"

namespace {

auto QuiteNaive = registration::AbstractRegister<stringology::exact::QuiteNaive, ext::set<unsigned>, const string::LinearString<>&, const string::LinearString<>&>(stringology::exact::QuiteNaive::match);

} /* namespace */
