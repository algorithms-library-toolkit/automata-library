#include <registration/AlgoRegistration.hpp>
#include "BoyerMoore.h"

namespace {

auto BoyerMooreLinearStringLinearString = registration::AbstractRegister<stringology::exact::BoyerMoore, ext::set<unsigned>, const string::LinearString<>&, const string::LinearString<>&>(stringology::exact::BoyerMoore::match);

} /* namespace */
