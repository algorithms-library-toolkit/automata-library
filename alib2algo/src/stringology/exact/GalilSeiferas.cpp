#include <registration/AlgoRegistration.hpp>
#include "GalilSeiferas.h"

namespace {

auto GalilSeiferas = registration::AbstractRegister<stringology::exact::GalilSeiferas, ext::set<unsigned>, const string::LinearString<>&, const string::LinearString<>&>(stringology::exact::GalilSeiferas::match);

} /* namespace */
