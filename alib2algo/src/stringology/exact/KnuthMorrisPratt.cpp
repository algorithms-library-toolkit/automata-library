#include <registration/AlgoRegistration.hpp>
#include "KnuthMorrisPratt.h"

namespace {

auto KnuthMorrisPratt = registration::AbstractRegister<stringology::exact::KnuthMorrisPratt, ext::set<unsigned>, const string::LinearString<>&, const string::LinearString<>&>(stringology::exact::KnuthMorrisPratt::match);

} /* namespace */
