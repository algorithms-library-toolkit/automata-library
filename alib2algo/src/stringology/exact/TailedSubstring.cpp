#include <registration/AlgoRegistration.hpp>
#include "TailedSubstring.h"

namespace {

auto TailedSubstring = registration::AbstractRegister<stringology::exact::TailedSubstring, ext::set<unsigned>, const string::LinearString<>&, const string::LinearString<>&>(stringology::exact::TailedSubstring::match);

} /* namespace */
