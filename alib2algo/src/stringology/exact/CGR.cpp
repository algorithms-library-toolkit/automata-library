#include <registration/AlgoRegistration.hpp>
#include "CGR.h"

namespace {

auto CGR = registration::AbstractRegister<stringology::exact::CGR, ext::set<unsigned>, const string::LinearString<>&, const string::LinearString<>&>(stringology::exact::CGR::match);

} /* namespace */
