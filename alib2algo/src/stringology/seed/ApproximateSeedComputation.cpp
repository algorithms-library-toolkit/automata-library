#include <registration/AlgoRegistration.hpp>
#include "ApproximateSeedComputation.h"


namespace stringology::seed {
auto ApproximateSeedsLinearString = registration::AbstractRegister<ApproximateSeedComputation, ext::set<ext::pair<string::LinearString<DefaultSymbolType>, unsigned>>, const string::LinearString<>&, unsigned>(ApproximateSeedComputation::compute);

}
