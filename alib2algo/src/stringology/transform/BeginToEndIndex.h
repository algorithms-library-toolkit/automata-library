#pragma once

#include <alib/set>

#include <string/LinearString.h>

namespace stringology {

namespace transform {

/**
 */
class BeginToEndIndex {
public:
    /**
     * Transforms a set of occurrences represented by begin indexes to set of occurrences representing by end indexes.
     *
     * \tparam SymbolType the type of symbols in the string
     *
     * \param pattern the pattern that was being searched for
     * \param indexes the original occurrences
     * \return the set of transformed occurences
     */
    template <class SymbolType>
    static ext::set<unsigned> transform(const string::LinearString<SymbolType>& pattern, const ext::set<unsigned>& indexes);
};

template <class SymbolType>
ext::set<unsigned> BeginToEndIndex::transform(const string::LinearString<SymbolType>& pattern, const ext::set<unsigned>& indexes)
{
    ext::set<unsigned> res;

    for (unsigned index : indexes)
        res.insert(index + pattern.getContent().size());

    return res;
}

} /* namespace transform */

} /* namespace stringology */
