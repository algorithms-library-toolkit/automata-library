/*
 * Author: Radovan Cerveny
 */

#include <registration/AlgoRegistration.hpp>
#include "BackwardOracleMatching.h"

namespace {

auto BackwardOracleMatchingLinearStringLinearString = registration::AbstractRegister<stringology::query::BackwardOracleMatching, ext::set<unsigned>, const string::LinearString<>&, const automaton::DFA<>&>(stringology::query::BackwardOracleMatching::match);

} /* namespace */
