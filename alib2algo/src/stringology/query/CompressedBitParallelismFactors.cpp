#include <registration/AlgoRegistration.hpp>
#include "CompressedBitParallelismFactors.h"

namespace {

auto CompressedBitParallelismFactorsLinearString = registration::AbstractRegister<stringology::query::CompressedBitParallelismFactors, ext::set<unsigned>, const indexes::stringology::CompressedBitParallelIndex<>&, const string::LinearString<>&>(stringology::query::CompressedBitParallelismFactors::query);

} /* namespace */
