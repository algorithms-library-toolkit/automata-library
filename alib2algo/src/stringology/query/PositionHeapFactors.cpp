#include <registration/AlgoRegistration.hpp>
#include "PositionHeapFactors.h"

namespace {

auto PositionHeapFactorsLinearString = registration::AbstractRegister<stringology::query::PositionHeapFactors, ext::set<unsigned>, const indexes::stringology::PositionHeap<>&, const string::LinearString<>&>(stringology::query::PositionHeapFactors::query);

} /* namespace */
