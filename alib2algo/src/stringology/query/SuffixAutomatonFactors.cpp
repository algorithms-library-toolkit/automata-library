#include <registration/AlgoRegistration.hpp>
#include "SuffixAutomatonFactors.h"

namespace {

auto SuffixAutomatonFactorsLinearString = registration::AbstractRegister<stringology::query::SuffixAutomatonFactors, ext::set<unsigned>, const indexes::stringology::SuffixAutomaton<>&, const string::LinearString<>&>(stringology::query::SuffixAutomatonFactors::query);

} /* namespace */
