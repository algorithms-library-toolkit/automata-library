#include <registration/AlgoRegistration.hpp>
#include "WideBNDMOccurrences.h"

namespace {

auto wideBNDMOccurrencesLinearString = registration::AbstractRegister<stringology::query::WideBNDMOccurrences, ext::set<unsigned>, const indexes::stringology::BitParallelIndex<DefaultSymbolType>&, const string::LinearString<DefaultSymbolType>&>(stringology::query::WideBNDMOccurrences::query);

} /* namespace */
