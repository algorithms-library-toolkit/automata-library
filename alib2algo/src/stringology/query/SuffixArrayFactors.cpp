#include <registration/AlgoRegistration.hpp>
#include "SuffixArrayFactors.h"

namespace {

auto SuffixArrayFactorsLinearString = registration::AbstractRegister<stringology::query::SuffixArrayFactors, ext::set<unsigned>, const indexes::stringology::SuffixArray<>&, const string::LinearString<>&>(stringology::query::SuffixArrayFactors::query);

} /* namespace */
