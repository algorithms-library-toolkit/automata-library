/*
 * Author: Radovan Cerveny
 */

#include <registration/AlgoRegistration.hpp>
#include "BackwardDAWGMatching.h"

namespace {

auto BackwardDAWGMatchingLinearStringLinearString = registration::AbstractRegister<stringology::query::BackwardDAWGMatching, ext::set<unsigned>, const string::LinearString<>&, const indexes::stringology::SuffixAutomaton<>&>(stringology::query::BackwardDAWGMatching::match);

} /* namespace */
