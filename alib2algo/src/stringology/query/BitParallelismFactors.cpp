#include <registration/AlgoRegistration.hpp>
#include "BitParallelismFactors.h"

namespace {

auto BitParallelismFactorsLinearString = registration::AbstractRegister<stringology::query::BitParallelismFactors, ext::set<unsigned>, const indexes::stringology::BitParallelIndex<>&, const string::LinearString<>&>(stringology::query::BitParallelismFactors::query);

} /* namespace */
