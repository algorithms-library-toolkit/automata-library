#include <registration/AlgoRegistration.hpp>
#include "NyldonFactoring.h"

namespace {

auto nyldonFactoringString = registration::AbstractRegister<stringology::properties::NyldonFactoring, ext::vector<unsigned>, const string::LinearString<>&>(stringology::properties::NyldonFactoring::factorize).setDocumentation("Computes the nyldon factoring of a given nonempty string\n\
\n\
@param string the nonempty string to factorize\n\
@return positions where the string is split to nyldon factors");

} /* namespace */
