#include <registration/AlgoRegistration.hpp>
#include "BackboneLength.h"

namespace {

auto backboneLengthDFA = registration::AbstractRegister<stringology::properties::BackboneLength, unsigned, const automaton::DFA<>&>(stringology::properties::BackboneLength::length);

} /* namespace */
