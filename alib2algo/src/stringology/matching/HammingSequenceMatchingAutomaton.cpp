#include <registration/AlgoRegistration.hpp>
#include "HammingSequenceMatchingAutomaton.h"

namespace {

auto HammingSequenceMatchingAutomatonLinearString = registration::AbstractRegister<stringology::matching::HammingSequenceMatchingAutomaton, automaton::NFA<DefaultSymbolType, ext::pair<unsigned int, unsigned int>>, const string::LinearString<>&, unsigned>(stringology::matching::HammingSequenceMatchingAutomaton::construct);

auto HammingSequenceMatchingAutomatonWildcardStringLinearString = registration::AbstractRegister<stringology::matching::HammingSequenceMatchingAutomaton, automaton::NFA<DefaultSymbolType, ext::pair<unsigned int, unsigned int>>, const string::WildcardLinearString<>&, unsigned>(stringology::matching::HammingSequenceMatchingAutomaton::construct);

} /* namespace */
