#include <registration/AlgoRegistration.hpp>
#include "LevenshteinMatchingAutomaton.h"

namespace {

auto LevenshteinMatchingAutomatonLinearString = registration::AbstractRegister<stringology::matching::LevenshteinMatchingAutomaton, automaton::EpsilonNFA<DefaultSymbolType, ext::pair<unsigned int, unsigned int>>, const string::LinearString<>&, unsigned>(stringology::matching::LevenshteinMatchingAutomaton::construct);

auto LevenshteinMatchingAutomatonWildcardLinearString = registration::AbstractRegister<stringology::matching::LevenshteinMatchingAutomaton, automaton::EpsilonNFA<DefaultSymbolType, ext::pair<unsigned int, unsigned int>>, const string::WildcardLinearString<>&, unsigned>(stringology::matching::LevenshteinMatchingAutomaton::construct);

} /* namespace */
