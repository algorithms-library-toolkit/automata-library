/*
 * Author: Radovan Cerveny
 */

#include <registration/AlgoRegistration.hpp>
#include "OracleMatcherConstruction.h"

namespace {

auto OracleMatcherConstructionLinearString = registration::AbstractRegister<stringology::matching::OracleMatcherConstruction, automaton::DFA<DefaultSymbolType, unsigned>, const string::LinearString<>&>(stringology::matching::OracleMatcherConstruction::construct);

} /* namespace */
