/*
 * Author: Radovan Cerveny
 */

#pragma once

#include <automaton/FSM/DFA.h>
#include <string/LinearString.h>

#include <stringology/indexing/ExactSuffixAutomaton.h>

namespace stringology {

namespace matching {

class OracleMatcherConstruction {
public:
    /**
     * Linear time on-line construction of minimal suffix automaton for given pattern.
     * @return minimal suffix automaton for given pattern.
     */
    template <class SymbolType>
    static automaton::DFA<SymbolType, unsigned> construct(const string::LinearString<SymbolType>& pattern);
};

template <class SymbolType>
automaton::DFA<SymbolType, unsigned> OracleMatcherConstruction::construct(const string::LinearString<SymbolType>& pattern)
{
    auto patternData = pattern.getContent();
    reverse(patternData.begin(), patternData.end());
    string::LinearString<SymbolType> reversedPattern(pattern.getAlphabet(), std::move(patternData));

    return stringology::indexing::ExactSuffixAutomaton::construct(reversedPattern).getAutomaton(); // FIXME this is not oracle automaton
}

} /* namespace matching */

} /* namespace stringology */
