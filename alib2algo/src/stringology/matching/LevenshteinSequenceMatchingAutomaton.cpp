#include <registration/AlgoRegistration.hpp>
#include "LevenshteinSequenceMatchingAutomaton.h"

namespace {

auto LevenshteinSequenceMatchingAutomatonLinearString = registration::AbstractRegister<stringology::matching::LevenshteinSequenceMatchingAutomaton, automaton::EpsilonNFA<DefaultSymbolType, ext::pair<unsigned int, unsigned int>>, const string::LinearString<>&, unsigned>(stringology::matching::LevenshteinSequenceMatchingAutomaton::construct);

auto LevenshteinSequenceMatchingAutomatonWildcardLinearString = registration::AbstractRegister<stringology::matching::LevenshteinSequenceMatchingAutomaton, automaton::EpsilonNFA<DefaultSymbolType, ext::pair<unsigned int, unsigned int>>, const string::WildcardLinearString<>&, unsigned>(stringology::matching::LevenshteinSequenceMatchingAutomaton::construct);

} /* namespace */
