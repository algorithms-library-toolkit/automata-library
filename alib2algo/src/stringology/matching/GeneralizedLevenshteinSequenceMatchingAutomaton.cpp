#include <registration/AlgoRegistration.hpp>
#include "GeneralizedLevenshteinSequenceMatchingAutomaton.h"

namespace {

auto GeneralizedLevenshteinSequenceMatchingAutomatonLinearString = registration::AbstractRegister<stringology::matching::GeneralizedLevenshteinSequenceMatchingAutomaton, automaton::EpsilonNFA<DefaultSymbolType, ext::pair<unsigned int, unsigned int>>, const string::LinearString<>&, unsigned>(stringology::matching::GeneralizedLevenshteinSequenceMatchingAutomaton::construct);

auto GeneralizedLevenshteinSequenceMatchingAutomatonWildcardLinearString = registration::AbstractRegister<stringology::matching::GeneralizedLevenshteinSequenceMatchingAutomaton, automaton::EpsilonNFA<DefaultSymbolType, ext::pair<unsigned int, unsigned int>>, const string::WildcardLinearString<>&, unsigned>(stringology::matching::GeneralizedLevenshteinSequenceMatchingAutomaton::construct);

} /* namespace */
