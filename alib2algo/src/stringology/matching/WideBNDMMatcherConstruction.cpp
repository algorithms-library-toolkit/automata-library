#include <registration/AlgoRegistration.hpp>
#include "WideBNDMMatcherConstruction.h"

namespace {

auto WideBNDMIndexConstructionLinearString = registration::AbstractRegister<stringology::matching::WideBNDMMatcherConstruction, indexes::stringology::BitParallelIndex<DefaultSymbolType>, const string::LinearString<>&>(stringology::matching::WideBNDMMatcherConstruction::construct);

} /* namespace */
