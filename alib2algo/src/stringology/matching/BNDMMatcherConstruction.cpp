#include <registration/AlgoRegistration.hpp>
#include "BNDMMatcherConstruction.h"

namespace {

auto BNDMIndexConstructionLinearString = registration::AbstractRegister<stringology::matching::BNDMMatcherConstruction, indexes::stringology::BitSetIndex<>, const string::LinearString<>&>(stringology::matching::BNDMMatcherConstruction::construct);

} /* namespace */
