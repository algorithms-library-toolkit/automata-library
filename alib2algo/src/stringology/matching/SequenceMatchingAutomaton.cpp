#include <registration/AlgoRegistration.hpp>
#include "SequenceMatchingAutomaton.h"

namespace {

auto SequenceMatchingAutomatonLinearString = registration::AbstractRegister<stringology::matching::SequenceMatchingAutomaton, automaton::NFA<DefaultSymbolType, unsigned>, const string::LinearString<>&>(stringology::matching::SequenceMatchingAutomaton::construct);

} /* namespace */
