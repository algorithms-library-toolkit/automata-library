#include <registration/AlgoRegistration.hpp>
#include "GeneralizedLevenshteinMatchingAutomaton.h"

namespace {

auto GeneralizedLevenshteinMatchingAutomatonLinearString = registration::AbstractRegister<stringology::matching::GeneralizedLevenshteinMatchingAutomaton, automaton::EpsilonNFA<DefaultSymbolType, ext::pair<unsigned int, unsigned int>>, const string::LinearString<>&, unsigned int>(stringology::matching::GeneralizedLevenshteinMatchingAutomaton::construct);

auto GeneralizedLevenshteinMatchingAutomatonWildcardLinearString = registration::AbstractRegister<stringology::matching::GeneralizedLevenshteinMatchingAutomaton, automaton::EpsilonNFA<DefaultSymbolType, ext::pair<unsigned int, unsigned int>>, const string::WildcardLinearString<>&, unsigned int>(stringology::matching::GeneralizedLevenshteinMatchingAutomaton::construct);

} /* namespace */
