/*
 * Author: Radovan Cerveny
 */

#include <registration/AlgoRegistration.hpp>
#include "DAWGMatcherConstruction.h"

namespace {

auto DAWGMatcherConstructionLinearString = registration::AbstractRegister<stringology::matching::DAWGMatcherConstruction, indexes::stringology::SuffixAutomaton<DefaultSymbolType>, const string::LinearString<>&>(stringology::matching::DAWGMatcherConstruction::construct);

} /* namespace */
