/*
 * Author: Radovan Cerveny
 */

#include <registration/AlgoRegistration.hpp>
#include "NaiveDAWGMatcherConstruction.h"

namespace {

auto NaiveDAWGMatcherConstructionLinearString = registration::AbstractRegister<stringology::matching::NaiveDAWGMatcherConstruction, indexes::stringology::SuffixAutomaton<>, const string::LinearString<>&>(stringology::matching::NaiveDAWGMatcherConstruction::naiveConstruct);

} /* namespace */
