#include <registration/AlgoRegistration.hpp>
#include "LevenshteinDynamicProgramming.h"

namespace {

auto LevenshteinDynamicProgrammingLinearString = registration::AbstractRegister<stringology::simulations::LevenshteinDynamicProgramming, ext::set<unsigned>, const string::LinearString<>&, const string::LinearString<>&, unsigned>(stringology::simulations::LevenshteinDynamicProgramming::search);

} /* namespace */
