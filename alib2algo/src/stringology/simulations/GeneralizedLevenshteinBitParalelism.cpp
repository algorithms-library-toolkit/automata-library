#include <registration/AlgoRegistration.hpp>
#include "GeneralizedLevenshteinBitParalelism.h"

namespace {

auto GeneralizedLevenshteinBitParalelismLinearString = registration::AbstractRegister<stringology::simulations::GeneralizedLevenshteinBitParalelism, ext::set<unsigned>, const string::LinearString<>&, const string::LinearString<>&, unsigned>(stringology::simulations::GeneralizedLevenshteinBitParalelism::search);

} /* namespace */
