#include <registration/AlgoRegistration.hpp>
#include "HammingDynamicProgramming.h"

namespace {

auto HammingDynamicProgrammingLinearString = registration::AbstractRegister<stringology::simulations::HammingDynamicProgramming, ext::set<unsigned>, const string::LinearString<>&, const string::LinearString<>&, unsigned>(stringology::simulations::HammingDynamicProgramming::search);

} /* namespace */
