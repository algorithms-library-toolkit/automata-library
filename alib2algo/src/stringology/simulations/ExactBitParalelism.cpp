#include <registration/AlgoRegistration.hpp>
#include "ExactBitParalelism.h"

namespace {

auto ExactBitParalelismLinearString = registration::AbstractRegister<stringology::simulations::ExactBitParalelism, ext::set<unsigned>, const string::LinearString<>&, const string::LinearString<>&>(stringology::simulations::ExactBitParalelism::search);

} /* namespace */
