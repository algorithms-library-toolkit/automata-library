#include <registration/AlgoRegistration.hpp>
#include "ExactSubsequenceAutomaton.h"

namespace {

auto ExactSubsequenceAutomatonLinearString = registration::AbstractRegister<stringology::indexing::ExactSubsequenceAutomaton, automaton::DFA<DefaultSymbolType, unsigned>, const string::LinearString<>&>(stringology::indexing::ExactSubsequenceAutomaton::construct);

} /* namespace */
