#include <registration/AlgoRegistration.hpp>
#include "ExactMultiNondeterministicSubsequenceAutomaton.h"

namespace {

auto ExactMultiNondeterministicSubsequenceAutomatonLinearString = registration::AbstractRegister<stringology::indexing::ExactMultiNondeterministicSubsequenceAutomaton, automaton::EpsilonNFA<DefaultSymbolType, ext::pair<unsigned, unsigned>>, const ext::set<string::LinearString<>>&>(stringology::indexing::ExactMultiNondeterministicSubsequenceAutomaton::construct);

} /* namespace */
