#include "SuffixTrieNaive.h"

#include <registration/AlgoRegistration.hpp>
#include <string/LinearString.h>

namespace {

auto suffixTrieNaiveLinearString = registration::AbstractRegister<stringology::indexing::SuffixTrieNaive, indexes::stringology::SuffixTrie<DefaultSymbolType>, const string::LinearString<>&>(stringology::indexing::SuffixTrieNaive::construct);

} /* namespace */
