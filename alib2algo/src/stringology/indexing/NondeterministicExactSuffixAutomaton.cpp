/*
 * Author: Radovan Cerveny
 */

#include <registration/AlgoRegistration.hpp>
#include "NondeterministicExactSuffixAutomaton.h"

namespace {

auto SuffixAutomatonNondeterministicLinearString = registration::AbstractRegister<stringology::indexing::NondeterministicExactSuffixAutomaton, automaton::NFA<DefaultSymbolType, unsigned>, const string::LinearString<>&>(stringology::indexing::NondeterministicExactSuffixAutomaton::construct);

} /* namespace */
