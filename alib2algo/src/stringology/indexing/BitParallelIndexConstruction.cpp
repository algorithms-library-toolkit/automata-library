#include <registration/AlgoRegistration.hpp>
#include "BitParallelIndexConstruction.h"

namespace {

auto bitParallelIndexConstructionLinearString = registration::AbstractRegister<stringology::indexing::BitParallelIndexConstruction, indexes::stringology::BitParallelIndex<DefaultSymbolType>, const string::LinearString<>&>(stringology::indexing::BitParallelIndexConstruction::construct);

} /* namespace */
