#pragma once

#include <automaton/FSM/EpsilonNFA.h>
#include <string/LinearString.h>

namespace stringology {

namespace indexing {

class NondeterministicExactFactorAutomaton {
public:
    /**
     * Performs conversion.
     * @return left regular grammar equivalent to source automaton.
     */
    template <class SymbolType>
    static automaton::EpsilonNFA<SymbolType, unsigned> construct(const string::LinearString<SymbolType>& text);
};

template <class SymbolType>
automaton::EpsilonNFA<SymbolType, unsigned> NondeterministicExactFactorAutomaton::construct(const string::LinearString<SymbolType>& text)
{
    automaton::EpsilonNFA<SymbolType, unsigned> res(0);
    res.addFinalState(0);
    res.setInputAlphabet(text.getAlphabet());

    unsigned i = 0;
    for (const SymbolType& symbol : text.getContent()) {
        res.addState(++i);
        res.addFinalState(i);
        res.addTransition(i - 1, symbol, i);
        res.addTransition(0, i);
    }

    return res;
}

} /* namespace indexing */

} /* namespace stringology */
