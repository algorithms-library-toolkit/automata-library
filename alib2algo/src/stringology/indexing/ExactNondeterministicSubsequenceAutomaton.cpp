#include <registration/AlgoRegistration.hpp>
#include "ExactNondeterministicSubsequenceAutomaton.h"

namespace {

auto ExactNondeterministicSubsequenceAutomatonLinearString = registration::AbstractRegister<stringology::indexing::ExactNondeterministicSubsequenceAutomaton, automaton::EpsilonNFA<DefaultSymbolType, unsigned>, const string::LinearString<>&>(stringology::indexing::ExactNondeterministicSubsequenceAutomaton::construct);

} /* namespace */
