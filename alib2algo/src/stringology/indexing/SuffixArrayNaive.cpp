#include <registration/AlgoRegistration.hpp>
#include "SuffixArrayNaive.h"

namespace {

auto suffixArrayNaiveLinearString = registration::AbstractRegister<stringology::indexing::SuffixArrayNaive, indexes::stringology::SuffixArray<>, const string::LinearString<>&>(stringology::indexing::SuffixArrayNaive::construct);

} /* namespace */
