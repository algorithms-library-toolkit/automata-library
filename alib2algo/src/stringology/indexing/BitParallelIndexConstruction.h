#pragma once

#include <exception/CommonException.h>
#include <indexes/stringology/BitParallelIndex.h>
#include <string/LinearString.h>

namespace stringology {

namespace indexing {

/**
 * Constructs a bit parallel index for given string.
 *
 */

class BitParallelIndexConstruction {
public:
    /**
     * Creates suffix trie
     * @param string string to construct suffix trie for
     * @return automaton
     */
    template <class SymbolType>
    static indexes::stringology::BitParallelIndex<SymbolType> construct(const string::LinearString<SymbolType>& w);
};

template <class SymbolType>
indexes::stringology::BitParallelIndex<SymbolType> BitParallelIndexConstruction::construct(const string::LinearString<SymbolType>& w)
{
    ext::map<SymbolType, ext::vector<bool>> res;
    for (const SymbolType& symbol : w.getAlphabet())
        res[symbol].resize(w.getContent().size());

    for (unsigned i = 0; i < w.getContent().size(); ++i)
        res[w.getContent()[i]][i] = true;

    return indexes::stringology::BitParallelIndex<SymbolType>(w.getAlphabet(), std::move(res));
}

} /* namespace indexing */

} /* namespace stringology */
