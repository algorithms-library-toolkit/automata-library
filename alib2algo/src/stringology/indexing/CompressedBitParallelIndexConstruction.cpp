#include <registration/AlgoRegistration.hpp>
#include "CompressedBitParallelIndexConstruction.h"

namespace {

auto compressedCompressedBitParallelIndexConstructionLinearString = registration::AbstractRegister<stringology::indexing::CompressedBitParallelIndexConstruction, indexes::stringology::CompressedBitParallelIndex<DefaultSymbolType>, const string::LinearString<>&>(stringology::indexing::CompressedBitParallelIndexConstruction::construct);

} /* namespace */
