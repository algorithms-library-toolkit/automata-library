/*
 * Author: Radovan Cerveny
 */

#include <registration/AlgoRegistration.hpp>
#include "ExactFactorOracleAutomaton.h"

namespace {

auto FactorOracleAutomatonLinearString = registration::AbstractRegister<stringology::indexing::ExactFactorOracleAutomaton, indexes::stringology::FactorOracleAutomaton<>, const string::LinearString<>&>(stringology::indexing::ExactFactorOracleAutomaton::construct);

} /* namespace */
