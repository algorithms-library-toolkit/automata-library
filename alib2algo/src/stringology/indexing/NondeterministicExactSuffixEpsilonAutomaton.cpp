/*
 * Author: Radovan Cerveny
 */

#include <registration/AlgoRegistration.hpp>
#include "NondeterministicExactSuffixEpsilonAutomaton.h"

namespace {

auto SuffixAutomatonNondeterministicLinearString = registration::AbstractRegister<stringology::indexing::NondeterministicExactSuffixEpsilonAutomaton, automaton::EpsilonNFA<DefaultSymbolType, unsigned>, const string::LinearString<>&>(stringology::indexing::NondeterministicExactSuffixEpsilonAutomaton::construct);

} /* namespace */
