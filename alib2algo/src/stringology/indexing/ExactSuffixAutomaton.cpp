/*
 * Author: Radovan Cerveny
 */

#include <registration/AlgoRegistration.hpp>
#include "ExactSuffixAutomaton.h"

namespace {

auto SuffixAutomatonLinearString = registration::AbstractRegister<stringology::indexing::ExactSuffixAutomaton, indexes::stringology::SuffixAutomaton<>, const string::LinearString<>&>(stringology::indexing::ExactSuffixAutomaton::construct);

} /* namespace */
