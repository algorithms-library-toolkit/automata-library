#include <registration/AlgoRegistration.hpp>
#include "NondeterministicApproximateSuffixAutomatonForHammingDistance.h"

namespace stringology::indexing {

auto NDApproximateSuffixAutomaton = registration::AbstractRegister<NondeterministicApproximateSuffixAutomatonForHammingDistance, automaton::NFA<DefaultSymbolType, ext::pair<unsigned, unsigned>>, const string::LinearString<>&, unsigned>(NondeterministicApproximateSuffixAutomatonForHammingDistance::construct);

} /* namespace stringology::indexing */
