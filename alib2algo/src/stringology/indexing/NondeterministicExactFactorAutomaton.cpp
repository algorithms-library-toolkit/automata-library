#include <registration/AlgoRegistration.hpp>
#include "NondeterministicExactFactorAutomaton.h"

namespace {

auto ExactFactorAutomatonLinearString = registration::AbstractRegister<stringology::indexing::NondeterministicExactFactorAutomaton, automaton::EpsilonNFA<DefaultSymbolType, unsigned>, const string::LinearString<>&>(stringology::indexing::NondeterministicExactFactorAutomaton::construct);

} /* namespace */
