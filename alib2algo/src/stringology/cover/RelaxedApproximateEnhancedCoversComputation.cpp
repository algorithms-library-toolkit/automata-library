#include <registration/AlgoRegistration.hpp>
#include "RelaxedApproximateEnhancedCoversComputation.h"

namespace stringology::cover {
auto RelaxedApproximateEnhancedCoversString = registration::AbstractRegister<RelaxedApproximateEnhancedCoversComputation, ext::set<string::LinearString<DefaultSymbolType>>, const string::LinearString<>&, unsigned>(RelaxedApproximateEnhancedCoversComputation::compute);
} /* namespace stringology::cover */
