#include <registration/AlgoRegistration.hpp>
#include "RestrictedApproximateCoversComputation.h"

namespace stringology::cover {

auto RestictedApproximateCoversLinearString = registration::AbstractRegister<RestrictedApproximateCoversComputation, ext::set<ext::pair<string::LinearString<DefaultSymbolType>, unsigned>>, const string::LinearString<>&, unsigned>(RestrictedApproximateCoversComputation::compute);

} /* namespace stringology::cover */
