#include <registration/AlgoRegistration.hpp>
#include "ExactCoversComputation.h"

namespace stringology::cover {

auto ExactCoversLinearString = registration::AbstractRegister<ExactCoversComputation, ext::set<string::LinearString<DefaultSymbolType>>, const string::LinearString<>&>(ExactCoversComputation::compute);

} /* namespace stringology::cover */
