#include <registration/AlgoRegistration.hpp>
#include "ApproximateEnhancedCoversComputation.h"

namespace stringology::cover {
auto ApproximateEnhancedCoversString = registration::AbstractRegister<ApproximateEnhancedCoversComputation, ext::set<string::LinearString<DefaultSymbolType>>, const string::LinearString<>&, unsigned>(ApproximateEnhancedCoversComputation::compute);
} /* namespace stringology::cover */
