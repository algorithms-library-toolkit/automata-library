#include <registration/AlgoRegistration.hpp>
#include "LZ77Compression.h"

namespace {

auto LZ77CompressionLinearString = registration::AbstractRegister<stringology::compression::LZ77Compression, ext::vector<ext::tuple<unsigned, unsigned, DefaultSymbolType>>, const string::LinearString<>&, unsigned, unsigned>(stringology::compression::LZ77Compression::compress);

} /* namespace */
