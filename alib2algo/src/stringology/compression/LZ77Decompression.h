#pragma once

#include <alib/set>
#include <alib/tuple>
#include <alib/vector>

#include <string/LinearString.h>

namespace stringology {

namespace compression {

class LZ77Decompression {
public:
    template <class SymbolType>
    static string::LinearString<SymbolType> decompress(const ext::vector<ext::tuple<unsigned, unsigned, SymbolType>>& input);
};

// Main method that handle decompress
template <class SymbolType>
string::LinearString<SymbolType> LZ77Decompression::decompress(const ext::vector<ext::tuple<unsigned, unsigned, SymbolType>>& input)
{

    string::LinearString<SymbolType> output;

    for (unsigned i = 0; i < input.size(); i++) {
        for (unsigned j = 0; j < std::get<1>(input[i]); j++) {
            output.appendSymbol(output.getContent()[output.getContent().size() - std::get<0>(input[i])]);
        }

        output.extendAlphabet(ext::set<SymbolType>{std::get<2>(input[i])});
        output.appendSymbol(std::get<2>(input[i]));
    }

    return output;
}

} /* namespace compression */

} /* namespace stringology */
