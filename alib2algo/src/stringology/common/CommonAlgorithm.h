#pragma once

namespace stringology::common {

inline size_t div_up(size_t x, size_t y)
{
    return x / y + (x % y == 1);
}

} /* namespace stringology::common */
