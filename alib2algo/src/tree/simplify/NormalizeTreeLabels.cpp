#include <registration/AlgoRegistration.hpp>
#include "NormalizeTreeLabels.h"

namespace {

auto NormalizeTreeLabelsRankedTree = registration::AbstractRegister<tree::simplify::NormalizeTreeLabels, tree::RankedTree<unsigned>, const tree::RankedTree<>&>(tree::simplify::NormalizeTreeLabels::normalize);

} /* namespace */
