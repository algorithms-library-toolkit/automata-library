#include "RandomTreeFactory.h"

#include <cmath>
#include <cstdlib>
#include <ctime>

#include <ext/algorithm>
#include <ext/iostream>
#include <ext/random>

#include <alib/map>
#include <alib/set>
#include <alib/vector>

#include <common/ranked_symbol.hpp>

#include <alphabet/NonlinearVariable.h>
#include <alphabet/Wildcard.h>
#include <exception/CommonException.h>

#include <registration/AlgoRegistration.hpp>

#include <global/GlobalData.h>

namespace tree::generate {

struct Node {
    std::optional<DefaultSymbolType> symbol;
    int depth = 0;
    Node* right;
    Node* child = nullptr;
    size_t rank = 0;

    Node() = default;
    Node(Node* parent)
        : depth(parent->depth + 1)
    {
        if (parent->child == nullptr) {
            parent->child = this;
            right = this;
        } else {
            right = parent->child->right;
            parent->child->right = this;
        }

        parent->rank++;
    }

    ~Node()
    {
        if (child == nullptr)
            return;

        Node* ch = child;

        do {
            Node* tmp = ch->right;
            delete ch;
            ch = tmp;
        } while (ch != child);
    }

    void rotateLeftBranch()
    {
        if (child != nullptr)
            child->rotateLeftBranch();

        if (rank > 1) {
            size_t angle = ext::random_devices::semirandom() % rank;
            Node* newChild = child;

            for (size_t i = 0; i < angle; ++i)
                newChild = newChild->right;

            child = newChild;
        }
    }

    void generateUnrankedSymbols(const ext::vector<DefaultSymbolType>& alphabet)
    {
        symbol = alphabet[ext::random_devices::semirandom() % alphabet.size()];
        Node* nextChild = child;

        for (size_t i = 0; i < rank; i++) {
            nextChild->generateUnrankedSymbols(alphabet);
            nextChild = nextChild->right;
        }
    }

    void generateRankedSymbols(const ext::map<size_t, ext::vector<DefaultSymbolType>>& rankedAlphabet)
    {
        const ext::vector<DefaultSymbolType>& alphabet = rankedAlphabet.at(rank);

        symbol = alphabet[ext::random_devices::semirandom() % alphabet.size()];
        Node* nextChild = child;

        for (size_t i = 0; i < rank; i++) {
            nextChild->generateRankedSymbols(rankedAlphabet);
            nextChild = nextChild->right;
        }
    }

    void fillRanks(ext::map<size_t, ext::vector<DefaultSymbolType>>& rankedAlphabet)
    {
        rankedAlphabet[rank];
        Node* nextChild = child;

        for (size_t i = 0; i < rank; i++) {
            nextChild->fillRanks(rankedAlphabet);
            nextChild = nextChild->right;
        }
    }

    ext::tree<DefaultSymbolType> createUnrankedNode()
    {
        ext::vector<ext::tree<DefaultSymbolType>> children;
        Node* nextChild = child;

        for (size_t i = 0; i < rank; i++) {
            children.emplace_back(nextChild->createUnrankedNode());
            nextChild = nextChild->right;
        }

        return ext::tree<DefaultSymbolType>(symbol.value(), std::move(children));
    }

    ext::tree<DefaultSymbolType> createUnrankedPatternNode()
    {
        if (rank == 0) {
            if (ext::random_devices::semirandom() % 2 == 0)
                return ext::tree<DefaultSymbolType>(alphabet::Wildcard::instance<DefaultSymbolType>(), {});
            else
                return ext::tree<DefaultSymbolType>(alphabet::Gap::instance<DefaultSymbolType>(), {});
        } else {
            ext::vector<ext::tree<DefaultSymbolType>> children;
            Node* nextChild = child;

            for (size_t i = 0; i < rank; i++) {
                children.emplace_back(nextChild->createUnrankedPatternNode());
                nextChild = nextChild->right;
            }

            return ext::tree<DefaultSymbolType>(symbol.value(), std::move(children));
        }
    }

    ext::tree<DefaultSymbolType> createUnrankedExtendedPatternNode(const ext::set<DefaultSymbolType>& selectedNodeWildcards)
    {
        if (rank == 0) {
            if (ext::random_devices::semirandom() % 2 == 0)
                return ext::tree<DefaultSymbolType>(alphabet::Wildcard::instance<DefaultSymbolType>(), {});
            else
                return ext::tree<DefaultSymbolType>(alphabet::Gap::instance<DefaultSymbolType>(), {});
        } else {
            ext::vector<ext::tree<DefaultSymbolType>> children;
            Node* nextChild = child;

            for (size_t i = 0; i < rank; i++) {
                children.emplace_back(nextChild->createUnrankedExtendedPatternNode(selectedNodeWildcards));
                nextChild = nextChild->right;
            }

            DefaultSymbolType label{symbol.value()};
            if (selectedNodeWildcards.contains(label))
                return ext::tree<DefaultSymbolType>(alphabet::NodeWildcard::template instance<DefaultSymbolType>(), std::move(children));
            else
                return ext::tree<DefaultSymbolType>(std::move(label), std::move(children));
        }
    }

    ext::tree<common::ranked_symbol<>> createRankedNode()
    {
        ext::vector<ext::tree<common::ranked_symbol<>>> children;
        Node* nextChild = child;

        for (size_t i = 0; i < rank; i++) {
            children.emplace_back(nextChild->createRankedNode());
            nextChild = nextChild->right;
        }

        return ext::tree<common::ranked_symbol<>>(common::ranked_symbol<>(symbol.value(), rank), std::move(children));
    }

    ext::tree<common::ranked_symbol<>> createRankedPatternNode()
    {
        if (rank == 0) {
            return ext::tree<common::ranked_symbol<>>(common::ranked_symbol<>(alphabet::Wildcard::instance<DefaultSymbolType>(), 0), {});
        } else {
            ext::vector<ext::tree<common::ranked_symbol<>>> children;
            Node* nextChild = child;

            for (size_t i = 0; i < rank; i++) {
                children.emplace_back(nextChild->createRankedPatternNode());
                nextChild = nextChild->right;
            }

            return ext::tree<common::ranked_symbol<>>(common::ranked_symbol<>(symbol.value(), rank), std::move(children));
        }
    }

    ext::tree<common::ranked_symbol<>> createRankedExtendedPatternNode(const ext::set<common::ranked_symbol<>>& selectedNodeWildcards)
    {
        if (rank == 0) {
            return ext::tree<common::ranked_symbol<>>(common::ranked_symbol<>(alphabet::Wildcard::instance<DefaultSymbolType>(), 0), {});
        } else {
            ext::vector<ext::tree<common::ranked_symbol<>>> children;
            Node* nextChild = child;

            for (size_t i = 0; i < rank; i++) {
                children.emplace_back(nextChild->createRankedExtendedPatternNode(selectedNodeWildcards));
                nextChild = nextChild->right;
            }

            common::ranked_symbol<> label{symbol.value(), rank};
            if (selectedNodeWildcards.contains(label))
                return ext::tree<common::ranked_symbol<>>(common::ranked_symbol<>(alphabet::NodeWildcard::template instance<DefaultSymbolType>(), rank), std::move(children));
            else
                return ext::tree<common::ranked_symbol<>>(std::move(label), std::move(children));
        }
    }

    ext::tree<common::ranked_symbol<>> createRankedNonlinearPatternNode(bool singleNonlinearVariable)
    {
        if (rank == 0) {
            if (singleNonlinearVariable)
                return ext::tree<common::ranked_symbol<>>(common::ranked_symbol<>(object::ObjectFactory<>::construct(alphabet::NonlinearVariable<>(object::ObjectFactory<>::construct("A"))), 0), {});
            else
                return ext::tree<common::ranked_symbol<>>(common::ranked_symbol<>(object::ObjectFactory<>::construct(alphabet::NonlinearVariable<>(symbol.value())), 0), {});
        } else {
            ext::vector<ext::tree<common::ranked_symbol<>>> children;
            Node* nextChild = child;

            for (size_t i = 0; i < rank; i++) {
                children.emplace_back(nextChild->createRankedNonlinearPatternNode(singleNonlinearVariable));
                nextChild = nextChild->right;
            }

            return ext::tree<common::ranked_symbol<>>(common::ranked_symbol<>(symbol.value(), rank), std::move(children));
        }
    }

    void nicePrint(ext::ostream& os = common::Streams::out, const std::string& prefix = "", const bool last = true) const
    {
        os << prefix;

        std::string nextPrefix(prefix);

        if (last) {
            os << "\\-";
            nextPrefix += "  ";
        } else {
            os << "|-";
            nextPrefix += "| ";
        }

        os << symbol.value() << " (" << rank << ")" << std::endl;

        Node* nextChild = child;

        for (size_t i = 0; i < rank; i++) {
            // os << nextPrefix << "|" << std::endl;
            nextChild->nicePrint(os, nextPrefix, i == rank - 1);
            nextChild = nextChild->right;
        }
    }
};

namespace {
constexpr unsigned ENGLISH_ALPHABET_SIZE = 26;
}

ext::vector<DefaultSymbolType> generateUnrankedAlphabet(size_t maxAlphabetSize, bool randomizedAlphabet)
{
    if (maxAlphabetSize > ENGLISH_ALPHABET_SIZE)
        throw exception::CommonException("maximum alphabet size is 26");

    ext::vector<DefaultSymbolType> symbols;
    symbols.reserve(ENGLISH_ALPHABET_SIZE);

    for (unsigned i = 0; i < ENGLISH_ALPHABET_SIZE; i++)
        symbols.push_back(object::ObjectFactory<>::construct(i + 'a'));

    if (randomizedAlphabet)
        shuffle(symbols.begin(), symbols.end(), ext::random_devices::semirandom);

    auto cutoffPoint = symbols.begin();
    std::advance(cutoffPoint, maxAlphabetSize);
    symbols.erase(cutoffPoint, symbols.end());

    return symbols;
}

void generateRankedAlphabet(ext::map<size_t, ext::vector<DefaultSymbolType>>& rankedAlphabet, size_t maxAlphabetSize, bool randomizedAlphabet)
{
    size_t ranksCount = rankedAlphabet.size();
    ext::vector<DefaultSymbolType> unrankedAlphabet = generateUnrankedAlphabet(maxAlphabetSize > ranksCount ? maxAlphabetSize : ranksCount, randomizedAlphabet);

    for (std::pair<const size_t, ext::vector<DefaultSymbolType>>& symbolGroup : rankedAlphabet) // TODO except first value once we have ranges
        std::sample(unrankedAlphabet.begin(), unrankedAlphabet.end(), std::back_inserter(symbolGroup.second), ext::random_devices::semirandom() % (unrankedAlphabet.size() - 1) + 1, ext::random_devices::semirandom); // We need at least one node in each group
    rankedAlphabet[0] = unrankedAlphabet;
}

Node* generateTreeStructure(int depth, int nodesCount, size_t maxRank = std::numeric_limits<size_t>::max())
{
    if (depth >= nodesCount)
        throw exception::CommonException("number of nodes is too small");

    if ((maxRank != std::numeric_limits<size_t>::max()) && (pow(maxRank, depth + 1) - 1 < nodesCount))
        throw exception::CommonException("number of nodes is too small");

    ext::vector<Node*> nodes(nodesCount);

    // generate path depth long
    Node* root = nodes[0] = new Node();

    for (int i = 1; i <= depth; i++)
        nodes[i] = new Node(nodes[i - 1]);

    // move final leaf to end
    nodes[nodesCount - 1] = nodes[depth];

    int availableNodesIndex = depth;
    int finalNodesIndex = nodesCount - 2;

    while (finalNodesIndex >= availableNodesIndex) {
        int randomIndex = ext::random_devices::semirandom() % availableNodesIndex;
        Node* parent = nodes[randomIndex];
        Node* node = new Node(parent);

        // put node to end if it reached depth limit
        if (node->depth < depth) {
            nodes[availableNodesIndex] = node;
            availableNodesIndex++;
        } else {
            nodes[finalNodesIndex] = node;
            finalNodesIndex--;
        }

        // put parent node to end if it reached rank limit
        if (parent->rank >= maxRank) {
            nodes[randomIndex] = nodes[availableNodesIndex - 1];
            nodes[finalNodesIndex] = parent;
            availableNodesIndex--;
            finalNodesIndex--;
        }
    }

    // move generated path to random branch
    root->rotateLeftBranch();

    return root;
}

UnrankedTree<> RandomUnrankedTreeFactory::generateUnrankedTree(int depth, int nodesCount, size_t maxAlphabetSize, bool randomizedAlphabet, size_t maxRank)
{
    Node* root = generateTreeStructure(depth, nodesCount, maxRank);
    ext::vector<DefaultSymbolType> alphabet = generateUnrankedAlphabet(maxAlphabetSize, randomizedAlphabet);

    root->generateUnrankedSymbols(alphabet);

    UnrankedTree<> tree(root->createUnrankedNode());
    delete root;
    return tree;
}

auto GenerateUnrankedTree = registration::AbstractRegister<RandomUnrankedTreeFactory, tree::UnrankedTree<>, int, int, size_t, bool, size_t>(RandomUnrankedTreeFactory::generateUnrankedTree, abstraction::AlgorithmCategories::AlgorithmCategory::DEFAULT, "depth", "nodesCount", "maxAlphabetSize", "randomizedAlphabet", "maxRank");

UnrankedPattern<> RandomUnrankedPatternFactory::generateUnrankedPattern(int depth, int nodesCount, size_t maxAlphabetSize, bool randomizedAlphabet, size_t maxRank)
{
    Node* root = generateTreeStructure(depth, nodesCount, maxRank);
    ext::vector<DefaultSymbolType> alphabet = generateUnrankedAlphabet(maxAlphabetSize, randomizedAlphabet);

    root->generateUnrankedSymbols(alphabet);

    DefaultSymbolType subtreeWildcard = alphabet::Wildcard::instance<DefaultSymbolType>();
    DefaultSymbolType subtreeGap = alphabet::Gap::instance<DefaultSymbolType>();

    UnrankedPattern<> tree(std::move(subtreeWildcard), std::move(subtreeGap), root->createUnrankedPatternNode());
    delete root;
    return tree;
}

auto GenerateUnrankedPattern = registration::AbstractRegister<RandomUnrankedPatternFactory, tree::UnrankedPattern<>, int, int, size_t, bool, size_t>(RandomUnrankedPatternFactory::generateUnrankedPattern, abstraction::AlgorithmCategories::AlgorithmCategory::DEFAULT, "depth", "nodesCount", "maxAlphabetSize", "randomizedAlphabet", "maxRank");

UnrankedExtendedPattern<> RandomUnrankedExtendedPatternFactory::generateUnrankedExtendedPattern(int depth, int nodesCount, size_t maxAlphabetSize, bool randomizedAlphabet, size_t maxRank, double nodeWildcardProbability)
{
    Node* root = generateTreeStructure(depth, nodesCount, maxRank);
    ext::vector<DefaultSymbolType> alphabet = generateUnrankedAlphabet(maxAlphabetSize, randomizedAlphabet);

    root->generateUnrankedSymbols(alphabet);

    ext::set<DefaultSymbolType> selectedNodeWildcards;
    for (const DefaultSymbolType& symbol : alphabet) {
        if (ext::random_devices::semirandom() % 100 < nodeWildcardProbability)
            selectedNodeWildcards.insert(symbol);
    }

    DefaultSymbolType nodeWildcard = alphabet::NodeWildcard::instance<DefaultSymbolType>();
    DefaultSymbolType subtreeWildcard = alphabet::Wildcard::instance<DefaultSymbolType>();
    DefaultSymbolType subtreeGap = alphabet::Gap::instance<DefaultSymbolType>();

    UnrankedExtendedPattern<> tree(std::move(subtreeWildcard), std::move(subtreeGap), std::move(nodeWildcard), root->createUnrankedExtendedPatternNode(selectedNodeWildcards));
    delete root;
    return tree;
}

auto GenerateUnrankedExtendedPattern = registration::AbstractRegister<RandomUnrankedExtendedPatternFactory, tree::UnrankedExtendedPattern<>, int, int, size_t, bool, size_t, double>(RandomUnrankedExtendedPatternFactory::generateUnrankedExtendedPattern, abstraction::AlgorithmCategories::AlgorithmCategory::DEFAULT, "depth", "nodesCount", "maxAlphabetSize", "randomizedAlphabet", "maxRank", "nodeWildcardProbability");

RankedTree<> RandomRankedTreeFactory::generateRankedTree(int depth, int nodesCount, size_t maxAlphabetSize, bool randomizedAlphabet, size_t maxRank)
{
    Node* root = generateTreeStructure(depth, nodesCount, maxRank);
    ext::map<size_t, ext::vector<DefaultSymbolType>> rankedAlphabet;

    root->fillRanks(rankedAlphabet);
    generateRankedAlphabet(rankedAlphabet, maxAlphabetSize, randomizedAlphabet);
    root->generateRankedSymbols(rankedAlphabet);

    RankedTree<> tree(root->createRankedNode());
    delete root;
    return tree;
}

auto GenerateRankedTree = registration::AbstractRegister<RandomRankedTreeFactory, tree::RankedTree<>, int, int, size_t, bool, size_t>(RandomRankedTreeFactory::generateRankedTree, abstraction::AlgorithmCategories::AlgorithmCategory::DEFAULT, "depth", "nodesCount", "maxAlphabetSize", "randomizedAlphabet", "maxRank");

RankedPattern<> RandomRankedPatternFactory::generateRankedPattern(int depth, int nodesCount, size_t maxAlphabetSize, bool randomizedAlphabet, size_t maxRank)
{
    Node* root = generateTreeStructure(depth, nodesCount, maxRank);
    ext::map<size_t, ext::vector<DefaultSymbolType>> rankedAlphabet;

    root->fillRanks(rankedAlphabet);
    generateRankedAlphabet(rankedAlphabet, maxAlphabetSize, randomizedAlphabet);
    root->generateRankedSymbols(rankedAlphabet);

    common::ranked_symbol<> subtreeWildcard = alphabet::Wildcard::instance<common::ranked_symbol<>>();

    RankedPattern<> tree(std::move(subtreeWildcard), root->createRankedPatternNode());
    delete root;
    return tree;
}

auto GenerateRankedPattern = registration::AbstractRegister<RandomRankedPatternFactory, tree::RankedPattern<>, int, int, size_t, bool, size_t>(RandomRankedPatternFactory::generateRankedPattern, abstraction::AlgorithmCategories::AlgorithmCategory::DEFAULT, "depth", "nodesCount", "maxAlphabeSize", "randomizedAlphabet", "maxRank");

RankedExtendedPattern<> RandomRankedExtendedPatternFactory::generateRankedExtendedPattern(int depth, int nodesCount, size_t maxAlphabetSize, bool randomizedAlphabet, size_t maxRank, double nodeWildcardProbability)
{
    Node* root = generateTreeStructure(depth, nodesCount, maxRank);
    ext::map<size_t, ext::vector<DefaultSymbolType>> rankedAlphabet;

    root->fillRanks(rankedAlphabet);
    generateRankedAlphabet(rankedAlphabet, maxAlphabetSize, randomizedAlphabet);
    root->generateRankedSymbols(rankedAlphabet);

    ext::set<common::ranked_symbol<>> selectedNodeWildcards;
    for (const std::pair<const size_t, ext::vector<DefaultSymbolType>>& it : rankedAlphabet)
        for (const DefaultSymbolType& i : it.second)
            if (ext::random_devices::semirandom() % 100 < nodeWildcardProbability)
                selectedNodeWildcards.insert(common::ranked_symbol<>(i, it.first));

    ext::set<common::ranked_symbol<>> nodeWildcards;
    for (const common::ranked_symbol<>& symbol : selectedNodeWildcards) {
        common::ranked_symbol<> wildcard(alphabet::NodeWildcard::template instance<DefaultSymbolType>(), symbol.getRank());
        nodeWildcards.insert(wildcard);
    }

    common::ranked_symbol<> subtreeWildcard = alphabet::Wildcard::instance<common::ranked_symbol<>>();
    RankedExtendedPattern<> tree(std::move(subtreeWildcard), std::move(nodeWildcards), root->createRankedExtendedPatternNode(selectedNodeWildcards));
    delete root;
    return tree;
}

auto GenerateRankedExtendedPattern = registration::AbstractRegister<RandomRankedExtendedPatternFactory, tree::RankedExtendedPattern<>, int, int, size_t, bool, size_t, double>(RandomRankedExtendedPatternFactory::generateRankedExtendedPattern, abstraction::AlgorithmCategories::AlgorithmCategory::DEFAULT, "depth", "nodesCount", "maxAlphabetSize", "randomizedAlphabet", "maxRank", "nodeWildcardProbability");

RankedNonlinearPattern<> RandomRankedNonlinearPatternFactory::generateRankedNonlinearPattern(int depth, int nodesCount, size_t maxAlphabetSize, bool randomizedAlphabet, bool singleNonlinearVariable, size_t maxRank)
{
    Node* root = generateTreeStructure(depth, nodesCount, maxRank);
    ext::map<size_t, ext::vector<DefaultSymbolType>> rankedAlphabet;

    root->fillRanks(rankedAlphabet);
    generateRankedAlphabet(rankedAlphabet, maxAlphabetSize, randomizedAlphabet);
    root->generateRankedSymbols(rankedAlphabet);

    ext::set<common::ranked_symbol<>> nonlinearVariables;

    if (singleNonlinearVariable)
        nonlinearVariables.insert(common::ranked_symbol<>(object::ObjectFactory<>::construct(alphabet::NonlinearVariable<>(object::ObjectFactory<>::construct("A"))), 0));
    else
        for (DefaultSymbolType i : rankedAlphabet[0])
            nonlinearVariables.insert(common::ranked_symbol<>(object::ObjectFactory<>::construct(alphabet::NonlinearVariable<>(object::ObjectFactory<>::construct(i))), 0));

    common::ranked_symbol<> subtreeWildcard = alphabet::Wildcard::instance<common::ranked_symbol<>>();
    RankedNonlinearPattern<> tree(std::move(subtreeWildcard), nonlinearVariables, root->createRankedNonlinearPatternNode(singleNonlinearVariable));
    delete root;
    return tree;
}

} /* namespace tree::generate */

namespace {

auto GenerateRankedNonlinearPattern = registration::AbstractRegister<tree::generate::RandomRankedNonlinearPatternFactory, tree::RankedNonlinearPattern<>, int, int, size_t, bool, bool, size_t>(tree::generate::RandomRankedNonlinearPatternFactory::generateRankedNonlinearPattern, abstraction::AlgorithmCategories::AlgorithmCategory::DEFAULT, "depth", "nodesCount", "maxAlphabetSize", "randomizedAlphabet", "singleNonlinearVariable", "maxRank");

} /* namespace */
