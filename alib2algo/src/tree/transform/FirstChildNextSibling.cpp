#include <registration/AlgoRegistration.hpp>
#include "FirstChildNextSibling.h"

namespace {

auto FirstChildNextSiblingUnrankedTree = registration::AbstractRegister<tree::transform::FirstChildNextSibling, tree::RankedTree<>, const tree::UnrankedTree<>&>(tree::transform::FirstChildNextSibling::transform);

} /* namespace */
