#pragma once

#include <climits>

#include <alib/pair>

#include <tree/ranked/PrefixRankedBarNonlinearPattern.h>
#include <tree/ranked/PrefixRankedBarPattern.h>
#include <tree/ranked/PrefixRankedBarTree.h>
#include <tree/ranked/PrefixRankedNonlinearPattern.h>
#include <tree/ranked/PrefixRankedPattern.h>
#include <tree/ranked/PrefixRankedTree.h>

namespace tree {

namespace exact {

class BackwardOccurrenceTest {
public:
    template <class SymbolType>
    static ext::pair<size_t, size_t> occurrence(const PrefixRankedBarTree<SymbolType>& subject, const ext::vector<int>& subjectSubtreeJumpTable, const PrefixRankedBarTree<SymbolType>& pattern, size_t subjectPosition);
    template <class SymbolType>
    static ext::pair<size_t, size_t> occurrence(const PrefixRankedBarTree<SymbolType>& subject, const ext::vector<int>& subjectSubtreeJumpTable, const PrefixRankedBarPattern<SymbolType>& pattern, size_t subjectPosition);
    template <class SymbolType>
    static ext::pair<size_t, size_t> occurrence(const PrefixRankedBarTree<SymbolType>& subject, const ext::vector<int>& subjectSubtreeJumpTable, const tree::PrefixRankedBarTree<unsigned>& repeats, const PrefixRankedBarNonlinearPattern<SymbolType>& pattern, size_t subjectPosition);
};

template <class SymbolType>
ext::pair<size_t, size_t> BackwardOccurrenceTest::occurrence(const PrefixRankedBarTree<SymbolType>& subject, const ext::vector<int>& subjectSubtreeJumpTable, const PrefixRankedBarTree<SymbolType>& pattern, size_t subjectPosition)
{
    return occurrence(subject, subjectSubtreeJumpTable, tree::PrefixRankedBarPattern<SymbolType>(pattern), subjectPosition);
}

template <class SymbolType>
ext::pair<size_t, size_t> BackwardOccurrenceTest::occurrence(const PrefixRankedBarTree<SymbolType>& subject, const ext::vector<int>& subjectSubtreeJumpTable, const PrefixRankedBarPattern<SymbolType>& pattern, size_t subjectPosition)
{
    // index to the pattern
    size_t j = pattern.getContent().size();

    // offset to the subject
    size_t offset = subjectPosition + 1;

    while ((j > 0) && (offset > 0)) {
        --j;
        --offset;

        if (subject.getContent()[offset] == pattern.getContent()[j]) {
            // match of symbol
        } else if ((pattern.getContent()[j] == pattern.getVariablesBar()) && (subject.getBars().contains(subject.getContent()[offset]))) { // the second part of the condition is needed to handle S |S
            // match of variable with subtree
            offset = subjectSubtreeJumpTable[offset] + 1;
            j = j - 1;
        } else {
            ++j;
            ++offset;

            break;
        }
    }

    return ext::make_pair(j, offset);
}

template <class SymbolType>
ext::pair<size_t, size_t> BackwardOccurrenceTest::occurrence(const PrefixRankedBarTree<SymbolType>& subject, const ext::vector<int>& subjectSubtreeJumpTable, const tree::PrefixRankedBarTree<unsigned>& repeats, const PrefixRankedBarNonlinearPattern<SymbolType>& pattern, size_t subjectPosition)
{

    // to represent state of variable to subtree repeat
    ext::map<common::ranked_symbol<SymbolType>, unsigned> variablesSetting;

    // index to the pattern
    size_t j = pattern.getContent().size();

    // offset to the subject
    size_t offset = subjectPosition + 1;

    while ((j > 0) && (offset > 0)) {
        --j;
        --offset;

        if (subject.getContent()[offset] == pattern.getContent()[j]) {
            // match of symbol
        } else if ((pattern.getContent()[j] == pattern.getVariablesBar()) && (subject.getBars().contains(subject.getContent()[offset]))) { // the second part of the condition is needed to handle S |S
            // else match of variable with subtree
            offset = subjectSubtreeJumpTable[offset] + 1;
            j = j - 1;

            // check nonlinear variable
            if (pattern.getNonlinearVariables().contains(pattern.getContent()[j])) {
                auto setting = variablesSetting.find(pattern.getContent()[j]);

                if (setting != variablesSetting.end() && repeats.getContent()[offset].getSymbol() != setting->second) {
                    ++j;
                    ++offset;

                    break;
                }

                variablesSetting.insert(std::make_pair(pattern.getContent()[j], repeats.getContent()[offset].getSymbol()));
            }
        } else {
            ++j;
            ++offset;

            break;
        }
    }

    return ext::make_pair(j, offset);
}

} /* namespace exact */

} /* namespace tree */
