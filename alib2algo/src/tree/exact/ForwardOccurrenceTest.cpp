#include <registration/AlgoRegistration.hpp>
#include "ForwardOccurrenceTest.h"

namespace {

auto ForwardOccurrenceTestPrefixRankedTree = registration::AbstractRegister<tree::exact::ForwardOccurrenceTest, size_t, const tree::PrefixRankedTree<>&, const ext::vector<int>&, const tree::PrefixRankedTree<>&, size_t, size_t>(tree::exact::ForwardOccurrenceTest::occurrence, abstraction::AlgorithmCategories::AlgorithmCategory::DEFAULT, "subject", "subjectSubtreeJumpTable", "pattern", "subjectPosition", "patternStartIndex");

auto ForwardOccurrenceTestPrefixRankedBarTree = registration::AbstractRegister<tree::exact::ForwardOccurrenceTest, size_t, const tree::PrefixRankedBarTree<>&, const ext::vector<int>&, const tree::PrefixRankedBarTree<>&, size_t, size_t>(tree::exact::ForwardOccurrenceTest::occurrence, abstraction::AlgorithmCategories::AlgorithmCategory::DEFAULT, "subject", "subjectSubtreeJumpTable", "pattern", "subjectPosition", "patternStartIndex");

} /* namespace */
