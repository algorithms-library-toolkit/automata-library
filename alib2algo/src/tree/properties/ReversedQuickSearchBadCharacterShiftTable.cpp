#include <registration/AlgoRegistration.hpp>
#include "ReversedQuickSearchBadCharacterShiftTable.h"

namespace {

auto ReversedQuickSearchBadCharacterShiftTablePrefixRankedBarPattern = registration::AbstractRegister<tree::properties::ReversedQuickSearchBadCharacterShiftTable, ext::map<common::ranked_symbol<>, size_t>, const tree::PrefixRankedBarPattern<>&>(tree::properties::ReversedQuickSearchBadCharacterShiftTable::bcs);
auto ReversedQuickSearchBadCharacterShiftTablePrefixRankedBarNonlinearPattern = registration::AbstractRegister<tree::properties::ReversedQuickSearchBadCharacterShiftTable, ext::map<common::ranked_symbol<>, size_t>, const tree::PrefixRankedBarNonlinearPattern<>&>(tree::properties::ReversedQuickSearchBadCharacterShiftTable::bcs);
auto ReversedQuickSearchBadCharacterShiftTablePrefixRankedPattern = registration::AbstractRegister<tree::properties::ReversedQuickSearchBadCharacterShiftTable, ext::map<common::ranked_symbol<>, size_t>, const tree::PrefixRankedPattern<>&>(tree::properties::ReversedQuickSearchBadCharacterShiftTable::bcs);
auto ReversedQuickSearchBadCharacterShiftTablePrefixRankedNonlinearPattern = registration::AbstractRegister<tree::properties::ReversedQuickSearchBadCharacterShiftTable, ext::map<common::ranked_symbol<>, size_t>, const tree::PrefixRankedNonlinearPattern<>&>(tree::properties::ReversedQuickSearchBadCharacterShiftTable::bcs);

} /* namespace */
