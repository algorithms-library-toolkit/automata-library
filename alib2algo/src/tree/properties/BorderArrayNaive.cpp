#include <registration/AlgoRegistration.hpp>
#include "BorderArrayNaive.h"

namespace {

auto BorderArrayPrefixRankedBarNonlinearPattern = registration::AbstractRegister<tree::properties::BorderArrayNaive, ext::vector<size_t>, const tree::PrefixRankedBarNonlinearPattern<>&>(tree::properties::BorderArrayNaive::construct);
auto BorderArrayPrefixRankedBarPattern = registration::AbstractRegister<tree::properties::BorderArrayNaive, ext::vector<size_t>, const tree::PrefixRankedBarPattern<>&>(tree::properties::BorderArrayNaive::construct);
auto BorderArrayPrefixRankedNonlinearPattern = registration::AbstractRegister<tree::properties::BorderArrayNaive, ext::vector<size_t>, const tree::PrefixRankedNonlinearPattern<>&>(tree::properties::BorderArrayNaive::construct);
auto BorderArrayPrefixRankedPattern = registration::AbstractRegister<tree::properties::BorderArrayNaive, ext::vector<size_t>, const tree::PrefixRankedPattern<>&>(tree::properties::BorderArrayNaive::construct);
auto BorderArrayPrefixRankedExtendedPattern = registration::AbstractRegister<tree::properties::BorderArrayNaive, ext::vector<size_t>, const tree::PrefixRankedExtendedPattern<>&>(tree::properties::BorderArrayNaive::construct);

} /* namespace */
