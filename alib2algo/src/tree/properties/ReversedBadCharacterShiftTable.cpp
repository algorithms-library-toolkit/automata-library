#include <registration/AlgoRegistration.hpp>
#include "ReversedBadCharacterShiftTable.h"

namespace {

auto ReversedBadCharacterShiftTablePrefixRankedBarPattern = registration::AbstractRegister<tree::properties::ReversedBadCharacterShiftTable, ext::map<common::ranked_symbol<>, size_t>, const tree::PrefixRankedBarPattern<>&>(tree::properties::ReversedBadCharacterShiftTable::bcs);
auto ReversedBadCharacterShiftTablePrefixRankedBarNonlinearPattern = registration::AbstractRegister<tree::properties::ReversedBadCharacterShiftTable, ext::map<common::ranked_symbol<>, size_t>, const tree::PrefixRankedBarNonlinearPattern<>&>(tree::properties::ReversedBadCharacterShiftTable::bcs);
auto ReversedBadCharacterShiftTablePrefixRankedPattern = registration::AbstractRegister<tree::properties::ReversedBadCharacterShiftTable, ext::map<common::ranked_symbol<>, size_t>, const tree::PrefixRankedPattern<>&>(tree::properties::ReversedBadCharacterShiftTable::bcs);
auto ReversedBadCharacterShiftTablePrefixRankedNonlinearPattern = registration::AbstractRegister<tree::properties::ReversedBadCharacterShiftTable, ext::map<common::ranked_symbol<>, size_t>, const tree::PrefixRankedNonlinearPattern<>&>(tree::properties::ReversedBadCharacterShiftTable::bcs);

} /* namespace */
