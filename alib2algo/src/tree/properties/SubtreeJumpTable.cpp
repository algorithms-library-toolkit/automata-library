#include <registration/AlgoRegistration.hpp>
#include "SubtreeJumpTable.h"

namespace {

auto SubtreeSizesPrefixRankedBarTree = registration::AbstractRegister<tree::properties::SubtreeJumpTable, ext::vector<int>, const tree::PrefixRankedBarTree<>&>(tree::properties::SubtreeJumpTable::compute);
auto SubtreeSizesPrefixRankedBarPattern = registration::AbstractRegister<tree::properties::SubtreeJumpTable, ext::vector<int>, const tree::PrefixRankedBarPattern<>&>(tree::properties::SubtreeJumpTable::compute);
auto SubtreeSizesPrefixRankedBarNonlinearPattern = registration::AbstractRegister<tree::properties::SubtreeJumpTable, ext::vector<int>, const tree::PrefixRankedBarNonlinearPattern<>&>(tree::properties::SubtreeJumpTable::compute);
auto SubtreeSizesPrefixRankedTree = registration::AbstractRegister<tree::properties::SubtreeJumpTable, ext::vector<int>, const tree::PrefixRankedTree<>&>(tree::properties::SubtreeJumpTable::compute);
auto SubtreeSizesPrefixRankedPattern = registration::AbstractRegister<tree::properties::SubtreeJumpTable, ext::vector<int>, const tree::PrefixRankedPattern<>&>(tree::properties::SubtreeJumpTable::compute);
auto SubtreeSizesPrefixRankedExtendedPattern = registration::AbstractRegister<tree::properties::SubtreeJumpTable, ext::vector<int>, const tree::PrefixRankedExtendedPattern<>&>(tree::properties::SubtreeJumpTable::compute);
auto SubtreeSizesPrefixRankedNonlinearPattern = registration::AbstractRegister<tree::properties::SubtreeJumpTable, ext::vector<int>, const tree::PrefixRankedNonlinearPattern<>&>(tree::properties::SubtreeJumpTable::compute);
auto SubtreeSizesPrefixBarTree = registration::AbstractRegister<tree::properties::SubtreeJumpTable, ext::vector<int>, const tree::PrefixBarTree<>&>(tree::properties::SubtreeJumpTable::compute);

} /* namespace */
