#include <registration/AlgoRegistration.hpp>
#include "BadCharacterShiftTable.h"

namespace {

auto BadCharacterShiftTablePrefixRankedBarPattern = registration::AbstractRegister<tree::properties::BadCharacterShiftTable, ext::map<common::ranked_symbol<>, size_t>, const tree::PrefixRankedBarPattern<>&>(tree::properties::BadCharacterShiftTable::bcs);
auto BadCharacterShiftTablePrefixRankedBarNonlinearPattern = registration::AbstractRegister<tree::properties::BadCharacterShiftTable, ext::map<common::ranked_symbol<>, size_t>, const tree::PrefixRankedBarNonlinearPattern<>&>(tree::properties::BadCharacterShiftTable::bcs);

} /* namespace */
