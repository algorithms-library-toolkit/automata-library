#include <registration/AlgoRegistration.hpp>
#include "NonlinearFullAndLinearIndexPatterns.h"

namespace {

auto nonlinearFullAndLinearIndexPatternsPrefixRankedPattern = registration::AbstractRegister<arbology::query::NonlinearFullAndLinearIndexPatterns, ext::set<unsigned>, const indexes::arbology::NonlinearFullAndLinearIndex<>&, const tree::PrefixRankedNonlinearPattern<>&>(arbology::query::NonlinearFullAndLinearIndexPatterns::query);
auto nonlinearFullAndLinearIndexPatternsPrefixRankedBarPattern = registration::AbstractRegister<arbology::query::NonlinearFullAndLinearIndexPatterns, ext::set<unsigned>, const indexes::arbology::NonlinearFullAndLinearIndex<>&, const tree::PrefixRankedBarNonlinearPattern<>&>(arbology::query::NonlinearFullAndLinearIndexPatterns::query);

} /* namespace */
