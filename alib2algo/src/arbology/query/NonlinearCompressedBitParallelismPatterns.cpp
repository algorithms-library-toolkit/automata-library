#include <registration/AlgoRegistration.hpp>
#include "NonlinearCompressedBitParallelismPatterns.h"

namespace {

auto NonlinearCompressedBitParallelismPatternsPrefixRankedBarPattern = registration::AbstractRegister<arbology::query::NonlinearCompressedBitParallelismPatterns, ext::set<unsigned>, const indexes::arbology::NonlinearCompressedBitParallelTreeIndex<>&, const tree::PrefixRankedBarNonlinearPattern<>&>(arbology::query::NonlinearCompressedBitParallelismPatterns::query);

} /* namespace */
