#include <registration/AlgoRegistration.hpp>
#include "ExactSubtreeRepeatsFromSubtreeAutomaton.h"

namespace {

auto ExactRepeatsFromSubtreeAutomatonPrefixRankedTree = registration::AbstractRegister<arbology::properties::ExactSubtreeRepeatsFromSubtreeAutomaton, tree::PrefixRankedTree<unsigned>, const tree::PrefixRankedTree<>&>(arbology::properties::ExactSubtreeRepeatsFromSubtreeAutomaton::repeats);

} /* namespace */
