#include <registration/AlgoRegistration.hpp>
#include "ExactPatternMatchingAutomaton.h"

namespace {

auto ExactPatternMatchingAutomatonPrefixRankedTree = registration::AbstractRegister<arbology::exact::ExactPatternMatchingAutomaton, automaton::InputDrivenNPDA<common::ranked_symbol<DefaultSymbolType>, char, unsigned>, const tree::PrefixRankedTree<>&>(arbology::exact::ExactPatternMatchingAutomaton::construct);

auto ExactPatternMatchingAutomatonPrefixRankedPattern = registration::AbstractRegister<arbology::exact::ExactPatternMatchingAutomaton, automaton::NPDA<common::ranked_symbol<DefaultSymbolType>, char, unsigned>, const tree::PrefixRankedPattern<>&>(arbology::exact::ExactPatternMatchingAutomaton::construct);

auto ExactPatternMatchingAutomatonPrefixRankedBarTree = registration::AbstractRegister<arbology::exact::ExactPatternMatchingAutomaton, automaton::InputDrivenNPDA<common::ranked_symbol<DefaultSymbolType>, char, unsigned>, const tree::PrefixRankedBarTree<>&>(arbology::exact::ExactPatternMatchingAutomaton::construct);

auto ExactPatternMatchingAutomatonPrefixRankedBarPattern = registration::AbstractRegister<arbology::exact::ExactPatternMatchingAutomaton, automaton::VisiblyPushdownNPDA<common::ranked_symbol<DefaultSymbolType>, char, unsigned>, const tree::PrefixRankedBarPattern<>&>(arbology::exact::ExactPatternMatchingAutomaton::construct);

auto ExactPatternMatchingAutomatonRankedTree = registration::AbstractRegister<arbology::exact::ExactPatternMatchingAutomaton, automaton::NFTA<DefaultSymbolType, unsigned>, const tree::RankedTree<>&>(arbology::exact::ExactPatternMatchingAutomaton::construct);

auto ExactPatternMatchingAutomatonRankedPattern = registration::AbstractRegister<arbology::exact::ExactPatternMatchingAutomaton, automaton::NFTA<DefaultSymbolType, unsigned>, const tree::RankedPattern<>&>(arbology::exact::ExactPatternMatchingAutomaton::construct);

auto ExactPatternMatchingAutomatonUnorderedRankedTree = registration::AbstractRegister<arbology::exact::ExactPatternMatchingAutomaton, automaton::UnorderedNFTA<DefaultSymbolType, unsigned>, const tree::UnorderedRankedTree<>&>(arbology::exact::ExactPatternMatchingAutomaton::construct);

auto ExactPatternMatchingAutomatonUnorderedRankedPattern = registration::AbstractRegister<arbology::exact::ExactPatternMatchingAutomaton, automaton::UnorderedNFTA<DefaultSymbolType, unsigned>, const tree::UnorderedRankedPattern<>&>(arbology::exact::ExactPatternMatchingAutomaton::construct);

auto ExactPatternMatchingAutomatonUnrankedTree = registration::AbstractRegister<arbology::exact::ExactPatternMatchingAutomaton, automaton::NondeterministicZAutomaton<DefaultSymbolType, unsigned>, const tree::UnrankedTree<>&>(arbology::exact::ExactPatternMatchingAutomaton::construct);

auto ExactPatternMatchingAutomatonUnrankedPattern = registration::AbstractRegister<arbology::exact::ExactPatternMatchingAutomaton, automaton::NondeterministicZAutomaton<DefaultSymbolType, unsigned>, const tree::UnrankedPattern<>&>(arbology::exact::ExactPatternMatchingAutomaton::construct);

auto ExactPatternMatchingAutomatonUnrankedExtendedPattern = registration::AbstractRegister<arbology::exact::ExactPatternMatchingAutomaton, automaton::NondeterministicZAutomaton<DefaultSymbolType, unsigned>, const tree::UnrankedExtendedPattern<>&>(arbology::exact::ExactPatternMatchingAutomaton::construct);

auto ExactPatternMatchingAutomatonUnrankedExtendedPatternArcFactored = registration::AbstractRegister<arbology::exact::ExactPatternMatchingAutomatonArcFactored, automaton::ArcFactoredNondeterministicZAutomaton<DefaultSymbolType, unsigned>, const tree::UnrankedExtendedPattern<>&>(arbology::exact::ExactPatternMatchingAutomatonArcFactored::construct);

} /* namespace */
