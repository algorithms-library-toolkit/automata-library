#pragma once

#include <alib/map>
#include <alib/set>

#include <common/ranked_symbol.hpp>

#include <tree/exact/BackwardOccurrenceTest.h>
#include <tree/properties/BadCharacterShiftTable.h>
#include <tree/properties/ExactSubtreeRepeatsNaive.h>
#include <tree/properties/SubtreeJumpTable.h>

#include <tree/ranked/PrefixRankedBarNonlinearPattern.h>
#include <tree/ranked/PrefixRankedBarPattern.h>
#include <tree/ranked/PrefixRankedBarTree.h>

namespace arbology {

namespace exact {

/**
 * Implementation of BMH for MI(E+\eps)-EVY course 2014
 * To get rid of zeros in BCS table we ignore last haystack character
 */
class BoyerMooreHorspool {
public:
    /**
     * Search for pattern in linear string.
     * @return set set of occurences
     */
    template <class SymbolType>
    static ext::set<unsigned> match(const tree::PrefixRankedBarTree<SymbolType>& subject, const tree::PrefixRankedBarTree<SymbolType>& pattern);
    template <class SymbolType>
    static ext::set<unsigned> match(const tree::PrefixRankedBarTree<SymbolType>& subject, const tree::PrefixRankedBarPattern<SymbolType>& pattern);
    template <class SymbolType>
    static ext::set<unsigned> match(const tree::PrefixRankedBarTree<SymbolType>& subject, const tree::PrefixRankedBarNonlinearPattern<SymbolType>& pattern);
};

template <class SymbolType>
ext::set<unsigned> BoyerMooreHorspool::match(const tree::PrefixRankedBarTree<SymbolType>& subject, const tree::PrefixRankedBarTree<SymbolType>& pattern)
{
    return match(subject, tree::PrefixRankedBarPattern<SymbolType>(pattern));
}

template <class SymbolType>
ext::set<unsigned> BoyerMooreHorspool::match(const tree::PrefixRankedBarTree<SymbolType>& subject, const tree::PrefixRankedBarPattern<SymbolType>& pattern)
{
    ext::set<unsigned> occ;
    ext::map<common::ranked_symbol<SymbolType>, size_t> bcs = tree::properties::BadCharacterShiftTable::bcs(pattern); // NOTE: the subjects alphabet must be a subset or equal to the pattern
    ext::vector<int> subjectSubtreeJumpTable = tree::properties::SubtreeJumpTable::compute(subject);

    // index to the subject
    unsigned i = pattern.getContent().size() - 1;

    // main loop of the algorithm over all possible indexes where the pattern can start
    while (i < subject.getContent().size()) {
        // pair j and offset
        ext::pair<size_t, size_t> jOffset = tree::exact::BackwardOccurrenceTest::occurrence(subject, subjectSubtreeJumpTable, pattern, i);

        // match was found
        if (jOffset.first == 0)
            occ.insert(jOffset.second);

        // shift heuristics
        i += bcs[subject.getContent()[i]];
    }

    return occ;
}

template <class SymbolType>
ext::set<unsigned> BoyerMooreHorspool::match(const tree::PrefixRankedBarTree<SymbolType>& subject, const tree::PrefixRankedBarNonlinearPattern<SymbolType>& pattern)
{
    ext::set<unsigned> occ;
    ext::map<common::ranked_symbol<SymbolType>, size_t> bcs = tree::properties::BadCharacterShiftTable::bcs(pattern); // NOTE: the subjects alphabet must be a subset or equal to the pattern

    ext::vector<int> subjectSubtreeJumpTable = tree::properties::SubtreeJumpTable::compute(subject);
    tree::PrefixRankedBarTree<unsigned> repeats = tree::properties::ExactSubtreeRepeatsNaive::repeats(subject);

    // index to the subject
    unsigned i = pattern.getContent().size() - 1;

    // main loop of the algorithm over all possible indexes where the pattern can start
    while (i < subject.getContent().size()) {
        // pair j and offset
        ext::pair<size_t, size_t> jOffset = tree::exact::BackwardOccurrenceTest::occurrence(subject, subjectSubtreeJumpTable, repeats, pattern, i);

        // match was found
        if (jOffset.first == 0)
            occ.insert(jOffset.second);

        // shift heuristics
        i += bcs[subject.getContent()[i]];
    }

    return occ;
}

} /* namespace exact */

} /* namespace arbology */
