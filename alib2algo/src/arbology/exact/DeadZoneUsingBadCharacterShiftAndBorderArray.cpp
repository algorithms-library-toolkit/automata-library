#include <registration/AlgoRegistration.hpp>
#include "DeadZoneUsingBadCharacterShiftAndBorderArray.h"

namespace {

auto DeadZoneUsingBadCharacterShiftAndBorderArrayPrefixRankedBarTreePrefixRankedBarTree = registration::AbstractRegister<arbology::exact::DeadZoneUsingBadCharacterShiftAndBorderArray, ext::set<unsigned>, const tree::PrefixRankedBarTree<>&, const tree::PrefixRankedBarTree<>&>(arbology::exact::DeadZoneUsingBadCharacterShiftAndBorderArray::match);
auto DeadZoneUsingBadCharacterShiftAndBorderArrayPrefixRankedBarTreePrefixRankedBarPattern = registration::AbstractRegister<arbology::exact::DeadZoneUsingBadCharacterShiftAndBorderArray, ext::set<unsigned>, const tree::PrefixRankedBarTree<>&, const tree::PrefixRankedBarPattern<>&>(arbology::exact::DeadZoneUsingBadCharacterShiftAndBorderArray::match);
auto DeadZoneUsingBadCharacterShiftAndBorderArrayPrefixRankedTreePrefixRankedTree = registration::AbstractRegister<arbology::exact::DeadZoneUsingBadCharacterShiftAndBorderArray, ext::set<unsigned>, const tree::PrefixRankedTree<>&, const tree::PrefixRankedTree<>&>(arbology::exact::DeadZoneUsingBadCharacterShiftAndBorderArray::match);
auto DeadZoneUsingBadCharacterShiftAndBorderArrayPrefixRankedTreePrefixRankedPattern = registration::AbstractRegister<arbology::exact::DeadZoneUsingBadCharacterShiftAndBorderArray, ext::set<unsigned>, const tree::PrefixRankedTree<>&, const tree::PrefixRankedPattern<>&>(arbology::exact::DeadZoneUsingBadCharacterShiftAndBorderArray::match);

} /* namespace */
