#include <registration/AlgoRegistration.hpp>
#include "ExactPatternMatch.h"

namespace {

auto ExactPatternMatchUnrankedPattern = registration::AbstractRegister<arbology::exact::ExactPatternMatch, ext::set<unsigned>, const tree::UnrankedTree<>&, const tree::UnrankedPattern<>&>(arbology::exact::ExactPatternMatch::match);
auto ExactPatternMatchUnrankedExtendedPattern = registration::AbstractRegister<arbology::exact::ExactPatternMatch, ext::set<unsigned>, const tree::UnrankedTree<>&, const tree::UnrankedExtendedPattern<>&>(arbology::exact::ExactPatternMatch::match);
auto ExactPatternMatchUnorderedRankedPattern = registration::AbstractRegister<arbology::exact::ExactPatternMatch, ext::set<unsigned>, const tree::UnorderedRankedTree<>&, const tree::UnorderedRankedPattern<>&>(arbology::exact::ExactPatternMatch::match);
auto ExactPatternMatchUnorderedUnrankedRankedPattern = registration::AbstractRegister<arbology::exact::ExactPatternMatch, ext::set<unsigned>, const tree::UnorderedUnrankedTree<>&, const tree::UnorderedUnrankedPattern<>&>(arbology::exact::ExactPatternMatch::match);
auto ExactPatternMatchRankedPattern = registration::AbstractRegister<arbology::exact::ExactPatternMatch, ext::set<unsigned>, const tree::RankedTree<>&, const tree::RankedPattern<>&>(arbology::exact::ExactPatternMatch::match);
auto ExactPatternMatchRankedNonlinearPattern = registration::AbstractRegister<arbology::exact::ExactPatternMatch, ext::set<unsigned>, const tree::RankedTree<>&, const tree::RankedNonlinearPattern<>&>(arbology::exact::ExactPatternMatch::match);
auto ExactPatternMatchRankedExtendedPattern = registration::AbstractRegister<arbology::exact::ExactPatternMatch, ext::set<unsigned>, const tree::RankedTree<>&, const tree::RankedExtendedPattern<>&>(arbology::exact::ExactPatternMatch::match);
auto ExactPatternMatchPrefixRankedPattern = registration::AbstractRegister<arbology::exact::ExactPatternMatch, ext::set<unsigned>, const tree::PrefixRankedTree<>&, const tree::PrefixRankedPattern<>&>(arbology::exact::ExactPatternMatch::match);
auto ExactPatternMatchPrefixRankedNonlinearPattern = registration::AbstractRegister<arbology::exact::ExactPatternMatch, ext::set<unsigned>, const tree::PrefixRankedTree<>&, const tree::PrefixRankedNonlinearPattern<>&>(arbology::exact::ExactPatternMatch::match);
auto ExactPatternMatchPrefixRankedBarPattern = registration::AbstractRegister<arbology::exact::ExactPatternMatch, ext::set<unsigned>, const tree::PrefixRankedBarTree<>&, const tree::PrefixRankedBarPattern<>&>(arbology::exact::ExactPatternMatch::match);
auto ExactPatternMatchPrefixRankedBarNonlinearPattern = registration::AbstractRegister<arbology::exact::ExactPatternMatch, ext::set<unsigned>, const tree::PrefixRankedBarTree<>&, const tree::PrefixRankedBarNonlinearPattern<>&>(arbology::exact::ExactPatternMatch::match);

} /* namespace */
