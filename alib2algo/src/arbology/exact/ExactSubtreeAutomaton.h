#pragma once

#include <arbology/exact/ExactSubtreeMatchingAutomaton.h>
#include <automaton/PDA/InputDrivenNPDA.h>
#include <automaton/TA/NFTA.h>
#include <tree/ranked/PrefixRankedTree.h>
#include <tree/ranked/RankedTree.h>

namespace arbology::exact {

class ExactSubtreeAutomaton {
public:
    /** @brief Constructs subtree PDA (a "factor" automaton for linearised tree) */
    template <class SymbolType>
    static automaton::InputDrivenNPDA<common::ranked_symbol<SymbolType>, char, unsigned> construct(const tree::PrefixRankedTree<SymbolType>& tree);

    /** @brief Constructs subtree FTA (a "factor" automaton for tree) */
    template <class SymbolType>
    static automaton::NFTA<SymbolType, unsigned> construct(const tree::RankedTree<SymbolType>& tree);
};

template <class SymbolType>
automaton::InputDrivenNPDA<common::ranked_symbol<SymbolType>, char, unsigned> ExactSubtreeAutomaton::construct(const tree::PrefixRankedTree<SymbolType>& tree)
{
    automaton::InputDrivenNPDA<common::ranked_symbol<SymbolType>, char, unsigned> res(0, 'S');

    for (const common::ranked_symbol<SymbolType>& symbol : tree.getAlphabet()) {
        res.addInputSymbol(symbol);
        res.setPushdownStoreOperation(symbol, ext::vector<char>(1, 'S'), ext::vector<char>(symbol.getRank(), 'S'));
    }

    unsigned i = 1;

    for (const common::ranked_symbol<SymbolType>& symbol : tree.getContent()) {
        res.addState(i);
        res.addTransition(i - 1, symbol, i);
        res.addTransition(0, std::move(symbol), i);
        i++;
    }

    return res;
}

template <class SymbolType>
automaton::NFTA<SymbolType, unsigned> ExactSubtreeAutomaton::construct(const tree::RankedTree<SymbolType>& tree)
{
    auto res = ExactSubtreeMatchingAutomaton::construct(tree);
    res.setFinalStates(res.getStates());
    return res;
}

} /* namespace arbology::exact */
