#include <registration/AlgoRegistration.hpp>
#include "KnuthMorrisPratt.h"

namespace {

auto KnuthMorrisPrattPrefixRankedBarTreePrefixRankedBarTree = registration::AbstractRegister<arbology::exact::KnuthMorrisPratt, ext::set<unsigned>, const tree::PrefixRankedBarTree<>&, const tree::PrefixRankedBarTree<>&>(arbology::exact::KnuthMorrisPratt::match);

auto KnuthMorrisPrattPrefixRankedBarTreePrefixRankedBarPattern = registration::AbstractRegister<arbology::exact::KnuthMorrisPratt, ext::set<unsigned>, const tree::PrefixRankedBarTree<>&, const tree::PrefixRankedBarPattern<>&>(arbology::exact::KnuthMorrisPratt::match);

auto KnuthMorrisPrattPrefixRankedBarTreePrefixRankedBarNonlinearPattern = registration::AbstractRegister<arbology::exact::KnuthMorrisPratt, ext::set<unsigned>, const tree::PrefixRankedBarTree<>&, const tree::PrefixRankedBarNonlinearPattern<>&>(arbology::exact::KnuthMorrisPratt::match);

auto KnuthMorrisPrattPrefixRankedTreePrefixRankedTree = registration::AbstractRegister<arbology::exact::KnuthMorrisPratt, ext::set<unsigned>, const tree::PrefixRankedTree<>&, const tree::PrefixRankedTree<>&>(arbology::exact::KnuthMorrisPratt::match);

auto KnuthMorrisPrattPrefixRankedTreePrefixRankedPattern = registration::AbstractRegister<arbology::exact::KnuthMorrisPratt, ext::set<unsigned>, const tree::PrefixRankedTree<>&, const tree::PrefixRankedPattern<>&>(arbology::exact::KnuthMorrisPratt::match);

auto KnuthMorrisPrattPrefixRankedTreePrefixRankedExtendedPattern = registration::AbstractRegister<arbology::exact::KnuthMorrisPratt, ext::set<unsigned>, const tree::PrefixRankedTree<>&, const tree::PrefixRankedExtendedPattern<>&>(arbology::exact::KnuthMorrisPratt::match);

auto KnuthMorrisPrattPrefixRankedTreePrefixRankedNonlinearPattern = registration::AbstractRegister<arbology::exact::KnuthMorrisPratt, ext::set<unsigned>, const tree::PrefixRankedTree<>&, const tree::PrefixRankedNonlinearPattern<>&>(arbology::exact::KnuthMorrisPratt::match);

} /* namespace */
