#include <registration/AlgoRegistration.hpp>
#include "ExactTreePatternAutomaton.h"

namespace {

auto ExactTreePatternAutomatonPrefixRankedTree = registration::AbstractRegister<arbology::exact::ExactTreePatternAutomaton, automaton::InputDrivenNPDA<common::ranked_symbol<DefaultSymbolType>, char, unsigned>, const tree::PrefixRankedTree<>&, const common::ranked_symbol<DefaultSymbolType>&>(arbology::exact::ExactTreePatternAutomaton::construct);

auto ExactTreePatternAutomatonPrefixRankedBarTree = registration::AbstractRegister<arbology::exact::ExactTreePatternAutomaton, automaton::InputDrivenNPDA<common::ranked_symbol<DefaultSymbolType>, char, unsigned>, const tree::PrefixRankedBarTree<>&, const common::ranked_symbol<DefaultSymbolType>&, const common::ranked_symbol<DefaultSymbolType>&>(arbology::exact::ExactTreePatternAutomaton::construct);

} /* namespace */
