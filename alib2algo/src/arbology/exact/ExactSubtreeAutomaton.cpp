#include <registration/AlgoRegistration.hpp>
#include "ExactSubtreeAutomaton.h"

namespace {

auto ExactSubtreeAutomatonPrefixRankedTree = registration::AbstractRegister<arbology::exact::ExactSubtreeAutomaton, automaton::InputDrivenNPDA<common::ranked_symbol<DefaultSymbolType>, char, unsigned>, const tree::PrefixRankedTree<>&>(arbology::exact::ExactSubtreeAutomaton::construct);

auto ExactSubtreeAutomatonRankedTree = registration::AbstractRegister<arbology::exact::ExactSubtreeAutomaton, automaton::NFTA<DefaultSymbolType, unsigned>, const tree::RankedTree<>&>(arbology::exact::ExactSubtreeAutomaton::construct);

} /* namespace */
