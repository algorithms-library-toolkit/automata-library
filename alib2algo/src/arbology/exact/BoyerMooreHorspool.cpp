#include <registration/AlgoRegistration.hpp>
#include "BoyerMooreHorspool.h"

namespace {

auto BoyerMooreHorspoolPrefixRankedBarTreePrefixRankedBarTree = registration::AbstractRegister<arbology::exact::BoyerMooreHorspool, ext::set<unsigned>, const tree::PrefixRankedBarTree<>&, const tree::PrefixRankedBarTree<>&>(arbology::exact::BoyerMooreHorspool::match);
auto BoyerMooreHorspoolPrefixRankedBarTreePrefixRankedBarPattern = registration::AbstractRegister<arbology::exact::BoyerMooreHorspool, ext::set<unsigned>, const tree::PrefixRankedBarTree<>&, const tree::PrefixRankedBarPattern<>&>(arbology::exact::BoyerMooreHorspool::match);
auto BoyerMooreHorspoolPrefixRankedBarTreePrefixRankedBarNonlinearPattern = registration::AbstractRegister<arbology::exact::BoyerMooreHorspool, ext::set<unsigned>, const tree::PrefixRankedBarTree<>&, const tree::PrefixRankedBarNonlinearPattern<>&>(arbology::exact::BoyerMooreHorspool::match);

} /* namespace */
