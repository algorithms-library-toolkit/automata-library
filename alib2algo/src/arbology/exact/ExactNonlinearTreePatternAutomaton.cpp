#include <registration/AlgoRegistration.hpp>
#include "ExactNonlinearTreePatternAutomaton.h"

namespace {

auto ExactNonlinearTreePatternAutomatonPrefixRankedTree = registration::AbstractRegister<arbology::exact::ExactNonlinearTreePatternAutomaton, automaton::InputDrivenNPDA<common::ranked_symbol<DefaultSymbolType>, char, ext::pair<unsigned, unsigned>>, const tree::PrefixRankedTree<>&, const common::ranked_symbol<DefaultSymbolType>&, const ext::set<common::ranked_symbol<DefaultSymbolType>>&>(arbology::exact::ExactNonlinearTreePatternAutomaton::construct);

auto ExactNonlinearTreePatternAutomatonPrefixRankedBarTree = registration::AbstractRegister<arbology::exact::ExactNonlinearTreePatternAutomaton, automaton::InputDrivenNPDA<common::ranked_symbol<DefaultSymbolType>, char, ext::pair<unsigned, unsigned>>, const tree::PrefixRankedBarTree<>&, const common::ranked_symbol<DefaultSymbolType>&, const ext::set<common::ranked_symbol<DefaultSymbolType>>&, const common::ranked_symbol<DefaultSymbolType>&>(arbology::exact::ExactNonlinearTreePatternAutomaton::construct);

} /* namespace */
