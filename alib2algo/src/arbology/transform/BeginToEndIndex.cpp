#include <registration/AlgoRegistration.hpp>
#include "BeginToEndIndex.h"

namespace {

auto BeginToEndIndexPrefixRankedBarTreePrefixRankedBarTree = registration::AbstractRegister<arbology::transform::BeginToEndIndex, ext::set<unsigned>, const tree::PrefixRankedBarTree<>&, const ext::set<unsigned>&>(arbology::transform::BeginToEndIndex::transform);
auto BeginToEndIndexPrefixRankedBarTreePrefixRankedBarPattern = registration::AbstractRegister<arbology::transform::BeginToEndIndex, ext::set<unsigned>, const tree::PrefixRankedTree<>&, const ext::set<unsigned>&>(arbology::transform::BeginToEndIndex::transform);
auto BeginToEndIndexPrefixdBarTree = registration::AbstractRegister<arbology::transform::BeginToEndIndex, ext::set<unsigned>, const tree::PrefixBarTree<>&, const ext::set<unsigned>&>(arbology::transform::BeginToEndIndex::transform);

} /* namespace */
