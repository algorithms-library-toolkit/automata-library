#pragma once

#include <alib/set>
#include <tree/properties/SubtreeJumpTable.h>

#include <tree/ranked/PrefixRankedBarTree.h>
#include <tree/ranked/PrefixRankedTree.h>
#include <tree/unranked/PrefixBarTree.h>

namespace arbology::transform {

/**
 */
class BeginToEndIndex {
public:
    /**
     * Search for pattern in linear string.
     * @return set set of occurences
     */
    template <class Tree>
    static ext::set<unsigned> transform(const Tree& subject, const ext::set<unsigned>& indexes);
};

template <class Tree>
ext::set<unsigned> BeginToEndIndex::transform(const Tree& subject, const ext::set<unsigned>& indexes)
{
    ext::vector<int> subjectSubtreeJumpTable = tree::properties::SubtreeJumpTable::compute(subject);
    ext::set<unsigned> res;

    for (unsigned index : indexes)
        res.insert(subjectSubtreeJumpTable[index]);

    return res;
}

} /* namespace arbology::transform */
