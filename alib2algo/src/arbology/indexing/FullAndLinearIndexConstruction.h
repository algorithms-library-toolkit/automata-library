#pragma once

#include <indexes/arbology/FullAndLinearIndex.h>
#include <stringology/indexing/PositionHeapNaive.h>
#include <tree/properties/SubtreeJumpTable.h>
#include <tree/ranked/PrefixRankedBarTree.h>
#include <tree/ranked/PrefixRankedTree.h>

namespace arbology {

namespace indexing {

/**
 * Constructs a compressed bit parallel index for given tree.
 *
 */

class FullAndLinearIndexConstruction {
public:
    /**
     * Creates compressed bit parallel index for trees
     * @param tree tree to construct the index for
     * @return the index
     */
    template <class SymbolType, template <typename> class StringIndex = indexes::stringology::PositionHeap, class StringIndexConstructionAlgo = stringology::indexing::PositionHeapNaive>
    static indexes::arbology::FullAndLinearIndex<SymbolType, StringIndex> construct(const tree::PrefixRankedTree<SymbolType>& w);

    template <class SymbolType, template <typename> class StringIndex = indexes::stringology::PositionHeap, class StringIndexConstructionAlgo = stringology::indexing::PositionHeapNaive>
    static indexes::arbology::FullAndLinearIndex<SymbolType, StringIndex> construct(const tree::PrefixRankedBarTree<SymbolType>& w);
};

template <class SymbolType, template <typename> class StringIndex, class StringIndexConstructionAlgo>
indexes::arbology::FullAndLinearIndex<SymbolType, StringIndex> FullAndLinearIndexConstruction::construct(const tree::PrefixRankedTree<SymbolType>& w)
{
    return indexes::arbology::FullAndLinearIndex<SymbolType, StringIndex>(StringIndexConstructionAlgo::construct(string::LinearString<common::ranked_symbol<SymbolType>>(w)), tree::properties::SubtreeJumpTable::compute(w));
}

template <class SymbolType, template <typename> class StringIndex, class StringIndexConstructionAlgo>
indexes::arbology::FullAndLinearIndex<SymbolType, StringIndex> FullAndLinearIndexConstruction::construct(const tree::PrefixRankedBarTree<SymbolType>& w)
{
    return indexes::arbology::FullAndLinearIndex<SymbolType, StringIndex>(StringIndexConstructionAlgo::construct(string::LinearString<common::ranked_symbol<SymbolType>>(w)), tree::properties::SubtreeJumpTable::compute(w));
}

} /* namespace indexing */

} /* namespace arbology */
