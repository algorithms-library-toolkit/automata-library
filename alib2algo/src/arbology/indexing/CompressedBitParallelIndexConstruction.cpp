#include <registration/AlgoRegistration.hpp>
#include "CompressedBitParallelIndexConstruction.h"

namespace {

auto compressedBitParallelIndexConstructionPrefixRankedTree = registration::AbstractRegister<arbology::indexing::CompressedBitParallelIndexConstruction, indexes::arbology::CompressedBitParallelTreeIndex<DefaultSymbolType>, const tree::PrefixRankedTree<>&>(arbology::indexing::CompressedBitParallelIndexConstruction::construct);

auto compressedBitParallelIndexConstructionPrefixRankedBarTree = registration::AbstractRegister<arbology::indexing::CompressedBitParallelIndexConstruction, indexes::arbology::CompressedBitParallelTreeIndex<DefaultSymbolType>, const tree::PrefixRankedBarTree<>&>(arbology::indexing::CompressedBitParallelIndexConstruction::construct);

} /* namespace */
