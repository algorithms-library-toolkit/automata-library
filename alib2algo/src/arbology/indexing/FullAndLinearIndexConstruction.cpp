#include <registration/AlgoRegistration.hpp>
#include "FullAndLinearIndexConstruction.h"

namespace {

auto fullAndLinearIndexConstructionPrefixRankedTree = registration::AbstractRegister<arbology::indexing::FullAndLinearIndexConstruction, indexes::arbology::FullAndLinearIndex<DefaultSymbolType>, const tree::PrefixRankedTree<>&>(arbology::indexing::FullAndLinearIndexConstruction::construct);

auto fullAndLinearIndexConstructionPrefixRankedBarTree = registration::AbstractRegister<arbology::indexing::FullAndLinearIndexConstruction, indexes::arbology::FullAndLinearIndex<DefaultSymbolType>, const tree::PrefixRankedBarTree<>&>(arbology::indexing::FullAndLinearIndexConstruction::construct);

} /* namespace */
