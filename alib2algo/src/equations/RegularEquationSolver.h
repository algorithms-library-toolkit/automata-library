#pragma once

#include <queue>

#include <ext/algorithm>

#include <alib/deque>
#include <alib/map>

#include <exception/CommonException.h>

#include <regexp/unbounded/UnboundedRegExp.h>
#include <regexp/unbounded/UnboundedRegExpElement.h>
#include <regexp/unbounded/UnboundedRegExpElements.h>

#include <regexp/simplify/RegExpOptimize.h>

namespace equations {

/**
 * Base class for regular equations solvers.
 */
template <class TerminalSymbolType, class VariableSymbolType>
class RegularEquationSolver {
public:
    virtual ~RegularEquationSolver() noexcept = default;

    /**
     * Adds nonterminal symbol into system.
     *
     * @param symbol given symbol
     */
    void addVariableSymbol(const VariableSymbolType& symbol);

    /**
     * Removes nonterminal symbol from equation system.
     *
     * @param symbol given symbol
     * @throws CommonException when symbol is in use
     */
    void removeVariableSymbol(const VariableSymbolType& symbol);

    /**
     * Sets nonterminal symbols of the equation system.
     * @param symbol Symbols to set
     * @throws CommonException
     */
    void setVariableSymbols(const ext::set<VariableSymbolType>& newSymbols);

    /**
     * Adds equation in form FROM = eq TO
     *
     * @param from symbol
     * @param to symbol
     * @param eq equation
     */
    void addEquation(const VariableSymbolType& from, const VariableSymbolType& to, const regexp::UnboundedRegExpElement<TerminalSymbolType>& eq);

    /**
     * Adds equation in form: FROM = eq
     *
     * @param from
     * @param eq
     */
    void addEquation(const VariableSymbolType& from, const regexp::UnboundedRegExpElement<TerminalSymbolType>& eq);

    /**
     * Solve expression system
     *
     * @param solveFor will solve equation system for given symbol
     * @return regexp
     */
    regexp::UnboundedRegExp<TerminalSymbolType> solve(const VariableSymbolType& solveFor);

protected:
    /**
     * Implementation of order specific concatenation.
     * @return the resulting concatenation
     */
    virtual regexp::UnboundedRegExpConcatenation<TerminalSymbolType> concatenate(regexp::UnboundedRegExpElement<TerminalSymbolType>&&, regexp::UnboundedRegExpElement<TerminalSymbolType>&&) = 0;

private:
    /**
     * actual equations elimination
     * @return pointer to solutions RegExp tree root
     */
    regexp::UnboundedRegExp<TerminalSymbolType> eliminate();

    /**
     * Runs BFS to determine depth of symbols in equation system and stores it in nonterminalSymbolsByDepth;
     * @see nonterminalSymbolsByDepth
     */
    void sortSymbolsByDepth(const VariableSymbolType& solveFor);

    /**
     * @see symbolsByDepth
     */
    ext::deque<VariableSymbolType> nonterminalSymbolsByDepth;

    /**
     * Stores transitions from nonterminal to nonterminal, eg A = 2A + 2B + 1C
     */
    ext::map<std::pair<VariableSymbolType, VariableSymbolType>, regexp::UnboundedRegExpAlternation<TerminalSymbolType>> equationTransition;

    /**
     * Stores equation not going to particular nonterminal, eg A = 01*
     */
    ext::map<VariableSymbolType, regexp::UnboundedRegExpAlternation<TerminalSymbolType>> equationFinal;

    /**
     * Set of symbols
     */
    ext::set<VariableSymbolType> nonterminalSymbols;
};

template <class TerminalSymbolType, class VariableSymbolType>
void RegularEquationSolver<TerminalSymbolType, VariableSymbolType>::setVariableSymbols(const ext::set<VariableSymbolType>& newSymbols)
{
    ext::set<VariableSymbolType> removed;
    ext::set<VariableSymbolType> added;

    ext::set_symmetric_difference(nonterminalSymbols.begin(), nonterminalSymbols.end(), newSymbols.begin(), newSymbols.end(), std::inserter(removed, removed.end()), std::inserter(added, added.end()));

    for (const VariableSymbolType& symbol : removed) {
        removeVariableSymbol(symbol);
    }

    for (const VariableSymbolType& symbol : added) {
        addVariableSymbol(symbol);
    }
}

template <class TerminalSymbolType, class VariableSymbolType>
void RegularEquationSolver<TerminalSymbolType, VariableSymbolType>::removeVariableSymbol(const VariableSymbolType& symbol)
{
    for (const auto& kv : equationTransition) {
        const VariableSymbolType& from = kv.first.first;
        const VariableSymbolType& to = kv.first.second;
        const regexp::UnboundedRegExpAlternation<TerminalSymbolType>& alt = kv.second;

        if ((from == symbol || to == symbol) && !alt.getElements().empty()) {
            throw exception::CommonException("Symbol '" + ext::to_string(symbol) + "' is in use.");
        }
    }

    for (const auto& kv : equationFinal) {
        const VariableSymbolType& from = kv.first;
        const regexp::UnboundedRegExpAlternation<TerminalSymbolType>& alt = kv.second;

        if (from == symbol && !alt.getElements().empty()) {
            throw exception::CommonException("Symbol '" + ext::to_string(from) + "' is in use.");
        }
    }


    nonterminalSymbols.erase(nonterminalSymbols.find(symbol));
    equationFinal.erase(equationFinal.find(symbol));

    for (const VariableSymbolType& variable : nonterminalSymbols) {
        equationTransition.erase(equationTransition.find(std::make_pair(variable, symbol)));
        equationTransition.erase(equationTransition.find(std::make_pair(symbol, variable)));
    }

    equationTransition.erase(equationTransition.find(std::make_pair(symbol, symbol)));
}

template <class TerminalSymbolType, class VariableSymbolType>
void RegularEquationSolver<TerminalSymbolType, VariableSymbolType>::addVariableSymbol(const VariableSymbolType& symbol)
{
    for (const VariableSymbolType& variable : nonterminalSymbols) {
        equationTransition.insert(std::make_pair(std::make_pair(symbol, variable), regexp::UnboundedRegExpAlternation<TerminalSymbolType>{}));
        equationTransition.insert(std::make_pair(std::make_pair(variable, symbol), regexp::UnboundedRegExpAlternation<TerminalSymbolType>{}));
    }

    equationTransition.insert(std::make_pair(std::make_pair(symbol, symbol), regexp::UnboundedRegExpAlternation<TerminalSymbolType>{}));

    nonterminalSymbols.insert(symbol);
    equationFinal.insert(std::make_pair(symbol, regexp::UnboundedRegExpAlternation<TerminalSymbolType>{}));
}

template <class TerminalSymbolType, class VariableSymbolType>
void RegularEquationSolver<TerminalSymbolType, VariableSymbolType>::addEquation(const VariableSymbolType& from, const VariableSymbolType& to, const regexp::UnboundedRegExpElement<TerminalSymbolType>& eq)
{
    if (nonterminalSymbols.count(from) == 0) {
        throw exception::CommonException("Symbol from ('" + ext::to_string(from) + "') is not in equation system.");
    }

    if (nonterminalSymbols.count(to) == 0) {
        throw exception::CommonException("Symbol to ('" + ext::to_string(to) + "') is not in equation system.");
    }

    equationTransition.find(std::make_pair(from, to))->second.appendElement(eq);
}

template <class TerminalSymbolType, class VariableSymbolType>
void RegularEquationSolver<TerminalSymbolType, VariableSymbolType>::addEquation(const VariableSymbolType& from, const regexp::UnboundedRegExpElement<TerminalSymbolType>& eq)
{
    if (nonterminalSymbols.count(from) == 0) {
        throw exception::CommonException("Symbol from ('" + ext::to_string(from) + "') is not in equation system.");
    }

    equationFinal.find(from)->second.appendElement(eq);
}

template <class TerminalSymbolType, class VariableSymbolType>
void RegularEquationSolver<TerminalSymbolType, VariableSymbolType>::sortSymbolsByDepth(const VariableSymbolType& solveFor)
{
    ext::set<VariableSymbolType> visited;
    std::queue<VariableSymbolType> queue;

    visited.insert(solveFor);
    queue.push(solveFor);

    while (!queue.empty()) {
        VariableSymbolType variable = std::move(queue.front());
        queue.pop();

        nonterminalSymbolsByDepth.push_back(variable);

        for (const auto& kv : equationTransition)
            // find all transitions from current symbol that are non-empty, enqueue transition'variable target symbol
            if (kv.first.first == variable && !kv.second.getElements().empty() && visited.insert(kv.first.second).second)
                queue.push(kv.first.second);
    }
}

template <class TerminalSymbolType, class VariableSymbolType>
regexp::UnboundedRegExp<TerminalSymbolType> RegularEquationSolver<TerminalSymbolType, VariableSymbolType>::solve(const VariableSymbolType& solveFor)
{
    if (nonterminalSymbols.count(solveFor) == 0) {
        throw exception::CommonException("Symbol solveFor ('" + ext::to_string(solveFor) + "') is not in equation system.");
    }

    /*
     * Firstly, organize states by depth so we can output better looking
     * expressions. We need to solve equation system for automaton's initial state,
     * so lets start with the deepest ones and walk towards the initial one.
     */
    sortSymbolsByDepth(solveFor);
    return eliminate();
}

template <class TerminalSymbolType, class VariableSymbolType>
regexp::UnboundedRegExp<TerminalSymbolType> RegularEquationSolver<TerminalSymbolType, VariableSymbolType>::eliminate()
{
    for (auto itA = this->nonterminalSymbolsByDepth.rbegin(); itA != this->nonterminalSymbolsByDepth.rend(); ++itA) {
        const VariableSymbolType& a = *itA;

        regexp::UnboundedRegExpIteration<TerminalSymbolType> loop(std::move(this->equationTransition.at(std::make_pair(a, a))));
        regexp::simplify::RegExpOptimize::optimize(loop);

        // for all transitions from A apply Arden's Lemma
        for (auto itB = std::next(itA); itB != this->nonterminalSymbolsByDepth.rend(); ++itB) {
            const VariableSymbolType& b = *itB;

            regexp::UnboundedRegExpConcatenation<TerminalSymbolType> concat = concatenate(ext::move_copy(loop), std::move(this->equationTransition.at(std::make_pair(a, b))));
            regexp::UnboundedRegExpAlternation<TerminalSymbolType> alt;
            alt.appendElement(std::move(concat));
            this->equationTransition.at(std::make_pair(a, b)) = std::move(alt);
            regexp::simplify::RegExpOptimize::optimize(this->equationTransition.at(std::make_pair(a, b)));
        }

        {
            regexp::UnboundedRegExpConcatenation<TerminalSymbolType> concat = concatenate(std::move(loop), std::move(this->equationFinal.at(a)));
            regexp::UnboundedRegExpAlternation<TerminalSymbolType> alt;
            alt.appendElement(std::move(concat));
            this->equationFinal.at(a) = std::move(alt);
            regexp::simplify::RegExpOptimize::optimize(this->equationFinal.at(a));
        }

        /*
         * eliminate A from rest of the equations using this pattern:
         * B->C = B->C + concatenate(B->A, A->C)
         */
        for (auto itB = std::next(itA); itB != this->nonterminalSymbolsByDepth.rend(); ++itB) {
            const VariableSymbolType& b = *itB;

            for (auto itC = std::next(itA); itC != this->nonterminalSymbolsByDepth.rend(); ++itC) {
                const VariableSymbolType& c = *itC;

                regexp::UnboundedRegExpConcatenation<TerminalSymbolType> concat = concatenate(ext::move_copy(this->equationTransition.at(std::make_pair(b, a))), ext::move_copy(this->equationTransition.at(std::make_pair(a, c))));
                regexp::UnboundedRegExpAlternation<TerminalSymbolType> alt;
                alt.appendElement(std::move(this->equationTransition.at(std::make_pair(b, c))));
                alt.appendElement(std::move(concat));
                this->equationTransition.at(std::make_pair(b, c)) = std::move(alt);
                regexp::simplify::RegExpOptimize::optimize(this->equationTransition.at(std::make_pair(b, c)));
            }

            {
                regexp::UnboundedRegExpConcatenation<TerminalSymbolType> concat = concatenate(std::move(this->equationTransition.at(std::make_pair(b, a))), ext::move_copy(this->equationFinal.at(a)));
                regexp::UnboundedRegExpAlternation<TerminalSymbolType> alt;
                alt.appendElement(std::move(this->equationFinal.at(b)));
                alt.appendElement(std::move(concat));
                this->equationFinal.at(b) = std::move(alt);
                regexp::simplify::RegExpOptimize::optimize(this->equationFinal.at(b));
            }
        }
    }

    return regexp::UnboundedRegExp<TerminalSymbolType>(regexp::simplify::RegExpOptimize::optimize(regexp::UnboundedRegExpStructure<TerminalSymbolType>(std::move(this->equationFinal.at(*this->nonterminalSymbolsByDepth.begin())))));
}

} /* namespace equations */
