#include <registration/AlgoRegistration.hpp>
#include "ToFTAThompson.h"

namespace {

auto ToFTAThompsonFormalRTE = registration::AbstractRegister<rte::convert::ToFTAThompson,
                                                             automaton::EpsilonNFTA<DefaultSymbolType, unsigned>,
                                                             const rte::FormalRTE<>&>(rte::convert::ToFTAThompson::convert);

} /* namespace */
