#include <registration/AlgoRegistration.hpp>
#include "ToPostfixPushdownAutomaton.h"

namespace {

auto ToAutomatonFormalRegExp = registration::AbstractRegister<rte::convert::ToPostfixPushdownAutomaton, automaton::NPDA<ext::variant<common::ranked_symbol<DefaultSymbolType>, alphabet::End>, ext::variant<common::ranked_symbol<ext::pair<DefaultSymbolType, unsigned>>, alphabet::BottomOfTheStack>, char>, const rte::FormalRTE<>&>(rte::convert::ToPostfixPushdownAutomaton::convert);

} /* namespace */
