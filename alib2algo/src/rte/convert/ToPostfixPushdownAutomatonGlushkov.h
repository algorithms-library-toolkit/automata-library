#pragma once

#include <automaton/convert/ToPostfixPushdownAutomaton.h>
#include "ToFTAGlushkov.h"

namespace rte {

namespace convert {

/**
 * Converts regular tree expression to a real-time height-deterministic pda
 *
 * Sources:
 *  - Tomas Pecka, Master Thesis 2016
 *  - Construction of a Pushdown Automaton Accepting a Postfix Notation of a Tree Language Given by a Regular Tree Expression @ SLATE 2018
 */
class ToPostfixPushdownAutomatonGlushkov {

public:
    /**
     * Implements conversion of the regular tree expressions to a real-time height-deterministic pushdown automaton using algorithm similar to Glushkov's method of neighbours.
     *
     * \tparam SymbolType the type of symbols in the regular expression
     * \tparam RankType the type of symbol ranks in the regular expression
     *
     * \param rte the converted regexp to convert
     *
     * \return real-time height-determinitic pushdown automaton accepting the language described by the original regular tree expression
     */
    template <class SymbolType>
    static automaton::NPDA<ext::variant<common::ranked_symbol<SymbolType>, alphabet::End>, ext::variant<ext::set<common::ranked_symbol<ext::pair<SymbolType, unsigned>>>, alphabet::BottomOfTheStack>, char> convert(const rte::FormalRTE<SymbolType>& rte);
};

template <class SymbolType>
automaton::NPDA<ext::variant<common::ranked_symbol<SymbolType>, alphabet::End>, ext::variant<ext::set<common::ranked_symbol<ext::pair<SymbolType, unsigned>>>, alphabet::BottomOfTheStack>, char> ToPostfixPushdownAutomatonGlushkov::convert(const rte::FormalRTE<SymbolType>& rte)
{
    return automaton::convert::ToPostfixPushdownAutomaton::convert(ToFTAGlushkov::convert(rte));
}

} /* namespace convert */

} /* namespace rte */
