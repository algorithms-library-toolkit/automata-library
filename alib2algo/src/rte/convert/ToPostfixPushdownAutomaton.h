#pragma once

#include <alib/pair>

#include <automaton/PDA/NPDA.h>
#include <rte/formal/FormalRTE.h>
#include "ToPostfixPushdownAutomatonGlushkovNaive.h"

namespace rte {

namespace convert {

/**
 * Converts a tree regular expression to a pushdown automaton.
 *
 * This class serves as a "default wrapper" over the conversion of RTE to PDA. It delegates to the conversion algorithm similar to glushkov construction.
 *
 * @sa rte::convert::ToPostfixPushdownAutomatonGlushkov
 */
class ToPostfixPushdownAutomaton {
public:
    /**
     * Implements conversion of the tree regular expressions to a pushdown automaton using Glushkov's method of neighbours.
     *
     * \tparam SymbolType the type of symbols in the regular expression
     * \tparam RankType the type of symbol ranks in the regular expression
     *
     * \param regexp the regexp to convert
     *
     * \return PDA equivalent to original regular rte expression reading linearized postfix tree
     */
    template <class SymbolType>
    static automaton::NPDA<ext::variant<common::ranked_symbol<SymbolType>, alphabet::End>, ext::variant<common::ranked_symbol<ext::pair<SymbolType, unsigned>>, alphabet::BottomOfTheStack>, char> convert(const rte::FormalRTE<SymbolType>& rte);
};

template <class SymbolType>
automaton::NPDA<ext::variant<common::ranked_symbol<SymbolType>, alphabet::End>, ext::variant<common::ranked_symbol<ext::pair<SymbolType, unsigned>>, alphabet::BottomOfTheStack>, char> ToPostfixPushdownAutomaton::convert(const rte::FormalRTE<SymbolType>& rte)
{
    return ToPostfixPushdownAutomatonGlushkovNaive::convert(rte);
}

} /* namespace convert */

} /* namespace rte */
