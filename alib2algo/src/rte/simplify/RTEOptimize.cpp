#include <registration/AlgoRegistration.hpp>
#include "RTEOptimize.h"

namespace {

auto RTEOptimizeFormalRTE = registration::AbstractRegister<rte::simplify::RTEOptimize, rte::FormalRTE<>, const rte::FormalRTE<>&>(rte::simplify::RTEOptimize::optimize, "rte").setDocumentation("Implements a rte simplification algorithm that is transforming the regular expression to be smaller.\n\
\n\
@param rte the simplified rte\n\
@return the simlified rte");

} /* namespace */
