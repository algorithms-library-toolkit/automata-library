#include <registration/AlgoRegistration.hpp>
#include "ToAutomaton.h"

namespace {

auto ToAutomatonFormalRegExp = registration::AbstractRegister<regexp::convert::ToAutomaton, automaton::NFA<DefaultSymbolType, ext::pair<DefaultSymbolType, unsigned>>, const regexp::FormalRegExp<>&>(regexp::convert::ToAutomaton::convert, "regexp").setDocumentation("Converts the regular expression into an automaton (@sa regexp::convert::ToGlushkovAutomaton::convert).\n\
\n\
@param regexp the regular expression\n\
@return finite automaton equivalent to original regular expression");

auto ToAutomatonUnboundedRegExp = registration::AbstractRegister<regexp::convert::ToAutomaton, automaton::NFA<DefaultSymbolType, ext::pair<DefaultSymbolType, unsigned>>, const regexp::UnboundedRegExp<>&>(regexp::convert::ToAutomaton::convert, "regexp").setDocumentation("Converts the regular expression into an automaton (@sa regexp::convert::ToGlushkovAutomaton::convert).\n\
\n\
@param regexp the regular expression\n\
@return finite automaton equivalent to original regular expression");

} /* namespace */
