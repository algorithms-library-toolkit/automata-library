#pragma once

#include "ToGrammarRightRGGlushkov.h"

namespace regexp {

namespace convert {

/**
 * Conversion of regular expression to regular grammar.
 * This class serves as a "default wrapper" over the conversion of RE to RG. It delegates to the glushkov conversion algorithm.
 * @sa regexp::convert::ToGrammarRightRGGlushkov
 */
class ToGrammar {
public:
    /**
     * Converts the regular expression into a grammar (@sa regexp::convert::ToGrammarRightRGGlushkov::convert).
     *
     * \tparam SymbolType the type of regular expression
     *
     * \param regexp the regular expression
     *
     * \return right regular grammar equivalent to original regular expression.
     */
    template <class SymbolType>
    static grammar::RightRG<SymbolType, ext::pair<SymbolType, unsigned>> convert(const regexp::FormalRegExp<SymbolType>& regexp);

    /**
     * \overload
     */
    template <class SymbolType>
    static grammar::RightRG<SymbolType, ext::pair<SymbolType, unsigned>> convert(const regexp::UnboundedRegExp<SymbolType>& regexp);
};

template <class SymbolType>
grammar::RightRG<SymbolType, ext::pair<SymbolType, unsigned>> ToGrammar::convert(const regexp::FormalRegExp<SymbolType>& regexp)
{
    return ToGrammarRightRGGlushkov::convert(regexp);
}

template <class SymbolType>
grammar::RightRG<SymbolType, ext::pair<SymbolType, unsigned>> ToGrammar::convert(const regexp::UnboundedRegExp<SymbolType>& regexp)
{
    return ToGrammarRightRGGlushkov::convert(regexp);
}

} /* namespace convert */

} /* namespace regexp */
