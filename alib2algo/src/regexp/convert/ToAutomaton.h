#pragma once

#include <alib/pair>

#include <regexp/formal/FormalRegExp.h>
#include <regexp/unbounded/UnboundedRegExp.h>

#include "ToAutomatonGlushkov.h"

namespace regexp {

namespace convert {

/**
 * Conversion of regular expression to finite automata.
 * This class serves as a "default wrapper" over the conversion of RE to FA. It delegates to the glushkov conversion algorithm.
 * @sa regexp::convert::ToGlushkovAutomaton
 */
class ToAutomaton {
public:
    /**
     * Converts the regular expression into an automaton (@sa regexp::convert::ToGlushkovAutomaton::convert).
     *
     * \tparam SymbolType the type of regular expression
     *
     * \param regexp the regular expression
     *
     * \return finite automaton equivalent to original regular expression
     */
    template <class SymbolType>
    static automaton::NFA<SymbolType, ext::pair<SymbolType, unsigned>> convert(const regexp::FormalRegExp<SymbolType>& regexp);

    /**
     * Converts the regular expression into an automaton (@sa regexp::convert::ToGlushkovAutomaton::convert).
     *
     * \tparam SymbolType the type of regular expression
     *
     * \param regexp the regular expression
     *
     * \return finite automaton equivalent to original regular expression
     */
    template <class SymbolType>
    static automaton::NFA<SymbolType, ext::pair<SymbolType, unsigned>> convert(const regexp::UnboundedRegExp<SymbolType>& regexp);
};

template <class SymbolType>
automaton::NFA<SymbolType, ext::pair<SymbolType, unsigned>> ToAutomaton::convert(const regexp::FormalRegExp<SymbolType>& regexp)
{
    return ToAutomatonGlushkov::convert(regexp);
}

template <class SymbolType>
automaton::NFA<SymbolType, ext::pair<SymbolType, unsigned>> ToAutomaton::convert(const regexp::UnboundedRegExp<SymbolType>& regexp)
{
    return ToAutomatonGlushkov::convert(regexp);
}

} /* namespace convert */

} /* namespace regexp */
