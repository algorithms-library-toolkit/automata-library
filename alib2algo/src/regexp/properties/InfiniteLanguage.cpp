#include <regexp/formal/FormalRegExpElements.h>
#include <regexp/unbounded/UnboundedRegExpElements.h>
#include <registration/AlgoRegistration.hpp>
#include "InfiniteLanguage.h"

namespace {

auto FormalRegExp = registration::AbstractRegister<regexp::properties::InfiniteLanguage, bool, const regexp::FormalRegExp<>&>(regexp::properties::InfiniteLanguage::isInfiniteLanguage);
auto UnboundedRegExp = registration::AbstractRegister<regexp::properties::InfiniteLanguage, bool, const regexp::UnboundedRegExp<>&>(regexp::properties::InfiniteLanguage::isInfiniteLanguage);

} /* namespace */
