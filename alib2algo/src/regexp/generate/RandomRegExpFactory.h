#pragma once

#include <queue>

#include <alib/set>
#include <exception/CommonException.h>
#include <ext/algorithm>
#include <ext/ptr_vector>
#include <ext/random>
#include <regexp/unbounded/UnboundedRegExp.h>
#include <regexp/unbounded/UnboundedRegExpElements.h>
#include "extensions/random.hpp"
#include "regexp/formal/FormalRegExpIteration.h"

namespace regexp::generate {

/**
 * Generator of random regexps.
 *
 * The algorithm tries to generate a random regular expression with given number of leaf nodes and height.
 */
class RandomRegExpFactory {
public:
    /**
     * Generates a random regular expression.
     *
     * \tparam SymbolType the type of terminal symbols of the random automaton
     *
     * \param leafNodes number of leaf nodes in the generated regexp
     * \param height the height of the generated regular expression
     * \param alphabet the alphabet of the regular expression
     *
     * \return random regular expression
     */
    template <class SymbolType>
    static regexp::UnboundedRegExp<SymbolType> generateUnboundedRegExp(size_t leafNodes, size_t height, ext::set<SymbolType> alphabet);

    template <class SymbolType>
    static regexp::FormalRegExp<SymbolType> generateFormalRegExp(size_t leafNodes, ext::set<SymbolType> alphabet);

private:
    template <class SymbolType>
    static regexp::UnboundedRegExp<SymbolType> SimpleUnboundedRegExp(size_t n, size_t h, const ext::ptr_vector<regexp::UnboundedRegExpElement<SymbolType>>& elems);

    template <class SymbolType>
    static ext::ptr_value<regexp::UnboundedRegExpElement<SymbolType>> SimpleUnboundedRegExpElement(size_t n, size_t h, const ext::ptr_vector<regexp::UnboundedRegExpElement<SymbolType>>& elems);
};

template <class SymbolType>
regexp::UnboundedRegExp<SymbolType> RandomRegExpFactory::generateUnboundedRegExp(size_t leafNodes, size_t height, ext::set<SymbolType> alphabet)
{
    if (alphabet.empty())
        throw exception::CommonException("Alphabet size must be greater than 0.");

    ext::ptr_vector<regexp::UnboundedRegExpElement<SymbolType>> elems;

    {
        elems.push_back(regexp::UnboundedRegExpEmpty<SymbolType>());
        elems.push_back(regexp::UnboundedRegExpEpsilon<SymbolType>());
    }
    if (alphabet.size() > 6) {
        elems.push_back(regexp::UnboundedRegExpEmpty<SymbolType>());
        elems.push_back(regexp::UnboundedRegExpEpsilon<SymbolType>());
    }
    if (alphabet.size() > 16) {
        elems.push_back(regexp::UnboundedRegExpEmpty<SymbolType>());
        elems.push_back(regexp::UnboundedRegExpEpsilon<SymbolType>());
    }

    for (const SymbolType& symbol : alphabet) {
        elems.push_back(regexp::UnboundedRegExpSymbol<SymbolType>(symbol));
    }

    regexp::UnboundedRegExp<SymbolType> res = regexp::generate::RandomRegExpFactory::SimpleUnboundedRegExp(leafNodes, height, elems);

    return res;
}

template <class SymbolType>
regexp::UnboundedRegExp<SymbolType> RandomRegExpFactory::SimpleUnboundedRegExp(size_t n, size_t h, const ext::ptr_vector<regexp::UnboundedRegExpElement<SymbolType>>& elems)
{
    return regexp::UnboundedRegExp<SymbolType>(regexp::UnboundedRegExpStructure<SymbolType>(SimpleUnboundedRegExpElement(n, h, elems)));
}

template <class SymbolType>
ext::ptr_value<regexp::UnboundedRegExpElement<SymbolType>> RandomRegExpFactory::SimpleUnboundedRegExpElement(size_t n, size_t h, const ext::ptr_vector<regexp::UnboundedRegExpElement<SymbolType>>& elems)
{
    if (h == 0 || n == 0) {
        return ext::ptr_value<UnboundedRegExpElement<SymbolType>>(elems[ext::random_devices::semirandom() % elems.size()]);
    } else {
        unsigned childNodes = ext::random_devices::semirandom() % 10;
        if (childNodes < 4)
            childNodes = 1;
        else if (childNodes < 6)
            childNodes = 2;
        else if (childNodes < 8)
            childNodes = 3;
        else
            childNodes = 4;

        childNodes = childNodes > n ? n : childNodes;

        std::array<size_t, 4> subSizes;
        if (childNodes == 4) {
            subSizes[3] = ext::random_devices::semirandom() % (n - 1);
            subSizes[2] = ext::random_devices::semirandom() % (n - subSizes[3] - 1);
            subSizes[1] = ext::random_devices::semirandom() % (n - subSizes[2] - subSizes[3] - 1);

            subSizes[3] += 1;
            subSizes[2] += 1;
            subSizes[1] += 1;

            subSizes[0] = n - subSizes[1] - subSizes[2] - subSizes[3];
        }
        if (childNodes == 3) {
            subSizes[2] = ext::random_devices::semirandom() % (n - 1);
            subSizes[1] = ext::random_devices::semirandom() % (n - subSizes[2] - 1);

            subSizes[2] += 1;
            subSizes[1] += 1;

            subSizes[0] = n - subSizes[1] - subSizes[2];
        }
        if (childNodes == 2) {
            subSizes[1] = ext::random_devices::semirandom() % (n - 1);

            subSizes[1] += 1;

            subSizes[0] = n - subSizes[1];
        }
        if (childNodes == 1) {
            return ext::ptr_value<regexp::UnboundedRegExpElement<SymbolType>>(regexp::UnboundedRegExpIteration<SymbolType>(SimpleUnboundedRegExpElement(n, h - 1, elems)));
        }

        int nodeType = ext::random_devices::semirandom() % 2;
        if (nodeType == 0) {
            regexp::UnboundedRegExpConcatenation<SymbolType> con;
            for (unsigned i = 0; i < childNodes; i++) {
                con.appendElement(std::move(SimpleUnboundedRegExpElement(subSizes[i], h - 1, elems)));
            }
            return ext::ptr_value<regexp::UnboundedRegExpElement<SymbolType>>(std::move(con));
        } else {
            regexp::UnboundedRegExpAlternation<SymbolType> alt;
            for (unsigned i = 0; i < childNodes; i++) {
                alt.appendElement(std::move(SimpleUnboundedRegExpElement(subSizes[i], h - 1, elems)));
            }
            return ext::ptr_value<regexp::UnboundedRegExpElement<SymbolType>>(std::move(alt));
        }
    }
}

template <class SymbolType>
regexp::FormalRegExp<SymbolType> RandomRegExpFactory::generateFormalRegExp(size_t leafNodes, ext::set<SymbolType> alphabet)
{
    std::queue<ext::ptr_value<regexp::FormalRegExpElement<SymbolType>>> queue;

    const ext::vector<SymbolType> alphabetVector(alphabet.begin(), alphabet.end());
    for (size_t i = 0; i < leafNodes; ++i) {
        unsigned r = ext::random_devices::semirandom() % 1024;
        if (r < 150) {
            queue.push(ext::ptr_value<regexp::FormalRegExpElement<SymbolType>>(regexp::FormalRegExpEmpty<SymbolType>()));
        } else if (r < 350) {
            queue.push(ext::ptr_value<regexp::FormalRegExpElement<SymbolType>>(regexp::FormalRegExpEpsilon<SymbolType>()));
        } else {
            queue.push(ext::ptr_value<regexp::FormalRegExpElement<SymbolType>>(regexp::FormalRegExpSymbol<SymbolType>(alphabetVector[ext::random_devices::semirandom() % alphabetVector.size()])));
        }
    }

    while (queue.size() > 1) {
        if (ext::random_devices::semirandom() % 32 < 5) {
            auto child = std::move(queue.front());
            queue.pop();
            queue.push(std::move(child));
            continue;
        }

        unsigned int op = ext::random_devices::semirandom() % 4;

        // iterate in 25 % of the cases if child is not iteration as well. If it is, then the probability is 0.25 x 0.5
        bool isIter = dynamic_cast<const FormalRegExpIteration<SymbolType>*>(queue.front().operator->()) != nullptr;
        if (op == 0 && (!isIter || (isIter && op == 0 && ext::random_devices::semirandom() % 2 == 0))) {
            auto child = std::move(queue.front());
            queue.pop();
            queue.push(ext::ptr_value<regexp::FormalRegExpElement<SymbolType>>(regexp::FormalRegExpIteration<SymbolType>(child)));
        } else {
            auto left = std::move(queue.front());
            queue.pop();
            auto right = std::move(queue.front());
            queue.pop();

            if (ext::random_devices::semirandom() % 4 == 0) {
                queue.push(ext::ptr_value<regexp::FormalRegExpElement<SymbolType>>(regexp::FormalRegExpAlternation<SymbolType>(left, right)));
            } else {
                queue.push(ext::ptr_value<regexp::FormalRegExpElement<SymbolType>>(regexp::FormalRegExpConcatenation<SymbolType>(left, right)));
            }
        }
    }

    auto res = std::move(queue.front());

    if (ext::random_devices::semirandom() % 16 < 5) {
        res = ext::ptr_value<regexp::FormalRegExpElement<SymbolType>>(regexp::FormalRegExpIteration<SymbolType>(res));
    }

    return regexp::FormalRegExp<SymbolType>(regexp::FormalRegExpStructure<SymbolType>(res));
}

} /* namespace regexp::generate */
