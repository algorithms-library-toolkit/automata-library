#include <registration/AlgoRegistration.hpp>
#include "RandomRegExpFactory.h"

namespace regexp::generate {

auto GenerateUnboundedRegExp = registration::AbstractRegister<regexp::generate::RandomRegExpFactory, regexp::UnboundedRegExp<>, size_t, size_t, ext::set<DefaultSymbolType>>(regexp::generate::RandomRegExpFactory::generateUnboundedRegExp, abstraction::AlgorithmCategories::AlgorithmCategory::DEFAULT, "leafNodes", "height", "alphabet").setDocumentation("Generates a random regular expression.\n\
\n\
@param leafNodes number of leaf nodes in the generated regexp\n\
@param height the height of the generated regular expression\n\
@param alphabet the alphabet of the regular expression\n\
\n\
@return random regular expression");

auto GenerateFormalRegExp = registration::AbstractRegister<regexp::generate::RandomRegExpFactory, regexp::FormalRegExp<>, size_t, ext::set<DefaultSymbolType>>(regexp::generate::RandomRegExpFactory::generateFormalRegExp, abstraction::AlgorithmCategories::AlgorithmCategory::DEFAULT, "leafNodes", "alphabet");

} /* namespace regexp::generate */
