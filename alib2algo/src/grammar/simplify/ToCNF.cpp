#include <registration/AlgoRegistration.hpp>
#include "ToCNF.h"

namespace {

auto ToCNFCFG = registration::AbstractRegister<grammar::simplify::ToCNF, grammar::CNF<DefaultSymbolType, ext::vector<ext::variant<DefaultSymbolType, DefaultSymbolType>>>, const grammar::CFG<>&>(grammar::simplify::ToCNF::convert, "grammar").setDocumentation("Implements transformation of a grammar into chomsky's normal form.\n\
\n\
@param grammar the transformed grammar\n\
@return an grammar in chomsky's normal form equivalent to the @p grammar");

auto ToCNFEpsilonFreeCFG = registration::AbstractRegister<grammar::simplify::ToCNF, grammar::CNF<DefaultSymbolType, ext::vector<ext::variant<DefaultSymbolType, DefaultSymbolType>>>, const grammar::EpsilonFreeCFG<>&>(grammar::simplify::ToCNF::convert, "grammar").setDocumentation("Implements transformation of a grammar into chomsky's normal form.\n\
\n\
@param grammar the transformed grammar\n\
@return an grammar in chomsky's normal form equivalent to the @p grammar");

auto ToCNFCNF = registration::AbstractRegister<grammar::simplify::ToCNF, grammar::CNF<>, const grammar::CNF<>&>(grammar::simplify::ToCNF::convert, "grammar").setDocumentation("Implements transformation of a grammar into chomsky's normal form.\n\
\n\
@param grammar the transformed grammar\n\
@return an grammar in chomsky's normal form equivalent to the @p grammar");

auto ToCNFGNF = registration::AbstractRegister<grammar::simplify::ToCNF, grammar::CNF<DefaultSymbolType, ext::vector<ext::variant<DefaultSymbolType, DefaultSymbolType>>>, const grammar::GNF<>&>(grammar::simplify::ToCNF::convert, "grammar").setDocumentation("Implements transformation of a grammar into chomsky's normal form.\n\
\n\
@param grammar the transformed grammar\n\
@return an grammar in chomsky's normal form equivalent to the @p grammar");

auto ToCNFLG = registration::AbstractRegister<grammar::simplify::ToCNF, grammar::CNF<DefaultSymbolType, ext::vector<ext::variant<DefaultSymbolType, DefaultSymbolType>>>, const grammar::LG<>&>(grammar::simplify::ToCNF::convert, "grammar").setDocumentation("Implements transformation of a grammar into chomsky's normal form.\n\
\n\
@param grammar the transformed grammar\n\
@return an grammar in chomsky's normal form equivalent to the @p grammar");

auto ToCNFLeftLG = registration::AbstractRegister<grammar::simplify::ToCNF, grammar::CNF<DefaultSymbolType, ext::vector<ext::variant<DefaultSymbolType, DefaultSymbolType>>>, const grammar::LeftLG<>&>(grammar::simplify::ToCNF::convert, "grammar").setDocumentation("Implements transformation of a grammar into chomsky's normal form.\n\
\n\
@param grammar the transformed grammar\n\
@return an grammar in chomsky's normal form equivalent to the @p grammar");

auto ToCNFLeftRG = registration::AbstractRegister<grammar::simplify::ToCNF, grammar::CNF<DefaultSymbolType, ext::variant<DefaultSymbolType, DefaultSymbolType>>, const grammar::LeftRG<>&>(grammar::simplify::ToCNF::convert, "grammar").setDocumentation("Implements transformation of a grammar into chomsky's normal form.\n\
\n\
@param grammar the transformed grammar\n\
@return an grammar in chomsky's normal form equivalent to the @p grammar");

auto ToCNFRightLG = registration::AbstractRegister<grammar::simplify::ToCNF, grammar::CNF<DefaultSymbolType, ext::vector<ext::variant<DefaultSymbolType, DefaultSymbolType>>>, const grammar::RightLG<>&>(grammar::simplify::ToCNF::convert, "grammar").setDocumentation("Implements transformation of a grammar into chomsky's normal form.\n\
\n\
@param grammar the transformed grammar\n\
@return an grammar in chomsky's normal form equivalent to the @p grammar");

auto ToCNFRightRG = registration::AbstractRegister<grammar::simplify::ToCNF, grammar::CNF<DefaultSymbolType, ext::variant<DefaultSymbolType, DefaultSymbolType>>, const grammar::RightRG<>&>(grammar::simplify::ToCNF::convert, "grammar").setDocumentation("Implements transformation of a grammar into chomsky's normal form.\n\
\n\
@param grammar the transformed grammar\n\
@return an grammar in chomsky's normal form equivalent to the @p grammar");

} /* namespace */
