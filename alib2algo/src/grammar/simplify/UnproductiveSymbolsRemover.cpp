#include <registration/AlgoRegistration.hpp>
#include "UnproductiveSymbolsRemover.h"

namespace {

auto UnproductiveSymbolsRemoverCFG = registration::AbstractRegister<grammar::simplify::UnproductiveSymbolsRemover, grammar::CFG<>, const grammar::CFG<>&>(grammar::simplify::UnproductiveSymbolsRemover::remove, "grammar").setDocumentation("Removes unproductive symbols.\n\
\n\
@param grammar the modified grammar\n\
@return grammar equivalent to @p grammar without unproductive symbols");

auto UnproductiveSymbolsRemoverEpsilonFreeCFG = registration::AbstractRegister<grammar::simplify::UnproductiveSymbolsRemover, grammar::EpsilonFreeCFG<>, const grammar::EpsilonFreeCFG<>&>(grammar::simplify::UnproductiveSymbolsRemover::remove, "grammar").setDocumentation("Removes unproductive symbols.\n\
\n\
@param grammar the modified grammar\n\
@return grammar equivalent to @p grammar without unproductive symbols");

auto UnproductiveSymbolsRemoverGNF = registration::AbstractRegister<grammar::simplify::UnproductiveSymbolsRemover, grammar::GNF<>, const grammar::GNF<>&>(grammar::simplify::UnproductiveSymbolsRemover::remove, "grammar").setDocumentation("Removes unproductive symbols.\n\
\n\
@param grammar the modified grammar\n\
@return grammar equivalent to @p grammar without unproductive symbols");

auto UnproductiveSymbolsRemoverCNF = registration::AbstractRegister<grammar::simplify::UnproductiveSymbolsRemover, grammar::CNF<>, const grammar::CNF<>&>(grammar::simplify::UnproductiveSymbolsRemover::remove, "grammar").setDocumentation("Removes unproductive symbols.\n\
\n\
@param grammar the modified grammar\n\
@return grammar equivalent to @p grammar without unproductive symbols");

auto UnproductiveSymbolsRemoverLG = registration::AbstractRegister<grammar::simplify::UnproductiveSymbolsRemover, grammar::LG<>, const grammar::LG<>&>(grammar::simplify::UnproductiveSymbolsRemover::remove, "grammar").setDocumentation("Removes unproductive symbols.\n\
\n\
@param grammar the modified grammar\n\
@return grammar equivalent to @p grammar without unproductive symbols");

auto UnproductiveSymbolsRemoverLeftLG = registration::AbstractRegister<grammar::simplify::UnproductiveSymbolsRemover, grammar::LeftLG<>, const grammar::LeftLG<>&>(grammar::simplify::UnproductiveSymbolsRemover::remove, "grammar").setDocumentation("Removes unproductive symbols.\n\
\n\
@param grammar the modified grammar\n\
@return grammar equivalent to @p grammar without unproductive symbols");

auto UnproductiveSymbolsRemoverLeftRG = registration::AbstractRegister<grammar::simplify::UnproductiveSymbolsRemover, grammar::LeftRG<>, const grammar::LeftRG<>&>(grammar::simplify::UnproductiveSymbolsRemover::remove, "grammar").setDocumentation("Removes unproductive symbols.\n\
\n\
@param grammar the modified grammar\n\
@return grammar equivalent to @p grammar without unproductive symbols");

auto UnproductiveSymbolsRemoverRightLG = registration::AbstractRegister<grammar::simplify::UnproductiveSymbolsRemover, grammar::RightLG<>, const grammar::RightLG<>&>(grammar::simplify::UnproductiveSymbolsRemover::remove, "grammar").setDocumentation("Removes unproductive symbols.\n\
\n\
@param grammar the modified grammar\n\
@return grammar equivalent to @p grammar without unproductive symbols");

auto UnproductiveSymbolsRemoverRightRG = registration::AbstractRegister<grammar::simplify::UnproductiveSymbolsRemover, grammar::RightRG<>, const grammar::RightRG<>&>(grammar::simplify::UnproductiveSymbolsRemover::remove, "grammar").setDocumentation("Removes unproductive symbols.\n\
\n\
@param grammar the modified grammar\n\
@return grammar equivalent to @p grammar without unproductive symbols");

} /* namespace */
