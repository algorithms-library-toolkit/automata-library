#include <registration/AlgoRegistration.hpp>
#include "Trim.h"

namespace {

auto TrimCFG = registration::AbstractRegister<grammar::simplify::Trim, grammar::CFG<>, const grammar::CFG<>&>(grammar::simplify::Trim::trim, "grammar").setDocumentation("Removes unproductive and unreachable symbols from the given grammar. Uses first the @sa UnproductiveSymbolsRemover and next @sa UnreachableSymbolsRemover in the process.\n\
\n\
@param grammar the context free grammar to trim\n\
@return the trimmed grammar equivalent to @p grammar");

auto TrimEpsilonFreeCFG = registration::AbstractRegister<grammar::simplify::Trim, grammar::EpsilonFreeCFG<>, const grammar::EpsilonFreeCFG<>&>(grammar::simplify::Trim::trim, "grammar").setDocumentation("Removes unproductive and unreachable symbols from the given grammar. Uses first the @sa UnproductiveSymbolsRemover and next @sa UnreachableSymbolsRemover in the process.\n\
\n\
@param grammar the context free grammar to trim\n\
@return the trimmed grammar equivalent to @p grammar");

auto TrimGNF = registration::AbstractRegister<grammar::simplify::Trim, grammar::GNF<>, const grammar::GNF<>&>(grammar::simplify::Trim::trim, "grammar").setDocumentation("Removes unproductive and unreachable symbols from the given grammar. Uses first the @sa UnproductiveSymbolsRemover and next @sa UnreachableSymbolsRemover in the process.\n\
\n\
@param grammar the context free grammar to trim\n\
@return the trimmed grammar equivalent to @p grammar");

auto TrimCNF = registration::AbstractRegister<grammar::simplify::Trim, grammar::CNF<>, const grammar::CNF<>&>(grammar::simplify::Trim::trim, "grammar").setDocumentation("Removes unproductive and unreachable symbols from the given grammar. Uses first the @sa UnproductiveSymbolsRemover and next @sa UnreachableSymbolsRemover in the process.\n\
\n\
@param grammar the context free grammar to trim\n\
@return the trimmed grammar equivalent to @p grammar");

auto TrimLG = registration::AbstractRegister<grammar::simplify::Trim, grammar::LG<>, const grammar::LG<>&>(grammar::simplify::Trim::trim, "grammar").setDocumentation("Removes unproductive and unreachable symbols from the given grammar. Uses first the @sa UnproductiveSymbolsRemover and next @sa UnreachableSymbolsRemover in the process.\n\
\n\
@param grammar the context free grammar to trim\n\
@return the trimmed grammar equivalent to @p grammar");

auto TrimLeftLG = registration::AbstractRegister<grammar::simplify::Trim, grammar::LeftLG<>, const grammar::LeftLG<>&>(grammar::simplify::Trim::trim, "grammar").setDocumentation("Removes unproductive and unreachable symbols from the given grammar. Uses first the @sa UnproductiveSymbolsRemover and next @sa UnreachableSymbolsRemover in the process.\n\
\n\
@param grammar the context free grammar to trim\n\
@return the trimmed grammar equivalent to @p grammar");

auto TrimLeftRG = registration::AbstractRegister<grammar::simplify::Trim, grammar::LeftRG<>, const grammar::LeftRG<>&>(grammar::simplify::Trim::trim, "grammar").setDocumentation("Removes unproductive and unreachable symbols from the given grammar. Uses first the @sa UnproductiveSymbolsRemover and next @sa UnreachableSymbolsRemover in the process.\n\
\n\
@param grammar the context free grammar to trim\n\
@return the trimmed grammar equivalent to @p grammar");

auto TrimRightLG = registration::AbstractRegister<grammar::simplify::Trim, grammar::RightLG<>, const grammar::RightLG<>&>(grammar::simplify::Trim::trim, "grammar").setDocumentation("Removes unproductive and unreachable symbols from the given grammar. Uses first the @sa UnproductiveSymbolsRemover and next @sa UnreachableSymbolsRemover in the process.\n\
\n\
@param grammar the context free grammar to trim\n\
@return the trimmed grammar equivalent to @p grammar");

auto TrimRightRG = registration::AbstractRegister<grammar::simplify::Trim, grammar::RightRG<>, const grammar::RightRG<>&>(grammar::simplify::Trim::trim, "grammar").setDocumentation("Removes unproductive and unreachable symbols from the given grammar. Uses first the @sa UnproductiveSymbolsRemover and next @sa UnreachableSymbolsRemover in the process.\n\
\n\
@param grammar the context free grammar to trim\n\
@return the trimmed grammar equivalent to @p grammar");

} /* namespace */
