#include <registration/AlgoRegistration.hpp>
#include "Rename.h"

namespace {

auto RenameRightRG = registration::AbstractRegister<grammar::simplify::Rename, grammar::RightRG<DefaultSymbolType, unsigned>, const grammar::RightRG<>&>(grammar::simplify::Rename::rename, "grammar").setDocumentation("Renames nonterminal symbols in given grammar\n\
\n\
@param grammar the renamed grammar\n\
@return grammar with nonterminal symbols renamed");

auto RenameLeftRG = registration::AbstractRegister<grammar::simplify::Rename, grammar::LeftRG<DefaultSymbolType, unsigned>, const grammar::LeftRG<>&>(grammar::simplify::Rename::rename, "grammar").setDocumentation("Renames nonterminal symbols in given grammar\n\
\n\
@param grammar the renamed grammar\n\
@return grammar with nonterminal symbols renamed");

} /* namespace */
