#include <registration/AlgoRegistration.hpp>
#include "ToGrammarRightRG.h"

namespace {

auto ToGrammarRightRGLeftRG = registration::AbstractRegister<grammar::convert::ToGrammarRightRG, grammar::RightRG<>, const grammar::LeftRG<>&>(grammar::convert::ToGrammarRightRG::convert, "grammar").setDocumentation("Transforms a left regular grammar to right regular grammar.\n\
\n\
@param grammar the left regular grammar to convert\n\
@return right regular grammar which is equivalent to source left regular grammar.");

} /* namespace */
