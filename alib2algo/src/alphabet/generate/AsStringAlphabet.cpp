#include "AsStringAlphabet.h"

#include <registration/AlgoRegistration.hpp>

namespace {

auto AsStringAlphabetChar = registration::AbstractRegister<alphabet::generate::AsStringAlphabet, ext::set<std::string>, const ext::set<char>&>(alphabet::generate::AsStringAlphabet::asStringAlphabet<char>, "alphabet").setDocumentation("Converts a character alphabet to alphabet of string type.\n\
\n\
@param alphabet the converted alphabet\n\
@return string typed alphabet");

auto AsStringAlphabetInt = registration::AbstractRegister<alphabet::generate::AsStringAlphabet, ext::set<std::string>, const ext::set<int>&>(alphabet::generate::AsStringAlphabet::asStringAlphabet<int>, "alphabet").setDocumentation("Converts an integer alphabet to alphabet of string type.\n\
\n\
@param alphabet the converted alphabet\n\
@return string typed alphabet");

} /* namespace */
