#pragma once

#include <ext/random>

#include <alib/set>

#include <common/ranked_symbol.hpp>

namespace alphabet::generate {

class AsFullRankedAlphabet {
public:
    template <class T>
    static ext::set<common::ranked_symbol<T>> asRankedAlphabet(const ext::set<T>& alphabet, size_t maxRank)
    {
        ext::set<common::ranked_symbol<T>> res;

        if (alphabet.empty())
            throw std::invalid_argument("There needs to be at least one symbol for nullary symbol.");

        for (size_t i = 0; i <= maxRank; ++i) {
            for (const T& symbol : alphabet) {
                res.insert(common::ranked_symbol<T>(symbol, i));
            }
        }
        return res;
    }
};

class AsRandomRankedAlphabet {
public:
    template <class T>
    static ext::set<common::ranked_symbol<T>> asRankedAlphabet(const ext::set<T>& alphabet, size_t maxRank)
    {
        ext::set<common::ranked_symbol<T>> res;

        if (alphabet.empty())
            throw std::invalid_argument("There needs to be at least one symbol for nullary symbol.");

        size_t nullaryIndex = static_cast<size_t>(ext::random_devices::semirandom() % (alphabet.size()));

        for (auto iter = alphabet.begin(); iter != alphabet.end(); ++iter) {
            if (res.size() == nullaryIndex)
                res.insert(common::ranked_symbol<T>(*iter, 0));
            else
                res.insert(common::ranked_symbol<T>(*iter, static_cast<size_t>(ext::random_devices::semirandom() % (maxRank + 1))));
        }

        return res;
    }
};

class AsSequencedRankedAlphabet {
public:
    template <class T>
    static ext::set<common::ranked_symbol<T>> asRankedAlphabet(const ext::set<T>& alphabet, size_t maxRank)
    {
        ext::set<common::ranked_symbol<T>> res;

        if (alphabet.size() <= maxRank)
            throw std::invalid_argument("Not enough symbols were provided to fulfill the max rank request.");

        auto iter = alphabet.begin();
        for (size_t i = 0; iter != alphabet.end(); i = (i + 1) % (maxRank + 1)) {
            res.insert(common::ranked_symbol<T>(*iter++, i));
        }

        return res;
    }
};

} /* namespace alphabet::generate */
