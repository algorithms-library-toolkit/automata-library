#include "AsRankedAlphabet.h"

#include <registration/AlgoRegistration.hpp>

namespace {

auto AsRankedAlphabet = registration::AbstractRegister<alphabet::generate::AsFullRankedAlphabet, ext::set<common::ranked_symbol<object::Object>>, const ext::set<object::Object>&, size_t>(alphabet::generate::AsFullRankedAlphabet::asRankedAlphabet<object::Object>, "alphabet", "maxRank").setDocumentation("Generates ranked alphabet from unranked alphabet so that all symbols are put into the result with all ranks.\n\
\n\
@param alphabet the converted alphabet\n\
@param maxRank the maximal rank used\n\
@return the generated ranked alphabet");

auto AsRandomRankedAlphabet = registration::AbstractRegister<alphabet::generate::AsRandomRankedAlphabet, ext::set<common::ranked_symbol<object::Object>>, const ext::set<object::Object>&, size_t>(alphabet::generate::AsRandomRankedAlphabet::asRankedAlphabet<object::Object>, "alphabet", "maxRank").setDocumentation("Generates ranked alphabet from unranked alphabet so that at least one symbol is assigned rank 0 and other are random.\n\
\n\
@param alphabet the converted alphabet\n\
@param maxRank the maximal rank used\n\
@return the generated ranked alphabet");

auto AsSequencedRankedAlphabet = registration::AbstractRegister<alphabet::generate::AsSequencedRankedAlphabet, ext::set<common::ranked_symbol<object::Object>>, const ext::set<object::Object>&, size_t>(alphabet::generate::AsSequencedRankedAlphabet::asRankedAlphabet<object::Object>, "alphabet", "maxRank").setDocumentation("Generates ranked alphabet from unranked alphabet so that first symbol is assigned rank 0, second rank 1 up to maxRank-th symbol rank maxRank when the assigned ranks assigned also reset to 0 and are increases again.\n\
\n\
@param alphabet the converted alphabet\n\
@param maxRank the maximal rank used\n\
@return the generated ranked alphabet");

} /* namespace */
