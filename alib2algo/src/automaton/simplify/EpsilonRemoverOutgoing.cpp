#include <registration/AlgoRegistration.hpp>
#include "EpsilonRemoverOutgoing.h"

namespace {

auto EpsilonRemoverOutgoingDFA = registration::AbstractRegister<automaton::simplify::EpsilonRemoverOutgoing, automaton::DFA<>, const automaton::DFA<>&>(automaton::simplify::EpsilonRemoverOutgoing::remove, "fsm").setDocumentation("Removes epsilon transitions from an automaton.\n\
\n\
@param fsm automaton to remove epsilon transitions from\n\
@return an automaton (with multiple initial states) equivalent to @p fsm but without epsilon transitions");

auto EpsilonRemoverOutgoingMultiInitialStateNFA = registration::AbstractRegister<automaton::simplify::EpsilonRemoverOutgoing, automaton::MultiInitialStateNFA<>, const automaton::MultiInitialStateNFA<>&>(automaton::simplify::EpsilonRemoverOutgoing::remove, "fsm").setDocumentation("Removes epsilon transitions from an automaton.\n\
\n\
@param fsm automaton to remove epsilon transitions from\n\
@return an automaton (with multiple initial states) equivalent to @p fsm but without epsilon transitions");

auto EpsilonRemoverOutgoingNFA = registration::AbstractRegister<automaton::simplify::EpsilonRemoverOutgoing, automaton::NFA<>, const automaton::NFA<>&>(automaton::simplify::EpsilonRemoverOutgoing::remove, "fsm").setDocumentation("Removes epsilon transitions from an automaton.\n\
\n\
@param fsm automaton to remove epsilon transitions from\n\
@return an automaton (with multiple initial states) equivalent to @p fsm but without epsilon transitions");

auto EpsilonRemoverOutgoingEpsilonNFA = registration::AbstractRegister<automaton::simplify::EpsilonRemoverOutgoing, automaton::MultiInitialStateNFA<>, const automaton::EpsilonNFA<>&>(automaton::simplify::EpsilonRemoverOutgoing::remove, "fsm").setDocumentation("Removes epsilon transitions from an automaton.\n\
\n\
@param fsm automaton to remove epsilon transitions from\n\
@return an automaton (with multiple initial states) equivalent to @p fsm but without epsilon transitions");

auto EpsilonRemoverOutgoingEpsilonNFTA = registration::AbstractRegister<automaton::simplify::EpsilonRemoverOutgoing, automaton::NFTA<>, const automaton::EpsilonNFTA<>&>(automaton::simplify::EpsilonRemoverOutgoing::remove, "fta").setDocumentation("Removes epsilon transitions from a nondeterministic epsilon finite tree automaton.\n\
\n\
@param fta automaton to remove epsilon transitions from\n\
@return an automaton equivalent to @p fta but without epsilon transitions");

} /* namespace */
