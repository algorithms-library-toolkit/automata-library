#include <registration/AlgoRegistration.hpp>
#include "SingleInitialState.h"

namespace {

auto SingleInitialStateMultiInitialStateNFA = registration::AbstractRegister<automaton::simplify::SingleInitialState, automaton::NFA<>, const automaton::MultiInitialStateNFA<>&>(automaton::simplify::SingleInitialState::convert, "automaton").setDocumentation("Converts multi-initial state automaton to a single-initial state automaton.\n\
\n\
@param automaton automaton to convert\n\
@return an automaton equivalent to @p with only one initial state");

auto SingleInitialStateMultiInitialStateEpsilonNFA = registration::AbstractRegister<automaton::simplify::SingleInitialState, automaton::EpsilonNFA<>, const automaton::MultiInitialStateEpsilonNFA<>&>(automaton::simplify::SingleInitialState::convert, "automaton").setDocumentation("Converts multi-initial state epsilon automaton to a single-initial state epsilon automaton.\n\
\n\
@param automaton automaton to convert\n\
@return an automaton equivalent to @p with only one initial state");

auto SingleInitialStateDFA = registration::AbstractRegister<automaton::simplify::SingleInitialState, automaton::DFA<>, const automaton::DFA<>&>(automaton::simplify::SingleInitialState::convert, "automaton").setDocumentation("No-op for deterministic finite automaton\n\
\n\
@param automaton automaton to convert\n\
@return an automaton equivalent to @p with only one initial state");

auto SingleInitialStateEpsilonNFA = registration::AbstractRegister<automaton::simplify::SingleInitialState, automaton::EpsilonNFA<>, const automaton::EpsilonNFA<>&>(automaton::simplify::SingleInitialState::convert, "automaton").setDocumentation("No-op for epsilon nondeterministic finite automaton\n\
\n\
@param automaton automaton to convert\n\
@return an automaton equivalent to @p with only one initial state");

auto SingleInitialStateNFA = registration::AbstractRegister<automaton::simplify::SingleInitialState, automaton::NFA<>, const automaton::NFA<>&>(automaton::simplify::SingleInitialState::convert, "automaton").setDocumentation("No-op for nondeterministic finite automaton\n\
\n\
@param automaton automaton to convert\n\
@return an automaton equivalent to @p with only one initial state");

auto SingleInitialStateCompactNFA = registration::AbstractRegister<automaton::simplify::SingleInitialState, automaton::CompactNFA<>, const automaton::CompactNFA<>&>(automaton::simplify::SingleInitialState::convert, "automaton").setDocumentation("No-op for compact nondeterministic finite automaton\n\
\n\
@param automaton automaton to convert\n\
@return an automaton equivalent to @p with only one initial state");

auto SingleInitialStateExtendedNFA = registration::AbstractRegister<automaton::simplify::SingleInitialState, automaton::ExtendedNFA<>, const automaton::ExtendedNFA<>&>(automaton::simplify::SingleInitialState::convert, "automaton").setDocumentation("No-op for extended nondeterministic finite automaton\n\
\n\
@param automaton automaton to convert\n\
@return an automaton equivalent to @p with only one initial state");

} /* namespace */
