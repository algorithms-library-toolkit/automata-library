/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <ext/algorithm>
#include <ext/iostream>

#include <alib/map>
#include <alib/vector>

#include <automaton/PDA/RealTimeHeightDeterministicDPDA.h>

namespace automaton {

namespace simplify {

/**
 * Removes unused states/symbols in an automaton's structures based on its transitions .
 */
class RemoveUnused {
public:
    /**
     * Removes unused automaton's states and pushdown store or input symbols.
     *
     * @tparam InputSymbolType Type for input symbols
     * @tparam PushdownStoreSymbolType Type of epsilon representation
     * @tparam StateType Type for states
     *
     * @param pda pushdown automaton to rename
     *
     * @return @p pda with renamed states and pushdown store symbols
     */
    template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
    static automaton::RealTimeHeightDeterministicDPDA<InputSymbolType, PushdownStoreSymbolType, StateType> removeUnused(const automaton::RealTimeHeightDeterministicDPDA<InputSymbolType, PushdownStoreSymbolType, StateType>& pda);
};

template <class InputSymbolType, class PushdownStoreSymbolType, class StateType>
automaton::RealTimeHeightDeterministicDPDA<InputSymbolType, PushdownStoreSymbolType, StateType> RemoveUnused::removeUnused(const automaton::RealTimeHeightDeterministicDPDA<InputSymbolType, PushdownStoreSymbolType, StateType>& pda)
{
    ext::set<InputSymbolType> alphabet;
    ext::set<PushdownStoreSymbolType> pushdownAlphabet;
    ext::set<StateType> states;

    states.insert(pda.getInitialState());
    pushdownAlphabet.insert(pda.getBottomOfTheStackSymbol());

    for (const auto& transition : pda.getCallTransitions()) {
        if (!transition.first.second.is_epsilon())
            alphabet.insert(transition.first.second.getSymbol());
        pushdownAlphabet.insert(transition.second.second);
        states.insert(transition.first.first);
        states.insert(transition.second.first);
    }

    for (const auto& transition : pda.getLocalTransitions()) {
        if (!transition.first.second.is_epsilon())
            alphabet.insert(transition.first.second.getSymbol());
        states.insert(transition.first.first);
        states.insert(transition.second);
    }

    for (const auto& transition : pda.getReturnTransitions()) {
        if (!std::get<1>(transition.first).is_epsilon())
            alphabet.insert(std::get<1>(transition.first).getSymbol());
        pushdownAlphabet.insert(std::get<2>(transition.first));
        states.insert(std::get<0>(transition.first));
        states.insert(transition.second);
    }

    automaton::RealTimeHeightDeterministicDPDA<InputSymbolType, PushdownStoreSymbolType, StateType> result(pda.getInitialState(), pda.getBottomOfTheStackSymbol());

    result.setInputAlphabet(alphabet);
    result.setPushdownStoreAlphabet(pushdownAlphabet);
    result.setStates(states);

    for (const StateType& state : pda.getFinalStates())
        if (states.contains(state))
            result.addFinalState(state);

    for (const auto& transition : pda.getCallTransitions())
        result.addCallTransition(transition.first.first, transition.first.second, transition.second.first, transition.second.second);

    for (const auto& transition : pda.getLocalTransitions())
        result.addLocalTransition(transition.first.first, transition.first.second, transition.second);

    for (const auto& transition : pda.getReturnTransitions())
        result.addReturnTransition(std::get<0>(transition.first), std::get<1>(transition.first), std::get<2>(transition.first), transition.second);

    return result;
}

} /* namespace simplify */

} /* namespace automaton */
