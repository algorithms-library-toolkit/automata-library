#include <registration/AlgoRegistration.hpp>
#include "MinimizeByPartitioning.h"

namespace {

auto MinimizeDistinguishableStatesDFA = registration::AbstractRegister<automaton::simplify::MinimizeByPartitioning, automaton::DFA<DefaultSymbolType, ext::set<DefaultStateType>>, const automaton::DFA<>&, const ext::set<ext::set<DefaultStateType>>&>(automaton::simplify::MinimizeByPartitioning::minimize, "dfa").setDocumentation("Minimizes the given automaton using Distinguishable states method.\n\
\n\
@param dfa finite automaton to minimize.\n\
@return Minimal deterministic finite automaton equivalent to @p dfa");

auto MinimizeDistinguishableStatesNFA = registration::AbstractRegister<automaton::simplify::MinimizeByPartitioning, automaton::NFA<DefaultSymbolType, ext::set<DefaultStateType>>, const automaton::NFA<>&, const ext::set<ext::set<DefaultStateType>>&>(automaton::simplify::MinimizeByPartitioning::minimize, "nfa").setDocumentation("Minimizes the given automaton using Distinguishable states method.\n\
\n\
@param nfa finite automaton to minimize.\n\
@return Minimal deterministic finite automaton equivalent to @p nfa");

auto MinimizeDistinguishableStatesDFTA = registration::AbstractRegister<automaton::simplify::MinimizeByPartitioning, automaton::DFTA<DefaultSymbolType, ext::set<DefaultStateType>>, const automaton::DFTA<>&, const ext::set<ext::set<DefaultStateType>>&>(automaton::simplify::MinimizeByPartitioning::minimize, "dfta").setDocumentation("Minimizes the given automaton using Distinguishable states method.\n\
\n\
@param dfta finite tree automaton to minimize.\n\
@return Minimal deterministic finite tree automaton equivalent to @p dfta");

auto MinimizeDistinguishableStatesNFTA = registration::AbstractRegister<automaton::simplify::MinimizeByPartitioning, automaton::NFTA<DefaultSymbolType, ext::set<DefaultStateType>>, const automaton::NFTA<>&, const ext::set<ext::set<DefaultStateType>>&>(automaton::simplify::MinimizeByPartitioning::minimize, "dfta").setDocumentation("Minimizes the given automaton using Distinguishable states method.\n\
\n\
@param dfta finite tree automaton to minimize.\n\
@return Minimal deterministic finite tree automaton equivalent to @p dfta");

auto MinimizeDistinguishableStatesUnorderedDFTA = registration::AbstractRegister<automaton::simplify::MinimizeByPartitioning, automaton::UnorderedDFTA<DefaultSymbolType, ext::set<DefaultStateType>>, const automaton::UnorderedDFTA<>&, const ext::set<ext::set<DefaultStateType>>&>(automaton::simplify::MinimizeByPartitioning::minimize, "dfta").setDocumentation("Minimizes the given automaton using Distinguishable states method.\n\
\n\
@param dfta unordered finite tree automaton to minimize.\n\
@return Minimal deterministic unordered finite tree automaton equivalent to @p dfta");

auto MinimizeDistinguishableStatesUnorderedNFTA = registration::AbstractRegister<automaton::simplify::MinimizeByPartitioning, automaton::UnorderedNFTA<DefaultSymbolType, ext::set<DefaultStateType>>, const automaton::UnorderedNFTA<>&, const ext::set<ext::set<DefaultStateType>>&>(automaton::simplify::MinimizeByPartitioning::minimize, "dfta").setDocumentation("Minimizes the given automaton using Distinguishable states method.\n\
\n\
@param dfta unordered finite tree automaton to minimize.\n\
@return Minimal deterministic unordered finite tree automaton equivalent to @p dfta");

} /* namespace */
