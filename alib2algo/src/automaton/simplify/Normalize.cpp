#include <registration/AlgoRegistration.hpp>
#include "Normalize.h"

namespace {

auto NormalizeDFA = registration::AbstractRegister<automaton::simplify::Normalize, automaton::DFA<DefaultSymbolType, unsigned>, const automaton::DFA<>&>(automaton::simplify::Normalize::normalize, "automaton").setDocumentation("Normalization of deterministic finite automata.\n\
The process of normalization is a BFS traversal through the graph representing the automaton. The states are named by integers in the visited order.\n\
\n\
@param automaton determinsitic finite automaton to normalize\n\
@return @p dfa with state labels normalized\n\
\n\
@throws exception::CommonException if the passed dfa was not minimal connected dfa");

auto NormalizeCompactDFA = registration::AbstractRegister<automaton::simplify::Normalize, automaton::CompactDFA<DefaultSymbolType, unsigned>, const automaton::CompactDFA<>&>(automaton::simplify::Normalize::normalize, "automaton").setDocumentation("Normalization of compact deterministic finite automata.\n\
The process of normalization is a BFS traversal through the graph representing the automaton. The states are named by integers in the visited order.\n\
\n\
@param automaton compact determinsitic finite automaton to normalize\n\
@return @p compact dfa with state labels normalized\n\
\n\
@throws exception::CommonException if the passed compact dfa was not minimal connected dfa");

auto NormalizeRealTimeHeightDeterministicDPDA = registration::AbstractRegister<automaton::simplify::Normalize, automaton::RealTimeHeightDeterministicDPDA<DefaultSymbolType, unsigned, unsigned>, const automaton::RealTimeHeightDeterministicDPDA<>&>(automaton::simplify::Normalize::normalize, "automaton").setDocumentation("Normalization of deterministic pushdown automata.\n\
The process of normalization of states is a BFS traversal through the graph representing the automaton. The states are named by integers in the visited order.\n\
We normalize also the pushdown store symbols.\n\
\n\
@param automaton determinsitic pushdown automaton to normalize\n\
@return @p dpda with state labels normalized\n\
\n\
@throws exception::CommonException if the passed dpda was not deterministic connected pda");

auto NormalizeDPDA = registration::AbstractRegister<automaton::simplify::Normalize, automaton::DPDA<DefaultSymbolType, unsigned, unsigned>, const automaton::DPDA<>&>(automaton::simplify::Normalize::normalize, "automaton").setDocumentation("Normalization of deterministic pushdown automata.\n\
The process of normalization of states is a BFS traversal through the graph representing the automaton. The states are named by integers in the visited order.\n\
We normalize also the pushdown store symbols.\n\
\n\
@param automaton determinsitic pushdown automaton to normalize\n\
@return @p dpda with state labels normalized\n\
\n\
@throws exception::CommonException if the passed dpda was not deterministic connected pda");

auto NormalizeDFTA = registration::AbstractRegister<automaton::simplify::Normalize, automaton::DFTA<DefaultSymbolType, unsigned>, const automaton::DFTA<>&>(automaton::simplify::Normalize::normalize, "automaton").setDocumentation("Normalization of deterministic finite tree automata.\n\
The process of normalization is a BFS traversal through the graph representing the automaton. The states are named by integers in the visited order.\n\
\n\
@param dfa determinsitic finite tree automaton to normalize\n\
@return @p dfta with state labels normalized\n\
\n\
@throws exception::CommonException if the passed dfta was not minimal connected dfta");

} /* namespace */
