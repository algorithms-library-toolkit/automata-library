#include <registration/AlgoRegistration.hpp>
#include "Total.h"

namespace {

auto TotalNFA = registration::AbstractRegister<automaton::simplify::Total, automaton::NFA<>, const automaton::NFA<>&>(automaton::simplify::Total::total, "automaton").setDocumentation("Makes a finite automaton's transition function total.\n\
\n\
@param automaton automaton to alter\n\
@return an automaton equivalent to @p automaton with total transition function");

auto TotalDFA = registration::AbstractRegister<automaton::simplify::Total, automaton::DFA<>, const automaton::DFA<>&>(automaton::simplify::Total::total, "automaton").setDocumentation("Makes a finite automaton's transition function total.\n\
\n\
@param automaton automaton to alter\n\
@return an automaton equivalent to @p automaton with total transition function");

auto TotalDFTA = registration::AbstractRegister<automaton::simplify::Total, automaton::DFTA<>, const automaton::DFTA<>&>(automaton::simplify::Total::total, "automaton").setDocumentation("Makes a finite tree automaton's transition function total.\n\
\n\
@param automaton automaton to alter\n\
@return an automaton equivalent to @p automaton with total transition function");

auto TotalNFTA = registration::AbstractRegister<automaton::simplify::Total, automaton::NFTA<>, const automaton::NFTA<>&>(automaton::simplify::Total::total, "automaton").setDocumentation("Makes a finite tree automaton's transition function total.\n\
\n\
@param automaton automaton to alter\n\
@return an automaton equivalent to @p automaton with total transition function");

} /* namespace */
