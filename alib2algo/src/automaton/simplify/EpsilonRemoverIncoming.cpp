#include <registration/AlgoRegistration.hpp>
#include "EpsilonRemoverIncoming.h"

namespace {

auto EpsilonRemoverIncomingDFA = registration::AbstractRegister<automaton::simplify::EpsilonRemoverIncoming, automaton::DFA<>, const automaton::DFA<>&>(automaton::simplify::EpsilonRemoverIncoming::remove, "fsm").setDocumentation("Removes epsilon transitions from an automaton.\n\
\n\
@param fsm automaton to remove epsilon transitions from\n\
@return an automaton equivalent to @p fsm but without epsilon transitions");

auto EpsilonRemoverIncomingMultiInitialStateNFA = registration::AbstractRegister<automaton::simplify::EpsilonRemoverIncoming, automaton::MultiInitialStateNFA<>, const automaton::MultiInitialStateNFA<>&>(automaton::simplify::EpsilonRemoverIncoming::remove, "fsm").setDocumentation("Removes epsilon transitions from an automaton.\n\
\n\
@param fsm automaton to remove epsilon transitions from\n\
@return an automaton equivalent to @p fsm but without epsilon transitions");

auto EpsilonRemoverIncomingNFA = registration::AbstractRegister<automaton::simplify::EpsilonRemoverIncoming, automaton::NFA<>, const automaton::NFA<>&>(automaton::simplify::EpsilonRemoverIncoming::remove, "fsm").setDocumentation("Removes epsilon transitions from an automaton.\n\
\n\
@param fsm automaton to remove epsilon transitions from\n\
@return an automaton equivalent to @p fsm but without epsilon transitions");

auto EpsilonRemoverIncomingEpsilonNFA = registration::AbstractRegister<automaton::simplify::EpsilonRemoverIncoming, automaton::NFA<>, const automaton::EpsilonNFA<>&>(automaton::simplify::EpsilonRemoverIncoming::remove, "fsm").setDocumentation("Removes epsilon transitions from an automaton.\n\
\n\
@param fsm automaton to remove epsilon transitions from\n\
@return an automaton equivalent to @p fsm but without epsilon transitions");

} /* namespace */
