#include <registration/AlgoRegistration.hpp>
#include "RemoveUnused.h"

namespace {

auto RenameRealTimeHeightDeterministicDPDA = registration::AbstractRegister<automaton::simplify::RemoveUnused, automaton::RealTimeHeightDeterministicDPDA<>, const automaton::RealTimeHeightDeterministicDPDA<>&>(automaton::simplify::RemoveUnused::removeUnused, "automaton").setDocumentation("Remove automaton's unused states and pushdown store or input symbols.\n\
\n\
@param automaton pushdown automaton to remove unused states and symbols from\n\
@return @p automaton with removed states and symbols");

} /* namespace */
