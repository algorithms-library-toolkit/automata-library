#include <registration/AlgoRegistration.hpp>
#include "UnreachableStatesRemover.h"

namespace {

auto UnreachableStatesRemoverEpsilonNFA = registration::AbstractRegister<automaton::simplify::UnreachableStatesRemover, automaton::EpsilonNFA<>, const automaton::EpsilonNFA<>&>(automaton::simplify::UnreachableStatesRemover::remove, "automaton").setDocumentation("Removes unreachable states from an automaton.\n\
\n\
@param automaton automaton to trim\n\
@return @p automaton without unreachable states");

auto UnreachableStatesRemoverNFA = registration::AbstractRegister<automaton::simplify::UnreachableStatesRemover, automaton::NFA<>, const automaton::NFA<>&>(automaton::simplify::UnreachableStatesRemover::remove, "automaton").setDocumentation("Removes unreachable states from an automaton.\n\
\n\
@param automaton automaton to trim\n\
@return @p automaton without unreachable states");

auto UnreachableStatesRemoverCompactNFA = registration::AbstractRegister<automaton::simplify::UnreachableStatesRemover, automaton::CompactNFA<>, const automaton::CompactNFA<>&>(automaton::simplify::UnreachableStatesRemover::remove, "automaton").setDocumentation("Removes unreachable states from an automaton.\n\
\n\
@param automaton automaton to trim\n\
@return @p automaton without unreachable states");

auto UnreachableStatesRemoverExtendedNFA = registration::AbstractRegister<automaton::simplify::UnreachableStatesRemover, automaton::ExtendedNFA<>, const automaton::ExtendedNFA<>&>(automaton::simplify::UnreachableStatesRemover::remove, "automaton").setDocumentation("Removes unreachable states from an automaton.\n\
\n\
@param automaton automaton to trim\n\
@return @p automaton without unreachable states");

auto UnreachableStatesRemoverDFA = registration::AbstractRegister<automaton::simplify::UnreachableStatesRemover, automaton::DFA<>, const automaton::DFA<>&>(automaton::simplify::UnreachableStatesRemover::remove, "automaton").setDocumentation("Removes unreachable states from an automaton.\n\
\n\
@param automaton automaton to trim\n\
@return @p automaton without unreachable states");

auto UnreachableStatesRemoverMultiInitialStateNFA = registration::AbstractRegister<automaton::simplify::UnreachableStatesRemover, automaton::MultiInitialStateNFA<>, const automaton::MultiInitialStateNFA<>&>(automaton::simplify::UnreachableStatesRemover::remove, "automaton").setDocumentation("Removes unreachable states from an automaton.\n\
\n\
@param automaton automaton to trim\n\
@return @p automaton without unreachable states");

auto UnreachableStatesRemoverDFTA = registration::AbstractRegister<automaton::simplify::UnreachableStatesRemover, automaton::DFTA<>, const automaton::DFTA<>&>(automaton::simplify::UnreachableStatesRemover::remove, "fta").setDocumentation("Removes unreachable states from a deterministic finite tree automaton.\n\
\n\
@param fta automaton to trim\n\
@return @p autoamton without unreachable states");

auto UnreachableStatesRemoverNFTA = registration::AbstractRegister<automaton::simplify::UnreachableStatesRemover, automaton::NFTA<>, const automaton::NFTA<>&>(automaton::simplify::UnreachableStatesRemover::remove, "fta").setDocumentation("Removes unreachable states from a deterministic finite tree automaton.\n\
\n\
@param fta automaton to trim\n\
@return @p autoamton without unreachable states");


auto UnreachableStatesRemoverAFDZA = registration::AbstractRegister<automaton::simplify::UnreachableStatesRemover, automaton::ArcFactoredDeterministicZAutomaton<>, const automaton::ArcFactoredDeterministicZAutomaton<>&>(automaton::simplify::UnreachableStatesRemover::remove, "afdza").setDocumentation("Removes unreachable states from a deterministic arc-factored z-automaton.\n\
\n\
@param afdza automaton to trim\n\
@return @p autoamton without unreachable states");


auto UnreachableStatesRemoverAFNZA = registration::AbstractRegister<automaton::simplify::UnreachableStatesRemover, automaton::ArcFactoredNondeterministicZAutomaton<>, const automaton::ArcFactoredNondeterministicZAutomaton<>&>(automaton::simplify::UnreachableStatesRemover::remove, "afnza").setDocumentation("Removes unreachable states from a nondeterministic arc-factored z-automaton.\n\
\n\
@param afnza automaton to trim\n\
@return @p afnza without unreachable states");

} /* namespace */
