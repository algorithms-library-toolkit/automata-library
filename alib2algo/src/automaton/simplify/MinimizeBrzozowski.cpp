#include <registration/AlgoRegistration.hpp>
#include "MinimizeBrzozowski.h"

namespace {

auto MinimizeBrzozowskiDFA = registration::AbstractRegister<automaton::simplify::MinimizeBrzozowski, automaton::DFA<DefaultSymbolType, ext::set<ext::set<DefaultStateType>>>, const automaton::DFA<>&>(automaton::simplify::MinimizeBrzozowski::minimize, "dfa").setDocumentation("Minimizes the given automaton using Brzozowski's method.\n\
\n\
@param dfa finite automaton to minimize.\n\
@return Minimal deterministic finite automaton equivalent to @p dfa");

auto MinimizeBrzozowskiNFA = registration::AbstractRegister<automaton::simplify::MinimizeBrzozowski, automaton::DFA<DefaultSymbolType, ext::set<ext::set<DefaultStateType>>>, const automaton::NFA<>&>(automaton::simplify::MinimizeBrzozowski::minimize, "nfa").setDocumentation("Minimizes the given automaton using Brzozowski's method.\n\
\n\
@param nfa finite automaton to minimize.\n\
@return Minimal deterministic finite automaton equivalent to @p nfa");

} /* namespace */
