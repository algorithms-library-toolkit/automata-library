/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <automaton/FSM/EpsilonNFA.h>

namespace automaton {

namespace transform {

/**
 * Complement of a finite automaton.
 * For finite automaton A1, we create automaton A such that L(A) = complement(L(A1)). This is done by reversing the set of final states in the DFA.
 */
class AutomatonComplement {
public:
    /**
     * Complement of a deterministic finite automaton.
     * @tparam SymbolType Type for input symbols.
     * @tparam StateType Type for states.
     * @param automaton automaton to complement
     * @return deterministic FA representing the complement of @p automaton
     */
    template <class SymbolType, class StateType>
    static automaton::DFA<SymbolType, StateType> complement(const automaton::DFA<SymbolType, StateType>& automaton);
};

template <class SymbolType, class StateType>
automaton::DFA<SymbolType, StateType> AutomatonComplement::complement(const automaton::DFA<SymbolType, StateType>& automaton)
{
    // create a complement of automaton's final states
    ext::set<StateType> newFinalStates = automaton.getStates();

    // FIXME Not working?
    // const ext::set < StateType > & oldFinalStates = automaton.getFinalStates ( );
    // newFinalStates.erase ( oldFinalStates.begin ( ), oldFinalStates.end ( ) );

    for (const auto& fin : automaton.getFinalStates())
        newFinalStates.erase(fin);

    automaton::DFA<SymbolType, StateType> res(automaton);
    res.setFinalStates(newFinalStates);
    return res;
}

} /* namespace transform */

} /* namespace automaton */
