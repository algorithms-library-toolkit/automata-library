#include <common/createUnique.hpp>
#include <registration/AlgoRegistration.hpp>
#include "AutomatonComplement.h"

namespace {

auto AutomatonComplementDFA = registration::AbstractRegister<automaton::transform::AutomatonComplement, automaton::DFA<>, const automaton::DFA<>&>(automaton::transform::AutomatonComplement::complement, "automaton").setDocumentation("Complement of a deterministic finite automaton.\n\
\n\
@param automaton automaton to complement\n\
@return deterministic FA representing the complement of @p automaton");

} /* namespace */
