#include <registration/AlgoRegistration.hpp>
#include "AutomataUnion.h"

namespace {

auto AutomataUnionEpsilonTransitionEpsilonNFA2 = registration::AbstractRegister<automaton::transform::AutomataUnion, automaton::EpsilonNFA<DefaultSymbolType, ext::pair<DefaultStateType, unsigned>>, const automaton::EpsilonNFA<>&, const automaton::EpsilonNFA<>&>(automaton::transform::AutomataUnion::unification, "first", "second").setDocumentation("Union of two automata without epsilon transitions.\n\
\n\
@param first First automaton (A1)\n\
@param second Second automaton (A2)\n\
@return nondeterministic epsilon FA representing the union of two automata");

auto AutomataUnionEpsilonTransitionNFA2 = registration::AbstractRegister<automaton::transform::AutomataUnion, automaton::NFA<DefaultSymbolType, ext::pair<DefaultStateType, unsigned>>, const automaton::NFA<>&, const automaton::NFA<>&>(automaton::transform::AutomataUnion::unification, "first", "second").setDocumentation("Union of two automata without epsilon transitions.\n\
\n\
@param first First automaton (A1)\n\
@param second Second automaton (A2)\n\
@return nondeterministic FA representing the union of two automata");

auto AutomataUnionEpsilonTransitionDFA2 = registration::AbstractRegister<automaton::transform::AutomataUnion, automaton::NFA<DefaultSymbolType, ext::pair<DefaultStateType, unsigned>>, const automaton::DFA<>&, const automaton::DFA<>&>(automaton::transform::AutomataUnion::unification, "first", "second").setDocumentation("Union of two automata without epsilon transitions.\n\
\n\
@param first First automaton (A1)\n\
@param second Second automaton (A2)\n\
@return nondeterministic FA representing the union of two automata");

} /* namespace */
