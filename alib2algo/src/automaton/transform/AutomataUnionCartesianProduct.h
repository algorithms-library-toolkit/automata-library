/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <alib/pair>

#include <automaton/FSM/EpsilonNFA.h>

namespace automaton {

namespace transform {

/**
 * Union of two finite automata.
 * For finite automata A1, A2, we create a finite automaton A such that L(A) = L(A1) \cup L(A2).
 * This method utilizes epsilon transitions in finite automata (Melichar: Jazyky a překlady, 2.71).
 */
class AutomataUnionCartesianProduct {
public:
    /**
     * Union of two automata.
     * @tparam SymbolType Type for input symbols.
     * @tparam StateType1 Type for states in the first automaton.
     * @tparam StateType2 Type for states in the second automaton.
     * @param first First automaton (A1)
     * @param second Second automaton (A2)
     * @return (non)deterministic FA representing the union of two automata
     */
    template <class SymbolType, class StateType1, class StateType2>
    static automaton::NFA<SymbolType, ext::pair<StateType1, StateType2>> unification(const automaton::NFA<SymbolType, StateType1>& first, const automaton::NFA<SymbolType, StateType2>& second);

    /**
     * @overload
     */
    template <class SymbolType, class StateType1, class StateType2>
    static automaton::DFA<SymbolType, ext::pair<StateType1, StateType2>> unification(const automaton::DFA<SymbolType, StateType1>& first, const automaton::DFA<SymbolType, StateType2>& second);
};

template <class SymbolType, class StateType1, class StateType2>
automaton::DFA<SymbolType, ext::pair<StateType1, StateType2>> AutomataUnionCartesianProduct::unification(const automaton::DFA<SymbolType, StateType1>& first, const automaton::DFA<SymbolType, StateType2>& second)
{
    if (!first.isTotal() || !second.isTotal())
        throw exception::CommonException("Automata must be total to unify with cartesian product");
    if (first.getInputAlphabet() != second.getInputAlphabet())
        throw exception::CommonException("Automata must have the same input alphabet to compute cartesian product");

    ext::pair<StateType1, StateType2> q0(first.getInitialState(), second.getInitialState());
    automaton::DFA<SymbolType, ext::pair<StateType1, StateType2>> res(q0);

    for (const auto& a : first.getInputAlphabet())
        res.addInputSymbol(a);
    for (const auto& a : second.getInputAlphabet())
        res.addInputSymbol(a);

    for (const auto& p : first.getStates())
        for (const auto& q : second.getStates())
            res.addState(ext::make_pair(p, q));

    for (const auto& p : first.getFinalStates())
        for (const auto& q : second.getStates())
            res.addFinalState(ext::make_pair(p, q));

    for (const auto& p : first.getStates())
        for (const auto& q : second.getFinalStates())
            res.addFinalState(ext::make_pair(p, q));

    for (const auto& state : res.getStates())
        for (const auto& tp : first.getTransitionsFromState(state.first))
            for (const auto& tq : second.getTransitionsFromState(state.second))
                if (tp.first.second == tq.first.second)
                    res.addTransition(state, tp.first.second, ext::make_pair(tp.second, tq.second));


    return res;
}

template <class SymbolType, class StateType1, class StateType2>
automaton::NFA<SymbolType, ext::pair<StateType1, StateType2>> AutomataUnionCartesianProduct::unification(const automaton::NFA<SymbolType, StateType1>& first, const automaton::NFA<SymbolType, StateType2>& second)
{
    if (!first.isTotal() || !second.isTotal())
        throw exception::CommonException("Automata must be total to unify with cartesian product");
    if (first.getInputAlphabet() != second.getInputAlphabet())
        throw exception::CommonException("Automata must have the same input alphabet to compute cartesian product");

    ext::pair<StateType1, StateType2> q0(first.getInitialState(), second.getInitialState());
    automaton::NFA<SymbolType, ext::pair<StateType1, StateType2>> res(q0);

    for (const auto& a : first.getInputAlphabet())
        res.addInputSymbol(a);
    for (const auto& a : second.getInputAlphabet())
        res.addInputSymbol(a);

    for (const auto& p : first.getStates())
        for (const auto& q : second.getStates())
            res.addState(ext::make_pair(p, q));

    for (const auto& p : first.getFinalStates())
        for (const auto& q : second.getStates())
            res.addFinalState(ext::make_pair(p, q));

    for (const auto& p : first.getStates())
        for (const auto& q : second.getFinalStates())
            res.addFinalState(ext::make_pair(p, q));

    for (const auto& state : res.getStates())
        for (const auto& tp : first.getTransitionsFromState(state.first))
            for (const auto& tq : second.getTransitionsFromState(state.second))
                if (tp.first.second == tq.first.second)
                    res.addTransition(state, tp.first.second, ext::make_pair(tp.second, tq.second));

    return res;
}

} /* namespace transform */

} /* namespace automaton */
