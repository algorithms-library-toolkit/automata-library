#include <registration/AlgoRegistration.hpp>
#include "Reverse.h"

namespace {

auto ReverseDFA2 = registration::AbstractRegister<automaton::transform::Reverse, automaton::MultiInitialStateNFA<>, const automaton::DFA<>&>(automaton::transform::Reverse::convert, "automaton").setDocumentation("Computation of reverse of a finite automaton.\n\
\n\
@param automaton the automaton to reverse\n\
@return multi-initial state nondeterministic FA accepting reversed language of @p automaton");

auto ReverseNFA2 = registration::AbstractRegister<automaton::transform::Reverse, automaton::MultiInitialStateNFA<>, const automaton::NFA<>&>(automaton::transform::Reverse::convert, "automaton").setDocumentation("Computation of reverse of a finite automaton.\n\
\n\
@param automaton the automaton to reverse\n\
@return multi-initial state nondeterministic FA accepting reversed language of @p automaton");

auto ReverseMultiInitialStateNFA2 = registration::AbstractRegister<automaton::transform::Reverse, automaton::MultiInitialStateNFA<>, const automaton::MultiInitialStateNFA<>&>(automaton::transform::Reverse::convert, "automaton").setDocumentation("Computation of reverse of a finite automaton.\n\
\n\
@param automaton the automaton to reverse\n\
@return multi-initial state nondeterministic FA accepting reversed language of @p automaton");

} /* namespace */
