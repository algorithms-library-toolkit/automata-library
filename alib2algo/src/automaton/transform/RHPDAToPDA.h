/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <automaton/PDA/DPDA.h>
#include <automaton/PDA/NPDA.h>
#include <automaton/PDA/RealTimeHeightDeterministicDPDA.h>
#include <automaton/PDA/RealTimeHeightDeterministicNPDA.h>

namespace automaton {

namespace transform {

/**
 * Transforms real-time height-deterministic pushdown automaton (RHPDA) to a pushdown automaton (PDA).
 */
class RHPDAToPDA {
public:
    /**
     * Transformation of a RHPDA to a PDA.
     * @param pda automaton to transform
     * @return (non) deterministic PDA equivalent to @p automaton
     */
    static automaton::DPDA<> convert(const automaton::RealTimeHeightDeterministicDPDA<>& pda);

    /**
     * @overload
     */
    static automaton::DPDA<> convert(const automaton::DPDA<>& pda);

    /**
     * @overload
     */
    static automaton::NPDA<> convert(const automaton::RealTimeHeightDeterministicNPDA<>& pda);

    /**
     * @overload
     */
    static automaton::NPDA<> convert(const automaton::NPDA<>& pda);
};

} /* namespace transform */

} /* namespace automaton */
