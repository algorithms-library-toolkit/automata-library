#include "PDAToRHPDA.h"

#include <registration/AlgoRegistration.hpp>
#include <registration/CastRegistration.hpp>

namespace {

auto PDAToRHPDARealTimeHeightDeterministicDPDA = registration::AbstractRegister<automaton::transform::PDAToRHPDA, automaton::RealTimeHeightDeterministicDPDA<>, const automaton::RealTimeHeightDeterministicDPDA<>&>(automaton::transform::PDAToRHPDA::convert);

auto PDAToRHPDARealTimeHeightDeterministicNPDA = registration::AbstractRegister<automaton::transform::PDAToRHPDA, automaton::RealTimeHeightDeterministicNPDA<>, const automaton::RealTimeHeightDeterministicNPDA<>&>(automaton::transform::PDAToRHPDA::convert);

auto PDAToRHPDADPDA = registration::AbstractRegister<automaton::transform::PDAToRHPDA, automaton::RealTimeHeightDeterministicDPDA<DefaultSymbolType, DefaultSymbolType, ext::variant<DefaultStateType, std::string>>, const automaton::DPDA<>&>(automaton::transform::PDAToRHPDA::convert);

auto PDAToRHPDANPDA = registration::AbstractRegister<automaton::transform::PDAToRHPDA, automaton::RealTimeHeightDeterministicNPDA<DefaultSymbolType, DefaultSymbolType, ext::variant<DefaultStateType, std::string>>, const automaton::NPDA<>&>(automaton::transform::PDAToRHPDA::convert);

auto RealTimeHeightDeterministicDPDAFromDPDA = registration::CastRegister<automaton::RealTimeHeightDeterministicDPDA<DefaultSymbolType, DefaultSymbolType, ext::variant<DefaultStateType, std::string>>, automaton::DPDA<>>(automaton::transform::PDAToRHPDA::convert);
auto RealTimeHeightDeterministicNPDAFromNPDA = registration::CastRegister<automaton::RealTimeHeightDeterministicNPDA<DefaultSymbolType, DefaultSymbolType, ext::variant<DefaultStateType, std::string>>, automaton::NPDA<>>(automaton::transform::PDAToRHPDA::convert);

} /* namespace */
