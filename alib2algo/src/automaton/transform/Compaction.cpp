#include <registration/AlgoRegistration.hpp>
#include "Compaction.h"

namespace {

auto CompactionCompactDFA = registration::AbstractRegister<automaton::transform::Compaction, automaton::CompactDFA<>, const automaton::CompactDFA<>&>(automaton::transform::Compaction::convert, "automaton").setDocumentation("Compaction of a finite automaton.\n\
\n\
@param automaton automaton to compact\n\
@return compact nondeterministic FA equivalent to @p automaton");

auto CompactionCompactNFA = registration::AbstractRegister<automaton::transform::Compaction, automaton::CompactNFA<>, const automaton::CompactNFA<>&>(automaton::transform::Compaction::convert, "automaton").setDocumentation("Compaction of a finite automaton.\n\
\n\
@param automaton automaton to compact\n\
@return compact nondeterministic FA equivalent to @p automaton");

auto CompactionDFA = registration::AbstractRegister<automaton::transform::Compaction, automaton::CompactDFA<>, const automaton::DFA<>&>(automaton::transform::Compaction::convert, "automaton").setDocumentation("Compaction of a finite automaton.\n\
\n\
@param automaton automaton to compact\n\
@return compact nondeterministic FA equivalent to @p automaton");

auto CompactionNFA = registration::AbstractRegister<automaton::transform::Compaction, automaton::CompactNFA<>, const automaton::NFA<>&>(automaton::transform::Compaction::convert, "automaton").setDocumentation("Compaction of a finite automaton.\n\
\n\
@param automaton automaton to compact\n\
@return compact nondeterministic FA equivalent to @p automaton");

} /* namespace */
