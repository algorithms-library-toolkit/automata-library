#include <common/DefaultStateType.h>
#include <registration/AlgoRegistration.hpp>
#include "AutomataConcatenationEpsilonTransition.h"

namespace {

auto AutomataConcatenationEpsilonTransitionDFA = registration::AbstractRegister<automaton::transform::AutomataConcatenationEpsilonTransition, automaton::EpsilonNFA<DefaultSymbolType, ext::pair<DefaultStateType, unsigned>>, const automaton::DFA<>&, const automaton::DFA<>&>(automaton::transform::AutomataConcatenationEpsilonTransition::concatenation, "first", "second").setDocumentation("Concatenates two finite automata using epsilon transitions.\n\
\n\
@param first First automaton (A1)\n\
@param second Second automaton (A2)\n\
@return nondeterministic FA representing the concatenation of two automata");

auto AutomataConcatenationEpsilonTransitionNFA = registration::AbstractRegister<automaton::transform::AutomataConcatenationEpsilonTransition, automaton::EpsilonNFA<DefaultSymbolType, ext::pair<DefaultStateType, unsigned>>, const automaton::NFA<>&, const automaton::NFA<>&>(automaton::transform::AutomataConcatenationEpsilonTransition::concatenation, "first", "second").setDocumentation("Concatenates two finite automata using epsilon transitions.\n\
\n\
@param first First automaton (A1)\n\
@param second Second automaton (A2)\n\
@return nondeterministic FA representing the concatenation of two automata");

auto AutomataConcatenationEpsilonTransitionEpsilonNFA = registration::AbstractRegister<automaton::transform::AutomataConcatenationEpsilonTransition, automaton::EpsilonNFA<DefaultSymbolType, ext::pair<DefaultStateType, unsigned>>, const automaton::EpsilonNFA<>&, const automaton::EpsilonNFA<>&>(automaton::transform::AutomataConcatenationEpsilonTransition::concatenation, "first", "second").setDocumentation("Concatenates two finite automata using epsilon transitions.\n\
\n\
@param first First automaton (A1)\n\
@param second Second automaton (A2)\n\
@return nondeterministic FA representing the concatenation of two automata");

} /* namespace */
