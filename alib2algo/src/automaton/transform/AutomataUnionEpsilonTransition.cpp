#include <registration/AlgoRegistration.hpp>
#include "AutomataUnionEpsilonTransition.h"

namespace {

auto AutomataUnionEpsilonTransitionEpsilonNFA2 = registration::AbstractRegister<automaton::transform::AutomataUnionEpsilonTransition, automaton::EpsilonNFA<DefaultSymbolType, ext::pair<DefaultStateType, unsigned>>, const automaton::EpsilonNFA<>&, const automaton::EpsilonNFA<>&>(automaton::transform::AutomataUnionEpsilonTransition::unification, "first", "second").setDocumentation("Union of two automata using epsilon transitions.\n\
\n\
@param first First automaton (A1)\n\
@param second Second automaton (A2)\n\
@return (non)deterministic FA representing the union of two automata");

auto AutomataUnionEpsilonTransitionNFA2 = registration::AbstractRegister<automaton::transform::AutomataUnionEpsilonTransition, automaton::EpsilonNFA<DefaultSymbolType, ext::pair<DefaultStateType, unsigned>>, const automaton::NFA<>&, const automaton::NFA<>&>(automaton::transform::AutomataUnionEpsilonTransition::unification, "first", "second").setDocumentation("Union of two automata using epsilon transitions.\n\
\n\
@param first First automaton (A1)\n\
@param second Second automaton (A2)\n\
@return (non)deterministic FA representing the union of two automata");

auto AutomataUnionEpsilonTransitionDFA2 = registration::AbstractRegister<automaton::transform::AutomataUnionEpsilonTransition, automaton::EpsilonNFA<DefaultSymbolType, ext::pair<DefaultStateType, unsigned>>, const automaton::DFA<>&, const automaton::DFA<>&>(automaton::transform::AutomataUnionEpsilonTransition::unification, "first", "second").setDocumentation("Union of two automata using epsilon transitions.\n\
\n\
@param first First automaton (A1)\n\
@param second Second automaton (A2)\n\
@return (non)deterministic FA representing the union of two automata");

} /* namespace */
