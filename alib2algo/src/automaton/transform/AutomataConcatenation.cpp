#include <registration/AlgoRegistration.hpp>
#include "AutomataConcatenation.h"

namespace {

auto AutomataConcatenationNFA = registration::AbstractRegister<automaton::transform::AutomataConcatenation, automaton::NFA<DefaultSymbolType, ext::pair<DefaultStateType, unsigned>>, const automaton::NFA<>&, const automaton::NFA<>&>(automaton::transform::AutomataConcatenation::concatenation, "first", "second").setDocumentation("Concatenates two finite automata without using epsilon transitions.\n\
\n\
@param first First automaton (A1)\n\
@param second Second automaton (A2)\n\
@return nondeterministic FA representing the concatenation of two automata");

auto AutomataConcatenationDFA = registration::AbstractRegister<automaton::transform::AutomataConcatenation, automaton::NFA<DefaultSymbolType, ext::pair<DefaultStateType, unsigned>>, const automaton::DFA<>&, const automaton::DFA<>&>(automaton::transform::AutomataConcatenation::concatenation, "first", "second").setDocumentation("Concatenates two finite automata without using epsilon transitions.\n\
\n\
@param first First automaton (A1)\n\
@param second Second automaton (A2)\n\
@return nondeterministic FA representing the concatenation of two automata");

} /* namespace */
