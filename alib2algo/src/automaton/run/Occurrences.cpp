#include <registration/AlgoRegistration.hpp>
#include "Occurrences.h"

namespace {

auto OccurrencesDFALinearString = registration::AbstractRegister<automaton::run::Occurrences, ext::set<unsigned>, const automaton::DFA<>&, const string::LinearString<>&>(automaton::run::Occurrences::occurrences, "automaton", "string").setDocumentation("Automaton occurrences run implementation.\n\
\n\
@param automaton the runned automaton\n\
@param string the input of the automaton\n\
@return set of indexes to the string where the automaton passed a final state");

auto OccurrencesDFTARankedTree = registration::AbstractRegister<automaton::run::Occurrences, ext::set<unsigned>, const automaton::DFTA<>&, const tree::RankedTree<>&>(automaton::run::Occurrences::occurrences, "automaton", "tree").setDocumentation("Automaton occurrences run implementation.\n\
\n\
@param automaton the runned automaton\n\
@param string the input of the automaton\n\
@return set of indexes to the string where the automaton passed a final state");

auto OccurrencesArcFactoredDeterministicZAutomatonUnrankedTree = registration::AbstractRegister<automaton::run::Occurrences, ext::set<unsigned>, const automaton::ArcFactoredDeterministicZAutomaton<>&, const tree::UnrankedTree<>&>(automaton::run::Occurrences::occurrences, "automaton", "tree");

auto OccurrencesArcFactoredDeterministicZAutomatonUnrankedTreePrefixBarTree = registration::AbstractRegister<automaton::run::Occurrences, ext::set<unsigned>, const automaton::ArcFactoredDeterministicZAutomaton<>&, const tree::PrefixBarTree<>&>(automaton::run::Occurrences::occurrences, "automaton", "tree");

auto OccurrencesUnorderedDFTARankedTree = registration::AbstractRegister<automaton::run::Occurrences, ext::set<unsigned>, const automaton::UnorderedDFTA<>&, const tree::UnorderedRankedTree<>&>(automaton::run::Occurrences::occurrences, "automaton", "tree").setDocumentation("Automaton occurrences run implementation.\n\
\n\
@param automaton the runned automaton\n\
@param string the input of the automaton\n\
@return set of indexes to the string where the automaton passed a final state");

auto OccurrencesInputDrivenDPDALinearString = registration::AbstractRegister<automaton::run::Occurrences, ext::set<unsigned>, const automaton::InputDrivenDPDA<>&, const string::LinearString<>&>(automaton::run::Occurrences::occurrences, "automaton", "string").setDocumentation("Automaton occurrences run implementation.\n\
\n\
@param automaton the runned automaton\n\
@param string the input of the automaton\n\
@return set of indexes to the string where the automaton passed a final state");

auto OccurrencesVisiblyPushdownDPDALinearString = registration::AbstractRegister<automaton::run::Occurrences, ext::set<unsigned>, const automaton::VisiblyPushdownDPDA<>&, const string::LinearString<>&>(automaton::run::Occurrences::occurrences, "automaton", "string").setDocumentation("Automaton occurrences run implementation.\n\
\n\
@param automaton the runned automaton\n\
@param string the input of the automaton\n\
@return set of indexes to the string where the automaton passed a final state");

auto OccurrencesRealTimeHeightDeterministicDPDALinearString = registration::AbstractRegister<automaton::run::Occurrences, ext::set<unsigned>, const automaton::RealTimeHeightDeterministicDPDA<>&, const string::LinearString<>&>(automaton::run::Occurrences::occurrences, "automaton", "string").setDocumentation("Automaton occurrences run implementation.\n\
\n\
@param automaton the runned automaton\n\
@param string the input of the automaton\n\
@return set of indexes to the string where the automaton passed a final state");

auto OccurrencesDPDALinearString = registration::AbstractRegister<automaton::run::Occurrences, ext::set<unsigned>, const automaton::DPDA<>&, const string::LinearString<>&>(automaton::run::Occurrences::occurrences, "automaton", "string").setDocumentation("Automaton occurrences run implementation.\n\
\n\
@param automaton the runned automaton\n\
@param string the input of the automaton\n\
@return set of indexes to the string where the automaton passed a final state");

} /* namespace */
