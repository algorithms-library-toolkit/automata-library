#include <registration/AlgoRegistration.hpp>
#include "Translate.h"

namespace {

auto TranslateNPDTALinearString = registration::AbstractRegister<automaton::run::Translate, ext::set<string::LinearString<>>, const automaton::NPDTA<>&, const string::LinearString<>&>(automaton::run::Translate::translate, "automaton", "string").setDocumentation("Implementation of a run of an automaton that is producing output.\n\
\n\
@param automaton the runned automaton\n\
@param string the input of the automaton\n\
@return set of strings representing the translations");

} /* namespace */
