#include <registration/AlgoRegistration.hpp>
#include "ToGrammarLeftRG.h"

namespace {

auto ToGrammarLeftRGNFA = registration::AbstractRegister<automaton::convert::ToGrammarLeftRG, grammar::LeftRG<>, const automaton::NFA<>&>(automaton::convert::ToGrammarLeftRG::convert, "automaton").setDocumentation("Performs the conversion of the finite automaton to left regular grammar.\n\
\n\
@param automaton a finite automaton to convert\n\
@return left regular grammar equivalent to the source @p automaton.");

auto ToGrammarLeftRGDFA = registration::AbstractRegister<automaton::convert::ToGrammarLeftRG, grammar::LeftRG<>, const automaton::DFA<>&>(automaton::convert::ToGrammarLeftRG::convert, "automaton").setDocumentation("Performs the conversion of the finite automaton to left regular grammar.\n\
\n\
@param automaton a finite automaton to convert\n\
@return left regular grammar equivalent to the source @p automaton.");

} /* namespace */
