#include <registration/AlgoRegistration.hpp>
#include "ToRegExp.h"

namespace {

auto ToRegExpEpsilonNFA = registration::AbstractRegister<automaton::convert::ToRegExp, regexp::UnboundedRegExp<>, const automaton::EpsilonNFA<>&>(automaton::convert::ToRegExp::convert, "automaton").setDocumentation("Performs the conversion (@sa ToRegExpStateElimination::convert).\n\
\n\
@param automaton the automaton to convert\n\
@return regular expression equivalent to the input @p automaton");

auto ToRegExpMultiInitialStateNFA = registration::AbstractRegister<automaton::convert::ToRegExp, regexp::UnboundedRegExp<>, const automaton::MultiInitialStateNFA<>&>(automaton::convert::ToRegExp::convert, "automaton").setDocumentation("Performs the conversion (@sa ToRegExpStateElimination::convert).\n\
\n\
@param automaton the automaton to convert\n\
@return regular expression equivalent to the input @p automaton");

auto ToRegExpNFA = registration::AbstractRegister<automaton::convert::ToRegExp, regexp::UnboundedRegExp<>, const automaton::NFA<>&>(automaton::convert::ToRegExp::convert, "automaton").setDocumentation("Performs the conversion (@sa ToRegExpStateElimination::convert).\n\
\n\
@param automaton the automaton to convert\n\
@return regular expression equivalent to the input @p automaton");

auto ToRegExpDFA = registration::AbstractRegister<automaton::convert::ToRegExp, regexp::UnboundedRegExp<>, const automaton::DFA<>&>(automaton::convert::ToRegExp::convert, "automaton").setDocumentation("Performs the conversion (@sa ToRegExpStateElimination::convert).\n\
\n\
@param automaton the automaton to convert\n\
@return regular expression equivalent to the input @p automaton");

auto ToRegExpExtendedNFA = registration::AbstractRegister<automaton::convert::ToRegExp, regexp::UnboundedRegExp<>, const automaton::ExtendedNFA<>&>(automaton::convert::ToRegExp::convert, "automaton").setDocumentation("Performs the conversion (@sa ToRegExpStateElimination::convert).\n\
\n\
@param automaton the automaton to convert\n\
@return regular expression equivalent to the input @p automaton");

auto ToRegExpCompactNFA = registration::AbstractRegister<automaton::convert::ToRegExp, regexp::UnboundedRegExp<>, const automaton::CompactNFA<>&>(automaton::convert::ToRegExp::convert, "automaton").setDocumentation("Performs the conversion (@sa ToRegExpStateElimination::convert).\n\
\n\
@param automaton the automaton to convert\n\
@return regular expression equivalent to the input @p automaton");

} /* namespace */
