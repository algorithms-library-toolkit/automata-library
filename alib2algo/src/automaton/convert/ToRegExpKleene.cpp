#include <registration/AlgoRegistration.hpp>
#include "ToRegExpKleene.h"

namespace {

auto ToRegExpKleeneDFA = registration::AbstractRegister<automaton::convert::ToRegExpKleene, regexp::UnboundedRegExp<>, const automaton::DFA<>&>(automaton::convert::ToRegExpKleene::convert, "automaton").setDocumentation("Performs conversion.\n\
\n\
@param automaton finite automaton to convert\n\
@return unbounded regular expression equivalent to the original automaton");

auto ToRegExpKleeneNFA = registration::AbstractRegister<automaton::convert::ToRegExpKleene, regexp::UnboundedRegExp<>, const automaton::NFA<>&>(automaton::convert::ToRegExpKleene::convert, "automaton").setDocumentation("Performs conversion.\n\
\n\
@param automaton finite automaton to convert\n\
@return unbounded regular expression equivalent to the original automaton");

auto ToRegExpKleeneENFA = registration::AbstractRegister<automaton::convert::ToRegExpKleene, regexp::UnboundedRegExp<>, const automaton::EpsilonNFA<>&>(automaton::convert::ToRegExpKleene::convert, "automaton").setDocumentation("Performs conversion.\n\
\n\
@param automaton finite automaton to convert\n\
@return unbounded regular expression equivalent to the original automaton");

auto ToRegExpKleeneMISNFA = registration::AbstractRegister<automaton::convert::ToRegExpKleene, regexp::UnboundedRegExp<>, const automaton::MultiInitialStateNFA<>&>(automaton::convert::ToRegExpKleene::convert, "automaton").setDocumentation("Performs conversion.\n\
\n\
@param automaton finite automaton to convert\n\
@return unbounded regular expression equivalent to the original automaton");

auto ToRegExpKleeneMISENFA = registration::AbstractRegister<automaton::convert::ToRegExpKleene, regexp::UnboundedRegExp<>, const automaton::MultiInitialStateEpsilonNFA<>&>(automaton::convert::ToRegExpKleene::convert, "automaton").setDocumentation("Performs conversion.\n\
\n\
@param automaton finite automaton to convert\n\
@return unbounded regular expression equivalent to the original automaton");

} /* namespace */
