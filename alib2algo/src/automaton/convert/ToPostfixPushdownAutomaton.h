/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <alib/variant>

#include <automaton/PDA/DPDA.h>
#include <automaton/PDA/NPDA.h>
#include <automaton/TA/DFTA.h>
#include <automaton/TA/NFTA.h>

#include <alphabet/BottomOfTheStack.h>
#include <alphabet/End.h>

namespace automaton {

namespace convert {

/**
 * Converts a finite tree automaton (FTA) to a pushdown automaton (PDA) that reads linearised trees in their postfix notation.
 */
class ToPostfixPushdownAutomaton {
public:
    /**
     * Performs the conversion of the deterministic FTA to the deterministic PDA
     * @param dfta Deterministic finite tree automaton to convert
     * @return (D)PDA equivalent to original finite tree automaton reading linearized postfix tree
     */
    template <class SymbolType, class StateType>
    static automaton::DPDA<ext::variant<common::ranked_symbol<SymbolType>, alphabet::End>, ext::variant<StateType, alphabet::BottomOfTheStack>, char> convert(const automaton::DFTA<SymbolType, StateType>& dfta);

    /**
     * Performs the conversion of the nondeterministic FTA to the nondeterministic PDA.
     * @param nfta Nondeterministic finite tree automaton to convert
     * @return (N)PDA equivalent to original finite tree automaton reading linearized postfix tree
     */
    template <class SymbolType, class StateType>
    static automaton::NPDA<ext::variant<common::ranked_symbol<SymbolType>, alphabet::End>, ext::variant<StateType, alphabet::BottomOfTheStack>, char> convert(const automaton::NFTA<SymbolType, StateType>& nfta);
};

template <class SymbolType, class StateType>
automaton::DPDA<ext::variant<common::ranked_symbol<SymbolType>, alphabet::End>, ext::variant<StateType, alphabet::BottomOfTheStack>, char> ToPostfixPushdownAutomaton::convert(const automaton::DFTA<SymbolType, StateType>& dfta)
{
    automaton::DPDA<ext::variant<common::ranked_symbol<SymbolType>, alphabet::End>, ext::variant<StateType, alphabet::BottomOfTheStack>, char> automaton('q', alphabet::BottomOfTheStack());

    for (const auto& rankedSymbol : dfta.getInputAlphabet()) {
        automaton.addInputSymbol(rankedSymbol);
    }
    automaton.addInputSymbol(alphabet::End());

    for (const StateType& state : dfta.getStates()) {
        automaton.addPushdownStoreSymbol(state);
    }

    for (const auto& transition : dfta.getTransitions()) {
        ext::vector<ext::variant<StateType, alphabet::BottomOfTheStack>> pop(transition.first.second.rbegin(), transition.first.second.rend());
        ext::vector<ext::variant<StateType, alphabet::BottomOfTheStack>> push(1, transition.second);
        automaton.addTransition(automaton.getInitialState(), transition.first.first, pop, automaton.getInitialState(), push);
    }

    auto finalPDAState = 'r';
    automaton.addState(finalPDAState);
    automaton.addFinalState(finalPDAState);

    for (const auto& finalState : dfta.getFinalStates()) {
        ext::vector<ext::variant<StateType, alphabet::BottomOfTheStack>> pop = {finalState, alphabet::BottomOfTheStack()};
        ext::vector<ext::variant<StateType, alphabet::BottomOfTheStack>> push;
        automaton.addTransition(automaton.getInitialState(), alphabet::End(), pop, finalPDAState, push);
    }

    return automaton;
}

template <class SymbolType, class StateType>
automaton::NPDA<ext::variant<common::ranked_symbol<SymbolType>, alphabet::End>, ext::variant<StateType, alphabet::BottomOfTheStack>, char> ToPostfixPushdownAutomaton::convert(const automaton::NFTA<SymbolType, StateType>& nfta)
{
    automaton::NPDA<ext::variant<common::ranked_symbol<SymbolType>, alphabet::End>, ext::variant<StateType, alphabet::BottomOfTheStack>, char> automaton('q', alphabet::BottomOfTheStack());

    for (const auto& symbol : nfta.getInputAlphabet()) {
        automaton.addInputSymbol(symbol);
    }
    automaton.addInputSymbol(alphabet::End());

    for (const StateType& state : nfta.getStates()) {
        automaton.addPushdownStoreSymbol(state);
    }

    for (const auto& transition : nfta.getTransitions()) {
        ext::vector<ext::variant<StateType, alphabet::BottomOfTheStack>> pop(transition.first.second.rbegin(), transition.first.second.rend());
        ext::vector<ext::variant<StateType, alphabet::BottomOfTheStack>> push(1, transition.second);
        automaton.addTransition(automaton.getInitialState(), transition.first.first, pop, automaton.getInitialState(), push);
    }

    char finalPDAState = 'r';
    automaton.addState(finalPDAState);
    automaton.addFinalState(finalPDAState);

    for (const StateType& finalState : nfta.getFinalStates()) {
        ext::vector<ext::variant<StateType, alphabet::BottomOfTheStack>> pop = {finalState, alphabet::BottomOfTheStack()};
        ext::vector<ext::variant<StateType, alphabet::BottomOfTheStack>> push;
        automaton.addTransition(automaton.getInitialState(), alphabet::End(), pop, finalPDAState, push);
    }

    return automaton;
}

} /* namespace convert */

} /* namespace automaton */
