/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "ToRegExpStateElimination.h"

namespace automaton {

namespace convert {

/**
 * Conversion of finite automata to regular expressions.
 * This class serves as a "default wrapper" over the conversion of FA to RE. It delegates to the State Elimination algorithm.
 * @sa ToRegExpStateElimination
 */
class ToRegExp {
public:
    /**
     * Performs the conversion (@sa ToRegExpStateElimination::convert).
     * @tparam T Type of converted automaton.
     * @param automaton the automaton to convert
     * @return regular expression equivalent to the input @p automaton
     */
    template <class T>
    static regexp::UnboundedRegExp<typename T::SymbolType> convert(const T& automaton);
};

template <class T>
regexp::UnboundedRegExp<typename T::SymbolType> ToRegExp::convert(const T& automaton)
{
    return ToRegExpStateElimination::convert(automaton);
}

} /* namespace convert */

} /* namespace automaton */
