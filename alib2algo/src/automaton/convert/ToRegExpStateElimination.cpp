#include <registration/AlgoRegistration.hpp>
#include "ToRegExpStateElimination.h"

namespace {

auto ToRegExpStateEliminationEpsilonNFA = registration::AbstractRegister<automaton::convert::ToRegExpStateElimination, regexp::UnboundedRegExp<>, const automaton::EpsilonNFA<>&>(automaton::convert::ToRegExpStateElimination::convert, "automaton").setDocumentation("Performs conversion.\n\
\n\
@param automaton finite automaton to convert\n\
@return unbounded regular expression equivalent to the original automaton");

auto ToRegExpStateEliminationMultiInitialStateNFA = registration::AbstractRegister<automaton::convert::ToRegExpStateElimination, regexp::UnboundedRegExp<>, const automaton::MultiInitialStateNFA<>&>(automaton::convert::ToRegExpStateElimination::convert, "automaton").setDocumentation("Performs conversion.\n\
\n\
@param automaton finite automaton to convert\n\
@return unbounded regular expression equivalent to the original automaton");

auto ToRegExpStateEliminationNFA = registration::AbstractRegister<automaton::convert::ToRegExpStateElimination, regexp::UnboundedRegExp<>, const automaton::NFA<>&>(automaton::convert::ToRegExpStateElimination::convert, "automaton").setDocumentation("Performs conversion.\n\
\n\
@param automaton finite automaton to convert\n\
@return unbounded regular expression equivalent to the original automaton");

auto ToRegExpStateEliminationDFA = registration::AbstractRegister<automaton::convert::ToRegExpStateElimination, regexp::UnboundedRegExp<>, const automaton::DFA<>&>(automaton::convert::ToRegExpStateElimination::convert, "automaton").setDocumentation("Performs conversion.\n\
\n\
@param automaton finite automaton to convert\n\
@return unbounded regular expression equivalent to the original automaton");

auto ToRegExpStateEliminationExtendedNFA = registration::AbstractRegister<automaton::convert::ToRegExpStateElimination, regexp::UnboundedRegExp<>, const automaton::ExtendedNFA<>&>(automaton::convert::ToRegExpStateElimination::convert, "automaton").setDocumentation("Performs conversion.\n\
\n\
@param automaton finite automaton to convert\n\
@return unbounded regular expression equivalent to the original automaton");

auto ToRegExpStateEliminationCompactNFA = registration::AbstractRegister<automaton::convert::ToRegExpStateElimination, regexp::UnboundedRegExp<>, const automaton::CompactNFA<>&>(automaton::convert::ToRegExpStateElimination::convert, "automaton").setDocumentation("Performs conversion.\n\
\n\
@param automaton finite automaton to convert\n\
@return unbounded regular expression equivalent to the original automaton");

} /* namespace */
