/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <automaton/FSM/DFA.h>
#include <automaton/FSM/EpsilonNFA.h>
#include <automaton/FSM/MultiInitialStateNFA.h>
#include <automaton/FSM/NFA.h>

#include <common/Permutation.hpp>

namespace automaton {

namespace generate {

/**
 * Algorithm for random shuffling of the finite automata states.
 * Effectively just renames the states of the FSM using the original state names.
 */
class RandomizeAutomaton {
public:
    /**
     * Shuffle the set of states of the automaton.
     *
     * @tparam SymbolType Type of input symbols.
     * @tparam StateType Type of states.
     * @param res automaton to shuffle
     */
    template <class SymbolType, class StateType>
    static automaton::EpsilonNFA<SymbolType, StateType> randomize(const automaton::EpsilonNFA<SymbolType, StateType>& fsm);

    /**
     * @overload
     */
    template <class SymbolType, class StateType>
    static automaton::MultiInitialStateNFA<SymbolType, StateType> randomize(const automaton::MultiInitialStateNFA<SymbolType, StateType>& fsm);

    /**
     * @overload
     */
    template <class SymbolType, class StateType>
    static automaton::NFA<SymbolType, StateType> randomize(const automaton::NFA<SymbolType, StateType>& fsm);

    /**
     * @overload
     */
    template <class SymbolType, class StateType>
    static automaton::DFA<SymbolType, StateType> randomize(const automaton::DFA<SymbolType, StateType>& fsm);
};

template <class SymbolType, class StateType>
automaton::DFA<SymbolType, StateType> RandomizeAutomaton::randomize(const automaton::DFA<SymbolType, StateType>& fsm)
{
    ext::map<StateType, StateType> statePermutationMap = common::Permutation::permutationMap(fsm.getStates());

    automaton::DFA<SymbolType, StateType> res(statePermutationMap.find(fsm.getInitialState())->second);

    res.setStates(fsm.getStates());
    res.setInputAlphabet(fsm.getInputAlphabet());

    for (const StateType& finalState : fsm.getFinalStates())
        res.addFinalState(statePermutationMap.find(finalState)->second);

    for (const std::pair<const ext::pair<StateType, SymbolType>, StateType>& transition : fsm.getTransitions())
        res.addTransition(statePermutationMap.find(transition.first.first)->second, transition.first.second, statePermutationMap.find(transition.second)->second);

    return res;
}

template <class SymbolType, class StateType>
automaton::MultiInitialStateNFA<SymbolType, StateType> RandomizeAutomaton::randomize(const automaton::MultiInitialStateNFA<SymbolType, StateType>& fsm)
{
    ext::map<StateType, StateType> statePermutationMap = common::Permutation::permutationMap(fsm.getStates());

    automaton::MultiInitialStateNFA<SymbolType, StateType> res;

    res.setStates(fsm.getStates());
    res.setInputAlphabet(fsm.getInputAlphabet());

    for (const StateType& initialState : fsm.getInitialStates())
        res.addInitialState(statePermutationMap.find(initialState)->second);

    for (const StateType& finalState : fsm.getFinalStates())
        res.addFinalState(statePermutationMap.find(finalState)->second);

    for (const std::pair<const ext::pair<StateType, SymbolType>, StateType>& transition : fsm.getTransitions())
        res.addTransition(statePermutationMap.find(transition.first.first)->second, transition.first.second, statePermutationMap.find(transition.second)->second);

    return res;
}

template <class SymbolType, class StateType>
automaton::NFA<SymbolType, StateType> RandomizeAutomaton::randomize(const automaton::NFA<SymbolType, StateType>& fsm)
{
    ext::map<StateType, StateType> statePermutationMap = common::Permutation::permutationMap(fsm.getStates());

    automaton::NFA<SymbolType, StateType> res(statePermutationMap.find(fsm.getInitialState())->second);

    res.setStates(fsm.getStates());
    res.setInputAlphabet(fsm.getInputAlphabet());

    for (const StateType& finalState : fsm.getFinalStates())
        res.addFinalState(statePermutationMap.find(finalState)->second);

    for (const std::pair<const ext::pair<StateType, SymbolType>, StateType>& transition : fsm.getTransitions())
        res.addTransition(statePermutationMap.find(transition.first.first)->second, transition.first.second, statePermutationMap.find(transition.second)->second);

    return res;
}

template <class SymbolType, class StateType>
automaton::EpsilonNFA<SymbolType, StateType> RandomizeAutomaton::randomize(const automaton::EpsilonNFA<SymbolType, StateType>& fsm)
{
    ext::map<StateType, StateType> statePermutationMap = common::Permutation::permutationMap(fsm.getStates());

    automaton::EpsilonNFA<SymbolType, StateType> res(statePermutationMap.find(fsm.getInitialState())->second);

    res.setStates(fsm.getStates());
    res.setInputAlphabet(fsm.getInputAlphabet());

    for (const StateType& finalState : fsm.getFinalStates())
        res.addFinalState(statePermutationMap.find(finalState)->second);

    for (const std::pair<const ext::pair<StateType, common::symbol_or_epsilon<SymbolType>>, StateType>& transition : fsm.getTransitions())
        res.addTransition(statePermutationMap.find(transition.first.first)->second, transition.first.second, statePermutationMap.find(transition.second)->second);

    return res;
}

} /* namespace generate */

} /* namespace automaton */
