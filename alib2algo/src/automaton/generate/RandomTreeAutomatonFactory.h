/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <ext/algorithm>
#include <ext/random>

#include <alib/deque>
#include <alib/set>
#include <alib/string>

#include <exception/CommonException.h>

#include <automaton/TA/NFTA.h>

namespace automaton {

namespace generate {

/**
 * Generator of random automata.
 *
 * \details
 * The underlying generation algorithm is from Leslie, T: Efficient Approaches to Subset Construction, 1995.
 */
class RandomTreeAutomatonFactory {
public:
    /**
     * Generates a random finite automaton.
     * \tparam SymbolType the type of terminal symbols of the random automaton
     *
     * \param statesCount number of states in the generated automaton
     * \param alphabet Input alphabet of the automaton
     * \param density density of the transition function
     *
     * \return random nondeterministic finite automaton
     */
    template <class SymbolType>
    static automaton::NFTA<SymbolType, unsigned> generateNFTA(size_t statesCount, const ext::set<common::ranked_symbol<SymbolType>>& alphabet, double density);

private:
    /**
     * Selects ith reachable state form \p VStates.
     *
     * \param VStates the states to select from
     * \param i the specification which reachable state to select
     *
     * \return ith reachable state based on VStates
     */
    static unsigned ithReachableState(const ext::deque<bool>& VStates, size_t i);

    /**
     * Selects ith unreachable state form \p VStates.
     *
     * \param VStates the states to select from
     * \param i the specification which unreachable state to select
     *
     * \return ith unreachable state based on VStates
     */
    static unsigned ithUnreachableState(const ext::deque<bool>& VStates, size_t i);

    /**
     * Leslie's connected NFTA algorithm
     *
     * \tparam SymbolType the type of terminal symbols of the random automaton
     *
     * \param n number of states
     * \param alphabet input alphabet
     * \param density density of the transition function (0-100). 100 means every possible transition is created
     *
     * \return the actual random nondeterministic automaton
     */
    template <class SymbolType>
    static automaton::NFTA<SymbolType, unsigned> LeslieConnectedNFTA(size_t n, const ext::deque<common::ranked_symbol<SymbolType>>& alphabet, double density);
};

template <class SymbolType>
automaton::NFTA<SymbolType, unsigned> RandomTreeAutomatonFactory::generateNFTA(size_t statesCount, const ext::set<common::ranked_symbol<SymbolType>>& alphabet, double density)
{
    ext::deque<common::ranked_symbol<SymbolType>> alphabet2(alphabet.begin(), alphabet.end());
    return RandomTreeAutomatonFactory::LeslieConnectedNFTA(statesCount, alphabet2, density);
}

template <class SymbolType>
automaton::NFTA<SymbolType, unsigned> RandomTreeAutomatonFactory::LeslieConnectedNFTA(size_t n, const ext::deque<common::ranked_symbol<SymbolType>>& alphabet, double density)
{
    if (alphabet.empty())
        throw exception::CommonException("Alphabet size must be greater than 0.");

    ext::deque<common::ranked_symbol<SymbolType>> nullaryAlphabet;
    for (const common::ranked_symbol<SymbolType>& symbol : alphabet)
        if (symbol.getRank() == 0)
            nullaryAlphabet.push_back(symbol);

    if (nullaryAlphabet.empty())
        throw exception::CommonException("Alphabet does not contain a nullary symbol");

    ext::deque<bool> VStates;
    ext::deque<unsigned> Q;

    automaton::NFTA<SymbolType, unsigned> automaton;

    for (const auto& s : alphabet)
        automaton.addInputSymbol(s);

    for (size_t i = 0; i < n; i++) {
        VStates.push_back(false);
        Q.push_back(i);
        automaton.addState(Q[i]);
    }

    if (n == 0) {
        return automaton;
    } else {
        size_t x = std::uniform_int_distribution<size_t>(0, n - 1)(ext::random_devices::semirandom);
        automaton.addTransition(nullaryAlphabet[std::uniform_int_distribution<size_t>(0, nullaryAlphabet.size() - 1)(ext::random_devices::semirandom)], ext::vector<unsigned>{}, Q[x]);

        VStates[x] = true;
    }

    size_t unvisited = n - 1;

    while (unvisited != 0) {
        size_t c = std::uniform_int_distribution<size_t>(0, alphabet.size() - 1)(ext::random_devices::semirandom);
        ext::vector<unsigned> from;
        while (from.size() < alphabet[c].getRank()) {
            size_t a = ithReachableState(VStates, std::uniform_int_distribution<size_t>(0, n - unvisited - 1)(ext::random_devices::semirandom)); // select y-th reachable state
            from.push_back(Q[a]);
        }

        size_t b = ithReachableState(VStates, std::uniform_int_distribution<size_t>(0, unvisited - 1)(ext::random_devices::semirandom)); // select z-th reachable state

        automaton.addTransition(alphabet[c], std::move(from), Q[b]);

        unvisited -= 1;
        VStates[b] = true;
    }

    for (const unsigned& q : Q)
        if (ext::uniform_unsigned_event(automaton.getTransitionsFromState(q).empty() ? 90 : 10)(ext::random_devices::semirandom))
            automaton.addFinalState(q);

    if (automaton.getFinalStates().empty()) {
        if (n == 1) {
            automaton.addFinalState(*automaton.getStates().begin());
        } else {
            for (const unsigned& q : Q) {
                if (automaton.getTransitionsFromState(q).empty()) {
                    automaton.addFinalState(q);
                    break;
                }
            }
        }
    }

    size_t accSourceStates = 1;
    for (const common::ranked_symbol<SymbolType>& symbol : alphabet)
        accSourceStates += symbol.getRank();

    double mnn100 = 100.0 / alphabet.size() / accSourceStates / n; // FIXME check the computation of density
    while (automaton.getTransitions().size() * mnn100 < density) {
        size_t a = std::uniform_int_distribution<size_t>(0, alphabet.size() - 1)(ext::random_devices::semirandom);

        ext::vector<unsigned> from;
        while (from.size() < alphabet[a].getRank()) {
            size_t y = std::uniform_int_distribution<size_t>(0, n - 1)(ext::random_devices::semirandom);
            from.push_back(Q[y]);
        }

        size_t z = std::uniform_int_distribution<size_t>(0, n - 1)(ext::random_devices::semirandom);

        automaton.addTransition(alphabet[a], std::move(from), Q[z]);
    }

    ext::set<common::ranked_symbol<SymbolType>> alphabetUsage = automaton.getInputAlphabet();
    for (const auto& t : automaton.getTransitions())
        alphabetUsage.erase(t.first.first);

    for (const auto& kv : alphabetUsage)
        automaton.removeInputSymbol(kv);

    return automaton;
}

} /* namespace generate */

} /* namespace automaton */
