#include <registration/AlgoRegistration.hpp>
#include "RandomAutomatonFactory.h"

namespace automaton::generate {

unsigned RandomAutomatonFactory::ithReachableState(const ext::deque<bool>& VStates, size_t i)
{
    i++;
    for (size_t j = 0; j < VStates.size(); j++) {
        if (VStates[j])
            i--;

        if (i == 0)
            return j;
    }
    throw std::logic_error("Not enough states in deque of visited states");
}

unsigned RandomAutomatonFactory::ithUnreachableState(const ext::deque<bool>& VStates, size_t i)
{
    i++;
    for (size_t j = 0; j < VStates.size(); j++) {
        if (!VStates[j])
            i--;

        if (i == 0)
            return j;
    }
    throw std::logic_error("Not enough states in deque of visited states");
}

} /* namespace automaton::generate */

namespace {

auto GenerateNFA1 = registration::AbstractRegister<automaton::generate::RandomAutomatonFactory, automaton::NFA<DefaultSymbolType, unsigned>, size_t, const ext::set<DefaultSymbolType>&, double>(automaton::generate::RandomAutomatonFactory::generateNFA, abstraction::AlgorithmCategories::AlgorithmCategory::DEFAULT, "statesCount", "alphabet", "density").setDocumentation("Generates a random finite automaton.\n\
@param statesCount number of states in the generated automaton\n\
@param alphabet Input alphabet of the automaton\n\
@param density density of the transition function (0-100)\n\
@return random nondeterministic finite automaton");

} /* namespace */
