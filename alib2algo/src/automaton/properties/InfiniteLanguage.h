/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <queue>
#include <set>

#include <automaton/FSM/DFA.h>
#include <automaton/FSM/NFA.h>

#include <automaton/properties/UsefulStates.h>

namespace automaton {

namespace properties {

/**
 * Determine whether language defined by DFA is infinite.
 */
class InfiniteLanguage {
public:
    /**
     * Detects whether the language accepted by the automaton is finite or infinite.
     *
     * @T the type of tested automaton
     *
     * @param fsm automaton
     *
     * @return boolean indicating infinitness of the language
     */
    template <class T>
        requires isDFA<T> || isNFA<T>
    static bool infinite(const T& fsm);
};

template <class T>
    requires isDFA<T> || isNFA<T> bool
InfiniteLanguage::infinite(const T& fsm)
{
    using StateType = typename T::StateType;

    const ext::set<StateType> usefulStates = automaton::properties::UsefulStates::usefulStates(fsm);

    std::queue<StateType> q;
    std::set<StateType> visited{fsm.getInitialState()};

    q.push(fsm.getInitialState());

    while (!q.empty()) {
        const StateType state = std::move(q.front());
        q.pop();

        for (const auto& transition : fsm.getTransitionsFromState(state)) {
            if (visited.insert(transition.second).second) {
                q.push(transition.second);
            } else if (usefulStates.contains(transition.second)) {
                return true;
            }
        }
    }

    return false;
}

} /* namespace properties */

} /* namespace automaton */
