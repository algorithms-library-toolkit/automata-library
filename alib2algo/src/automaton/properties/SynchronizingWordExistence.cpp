#include <registration/AlgoRegistration.hpp>
#include "SynchronizingWordExistence.h"

namespace {

auto SynchronizingWordExistenceDFA = registration::AbstractRegister<automaton::properties::SynchronizingWordExistence, bool, const automaton::DFA<>&>(automaton::properties::SynchronizingWordExistence::exists, "dfa").setDocumentation("Checks whether the given DFA has a synchronizing word.\n\
\n\
@param dfa automaton\n\
@return boolean indicating whether the automaton has a synchronizing word");

} /* namespace */
