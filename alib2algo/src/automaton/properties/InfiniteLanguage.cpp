#include <registration/AlgoRegistration.hpp>
#include "InfiniteLanguage.h"

namespace {

auto InfiniteLanguageNFA = registration::AbstractRegister<automaton::properties::InfiniteLanguage, bool, const automaton::NFA<>&>(automaton::properties::InfiniteLanguage::infinite, "fsm").setDocumentation("Determines whether input automaton accepts infinite language.\n\
\n\
@param fsm the tested automaton\n\
@return true if the language is infinite, false otherwise");

auto InfiniteLanguageDFA = registration::AbstractRegister<automaton::properties::InfiniteLanguage, bool, const automaton::DFA<>&>(automaton::properties::InfiniteLanguage::infinite, "fsm").setDocumentation("Determines whether input automaton accepts infinite language.\n\
\n\
@param fsm the tested automaton\n\
@return true if the language is infinite, false otherwise");

} /* namespace */
