#include <registration/AlgoRegistration.hpp>
#include "AllEpsilonClosure.h"

namespace {

auto AllEpsilonClosureEpsilonNFA = registration::AbstractRegister<automaton::properties::AllEpsilonClosure, ext::map<DefaultStateType, ext::set<DefaultStateType>>, const automaton::EpsilonNFA<>&>(automaton::properties::AllEpsilonClosure::allEpsilonClosure, "fsm").setDocumentation("Computes epsilon closure for all states of a nondeterministic finite automaton with epsilon transitions.\n\
Implemented using closures (breadth-first search).\n\
\n\
@param fsm nondeterministic finite automaton with epsilon transitions\n\
@return mapping of states to set of states representing the epsilon closures for each state of @p fsm");

auto AllEpsilonClosureEpsilonNFTA = registration::AbstractRegister<automaton::properties::AllEpsilonClosure, ext::map<DefaultStateType, ext::set<DefaultStateType>>, const automaton::EpsilonNFTA<>&>(automaton::properties::AllEpsilonClosure::allEpsilonClosure, "fta").setDocumentation("Computes epsilon closure for all states of a nondeterministic finite tree automaton with epsilon transitions.\n\
Implemented using closures (breadth-first search).\n\
\n\
@param tfa nondeterministic finite tree automaton with epsilon transitions\n\
@return mapping of states to set of states representing the epsilon closures for each state of @p fta");

auto AllEpsilonClosureMultiInitialStateNFA = registration::AbstractRegister<automaton::properties::AllEpsilonClosure, ext::map<DefaultStateType, ext::set<DefaultStateType>>, const automaton::MultiInitialStateNFA<>&>(automaton::properties::AllEpsilonClosure::allEpsilonClosure, "fsm").setDocumentation("Computes epsilon closure for all states of a nondeterministic finite automaton with epsilon transitions.\n\
Implemented using closures (breadth-first search).\n\
\n\
@param fsm nondeterministic finite automaton with epsilon transitions\n\
@return mapping of states to set of states representing the epsilon closures for each state of @p fsm");

auto AllEpsilonClosureNFA = registration::AbstractRegister<automaton::properties::AllEpsilonClosure, ext::map<DefaultStateType, ext::set<DefaultStateType>>, const automaton::NFA<>&>(automaton::properties::AllEpsilonClosure::allEpsilonClosure, "fsm").setDocumentation("Computes epsilon closure for all states of a nondeterministic finite automaton with epsilon transitions.\n\
Implemented using closures (breadth-first search).\n\
\n\
@param fsm nondeterministic finite automaton with epsilon transitions\n\
@return mapping of states to set of states representing the epsilon closures for each state of @p fsm");

auto AllEpsilonClosureDFA = registration::AbstractRegister<automaton::properties::AllEpsilonClosure, ext::map<DefaultStateType, ext::set<DefaultStateType>>, const automaton::DFA<>&>(automaton::properties::AllEpsilonClosure::allEpsilonClosure, "fsm").setDocumentation("Computes epsilon closure for all states of a nondeterministic finite automaton with epsilon transitions.\n\
Implemented using closures (breadth-first search).\n\
\n\
@param fsm nondeterministic finite automaton with epsilon transitions\n\
@return mapping of states to set of states representing the epsilon closures for each state of @p fsm");

auto AllEpsilonClosureExtendedNFA = registration::AbstractRegister<automaton::properties::AllEpsilonClosure, ext::map<DefaultStateType, ext::set<DefaultStateType>>, const automaton::ExtendedNFA<>&>(automaton::properties::AllEpsilonClosure::allEpsilonClosure, "fsm").setDocumentation("Computes epsilon closure for all states of a nondeterministic finite automaton with epsilon transitions.\n\
Implemented using closures (breadth-first search).\n\
\n\
@param fsm nondeterministic finite automaton with epsilon transitions\n\
@return mapping of states to set of states representing the epsilon closures for each state of @p fsm");

auto AllEpsilonClosureCompactNFA = registration::AbstractRegister<automaton::properties::AllEpsilonClosure, ext::map<DefaultStateType, ext::set<DefaultStateType>>, const automaton::CompactNFA<>&>(automaton::properties::AllEpsilonClosure::allEpsilonClosure, "fsm").setDocumentation("Computes epsilon closure for all states of a nondeterministic finite automaton with epsilon transitions.\n\
Implemented using closures (breadth-first search).\n\
\n\
@param fsm nondeterministic finite automaton with epsilon transitions\n\
@return mapping of states to set of states representing the epsilon closures for each state of @p fsm");

auto AllEpsilonClosureNondeterministicZAutomaton = registration::AbstractRegister<automaton::properties::AllEpsilonClosure, ext::map<DefaultStateType, ext::set<DefaultStateType>>, const automaton::NondeterministicZAutomaton<>&>(automaton::properties::AllEpsilonClosure::allEpsilonClosure, "fsm");

} /* namespace */
