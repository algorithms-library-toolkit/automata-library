#include <registration/AlgoRegistration.hpp>
#include "UndistinguishableStates.h"

namespace {

auto UndistinguishableStatesDFA = registration::AbstractRegister<automaton::properties::UndistinguishableStates, ext::set<ext::pair<DefaultStateType, DefaultStateType>>, const automaton::DFA<>&>(automaton::properties::UndistinguishableStates::undistinguishable, "fsm").setDocumentation("Computes the partitions of undistinguishable states states in given DFA.\n\
\n\
@param dfa finite automaton.\n\
@return undistinguishable states relation");

auto UndistinguishableStatesDFTA = registration::AbstractRegister<automaton::properties::UndistinguishableStates, ext::set<ext::pair<DefaultStateType, DefaultStateType>>, const automaton::DFTA<>&>(automaton::properties::UndistinguishableStates::undistinguishable, "fta").setDocumentation("Computes the partitions of undistinguishable states states in given DFTA.\n\
\n\
@param dfta finite tree automaton.\n\
@return undistinguishable states relation");

} /* namespace */
