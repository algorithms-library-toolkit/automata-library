#include <registration/AlgoRegistration.hpp>
#include "ForwardBisimulation.h"

namespace {

auto ForwardBisimulationNFA = registration::AbstractRegister<automaton::properties::ForwardBisimulation, ext::set<ext::pair<DefaultStateType, DefaultStateType>>, const automaton::NFA<>&>(automaton::properties::ForwardBisimulation::forwardBisimulation, "fta").setDocumentation("Computes a relation on states of the NFA satisfying the forward bisimulation definition.\n\
\n\
@param fta the examined automaton.\n\
@return set of pairs of states of the @p fta that are the forward bisimulation.");

auto ForwardBisimulationDFA = registration::AbstractRegister<automaton::properties::ForwardBisimulation, ext::set<ext::pair<DefaultStateType, DefaultStateType>>, const automaton::DFA<>&>(automaton::properties::ForwardBisimulation::forwardBisimulation, "fta").setDocumentation("Computes a relation on states of the DFA satisfying the forward bisimulation definition.\n\
\n\
@param fta the examined automaton.\n\
@return set of pairs of states of the @p fta that are the forward bisimulation.");

auto ForwardBisimulationArcFactoredNondeterministicZAutomaton = registration::AbstractRegister<automaton::properties::ForwardBisimulation, ext::set<ext::pair<DefaultStateType, DefaultStateType>>, const automaton::ArcFactoredNondeterministicZAutomaton<>&>(automaton::properties::ForwardBisimulation::forwardBisimulation, "afnza").setDocumentation("Computes a relation on states of the Arc Factored Nondeterministic Z Automaton satisfying the forward bisimulation definition.\n\
\n\
@param afnza the examined automaton.\n\
@return set of pairs of states of the @p afnza that are the forward bisimulation.");

auto ForwardBisimulationArcFactoredDeterministicZAutomaton = registration::AbstractRegister<automaton::properties::ForwardBisimulation, ext::set<ext::pair<DefaultStateType, DefaultStateType>>, const automaton::ArcFactoredDeterministicZAutomaton<>&>(automaton::properties::ForwardBisimulation::forwardBisimulation, "afdza").setDocumentation("Computes a relation on states of the Arc Factored Deterministic Z Automaton satisfying the forward bisimulation definition.\n\
\n\
@param afdza the examined automaton.\n\
@return set of pairs of states of the @p afdza that are the forward bisimulation.");

auto ForwardBisimulationNFTA = registration::AbstractRegister<automaton::properties::ForwardBisimulation, ext::set<ext::pair<DefaultStateType, DefaultStateType>>, const automaton::NFTA<>&>(automaton::properties::ForwardBisimulation::forwardBisimulation, "fta").setDocumentation("Computes a relation on states of the NFTA satisfying the forward bisimulation definition.\n\
\n\
@param fta the examined automaton.\n\
@return set of pairs of states of the @p fta that are the forward bisimulation.");

auto ForwardBisimulationDFTA = registration::AbstractRegister<automaton::properties::ForwardBisimulation, ext::set<ext::pair<DefaultStateType, DefaultStateType>>, const automaton::DFTA<>&>(automaton::properties::ForwardBisimulation::forwardBisimulation, "fta").setDocumentation("Computes a relation on states of the DFTA satisfying the forward bisimulation definition.\n\
\n\
@param fta the examined automaton.\n\
@return set of pairs of states of the @p fta that are the forward bisimulation.");

auto ForwardBisimulationUnorderedNFTA = registration::AbstractRegister<automaton::properties::ForwardBisimulation, ext::set<ext::pair<DefaultStateType, DefaultStateType>>, const automaton::UnorderedNFTA<>&>(automaton::properties::ForwardBisimulation::forwardBisimulation, "fta").setDocumentation("Computes a relation on states of the Unordered NFTA satisfying the forward bisimulation definition.\n\
\n\
@param fta the examined automaton.\n\
@return set of pairs of states of the @p fta that are the forward bisimulation.");

auto ForwardBisimulationUnorderedDFTA = registration::AbstractRegister<automaton::properties::ForwardBisimulation, ext::set<ext::pair<DefaultStateType, DefaultStateType>>, const automaton::UnorderedDFTA<>&>(automaton::properties::ForwardBisimulation::forwardBisimulation, "fta").setDocumentation("Computes a relation on states of the Unordered DFTA satisfying the forward bisimulation definition.\n\
\n\
@param fta the examined automaton.\n\
@return set of pairs of states of the @p fta that are the forward bisimulation.");

} /* namespace */
