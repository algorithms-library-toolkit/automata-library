#include <registration/AlgoRegistration.hpp>
#include "UsefulStates.h"

namespace {

auto UsefulStatesEpsilonNFA = registration::AbstractRegister<automaton::properties::UsefulStates, ext::set<DefaultStateType>, const automaton::EpsilonNFA<>&>(automaton::properties::UsefulStates::usefulStates, "fsm").setDocumentation("Finds all useful states of a finite automaton.\n\
Using closure implementation of the BFS algorithm.\n\
\n\
@param fsm automaton\n\
@return set of useful states of @p fsm");

auto UsefulStatesNFA = registration::AbstractRegister<automaton::properties::UsefulStates, ext::set<DefaultStateType>, const automaton::NFA<>&>(automaton::properties::UsefulStates::usefulStates, "fsm").setDocumentation("Finds all useful states of a finite automaton.\n\
Using closure implementation of the BFS algorithm.\n\
\n\
@param fsm automaton\n\
@return set of useful states of @p fsm");

auto UsefulStatesCompactNFA = registration::AbstractRegister<automaton::properties::UsefulStates, ext::set<DefaultStateType>, const automaton::CompactNFA<>&>(automaton::properties::UsefulStates::usefulStates, "fsm").setDocumentation("Finds all useful states of a finite automaton.\n\
Using closure implementation of the BFS algorithm.\n\
\n\
@param fsm automaton\n\
@return set of useful states of @p fsm");

auto UsefulStatesExtendedNFA = registration::AbstractRegister<automaton::properties::UsefulStates, ext::set<DefaultStateType>, const automaton::ExtendedNFA<>&>(automaton::properties::UsefulStates::usefulStates, "fsm").setDocumentation("Finds all useful states of a finite automaton.\n\
Using closure implementation of the BFS algorithm.\n\
\n\
@param fsm automaton\n\
@return set of useful states of @p fsm");

auto UsefulStatesMultiInitialStateNFA = registration::AbstractRegister<automaton::properties::UsefulStates, ext::set<DefaultStateType>, const automaton::MultiInitialStateNFA<>&>(automaton::properties::UsefulStates::usefulStates, "fsm").setDocumentation("Finds all useful states of a finite automaton.\n\
Using closure implementation of the BFS algorithm.\n\
\n\
@param fsm automaton\n\
@return set of useful states of @p fsm");

auto UsefulStatesDFA = registration::AbstractRegister<automaton::properties::UsefulStates, ext::set<DefaultStateType>, const automaton::DFA<>&>(automaton::properties::UsefulStates::usefulStates, "fsm").setDocumentation("Finds all useful states of a finite automaton.\n\
Using closure implementation of the BFS algorithm.\n\
\n\
@param fsm automaton\n\
@return set of useful states of @p fsm");

auto UsefulStatesDFTA = registration::AbstractRegister<automaton::properties::UsefulStates, ext::set<DefaultStateType>, const automaton::DFTA<>&>(automaton::properties::UsefulStates::usefulStates, "fta").setDocumentation("Finds all useful states of a finite tree automaton.\n\
Using closure implementation of the BFS algorithm.\n\
\n\
@param fta automaton\n\
@return set of useful states of @p fta");

auto UsefulStatesNFTA = registration::AbstractRegister<automaton::properties::UsefulStates, ext::set<DefaultStateType>, const automaton::NFTA<>&>(automaton::properties::UsefulStates::usefulStates, "fta").setDocumentation("Finds all useful states of a finite tree automaton.\n\
Using closure implementation of the BFS algorithm.\n\
\n\
@param fta automaton\n\
@return set of useful states of @p fta");

auto UsefulStatesAFDZA = registration::AbstractRegister<automaton::properties::UsefulStates, ext::set<DefaultStateType>, const automaton::ArcFactoredDeterministicZAutomaton<>&>(automaton::properties::UsefulStates::usefulStates, "afdza").setDocumentation("Finds all useful states of a arc-factored deterministic z-automaton.\n\
\n\
@param afdza automaton\n\
@return set of useful states of @p afdza");

auto UsefulStatesAFNZA = registration::AbstractRegister<automaton::properties::UsefulStates, ext::set<DefaultStateType>, const automaton::ArcFactoredNondeterministicZAutomaton<>&>(automaton::properties::UsefulStates::usefulStates, "afnza").setDocumentation("Finds all useful states of a arc-factored nondeterministic z-automaton.\n\
\n\
@param afnza automaton\n\
@return set of useful states of @p afnza");

} /* namespace */
