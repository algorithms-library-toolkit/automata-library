/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <alib/map>
#include <alib/pair>
#include <alib/set>

#include <automaton/TA/DFTA.h>
#include <automaton/TA/NFTA.h>

#include <automaton/TA/UnorderedDFTA.h>
#include <automaton/TA/UnorderedNFTA.h>

#include <automaton/FSM/DFA.h>
#include <automaton/FSM/NFA.h>

#include <automaton/TA/ArcFactoredDeterministicZAutomaton.h>
#include <automaton/TA/ArcFactoredNondeterministicZAutomaton.h>

namespace automaton {

namespace properties {

/**
 * Find all backwardBisimulation pairs of states of DFA.
 * Implements table-filling algorithm, Hopcroft 2nd edition, 4.4.1
 */
class BackwardBisimulation {
    template <class StateType>
    static ext::set<ext::pair<StateType, StateType>> initial(const ext::set<StateType>& states, const ext::set<StateType>& initials)
    {
        ext::set<ext::pair<StateType, StateType>> init;

        for (const StateType& a : states) {
            for (const StateType& b : states) {
                if (initials.count(a) == initials.count(b)) {
                    init.insert(ext::make_pair(a, b));
                    init.insert(ext::make_pair(b, a));
                }
            }
        }

        return init;
    }

public:
    /**
     * Computes a relation on states of the DFA satisfying the backward bisimulation definition.
     *
     * @param fta the examined automaton
     *
     * @return set of pairs of states of the @p fta that are the backward bisimulation.
     */
    template <class SymbolType, class StateType>
    static ext::set<ext::pair<StateType, StateType>> backwardBisimulation(const automaton::DFA<SymbolType, StateType>& fta);

    /**
     * Computes a relation on states of the NFA satisfying the backward bisimulation definition.
     *
     * @param fta the examined automaton
     *
     * @return set of pairs of states of the @p fta that are the backward bisimulation.
     */
    template <class SymbolType, class StateType>
    static ext::set<ext::pair<StateType, StateType>> backwardBisimulation(const automaton::NFA<SymbolType, StateType>& fta);

    template <class SymbolType, class StateType>
    static ext::set<ext::pair<StateType, StateType>> backwardBisimulation(const automaton::ArcFactoredDeterministicZAutomaton<SymbolType, StateType>& afdza);

    template <class SymbolType, class StateType>
    static ext::set<ext::pair<StateType, StateType>> backwardBisimulation(const automaton::ArcFactoredNondeterministicZAutomaton<SymbolType, StateType>& afnza);

    /**
     * Computes a relation on states of the DFTA satisfying the backward bisimulation definition.
     *
     * @param fta the examined automaton
     *
     * @return set of pairs of states of the @p fta that are the backward bisimulation.
     */
    template <class SymbolType, class StateType>
    static ext::set<ext::pair<StateType, StateType>> backwardBisimulation(const automaton::DFTA<SymbolType, StateType>& fta);

    /**
     * Computes a relation on states of the NFTA satisfying the backward bisimulation definition.
     *
     * @param fta the examined automaton
     *
     * @return set of pairs of states of the @p fta that are the backward bisimulation.
     */
    template <class SymbolType, class StateType>
    static ext::set<ext::pair<StateType, StateType>> backwardBisimulation(const automaton::NFTA<SymbolType, StateType>& fta);

    /**
     * Computes a relation on states of the DFTA satisfying the backward bisimulation definition.
     *
     * @param fta the examined automaton
     *
     * @return set of pairs of states of the @p fta that are the backward bisimulation.
     */
    template <class SymbolType, class StateType>
    static ext::set<ext::pair<StateType, StateType>> backwardBisimulation(const automaton::UnorderedDFTA<SymbolType, StateType>& fta);

    /**
     * Computes a relation on states of the NFTA satisfying the backward bisimulation definition.
     *
     * @param fta the examined automaton
     *
     * @return set of pairs of states of the @p fta that are the backward bisimulation.
     */
    template <class SymbolType, class StateType>
    static ext::set<ext::pair<StateType, StateType>> backwardBisimulation(const automaton::UnorderedNFTA<SymbolType, StateType>& fta);
};

template <class SymbolType, class StateType>
ext::set<ext::pair<StateType, StateType>> BackwardBisimulation::backwardBisimulation(const automaton::DFA<SymbolType, StateType>& fta)
{
    return backwardBisimulation(NFA<SymbolType, StateType>(fta));
}

template <class SymbolType, class StateType>
ext::set<ext::pair<StateType, StateType>> BackwardBisimulation::backwardBisimulation(const automaton::NFA<SymbolType, StateType>& fta)
{
    ext::set<ext::pair<StateType, StateType>> backwardBisimulation = initial(fta.getStates(), {fta.getInitialState()});

    bool changed;
    do {
        changed = false;

        for (const StateType& p : fta.getStates()) {
            for (const StateType& q : fta.getStates()) {
                if (!backwardBisimulation.contains(ext::make_pair(p, q)))
                    continue;

                for (const std::pair<const ext::pair<StateType, SymbolType>, StateType>& pTransition : fta.getTransitionsToState(p)) {

                    bool exists = false;
                    for (const std::pair<const ext::pair<StateType, SymbolType>, StateType>& qTransition : fta.getTransitionsToState(q)) {
                        if (qTransition.first.second != pTransition.first.second)
                            continue;

                        if (backwardBisimulation.contains(ext::make_pair(pTransition.first.first, qTransition.first.first))) {
                            exists = true;
                            break;
                        }
                    }

                    if (!exists) {
                        backwardBisimulation.erase(ext::make_pair(p, q));
                        backwardBisimulation.erase(ext::make_pair(q, p));
                        changed = true;
                        break;
                    }
                }
            }
        }
    } while (changed);

    return backwardBisimulation;
}

template <class SymbolType, class StateType>
ext::set<ext::pair<StateType, StateType>> BackwardBisimulation::backwardBisimulation(const automaton::ArcFactoredDeterministicZAutomaton<SymbolType, StateType>& afdza)
{
    return backwardBisimulation(ArcFactoredNondeterministicZAutomaton<SymbolType, StateType>(afdza));
}

template <class SymbolType, class StateType>
ext::set<ext::pair<StateType, StateType>> BackwardBisimulation::backwardBisimulation(const automaton::ArcFactoredNondeterministicZAutomaton<SymbolType, StateType>& afnza)
{
    ext::set<ext::pair<StateType, StateType>> backwardBisimulation = initial(afnza.getStates(), {});

    bool changed;
    do {
        changed = false;

        for (const StateType& p : afnza.getStates()) {
            for (const StateType& q : afnza.getStates()) {
                if (!backwardBisimulation.contains(ext::make_pair(p, q)))
                    continue;

                for (const std::pair<const ext::variant<SymbolType, ext::pair<StateType, StateType>>, StateType>& pTransition : afnza.getTransitionsToState(p)) {
                    if (!pTransition.first.template is<SymbolType>())
                        continue;

                    const SymbolType& pSource = pTransition.first.template get<SymbolType>();

                    bool exists = false;
                    for (const std::pair<const ext::variant<SymbolType, ext::pair<StateType, StateType>>, StateType>& qTransition : afnza.getTransitionsToState(q)) {
                        if (!qTransition.first.template is<SymbolType>())
                            continue;

                        const SymbolType& qSource = qTransition.first.template get<SymbolType>();

                        if (qSource != pSource)
                            continue;

                        exists = true;
                        break;
                    }

                    if (!exists) {
                        backwardBisimulation.erase(ext::make_pair(p, q));
                        backwardBisimulation.erase(ext::make_pair(q, p));
                        changed = true;
                        break;
                    }
                }
            }
        }
    } while (changed);

    do {
        changed = false;

        for (const StateType& p : afnza.getStates()) {
            for (const StateType& q : afnza.getStates()) {
                if (!backwardBisimulation.contains(ext::make_pair(p, q)))
                    continue;

                for (const std::pair<const ext::variant<SymbolType, ext::pair<StateType, StateType>>, StateType>& pTransition : afnza.getTransitionsToState(p)) {
                    if (pTransition.first.template is<SymbolType>())
                        continue;

                    const ext::pair<StateType, StateType>& pSource = pTransition.first.template get<ext::pair<StateType, StateType>>();

                    bool exists = false;
                    for (const std::pair<const ext::variant<SymbolType, ext::pair<StateType, StateType>>, StateType>& qTransition : afnza.getTransitionsToState(q)) {
                        if (qTransition.first.template is<SymbolType>())
                            continue;

                        const ext::pair<StateType, StateType>& qSource = qTransition.first.template get<ext::pair<StateType, StateType>>();

                        if (backwardBisimulation.contains(ext::make_pair(pSource.first, qSource.first))
                            && backwardBisimulation.contains(ext::make_pair(pSource.second, qSource.second))) {
                            exists = true;
                            break;
                        }
                    }

                    if (!exists) {
                        backwardBisimulation.erase(ext::make_pair(p, q));
                        backwardBisimulation.erase(ext::make_pair(q, p));
                        changed = true;
                        break;
                    }
                }
            }
        }
    } while (changed);

    return backwardBisimulation;
}

template <class SymbolType, class StateType>
ext::set<ext::pair<StateType, StateType>> BackwardBisimulation::backwardBisimulation(const automaton::DFTA<SymbolType, StateType>& fta)
{
    return backwardBisimulation(NFTA<SymbolType, StateType>(fta));
}

template <class SymbolType, class StateType>
ext::set<ext::pair<StateType, StateType>> BackwardBisimulation::backwardBisimulation(const automaton::NFTA<SymbolType, StateType>& fta)
{
    ext::set<ext::pair<StateType, StateType>> backwardBisimulation = initial(fta.getStates(), {});

    bool changed;
    do {
        changed = false;

        for (const StateType& p : fta.getStates()) {
            for (const StateType& q : fta.getStates()) {
                if (!backwardBisimulation.contains(ext::make_pair(p, q)))
                    continue;

                for (const std::pair<const ext::pair<common::ranked_symbol<SymbolType>, ext::vector<StateType>>, StateType>& pTransition : fta.getTransitionsToState(p)) {

                    bool exists = false;
                    for (const std::pair<const ext::pair<common::ranked_symbol<SymbolType>, ext::vector<StateType>>, StateType>& qTransition : fta.getTransitionsToState(q)) {
                        if (qTransition.first.first != pTransition.first.first)
                            continue;

                        size_t inRelation = 0;
                        for (size_t i = 0; i < pTransition.first.second.size(); ++i) {
                            if (backwardBisimulation.contains(ext::make_pair(pTransition.first.second[i], qTransition.first.second[i])))
                                ++inRelation;
                        }

                        if (inRelation == pTransition.first.second.size()) {
                            exists = true;
                            break;
                        }
                    }

                    if (!exists) {
                        backwardBisimulation.erase(ext::make_pair(p, q));
                        backwardBisimulation.erase(ext::make_pair(q, p));
                        changed = true;
                        break;
                    }
                }
            }
        }
    } while (changed);

    return backwardBisimulation;
}

template <class SymbolType, class StateType>
ext::set<ext::pair<StateType, StateType>> BackwardBisimulation::backwardBisimulation(const automaton::UnorderedDFTA<SymbolType, StateType>& fta)
{
    return backwardBisimulation(UnorderedNFTA<SymbolType, StateType>(fta));
}

template <class SymbolType, class StateType>
ext::set<ext::pair<StateType, StateType>> BackwardBisimulation::backwardBisimulation(const automaton::UnorderedNFTA<SymbolType, StateType>& fta)
{
    ext::set<ext::pair<StateType, StateType>> backwardBisimulation = initial(fta.getStates(), {});

    bool changed;
    do {
        changed = false;

        for (const StateType& p : fta.getStates()) {
            for (const StateType& q : fta.getStates()) {
                if (!backwardBisimulation.contains(ext::make_pair(p, q)))
                    continue;

                for (const std::pair<const ext::pair<common::ranked_symbol<SymbolType>, ext::multiset<StateType>>, StateType>& pTransition : fta.getTransitionsToState(p)) {

                    ext::multiset<StateType> pSource;
                    for (const StateType& state : pTransition.first.second) {
                        auto lower = backwardBisimulation.lower_bound(ext::slice_comp(state));
                        pSource.insert(lower->second);
                    }

                    bool exists = false;
                    for (const std::pair<const ext::pair<common::ranked_symbol<SymbolType>, ext::multiset<StateType>>, StateType>& qTransition : fta.getTransitionsToState(q)) {
                        if (qTransition.first.first != pTransition.first.first)
                            continue;

                        ext::multiset<StateType> qSource;
                        for (const StateType& state : qTransition.first.second) {
                            auto lower = backwardBisimulation.lower_bound(ext::slice_comp(state));
                            qSource.insert(lower->second);
                        }

                        if (pSource == qSource) {
                            exists = true;
                            break;
                        }
                    }

                    if (!exists) {
                        backwardBisimulation.erase(ext::make_pair(p, q));
                        backwardBisimulation.erase(ext::make_pair(q, p));
                        changed = true;
                        break;
                    }
                }
            }
        }
    } while (changed);

    return backwardBisimulation;
}

} /* namespace properties */

} /* namespace automaton */
