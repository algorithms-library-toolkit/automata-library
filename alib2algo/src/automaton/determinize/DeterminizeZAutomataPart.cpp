#include <registration/AlgoRegistration.hpp>
#include "Determinize.h"

namespace {

auto DeterminizeArcFactoredNondeterministicZAutomaton = registration::AbstractRegister<automaton::determinize::Determinize, automaton::ArcFactoredDeterministicZAutomaton<DefaultSymbolType, ext::set<DefaultStateType>>, const automaton::ArcFactoredNondeterministicZAutomaton<>&>(automaton::determinize::Determinize::determinize, "automaton");

} /* namespace */
