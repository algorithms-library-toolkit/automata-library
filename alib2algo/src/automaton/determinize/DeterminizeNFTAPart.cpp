#include <registration/AlgoRegistration.hpp>
#include "Determinize.h"

namespace {

auto DeterminizeUnorderedNFTA = registration::AbstractRegister<automaton::determinize::Determinize, automaton::UnorderedDFTA<DefaultSymbolType, ext::set<DefaultSymbolType>>, const automaton::UnorderedNFTA<>&>(automaton::determinize::Determinize::determinize, "nfta").setDocumentation("Implementation of subset determinization for nondeterministic finite tree automata.\n\
\n\
@param nfta nondeterministic finite tree automaton\n\
@return deterministic finite tree automaton equivalent to @p nfta");

auto DeterminizeNFTA = registration::AbstractRegister<automaton::determinize::Determinize, automaton::DFTA<DefaultSymbolType, ext::set<DefaultSymbolType>>, const automaton::NFTA<>&>(automaton::determinize::Determinize::determinize, "nfta").setDocumentation("Implementation of subset determinization for nondeterministic finite tree automata.\n\
\n\
@param nfta nondeterministic finite tree automaton\n\
@return deterministic finite tree automaton equivalent to @p nfta");

} /* namespace */
