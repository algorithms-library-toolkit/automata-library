#include <registration/AlgoRegistration.hpp>
#include "Determinize.h"

namespace {

auto DeterminizeNFA = registration::AbstractRegister<automaton::determinize::Determinize, automaton::DFA<DefaultSymbolType, ext::set<DefaultStateType>>, const automaton::NFA<>&>(automaton::determinize::Determinize::determinize, "nfa").setDocumentation("Implementation of subset determinization for nondeterministic finite automata.\n\
\n\
@param nfa nondeterministic finite automaton\n\
@return deterministic finite automaton equivalent to @p nfa");

auto DeterminizeMultiInitialStateNFA = registration::AbstractRegister<automaton::determinize::Determinize, automaton::DFA<DefaultSymbolType, ext::set<DefaultStateType>>, const automaton::MultiInitialStateNFA<>&>(automaton::determinize::Determinize::determinize, "nfa").setDocumentation("Implementation of subset determinization for nondeterministic finite automata with multiple initial states.\n\
\n\
@param nfa nondeterministic finite automaton with multiple initial states\n\
@return deterministic finite automaton equivalent to @p nfa");

} /* namespace */
