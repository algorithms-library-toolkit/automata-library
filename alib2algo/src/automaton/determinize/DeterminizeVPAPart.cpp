#include <registration/AlgoRegistration.hpp>
#include "Determinize.h"

namespace {

auto DeterminizeVisiblyPushdownNPDA = registration::AbstractRegister<automaton::determinize::Determinize, automaton::VisiblyPushdownDPDA<DefaultSymbolType, ext::pair<ext::set<ext::pair<DefaultStateType, DefaultStateType>>, DefaultSymbolType>, ext::set<ext::pair<DefaultStateType, DefaultStateType>>>, const automaton::VisiblyPushdownNPDA<>&>(automaton::determinize::Determinize::determinize, "npda").setDocumentation("Determinization of nondeterministic visibly pushdown automata.\n\
\n\
@param npda nondeterministic visibly pushdown automaton\n\
@return deterministic pushdown automaton equivalent to @p npda");

} /* namespace */
