#include <registration/AlgoRegistration.hpp>
#include "ExactCompare.h"

namespace {

auto ExactCompareLinearString = registration::AbstractRegister<string::naive::ExactCompare, int, const string::LinearString<>&, const string::LinearString<>&>(string::naive::ExactCompare::compare, "u", "v").setDocumentation("Implementation of exact comparison of strings.\n\
\n\
@param u the first string to compare\n\
@param v the second string to compare\n\
@return negative value if u compares smaller than v, positive value if u compares bigger than v, zero if u and v are equal");

auto ExactCompareCyclicString = registration::AbstractRegister<string::naive::ExactCompare, int, const string::CyclicString<>&, const string::CyclicString<>&>(string::naive::ExactCompare::compare, "u", "v").setDocumentation("Implementation of exact comparison of strings without taking rotation into account.\n\
\n\
@param u the first string to compare\n\
@param v the second string to compare\n\
@return negative value if u compares smaller than v, positive value if u compares bigger than v, zero if u and v are equal");

} /* namespace */
