#include <registration/AlgoRegistration.hpp>
#include "GoodSuffixShiftTable.h"

namespace {

auto GoodSuffixShiftTableLinearString = registration::AbstractRegister<string::properties::GoodSuffixShiftTable, ext::vector<size_t>, const string::LinearString<>&>(string::properties::GoodSuffixShiftTable::gss);

} /* namespace */
