#pragma once

#include <alib/map>
#include <alib/set>

#include <string/LinearString.h>

namespace string {

namespace properties {

/**
 * Computation of BCS table for BMH from MI(E+\eps)-EVY course 2014
 * To get rid of zeros in BCS table we ignore last haystack character
 */
class BadCharacterShiftTable {
public:
    /**
     * Search for pattern in linear string.
     * @return set set of occurences
     */
    template <class SymbolType>
    static ext::map<SymbolType, size_t> bcs(const string::LinearString<SymbolType>& pattern);
};

template <class SymbolType>
ext::map<SymbolType, size_t> BadCharacterShiftTable::bcs(const string::LinearString<SymbolType>& pattern)
{
    const ext::set<SymbolType>& alphabet = pattern.getAlphabet();
    ext::map<SymbolType, size_t> bcs;

    /* Initialization of BCS to the length of the needle. */
    for (const auto& symbol : alphabet)
        bcs.insert(std::make_pair(symbol, pattern.getContent().size()));

    /* Filling out BCS, ignoring last character. */
    for (size_t i = 0; i < pattern.getContent().size() - 1; i++)
        bcs[pattern.getContent().at(i)] = pattern.getContent().size() - i - 1;

    /*
    for(const auto& kv: bcs)
            common::Streams::out << std::string(kv.first) << " " << kv.second << std::endl;
    for(const auto& s: string.getContent())
            common::Streams::out << std::string(s);common::Streams::out << std::endl;
    */

    return bcs;
}

} /* namespace properties */

} /* namespace string */
