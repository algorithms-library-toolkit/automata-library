#include <registration/AlgoRegistration.hpp>
#include "ReversedBadCharacterShiftTable.h"

namespace {

auto ReversedBadCharacterShiftTableLinearString = registration::AbstractRegister<string::properties::ReversedBadCharacterShiftTable, ext::map<DefaultSymbolType, size_t>, const string::LinearString<>&>(string::properties::ReversedBadCharacterShiftTable::bcs);

} /* namespace */
