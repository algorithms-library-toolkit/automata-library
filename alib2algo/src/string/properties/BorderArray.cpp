#include <registration/AlgoRegistration.hpp>
#include "BorderArray.h"

namespace {

auto BorderArrayLinearString = registration::AbstractRegister<string::properties::BorderArray, ext::vector<size_t>, const string::LinearString<>&>(string::properties::BorderArray::construct);

} /* namespace */
