#include <registration/AlgoRegistration.hpp>
#include "Repetition.h"

namespace {
// todo Note is it necessary to register
auto Repetition = registration::AbstractRegister<string::properties::Repetition, ext::tuple<size_t, size_t, size_t>, const string::LinearString<>&>(string::properties::Repetition::construct);

} /* namespace */
