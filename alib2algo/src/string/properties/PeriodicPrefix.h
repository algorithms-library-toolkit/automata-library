#pragma once

#include <alib/pair>
#include <alib/vector>

#include <string/LinearString.h>

namespace string::properties {

class PeriodicPrefix {
public:
    /**
     * Computes the longest periodic prefix of a string and its period
     * Naive solution implemented
     * @param string string to compute the periodic prefix for
     * @return Pair of lenght of that prefix and lenght of its period, if no prefix is return pair (string len, string len). Note that even though prefix length = string len is valid result, period len may never be string len in a valid situation
     */
    template <class SymbolType>
    static ext::pair<size_t, size_t> construct(const string::LinearString<SymbolType>& string);
};

namespace {

// return period if pattern is periodic, else lenght of x
template <class SymbolType>
size_t hasShortPeriod(const ext::vector<SymbolType>& x, size_t prefixLen)
{
    for (size_t per = 1; per <= prefixLen / 2; ++per) {
        bool hasPer = true;
        for (size_t i = per; i < prefixLen; ++i) {
            if (x[i] != x[i - per]) {
                hasPer = false;
                break;
            }
        }
        if (hasPer)
            return per;
    }
    return x.size();
}

} /* namespace */

template <class SymbolType>
ext::pair<size_t, size_t> PeriodicPrefix::construct(const string::LinearString<SymbolType>& string)
{
    const auto& x = string.getContent();

    size_t maxlen = x.size();
    size_t maxper = x.size();
    for (size_t i = 1; i <= x.size(); ++i) {
        auto per = hasShortPeriod(x, i);
        if (per != x.size()) {
            maxlen = i;
            maxper = per;
        }
    }

    return ext::make_pair(maxlen, maxper);
}

} /* namespace string::properties */
