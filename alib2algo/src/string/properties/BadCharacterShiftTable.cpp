#include <registration/AlgoRegistration.hpp>
#include "BadCharacterShiftTable.h"

namespace {

auto BadCharacterShiftTableLinearString = registration::AbstractRegister<string::properties::BadCharacterShiftTable, ext::map<DefaultSymbolType, size_t>, const string::LinearString<>&>(string::properties::BadCharacterShiftTable::bcs);

} /* namespace */
