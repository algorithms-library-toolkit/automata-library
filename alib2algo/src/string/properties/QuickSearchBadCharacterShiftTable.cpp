#include <registration/AlgoRegistration.hpp>
#include "QuickSearchBadCharacterShiftTable.h"

namespace {

auto QuickSearchBadCharacterShiftTableLinearString = registration::AbstractRegister<string::properties::QuickSearchBadCharacterShiftTable, ext::map<DefaultSymbolType, size_t>, const string::LinearString<>&>(string::properties::QuickSearchBadCharacterShiftTable::qsbcs);

} /* namespace */
