#include <registration/AlgoRegistration.hpp>
#include "PeriodicPrefix.h"

namespace {

auto PeriodicPrefix = registration::AbstractRegister<string::properties::PeriodicPrefix, ext::pair<size_t, size_t>, const string::LinearString<>&>(string::properties::PeriodicPrefix::construct);

} /* namespace */
