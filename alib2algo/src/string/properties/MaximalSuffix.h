#pragma once

#include <alib/pair>
#include <alib/vector>

#include <string/LinearString.h>

namespace string::properties {

class MaximalSuffix {

    template <class SymbolType, class Comparator>
    static ext::pair<size_t, size_t> constructImpl(const ext::vector<SymbolType>& x, const Comparator& comp);

public:
    /**
     * Computes maximal suffix and its period of string
     * Inspired be fig.17 in by article Two-way string-matching by Crochemore
     * @param string string to compute max suffix for
     * @return Pair of starting index of the suffix and lenght of its period
     */
    template <class SymbolType>
    static ext::pair<size_t, size_t> construct(const string::LinearString<SymbolType>& string)
    {
        return constructImpl(string.getContent(), std::less<SymbolType>());
    }

    /**
     * Computes maximal suffix and its period of string for reversed alphabet
     * Inspired be fig.17 in by article Two-way string-matching by Crochemore
     * @note this is for reversed alphabet
     * @param string string to compute max suffix for
     * @return Pair of starting index of the suffix and lenght of its period
     */
    template <class SymbolType>
    static ext::pair<size_t, size_t> constructReversed(const string::LinearString<SymbolType>& string)
    {
        return constructImpl(string.getContent(), std::greater<SymbolType>());
    }
};

template <class SymbolType, class Comparator>
ext::pair<size_t, size_t> MaximalSuffix::constructImpl(const ext::vector<SymbolType>& x, const Comparator& comp)
{
    size_t i = 0;
    size_t j = 1;
    size_t k = 1;
    size_t p = 1;

    while (j + k <= x.size()) {
        auto A = x[i + k - 1];
        auto a = x[j + k - 1];
        if (comp(a, A)) {
            j = j + k;
            k = 1;
            p = j - i;
        } else if (a == A) {
            if (k == p) {
                j = j + p;
                k = 1;
            } else {
                k = k + 1;
            }
        } else {
            i = j;
            j = i + 1;
            k = 1;
            p = 1;
        }
    }

    return ext::make_pair(i, p);
}

} /* namespace string::properties */
