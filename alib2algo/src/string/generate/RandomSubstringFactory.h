#pragma once

#include <ext/algorithm>
#include <ext/random>

#include <exception/CommonException.h>

#include <string/LinearString.h>

namespace string {

namespace generate {

/**
 * Generates a radnom substring of given size from a string.
 *
 */
class RandomSubstringFactory {
public:
    /**
     * Generates a random substring of a given size from a string.
     *
     * \tparam SymbolType the type of symbols in the string
     *
     * \param size the length of generated substring
     * \param string the source string of the substring
     *
     * \return a substring of the @p string
     */
    template <class SymbolType>
    static string::LinearString<SymbolType> generateSubstring(size_t size, const string::LinearString<SymbolType>& string);
};

template <class SymbolType>
string::LinearString<SymbolType> RandomSubstringFactory::generateSubstring(size_t size, const string::LinearString<SymbolType>& string)
{
    if (size > string.getContent().size())
        throw exception::CommonException("String not long enough");

    size_t begin = ext::random_devices::semirandom() % (string.getContent().size() - size + 1);

    ext::vector<SymbolType> data(string.getContent().begin() + begin, string.getContent().begin() + begin + size);

    return LinearString<SymbolType>(string.getAlphabet(), data);
}

} /* namespace generate */

} /* namespace string */
