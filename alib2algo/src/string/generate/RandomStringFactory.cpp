#include "RandomStringFactory.h"

#include <registration/AlgoRegistration.hpp>

namespace {

auto GenerateLinearString3 = registration::AbstractRegister<string::generate::RandomStringFactory, string::LinearString<>, size_t, ext::set<DefaultSymbolType>>(string::generate::RandomStringFactory::generateLinearString, abstraction::AlgorithmCategories::AlgorithmCategory::DEFAULT, "size", "alphabet").setDocumentation("Generates a random string of given size\n\
\n\
@param size the length of the generated string\n\
@param alphabet alphabet of the generated string\n\
@return random string");

} /* namespace */
