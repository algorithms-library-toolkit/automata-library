#pragma once

#include <ext/random>

#include <alib/set>
#include <alib/vector>

#include <exception/CommonException.h>

#include <string/LinearString.h>

namespace string {

namespace generate {

/**
 * Generator of random strings.
 *
 */
class RandomStringFactory {
    /**
     * Generates a random string of given size
     * \tparam SymbolType the type of symbols of the random string
     *
     * \param size the length of the generated string
     * \param alphabet alphabet of the generated string
     *
     * \return random string
     */
    template <class SymbolType>
    static string::LinearString<SymbolType> generateLinearString(size_t size, ext::vector<SymbolType> alphabet);

public:
    /**
     * Generates a random string of given size
     * \tparam SymbolType the type of symbols of the random string
     *
     * \param size the length of the generated string
     * \param alphabet alphabet of the generated string
     *
     * \return random string
     */
    template <class SymbolType>
    static string::LinearString<SymbolType> generateLinearString(size_t size, ext::set<SymbolType> alphabet);
};

template <class SymbolType>
string::LinearString<SymbolType> RandomStringFactory::generateLinearString(size_t size, ext::set<SymbolType> alphabet)
{
    return generateLinearString(size, ext::vector<SymbolType>(alphabet.begin(), alphabet.end()));
}

template <class SymbolType>
string::LinearString<SymbolType> RandomStringFactory::generateLinearString(size_t size, ext::vector<SymbolType> alphabet)
{

    if (alphabet.empty())
        throw exception::CommonException("Alphabet size must be greater than 0.");

    ext::vector<SymbolType> elems;

    for (size_t i = 0; i < size; i++)
        elems.push_back(alphabet[ext::random_devices::semirandom() % alphabet.size()]);

    return string::LinearString<SymbolType>(elems);
}

} /* namespace generate */

} /* namespace string */
