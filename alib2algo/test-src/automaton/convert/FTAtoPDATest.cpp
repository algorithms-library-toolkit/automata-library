#include <catch2/catch.hpp>

#include <alphabet/BottomOfTheStack.h>
#include <automaton/TA/DFTA.h>
#include <automaton/TA/NFTA.h>

#include <automaton/PDA/DPDA.h>
#include <automaton/PDA/NPDA.h>
#include <automaton/xml/PDA/DPDA.h>
#include <automaton/xml/PDA/NPDA.h>

#include <automaton/convert/ToPostfixPushdownAutomaton.h>

#include <factory/XmlDataFactory.hpp>

#include <label/FinalStateLabel.h>
#include <label/InitialStateLabel.h>

TEST_CASE("FTAtoPDA", "[unit][algo][automaton][convert]")
{
    SECTION("Test DFTA to DPDA")
    {
        automaton::DFTA<> automaton;

        const common::ranked_symbol<> a(object::ObjectFactory<>::construct('a'), 2);
        const common::ranked_symbol<> b(object::ObjectFactory<>::construct('b'), 1);
        const common::ranked_symbol<> c(object::ObjectFactory<>::construct('c'), 0);
        const ext::set<common::ranked_symbol<>> alphabet{a, b, c};
        automaton.setInputAlphabet(alphabet);

        automaton.addState(object::ObjectFactory<>::construct(1));
        automaton.addState(object::ObjectFactory<>::construct(2));
        automaton.addState(object::ObjectFactory<>::construct(3));

        ext::vector<DefaultStateType> a1States = {object::ObjectFactory<>::construct(1), object::ObjectFactory<>::construct(3)};
        automaton.addTransition(a, a1States, object::ObjectFactory<>::construct(1));
        ext::vector<DefaultStateType> a2States = {object::ObjectFactory<>::construct(3), object::ObjectFactory<>::construct(3)};
        automaton.addTransition(a, a2States, object::ObjectFactory<>::construct(2));
        ext::vector<DefaultStateType> bStates = {object::ObjectFactory<>::construct(2)};
        automaton.addTransition(b, bStates, object::ObjectFactory<>::construct(1));
        ext::vector<DefaultStateType> cStates;
        automaton.addTransition(c, cStates, object::ObjectFactory<>::construct(3));

        automaton.addFinalState(object::ObjectFactory<>::construct(3));

        automaton::DPDA<ext::variant<common::ranked_symbol<object::Object>, alphabet::End>, ext::variant<object::Object, alphabet::BottomOfTheStack>, char> res = automaton::convert::ToPostfixPushdownAutomaton::convert(automaton);
        CHECK(res.getStates().size() == 2);
        CHECK(res.getTransitions().size() == 5);
        CHECK(res.getFinalStates().size() == 1);
    }

    SECTION("Test NFTA to NPDA")
    {
        automaton::NFTA<> automaton;

        const common::ranked_symbol<> a(object::ObjectFactory<>::construct('a'), 2);
        const common::ranked_symbol<> b(object::ObjectFactory<>::construct('b'), 1);
        const common::ranked_symbol<> c(object::ObjectFactory<>::construct('c'), 0);
        const ext::set<common::ranked_symbol<>> alphabet{a, b, c};
        automaton.setInputAlphabet(alphabet);

        automaton.addState(object::ObjectFactory<>::construct(1));
        automaton.addState(object::ObjectFactory<>::construct(2));
        automaton.addState(object::ObjectFactory<>::construct(3));

        ext::vector<DefaultStateType> a1States = {object::ObjectFactory<>::construct(1), object::ObjectFactory<>::construct(3)};
        automaton.addTransition(a, a1States, object::ObjectFactory<>::construct(1));
        automaton.addTransition(a, a1States, object::ObjectFactory<>::construct(3));
        ext::vector<DefaultStateType> a2States = {object::ObjectFactory<>::construct(3), object::ObjectFactory<>::construct(3)};
        automaton.addTransition(a, a2States, object::ObjectFactory<>::construct(2));
        ext::vector<DefaultStateType> bStates = {object::ObjectFactory<>::construct(2)};
        automaton.addTransition(b, bStates, object::ObjectFactory<>::construct(1));
        ext::vector<DefaultStateType> cStates;
        automaton.addTransition(c, cStates, object::ObjectFactory<>::construct(3));

        automaton.addFinalState(object::ObjectFactory<>::construct(3));

        automaton::NPDA<ext::variant<common::ranked_symbol<object::Object>, alphabet::End>, ext::variant<object::Object, alphabet::BottomOfTheStack>, char> res = automaton::convert::ToPostfixPushdownAutomaton::convert(automaton);

        CHECK(res.getStates().size() == 2);
        CHECK(res.getTransitions().size() == 6);
        CHECK(res.getFinalStates().size() == 1);
    }
}
