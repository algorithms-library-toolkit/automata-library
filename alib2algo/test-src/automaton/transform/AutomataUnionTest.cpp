#include <catch2/catch.hpp>

#include <alib/list>

#include "automaton/determinize/Determinize.h"
#include "automaton/simplify/EpsilonRemoverIncoming.h"
#include "automaton/simplify/MinimizeBrzozowski.h"
#include "automaton/simplify/Normalize.h"
#include "automaton/simplify/Total.h"
#include "automaton/simplify/Trim.h"
#include "automaton/transform/AutomataUnionCartesianProduct.h"
#include "automaton/transform/AutomataUnionEpsilonTransition.h"

#include <exception/CommonException.h>

TEST_CASE("Automata Union", "[unit][algo][automaton][transform]")
{
    SECTION("NFA/DFA")
    {
        // based on Melichar, 2.72

        std::string q1a = std::string("1");
        std::string q2a = std::string("2");
        std::string q0a = std::string("0");
        std::string q1b = std::string("1'");
        std::string q2b = std::string("2'");
        std::string q0b = std::string("0'");
        std::string q11 = std::string("11");
        std::string q20 = std::string("20");
        std::string q00 = std::string("00");
        std::string q02 = std::string("02");
        char a = 'a';
        char b = 'b';

        automaton::DFA<char, std::string> m1(q1a);
        automaton::DFA<char, std::string> m2(q1b);
        automaton::DFA<char, std::string> m3(q11);

        m1.setInputAlphabet({a, b});
        m1.setStates({q1a, q2a, q0a});
        m1.addTransition(q1a, a, q2a);
        m1.addTransition(q1a, b, q0a);
        m1.addTransition(q2a, a, q2a);
        m1.addTransition(q2a, b, q0a);
        m1.addTransition(q0a, a, q0a);
        m1.addTransition(q0a, b, q0a);
        m1.addFinalState(q2a);

        m2.setInputAlphabet({a, b});
        m2.setStates({q1b, q2b});
        m2.addTransition(q1b, b, q2b);
        m2.addTransition(q2b, b, q2b);
        m2.addFinalState(q2b);

        m3.setInputAlphabet({a, b});
        m3.setStates({q11, q20, q00, q02});
        m3.addTransition(q11, a, q20);
        m3.addTransition(q11, b, q02);
        m3.addTransition(q20, a, q20);
        m3.addTransition(q20, b, q00);
        m3.addTransition(q00, a, q00);
        m3.addTransition(q00, b, q00);
        m3.addTransition(q02, a, q00);
        m3.addTransition(q02, b, q02);
        m3.setFinalStates({q20, q02});

        auto u1 = automaton::transform::AutomataUnionEpsilonTransition::unification(m1, m2);
        CHECK_THROWS_AS(automaton::transform::AutomataUnionCartesianProduct::unification(m1, m2), exception::CommonException);
        CHECK_THROWS_AS(automaton::transform::AutomataUnionCartesianProduct::unification(automaton::NFA<char, std::string>(m1), automaton::NFA<char, std::string>(m2)), exception::CommonException);
        auto u2 = automaton::transform::AutomataUnionEpsilonTransition::unification(automaton::simplify::Total::total(m1), automaton::simplify::Total::total(m2));

        auto umdfa(automaton::simplify::Normalize::normalize(automaton::simplify::Trim::trim(automaton::simplify::MinimizeBrzozowski::minimize(automaton::simplify::EpsilonRemoverIncoming::remove(m3)))));
        auto umdfa1(automaton::simplify::Normalize::normalize(automaton::simplify::Trim::trim(automaton::simplify::MinimizeBrzozowski::minimize(automaton::simplify::EpsilonRemoverIncoming::remove(u1)))));
        auto umdfa2(automaton::simplify::Normalize::normalize(automaton::simplify::Trim::trim(automaton::simplify::MinimizeBrzozowski::minimize(automaton::simplify::EpsilonRemoverIncoming::remove(u2)))));

        CHECK(umdfa1 == umdfa);
        CHECK(umdfa2 == umdfa);
    }
}
