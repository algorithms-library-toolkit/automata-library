#include <catch2/catch.hpp>

#include <automaton/FSM/DFA.h>

#include "automaton/simplify/Normalize.h"
#include "automaton/simplify/Total.h"
#include "automaton/simplify/Trim.h"

TEST_CASE("Total automaton", "[unit][algo][automaton][simplify]")
{
    SECTION("DFA to DFA with total transition function")
    {
        std::string q0 = std::string("q0");
        std::string q1 = std::string("q1");
        std::string q2 = std::string("q2");

        char a = 'a';
        char b = 'b';
        char c = 'c';

        automaton::DFA<char, std::string> automaton(q0);
        automaton.setStates({q0, q1, q2});
        automaton.setFinalStates({q0, q1, q2});
        automaton.setInputAlphabet({a, b, c});

        automaton.addTransition(q0, a, q0);
        automaton.addTransition(q0, b, q1);
        automaton.addTransition(q0, c, q2);
        automaton.addTransition(q1, b, q1);
        automaton.addTransition(q1, c, q2);
        automaton.addTransition(q2, c, q2);

        automaton::DFA<char, std::string> totalAutomaton = automaton::simplify::Total::total(automaton);
        CHECK(totalAutomaton.isTotal());

        automaton::DFA<char, std::string> trimmedAutomaton = automaton::simplify::Trim::trim(automaton);
        automaton::DFA<char, std::string> trimmedTotalAutomaton = automaton::simplify::Trim::trim(totalAutomaton);

        CHECK(automaton::simplify::Normalize::normalize(trimmedAutomaton) == automaton::simplify::Normalize::normalize(trimmedTotalAutomaton));
    }
}
