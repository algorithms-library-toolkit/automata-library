#include <catch2/catch.hpp>

#include <alib/list>

#include "automaton/simplify/Minimize.h"
#include "automaton/simplify/MinimizeBrzozowski.h"
#include "automaton/simplify/Normalize.h"
#include "automaton/simplify/Trim.h"

TEST_CASE("Brzozowski minimization", "[unit][algo][automaton][simplify]")
{
    SECTION("Test DFA")
    {
        automaton::DFA<std::string, int> automaton(1);

        automaton.addState(1);
        automaton.addState(2);
        automaton.addState(3);
        automaton.addInputSymbol(std::string("a"));
        automaton.addInputSymbol(std::string("b"));

        automaton.addTransition(1, std::string("a"), 2);
        automaton.addTransition(2, std::string("b"), 1);

        automaton.addFinalState(3);

        automaton::DFA<std::string, int> minimizedHopcroft = automaton::simplify::Minimize::minimize(automaton);
        automaton::DFA<std::string, ext::set<ext::set<int>>> minimizedBrzozowski = automaton::simplify::MinimizeBrzozowski::minimize(automaton);

        CHECK(minimizedHopcroft.getStates().size() == 3);
        CHECK(automaton::simplify::Normalize::normalize(automaton::simplify::Trim::trim(minimizedHopcroft)) == automaton::simplify::Normalize::normalize(automaton::simplify::Trim::trim(minimizedBrzozowski)));
    }
}
