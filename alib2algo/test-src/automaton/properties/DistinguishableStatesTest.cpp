#include <catch2/catch.hpp>

#include <automaton/FSM/DFA.h>

#include "automaton/properties/DistinguishableStates.h"

TEST_CASE("Distringuishable States Test", "[unit][algo][automaton]")
{
    SECTION("Test")
    {
        {
            std::string qA("qA");
            std::string qB("qB");
            std::string qC("qC");
            std::string qD("qD");
            std::string qE("qE");
            std::string qF("qF");
            std::string qG("qG");
            std::string qH("qH");

            automaton::DFA<int, std::string> automaton(qA);
            automaton.setStates({qA, qB, qC, qD, qE, qF, qG, qH});
            automaton.setFinalStates({qC});
            automaton.addInputSymbols({0, 1});

            automaton.addTransition(qA, 0, qB);
            automaton.addTransition(qA, 1, qF);
            automaton.addTransition(qB, 0, qG);
            automaton.addTransition(qB, 1, qC);
            automaton.addTransition(qC, 0, qA);
            automaton.addTransition(qC, 1, qC);
            automaton.addTransition(qD, 0, qC);
            automaton.addTransition(qD, 1, qG);
            automaton.addTransition(qE, 0, qH);
            automaton.addTransition(qE, 1, qF);
            automaton.addTransition(qF, 0, qC);
            automaton.addTransition(qF, 1, qG);
            automaton.addTransition(qG, 0, qG);
            automaton.addTransition(qG, 1, qE);
            automaton.addTransition(qH, 0, qG);
            automaton.addTransition(qH, 1, qC);

            ext::set<ext::pair<std::string, std::string>> exp;
            const ext::set<ext::pair<std::string, std::string>> res = automaton::properties::DistinguishableStates::distinguishable(automaton);
            const ext::set<ext::pair<std::string, std::string>> exp_({
                ext::make_pair(qA, qB),
                ext::make_pair(qA, qC),
                ext::make_pair(qA, qD),
                ext::make_pair(qA, qF),
                ext::make_pair(qA, qG),
                ext::make_pair(qA, qH),
                ext::make_pair(qB, qC),
                ext::make_pair(qB, qD),
                ext::make_pair(qB, qE),
                ext::make_pair(qB, qF),
                ext::make_pair(qB, qG),
                ext::make_pair(qC, qD),
                ext::make_pair(qC, qE),
                ext::make_pair(qC, qF),
                ext::make_pair(qC, qG),
                ext::make_pair(qC, qH),
                ext::make_pair(qD, qE),
                ext::make_pair(qD, qG),
                ext::make_pair(qD, qH),
                ext::make_pair(qE, qF),
                ext::make_pair(qE, qG),
                ext::make_pair(qE, qH),
                ext::make_pair(qF, qG),
                ext::make_pair(qF, qH),
                ext::make_pair(qG, qH),
            });

            for (const auto& x : exp_) {
                exp.insert(ext::make_pair(x.first, x.second));
                exp.insert(ext::make_pair(x.second, x.first));
            }

            CHECK(res == exp);
        }
        {
            std::string qA("qA");
            std::string qB("qB");
            std::string qC("qC");
            std::string qD("qD");
            std::string qE("qE");
            std::string qF("qF");

            automaton::DFA<int, std::string> automaton(qA);
            automaton.setStates({qA, qB, qC, qD, qE, qF});
            automaton.setFinalStates({qC, qF});
            automaton.addInputSymbols({0, 1});

            automaton.addTransition(qA, 0, qB);
            automaton.addTransition(qA, 1, qD);
            automaton.addTransition(qB, 0, qC);
            automaton.addTransition(qD, 0, qE);
            automaton.addTransition(qE, 0, qF);

            ext::set<ext::pair<std::string, std::string>> exp;
            const ext::set<ext::pair<std::string, std::string>> res = automaton::properties::DistinguishableStates::distinguishable(automaton);
            const ext::set<ext::pair<std::string, std::string>> exp_({ext::make_pair(qA, qB), ext::make_pair(qA, qC), ext::make_pair(qA, qD), ext::make_pair(qA, qE), ext::make_pair(qA, qF), ext::make_pair(qB, qC), ext::make_pair(qB, qD), ext::make_pair(qB, qF), ext::make_pair(qC, qD), ext::make_pair(qC, qE), ext::make_pair(qD, qE), ext::make_pair(qD, qF), ext::make_pair(qE, qF)});

            for (const auto& x : exp_) {
                exp.insert(ext::make_pair(x.first, x.second));
                exp.insert(ext::make_pair(x.second, x.first));
            }

            CHECK(res == exp);
        }
    }
}
