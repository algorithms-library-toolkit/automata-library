#include <alib/list>
#include <catch2/catch.hpp>

#include <alphabet/BottomOfTheStack.h>
#include <automaton/FSM/NFA.h>
#include <automaton/PDA/InputDrivenDPDA.h>
#include <automaton/PDA/InputDrivenNPDA.h>
#include <automaton/PDA/VisiblyPushdownDPDA.h>
#include <automaton/PDA/VisiblyPushdownNPDA.h>
#include <automaton/TA/NFTA.h>

#include <automaton/determinize/Determinize.h>

TEST_CASE("Determinize", "[unit][algo][automaton][determinize]")
{
    SECTION("NFA")
    {
        automaton::NFA<> automaton(object::ObjectFactory<>::construct(1));

        automaton.addState(object::ObjectFactory<>::construct(1));
        automaton.addState(object::ObjectFactory<>::construct(2));
        automaton.addState(object::ObjectFactory<>::construct(3));
        automaton.addInputSymbol(object::ObjectFactory<>::construct("a"));
        automaton.addInputSymbol(object::ObjectFactory<>::construct("b"));

        automaton.addTransition(object::ObjectFactory<>::construct(1), object::ObjectFactory<>::construct("a"), object::ObjectFactory<>::construct(2));
        automaton.addTransition(object::ObjectFactory<>::construct(2), object::ObjectFactory<>::construct("b"), object::ObjectFactory<>::construct(1));

        automaton.addFinalState(object::ObjectFactory<>::construct(3));

        automaton::DFA<DefaultSymbolType, ext::set<DefaultStateType>> determinized = automaton::determinize::Determinize::determinize(automaton);

        CHECK(determinized.getStates().size() == 2);
    }

    SECTION("IDPDA")
    {
        automaton::InputDrivenNPDA<> automaton(object::ObjectFactory<>::construct(1), object::ObjectFactory<>::construct('S'));

        automaton.addInputSymbol(object::ObjectFactory<>::construct("a"));
        automaton.addInputSymbol(object::ObjectFactory<>::construct("b"));

        automaton.setPushdownStoreOperation(object::ObjectFactory<>::construct("a"), ext::vector<DefaultSymbolType>{}, ext::vector<DefaultSymbolType>{});
        automaton.setPushdownStoreOperation(object::ObjectFactory<>::construct("b"), ext::vector<DefaultSymbolType>{}, ext::vector<DefaultSymbolType>{});

        automaton.addState(object::ObjectFactory<>::construct(1));
        automaton.addState(object::ObjectFactory<>::construct(2));
        automaton.addState(object::ObjectFactory<>::construct(3));
        automaton.addTransition(object::ObjectFactory<>::construct(1), object::ObjectFactory<>::construct("a"), object::ObjectFactory<>::construct(2));
        automaton.addTransition(object::ObjectFactory<>::construct(2), object::ObjectFactory<>::construct("b"), object::ObjectFactory<>::construct(1));

        automaton.addFinalState(object::ObjectFactory<>::construct(3));

        automaton::InputDrivenDPDA<DefaultSymbolType, DefaultSymbolType, ext::set<DefaultStateType>> determinized = automaton::determinize::Determinize::determinize(automaton);

        CHECK(determinized.getStates().size() == 3);
    }

    SECTION("VPA")
    {
        automaton::VisiblyPushdownNPDA<> automaton(object::ObjectFactory<>::construct(alphabet::BottomOfTheStack{}));

        automaton.addCallInputSymbol(object::ObjectFactory<>::construct('a'));
        automaton.addReturnInputSymbol(object::ObjectFactory<>::construct('^'));

        automaton.addPushdownStoreSymbol(object::ObjectFactory<>::construct('A'));
        automaton.addPushdownStoreSymbol(object::ObjectFactory<>::construct('B'));
        automaton.addPushdownStoreSymbol(object::ObjectFactory<>::construct('C'));
        automaton.addPushdownStoreSymbol(object::ObjectFactory<>::construct('D'));
        automaton.addPushdownStoreSymbol(object::ObjectFactory<>::construct('E'));
        automaton.addPushdownStoreSymbol(object::ObjectFactory<>::construct('F'));
        automaton.addPushdownStoreSymbol(object::ObjectFactory<>::construct('T'));

        automaton.addState(object::ObjectFactory<>::construct(0));
        automaton.addState(object::ObjectFactory<>::construct(1));
        automaton.addState(object::ObjectFactory<>::construct(2));
        automaton.addState(object::ObjectFactory<>::construct(3));
        automaton.addState(object::ObjectFactory<>::construct(4));
        automaton.addState(object::ObjectFactory<>::construct(5));
        automaton.addState(object::ObjectFactory<>::construct(6));


        automaton.addCallTransition(object::ObjectFactory<>::construct(0), object::ObjectFactory<>::construct('a'), object::ObjectFactory<>::construct(0), object::ObjectFactory<>::construct('A'));
        automaton.addReturnTransition(object::ObjectFactory<>::construct(0), object::ObjectFactory<>::construct('^'), object::ObjectFactory<>::construct('A'), object::ObjectFactory<>::construct(0));

        automaton.addCallTransition(object::ObjectFactory<>::construct(0), object::ObjectFactory<>::construct('a'), object::ObjectFactory<>::construct(1), object::ObjectFactory<>::construct('B'));

        automaton.addCallTransition(object::ObjectFactory<>::construct(1), object::ObjectFactory<>::construct('a'), object::ObjectFactory<>::construct(2), object::ObjectFactory<>::construct('C'));
        automaton.addCallTransition(object::ObjectFactory<>::construct(2), object::ObjectFactory<>::construct('a'), object::ObjectFactory<>::construct(2), object::ObjectFactory<>::construct('D'));
        automaton.addReturnTransition(object::ObjectFactory<>::construct(2), object::ObjectFactory<>::construct('^'), object::ObjectFactory<>::construct('D'), object::ObjectFactory<>::construct(2));
        automaton.addReturnTransition(object::ObjectFactory<>::construct(2), object::ObjectFactory<>::construct('^'), object::ObjectFactory<>::construct('C'), object::ObjectFactory<>::construct(3));

        automaton.addCallTransition(object::ObjectFactory<>::construct(3), object::ObjectFactory<>::construct('a'), object::ObjectFactory<>::construct(4), object::ObjectFactory<>::construct('E'));
        automaton.addCallTransition(object::ObjectFactory<>::construct(4), object::ObjectFactory<>::construct('a'), object::ObjectFactory<>::construct(4), object::ObjectFactory<>::construct('F'));
        automaton.addReturnTransition(object::ObjectFactory<>::construct(4), object::ObjectFactory<>::construct('^'), object::ObjectFactory<>::construct('F'), object::ObjectFactory<>::construct(4));
        automaton.addReturnTransition(object::ObjectFactory<>::construct(4), object::ObjectFactory<>::construct('^'), object::ObjectFactory<>::construct('E'), object::ObjectFactory<>::construct(5));

        automaton.addReturnTransition(object::ObjectFactory<>::construct(5), object::ObjectFactory<>::construct('^'), object::ObjectFactory<>::construct('B'), object::ObjectFactory<>::construct(6));

        automaton.addInitialState(object::ObjectFactory<>::construct(0));
        automaton.addFinalState(object::ObjectFactory<>::construct(4));

        auto determinized = automaton::determinize::Determinize::determinize(automaton);
        CHECK(determinized.getStates().size() == 28);
        CHECK(determinized.getFinalStates().size() == 14);
        CHECK(determinized.getCallTransitions().size() == 28);
        CHECK(determinized.getReturnTransitions().size() == 196);
        CHECK(determinized.getLocalTransitions().size() == 0);
        CHECK(determinized.getCallTransitions().size() == 28);
    }

    SECTION("NFTA")
    {
        automaton::NFTA<char, int> automaton;

        const common::ranked_symbol<char> a('a', 2);
        const common::ranked_symbol<char> b('b', 1);
        const common::ranked_symbol<char> c('c', 0);
        const ext::set<common::ranked_symbol<char>> alphabet{a, b, c};
        automaton.setInputAlphabet(alphabet);

        automaton.addState(1);
        automaton.addState(2);
        automaton.addState(3);

        ext::vector<int> a1States = {1, 3};
        automaton.addTransition(a, a1States, 1);
        automaton.addTransition(a, a1States, 3);
        ext::vector<int> a2States = {3, 3};
        automaton.addTransition(a, a2States, 2);
        ext::vector<int> bStates = {2};
        automaton.addTransition(b, bStates, 1);
        ext::vector<int> cStates;
        automaton.addTransition(c, cStates, 3);

        automaton.addFinalState(3);

        automaton::DFTA<char, ext::set<int>> determinized = automaton::determinize::Determinize::determinize(automaton);

        CHECK(determinized.getStates().size() == 5);
        CHECK(determinized.getFinalStates().size() == 3);
        CHECK(determinized.getTransitions().size() == 15);
    }
}
