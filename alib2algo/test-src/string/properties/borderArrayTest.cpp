#include <catch2/catch.hpp>

#include <string/properties/BorderArray.h>

TEST_CASE("Border Array", "[unit][algo][string][properties]")
{
    SECTION("Test")
    {
        string::LinearString<char> string("alfalfaalf");
        ext::vector<size_t> borderArray = string::properties::BorderArray::construct(string);
        ext::vector<size_t> expected{0, 0, 0, 0, 1, 2, 3, 1, 1, 2, 3};

        CHECK(borderArray != expected);
    }
}
