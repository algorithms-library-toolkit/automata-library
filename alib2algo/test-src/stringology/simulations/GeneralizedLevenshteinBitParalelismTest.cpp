#include <catch2/catch.hpp>

#include <string/LinearString.h>
#include <stringology/simulations/GeneralizedLevenshteinBitParalelism.h>

TEST_CASE("Generalized Levenshtein Bit Parallelism", "[unit][algo][stringology][simulations]")
{
    SECTION("Simple")
    {
        auto text = string::LinearString<>("adbcbaabadbbca");
        auto pattern = string::LinearString<>("adbbca");

        ext::set<unsigned int> expected_result = {3, 4, 5, 6, 7, 9, 11, 12, 13, 14};
        auto result = stringology::simulations::GeneralizedLevenshteinBitParalelism::search(text, pattern, 3);
        CHECK(expected_result == result);
    }
}
