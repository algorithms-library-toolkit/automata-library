#include <catch2/catch.hpp>

#include <string/LinearString.h>
#include <stringology/simulations/LevenshteinBitParalelism.h>

TEST_CASE("Levenshtein Bit Parallelism", "[unit][algo][stringology][simulations]")
{
    SECTION("Simple")
    {
        auto text = string::LinearString<>("adcabcaabadbbca");
        auto pattern = string::LinearString<>("adbbca");

        ext::set<unsigned int> expected_result = {3, 4, 6, 7, 8, 10, 12, 13, 14, 15};
        auto result = stringology::simulations::LevenshteinBitParalelism::search(text, pattern, 3);
        CHECK(expected_result == result);
    }
}
