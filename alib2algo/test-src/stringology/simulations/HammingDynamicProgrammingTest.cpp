#include <catch2/catch.hpp>

#include <string/LinearString.h>
#include <stringology/simulations/HammingDynamicProgramming.h>

TEST_CASE("Hamming Dynamic Programming", "[unit][algo][stringology][simulations]")
{
    SECTION("Table")
    {
        auto text = string::LinearString<>("adcabcaabadbbca");
        auto pattern = string::LinearString<>("adbbca");

        ext::vector<ext::vector<unsigned int>> expected_result = {
            ext::vector<unsigned int>({0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}),
            ext::vector<unsigned int>({4, 0, 1, 1, 0, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0}),
            ext::vector<unsigned int>({4, 5, 0, 2, 2, 1, 2, 2, 1, 1, 2, 0, 2, 2, 2, 2}),
            ext::vector<unsigned int>({4, 5, 6, 1, 3, 2, 2, 3, 3, 1, 2, 3, 0, 2, 3, 3}),
            ext::vector<unsigned int>({4, 5, 6, 7, 2, 3, 3, 3, 4, 3, 2, 3, 3, 0, 3, 4}),
            ext::vector<unsigned int>({4, 5, 6, 6, 8, 3, 3, 4, 4, 5, 4, 3, 4, 4, 0, 4}),
            ext::vector<unsigned int>({4, 4, 6, 7, 6, 9, 4, 3, 4, 5, 5, 5, 4, 5, 5, 0}),
        };

        CHECK(expected_result == stringology::simulations::HammingDynamicProgramming::compute_table(text, pattern, 3));
    }

    SECTION("Occurences")
    {
        auto text = string::LinearString<>("patternpettannbalastpettern");
        auto pattern = string::LinearString<>("pattern");

        auto expected_result = ext::set<unsigned int>({7, 14, 27});
        auto result = stringology::simulations::HammingDynamicProgramming::search(text, pattern, 3);

        CHECK(expected_result == result);
    }
}
