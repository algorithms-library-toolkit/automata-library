#include <catch2/catch.hpp>

#include <string/LinearString.h>
#include <stringology/simulations/LevenshteinDynamicProgramming.h>

TEST_CASE("Levenshtein Dynamic Programming", "[unit][algo][stringology][simulations]")
{
    SECTION("Table")
    {

        auto text = string::LinearString<>("adcabcaabadbbca");
        auto pattern = string::LinearString<>("adbbca");

        ext::vector<ext::vector<unsigned int>> expected_result = {
            ext::vector<unsigned int>({0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}),
            ext::vector<unsigned int>({1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0}),
            ext::vector<unsigned int>({2, 1, 0, 1, 1, 1, 2, 1, 1, 1, 1, 0, 1, 2, 2, 1}),
            ext::vector<unsigned int>({3, 2, 1, 1, 2, 1, 2, 2, 2, 1, 2, 1, 0, 1, 2, 2}),
            ext::vector<unsigned int>({4, 3, 2, 2, 2, 2, 2, 3, 3, 2, 2, 2, 1, 0, 1, 2}),
            ext::vector<unsigned int>({5, 4, 3, 2, 3, 3, 2, 3, 4, 3, 3, 3, 2, 1, 0, 1}),
            ext::vector<unsigned int>({6, 5, 4, 3, 2, 4, 3, 2, 3, 4, 3, 4, 3, 2, 1, 0}),
        };

        CHECK(expected_result == stringology::simulations::LevenshteinDynamicProgramming::compute_table(text, pattern));
    }

    SECTION("Occurences")
    {
        auto text = string::LinearString<>("adcabcaabadbbca");
        auto pattern = string::LinearString<>("adbbca");

        ext::set<unsigned int> expected_result = {3, 4, 6, 7, 8, 10, 12, 13, 14, 15};
        auto result = stringology::simulations::LevenshteinDynamicProgramming::search(text, pattern, 3);
        CHECK(expected_result == result);
    }
}
