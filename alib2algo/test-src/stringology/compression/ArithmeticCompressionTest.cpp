#include <catch2/catch.hpp>

#include <stringology/compression/ArithmeticCompression.h>
#include <stringology/compression/ArithmeticDecompression.h>

TEST_CASE("Arithmetic Compression", "[unit][algo][stringology][compression]")
{
    SECTION("basics")
    {
        std::string rawInput("abbabbabaae123456789r0 8723 babababb  ab bapobababbbabaaabbafjfjdjlvldsuiueqwpomvdhgataewpvdihviasubababbba 5475 baaabba");
        string::LinearString<char> input(rawInput);

        ext::vector<bool> compressed = stringology::compression::AdaptiveIntegerArithmeticCompression::compress(input);
        INFO("compressed = " << ext::to_string(compressed));
        string::LinearString<char> output = stringology::compression::AdaptiveIntegerArithmeticDecompression::decompress(compressed, input.getAlphabet());

        INFO("original= " << ext::to_string(input) << " decompressed = " << ext::to_string(output));
        INFO("compressed size = " << compressed.size() << " original_size = " << input.getContent().size() * 8);
        CHECK(input == output);
    }
}
