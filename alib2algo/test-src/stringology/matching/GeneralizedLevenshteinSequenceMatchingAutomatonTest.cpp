#include <catch2/catch.hpp>

#include <automaton/FSM/NFA.h>
#include <automaton/simplify/UnreachableStatesRemover.h>
#include <string/LinearString.h>
#include <stringology/matching/GeneralizedLevenshteinSequenceMatchingAutomaton.h>

TEST_CASE("Generalized Levenshtein Sequence Matching Automaton", "[unit][algo][stringology][matching]")
{
    SECTION("Test construction")
    {
        ext::set<char> alphabet{'a', 'b', 'c', 'd'};
        string::LinearString<char> input_string(alphabet, ext::vector<char>{'a', 'b', 'c'});
        auto resulting_automata = stringology::matching::GeneralizedLevenshteinSequenceMatchingAutomaton::construct(input_string, 2);

        typedef ext::pair<unsigned int, unsigned int> State;

        automaton::EpsilonNFA<char, State> test(ext::make_pair(0, 0));
        test.setInputAlphabet(ext::set<char>{'a', 'b', 'c', 'd'});

        State q0 = ext::make_pair(0, 0);
        State q1 = ext::make_pair(1, 0);
        State q2 = ext::make_pair(2, 0);
        State q3 = ext::make_pair(3, 0);
        State q4 = ext::make_pair(1, 1);
        State q5 = ext::make_pair(2, 1);
        State q6 = ext::make_pair(3, 1);
        State q7 = ext::make_pair(2, 2);
        State q8 = ext::make_pair(3, 2);

        State r3 = ext::make_pair(5, 1);
        State r2 = ext::make_pair(5, 0);
        State r1 = ext::make_pair(4, 0);

        test.setStates(ext::set<State>{q0, q1, q2, q3, q4, q5, q6, q7, q8, r1, r2, r3});
        test.setFinalStates(ext::set<State>{q3, q6, q8});

        test.addTransition(q0, 'a', q1); // vertical transitions (exact matching automata)

        test.addTransition(q1, 'b', q2);
        test.addTransition(q4, 'b', q5);

        test.addTransition(q2, 'c', q3);
        test.addTransition(q5, 'c', q6);
        test.addTransition(q7, 'c', q8);

        test.addTransition(q0, 'a', q0); // loops in initial state
        test.addTransition(q0, 'b', q0);
        test.addTransition(q0, 'c', q0);
        test.addTransition(q0, 'd', q0);

        test.addTransition(q0, 'b', q4); // diagonal transitions reptestenting replace
        test.addTransition(q0, 'c', q4);
        test.addTransition(q0, 'd', q4);

        test.addTransition(q0, q4); // deletion

        test.addTransition(q1, 'a', q5);
        test.addTransition(q1, 'c', q5);
        test.addTransition(q1, 'd', q5);
        test.addTransition(q4, 'a', q7);
        test.addTransition(q4, 'c', q7);
        test.addTransition(q4, 'd', q7);

        test.addTransition(q1, q5); // deletion
        test.addTransition(q4, q7);

        test.addTransition(q2, 'a', q6);
        test.addTransition(q2, 'b', q6);
        test.addTransition(q2, 'd', q6);
        test.addTransition(q5, 'a', q8);
        test.addTransition(q5, 'b', q8);
        test.addTransition(q5, 'd', q8);

        test.addTransition(q2, q6); // deletion
        test.addTransition(q5, q8);

        test.addTransition(q1, 'a', q4); // insertions
        test.addTransition(q1, 'b', q4);
        test.addTransition(q1, 'c', q4);
        test.addTransition(q1, 'd', q4);

        test.addTransition(q2, 'a', q5);
        test.addTransition(q2, 'b', q5);
        test.addTransition(q2, 'c', q5);
        test.addTransition(q2, 'd', q5);

        test.addTransition(q5, 'a', q7);
        test.addTransition(q5, 'b', q7);
        test.addTransition(q5, 'c', q7);
        test.addTransition(q5, 'd', q7);

        test.addTransition(q1, 'a', q1); // loops for sequence matching
        test.addTransition(q1, 'c', q1);
        test.addTransition(q1, 'd', q1);

        test.addTransition(q2, 'a', q2);
        test.addTransition(q2, 'b', q2);
        test.addTransition(q2, 'd', q2);

        test.addTransition(q4, 'a', q4);
        test.addTransition(q4, 'c', q4);
        test.addTransition(q4, 'd', q4);

        test.addTransition(q5, 'a', q5);
        test.addTransition(q5, 'b', q5);
        test.addTransition(q5, 'd', q5);

        test.addTransition(q7, 'a', q7);
        test.addTransition(q7, 'b', q7);
        test.addTransition(q7, 'd', q7);

        test.addTransition(q0, 'b', r1); // transposition
        test.addTransition(r1, 'a', q5);

        test.addTransition(q1, 'c', r2);
        test.addTransition(r2, 'b', q6);

        test.addTransition(q4, 'c', r3);
        test.addTransition(r3, 'b', q8);

        test.addTransition(r1, 'b', r1); // loops in tranposition states
        test.addTransition(r1, 'c', r1);
        test.addTransition(r1, 'd', r1);

        test.addTransition(r2, 'a', r2);
        test.addTransition(r2, 'c', r2);
        test.addTransition(r2, 'd', r2);

        test.addTransition(r3, 'a', r3);
        test.addTransition(r3, 'c', r3);
        test.addTransition(r3, 'd', r3);

        CHECK(test == automaton::simplify::UnreachableStatesRemover::remove(resulting_automata));
    }

    SECTION("Test Wildcard construction")
    {
        ext::set<char> alphabet{'a', 'b', '@'};
        string::WildcardLinearString<char> input_string(alphabet, ext::vector<char>{'a', '@', 'b'}, '@');
        auto resulting_automata = stringology::matching::GeneralizedLevenshteinSequenceMatchingAutomaton::construct(input_string, 2);

        typedef ext::pair<unsigned int, unsigned int> State;

        automaton::EpsilonNFA<char, State> res(ext::make_pair(0, 0));
        res.setInputAlphabet(alphabet);

        State q0 = ext::make_pair(0, 0);
        State q1 = ext::make_pair(1, 0);
        State q2 = ext::make_pair(2, 0);
        State q3 = ext::make_pair(3, 0);
        State q4 = ext::make_pair(1, 1);
        State q5 = ext::make_pair(2, 1);
        State q6 = ext::make_pair(3, 1);
        State q7 = ext::make_pair(2, 2);
        State q8 = ext::make_pair(3, 2);

        State r3 = ext::make_pair(5, 1);
        State r2 = ext::make_pair(5, 0);
        State r1 = ext::make_pair(4, 0);

        res.setStates(ext::set<State>{q0, q1, q2, q3, q4, q5, q6, q7, q8, r1, r2, r3});
        res.setFinalStates(ext::set<State>{q3, q6, q8});

        res.addTransition(q0, 'a', q0); // initial loops
        res.addTransition(q0, 'b', q0);

        res.addTransition(q0, 'a', q1); // 3 simple matching automatas

        res.addTransition(q1, 'a', q2);
        res.addTransition(q1, 'b', q2);
        res.addTransition(q4, 'a', q5);
        res.addTransition(q4, 'b', q5);

        res.addTransition(q2, 'b', q3);
        res.addTransition(q5, 'b', q6);
        res.addTransition(q7, 'b', q8);

        res.addTransition(q0, 'b', q4); // error transitions for replace

        res.addTransition(q2, 'a', q6);
        res.addTransition(q5, 'a', q8);

        res.addTransition(q0, q4); // delete transition
        res.addTransition(q1, q5);
        res.addTransition(q2, q6);

        res.addTransition(q4, q7);
        res.addTransition(q5, q8);

        res.addTransition(q1, 'a', q4);
        res.addTransition(q1, 'b', q4);

        res.addTransition(q2, 'a', q5);
        res.addTransition(q2, 'b', q5);

        res.addTransition(q5, 'a', q7);
        res.addTransition(q5, 'b', q7);

        res.addTransition(q2, 'a', q2); // sequence matching loops
        res.addTransition(q5, 'a', q5);
        res.addTransition(q7, 'a', q7);

        res.addTransition(r1, 'b', r1);

        res.addTransition(q0, 'a', r1); // transpose states
        res.addTransition(q0, 'b', r1);
        res.addTransition(r1, 'a', q5);

        res.addTransition(q1, 'b', r2);
        res.addTransition(r2, 'a', q6);
        res.addTransition(r2, 'b', q6);

        res.addTransition(q4, 'b', r3);
        res.addTransition(r3, 'a', q8);
        res.addTransition(r3, 'b', q8);

        CHECK(res == automaton::simplify::UnreachableStatesRemover::remove(resulting_automata));
    }
}
