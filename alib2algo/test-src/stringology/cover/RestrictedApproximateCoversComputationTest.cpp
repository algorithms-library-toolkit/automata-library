#include <catch2/catch.hpp>

#include <alib/set>
#include <string/LinearString.h>
#include <stringology/cover/RestrictedApproximateCoversComputation.h>

TEST_CASE("Restricted Approximate Covers", "[unit][stringology][cover]")
{
    SECTION("Test 1")
    {
        string::LinearString<char> pattern("acacca");
        unsigned int k = 1;

        string::LinearString<char> a1("aca");
        string::LinearString<char> a2("cc");
        string::LinearString<char> a3("cca");
        ext::set<ext::pair<string::LinearString<char>, unsigned int>> refSet;
        refSet.insert(ext::make_pair(a1, 1));
        refSet.insert(ext::make_pair(a2, 1));
        refSet.insert(ext::make_pair(a3, 1));

        ext::set<ext::pair<string::LinearString<char>, unsigned int>> result = stringology::cover::RestrictedApproximateCoversComputation::compute(pattern, k);

        CHECK(result == refSet);
    }

    SECTION("Test 2")
    {
        string::LinearString<char> pattern("aca");
        unsigned int k = 1;

        ext::set<ext::pair<string::LinearString<char>, unsigned int>> result = stringology::cover::RestrictedApproximateCoversComputation::compute(pattern, k);

        CHECK(result.size() == 0);
    }

    SECTION("Test 3")
    {
        string::LinearString<char> pattern("abab");
        unsigned int k = 1;

        string::LinearString<char> c1("ab");
        ext::set<ext::pair<string::LinearString<char>, unsigned int>> refSet;
        refSet.insert(ext::make_pair(c1, 0));

        ext::set<ext::pair<string::LinearString<char>, unsigned int>> result = stringology::cover::RestrictedApproximateCoversComputation::compute(pattern, k);

        CHECK(result == refSet);
    }
}
