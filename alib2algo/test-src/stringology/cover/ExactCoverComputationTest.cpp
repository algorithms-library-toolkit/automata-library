#include <catch2/catch.hpp>

#include <alib/set>
#include <string/LinearString.h>
#include <stringology/cover/ExactCoversComputation.h>

TEST_CASE("Exact Covers", "[unit][stringology][cover]")
{
    SECTION("Test 1")
    {
        string::LinearString<char> pattern("atatata");

        ext::vector<char> c{'a', 't', 'a'};
        ext::vector<char> c1{'a', 't', 'a', 't', 'a'};
        string::LinearString<char> first(c);
        string::LinearString<char> second(c1);
        ext::set<string::LinearString<char>> refSet;
        refSet.insert(first);
        refSet.insert(second);

        auto covers = stringology::cover::ExactCoversComputation::compute(pattern);

        CHECK(refSet == covers);
    }

    SECTION("Test 2")
    {
        string::LinearString<char> pattern("abaabaaabaa");

        ext::vector<char> d{'a', 'b', 'a', 'a'};
        string::LinearString<char> first(d);
        ext::set<string::LinearString<char>> refSet;
        refSet.insert(first);

        auto covers = stringology::cover::ExactCoversComputation::compute(pattern);

        CHECK(refSet == covers);
    }

    SECTION("Test 3")
    {
        string::LinearString<char> pattern("abcdf");

        ext::set<string::LinearString<char>> refSet;

        auto covers = stringology::cover::ExactCoversComputation::compute(pattern);

        CHECK(refSet == covers);
    }

    SECTION("Test 4")
    {
        string::LinearString<char> pattern("aaaa");

        ext::vector<char> e{'a'};
        ext::vector<char> e1{'a', 'a'};
        ext::vector<char> e2{'a', 'a', 'a'};
        string::LinearString<char> first(e);
        string::LinearString<char> second(e1);
        string::LinearString<char> third(e2);
        ext::set<string::LinearString<char>> refSet;
        refSet.insert(first);
        refSet.insert(second);
        refSet.insert(third);

        auto covers = stringology::cover::ExactCoversComputation::compute(pattern);

        CHECK(refSet == covers);
    }

    SECTION("Test 5")
    {
        string::LinearString<char> pattern("");

        ext::set<string::LinearString<char>> refSet;

        auto covers = stringology::cover::ExactCoversComputation::compute(pattern);

        CHECK(refSet == covers);
    }
}
