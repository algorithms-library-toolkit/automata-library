#include <catch2/catch.hpp>

#include <string/LinearString.h>
#include <stringology/exact/ExactFactorMatch.h>

TEST_CASE("Exact String Match", "[unit][algo][stringology][exact]")
{
    SECTION("Basic")
    {
        string::LinearString<int> linear1(ext::vector<int>{0, 1, 2, 3});
        string::LinearString<int> linear2(ext::vector<int>{1, 2, 3});

        CHECK(stringology::exact::ExactFactorMatch::match(linear1, linear2).count(1u));
    }
}
