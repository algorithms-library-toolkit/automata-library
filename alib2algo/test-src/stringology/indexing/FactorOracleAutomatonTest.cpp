#include <catch2/catch.hpp>

#include "string/LinearString.h"
#include "stringology/indexing/ExactFactorOracleAutomaton.h"

TEST_CASE("Factor Oracle Automaton", "[unit][algo][stringology][indexing]")
{
    SECTION("Test construction")
    {
        string::LinearString<char> pattern("atatac");

        indexes::stringology::FactorOracleAutomaton<char> oracle = stringology::indexing::ExactFactorOracleAutomaton::construct(pattern);

        automaton::DFA<char, unsigned> tmp(0);

        tmp.addFinalState(0);

        tmp.setInputAlphabet(pattern.getAlphabet());

        for (int i = 1; i <= 6; ++i) {
            tmp.addState(i);
            tmp.addFinalState(i);
        }

        tmp.addTransition(0, 'a', 1);
        tmp.addTransition(0, 't', 2);
        tmp.addTransition(0, 'c', 6);
        tmp.addTransition(1, 't', 2);
        tmp.addTransition(1, 'c', 6);
        tmp.addTransition(2, 'a', 3);
        tmp.addTransition(3, 't', 4);
        tmp.addTransition(3, 'c', 6);
        tmp.addTransition(4, 'a', 5);
        tmp.addTransition(5, 'c', 6);

        indexes::stringology::FactorOracleAutomaton<char> refOracle(std::move(tmp));

        CHECK(oracle == refOracle);

        //		core::normalize < indexes::stringology::FactorOracleAutomaton < char > >::eval ( std::move ( refOracle ) );
    }
}
