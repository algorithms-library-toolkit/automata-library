#include <catch2/catch.hpp>
#include <ext/sstream>

#include <grammar/generate/GenerateUpToLength.h>

#include <grammar/ContextFree/EpsilonFreeCFG.h>
#include <primitive/string/Character.h>
#include <string/LinearString.h>
#include <string/string/LinearString.h>

#include <factory/StringDataFactory.hpp>

static unsigned countStrings(const ext::trie<char, bool>& node)
{
    unsigned res = node.getData();

    for (const auto& child : node.getChildren())
        res += countStrings(child.second);

    return res;
}

TEST_CASE("Generate Up To", "[unit][algo][grammar][generate]")
{
    SECTION("Test eps-free CFG")
    {
        {
            std::string S = std::string("S");
            std::string A = std::string("A");
            std::string B = std::string("B");
            std::string C = std::string("C");
            std::string D = std::string("D");

            char a = 'a';
            char b = 'b';

            grammar::EpsilonFreeCFG<char, std::string> grammar1(S);
            grammar1.setNonterminalAlphabet({S, A, B, C, D});
            grammar1.setTerminalAlphabet({a, b});
            grammar1.setGeneratesEpsilon(false);

            ext::set<string::LinearString<char>> strings;

            ext::trie<char, bool> generated = grammar::generate::GenerateUpToLength::generate(grammar1, 5);

            ext::ostringstream oss;
            generated.nicePrint(oss);
            CAPTURE(oss.str());

            for (const string::LinearString<char>& str : strings) {
                bool flag = true;
                ext::trie<char, bool>* node = &generated;
                for (char symbol : str.getContent()) {
                    auto iter = node->getChildren().find(symbol);
                    if (iter == node->getChildren().end()) {
                        flag = false;
                        break;
                    }
                    node = &iter->second;
                }

                CAPTURE(factory::StringDataFactory::toString(str));
                CHECK((flag && node->getData() == true));
            }

            CHECK(strings.size() == countStrings(generated));
        }
        {
            std::string S = std::string("S");
            std::string A = std::string("A");
            std::string B = std::string("B");
            std::string C = std::string("C");
            std::string D = std::string("D");

            char a = 'a';
            char b = 'b';
            char c = 'c';
            char d = 'd';

            grammar::EpsilonFreeCFG<char, std::string> grammar1(S);
            grammar1.setNonterminalAlphabet({S, A, B, C, D});
            grammar1.setTerminalAlphabet({a, b, c, d});
            grammar1.addRule(S, {A, D});
            grammar1.addRule(S, {A});
            grammar1.addRule(S, {D});
            grammar1.addRule(A, {a, A, d});
            grammar1.addRule(A, {a, d});
            grammar1.addRule(A, {B, C});
            grammar1.addRule(A, {B});
            grammar1.addRule(A, {C});
            grammar1.addRule(B, {b, B, c});
            grammar1.addRule(B, {b, c});
            grammar1.addRule(C, {c, C});
            grammar1.addRule(C, {c});
            grammar1.addRule(D, {d, D});
            grammar1.addRule(D, {d});
            grammar1.setGeneratesEpsilon(true);

            ext::set<string::LinearString<char>> strings;
            strings.insert(string::LinearString<char>(ext::vector<char>{}));
            strings.insert(string::LinearString<char>(ext::vector<char>{c}));
            strings.insert(string::LinearString<char>(ext::vector<char>{c, c}));
            strings.insert(string::LinearString<char>(ext::vector<char>{c, c, c}));
            strings.insert(string::LinearString<char>(ext::vector<char>{d}));
            strings.insert(string::LinearString<char>(ext::vector<char>{d, d}));
            strings.insert(string::LinearString<char>(ext::vector<char>{d, d, d}));
            strings.insert(string::LinearString<char>(ext::vector<char>{a, d}));
            strings.insert(string::LinearString<char>(ext::vector<char>{a, d, d}));
            strings.insert(string::LinearString<char>(ext::vector<char>{b, c}));
            strings.insert(string::LinearString<char>(ext::vector<char>{b, c, c}));
            strings.insert(string::LinearString<char>(ext::vector<char>{c, d}));
            strings.insert(string::LinearString<char>(ext::vector<char>{c, c, d}));
            strings.insert(string::LinearString<char>(ext::vector<char>{c, d, d}));
            strings.insert(string::LinearString<char>(ext::vector<char>{a, c, d}));
            strings.insert(string::LinearString<char>(ext::vector<char>{b, c, d}));

            ext::trie<char, bool> generated = grammar::generate::GenerateUpToLength::generate(grammar1, 3);

            ext::ostringstream oss;
            generated.nicePrint(oss);
            CAPTURE(oss.str());

            for (const string::LinearString<char>& str : strings) {
                bool flag = true;
                ext::trie<char, bool>* node = &generated;
                for (char symbol : str.getContent()) {
                    auto iter = node->getChildren().find(symbol);
                    if (iter == node->getChildren().end()) {
                        flag = false;
                        break;
                    }
                    node = &iter->second;
                }

                CAPTURE(factory::StringDataFactory::toString(str));
                CHECK((flag && node->getData() == true));
            }

            CHECK(strings.size() == countStrings(generated));
        }
        {
            std::string S = std::string("S");
            std::string A = std::string("A");
            std::string B = std::string("B");
            std::string C = std::string("C");
            std::string D = std::string("D");

            char a = 'a';
            char b = 'b';
            char c = 'c';
            char d = 'd';

            grammar::EpsilonFreeCFG<char, std::string> grammar1(S);
            grammar1.setNonterminalAlphabet({S, A, B, C, D});
            grammar1.setTerminalAlphabet({a, b, c, d});
            grammar1.addRule(S, {A, D});
            grammar1.addRule(S, {D});
            grammar1.addRule(A, {a, A, d});
            grammar1.addRule(A, {a, d});
            grammar1.addRule(A, {B, C});
            grammar1.addRule(A, {B});
            grammar1.addRule(A, {C});
            grammar1.addRule(B, {b, B, c});
            grammar1.addRule(B, {b, c});
            grammar1.addRule(C, {c, C});
            grammar1.addRule(C, {c});
            grammar1.addRule(D, {d, D});
            grammar1.addRule(D, {d});

            ext::set<string::LinearString<char>> strings;
            strings.insert(string::LinearString<char>(ext::vector<char>{d}));
            strings.insert(string::LinearString<char>(ext::vector<char>{d, d}));
            strings.insert(string::LinearString<char>(ext::vector<char>{d, d, d}));
            strings.insert(string::LinearString<char>(ext::vector<char>{a, d, d}));
            strings.insert(string::LinearString<char>(ext::vector<char>{c, d}));
            strings.insert(string::LinearString<char>(ext::vector<char>{c, c, d}));
            strings.insert(string::LinearString<char>(ext::vector<char>{c, d, d}));
            strings.insert(string::LinearString<char>(ext::vector<char>{b, c, d}));

            ext::trie<char, bool> generated = grammar::generate::GenerateUpToLength::generate(grammar1, 3);

            ext::ostringstream oss;
            generated.nicePrint(oss);
            CAPTURE(oss.str());

            for (const string::LinearString<char>& str : strings) {
                bool flag = true;
                ext::trie<char, bool>* node = &generated;
                for (char symbol : str.getContent()) {
                    auto iter = node->getChildren().find(symbol);
                    if (iter == node->getChildren().end()) {
                        flag = false;
                        break;
                    }
                    node = &iter->second;
                }

                CAPTURE(factory::StringDataFactory::toString(str));
                CHECK((flag && node->getData() == true));
            }

            CHECK(strings.size() == countStrings(generated));
        }
    }
}
