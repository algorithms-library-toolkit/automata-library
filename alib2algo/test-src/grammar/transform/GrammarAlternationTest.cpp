#include <alib/pair>

#include <catch2/catch.hpp>
#include <common/createUnique.hpp>

#include "grammar/ContextFree/CFG.h"
#include "grammar/transform/GrammarAlternation.h"

TEST_CASE("Grammar Alternation", "[unit][algo][grammar][transform]")
{
    SECTION("Test CFG")
    {
        {
            ext::pair<std::string, unsigned> S1 = ext::make_pair(std::string("S"), 1);
            ext::pair<std::string, unsigned> A1 = ext::make_pair(std::string("A"), 1);
            ext::pair<std::string, unsigned> S2 = ext::make_pair(std::string("S"), 2);
            ext::pair<std::string, unsigned> A2 = ext::make_pair(std::string("A"), 2);
            std::string S = std::string("S");
            std::string A = std::string("A");

            char a = 'a';
            char b = 'b';

            grammar::CFG<char, std::string> grammar1({S, A}, {a, b}, S);
            grammar::CFG<char, std::string> grammar2({S, A}, {a, b}, S);

            ext::pair<std::string, unsigned> S0 = ext::make_pair(common::createUnique(label::InitialStateLabel::instance<std::string>(), grammar1.getNonterminalAlphabet(), grammar2.getNonterminalAlphabet()), 0);
            grammar::CFG<char, ext::pair<std::string, unsigned>> grammar3({S0, S1, A1, S2, A2}, {a, b}, S0);

            grammar1.addRule(S, {});
            grammar1.addRule(S, {a, A});
            grammar1.addRule(S, {a, S});
            grammar1.addRule(A, {a, A});
            grammar1.addRule(A, {});

            grammar2.addRule(S, {});
            grammar2.addRule(S, {a, A});
            grammar2.addRule(S, {a, S});
            grammar2.addRule(A, {a, A});
            grammar2.addRule(A, {});

            grammar3.addRule(S0, {S1});
            grammar3.addRule(S0, {S2});

            grammar3.addRule(S1, {});
            grammar3.addRule(S1, {a, A1});
            grammar3.addRule(S1, {a, S1});
            grammar3.addRule(A1, {a, A1});
            grammar3.addRule(A1, {});

            grammar3.addRule(S2, {});
            grammar3.addRule(S2, {a, A2});
            grammar3.addRule(S2, {a, S2});
            grammar3.addRule(A2, {a, A2});
            grammar3.addRule(A2, {});

            CHECK(grammar3 == grammar::transform::GrammarAlternation::alternation(grammar1, grammar2));
        }
    }
}
