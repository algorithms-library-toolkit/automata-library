#include <catch2/catch.hpp>

#include "grammar/convert/ToGrammarLeftRG.h"
#include "grammar/convert/ToGrammarRightRG.h"

TEST_CASE("Left <-> Right Regular Grammar", "[unit][algo][grammar][convert]")
{
    SECTION("Right to left")
    {
        grammar::RightRG<std::string, int> rrGrammar(1);

        rrGrammar.addNonterminalSymbol(1);
        rrGrammar.addNonterminalSymbol(2);
        rrGrammar.addNonterminalSymbol(3);
        rrGrammar.addTerminalSymbol(std::string("a"));
        rrGrammar.addTerminalSymbol(std::string("b"));

        rrGrammar.addRule(1, ext::make_pair(std::string("a"), 2));
        rrGrammar.addRule(2, ext::make_pair(std::string("b"), 3));
        rrGrammar.addRule(3, std::string("a"));

        grammar::LeftRG<std::string, int> lrGrammar = grammar::convert::ToGrammarLeftRG::convert(rrGrammar);

        grammar::LeftRG<std::string, int> lrGrammarRef(4);

        lrGrammarRef.addNonterminalSymbol(1);
        lrGrammarRef.addNonterminalSymbol(2);
        lrGrammarRef.addNonterminalSymbol(3);
        lrGrammarRef.addTerminalSymbol(std::string("a"));
        lrGrammarRef.addTerminalSymbol(std::string("b"));

        lrGrammarRef.addRule(2, ext::make_pair(1, std::string("a")));
        lrGrammarRef.addRule(2, std::string("a"));
        lrGrammarRef.addRule(3, ext::make_pair(2, std::string("b")));
        lrGrammarRef.addRule(4, ext::make_pair(3, std::string("a")));

        CHECK(lrGrammarRef == lrGrammar);
    }
    SECTION("Left to right")
    {
        grammar::LeftRG<std::string, int> lrGrammar(4);

        lrGrammar.addNonterminalSymbol(2);
        lrGrammar.addNonterminalSymbol(3);
        lrGrammar.addNonterminalSymbol(4);
        lrGrammar.addTerminalSymbol(std::string("a"));
        lrGrammar.addTerminalSymbol(std::string("b"));

        lrGrammar.addRule(2, std::string("a"));
        lrGrammar.addRule(3, ext::make_pair(2, std::string("b")));
        lrGrammar.addRule(4, ext::make_pair(3, std::string("a")));

        grammar::RightRG<std::string, int> rrGrammar = grammar::convert::ToGrammarRightRG::convert(lrGrammar);

        grammar::RightRG<std::string, int> rrGrammarRef(5);

        rrGrammarRef.addNonterminalSymbol(2);
        rrGrammarRef.addNonterminalSymbol(3);
        rrGrammarRef.addNonterminalSymbol(4);
        rrGrammarRef.addTerminalSymbol(std::string("a"));
        rrGrammarRef.addTerminalSymbol(std::string("b"));

        rrGrammarRef.addRule(5, ext::make_pair(std::string("a"), 2));
        rrGrammarRef.addRule(2, ext::make_pair(std::string("b"), 3));
        rrGrammarRef.addRule(3, std::string("a"));
        rrGrammarRef.addRule(3, ext::make_pair(std::string("a"), 4));

        CHECK(rrGrammarRef == rrGrammar);
    }
}
