#include <catch2/catch.hpp>

#include <grammar/ContextFree/CFG.h>
#include <grammar/Grammar.h>
#include <grammar/Regular/LeftLG.h>

#include "grammar/properties/NonterminalUnitRuleCycle.h"
#include "grammar/properties/NullableNonterminals.h"
#include "grammar/properties/RecursiveNonterminal.h"

TEST_CASE("Grammar properties", "[unit][algo][grammar][properties]")
{
    SECTION("Test Nullable")
    {
        char X = 'X';
        char Y = 'Y';
        char Z = 'Z';
        char d = 'd';

        grammar::CFG<char, char> grammar(X);
        grammar.setTerminalAlphabet({d});
        grammar.setNonterminalAlphabet({{X, Y, Z}});
        grammar.setInitialSymbol(X);

        grammar.addRule(X, ext::vector<ext::variant<char, char>>{d});
        grammar.addRule(X, ext::vector<ext::variant<char, char>>{Y});
        grammar.addRule(Y, ext::vector<ext::variant<char, char>>{d});
        grammar.addRule(Y, ext::vector<ext::variant<char, char>>{});
        grammar.addRule(Z, ext::vector<ext::variant<char, char>>{d});
        grammar.addRule(Z, ext::vector<ext::variant<char, char>>{X, Y, Z});

        ext::set<char> res = {X, Y};
        CHECK(res == grammar::properties::NullableNonterminals::getNullableNonterminals(grammar));
    }
    SECTION("Test Unit rules")
    {
        char S = 'S';
        char A = 'A';
        char B = 'B';
        char C = 'C';
        char a = 'a';
        char b = 'b';

        grammar::LeftLG<char, char> llg(S);
        llg.setTerminalAlphabet({a, b});
        llg.setNonterminalAlphabet({S, A, B, C});
        llg.setInitialSymbol(S);

        llg.addRule(S, ext::make_pair(A, ext::vector<char>{}));
        llg.addRule(S, ext::make_pair(B, ext::vector<char>{}));
        llg.addRule(A, ext::make_pair(A, ext::vector<char>{a}));
        llg.addRule(A, ext::make_pair(B, ext::vector<char>{}));
        llg.addRule(A, ext::make_pair(C, ext::vector<char>{}));
        llg.addRule(B, ext::make_pair(B, ext::vector<char>{b, a}));
        llg.addRule(B, ext::make_pair(C, ext::vector<char>{b}));
        llg.addRule(C, ext::vector<char>{b});
        llg.addRule(C, ext::make_pair(C, ext::vector<char>{a}));

        ext::set<char> N_S = {S, A, B, C};
        ext::set<char> N_A = {A, B, C};
        ext::set<char> N_B = {B};
        ext::set<char> N_C = {C};

        CHECK(N_S == grammar::properties::NonterminalUnitRuleCycle::getNonterminalUnitRuleCycle(llg, S));
        CHECK(N_A == grammar::properties::NonterminalUnitRuleCycle::getNonterminalUnitRuleCycle(llg, A));
        CHECK(N_B == grammar::properties::NonterminalUnitRuleCycle::getNonterminalUnitRuleCycle(llg, B));
        CHECK(N_C == grammar::properties::NonterminalUnitRuleCycle::getNonterminalUnitRuleCycle(llg, C));
    }

    SECTION("Test recursive nonterminals")
    {
        {
            char A = 'A';
            char B = 'B';
            char C = 'C';
            char D = 'D';

            char a = 'a';
            char b = 'b';

            grammar::CFG<char, char> cfg(A);
            cfg.setTerminalAlphabet({a, b});
            cfg.setNonterminalAlphabet({A, B, C, D});
            cfg.setInitialSymbol(A);

            cfg.addRule(A, {B});
            cfg.addRule(B, {C});
            cfg.addRule(C, {D});
            cfg.addRule(D, {A});
            cfg.addRule(D, {});

            CHECK(grammar::properties::RecursiveNonterminal::isNonterminalRecursive(cfg, A));
            CHECK(grammar::properties::RecursiveNonterminal::isNonterminalRecursive(cfg, B));
            CHECK(grammar::properties::RecursiveNonterminal::isNonterminalRecursive(cfg, C));
            CHECK(grammar::properties::RecursiveNonterminal::isNonterminalRecursive(cfg, D));
        }
        {
            char A = 'A';
            char B = 'B';
            char C = 'C';
            char D = 'D';

            char a = 'a';
            char b = 'b';

            grammar::CFG<char, char> cfg(A);
            cfg.setTerminalAlphabet({a, b});
            cfg.setNonterminalAlphabet({A, B, C, D});
            cfg.setInitialSymbol(A);

            cfg.addRule(A, {B});
            cfg.addRule(B, {C});
            cfg.addRule(C, {D});
            cfg.addRule(D, {B});
            cfg.addRule(D, {});

            CHECK(!grammar::properties::RecursiveNonterminal::isNonterminalRecursive(cfg, A));
            CHECK(grammar::properties::RecursiveNonterminal::isNonterminalRecursive(cfg, B));
            CHECK(grammar::properties::RecursiveNonterminal::isNonterminalRecursive(cfg, C));
            CHECK(grammar::properties::RecursiveNonterminal::isNonterminalRecursive(cfg, D));
        }
        {
            char A = 'A';
            char B = 'B';
            char C = 'C';
            char D = 'D';

            char a = 'a';
            char b = 'b';

            grammar::CFG<char, char> cfg(A);
            cfg.setTerminalAlphabet({a, b});
            cfg.setNonterminalAlphabet({A, B, C, D});
            cfg.setInitialSymbol(A);

            cfg.addRule(A, {B});
            cfg.addRule(B, {C, D, A});
            cfg.addRule(C, {a});
            cfg.addRule(C, {});
            cfg.addRule(D, {b});
            cfg.addRule(D, {});

            CHECK(grammar::properties::RecursiveNonterminal::isNonterminalRecursive(cfg, A));
            CHECK(grammar::properties::RecursiveNonterminal::isNonterminalRecursive(cfg, B));
            CHECK(!grammar::properties::RecursiveNonterminal::isNonterminalRecursive(cfg, C));
            CHECK(!grammar::properties::RecursiveNonterminal::isNonterminalRecursive(cfg, D));
        }
    }
}
