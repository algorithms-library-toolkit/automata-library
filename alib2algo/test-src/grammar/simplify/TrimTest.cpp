#include <catch2/catch.hpp>

#include "grammar/Regular/RightRG.h"
#include "grammar/simplify/Trim.h"

TEST_CASE("Trim Grammar", "[unit][algo][grammar][simplify]")
{
    SECTION("Test CFG")
    {

        grammar::RightRG<std::string, int> rrGrammar(1);

        rrGrammar.addNonterminalSymbol(1);
        rrGrammar.addNonterminalSymbol(2);
        rrGrammar.addNonterminalSymbol(3);
        rrGrammar.addNonterminalSymbol(4);
        rrGrammar.addNonterminalSymbol(5);
        rrGrammar.addNonterminalSymbol(6);
        rrGrammar.addTerminalSymbol(std::string("a"));
        rrGrammar.addTerminalSymbol(std::string("b"));

        rrGrammar.addRule(1, ext::make_pair(std::string("a"), 2));
        rrGrammar.addRule(2, ext::make_pair(std::string("b"), 3));
        rrGrammar.addRule(3, std::string("a"));

        rrGrammar.addRule(4, ext::make_pair(std::string("b"), 5));
        rrGrammar.addRule(5, std::string("a"));
        rrGrammar.addRule(5, ext::make_pair(std::string("b"), 2));
        rrGrammar.addRule(6, ext::make_pair(std::string("b"), 6));

        grammar::RightRG<std::string, int> trimed = grammar::simplify::Trim::trim(rrGrammar);

        CHECK(trimed.getNonterminalAlphabet().size() == 3);
    }
}
