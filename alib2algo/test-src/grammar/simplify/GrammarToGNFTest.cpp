#include <catch2/catch.hpp>

#include "grammar/generate/GenerateUpToLength.h"
#include "grammar/simplify/ToGNF.h"

#include "grammar/ContextFree/EpsilonFreeCFG.h"
#include "grammar/ContextFree/GNF.h"
#include "grammar/string/ContextFree/GNF.h"

#include <common/createUnique.hpp>
#include <factory/StringDataFactory.hpp>

#include "container/string/ObjectsVariant.h"

TEST_CASE("Test to GNF", "[unit][algo][grammar][simplify]")
{
    SECTION("Test CFG")
    {
        {
            std::string S = std::string("S");
            std::string A = std::string("A");
            std::string B = std::string("B");
            std::string C = std::string("C");
            std::string D = std::string("D");

            std::string a = std::string("a");
            std::string b = std::string("b");

            grammar::EpsilonFreeCFG<std::string, std::string> grammar1(S);
            grammar1.setNonterminalAlphabet({S, A, B, C, D});
            grammar1.setTerminalAlphabet({a, b});

            std::string aprimed = common::createUnique(a, grammar1.getTerminalAlphabet(), grammar1.getNonterminalAlphabet());
            std::string bprimed = common::createUnique(b, grammar1.getTerminalAlphabet(), grammar1.getNonterminalAlphabet());

            grammar::GNF<std::string, ext::variant<std::string, std::string>> grammar2 = grammar::simplify::ToGNF::convert(grammar1);

            grammar::GNF<std::string, ext::variant<std::string, std::string>> grammar3(S);
            grammar3.setNonterminalAlphabet({S, A, B, C, D, aprimed, bprimed});
            grammar3.setTerminalAlphabet({a, b});
            grammar3.addRule(aprimed, ext::make_pair(a, ext::vector<ext::variant<std::string, std::string>>{}));
            grammar3.addRule(bprimed, ext::make_pair(b, ext::vector<ext::variant<std::string, std::string>>{}));

            CAPTURE(grammar2, grammar3);
            CHECK(grammar2 == grammar3);
        }
        {
            std::string S = std::string("S");
            std::string A = std::string("A");
            std::string B = std::string("B");
            std::string C = std::string("C");
            std::string D = std::string("D");

            std::string a = std::string("a");
            std::string b = std::string("b");
            std::string c = std::string("c");

            grammar::EpsilonFreeCFG<std::string, std::string> grammar1(S);
            grammar1.setNonterminalAlphabet({S, A, B, C, D});
            grammar1.setTerminalAlphabet({a, b, c});
            grammar1.addRule(S, {A});
            grammar1.addRule(A, {A, a});
            grammar1.addRule(A, {A, b});
            grammar1.addRule(A, {c});

            std::string Aprimed = common::createUnique(A, grammar1.getTerminalAlphabet(), grammar1.getNonterminalAlphabet());
            std::string aprimed = common::createUnique(a, grammar1.getTerminalAlphabet(), grammar1.getNonterminalAlphabet());
            std::string bprimed = common::createUnique(b, grammar1.getTerminalAlphabet(), grammar1.getNonterminalAlphabet());
            std::string cprimed = common::createUnique(c, grammar1.getTerminalAlphabet(), grammar1.getNonterminalAlphabet());

            grammar::GNF<std::string, ext::variant<std::string, std::string>> grammar2 = grammar::simplify::ToGNF::convert(grammar1);

            grammar::GNF<std::string, ext::variant<std::string, std::string>> grammar3(S);
            grammar3.setNonterminalAlphabet({S, A, Aprimed, B, C, D, aprimed, bprimed, cprimed});
            grammar3.setTerminalAlphabet({a, b, c});
            grammar3.addRule(S, ext::make_pair(c, ext::vector<ext::variant<std::string, std::string>>{Aprimed}));
            grammar3.addRule(S, ext::make_pair(c, ext::vector<ext::variant<std::string, std::string>>{}));
            grammar3.addRule(A, ext::make_pair(c, ext::vector<ext::variant<std::string, std::string>>{Aprimed}));
            grammar3.addRule(A, ext::make_pair(c, ext::vector<ext::variant<std::string, std::string>>{}));
            grammar3.addRule(Aprimed, ext::make_pair(a, ext::vector<ext::variant<std::string, std::string>>{Aprimed}));
            grammar3.addRule(Aprimed, ext::make_pair(a, ext::vector<ext::variant<std::string, std::string>>{}));
            grammar3.addRule(Aprimed, ext::make_pair(b, ext::vector<ext::variant<std::string, std::string>>{Aprimed}));
            grammar3.addRule(Aprimed, ext::make_pair(b, ext::vector<ext::variant<std::string, std::string>>{}));
            grammar3.addRule(aprimed, ext::make_pair(a, ext::vector<ext::variant<std::string, std::string>>{}));
            grammar3.addRule(bprimed, ext::make_pair(b, ext::vector<ext::variant<std::string, std::string>>{}));
            grammar3.addRule(cprimed, ext::make_pair(c, ext::vector<ext::variant<std::string, std::string>>{}));

            CAPTURE(grammar1, grammar2, grammar3);

            CHECK(grammar2 == grammar3);
        }
        {
            std::string S = std::string("S");
            std::string A = std::string("A");
            std::string B = std::string("B");
            std::string C = std::string("C");

            std::string a = std::string("a");
            std::string b = std::string("b");
            std::string c = std::string("c");

            grammar::EpsilonFreeCFG<std::string, std::string> grammar1(S);
            grammar1.setNonterminalAlphabet({S, A, B, C});
            grammar1.setTerminalAlphabet({a, b, c});
            grammar1.addRule(S, {A});
            grammar1.addRule(A, {B, a});
            grammar1.addRule(B, {A, b});
            grammar1.addRule(A, {c});

            std::string Bprimed = common::createUnique(B, grammar1.getTerminalAlphabet(), grammar1.getNonterminalAlphabet());
            std::string aprimed = common::createUnique(a, grammar1.getTerminalAlphabet(), grammar1.getNonterminalAlphabet());
            std::string bprimed = common::createUnique(b, grammar1.getTerminalAlphabet(), grammar1.getNonterminalAlphabet());
            std::string cprimed = common::createUnique(c, grammar1.getTerminalAlphabet(), grammar1.getNonterminalAlphabet());

            grammar::GNF<std::string, ext::variant<std::string, std::string>> grammar2 = grammar::simplify::ToGNF::convert(grammar1);

            grammar::GNF<std::string, ext::variant<std::string, std::string>> grammar3(S);
            grammar3.setNonterminalAlphabet({S, A, B, Bprimed, C, aprimed, bprimed, cprimed});
            grammar3.setTerminalAlphabet({a, b, c});
            grammar3.addRule(S, ext::make_pair(c, ext::vector<ext::variant<std::string, std::string>>{}));
            grammar3.addRule(S, ext::make_pair(c, ext::vector<ext::variant<std::string, std::string>>{bprimed, Bprimed, aprimed}));
            grammar3.addRule(S, ext::make_pair(c, ext::vector<ext::variant<std::string, std::string>>{bprimed, aprimed}));
            grammar3.addRule(A, ext::make_pair(c, ext::vector<ext::variant<std::string, std::string>>{bprimed, aprimed}));
            grammar3.addRule(A, ext::make_pair(c, ext::vector<ext::variant<std::string, std::string>>{bprimed, Bprimed, aprimed}));
            grammar3.addRule(A, ext::make_pair(c, ext::vector<ext::variant<std::string, std::string>>{}));
            grammar3.addRule(B, ext::make_pair(c, ext::vector<ext::variant<std::string, std::string>>{bprimed}));
            grammar3.addRule(B, ext::make_pair(c, ext::vector<ext::variant<std::string, std::string>>{bprimed, Bprimed}));
            grammar3.addRule(Bprimed, ext::make_pair(a, ext::vector<ext::variant<std::string, std::string>>{bprimed, Bprimed}));
            grammar3.addRule(Bprimed, ext::make_pair(a, ext::vector<ext::variant<std::string, std::string>>{bprimed}));
            grammar3.addRule(aprimed, ext::make_pair(a, ext::vector<ext::variant<std::string, std::string>>{}));
            grammar3.addRule(bprimed, ext::make_pair(b, ext::vector<ext::variant<std::string, std::string>>{}));
            grammar3.addRule(cprimed, ext::make_pair(c, ext::vector<ext::variant<std::string, std::string>>{}));

            CAPTURE(grammar1, grammar2, grammar3);
            CHECK(grammar2 == grammar3);
        }
        {
            std::string A = std::string("A");
            std::string B = std::string("B");
            std::string C = std::string("C");

            std::string a = std::string("a");
            std::string b = std::string("b");

            grammar::EpsilonFreeCFG<std::string, std::string> grammar1(A);
            grammar1.setNonterminalAlphabet({A, B, C});
            grammar1.setTerminalAlphabet({a, b});
            grammar1.addRule(A, {B, C});
            grammar1.addRule(A, {a});
            grammar1.addRule(B, {C, A});
            grammar1.addRule(B, {A, b});
            grammar1.addRule(C, {A, B});
            grammar1.addRule(C, {C, C});
            grammar1.addRule(C, {a});

            grammar::GNF<std::string, ext::variant<std::string, std::string>> grammar2 = grammar::simplify::ToGNF::convert(grammar1);

            CHECK(grammar::generate::GenerateUpToLength::generate(grammar1, 7) == grammar::generate::GenerateUpToLength::generate(grammar2, 7));
        }
    }
}
