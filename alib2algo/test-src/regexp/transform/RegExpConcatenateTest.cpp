#include <catch2/catch.hpp>

#include "regexp/transform/RegExpConcatenate.h"

#include <regexp/RegExp.h>
#include <regexp/formal/FormalRegExpElements.h>

#include <factory/StringDataFactory.hpp>
#include <regexp/string/UnboundedRegExp.h>

TEST_CASE("RegExp Concatenate", "[unit][algo][regexp][transform]")
{
    SECTION("Test Concat")
    {
        auto testcase = GENERATE(
            std::make_tuple("(#E a b)", "(#E a c)", "(#E a b)(#E a c)"),
            std::make_tuple("(#E a b)", "(#0 #E )", "(#E a b)(#0 #E )"),
            std::make_tuple("(#0 #E )", "(#E a b)", "(#0 #E )(#E a b)"));

        regexp::UnboundedRegExp<> re1 = factory::StringDataFactory::fromString(std::get<0>(testcase));
        regexp::UnboundedRegExp<> re2 = factory::StringDataFactory::fromString(std::get<1>(testcase));
        regexp::UnboundedRegExp<> exp = factory::StringDataFactory::fromString(std::get<2>(testcase));
        regexp::FormalRegExp<> re = regexp::transform::RegExpConcatenate::concatenate(regexp::FormalRegExp<>(re1), regexp::FormalRegExp<>(re2));

        CHECK(re == regexp::FormalRegExp<>(exp));
    }
}
