#include <catch2/catch.hpp>

#include "regexp/transform/RegExpIntegral.h"

#include <regexp/string/UnboundedRegExp.h>
#include <string/string/LinearString.h>

#include <factory/StringDataFactory.hpp>

TEST_CASE("Regexp Integral", "[unit][algo][grammar][transform]")
{
    SECTION("Integrals")
    {
        // https://edux.fit.cvut.cz/oppa/BI-AAG/cviceni/aag-cv04.pdf
        // Exc. 16
        auto testcase = GENERATE(std::make_tuple("0 1 0 + 1 0 1", "1", "1 0 1 0+1 1 0 1"));

        const std::string& regexp_str = std::get<0>(testcase);
        const std::string& string_str = std::get<1>(testcase);
        const std::string& result = std::get<2>(testcase);

        regexp::UnboundedRegExp<> regexp = factory::StringDataFactory::fromString(regexp_str);
        string::LinearString<> string = factory::StringDataFactory::fromString("\"" + string_str + "\"");

        CHECK(factory::StringDataFactory::toString(regexp::transform::RegExpIntegral::integral(regexp, string)) == result);
    }
}
