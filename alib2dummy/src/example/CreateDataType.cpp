#include <ext/iostream>
#include <registration/AlgoRegistration.hpp>
#include "CreateDataType.h"
#include "DataType.h"

namespace example {

DataType CreateDataType::create()
{
    return DataType(0);
}

DataType CreateDataType2::create(int a)
{
    return DataType(a);
}

} /* namespace example */

namespace {

/* registration of an algorithm inside ALT. This is used e.g. in AQL. */
auto create1 = registration::AbstractRegister<
    example::CreateDataType, // namespace-qualified name of the algorithm, corresponds to the algorithm class
    example::DataType // return value followed by 0 or more parameters
    >(example::CreateDataType::create); // algorithm entry point, corresponding static function to call

auto create2 = registration::AbstractRegister<example::CreateDataType2, example::DataType, int /* parameters */>(example::CreateDataType2::create);
auto create3 = registration::AbstractRegister<example::CreateDataType2, example::DataType, double>(example::CreateDataType2::create, "val").setDocumentation("Creates an object of DataType class with specified value\n\n@param val value passed to DataType\n@return DataType object with its value equal to val");

}
