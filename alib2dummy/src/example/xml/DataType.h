#pragma once

#include <core/xmlApi.hpp>
#include <example/DataType.h>

namespace core {

template <>
struct xmlApi<example::DataType> {
    static example::DataType parse(ext::deque<sax::Token>::iterator& input);
    static bool first(const ext::deque<sax::Token>::const_iterator& input);
    static std::string xmlTagName();
    static void compose(ext::deque<sax::Token>& output, const example::DataType& data);
};

} /* namespace core */
