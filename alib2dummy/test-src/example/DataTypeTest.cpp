#include <catch2/catch.hpp>
#include <factory/XmlDataFactory.hpp>
#include <sstream>
#include "example/DataType.h"
#include "example/xml/DataType.h"


TEST_CASE("DataType test", "[unit][dummy][example]")
{
    SECTION("int constructor")
    {
        example::DataType d(5);

        SECTION("holds correct value")
        {
            CHECK(d.getA() == 5);
        }

        SECTION("ostream operator")
        {
            std::ostringstream oss;
            oss << d;

            CHECK(oss.str() == "(DataType 5)");
        }
    }

    SECTION("xml")
    {
        SECTION("xml writer")
        {
            example::DataType d(5);
            CHECK(factory::XmlDataFactory::toString(d) == "<?xml version=\"1.0\"?>\n<DataType><Integer>5</Integer></DataType>\n");
        }

        SECTION("xml parser")
        {
            example::DataType d = factory::XmlDataFactory::fromString("<DataType><Integer>3</Integer></DataType>");
            CHECK(d.getA() == 3);
        }
    }
}
