project(alt-aql VERSION ${CMAKE_PROJECT_VERSION})

find_package(LibXml2 REQUIRED)
find_package(replxx 0.0.4 REQUIRED)
find_package(PkgConfig)

pkg_check_modules(TCLAP REQUIRED IMPORTED_TARGET tclap>=1.2.5)

alt_executable(aql2
	DEPENDS alib2elgo alib2graph_algo alib2algo alib2aux alib2raw_cli_integration alib2raw alib2str_cli_integration alib2str alib2graph_data alib2data alib2cli alib2xml alib2common alib2abstraction alib2measure alib2std LibXml2::LibXml2 PkgConfig::TCLAP replxx
)

# stdlib
set(STDLIB_DIRECTORY "${CMAKE_INSTALL_FULL_DATADIR}/algorithms-library/stdlib")
configure_file(
	${CMAKE_CURRENT_SOURCE_DIR}/src/aql.h.in
	${CMAKE_CURRENT_BINARY_DIR}/aql.h
	)
set(CMAKE_INCLUDE_CURRENT_DIR ON)
install(DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/src/stdlib"
	DESTINATION "${CMAKE_INSTALL_DATADIR}/algorithms-library"
	FILES_MATCHING PATTERN "*.aql"
	)
