#include "REPL.h"

#include <cstring>
#include <functional>
#include <regex>

using namespace replxx;

namespace replxx::helper {

int utf8str_codepoint_len(char const* s, int utf8len)
{
    int codepointLen = 0;
    unsigned char m4 = 128 + 64 + 32 + 16;
    unsigned char m3 = 128 + 64 + 32;
    unsigned char m2 = 128 + 64;
    for (int i = 0; i < utf8len; ++i, ++codepointLen) {
        char c = s[i];
        if ((c & m4) == m4) {
            i += 3;
        } else if ((c & m3) == m3) {
            i += 2;
        } else if ((c & m2) == m2) {
            i += 1;
        }
    }
    return (codepointLen);
}

int context_len(char const* prefix)
{
    auto wb = " \t\n\r\v\f-=+*&^%@!,./?<>;`~'\"[]{}()\\|";
    int i = static_cast<int>(strlen(prefix) - 1);
    int cl = 0;
    while (i >= 0) {
        if (strchr(wb, prefix[i]) != NULL) {
            break;
        }
        ++cl;
        --i;
    }
    return (cl);
}

inline bool is_kw(char ch)
{
    return isalnum(ch) || (ch == '_');
}

void hook_color(std::string const& context, replxx::Replxx::colors_t& colors, syntax_highlight_t const& regex_color, keyword_highlight_t const& word_color)
{

    bool inWord(false);
    int wordStart(0);
    int wordEnd(0);
    int colorOffset(0);
    auto dohl = [&](int i) {
        inWord = false;
        std::string intermission(context.substr(wordEnd, wordStart - wordEnd));
        colorOffset += utf8str_codepoint_len(intermission.c_str(), intermission.length());
        int wordLen(i - wordStart);
        std::string keyword(context.substr(wordStart, wordLen));
        bool bold(false);
        if (keyword.substr(0, 5) == "bold_") {
            keyword = keyword.substr(5);
            bold = true;
        }
        bool underline(false);
        if (keyword.substr(0, 10) == "underline_") {
            keyword = keyword.substr(10);
            underline = true;
        }
        keyword_highlight_t::const_iterator it(word_color.find(keyword));
        Replxx::Color color = Replxx::Color::DEFAULT;
        if (it != word_color.end()) {
            color = it->second;
        }
        if (bold) {
            color = replxx::color::bold(color);
        }
        if (underline) {
            color = replxx::color::underline(color);
        }
        for (int k(0); k < wordLen; ++k) {
            Replxx::Color& c(colors.at(colorOffset + k));
            if (color != Replxx::Color::DEFAULT) {
                c = color;
            }
        }
        colorOffset += wordLen;
        wordEnd = i;
    };
    for (int i(0); i < static_cast<int>(context.length()); ++i) {
        if (!inWord) {
            if (is_kw(context[i])) {
                inWord = true;
                wordStart = i;
            }
        } else if (inWord && !is_kw(context[i])) {
            dohl(i);
        }
        if ((context[i] != '_') && ispunct(context[i])) {
            wordStart = i;
            dohl(i + 1);
        }
    }
    if (inWord) {
        dohl(context.length());
    }

    // highlight matching regex sequences
    for (auto const& e : regex_color) {
        size_t pos{0};
        std::string str = context;
        std::smatch match;

        while (std::regex_search(str, match, std::regex(e.first))) {
            std::string c{match[0]};
            std::string prefix(match.prefix().str());
            pos += utf8str_codepoint_len(prefix.c_str(), static_cast<int>(prefix.length()));
            int len(utf8str_codepoint_len(c.c_str(), static_cast<int>(c.length())));

            for (int i = 0; i < len; ++i) {
                colors.at(pos + i) = e.second;
            }

            pos += len;
            str = match.suffix();
        }
    }
}
}
