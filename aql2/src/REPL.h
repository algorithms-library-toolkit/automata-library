#pragma once

#include <optional>
#include <string>
#include <vector>

#include <environment/Environment.h>
#include <replxx.hxx>

class REPL {
    std::unordered_map<std::string, replxx::Replxx::Color> m_tokens;

public:
    explicit REPL();

    cli::CommandResult run(cli::Environment& environment);

    static inline std::optional<std::string> historyFile;
};

namespace replxx::helper {
int utf8str_codepoint_len(char const* s, int utf8len);
int context_len(char const* prefix);

typedef std::vector<std::pair<std::string, Replxx::Color>> syntax_highlight_t;
typedef std::unordered_map<std::string, Replxx::Color> keyword_highlight_t;


void hook_color(std::string const& context, replxx::Replxx::colors_t& colors, syntax_highlight_t const& regex_color, keyword_highlight_t const& word_color);
}