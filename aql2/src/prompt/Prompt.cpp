#include <global/GlobalData.h>
#include "Prompt.h"
#include "REPL.h"

Prompt::Prompt(cli::Environment environment)
    : m_environment(std::move(environment))
{
}

cli::CommandResult Prompt::run()
{
    cli::CommandResult res = cli::CommandResult::OK;

    while (!m_interfaces.empty()) {
        const cli::CommandResult state = m_interfaces.front()->execute(getEnvironment());

        if (state == cli::CommandResult::EXCEPTION) {
            if (common::GlobalData::exitOnError)
                return state;
            else
                continue;
        }

        if (state == cli::CommandResult::QUIT)
            return state;
        if (state == cli::CommandResult::RETURN)
            res = cli::CommandResult::RETURN;

        m_interfaces.pop_front();
    }

    return res;
}
