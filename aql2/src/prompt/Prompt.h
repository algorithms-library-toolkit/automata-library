#pragma once

#include <deque>

#include <environment/Environment.h>
#include "src/prompt/interface/Interface.h"

class Prompt {
    std::deque<std::shared_ptr<Interface>> m_interfaces;

    cli::Environment m_environment;

    Prompt(cli::Environment environment);

public:
    Prompt(const Prompt&) = delete;

    Prompt(Prompt&&) = delete;

    Prompt& operator=(const Prompt&) = delete;

    Prompt& operator=(Prompt&&) = delete;

    static Prompt& getPrompt()
    {
        static Prompt instance{cli::Environment()};
        return instance;
    }

    void appendCharSequence(std::shared_ptr<Interface>&& interface)
    {
        m_interfaces.push_back(std::move(interface));
    }

    void prependCharSequence(std::shared_ptr<Interface>&& interface)
    {
        m_interfaces.push_front(std::move(interface));
    }

    cli::CommandResult run();

    cli::Environment& getEnvironment()
    {
        return m_environment;
    }
};
