#pragma once

#include <environment/Environment.h>
#include <fstream>
#include <sstream>
#include "REPL.h"

struct Interface {
    virtual cli::CommandResult execute(cli::Environment& environment) = 0;
    virtual ~Interface() = default;
};

class StreamInterface : public Interface {
    virtual std::istream& getStream() = 0;

    cli::CommandResult execute(cli::Environment& environment) override
    {
        return environment.execute(getStream());
    }
};

struct StringInterface : public StreamInterface {
private:
    std::istringstream m_stream;

public:
    explicit StringInterface(const std::string& content)
        : m_stream(content)
    {
    }

    std::istream& getStream() override
    {
        return m_stream;
    }
};

struct FileInterface : public StreamInterface {
private:
    std::ifstream m_stream;

public:
    explicit FileInterface(std::ifstream&& path)
        : m_stream(std::move(path))
    {
    }

    std::istream& getStream() override
    {
        return m_stream;
    }
};

struct CinInterface : public StreamInterface {
private:
    std::istream& getStream() override
    {
        return std::cin;
    }
};

struct ReplInterface : public Interface {
    cli::CommandResult execute(cli::Environment& environment) override
    {
        return REPL().run(environment);
    }
};