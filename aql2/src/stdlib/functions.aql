/** Converts arbitrary finite automaton to minimal automaton.
 */
function automaton::simplify::ToMinimalDFA(auto $automaton) returning auto begin
    return batch automaton::simplify::EpsilonRemoverIncoming $automaton
    | automaton::determinize::Determinize -
    | automaton::simplify::Trim - | automaton::simplify::Minimize - | automaton::simplify::Normalize - ;
end

/** Converts regular expression to minimal automaton
*/
function regexp::convert::ToMinimalDFA(auto $re) returning auto begin
    return batch regexp::convert::ToAutomaton $re
    | automaton::simplify::ToMinimalDFA - | automaton::simplify::Rename - ;
end

/** Converts regular grammar to minimal automaton
*/
function grammar::convert::ToMinimalDFA(auto $g) returning auto begin
    return batch grammar::convert::ToAutomaton $g
    | automaton::simplify::ToMinimalDFA - ;
end

/** Checks if two arbitrary finite automatons are equal.
*/
function automaton::properties::Equal(auto $a1, auto $a2) returning bool begin
    return batch compare::AutomatonCompare
    < (automaton::simplify::ToMinimalDFA $a1) < (automaton::simplify::ToMinimalDFA $a2);
end

/** Checks if two regular expressions generate the same language.
*/
function regexp::properties::Equal(auto $re1, auto $re2) returning bool begin
    return batch automaton::properties::Equal
    < (regexp::convert::ToMinimalDFA $re1) < (regexp::convert::ToMinimalDFA $re2);
end

/** Checks if two regular grammars generate the same language.
*/
function grammar::properties::Equal(auto $g1, auto $g2) returning bool begin
    return batch automaton::properties::Equal
    < (grammar::convert::ToMinimalDFA $g1) < (grammar::convert::ToMinimalDFA $g2);
end
