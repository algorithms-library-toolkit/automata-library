#include <AltCliLexer.h>
#include <functional>
#include <grammar/Autocomplete.h>
#include <parser/Parser.h>
#include <replxx.hxx>
#include "REPL.h"

using namespace replxx;
using namespace replxx::helper;

cli::CommandResult REPL::run(cli::Environment& env)
{
    using cl = Replxx::Color;
    syntax_highlight_t regex_color{
        // numbers
        {"[\\-|+]{0,1}[0-9]+", cl::BLUE}, // integers
        {"[\\-|+]{0,1}[0-9]*\\.[0-9]+", cl::BLUE}, // decimals

        // strings
        {"\"(.|\n)*?\"", cl::YELLOW}, // double quotes
    };

    Replxx rx;
    if (historyFile.has_value())
        rx.history_load(historyFile.value());

    // set the max history size
    rx.set_max_history_size(128);
    // set the max number of hint rows to show
    rx.set_max_hint_rows(3);
    rx.set_word_break_characters(" \n\t.,-%!;=*~^':\"/?<>|[](){}");
    rx.set_completion_count_cutoff(128);
    rx.set_double_tab_completion(false);
    rx.set_complete_on_empty(true);
    rx.set_beep_on_ambiguous_completion(false);
    rx.set_no_color(false);
    rx.set_indent_multiline(true);

    rx.set_highlighter_callback([regex_color, this](std::string const& context, replxx::Replxx::colors_t& colors) {
        return hook_color(context, colors, regex_color, this->m_tokens);
    });
    std::vector<std::string> lines;

    // TODO: We do not support Shift+Enter. But it is possible to use Ctrl+J instead.
    rx.bind_key(Replxx::KEY::ENTER, [&](char32_t) {
        std::string input{rx.get_state().text()};
        if (input.empty()) {
            return rx.invoke(replxx::Replxx::ACTION::COMMIT_LINE, Replxx::KEY::ENTER);
        }
        try {
            cli::Parser::parseString(input);
        } catch (const cli::Parser::ContinueException&) {
            return rx.invoke(Replxx::ACTION::NEW_LINE, Replxx::KEY::ENTER);
        } catch (const std::exception& e) {
            return rx.invoke(Replxx::ACTION::COMMIT_LINE, Replxx::KEY::ENTER);
        }

        return rx.invoke(replxx::Replxx::ACTION::COMMIT_LINE, Replxx::KEY::ENTER);
    });

    cli::Autocomplete complete(env);

    rx.set_completion_callback([&complete](const std::string& context, int& contextLen) {
        Replxx::completions_t completions;
        int utf8ContextLen(context_len(context.c_str()));
        int prefixLen(static_cast<int>(context.length()) - utf8ContextLen);
        if ((prefixLen > 0) && (context[prefixLen - 1] == '\\')) {
            --prefixLen;
            ++utf8ContextLen;
        }

        contextLen = utf8str_codepoint_len(context.c_str() + prefixLen, utf8ContextLen);

        const std::string prefix{context.substr(prefixLen)};

        const auto& suggestions = complete.getSuggestions(context, prefix);

        for (const auto& [kind, suggestion] : suggestions) {
            replxx::Replxx::Color color;
            switch (kind) {
            case cli::Autocomplete::SuggestionKind::Keyword:
                color = Replxx::Color::BRIGHTBLUE;
                break;
            case cli::Autocomplete::SuggestionKind::Variable:
                color = Replxx::Color::BRIGHTGREEN;
                break;
            case cli::Autocomplete::SuggestionKind::Identifier:
                color = Replxx::Color::GREEN;
                break;
            default:
                color = Replxx::Color::BRIGHTCYAN;
                break;
            }

            completions.emplace_back(suggestion, color);
        }

        return completions;
    });

    std::string prompt = "aql> ";

    cli::CommandResult state = cli::CommandResult::OK;
    // display the prompt and retrieve input from the user
    char const* cInput;
    while (state == cli::CommandResult::OK || state == cli::CommandResult::ERROR) {
        do {
            cInput = rx.input(prompt);
        } while ((cInput == nullptr) && (errno == EAGAIN));

        if (cInput == nullptr) {
            break;
        }

        // change cInput into a std::string
        // easier to manipulate
        const std::string input{cInput};
        if (input.empty()) {
            continue;
        }
        rx.history_add(input);

        try {
            state = env.execute(input);
        } catch (const std::exception& e) {
            std::cerr << e.what() << '\n';
        }
    }

    if (REPL::historyFile.has_value())
        rx.history_save(REPL::historyFile.value());

    return state;
}

REPL::REPL()
{
    antlr4::ANTLRInputStream inputStream{""};
    cli::grammar::lexer::AltCliLexer const lexer{&inputStream};

    const auto& vocabulary = lexer.getVocabulary();

    const auto tokensCount = vocabulary.getMaxTokenType();

    for (size_t i = 0; i < tokensCount; ++i) {
        auto token = vocabulary.getDisplayName(i);

        if (token == vocabulary.getSymbolicName(i)) {
            continue;
        }

        if (token[0] == '\'' && token[token.size() - 1] == '\'') {
            token = token.substr(1, token.size() - 2);
        }

        const bool isKeyword = std::all_of(token.cbegin(), token.cend(), [](char c) {
            return isalpha(c);
        });

        if (isKeyword)
            m_tokens.emplace(token, replxx::Replxx::Color::BRIGHTBLUE);
        else
            m_tokens.emplace(token, replxx::Replxx::Color::GREEN);
    }
}
