Algorithms Library Toolkit
=====================================

![automaton][logo]

For documentation and quickstart see our [website][7].

What is it?
-----------
The Algorithms library toolkit (ALT) is a collection of implementations
of various types of automata, grammars, and other common structires along
with mathematical implementation of algorithms over these structures.
Originaly developed at Faculty of Information Technology of Czech Technical
University in Prague. It supposed to be a reference implementation of formal
algorithms as-are in papers. It's interface allows practical reuse of existing
algorithms in form of by pipe comunicating, partially using a query language in command
line.


The Latest Version
------------------
Packages and docker images with latest stable and nightly versions can be found visiting at [website][7].


Requirements
------------
C++-20 capable compiler, `cmake` (plus `make` or `ninja`). Linux platform (99.9 % percent of the features
should work on different platforms but there are still small parts of code that are Linux only).

Build tools:
 * `cmake >= 3.12`
 * `g++ >= 10.0` or `clang++ >= 9.0` (compilation in C++20)
 * `antlr>=4.11` and `antlr4-runtime>=4.11`

Libraries:
 * `tclap` (header-only argument parsing library, fetched by CMake and git automatically during build process)
 * `libxml2`
 * `replxx>=0.0.4`

Optional dependencies:
 * `doxygen` (for documentation generation)
 * `graphviz` (for documentation generation and automata visualizations)


Installation
------------
You can build from sources using cmake or [download packages][7] or sources of [stable release][3]
We recommend running the testsuite using `ctest` as well.

We support common cmake options, additional ones are:
* `-DWITH_DOCS=ON` to build a `doxygen` target to generate API docs.
* `-DANTLR4_JAR_LOCATION=<path>` to specify path to `antlr-complete.jar` (you can also set `ANTLR4_JAR_LOCATION` env variable).

Documentation
-------------
In order to generate API documentation, run cmake with `-DWITH_DOCS=ON` and then invoke `doxygen` target.
The high-level documentation is located at [ALT website][7].


Contributing
------------
Feel free to [fork][4] this project and send your merge requests there.
In case you want to run our Gitlab CI, remove all the following parts of code in `.gitlab-ci.yml`.
```
tags:
 - altbuilder
```

Developers
----------
Editor configuration for this project is provided by [editorconfig][6].


Issues and bugs
---------------
Please use our [issue tracker][2].

Licensing
---------
Please see LICENSE file.


Contacts
--------
 * You can contact the authors using our [issue tracker][2] or by email.

Authors
-------
 * Ing. Jan Trávníček,
 * Ing. Tomáš Pecka,
 * Ing. Štěpán Plachý,
 * et. al.


[1]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library
[2]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library/issues
[3]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library/tags
[4]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library/forks/new
[5]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library/container_registry
[6]: https://editorconfig.org
[7]: https://alt.fit.cvut.cz
[logo]: docs/logo.png
