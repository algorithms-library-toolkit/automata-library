image: ${CI_REGISTRY}/algorithms-library-toolkit/infrastructure/ci-docker-images/alpine:3.15

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    # Disable double pipelines
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
    - if: $CI_COMMIT_BRANCH
    # Run pipeline for tags
    - if: $CI_COMMIT_TAG
    # Allow scheduled run
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
    # Allow manual run
    - if: '$CI_PIPELINE_SOURCE == "web"'

stages:
 - build
 - packaging
 - notify

.template:only:default: &only-default
  only:
    - merge_requests
    - branches
    - tags

.template:only:merge-and-master: &only-merge-and-master
  only:
    - merge_requests
    - master
    - tags

.template:only:master: &only-master
  only:
    refs:
      - master

.template:only:tag: &only-tag
  only:
    - /^v.*$/
  except:
    - branches

#######################################################################################################################
# build + test

.template:build:
  <<: *only-default
  dependencies: []
  stage: build
  tags:
    - altbuilder
  script:
    - mkdir -p build && cd build
    - cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -GNinja -DANTLR4_JAR_LOCATION=/usr/bin/antlr-4.11.1-complete.jar ..
    - ninja
    - ctest . --output-on-failure -j $(nproc) --output-junit testReport.xml
    - DESTDIR="." ninja install
  artifacts:
    paths:
      - build/
    expire_in: 1 day
    reports:
      junit: build/testReport.xml

.config:builder: &distro_builder
  image: ${CI_REGISTRY}/algorithms-library-toolkit/infrastructure/ci-docker-images/alt-builder:latest

.build:builder:
  <<: *distro_builder
  extends: .template:build

build:builder:gcc:
  extends: .build:builder

build:builder:clang-sanitizers:
  extends: .build:builder
  variables:
    CXX: clang++
    CXXFLAGS: "-fsanitize=address -fsanitize=undefined -fno-sanitize-recover=all" # -stdlib=libc++

#######################################################################################################################
# doc

build:doc:
  <<: *only-default
  image: ${CI_REGISTRY}/algorithms-library-toolkit/infrastructure/ci-docker-images/alt-builder:latest
  stage: build
  before_script:
    - apt-get update && apt-get install -y doxygen graphviz
  script:
    - mkdir -p build && cd build
    - cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DWITH_DOCS=1 -GNinja -DANTLR4_JAR_LOCATION=/usr/bin/antlr-4.11.1-complete.jar ..
    - ninja doxygen
  artifacts:
    paths:
      - build/apidoc/
    expire_in: 2 days

#######################################################################################################################
# package
# .template:package:
#   <<: *only-merge-and-master
#   stage: build
#   script:
#     - apkg build-dep
#     - apkg build
#   artifacts:
#     paths:
#       - pkg/pkgs
#       - images.tar
#     expire_in: 2 days

# .template:docker: &docker
#   cache: {}  # disable
#   services:
#     - name: ${CI_REGISTRY}/algorithms-library-toolkit/infrastructure/ci-docker-images/docker:dind
#       alias: docker
#   variables:
#     DOCKER_DRIVER: overlay2
#     DOCKER_HOST: docker
#     # TODO: Remove after enabling TLS; https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#docker-in-docker-with-tls-disabled-in-the-docker-executor.
#     DOCKER_HOST: tcp://docker:2375
#     DOCKER_TLS_CERTDIR: ""

#   before_script:
#     - apk add docker bash git
#     - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
#     - docker info

# package:docker:
#   <<: *docker
#   image: ${CI_REGISTRY}/algorithms-library-toolkit/infrastructure/ci-docker-images/alpine:3.15
#   extends: .template:package
#   script:
#     - docker build --target=deploy -f extra/docker/Dockerfile -t alt .
#     - docker run alt /usr/bin/aql2 --version
#     - docker tag alt "$CI_REGISTRY_IMAGE:snapshot"
#     - docker save -o images.tar "$CI_REGISTRY_IMAGE:snapshot"

# package:arch:rolling:
#   extends: .template:package
#   image: ${CI_REGISTRY}/algorithms-library-toolkit/infrastructure/ci-docker-images/archlinux:latest
#   before_script:
#     - pacman -Suy --noconfirm python-setuptools python-pip git sudo binutils fakeroot
#     - pacman -Sy --noconfirm cmake gcc make # https://gitlab.nic.cz/packaging/apkg/-/issues/64
#     - echo "MAKEFLAGS=\"\$MAKEFLAGS -j$(nproc)\"" >> /etc/makepkg.conf
#     - pip install apkg
#   script: # A little dance with apkg because build step must be done as non-root (makepkg) and we need everything to be writable by nobody
#     - apkg build-dep
#     - chown -R nobody:nobody .
#     - sudo -u nobody apkg build

# .package:deb:
#   extends: .template:package
#   before_script:
#     - apt-get update && apt-get install -y python3-pip git
#     - ${PIP_EXECUTABLE} install apkg
#   variables:
#     PIP_EXECUTABLE: pip
#     DEBIAN_FRONTEND: noninteractive

# package:debian:bullseye: # deb-11
#   extends: .package:deb
#   image: ${CI_REGISTRY}/algorithms-library-toolkit/infrastructure/ci-docker-images/amd64/debian:bullseye

# package:debian:bookworm: # deb-testing
#   extends: .package:deb
#   image: ${CI_REGISTRY}/algorithms-library-toolkit/infrastructure/ci-docker-images/amd64/debian:bookworm
#   script:
#     - apkg build-dep --distro debian-98 # otherwise identifies as "debian"
#     - apkg build --distro debian-98

# package:debian:sid: # deb-unstable
#   extends: .package:deb
#   image: ${CI_REGISTRY}/algorithms-library-toolkit/infrastructure/ci-docker-images/amd64/debian:sid
#   script:
#     - apkg build-dep --distro debian-99 # otherwise identifies as "debian"
#     - apkg build --distro debian-99

# package:ubuntu:20.04:
#   extends: .package:deb
#   image: ${CI_REGISTRY}/algorithms-library-toolkit/infrastructure/ci-docker-images/ubuntu:20.04
#   variables:
#     CXX: g++-10

# package:ubuntu:22.04:
#   extends: .package:deb
#   image: ${CI_REGISTRY}/algorithms-library-toolkit/infrastructure/ci-docker-images/ubuntu:22.04

# package:ubuntu:22.10:
#   extends: .package:deb
#   image: ${CI_REGISTRY}/algorithms-library-toolkit/infrastructure/ci-docker-images/ubuntu:22.10

# .package:opensuse:
#   extends: .template:package
#   before_script:
#     - zypper install --no-confirm python3-pip git rpm-build
#     - pip3 install apkg

# package:opensuse:tumbleweed:
#   extends: .package:opensuse
#   image: ${CI_REGISTRY}/algorithms-library-toolkit/infrastructure/ci-docker-images/opensuse/tumbleweed
#   variables:
#     CXX: g++-11

# package:opensuse:leap:15.3:
#   extends: .package:opensuse
#   image: ${CI_REGISTRY}/algorithms-library-toolkit/infrastructure/ci-docker-images/opensuse/leap:15.3
#   variables:
#     CXX: g++-11

# package:opensuse:leap:15.4:
#   extends: .package:opensuse
#   image: ${CI_REGISTRY}/algorithms-library-toolkit/infrastructure/ci-docker-images/opensuse/leap:15.4
#   variables:
#     CXX: g++-11

# .package:fedora:
#   extends: .template:package
#   before_script:
#     - dnf install -y python3-pip git rpm-build
#     - pip3 install apkg

# package:fedora:34:
#   extends: .package:fedora
#   image: ${CI_REGISTRY}/algorithms-library-toolkit/infrastructure/ci-docker-images/fedora:34

# package:fedora:35:
#   extends: .package:fedora
#   image: ${CI_REGISTRY}/algorithms-library-toolkit/infrastructure/ci-docker-images/fedora:35

# package:fedora:36:
#   extends: .package:fedora
#   image: ${CI_REGISTRY}/algorithms-library-toolkit/infrastructure/ci-docker-images/fedora:36

# packages:
#   stage: packaging
#   <<: *only-merge-and-master
#   dependencies:
#     - package:arch:rolling
#     - package:ubuntu:20.04
#     - package:ubuntu:22.04
#     - package:ubuntu:22.10
#     - package:debian:bullseye
#     - package:debian:bookworm
#     - package:debian:sid
#     - package:opensuse:tumbleweed
#     - package:opensuse:leap:15.3
#     - package:opensuse:leap:15.4
#     - package:fedora:34
#     - package:fedora:35
#     - package:fedora:36
#     - package:docker
#   script:
#     - echo "Hi, I can't be empty"
#   artifacts:
#     paths:
#       - pkg/pkgs
#     expire_in: 10 weeks

# packages:docker:
#   stage: packaging
#   <<: *docker
#   <<: *only-master
#   dependencies:
#     - package:docker
#   script:
#     - docker load -i images.tar
#     - docker push "$CI_REGISTRY_IMAGE":snapshot

#######################################################################################################################
# static analysis

.template:static-analysis:
  <<: *only-default
  stage: build
  image: ${CI_REGISTRY}/algorithms-library-toolkit/infrastructure/ci-docker-images/alt-builder:latest
  before_script:
    - mkdir -p build && pushd build
    - cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_EXPORT_COMPILE_COMMANDS=ON .. -GNinja -DANTLR4_JAR_LOCATION=/usr/bin/antlr-4.11.1-complete.jar
    - popd
  allow_failure: true # TODO: Remove
  dependencies: []

# -----------------------------------------------------------------------------

# static-analysis:cppcheck:
#   extends: .template:static-analysis
#   script:
#     - cppcheck --version
#     - cppcheck -q --enable=all --project=build/compile_commands.json --suppress="*:*/lib/*" --error-exitcode=1

static-analysis:clang-format:
  extends: .template:static-analysis
  script:
    - ANCESTOR=$(git merge-base origin/master HEAD) # find nearest common ancestor between HEAD and master
    - git diff $ANCESTOR..HEAD | tee orig.patch | clang-format-diff -p1 -regex="'./a.*/\(test-\)?src/.*'" | sed "s/+++ /+++ b\//    g;s/--- /--- a\//g" > clang-format.patch
    - if [[ -s clang-format.patch ]]; then cat clang-format.patch; echo "Download the patch in the artifact directory"; false; fi     # file exists and has nonzero size - fail
  allow_failure: false
  artifacts:
    when: always
    paths:
      - orig.patch
      - clang-format.patch

static-analysis:clang-tidy:
  extends: .template:static-analysis
  variables:
    CXX: clang++
  script:
    - exit 0 # Disable clang-tidy for now, it's too demanding and runner too slow
    - clang-tidy --version
    - jq ".[].file" build/compile_commands.json | tr -d "\"" | grep -v "test-src" | xargs -n1 -P$(grep -c processor /proc/cpuinfo) clang-tidy -quiet -p build/ 2>&1 | grep -v "warnings generated"

# static-analysis:sources-check:
#   extends: .template:static-analysis
#   script:
#     - python3 extra/scripts/sources-check.py .

#######################################################################################################################
# notify

.template:notify:
  stage: notify
  before_script:
    - apk add --no-cache curl
  dependencies: []
  script:
    - curl -X POST -F token="$TOKEN" -F ref=master $PARAMS https://gitlab.fit.cvut.cz/api/v4/projects/$PROJECT_ID/trigger/pipeline
