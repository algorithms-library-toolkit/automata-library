/*
 * Author: Radovan Cerveny
 */

#pragma once

#include "MeasurementEngine.hpp"

namespace measurements {

void start(measurements::stealth_string name, measurements::Type type);

void end();

void reset();

MeasurementResults results();

template <typename Hint>
void hint(Hint hint);

// hint shortcuts
void counterInc(const measurements::stealth_string&, CounterHint::value_type = 1);
void counterDec(const measurements::stealth_string&, CounterHint::value_type = 1);
}
