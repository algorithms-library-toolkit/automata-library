/*
 * Author: Radovan Cerveny
 */

#pragma once

#include <ostream>
#include "MeasurementFrame.hpp"
#include "MeasurementTypes.hpp"

namespace measurements {

enum class MeasurementFormat : long {
    LIST = 1,
    TREE = 2
};

struct MeasurementXalloc {
    static const int FORMAT;
};

struct MeasurementResults {
    measurements::stealth_vector<MeasurementFrame> frames;

    MeasurementResults();
    MeasurementResults(measurements::stealth_vector<MeasurementFrame>);

    void printAsList(std::ostream&) const;
    void printAsTree(std::ostream&) const;

    static MeasurementResults aggregate(const std::vector<MeasurementResults>&);

private:
    void printAsList(std::ostream&, unsigned) const;
    void printAsTree(std::ostream&, unsigned, std::string&, bool) const;
};

std::ostream& operator<<(std::ostream&, const MeasurementResults&);

template <typename CharT, typename Traits>
inline std::basic_ostream<CharT, Traits>& operator<<(std::basic_ostream<CharT, Traits>& x, MeasurementFormat f)
{
    x.iword(MeasurementXalloc::FORMAT) = static_cast<long>(f); // FIXME in C++23 use std::to_underlying
    return x;
}

}
