/*
 * Author: Radovan Cerveny
 */

#pragma once

void* operator new(std::size_t n, bool measure);

void operator delete(void* ptr, bool measure) noexcept;
