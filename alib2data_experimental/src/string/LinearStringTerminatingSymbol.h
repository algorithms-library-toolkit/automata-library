#pragma once

#include <ext/iostream>

#include <alib/deque>
#include <alib/set>
#include <alib/vector>

#include <core/modules.hpp>
#include <core/type_details_base.hpp>

#include <core/xmlApi.hpp>
#include <sax/Token.h>

#include <string/LinearString.h>

namespace string {

/**
 * Represents regular expression parsed from the XML. Regular expression is stored
 * as a tree of LinearStringElement.
 */
class LinearStringTerminatingSymbol final : public core::Components<LinearStringTerminatingSymbol, ext::set<DefaultSymbolType>, module::Set, component::GeneralAlphabet, DefaultSymbolType, module::Value, component::TerminatingSymbol> {
    ext::vector<DefaultSymbolType> m_Data;

public:
    explicit LinearStringTerminatingSymbol(DefaultSymbolType terminatingSymbol);
    explicit LinearStringTerminatingSymbol(ext::set<DefaultSymbolType> alphabet, DefaultSymbolType terminatingSymbol, ext::vector<DefaultSymbolType> data);
    explicit LinearStringTerminatingSymbol(DefaultSymbolType terminatingSymbol, ext::vector<DefaultSymbolType> data);
    explicit LinearStringTerminatingSymbol(DefaultSymbolType terminatingSymbol, const LinearString<>& str);

    virtual const ext::set<DefaultSymbolType>& getAlphabet() const
    {
        return accessComponent<component::GeneralAlphabet>().get();
    }

    void extendAlphabet(const ext::set<DefaultSymbolType>& symbols)
    {
        accessComponent<component::GeneralAlphabet>().add(symbols);
    }

    const DefaultSymbolType& getTerminatingSymbol() const
    {
        return accessComponent<component::TerminatingSymbol>().get();
    }

    /**
     * @param element to append
     */
    void appendSymbol(DefaultSymbolType symbol);

    /**
     * @return List of symbols forming string (const version).
     */
    const ext::vector<DefaultSymbolType>& getContent() const;

    void setContent(ext::vector<DefaultSymbolType> data);

    /**
     * @return true if string is an empty word (vector length is 0)
     */
    bool isEmpty() const;

    auto operator<=>(const LinearStringTerminatingSymbol& other) const
    {
        return std::tie(m_Data, getAlphabet()) <=> std::tie(other.m_Data, other.getAlphabet());
    }

    bool operator==(const LinearStringTerminatingSymbol& other) const
    {
        return std::tie(m_Data, getAlphabet()) == std::tie(other.m_Data, other.getAlphabet());
    }

    friend ext::ostream& operator<<(ext::ostream& out, const LinearStringTerminatingSymbol& instance);
};

} /* namespace string */

namespace core {

template <>
struct xmlApi<string::LinearStringTerminatingSymbol> {
    static string::LinearStringTerminatingSymbol parse(ext::deque<sax::Token>::iterator& input);
    static bool first(const ext::deque<sax::Token>::const_iterator& input);
    static std::string xmlTagName();
    static void compose(ext::deque<sax::Token>& output, const string::LinearStringTerminatingSymbol& data);
};

template <>
class SetConstraint<string::LinearStringTerminatingSymbol, DefaultSymbolType, component::GeneralAlphabet> {
public:
    static bool used(const string::LinearStringTerminatingSymbol& str, const DefaultSymbolType& symbol)
    {
        const ext::vector<DefaultSymbolType>& content = str.getContent();
        return std::find(content.begin(), content.end(), symbol) != content.end();
    }

    static bool available(const string::LinearStringTerminatingSymbol&, const DefaultSymbolType&)
    {
        return true;
    }

    static void valid(const string::LinearStringTerminatingSymbol&, const DefaultSymbolType&)
    {
    }
};

template <>
class ElementConstraint<string::LinearStringTerminatingSymbol, DefaultSymbolType, component::TerminatingSymbol> {
public:
    static bool available(const string::LinearStringTerminatingSymbol& str, const DefaultSymbolType& symbol)
    {
        return str.getAlphabet().contains(symbol);
    }

    static void valid(const string::LinearStringTerminatingSymbol&, const DefaultSymbolType&)
    {
    }
};

template <>
struct type_details_retriever<string::LinearStringTerminatingSymbol> {
    static std::unique_ptr<type_details_base> get()
    {
        return std::make_unique<type_details_type>("string::LinearStringTerminatingSymbol");
    }
};

} /* namespace core */
