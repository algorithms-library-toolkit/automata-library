#pragma once

#include <alib/deque>
#include <alib/set>

#include <sax/Token.h>
#include "../stringology/SuffixTrieNodeTerminatingSymbol.h"

namespace indexes {

/**
 * Parser used to get indexes from XML parsed into list of Tokens.
 */
class IndexFromXMLParser {
public:
    static SuffixTrieNodeTerminatingSymbol* parseSuffixTrieNodeTerminatingSymbol(ext::deque<sax::Token>::iterator& input);
    static ext::set<DefaultSymbolType> parseAlphabet(ext::deque<sax::Token>::iterator& input);
};

} /* namespace indexes */
