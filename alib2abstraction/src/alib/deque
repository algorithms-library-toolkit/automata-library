#pragma once

#include <ext/deque>

#include <core/type_details.hpp>
#include <ext/typeinfo>
#include <factory/NormalizeFactory.hpp>
#include <object/AnyObject.h>
#include <object/Object.h>
#include <object/ObjectFactory.h>

namespace core {

template <typename T>
struct type_util<ext::deque<T>> {
    static ext::deque<T> denormalize(ext::deque<object::Object>&& arg)
    {
        ext::deque<T> res;
        for (object::Object&& item : ext::make_mover(arg))
            res.push_back(factory::NormalizeFactory::denormalize<T>(std::move(item)));

        return res;
    }

    static ext::deque<object::Object> normalize(ext::deque<T>&& arg)
    {
        ext::deque<object::Object> res;
        for (T&& item : ext::make_mover(arg))
            res.push_back(factory::NormalizeFactory::normalize<T>(std::move(item)));

        return res;
    }

    static std::unique_ptr<type_details_base> type(const ext::deque<T>& arg)
    {
        core::unique_ptr_set<type_details_base> subTypes;

        for (const T& item : arg)
            subTypes.insert(type_util<T>::type(item));

        std::vector<std::unique_ptr<type_details_base>> sub_types_vec;
        sub_types_vec.push_back(type_details_variant_type::make_variant(std::move(subTypes)));
        return std::make_unique<type_details_template>("ext::deque", std::move(sub_types_vec));
    }
};

template <class T>
struct type_details_retriever<ext::deque<T>> {
    static std::unique_ptr<type_details_base> get()
    {
        std::vector<std::unique_ptr<type_details_base>> sub_types_vec;
        sub_types_vec.push_back(type_details_retriever<T>::get());
        return std::make_unique<type_details_template>("ext::deque", std::move(sub_types_vec));
    }
};

}
