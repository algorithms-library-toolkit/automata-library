#pragma once

#include <ext/unordered_map>

#include <core/type_details.hpp>
#include <ext/typeinfo>
#include <factory/NormalizeFactory.hpp>
#include <object/AnyObject.h>
#include <object/Object.h>
#include <object/ObjectFactory.h>

namespace core {

template <typename T, typename U>
struct type_util<ext::unordered_map<T, U>> {
    static ext::unordered_map<T, U> denormalize(ext::unordered_map<object::Object, object::Object>&& arg)
    {
        ext::unordered_map<T, U> res;
        for (std::pair<object::Object, object::Object>&& item : ext::make_mover(arg))
            res.insert(factory::NormalizeFactory::denormalize<T>(std::move(item.first)), factory::NormalizeFactory::denormalize<U>(std::move(item.second)));

        return res;
    }

    static ext::unordered_map<object::Object, object::Object> normalize(ext::unordered_map<T, U>&& arg)
    {
        ext::unordered_map<object::Object, object::Object> res;
        for (std::pair<T, U>&& item : ext::make_mover(arg))
            res.insert(factory::NormalizeFactory::normalize<T>(std::move(item.first)), factory::NormalizeFactory::normalize<U>(std::move(item.second)));

        return res;
    }

    static std::unique_ptr<type_details_base> type(const ext::unordered_map<T, U>& arg)
    {
        core::unique_ptr_set<type_details_base> subTypes1;
        core::unique_ptr_set<type_details_base> subTypes2;
        for (const std::pair<const T, U>& item : arg) {
            subTypes1.insert(type_util<T>::type(item.first));
            subTypes2.insert(type_util<U>::type(item.second));
        }

        std::vector<std::unique_ptr<type_details_base>> sub_types_vec;
        sub_types_vec.push_back(type_details_variant_type::make_variant(std::move(subTypes1)));
        sub_types_vec.push_back(type_details_variant_type::make_variant(std::move(subTypes2)));
        return std::make_unique<type_details_template>("ext::unordered_map", std::move(sub_types_vec));
    }
};

template <class T, class U>
struct type_details_retriever<ext::unordered_map<T, U>> {
    static std::unique_ptr<type_details_base> get()
    {
        std::vector<std::unique_ptr<type_details_base>> sub_types_vec;
        sub_types_vec.push_back(type_details_retriever<T>::get());
        sub_types_vec.push_back(type_details_retriever<U>::get());
        return std::make_unique<type_details_template>("ext::unordered_map", std::move(sub_types_vec));
    }
};

}
