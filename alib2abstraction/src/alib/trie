#pragma once

#include <ext/trie>

#include <core/type_details.hpp>
#include <ext/typeinfo>
#include <factory/NormalizeFactory.hpp>
#include <object/AnyObject.h>
#include <object/Object.h>
#include <object/ObjectFactory.h>

namespace core {

template <typename T, typename U>
class type_util<ext::trie<T, U>> {
    static ext::trie<T, U> recursiveDenormalize(ext::trie<object::Object, object::Object>&& arg)
    {
        ext::map<T, ext::trie<T, U>> children;

        for (std::pair<object::Object, ext::trie<object::Object, object::Object>>&& child : ext::make_mover(arg.getChildren()))
            children.insert(factory::NormalizeFactory::denormalize<T>(std::move(child.first)), recursiveDenormalize(std::move(child.second)));

        return ext::trie<T, U>(factory::NormalizeFactory::denormalize<U>(std::move(arg.getData())), std::move(children));
    }

    static ext::trie<object::Object, object::Object> recursiveNormalize(ext::trie<T, U>&& arg)
    {
        ext::map<object::Object, ext::trie<object::Object, object::Object>> children;

        for (std::pair<T, ext::trie<T, U>>&& child : ext::make_mover(arg.getChildren()))
            children.insert(factory::NormalizeFactory::normalize<T>(std::move(child.first)), recursiveNormalize(std::move(child.second)));

        return ext::trie<object::Object, object::Object>(factory::NormalizeFactory::normalize<U>(std::move(arg.getData())), std::move(children));
    }

    static void recursiveTypeRetrieval(core::unique_ptr_set<type_details_base>& subTypes1, core::unique_ptr_set<type_details_base>& subTypes2, const ext::trie<T, U>& arg)
    {
        for (const std::pair<const T, ext::trie<T, U>>& child : arg) {
            subTypes1.insert(type_util<T>::type(child.first));
            recursiveTypeRetrieval(subTypes1, subTypes2, child.second);
        }

        subTypes2.insert(type_util<U>::type(arg.getData()));
    }

public:
    static ext::trie<T, U> denormalize(ext::trie<object::Object, object::Object>&& arg)
    {
        return recursiveDenormalize(std::move(arg));
    }

    static ext::trie<object::Object, object::Object> normalize(ext::trie<T, U>&& arg)
    {
        return recursiveNormalize(std::move(arg));
    }

    static std::unique_ptr<type_details_base> type(const ext::trie<T, U>& arg)
    {
        core::unique_ptr_set<type_details_base> subTypes1;
        core::unique_ptr_set<type_details_base> subTypes2;
        recursiveTypeRetrieval(subTypes1, subTypes2, arg);

        std::vector<std::unique_ptr<type_details_base>> sub_types_vec;
        sub_types_vec.push_back(type_details_variant_type::make_variant(std::move(subTypes1)));
        sub_types_vec.push_back(type_details_variant_type::make_variant(std::move(subTypes2)));
        return std::make_unique<type_details_template>("ext::trie", std::move(sub_types_vec));
    }
};

template <class T, class U>
struct type_details_retriever<ext::trie<T, U>> {
    static std::unique_ptr<type_details_base> get()
    {
        std::vector<std::unique_ptr<type_details_base>> sub_types_vec;
        sub_types_vec.push_back(type_details_retriever<T>::get());
        sub_types_vec.push_back(type_details_retriever<U>::get());
        return std::make_unique<type_details_template>("ext::trie", std::move(sub_types_vec));
    }
};

}
