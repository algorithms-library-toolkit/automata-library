#include <abstraction/TemporariesHolder.h>

namespace abstraction {

void TemporariesHolder::holdTemporary(std::shared_ptr<abstraction::Value> temporary)
{
    m_temporaries.push_back(std::move(temporary));
}

void TemporariesHolder::clearTemporaries()
{
    m_temporaries.clear();
}

} /* namespace abstraction */
