#pragma once

#include <memory>

#include <abstraction/NaryOperationAbstraction.hpp>
#include <abstraction/ValueOperationAbstraction.hpp>

#include <ext/ostream>

namespace abstraction {

template <class ParamType>
class ValuePrinterAbstraction : virtual public NaryOperationAbstraction<ParamType, ext::ostream&>, virtual public ValueOperationAbstraction<void> {
public:
    ValuePrinterAbstraction() = default;

    std::shared_ptr<abstraction::Value> run() const override
    {
        const std::shared_ptr<abstraction::Value>& firstParam = std::get<0>(this->getParams());
        const std::shared_ptr<abstraction::Value>& secondParam = std::get<1>(this->getParams());

        retrieveValue<ext::ostream&>(secondParam) << retrieveValue<const ParamType&>(firstParam) << std::endl;
        return std::make_shared<abstraction::Void>();
    }
};

template <>
class ValuePrinterAbstraction<void> : virtual public NaryOperationAbstraction<>, virtual public ValueOperationAbstraction<void> {
public:
    ValuePrinterAbstraction() = default;

    std::shared_ptr<abstraction::Value> run() const override
    {
        return std::make_shared<abstraction::Void>();
    }
};

} /* namespace abstraction */
