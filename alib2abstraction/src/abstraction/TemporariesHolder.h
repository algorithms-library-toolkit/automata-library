#pragma once

#include <abstraction/Value.hpp>
#include <ext/vector>

namespace abstraction {

class TemporariesHolder {
    std::vector<std::shared_ptr<abstraction::Value>> m_temporaries;

public:
    TemporariesHolder() = default;

    TemporariesHolder(const TemporariesHolder&) = delete;

    TemporariesHolder(TemporariesHolder&&) = default;

    TemporariesHolder& operator=(const TemporariesHolder&) = delete;

    TemporariesHolder& operator=(TemporariesHolder&&) = default;

    virtual ~TemporariesHolder() = default;

    void holdTemporary(std::shared_ptr<abstraction::Value> temporary);

    void clearTemporaries();
};

} /* namespace abstraction */
