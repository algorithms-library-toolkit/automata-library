#pragma once

#include <abstraction/NaryOperationAbstraction.hpp>
#include <abstraction/ValueOperationAbstraction.hpp>

namespace abstraction {

template <class ReturnType, class... ParamTypes>
class AlgorithmAbstraction : virtual public NaryOperationAbstraction<ParamTypes...>, virtual public ValueOperationAbstraction<ReturnType> {
    std::function<ReturnType(ParamTypes...)> m_callback;

public:
    explicit AlgorithmAbstraction(std::function<ReturnType(ParamTypes...)> callback)
        : m_callback(std::move(callback))
    {
    }

    std::shared_ptr<abstraction::Value> run() const override
    {
        return ValueOperationAbstraction<ReturnType>::template run_helper<ParamTypes...>(m_callback, this->getParams());
    }
};

} /* namespace abstraction */
