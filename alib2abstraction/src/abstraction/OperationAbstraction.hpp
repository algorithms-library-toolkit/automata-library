#pragma once

#include <ext/memory>
#include <ext/set>
#include <ext/string>
#include <ext/typeindex>

#include <common/TypeQualifiers.hpp>
#include <core/type_details.hpp>

namespace abstraction {

class Value;

class OperationAbstraction {
public:
    virtual void attachInput(const std::shared_ptr<abstraction::Value>& input, size_t index) = 0;
    virtual void detachInput(size_t index) = 0;

    virtual ~OperationAbstraction() noexcept = default;

    virtual bool inputsAttached() const = 0;
    virtual std::shared_ptr<abstraction::Value> eval() = 0;
    virtual size_t numberOfParams() const = 0;

    virtual core::type_details getParamType(size_t index) const = 0;

    virtual abstraction::TypeQualifiers::TypeQualifierSet getParamTypeQualifiers(size_t index) const = 0;

    virtual core::type_details getReturnType() const = 0;

    virtual abstraction::TypeQualifiers::TypeQualifierSet getReturnTypeQualifiers() const = 0;
};

} /* namespace abstraction */
