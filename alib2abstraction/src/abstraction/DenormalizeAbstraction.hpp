#pragma once

#include <abstraction/NaryOperationAbstraction.hpp>
#include <abstraction/ValueOperationAbstraction.hpp>

#include <factory/NormalizeFactory.hpp>

#include <object/Object.h>

#include <ext/iostream>

namespace abstraction {

template <class ParamType>
class DenormalizeAbstraction : virtual public NaryOperationAbstraction<object::Object>, virtual public ValueOperationAbstraction<ParamType&&> {
public:
    std::shared_ptr<abstraction::Value> run() const override
    {
        const std::shared_ptr<abstraction::Value>& rawParam = std::get<0>(this->getParams());

        if (rawParam->isTemporary()) {
            using ObjectType = std::conditional_t<std::is_const_v<std::remove_reference_t<ParamType>>, const object::Object&&, object::Object&&>;
            ObjectType param = retrieveValue<ObjectType>(rawParam);

            decltype(auto) data = std::forward<ObjectType>(param).getData();

            using AnyObjectBaseType = object::AnyObject<std::decay_t<ParamType>>;
            using AnyObjectType = std::conditional_t<std::is_const_v<std::remove_reference_t<ParamType>>, const AnyObjectBaseType, AnyObjectBaseType>;
            AnyObjectType* casted = dynamic_cast<AnyObjectType*>(&data);

            if (casted != nullptr) {
                using AdjustedParamType = std::conditional_t<std::is_const_v<std::remove_reference_t<ParamType>>, const std::remove_reference_t<ParamType>&&, std::remove_reference_t<ParamType>&&>;
                AdjustedParamType paramRef = std::forward<AdjustedParamType>(casted->getData());
                return std::make_shared<abstraction::ValueHolder<AdjustedParamType>>(std::forward<AdjustedParamType>(paramRef), true);
            }
        } else {
            using ObjectType = std::conditional_t<std::is_const_v<std::remove_reference_t<ParamType>>, const object::Object&, object::Object&>;
            ObjectType param = retrieveValue<ObjectType>(rawParam);

            decltype(auto) data = std::forward<ObjectType>(param).getData();

            using AnyObjectBaseType = object::AnyObject<std::decay_t<ParamType>>;
            using AnyObjectType = std::conditional_t<std::is_const_v<std::remove_reference_t<ParamType>>, const AnyObjectBaseType, AnyObjectBaseType>;
            AnyObjectType* casted = dynamic_cast<AnyObjectType*>(&data);

            if (casted != nullptr) {
                using AdjustedParamType = std::conditional_t<std::is_const_v<std::remove_reference_t<ParamType>>, const ParamType&, ParamType&>;
                AdjustedParamType paramRef = std::forward<AdjustedParamType>(casted->getData());
                return std::make_shared<abstraction::ValueHolder<AdjustedParamType>>(std::forward<AdjustedParamType>(paramRef), false);
            }
        }

        object::Object param = retrieveValue<object::Object>(rawParam);
        std::decay_t<ParamType> denormalized = factory::NormalizeFactory::denormalize<std::decay_t<ParamType>>(std::move(param));
        return std::make_shared<abstraction::ValueHolder<std::decay_t<ParamType>>>(std::move(denormalized), true);
    }
};

} /* namespace abstraction */
