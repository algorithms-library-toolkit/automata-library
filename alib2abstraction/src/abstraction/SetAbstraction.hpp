#pragma once

#include <ext/memory>

#include <abstraction/AnyaryOperationAbstraction.hpp>
#include <abstraction/ValueOperationAbstraction.hpp>

namespace abstraction {

class SetAbstraction : virtual public AnyaryOperationAbstraction<object::Object>, virtual public ValueOperationAbstraction<ext::set<object::Object>> {
public:
    std::shared_ptr<abstraction::Value> run() const override;
};

} /* namespace abstraction */
