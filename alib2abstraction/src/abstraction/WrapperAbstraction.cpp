#include "WrapperAbstraction.hpp"

template class abstraction::WrapperAbstractionImpl<1>;
template class abstraction::WrapperAbstractionImpl<2>;
template class abstraction::WrapperAbstractionImpl<3>;

namespace core {

std::unique_ptr<type_details_base> type_details_retriever<abstraction::UnspecifiedType>::get()
{
    return std::make_unique<type_details_type>("abstraction::UnspecifiedType");
}

} /* namespace core */
