#pragma once

#include <ext/set>
#include <ext/typeindex>
#include <memory>

#include <core/type_details.hpp>

#include <common/TypeQualifiers.hpp>

namespace abstraction {

class OperationAbstraction;
class ValueReference;

class Value : public std::enable_shared_from_this<Value> {
protected:
    virtual std::unique_ptr<abstraction::Value> asValue(const std::shared_ptr<abstraction::Value>& originalValue, bool move, bool isTemporary) = 0;

public:
    virtual ~Value() noexcept = default;

    std::unique_ptr<abstraction::Value> clone(abstraction::TypeQualifiers::TypeQualifierSet typeQualifiers, bool isTemporary);

    virtual std::shared_ptr<abstraction::Value> getProxyAbstraction();

    virtual core::type_details getActualType() const = 0;

    virtual core::type_details getDeclaredType() const = 0;

    virtual abstraction::TypeQualifiers::TypeQualifierSet getTypeQualifiers() const = 0;

    virtual bool isTemporary() const = 0;

    friend class ValueReference;
};

class ValueReference : public Value {
    std::weak_ptr<abstraction::Value> m_value;

    abstraction::TypeQualifiers::TypeQualifierSet m_typeQualifiers;
    bool m_isTemporary;

    std::unique_ptr<abstraction::Value> asValue(const std::shared_ptr<abstraction::Value>& originalValue, bool move, bool isTemporary) override;

public:
    ValueReference(const std::shared_ptr<abstraction::Value>& value, abstraction::TypeQualifiers::TypeQualifierSet typeQualifiers, bool isTemporary);

    std::shared_ptr<abstraction::Value> getProxyAbstraction() override;

    abstraction::TypeQualifiers::TypeQualifierSet getTypeQualifiers() const override;

    core::type_details getActualType() const override;

    core::type_details getDeclaredType() const override;

    bool isTemporary() const override;
};

class Void : public Value {
public:
    std::unique_ptr<abstraction::Value> asValue(const std::shared_ptr<abstraction::Value>& originalValue, bool move, bool isTemporary) override;

    core::type_details getActualType() const override;

    core::type_details getDeclaredType() const override;

    abstraction::TypeQualifiers::TypeQualifierSet getTypeQualifiers() const override;

    bool isTemporary() const override;
};

} /* namespace abstraction */
