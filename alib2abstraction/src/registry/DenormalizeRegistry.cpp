#include <registry/DenormalizeRegistry.hpp>

#include <ext/algorithm>

namespace abstraction {

ext::map<core::type_details, std::list<std::unique_ptr<DenormalizeRegistry::Entry>>>& DenormalizeRegistry::getEntries()
{
    static ext::map<core::type_details, std::list<std::unique_ptr<Entry>>> entries;
    return entries;
}

void DenormalizeRegistry::unregisterDenormalize(const core::type_details& param, std::list<std::unique_ptr<Entry>>::const_iterator iter)
{
    auto& entry = getEntries()[param];

    if (!ext::range_contains_iterator(entry.begin(), entry.end(), iter))
        throw std::invalid_argument(ext::concat("Normalization entry not found in ", param, "."));

    entry.erase(iter);
}

std::list<std::unique_ptr<DenormalizeRegistry::Entry>>::const_iterator DenormalizeRegistry::registerDenormalize(core::type_details param, std::unique_ptr<Entry> entry)
{
    auto& collection = getEntries()[std::move(param)];
    return collection.insert(collection.end(), std::move(entry));
}

std::unique_ptr<abstraction::OperationAbstraction> DenormalizeRegistry::getAbstraction(const core::type_details& param)
{
    auto res = getEntries().find(param);
    if (res == getEntries().end() || res->second.empty())
        throw std::invalid_argument(ext::concat("Entry ", param, " not available."));

    return res->second.front()->getAbstraction();
}

bool DenormalizeRegistry::hasDenormalize(const core::type_details& param)
{
    auto res = getEntries().find(param);
    return res != getEntries().end() && !res->second.empty();
}

ext::list<std::string> DenormalizeRegistry::list()
{
    ext::list<std::string> res;
    for (const std::pair<const core::type_details, std::list<std::unique_ptr<Entry>>>& entry : getEntries()) {
        res.push_back(ext::to_string(entry.first));
    }
    return res;
}

} /* namespace abstraction */
