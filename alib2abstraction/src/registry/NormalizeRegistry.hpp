#pragma once

#include <ext/list>
#include <ext/map>
#include <ext/memory>
#include <ext/string>
#include <ext/typeinfo>

#include <abstraction/OperationAbstraction.hpp>
#include "BaseRegistryEntry.hpp"

namespace abstraction {

class NormalizeRegistry {
public:
    class Entry : public BaseRegistryEntry {
    };

private:
    template <class ReturnType>
    class EntryImpl : public Entry {
    public:
        EntryImpl() = default;

        std::unique_ptr<abstraction::OperationAbstraction> getAbstraction() const override;
    };

    static ext::map<core::type_details, std::list<std::unique_ptr<Entry>>>& getEntries();

public:
    static void unregisterNormalize(const core::type_details& ret, std::list<std::unique_ptr<Entry>>::const_iterator iter);

    template <class ReturnType>
    static void unregisterNormalize(std::list<std::unique_ptr<Entry>>::const_iterator iter)
    {
        core::type_details ret = core::type_details::get<std::decay_t<ReturnType>>();
        unregisterNormalize(ret, iter);
    }

    static std::list<std::unique_ptr<Entry>>::const_iterator registerNormalize(core::type_details ret, std::unique_ptr<Entry> entry);

    template <class ReturnType>
    static std::list<std::unique_ptr<Entry>>::const_iterator registerNormalize(core::type_details ret)
    {
        return registerNormalize(std::move(ret), std::unique_ptr<Entry>(new EntryImpl<ReturnType>()));
    }

    template <class ReturnType>
    static std::list<std::unique_ptr<Entry>>::const_iterator registerNormalize()
    {
        core::type_details ret = core::type_details::get<std::decay_t<ReturnType>>();
        return registerNormalize<ReturnType>(std::move(ret));
    }

    static bool hasNormalize(const core::type_details& ret);

    static std::unique_ptr<abstraction::OperationAbstraction> getAbstraction(const core::type_details& ret);

    static ext::list<std::string> list();
};

} /* namespace abstraction */

#include <abstraction/NormalizeAbstraction.hpp>

namespace abstraction {

template <class ReturnType>
std::unique_ptr<abstraction::OperationAbstraction> NormalizeRegistry::EntryImpl<ReturnType>::getAbstraction() const
{
    return std::make_unique<NormalizeAbstraction<ReturnType>>();
}

} /* namespace abstraction */
