#pragma once

#include <abstraction/OperationAbstraction.hpp>
#include <common/AlgorithmCategories.hpp>
#include <common/Operators.hpp>
#include <common/TypeQualifiers.hpp>
#include <optional>
#include "AlgorithmRegistryInfo.hpp"

#include <ext/list>
#include <ext/pair>
#include <ext/tuple>

namespace abstraction {

class Registry {
public:
    static ext::set<ext::pair<std::string, ext::vector<std::string>>> listAlgorithmGroup(const std::string& group);
    static ext::set<ext::pair<std::string, ext::vector<std::string>>> listAlgorithms();

    static ext::list<ext::pair<core::type_details, bool>> listCastsFrom(const core::type_details& type);
    static ext::list<ext::pair<core::type_details, bool>> listCastsTo(const core::type_details& type);
    static ext::list<ext::tuple<core::type_details, core::type_details, bool>> listCasts();

    static ext::list<ext::tuple<AlgorithmFullInfo, std::optional<std::string>>> listOverloads(const std::string& algorithm, const ext::vector<std::string>& templateParams);
    static std::optional<std::string> getDocumentation(const std::string& algorithm, const ext::vector<std::string>& templateParams);

    static ext::list<ext::pair<Operators::BinaryOperators, AlgorithmFullInfo>> listBinaryOperators();
    static ext::list<ext::pair<Operators::PrefixOperators, AlgorithmFullInfo>> listPrefixOperators();
    static ext::list<ext::pair<Operators::PostfixOperators, AlgorithmFullInfo>> listPostfixOperators();

    static ext::list<std::string> listNormalizations();
    static ext::list<std::string> listDenormalizations();

    static std::unique_ptr<abstraction::OperationAbstraction> getContainerAbstraction(const std::string& container);
    static std::unique_ptr<abstraction::OperationAbstraction> getAlgorithmAbstraction(const std::string& name, const ext::vector<std::string>& templateParams, const ext::vector<core::type_details>& paramTypes, const ext::vector<abstraction::TypeQualifiers::TypeQualifierSet>& typeQualifiers, AlgorithmCategories::AlgorithmCategory);
    static std::unique_ptr<abstraction::OperationAbstraction> getBinaryOperatorAbstraction(Operators::BinaryOperators type, const ext::vector<core::type_details>& paramTypes, const ext::vector<abstraction::TypeQualifiers::TypeQualifierSet>& typeQualifiers, AlgorithmCategories::AlgorithmCategory);
    static std::unique_ptr<abstraction::OperationAbstraction> getPrefixOperatorAbstraction(Operators::PrefixOperators type, const ext::vector<core::type_details>& paramTypes, const ext::vector<abstraction::TypeQualifiers::TypeQualifierSet>& typeQualifiers, AlgorithmCategories::AlgorithmCategory);
    static std::unique_ptr<abstraction::OperationAbstraction> getPostfixOperatorAbstraction(Operators::PostfixOperators type, const ext::vector<core::type_details>& paramTypes, const ext::vector<abstraction::TypeQualifiers::TypeQualifierSet>& typeQualifiers, AlgorithmCategories::AlgorithmCategory);
    static std::unique_ptr<abstraction::OperationAbstraction> getCastAbstraction(const core::type_details& target, const core::type_details& param);
    static bool isCastNoOp(const core::type_details& target, const core::type_details& param);
    static std::unique_ptr<abstraction::OperationAbstraction> getNormalizeAbstraction(const core::type_details& param);
    static bool hasNormalize(const core::type_details& param);
    static std::unique_ptr<abstraction::OperationAbstraction> getDenormalizeAbstraction(const core::type_details& param);
    static bool hasDenormalize(const core::type_details& param);
    static std::unique_ptr<abstraction::OperationAbstraction> getValuePrinterAbstraction(const core::type_details& param);
};

} /* namespace abstraction */
