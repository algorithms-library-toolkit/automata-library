#pragma once

#include <ext/list>
#include <ext/map>
#include <ext/memory>
#include <ext/string>
#include <ext/tuple>
#include <ext/typeinfo>

#include <abstraction/OperationAbstraction.hpp>
#include "BaseRegistryEntry.hpp"

namespace abstraction {

class CastRegistry {
public:
    class Entry : public BaseRegistryEntry {
        bool m_isExplicit;

    public:
        explicit Entry(bool p_isExplicit)
            : m_isExplicit(p_isExplicit)
        {
        }

        bool isExplicit() const
        {
            return m_isExplicit;
        }
    };

private:
    template <class Return, class Param>
    class DefaultEntryImpl : public Entry {
    public:
        explicit DefaultEntryImpl(bool isExplicit)
            : Entry(isExplicit)
        {
        }

        std::unique_ptr<abstraction::OperationAbstraction> getAbstraction() const override;
    };

    template <class Return, class Param>
    class AlgorithmEntryImpl : public Entry {
        std::function<Return(Param)> m_callback;

    public:
        explicit AlgorithmEntryImpl(std::function<Return(Param)> callback, bool isExplicit)
            : Entry(isExplicit)
            , m_callback(std::move(callback))
        {
        }

        std::unique_ptr<abstraction::OperationAbstraction> getAbstraction() const override;
    };

    static ext::map<ext::pair<core::type_details, core::type_details>, std::unique_ptr<Entry>>& getEntries();

public:
    static void unregisterCast(const core::type_details& target, const core::type_details& param);

    template <class TargetType, class ParamType>
    static void unregisterCast()
    {
        core::type_details target = core::type_details::get<std::decay_t<TargetType>>();
        core::type_details param = core::type_details::get<std::decay_t<ParamType>>();

        unregisterCast(target, param);
    }

    static void registerCast(core::type_details target, core::type_details param, std::unique_ptr<Entry> entry);

    template <class TargetType, class ParamType>
    static void registerCast(core::type_details target, core::type_details param, bool isExplicit = false)
    {
        registerCast(std::move(target), std::move(param), std::unique_ptr<Entry>(new DefaultEntryImpl<TargetType, ParamType>(isExplicit)));
    }

    template <class TargetType, class ParamType>
    static void registerCast(bool isExplicit = false)
    {
        core::type_details target = core::type_details::get<std::decay_t<TargetType>>();
        core::type_details param = core::type_details::get<std::decay_t<ParamType>>();

        registerCast<TargetType, ParamType>(std::move(target), std::move(param), isExplicit);
    }

    template <class TargetType, class ParamType>
    static void registerCastAlgorithm(core::type_details target, core::type_details param, TargetType (*callback)(const ParamType&), bool isExplicit = false)
    {
        registerCast(std::move(target), std::move(param), std::unique_ptr<Entry>(new AlgorithmEntryImpl<TargetType, const ParamType&>(callback, isExplicit)));
    }

    template <class TargetType, class ParamType>
    static void registerCastAlgorithm(TargetType (*callback)(const ParamType&), bool isExplicit = false)
    {
        core::type_details target = core::type_details::get<std::decay_t<TargetType>>();
        core::type_details param = core::type_details::get<std::decay_t<ParamType>>();

        registerCastAlgorithm<TargetType, ParamType>(std::move(target), std::move(param), callback, isExplicit);
    }

    template <class TargetType, class ParamType>
    static void registerCastAlgorithm(core::type_details target, core::type_details param, TargetType (*callback)(ParamType), bool isExplicit = false)
    {
        registerCast(std::move(target), std::move(param), std::unique_ptr<Entry>(new AlgorithmEntryImpl<TargetType, ParamType>(callback, isExplicit)));
    }

    template <class TargetType, class ParamType>
    static void registerCastAlgorithm(TargetType (*callback)(ParamType), bool isExplicit = false)
    {
        std::string target = ext::to_string<TargetType>();
        std::string param = ext::to_string<ParamType>();

        registerCastAlgorithm<TargetType, ParamType>(std::move(target), std::move(param), callback, isExplicit);
    }

    static bool isNoOp(const core::type_details& target, const core::type_details& param);

    static bool castAvailable(const core::type_details& target, const core::type_details& param, bool implicitOnly);

    static std::unique_ptr<abstraction::OperationAbstraction> getAbstraction(const core::type_details& target, const core::type_details& param);

    static ext::list<ext::pair<core::type_details, bool>> listFrom(const core::type_details& type);

    static ext::list<ext::pair<core::type_details, bool>> listTo(const core::type_details& type);

    static ext::list<ext::tuple<core::type_details, core::type_details, bool>> list();
};

} /* namespace abstraction */

#include <abstraction/AlgorithmAbstraction.hpp>
#include <abstraction/CastAbstraction.hpp>

namespace abstraction {

template <class Return, class Param>
std::unique_ptr<abstraction::OperationAbstraction> CastRegistry::DefaultEntryImpl<Return, Param>::getAbstraction() const
{
    return std::make_unique<abstraction::CastAbstraction<Return, const Param&>>();
}

template <class Return, class Param>
std::unique_ptr<abstraction::OperationAbstraction> CastRegistry::AlgorithmEntryImpl<Return, Param>::getAbstraction() const
{
    return std::make_unique<abstraction::AlgorithmAbstraction<Return, const Param&>>(m_callback);
}

} /* namespace abstraction */
