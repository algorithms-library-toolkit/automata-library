#include <registry/NormalizeRegistry.hpp>

#include <ext/algorithm>

namespace abstraction {

ext::map<core::type_details, std::list<std::unique_ptr<NormalizeRegistry::Entry>>>& NormalizeRegistry::getEntries()
{
    static ext::map<core::type_details, std::list<std::unique_ptr<Entry>>> entries;
    return entries;
}

void NormalizeRegistry::unregisterNormalize(const core::type_details& ret, std::list<std::unique_ptr<Entry>>::const_iterator iter)
{
    auto& entry = getEntries()[ret];

    if (!ext::range_contains_iterator(entry.begin(), entry.end(), iter))
        throw std::invalid_argument(ext::concat("Normalization entry not found in ", ret, "."));

    entry.erase(iter);
}

std::list<std::unique_ptr<NormalizeRegistry::Entry>>::const_iterator NormalizeRegistry::registerNormalize(core::type_details ret, std::unique_ptr<Entry> entry)
{
    auto& collection = getEntries()[std::move(ret)];
    return collection.insert(collection.end(), std::move(entry));
}

std::unique_ptr<abstraction::OperationAbstraction> NormalizeRegistry::getAbstraction(const core::type_details& ret)
{
    auto res = getEntries().find(ret);
    if (res == getEntries().end() || res->second.empty())
        throw std::invalid_argument(ext::concat("Entry ", ret, " not available."));

    return res->second.front()->getAbstraction();
}

bool NormalizeRegistry::hasNormalize(const core::type_details& ret)
{
    auto res = getEntries().find(ret);
    return res != getEntries().end() && !res->second.empty();
}

ext::list<std::string> NormalizeRegistry::list()
{
    ext::list<std::string> res;
    for (const std::pair<const core::type_details, std::list<std::unique_ptr<Entry>>>& entry : getEntries()) {
        res.push_back(ext::to_string(entry.first));
    }
    return res;
}

} /* namespace abstraction */
