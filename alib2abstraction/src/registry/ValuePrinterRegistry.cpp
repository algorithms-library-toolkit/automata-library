#include <registry/ValuePrinterRegistry.hpp>

namespace abstraction {

ext::map<core::type_details, std::unique_ptr<ValuePrinterRegistry::Entry>>& ValuePrinterRegistry::getEntries()
{
    static ext::map<core::type_details, std::unique_ptr<Entry>> fileWriters;
    return fileWriters;
}

std::unique_ptr<abstraction::OperationAbstraction> ValuePrinterRegistry::getAbstraction(const core::type_details& param)
{
    auto res = getEntries().find(param);
    if (res == getEntries().end())
        throw std::invalid_argument(ext::concat("Entry ", param, " not available."));

    return res->second->getAbstraction();
}

void ValuePrinterRegistry::unregisterValuePrinter(const core::type_details& param)
{
    if (getEntries().erase(param) == 0u)
        throw std::invalid_argument(ext::concat("Entry ", param, " not registered."));
}

void ValuePrinterRegistry::registerValuePrinter(core::type_details param, std::unique_ptr<Entry> entry)
{
    auto iter = getEntries().insert(std::make_pair(std::move(param), std::move(entry)));
    if (!iter.second)
        throw std::invalid_argument(ext::concat("Entry ", iter.first->first, " already registered."));
}

template <>
std::unique_ptr<abstraction::OperationAbstraction> ValuePrinterRegistry::EntryImpl<void>::getAbstraction() const
{
    return std::make_unique<abstraction::ValuePrinterAbstraction<void>>();
}

} /* namespace abstraction */
