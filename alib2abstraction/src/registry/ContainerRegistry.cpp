#include <ext/algorithm>
#include <ext/typeinfo>
#include <registry/ContainerRegistry.hpp>

#include <alib/set>

#include <abstraction/SetAbstraction.hpp>

namespace abstraction {

ext::map<std::string, std::unique_ptr<ContainerRegistry::Entry>>& ContainerRegistry::getEntries()
{
    static ext::map<std::string, std::unique_ptr<Entry>> containerGroups;
    return containerGroups;
}

void ContainerRegistry::unregisterSet()
{
    getEntries().erase("Set");
}

void ContainerRegistry::registerSet(std::unique_ptr<Entry> entry)
{
    getEntries().insert(std::make_pair(std::string("Set"), std::move(entry)));
}

bool ContainerRegistry::hasAbstraction(const std::string& container)
{
    return getEntries().contains(container);
}

std::unique_ptr<abstraction::OperationAbstraction> ContainerRegistry::getAbstraction(const std::string& container)
{
    auto iter = getEntries().find(container);
    if (iter == getEntries().end())
        throw std::invalid_argument(ext::concat("Entry ", container, " not available"));

    return iter->second->getAbstraction();
}

ext::set<std::string> ContainerRegistry::list()
{
    ext::set<std::string> res;

    for (const std::pair<const std::string, std::unique_ptr<Entry>>& groups : getEntries())
        res.insert(groups.first);

    return res;
}

std::unique_ptr<abstraction::OperationAbstraction> ContainerRegistry::SetEntryImpl::getAbstraction() const
{
    return std::make_unique<abstraction::SetAbstraction>();
}

} /* namespace abstraction */
