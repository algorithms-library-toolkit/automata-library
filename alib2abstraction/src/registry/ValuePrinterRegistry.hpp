#pragma once

#include <ext/map>
#include <ext/memory>
#include <ext/string>
#include <ext/typeinfo>

#include <abstraction/OperationAbstraction.hpp>
#include "BaseRegistryEntry.hpp"

namespace abstraction {

class ValuePrinterRegistry {
public:
    class Entry : public BaseRegistryEntry {
    };

private:
    template <class Param>
    class EntryImpl : public Entry {
    public:
        EntryImpl() = default;

        std::unique_ptr<abstraction::OperationAbstraction> getAbstraction() const override;
    };

    static ext::map<core::type_details, std::unique_ptr<Entry>>& getEntries();

public:
    static void unregisterValuePrinter(const core::type_details& param);

    template <class ParamType>
    static void unregisterValuePrinter()
    {
        core::type_details param = core::type_details::get<std::decay_t<ParamType>>();
        unregisterValuePrinter(param);
    }

    static void registerValuePrinter(core::type_details param, std::unique_ptr<Entry> entry);

    template <class ParamType>
    static void registerValuePrinter(core::type_details param)
    {
        registerValuePrinter(std::move(param), std::unique_ptr<Entry>(new EntryImpl<ParamType>()));
    }

    template <class ParamType>
    static void registerValuePrinter()
    {
        core::type_details param = core::type_details::get<std::decay_t<ParamType>>();
        registerValuePrinter<ParamType>(std::move(param));
    }

    static std::unique_ptr<abstraction::OperationAbstraction> getAbstraction(const core::type_details& param);
};

} /* namespace abstraction */

#include <abstraction/ValuePrinterAbstraction.hpp>

namespace abstraction {

template <class Param>
std::unique_ptr<abstraction::OperationAbstraction> ValuePrinterRegistry::EntryImpl<Param>::getAbstraction() const
{
    return std::make_unique<abstraction::ValuePrinterAbstraction<const Param&>>();
}

template <>
std::unique_ptr<abstraction::OperationAbstraction> ValuePrinterRegistry::EntryImpl<void>::getAbstraction() const;

} /* namespace abstraction */
