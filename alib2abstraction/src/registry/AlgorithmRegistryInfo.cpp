#include "AlgorithmRegistryInfo.hpp"
#include "Registry.h"

namespace abstraction {

AlgorithmBaseInfo::AlgorithmBaseInfo(AlgorithmCategories::AlgorithmCategory category, ext::vector<ext::pair<core::type_details, abstraction::TypeQualifiers::TypeQualifierSet>> params)
    : m_category(category)
    , m_params(std::move(params))
{
}

AlgorithmFullInfo::AlgorithmFullInfo(AlgorithmBaseInfo baseInfo, ext::vector<std::string> paramNames, ext::pair<core::type_details, abstraction::TypeQualifiers::TypeQualifierSet> result)
    : AlgorithmBaseInfo(std::move(baseInfo))
    , m_paramNames(std::move(paramNames))
    , m_result(std::move(result))
{
}

} /* namespace abstraction */
