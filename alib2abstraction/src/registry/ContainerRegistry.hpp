#pragma once

#include <exception>

#include <ext/list>
#include <ext/map>
#include <ext/memory>
#include <ext/set>
#include <ext/string>
#include <ext/typeinfo>

#include <abstraction/OperationAbstraction.hpp>
#include "BaseRegistryEntry.hpp"

namespace abstraction {

class ContainerRegistry {
public:
    class Entry : public BaseRegistryEntry {
    };

private:
    class SetEntryImpl : public Entry {
    public:
        std::unique_ptr<abstraction::OperationAbstraction> getAbstraction() const override;
    };

    static ext::map<std::string, std::unique_ptr<Entry>>& getEntries();

public:
    static void unregisterSet();

    static void registerSet(std::unique_ptr<Entry> entry);

    static void registerSet()
    {
        registerSet(std::make_unique<SetEntryImpl>());
    }

    static bool hasAbstraction(const std::string& container);

    static std::unique_ptr<abstraction::OperationAbstraction> getAbstraction(const std::string& container);

    static ext::set<std::string> list();
};

} /* namespace abstraction */
