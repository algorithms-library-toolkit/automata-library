#pragma once

#include <object/AnyObjectBase.h>

namespace object {

template <class Type>
class ObjectFactoryImpl;

/**
 * Wrapper around object.
 */
class Object {
    /**
     * The wrapped data.
     */
    ext::cow_shared_ptr<AnyObjectBase> m_data;

    /**
     * \brief
     * Unifies the shared pointers. For internal use when two pointer eval equal.
     */
    void unify(Object& other);

    /**
     * Constructor that wraps raw pointer. Takes ownership of the pointer.
     */
    explicit Object(AnyObjectBase* data);

    /**
     * Sets the wrapped object from raw pointer. Takes ownership of the pointer.
     *
     * Internally handles situation like storing the same value that is already wrapped. Usefull in normalisation.
     */
    void setData(AnyObjectBase* data);

    template <class Type>
    friend class ObjectFactoryImpl;

public:
    /**
     * Gets the wrapped object.
     *
     * \returns wrapped object.
     */
    const AnyObjectBase& getData() const&;

    /**
     * Gets the wrapped object.
     *
     * \returns wrapped object.
     */
    AnyObjectBase& getData() &;

    /**
     * Gets the wrapped object.
     *
     * \returns wrapped object.
     */
    AnyObjectBase&& getData() &&;

    /**
     * Sets the wrapped object from constant reference. Uses clone of the parameter internally.
     */
    void setData(const AnyObjectBase& data);

    /**
     * Sets the wrapped object from r-value reference. Uses clone of the parameter internally.
     */
    void setData(AnyObjectBase&& data);

    /**
     * The three way comparison implementation
     *
     * \param other the other object to compare with.
     *
     * \returns the strong ordering between this object and the @p other.
     */
    std::strong_ordering operator<=>(const Object& other) const;

    /**
     * The equality comparison implementation.
     *
     * \param other the other object to compare with.
     *
     * \returns true if this and other objects are equal, false othervise
     */
    bool operator==(const Object& other) const;

    /**
     * Print the wrapped object as raw representation to ostream.
     *
     * \param os ostream where to print
     * \param instance wrapper to print
     *
     * \returns modified output stream
     */
    friend ext::ostream& operator<<(ext::ostream& os, const Object& instance);

    /**
     * Casts the wrapped object to as compact as possible string representation.
     *
     * \returns string representation of the wrapped object
     */
    explicit operator std::string() const;

    /**
     * \brief
     * Increments the unique counter of the object by one. Prefix version.
     *
     * \return this instance
     */
    Object& operator++();

    /**
     * \brief
     * Increments the unique counter of the object by one. Postfix version.
     *
     * \return this instance
     */
    Object operator++(int);

    /**
     * \brief
     * Increments the unique counter of the object.
     *
     * \param by how much to increment
     */
    Object operator+=(unsigned by);

    /**
     * Getter of unique identifier
     *
     * \return the unique identifier
     */
    unsigned getId() const;

    /**
     * Deep inspection of the wrapped type
     *
     * \return wrapped type
     */
    std::unique_ptr<core::type_details_base> getType() const;

    /**
     * Allows computation of the hash for the given object
     */
    size_t Hash() const;
};

} /* namespace object */

#include <core/type_details.hpp>
#include <core/type_util.hpp>

namespace core {

template <>
struct type_util<object::Object> {
    static object::Object denormalize(object::Object&& object);

    static object::Object normalize(object::Object&& arg);

    static std::unique_ptr<type_details_base> type(const object::Object& arg);
};

template <>
struct type_details_retriever<object::Object> {
    static std::unique_ptr<type_details_base> get();
};

} /* namespace core */

namespace std {

template <>
struct hash<object::Object> {
    size_t operator()(const object::Object& obj) const;
};

} /* namespace std */
