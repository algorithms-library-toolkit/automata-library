#pragma once

#include <ext/registration>

#include <registry/OperatorRegistry.hpp>

namespace registration {

template <abstraction::Operators::BinaryOperators Type, class FirstParamType, class SecondParamType>
class BinaryOperatorRegister : public ext::Register<void> {
public:
    template <class ReturnType>
    explicit BinaryOperatorRegister(ReturnType (*callback)(FirstParamType, SecondParamType))
        : ext::Register<void>([=]() { abstraction::OperatorRegistry::registerBinary<ReturnType, FirstParamType, SecondParamType>(Type, callback); }, []() { abstraction::OperatorRegistry::unregisterBinary<FirstParamType, SecondParamType>(Type); })
    {
    }

    explicit BinaryOperatorRegister()
        : ext::Register<void>([]() { abstraction::OperatorRegistry::registerBinary<Type, FirstParamType, SecondParamType>(); }, []() { abstraction::OperatorRegistry::unregisterBinary<FirstParamType, SecondParamType>(Type); })
    {
    }
};

template <abstraction::Operators::PrefixOperators Type, class ParamType>
class PrefixOperatorRegister : public ext::Register<void> {
public:
    template <class ReturnType>
    explicit PrefixOperatorRegister(ReturnType (*callback)(ParamType))
        : ext::Register<void>([=]() { abstraction::OperatorRegistry::registerPrefix<ReturnType, ParamType>(Type, callback); }, []() { abstraction::OperatorRegistry::unregisterBinary<ParamType>(Type); })
    {
    }

    explicit PrefixOperatorRegister()
        : ext::Register<void>([]() { abstraction::OperatorRegistry::registerPrefix<Type, ParamType>(); }, []() { abstraction::OperatorRegistry::unregisterPrefix<ParamType>(Type); })
    {
    }
};

template <abstraction::Operators::PostfixOperators Type, class ParamType>
class PostfixOperatorRegister : public ext::Register<void> {
public:
    template <class ReturnType>
    explicit PostfixOperatorRegister(ReturnType (*callback)(ParamType))
        : ext::Register<void>([=]() { abstraction::OperatorRegistry::registerPostfix<ReturnType, ParamType>(Type, callback); }, []() { abstraction::OperatorRegistry::unregisterBinary<ParamType>(Type); })
    {
    }

    explicit PostfixOperatorRegister()
        : ext::Register<void>([]() { abstraction::OperatorRegistry::registerPostfix<Type, ParamType>(); }, []() { abstraction::OperatorRegistry::unregisterPostfix<ParamType>(Type); })
    {
    }
};

} /* namespace registration */
