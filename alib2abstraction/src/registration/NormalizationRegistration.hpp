#pragma once

#include <registry/NormalizeRegistry.hpp>

#include <ext/registration>

#include <object/Object.h>

namespace registration {

class NormalizationRegisterEmpty {
};

template <class ReturnType>
class NormalizationRegisterImpl : public ext::Register<std::list<std::unique_ptr<abstraction::NormalizeRegistry::Entry>>::const_iterator> {
public:
    explicit NormalizationRegisterImpl();
};

template <class ReturnType>
NormalizationRegisterImpl<ReturnType>::NormalizationRegisterImpl()
    : ext::Register<std::list<std::unique_ptr<abstraction::NormalizeRegistry::Entry>>::const_iterator>(
        []() {
            return abstraction::NormalizeRegistry::registerNormalize<ReturnType>();
        },
        [](std::list<std::unique_ptr<abstraction::NormalizeRegistry::Entry>>::const_iterator iter) {
            abstraction::NormalizeRegistry::unregisterNormalize<ReturnType>(iter);
        })
{
}

template <class ReturnType>
using NormalizationRegister = std::conditional_t<core::is_specialized<core::type_util<ReturnType>> && !std::is_same_v<std::decay_t<ReturnType>, object::Object>, NormalizationRegisterImpl<ReturnType>, NormalizationRegisterEmpty>;

} /* namespace registration */
