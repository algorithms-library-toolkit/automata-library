#pragma once

#include <registry/ValuePrinterRegistry.hpp>

namespace registration {

template <class Type>
class ValuePrinterRegister {
public:
    explicit ValuePrinterRegister()
    {
        abstraction::ValuePrinterRegistry::registerValuePrinter<Type>();
    }

    ~ValuePrinterRegister()
    {
        abstraction::ValuePrinterRegistry::unregisterValuePrinter<Type>();
    }
};

} /* namespace registration */
