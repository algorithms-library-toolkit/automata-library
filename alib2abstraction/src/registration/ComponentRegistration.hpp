#pragma once

namespace registration {

template <class ObjectType>
class ComponentRegister {
public:
    explicit ComponentRegister()
    {
        ObjectType::registerComponent();
    }

    ~ComponentRegister()
    {
        ObjectType::unregisterComponent();
    }
};

} /* namespace registration */
