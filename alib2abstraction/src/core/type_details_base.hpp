#pragma once

#include <memory>
#include <set>
#include <string>
#include <vector>

#include <ext/type_traits>
#include <ext/typeindex>

namespace core {

struct dereference_less {
    template <class T>
    bool operator()(const std::unique_ptr<T>& first, const std::unique_ptr<T>& second) const
    {
        return *first < *second;
    }
};

template <class T>
using unique_ptr_set = std::set<std::unique_ptr<T>, dereference_less>;

/*template < class T >
using unique_ptr_set = std::set < std::unique_ptr < T >, decltype ( [ ] ( const std::unique_ptr < T > & first, const std::unique_ptr < T > & second ) { return * first < * second; } ) >;*/

class type_details_base {
    virtual void print(std::ostream& os) const = 0;

protected:
    static std::pair<std::string, std::vector<std::string>> parseName(const std::string& name);

public:
    virtual ~type_details_base() noexcept = default;

    virtual bool compatible_with(const type_details_base& other) const = 0;

    friend std::ostream& operator<<(std::ostream& os, const type_details_base& arg);

    virtual std::strong_ordering operator<=>(const type_details_base& other) const = 0;
    virtual bool operator==(const type_details_base& other) const = 0;
};

class type_details_type;
class type_details_template;

class type_details_type : public type_details_base {
    std::string m_name;
    std::vector<std::string> m_namespaces;

    void print(std::ostream& os) const override;

    friend class type_details_template;

    type_details_type(std::pair<std::string, std::vector<std::string>> nameData);

public:
    type_details_type(const std::string& name);

    bool compatible_with(const type_details_base& other) const override;

    std::strong_ordering operator<=>(const type_details_base& other) const override;

    std::strong_ordering operator<=>(const type_details_type& other) const;

    bool operator==(const type_details_base& other) const override;

    bool operator==(const type_details_type& other) const;
};

class type_details_template : public type_details_base {
    std::string m_name;
    std::vector<std::string> m_namespaces;
    std::vector<std::unique_ptr<type_details_base>> m_templates;

    void print(std::ostream& os) const override;

    friend class type_details_type;

    type_details_template(std::pair<std::string, std::vector<std::string>> nameData, std::vector<std::unique_ptr<type_details_base>> templates);

public:
    type_details_template(const std::string& name, std::vector<std::unique_ptr<type_details_base>> templates);

    bool compatible_with(const type_details_base& other) const override;

    std::strong_ordering operator<=>(const type_details_base& other) const override;

    std::strong_ordering operator<=>(const type_details_template& other) const;

    bool operator==(const type_details_base& other) const override;

    bool operator==(const type_details_template& other) const;
};

class type_details_pointer : public type_details_base {
    bool m_is_const_qualified;

    std::unique_ptr<type_details_base> m_sub_type;

    void print(std::ostream& os) const override;

public:
    type_details_pointer(bool is_const_qualified, std::unique_ptr<type_details_base> sub_type);

    bool compatible_with(const type_details_base& other) const override;

    std::strong_ordering operator<=>(const type_details_base& other) const override;

    std::strong_ordering operator<=>(const type_details_pointer& other) const;

    bool operator==(const type_details_base& other) const override;

    bool operator==(const type_details_pointer& other) const;
};

class type_details_reference : public type_details_base {
    bool m_is_const_qualified;
    bool m_is_rvalue;

    std::unique_ptr<type_details_base> m_sub_type;

    void print(std::ostream& os) const override;

public:
    type_details_reference(bool is_const_qualified, bool is_rvalue, std::unique_ptr<type_details_base> sub_type);

    bool compatible_with(const type_details_base& other) const override;

    std::strong_ordering operator<=>(const type_details_base& other) const override;

    std::strong_ordering operator<=>(const type_details_reference& other) const;

    bool operator==(const type_details_base& other) const override;

    bool operator==(const type_details_reference& other) const;
};

class type_details_universal_type : public type_details_base {
    void print(std::ostream& os) const override;

public:
    bool compatible_with(const type_details_base& other) const override;

    std::strong_ordering operator<=>(const type_details_base& other) const override;

    std::strong_ordering operator<=>(const type_details_universal_type&) const;

    bool operator==(const type_details_base& other) const override;

    bool operator==(const type_details_universal_type&) const;
};

class type_details_unknown_type : public type_details_base {
    void print(std::ostream& os) const override;

public:
    bool compatible_with(const type_details_base& other) const override;

    std::strong_ordering operator<=>(const type_details_base& other) const override;

    std::strong_ordering operator<=>(const type_details_unknown_type&) const;

    bool operator==(const type_details_base& other) const override;

    bool operator==(const type_details_unknown_type&) const;
};

class type_details_variant_type : public type_details_base {
    unique_ptr_set<type_details_base> m_variants;

    friend class type_details_type;
    friend class type_details_template;

    void print(std::ostream& os) const override;

public:
    type_details_variant_type(unique_ptr_set<type_details_base> variants);

    bool compatible_with(const type_details_base& other) const override;

    std::strong_ordering operator<=>(const type_details_base& other) const override;

    std::strong_ordering operator<=>(const type_details_variant_type& other) const;

    bool operator==(const type_details_base& other) const override;

    bool operator==(const type_details_variant_type& other) const;

    static std::unique_ptr<type_details_base> make_variant(core::unique_ptr_set<type_details_base>&& types);
};

class type_details_function : public type_details_base {
    std::unique_ptr<type_details_base> m_return_type;
    std::vector<std::unique_ptr<type_details_base>> m_param_types;

    void print(std::ostream& os) const override;

public:
    type_details_function(std::unique_ptr<type_details_base> return_type, std::vector<std::unique_ptr<type_details_base>> param_types);

    bool compatible_with(const type_details_base& other) const override;

    std::strong_ordering operator<=>(const type_details_base& other) const override;

    std::strong_ordering operator<=>(const type_details_function& other) const;

    bool operator==(const type_details_base& other) const override;

    bool operator==(const type_details_function& other) const;
};

template <class T>
struct type_details_retriever {
};

template <typename T>
struct type_util : public std::false_type {
};

template <class T>
constexpr bool is_specialized = !std::is_base_of_v<std::false_type, T>;

} /* namespace core */
