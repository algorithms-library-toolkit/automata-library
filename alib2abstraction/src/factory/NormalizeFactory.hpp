#pragma once

#include <ext/type_traits>
#include <utility>

#include <object/ObjectFactory.h>

namespace factory {

class NormalizeFactory {

    template <typename T>
    struct DenormalizeVariantHelper;

    template <typename... Ts>
    struct DenormalizeVariantHelper<ext::variant<Ts...>> {
        static ext::variant<Ts...> denormalize(object::Object&& object, std::unique_ptr<core::type_details_base> type)
        {
            return impl<Ts...>(std::move(object), std::move(type));
        }

    private:
        template <typename R, typename... Rs>
        static ext::variant<Ts...> impl(object::Object&& object, std::unique_ptr<core::type_details_base> type)
        {
            std::unique_ptr<core::type_details_base> targetType = core::type_details_retriever<R>::get();

            if (type->compatible_with(*targetType))
                return ext::variant<Ts...>(factory::NormalizeFactory::denormalize<R>(std::move(object)));

            if constexpr (sizeof...(Rs) == 0)
                throw std::runtime_error("Denormalization failed: Object" + ext::to_string(ext::type_index(typeid(object))) + " is not any of variant types " + ext::to_string<ext::variant<Ts...>>());
            else
                return impl<Rs...>(std::move(object), std::move(type));
        }
    };

    template <class T>
    class is_variant : public std::false_type { };

    template <class... Ts>
    class is_variant<ext::variant<Ts...>> : public std::true_type { };

    template <class T>
    static inline constexpr bool is_variant_v = is_variant<T>::value;

public:
    template <class DesignatedType>
    static object::Object normalize(DesignatedType&& arg)
    {
        if constexpr (std::is_same_v<DesignatedType, object::Object>) {
            return std::forward<DesignatedType>(arg);
        } else if constexpr (is_variant_v<DesignatedType>) {
            auto visitor = []<class T>(T&& element) {
                return factory::NormalizeFactory::normalize<T>(std::forward<T>(element));
            };
            return ext::visit(visitor, std::forward<DesignatedType>(arg));
        } else {
            using normalizedType = typename std::decay<typename std::result_of<decltype (&core::type_util<DesignatedType>::normalize)(DesignatedType &&)>::type>::type;

            if constexpr (std::is_same_v<normalizedType, DesignatedType>) {
                return object::ObjectFactory<>::construct(std::forward<DesignatedType>(arg));
            } else {
                return object::ObjectFactory<>::construct(core::type_util<DesignatedType>::normalize(std::forward<DesignatedType>(arg)));
            }
        }
    }

    template <class DesignatedType>
    static decltype(auto) denormalize(object::Object&& arg)
    {
        if constexpr (std::is_same_v<DesignatedType, object::Object>) {
            return std::move(arg);
        } else if constexpr (is_variant_v<DesignatedType>) {
            std::unique_ptr<core::type_details_base> type = core::type_util<object::Object>::type(arg);
            return DenormalizeVariantHelper<DesignatedType>::denormalize(std::move(arg), std::move(type));
        } else {
            using normalizedType = typename std::decay_t<typename std::invoke_result_t<decltype(core::type_util<DesignatedType>::normalize), DesignatedType&&>>;

            object::AnyObjectBase& data = arg.getData();
            object::AnyObject<normalizedType>* casted = dynamic_cast<object::AnyObject<normalizedType>*>(&data);

            if (casted == nullptr)
                throw std::runtime_error("Denormalization failed: " + ext::to_string(ext::type_index(typeid(data))) + " is not " + ext::to_string<object::AnyObject<normalizedType>>());

            if constexpr (std::is_same_v<normalizedType, DesignatedType>) {
                return std::move(*casted).getData();
            } else {
                return core::type_util<DesignatedType>::denormalize(std::move(*casted).getData());
            }
        }
    }
};

} /* namespace factory */
