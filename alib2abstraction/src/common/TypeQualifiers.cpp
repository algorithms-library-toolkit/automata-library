#include "TypeQualifiers.hpp"

namespace abstraction {

std::ostream& operator<<(std::ostream& os, TypeQualifiers::TypeQualifierSet typeQualifiers)
{
    os << "[ ";
    bool first = true;
    if (TypeQualifiers::isConst(typeQualifiers))
        os << (first ? "" : ", ") << "Constant", first = false;
    if (TypeQualifiers::isLvalueRef(typeQualifiers))
        os << (first ? "" : ", ") << "LValue reference", first = false;
    if (TypeQualifiers::isRvalueRef(typeQualifiers))
        os << (first ? "" : ", ") << "RValue reference", first = false;
    os << " ]";
    return os;
}

} /* namespace abstraction */
