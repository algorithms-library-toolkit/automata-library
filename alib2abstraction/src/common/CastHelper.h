#pragma once

#include <abstraction/TemporariesHolder.h>
#include <abstraction/Value.hpp>
#include <memory>
#include <string>

namespace abstraction {

class CastHelper {
public:
    static std::shared_ptr<abstraction::Value> eval(abstraction::TemporariesHolder& environment, std::shared_ptr<abstraction::Value> param, const core::type_details& type);
};

} /* namespace abstraction */
