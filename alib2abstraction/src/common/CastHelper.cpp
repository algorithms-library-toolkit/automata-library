#include <common/CastHelper.h>

#include <exception>
#include <registry/Registry.h>

namespace abstraction {

std::shared_ptr<abstraction::Value> CastHelper::eval(abstraction::TemporariesHolder& environment, std::shared_ptr<abstraction::Value> param, const core::type_details& type)
{
    if (abstraction::Registry::isCastNoOp(param->getActualType(), type)) {
        return param;
    }

    std::unique_ptr<abstraction::OperationAbstraction> res = abstraction::Registry::getCastAbstraction(type, param->getActualType());

    if (abstraction::Registry::hasDenormalize(res->getParamType(0)) && res->getParamType(0) != param->getDeclaredType()) {
        std::unique_ptr<abstraction::OperationAbstraction> denormalize = abstraction::Registry::getDenormalizeAbstraction(res->getParamType(0));
        denormalize->attachInput(param, 0);
        param = denormalize->eval();
        if (!param)
            throw std::invalid_argument("Internal error in denormalization.");
        environment.holdTemporary(param);
    }

    res->attachInput(param, 0);

    std::shared_ptr<abstraction::Value> result = res->eval();
    if (!result)
        throw std::invalid_argument("Eval of cast to " + ext::to_string(type) + " failed.");

    if (abstraction::Registry::hasNormalize(result->getDeclaredType())) {
        std::unique_ptr<abstraction::OperationAbstraction> normalize = abstraction::Registry::getNormalizeAbstraction(result->getDeclaredType());
        normalize->attachInput(result, 0);
        result = normalize->eval();
        if (!result)
            throw std::invalid_argument("Eval of normalize of cast to " + ext::to_string(type) + " failed.");
    }

    environment.holdTemporary(result);

    return result;
}

} /* namespace abstraction */
