#include <common/EvalHelper.h>

#include <common/CastHelper.h>
#include <exception>
#include <registry/Registry.h>

namespace abstraction {

std::shared_ptr<abstraction::Value> EvalHelper::evalAlgorithm(abstraction::TemporariesHolder& environment, const std::string& name, const ext::vector<std::string>& templateParams, const ext::vector<std::shared_ptr<abstraction::Value>>& params, abstraction::AlgorithmCategories::AlgorithmCategory category)
{
    ext::vector<core::type_details> paramTypes;
    ext::vector<abstraction::TypeQualifiers::TypeQualifierSet> paramTypeQualifiers;
    for (const std::shared_ptr<abstraction::Value>& param : params) {
        paramTypes.push_back(param->getActualType());
        paramTypeQualifiers.push_back(param->getTypeQualifiers());
    }

    try {
        return evalAbstraction(environment, abstraction::Registry::getAlgorithmAbstraction(name, templateParams, paramTypes, paramTypeQualifiers, category), params);
    } catch (const std::exception& executionException) {
        std::throw_with_nested(std::runtime_error("Evaluation of algorithm " + name + " failed."));
    }
}

std::shared_ptr<abstraction::Value> EvalHelper::evalOperator(abstraction::TemporariesHolder& environment, abstraction::Operators::BinaryOperators type, const ext::vector<std::shared_ptr<abstraction::Value>>& params, abstraction::AlgorithmCategories::AlgorithmCategory category)
{
    ext::vector<core::type_details> paramTypes;
    ext::vector<abstraction::TypeQualifiers::TypeQualifierSet> paramTypeQualifiers;
    for (const std::shared_ptr<abstraction::Value>& param : params) {
        paramTypes.push_back(param->getActualType());
        paramTypeQualifiers.push_back(param->getTypeQualifiers());
    }

    try {
        return evalAbstraction(environment, abstraction::Registry::getBinaryOperatorAbstraction(type, paramTypes, paramTypeQualifiers, category), params);
    } catch (...) {
        std::throw_with_nested(std::runtime_error("Evaluation of binary operator " + abstraction::Operators::toString(type) + " failed."));
    }
}

std::shared_ptr<abstraction::Value> EvalHelper::evalOperator(abstraction::TemporariesHolder& environment, abstraction::Operators::PrefixOperators type, const ext::vector<std::shared_ptr<abstraction::Value>>& params, abstraction::AlgorithmCategories::AlgorithmCategory category)
{
    ext::vector<core::type_details> paramTypes;
    ext::vector<abstraction::TypeQualifiers::TypeQualifierSet> paramTypeQualifiers;
    for (const std::shared_ptr<abstraction::Value>& param : params) {
        paramTypes.push_back(param->getActualType());
        paramTypeQualifiers.push_back(param->getTypeQualifiers());
    }

    try {
        return evalAbstraction(environment, abstraction::Registry::getPrefixOperatorAbstraction(type, paramTypes, paramTypeQualifiers, category), params);
    } catch (...) {
        std::throw_with_nested(std::runtime_error("Evaluation of prefix operator " + abstraction::Operators::toString(type) + " failed."));
    }
}

std::shared_ptr<abstraction::Value> EvalHelper::evalOperator(abstraction::TemporariesHolder& environment, abstraction::Operators::PostfixOperators type, const ext::vector<std::shared_ptr<abstraction::Value>>& params, abstraction::AlgorithmCategories::AlgorithmCategory category)
{
    ext::vector<core::type_details> paramTypes;
    ext::vector<abstraction::TypeQualifiers::TypeQualifierSet> paramTypeQualifiers;
    for (const std::shared_ptr<abstraction::Value>& param : params) {
        paramTypes.push_back(param->getActualType());
        paramTypeQualifiers.push_back(param->getTypeQualifiers());
    }

    try {
        return evalAbstraction(environment, abstraction::Registry::getPostfixOperatorAbstraction(type, paramTypes, paramTypeQualifiers, category), params);
    } catch (...) {
        std::throw_with_nested(std::runtime_error("Evaluation of postfix operator " + abstraction::Operators::toString(type) + " failed."));
    }
}

std::shared_ptr<abstraction::Value> EvalHelper::evalAbstraction(abstraction::TemporariesHolder& environment, std::unique_ptr<abstraction::OperationAbstraction> abstraction, const ext::vector<std::shared_ptr<abstraction::Value>>& params)
{
    unsigned i = 0;
    ext::vector<std::shared_ptr<abstraction::Value>> casted_params;
    for (std::shared_ptr<abstraction::Value> param : params) {
        param = abstraction::CastHelper::eval(environment, param, abstraction->getParamType(i));

        if (abstraction::Registry::hasDenormalize(abstraction->getParamType(i)) && abstraction->getParamType(i) != param->getDeclaredType()) {
            std::unique_ptr<abstraction::OperationAbstraction> denormalize = abstraction::Registry::getDenormalizeAbstraction(abstraction->getParamType(i));
            denormalize->attachInput(param, 0);
            param = denormalize->eval();
            if (!param)
                throw std::invalid_argument("Internal error in denormalization.");
            environment.holdTemporary(param);
        }

        casted_params.push_back(param);
        ++i;
    }

    i = 0;
    for (const std::shared_ptr<abstraction::Value>& param : casted_params) {
        abstraction->attachInput(param, i);
        ++i;
    }

    std::shared_ptr<abstraction::Value> result = abstraction->eval();
    if (!result)
        throw std::invalid_argument("Internal error in abstraction evaluation.");

    if (abstraction::Registry::hasNormalize(result->getDeclaredType()) && !abstraction::TypeQualifiers::isRef(result->getTypeQualifiers())) {
        std::unique_ptr<abstraction::OperationAbstraction> normalize = abstraction::Registry::getNormalizeAbstraction(result->getDeclaredType());
        normalize->attachInput(result, 0);
        result = normalize->eval();
        if (!result)
            throw std::invalid_argument("Internal error in normalization.");
    }

    environment.holdTemporary(result);

    return result;
}

} /* namespace abstraction */
