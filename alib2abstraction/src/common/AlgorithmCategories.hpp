#pragma once

#include <ext/string>

namespace abstraction {

class AlgorithmCategories {
public:
    enum class AlgorithmCategory {
        DEFAULT,
        TEST,
        STUDENT,
        EFFICIENT,
        NAIVE,
        NONE
    };

    static AlgorithmCategory algorithmCategory(std::string category);

    static std::string toString(AlgorithmCategory category);
};

std::ostream& operator<<(std::ostream& os, AlgorithmCategories::AlgorithmCategory category);

} /* namespace abstraction */
