#include <algorithm>
#include <exception>

#include <common/AlgorithmCategories.hpp>

namespace abstraction {

abstraction::AlgorithmCategories::AlgorithmCategory AlgorithmCategories::algorithmCategory(std::string category)
{
    std::transform(category.begin(), category.end(), category.begin(), ::tolower);

    if (category == "default")
        return AlgorithmCategory::DEFAULT;

    if (category == "test")
        return AlgorithmCategory::TEST;

    if (category == "student")
        return AlgorithmCategory::STUDENT;

    if (category == "efficient")
        return AlgorithmCategory::EFFICIENT;

    if (category == "naive")
        return AlgorithmCategory::NAIVE;

    return AlgorithmCategory::NONE;
}

std::string AlgorithmCategories::toString(abstraction::AlgorithmCategories::AlgorithmCategory category)
{
    switch (category) {
    case abstraction::AlgorithmCategories::AlgorithmCategory::DEFAULT:
        return "default";
    case abstraction::AlgorithmCategories::AlgorithmCategory::TEST:
        return "test";
    case abstraction::AlgorithmCategories::AlgorithmCategory::STUDENT:
        return "student";
    case abstraction::AlgorithmCategories::AlgorithmCategory::EFFICIENT:
        return "efficient";
    case abstraction::AlgorithmCategories::AlgorithmCategory::NAIVE:
        return "naive";
    case abstraction::AlgorithmCategories::AlgorithmCategory::NONE:
        return "none";
    }
    throw std::logic_error("Unhandled category.");
}

std::ostream& operator<<(std::ostream& os, AlgorithmCategories::AlgorithmCategory category)
{
    return os << AlgorithmCategories::toString(category);
}

} /* namespace abstraction */
