#include <catch2/catch.hpp>

#include "factory/StringDataFactory.hpp"
#include "grammar/ContextFree/CFG.h"
#include "grammar/parsing/AbsorbTerminalSymbol.h"

#include <alib/pair>

TEST_CASE("Absorb Terminal Symbol", "[unit][grammar]")
{
    SECTION("Test 1")
    {
        DefaultSymbolType A = object::ObjectFactory<>::construct("A");
        DefaultSymbolType B = object::ObjectFactory<>::construct("B");
        DefaultSymbolType C = object::ObjectFactory<>::construct("C");

        DefaultSymbolType a = object::ObjectFactory<>::construct('a');
        DefaultSymbolType b = object::ObjectFactory<>::construct('b');
        DefaultSymbolType c = object::ObjectFactory<>::construct('c');

        DefaultSymbolType Ba = object::ObjectFactory<>::construct(ext::make_pair(B, a));

        grammar::CFG<> grammar(A);

        grammar.setTerminalAlphabet({a, b, c});
        grammar.setNonterminalAlphabet({A, B, C});
        grammar.setInitialSymbol(A);

        grammar.addRule(A, {B, a, C});
        grammar.addRule(B, {});
        grammar.addRule(B, {a, a, C});
        grammar.addRule(C, {c});
        grammar.addRule(C, {b, C});

        grammar::CFG<> res = grammar;
        grammar::parsing::AbsorbTerminalSymbol::absorbTerminalSymbol(res, a, {B});

        grammar::CFG<> comp(A);

        comp.setTerminalAlphabet({a, b, c});
        comp.setNonterminalAlphabet({A, B, Ba, C});
        comp.setInitialSymbol(A);

        comp.addRule(A, {Ba, C});
        comp.addRule(B, {});
        comp.addRule(B, {a, a, C});
        comp.addRule(Ba, {a});
        comp.addRule(Ba, {a, a, C, a});
        comp.addRule(C, {c});
        comp.addRule(C, {b, C});

        // INFO ( alib::StringDataFactory::toString ( grammar::Grammar ( res ) ) << std::endl << alib::StringDataFactory::toString ( grammar::Grammar ( comp ) ) );

        CHECK(res == comp);
    }

    SECTION("Test 2")
    {
        DefaultSymbolType A = object::ObjectFactory<>::construct("A");
        DefaultSymbolType B = object::ObjectFactory<>::construct("B");
        DefaultSymbolType C = object::ObjectFactory<>::construct("C");
        DefaultSymbolType X = object::ObjectFactory<>::construct("X");

        DefaultSymbolType a = object::ObjectFactory<>::construct('a');
        DefaultSymbolType b = object::ObjectFactory<>::construct('b');
        DefaultSymbolType c = object::ObjectFactory<>::construct('c');

        DefaultSymbolType Ba = object::ObjectFactory<>::construct(ext::make_pair(B, a));
        DefaultSymbolType Xa = object::ObjectFactory<>::construct(ext::make_pair(X, a));

        grammar::CFG<> grammar(A);

        grammar.setTerminalAlphabet({a, b, c});
        grammar.setNonterminalAlphabet({A, B, C, X});
        grammar.setInitialSymbol(A);

        grammar.addRule(A, {X, a, C});
        grammar.addRule(X, {c, B});
        grammar.addRule(B, {});
        grammar.addRule(B, {a, a, C});
        grammar.addRule(C, {c});
        grammar.addRule(C, {b, C});

        grammar::CFG<> res = grammar;
        grammar::parsing::AbsorbTerminalSymbol::absorbTerminalSymbol(res, a, {B, X});

        grammar::CFG<> comp(A);

        comp.setTerminalAlphabet({a, b, c});
        comp.setNonterminalAlphabet({A, B, Ba, C, X, Xa});
        comp.setInitialSymbol(A);

        comp.addRule(A, {Xa, C});
        comp.addRule(X, {c, B});
        comp.addRule(Xa, {c, Ba});
        comp.addRule(B, {});
        comp.addRule(B, {a, a, C});
        comp.addRule(Ba, {a});
        comp.addRule(Ba, {a, a, C, a});
        comp.addRule(C, {c});
        comp.addRule(C, {b, C});

        // INFO ( "res " << alib::StringDataFactory::toString ( grammar::Grammar ( res ) ) << std::endl << "comp " << alib::StringDataFactory::toString ( grammar::Grammar ( comp ) ) );

        CHECK(res == comp);
    }

    SECTION("Test 3")
    {
        DefaultSymbolType A = object::ObjectFactory<>::construct("A");
        DefaultSymbolType B = object::ObjectFactory<>::construct("B");
        DefaultSymbolType C = object::ObjectFactory<>::construct("C");

        DefaultSymbolType a = object::ObjectFactory<>::construct('a');
        DefaultSymbolType b = object::ObjectFactory<>::construct('b');
        DefaultSymbolType c = object::ObjectFactory<>::construct('c');

        DefaultSymbolType Ba = object::ObjectFactory<>::construct(ext::make_pair(B, a));

        grammar::CFG<> grammar(A);

        grammar.setTerminalAlphabet({a, b, c});
        grammar.setNonterminalAlphabet({A, B, C});
        grammar.setInitialSymbol(A);

        grammar.addRule(A, {B, a, C});
        grammar.addRule(B, {});
        grammar.addRule(B, {a, a, B});
        grammar.addRule(C, {c});
        grammar.addRule(C, {b, C});

        grammar::CFG<> res = grammar;
        grammar::parsing::AbsorbTerminalSymbol::absorbTerminalSymbol(res, a, {B});

        grammar::CFG<> comp(A);

        comp.setTerminalAlphabet({a, b, c});
        comp.setNonterminalAlphabet({A, B, Ba, C});
        comp.setInitialSymbol(A);

        comp.addRule(A, {Ba, C});
        comp.addRule(B, {});
        comp.addRule(B, {a, a, B});
        comp.addRule(Ba, {a});
        comp.addRule(Ba, {a, a, Ba});
        comp.addRule(C, {c});
        comp.addRule(C, {b, C});

        // INFO ( alib::StringDataFactory::toString ( grammar::Grammar ( res ) ) << std::endl << alib::StringDataFactory::toString ( grammar::Grammar ( comp ) ) );

        CHECK(res == comp);
    }
}
