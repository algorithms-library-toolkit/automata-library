#pragma once

#include <indexes/stringology/SuffixTrieTerminatingSymbol.h>
#include <string/LinearStringTerminatingSymbol.h>

namespace stringology {

namespace indexing {

/**
 * Constructs suffix trie for given string.
 *
 * Source: Lectures MI-EVY (CTU in Prague), Year 2014, Lecture 3, slide 4
 */

class ExperimentalSuffixTrie {
public:
    /**
     * Creates suffix trie
     * @param string string to construct suffix trie for
     * @return automaton
     */
    static indexes::SuffixTrieTerminatingSymbol construct(const string::LinearStringTerminatingSymbol& w);
};

} /* namespace indexing */

} /* namespace stringology */
