#include <registration/AlgoRegistration.hpp>
#include "ExperimentalCompactSuffixAutomatonConstruct.h"

namespace {

auto CompactSuffixAutomatonTerminatingSymbolLinearStringTerminatingSymbol = registration::AbstractRegister<stringology::indexing::ExperimentalCompactSuffixAutomatonConstruct, indexes::stringology::CompactSuffixAutomatonTerminatingSymbol<>, const string::LinearStringTerminatingSymbol&>(stringology::indexing::ExperimentalCompactSuffixAutomatonConstruct::construct);

auto CompactSuffixAutomatonTerminatingSymbolLinearString = registration::AbstractRegister<stringology::indexing::ExperimentalCompactSuffixAutomatonConstruct, indexes::stringology::CompactSuffixAutomatonTerminatingSymbol<>, const string::LinearString<>&>(stringology::indexing::ExperimentalCompactSuffixAutomatonConstruct::construct);

} /* namespace */
