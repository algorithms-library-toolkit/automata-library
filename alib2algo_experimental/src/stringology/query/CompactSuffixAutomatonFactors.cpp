#include <registration/AlgoRegistration.hpp>
#include "CompactSuffixAutomatonFactors.h"

namespace {

auto CompactSuffixAutomatonQueryLinearString = registration::AbstractRegister<stringology::query::CompactSuffixAutomatonFactors, ext::set<unsigned>, const indexes::stringology::CompactSuffixAutomatonTerminatingSymbol<>&, const string::LinearString<>&>(stringology::query::CompactSuffixAutomatonFactors::query);

} /* namespace */
