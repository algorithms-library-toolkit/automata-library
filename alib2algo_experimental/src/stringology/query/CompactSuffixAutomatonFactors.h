#include <indexes/stringology/CompactSuffixAutomatonTerminatingSymbol.h>
#include <string/LinearString.h>

namespace stringology {

namespace query {

class CompactSuffixAutomatonFactors {
public:
    template <class SymbolType>
    static ext::set<unsigned> query(const indexes::stringology::CompactSuffixAutomatonTerminatingSymbol<SymbolType>& a, const string::LinearString<SymbolType>& pattern)
    {
        try {
            unsigned curState = 0;
            unsigned index = 0;

            while (index < pattern.getContent().size()) {
                curState = a.GetNextState(curState, pattern.getContent(), index);
            }
            ext::set<unsigned> res;
            a.GetAllPathsLen(curState, index, res);

            return res;
        } catch (...) {
            return {};
        }
    }
};

} /* namespace query */

} /* namespace stringology */
