#include <grammar/ContextFree/CFG.h>
#include "Substitute.h"

namespace grammar::parsing {

void Substitute::substitute(const grammar::CFG<>& orig, grammar::CFG<>& res, const DefaultSymbolType& origLHS, const ext::vector<ext::variant<DefaultSymbolType, DefaultSymbolType>>& origRHS, ext::vector<ext::variant<DefaultSymbolType, DefaultSymbolType>>::const_iterator nonterminal)
{
    auto iter = orig.getRules().find(*nonterminal);

    if (iter == orig.getRules().end())
        return;

    for (const ext::vector<ext::variant<DefaultSymbolType, DefaultSymbolType>>& rhs : iter->second) {
        ext::vector<ext::variant<DefaultSymbolType, DefaultSymbolType>> newRHS(origRHS.begin(), nonterminal);
        newRHS.insert(newRHS.end(), rhs.begin(), rhs.end());
        newRHS.insert(newRHS.end(), nonterminal + 1, origRHS.end());
        res.addRule(origLHS, newRHS);
    }
}

} /* namespace grammar::parsing */
