#pragma once

#include <alib/vector>
#include <grammar/ContextFree/CFG.h>

namespace grammar {

namespace parsing {

class Substitute {
public:
    static void substitute(const grammar::CFG<>& orig, grammar::CFG<>& res, const DefaultSymbolType& origLHS, const ext::vector<ext::variant<DefaultSymbolType, DefaultSymbolType>>& origRHS, ext::vector<ext::variant<DefaultSymbolType, DefaultSymbolType>>::const_iterator nonterminal);
};

} /* namespace parsing */

} /* namespace grammar */
