#pragma once

#include "LRParser.h"

#include <grammar/ContextFree/CFG.h>
#include <grammar/parsing/LRParserTypes.h>

namespace grammar {

namespace parsing {

class SLR1ParseTable {
    static void insertToActionTable(LRActionTable& actionTable, LRActionTable::key_type key, LRActionTable::mapped_type value);

public:
    static LRActionTable getActionTable(const grammar::CFG<>& originalGrammar);

    static LRGotoTable getGotoTable(const grammar::CFG<>& originalGrammar);
};

} /* namespace parsing */

} /* namespace grammar */
