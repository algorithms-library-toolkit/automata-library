#pragma once

#include <alib/set>
#include <grammar/ContextFree/CFG.h>

namespace grammar {

namespace parsing {

class ExtractRightContext {
public:
    static void extractRightContext(grammar::CFG<>& grammar, const DefaultSymbolType& terminal, const ext::set<DefaultSymbolType>& nonterminals);
};

} /* namespace parsing */

} /* namespace grammar */
