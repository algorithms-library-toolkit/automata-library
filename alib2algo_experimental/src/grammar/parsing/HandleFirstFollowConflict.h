#pragma once

#include <alib/set>
#include <alib/vector>
#include <grammar/ContextFree/CFG.h>

namespace grammar {

namespace parsing {

class HandleFirstFollowConflict {
public:
    static void handleFirstFollowConflict(grammar::CFG<>& grammar, const DefaultSymbolType& terminal, const DefaultSymbolType& nonterminal, const ext::set<ext::vector<ext::variant<DefaultSymbolType, DefaultSymbolType>>>& /* rhsds */);
};

} /* namespace parsing */

} /* namespace grammar */
