#pragma once

#include <alib/map>
#include <alib/set>
#include <grammar/ContextFree/CFG.h>

namespace grammar {

namespace parsing {

class AbsorbTerminalSymbol {
public:
    static void handleAbsobtion(const grammar::CFG<>& orig, grammar::CFG<>& res, const DefaultSymbolType& terminal, const ext::set<DefaultSymbolType>& nonterminals, const ext::map<DefaultSymbolType, DefaultSymbolType>& nonterminalsPrimed);

    static void absorbTerminalSymbol(grammar::CFG<>& grammar, const DefaultSymbolType& terminal, const ext::set<DefaultSymbolType>& nonterminals);
};

} /* namespace parsing */

} /* namespace grammar */
