#pragma once

#include <alib/map>
#include <alib/set>
#include <alib/vector>

#include <common/DefaultSymbolType.h>

namespace grammar {

namespace parsing {

class DeterministicLL1ParseTable {
public:
    static ext::map<ext::pair<ext::vector<DefaultSymbolType>, DefaultSymbolType>, ext::vector<DefaultSymbolType>> parseTable(const ext::map<ext::pair<ext::vector<DefaultSymbolType>, DefaultSymbolType>, ext::set<ext::vector<DefaultSymbolType>>>& parseTable);
};

} /* namespace parsing */

} /* namespace grammar */
