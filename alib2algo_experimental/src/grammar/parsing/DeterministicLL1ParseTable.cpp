#include "DeterministicLL1ParseTable.h"

#include <exception/CommonException.h>

namespace grammar::parsing {

ext::map<ext::pair<ext::vector<DefaultSymbolType>, DefaultSymbolType>, ext::vector<DefaultSymbolType>> DeterministicLL1ParseTable::parseTable(const ext::map<ext::pair<ext::vector<DefaultSymbolType>, DefaultSymbolType>, ext::set<ext::vector<DefaultSymbolType>>>& parseTable)
{

    auto isEpsilon = [](const std::pair<const ext::pair<ext::vector<DefaultSymbolType>, DefaultSymbolType>, ext::set<ext::vector<DefaultSymbolType>>>& entry) {
        return entry.second.size() > 1;
    };

    if (std::none_of(parseTable.begin(), parseTable.end(), isEpsilon))
        throw exception::CommonException("Cant handle conflict in epsilon");

    ext::map<ext::pair<ext::vector<DefaultSymbolType>, DefaultSymbolType>, ext::vector<DefaultSymbolType>> res;

    for (const std::pair<const ext::pair<ext::vector<DefaultSymbolType>, DefaultSymbolType>, ext::set<ext::vector<DefaultSymbolType>>>& elem : parseTable)
        if (elem.second.size() == 1)
            res.insert(std::make_pair(elem.first, *elem.second.begin()));

    return res;
}

} /* namespace grammar::parsing */
