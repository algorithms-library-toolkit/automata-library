Name:             algorithms-library-snapshot
Url:              https://alt.fit.cvut.cz
Version:          {{ version }}
Release:          {{ release }}%{?dist}
Summary:          Stringology and arbology algorithms toolkit

License:          GPL 3.0
Source0:          algorithms-library-v%{version}.tar.gz

Requires:         libxml2
Requires:         readline
BuildRequires:    cmake
BuildRequires:    libxml2-devel
BuildRequires:    readline-devel

{%- if distro.match('fedora') %}
BuildRequires:    gcc-c++
{%- elif distro.match('opensuse') %}
BuildRequires:    gcc11-c++
{%- endif %}

%description
Theoretical computer science datastructures and algorithms implementation. The library provides graphical and console interface similar to bash with many classical algorithms manipulating automata, grammars, regular expressions, trees and strings.

%package devel
Summary:        Development headers for algorithms-library-snapshot.
Requires:       %{name} = %{version}-%{release}

%description devel
Development headers for algorithms-library-snapshot.

%prep
%autosetup -n algorithms-library-v%{version} -p1

%build
%cmake -DCMAKE_BUILD_TYPE=Release -DBUILD_TESTING=OFF
%cmake_build

%install
%cmake_install

%files
%doc README.md
%{_bindir}/aql2
%{_libdir}/libalib*
%{_datadir}/algorithms-library

%files devel
%{_includedir}/algorithms-library

%changelog
* {{ now }} Tomáš Pecka <peckato1@fit.cvut.cz> - {{ version }}-{{ release }}
- new upstream version {{ version }}
