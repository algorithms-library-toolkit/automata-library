#include <catch2/catch.hpp>

#include <ext/iostream>

#include <common/EvalHelper.h>
#include <environment/Environment.h>

#include <readline/StringLineInterface.h>
#include <registry/AlgorithmRegistry.hpp>

TEST_CASE("Udf eval", "[unit][cli]")
{

    SECTION("Test Udf")
    {
        cli::Environment environment;

        environment.execute("function mojeFunkce ( const auto & $mojepromenna ) returning auto begin\n"
                            "return $mojepromenna;\n"
                            "end");

        auto input = std::make_shared<abstraction::ValueHolder<std::string>>(std::string{"neco"}, true);

        auto compose1 = abstraction::EvalHelper::evalAlgorithm(environment, "string::Compose", {}, {input}, abstraction::AlgorithmCategories::AlgorithmCategory::NONE);

        // commenting next line causes the code NOT to crash
        auto mojeFunk = abstraction::EvalHelper::evalAlgorithm(environment, "mojeFunkce", {}, {compose1}, abstraction::AlgorithmCategories::AlgorithmCategory::NONE);

        // This is enough to crash, you don't have to call the following evalAlgorithm
        auto X = compose1->getActualType();
    }
}
