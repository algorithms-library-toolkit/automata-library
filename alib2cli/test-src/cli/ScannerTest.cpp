#include <catch2/catch.hpp>
#include <grammar/completion/Scanner.h>

#include <AltCliLexer.h>

using namespace cli::grammar;

TEST_CASE("Scanner Test")
{
    SECTION("Basic parse")
    {
        std::istringstream iss("print exp");
        antlr4::ANTLRInputStream inputStream{iss};
        lexer::AltCliLexer lexer{&inputStream};
        antlr4::CommonTokenStream tokenStream(&lexer);
        lexer.removeErrorListeners();

        completion::Scanner scanner{&tokenStream};
        while (scanner.next())
            ;
        scanner.previous();

        CHECK(scanner.tokenSubText() == "exp");
    }

    SECTION("Basic parse 2")
    {
        std::istringstream iss("print");
        antlr4::ANTLRInputStream inputStream{iss};
        lexer::AltCliLexer lexer{&inputStream};
        antlr4::CommonTokenStream tokenStream(&lexer);
        lexer.removeErrorListeners();

        completion::Scanner scanner{&tokenStream};
        while (scanner.next())
            ;
        scanner.previous();

        CHECK(scanner.tokenSubText() == "print");
    }
}
