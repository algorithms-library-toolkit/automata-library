#include <catch2/catch.hpp>
#include <grammar/Autocomplete.h>

TEST_CASE("Autocomplete")
{
    SECTION("Empty")
    {
        cli::Environment env;
        cli::Autocomplete autocomplete{env};

        auto suggestions = autocomplete.getSuggestions("", "");

        CHECK(suggestions.size() == 22);
    }

    SECTION("pri")
    {
        cli::Environment env;
        cli::Autocomplete autocomplete{env};

        auto suggestions = autocomplete.getSuggestions("pri", "pri");

        CHECK(suggestions.size() == 1);
    }

    SECTION("print ")
    {
        cli::Environment env;
        cli::Autocomplete autocomplete{env};

        auto suggestions = autocomplete.getSuggestions("print ", "");

        // CHECK(suggestions.size() == 2);
    }

    SECTION("print e")
    {
        cli::Environment env;
        cli::Autocomplete autocomplete{env};

        auto suggestions = autocomplete.getSuggestions("print e", "e");

        CHECK(suggestions.size() == 1);
    }

    SECTION("print raw")
    {
        cli::Environment env;
        cli::Autocomplete autocomplete{env};

        auto suggestions = autocomplete.getSuggestions("print raw", "raw");

        // CHECK(suggestions.size() == 1);
    }
}