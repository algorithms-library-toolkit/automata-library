#include "TestLine.hpp"
#include "parser/Parser.h"

#include <readline/StringLineInterface.h>

#include <ext/exception>
#include <global/GlobalData.h>

void testLine(std::string line, cli::Environment& environment)
{
    try {
        cli::Parser::parseString(line)->run(environment);
    } catch (...) {
        alib::ExceptionHandler::handle(common::Streams::err);
        throw;
    }
}
