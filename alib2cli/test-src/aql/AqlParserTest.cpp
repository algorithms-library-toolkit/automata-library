#include <catch2/catch.hpp>
#include <configure_tests.hpp>
#include <extensions/exception/ExceptionHandler.hpp>
#include <filesystem>
#include <fstream>
#include <global/GlobalData.h>
#include <iostream>
#include <parser/Parser.h>
#include <regex>
#include <unordered_set>
using namespace std::literals;


std::unique_ptr<cli::CommandList> newParseFile(std::istream&& stream)
{
    try {
        return cli::Parser::parseStream(stream);
    } catch (...) {
        ext::ostringstream outErr;
        alib::ExceptionHandler::handle(outErr);
        // Shows only in case of test failure
        INFO(outErr.str());
        throw;
    }
}

std::string normalizeAst(const std::string& ast)
{
    std::regex r("(^|\n)([ \t]*\\()");
    // std::regex r("\\s+");
    return std::regex_replace(ast, r, "(");
}

void compareAst(const std::filesystem::path& file, const std::filesystem::path& astFile)
{
    std::ifstream ifs(file);
    REQUIRE(ifs.is_open());
    std::stringstream buffer;
    buffer << ifs.rdbuf();
    ifs.close();

    std::istringstream aqlFile(buffer.str());

    const auto& ast = newParseFile(std::move(aqlFile));
    std::ostringstream outAst;
    ast->print(outAst);
    std::string const textAst = outAst.str();

    std::ifstream astFileIn(astFile);
    REQUIRE(astFileIn.is_open());
    buffer.str("");
    buffer << astFileIn.rdbuf();

    CHECK((textAst) == normalizeAst(buffer.str()));
}


void testFolder(const std::filesystem::path& folder, bool checkAst)
{
    for (const auto& entry : std::filesystem::directory_iterator(folder)) {
        if (entry.is_regular_file()) {
            std::string fileName = entry.path().filename().string();

            DYNAMIC_SECTION("New Parse - " << fileName)
            {
                std::ifstream ifs(entry.path());
                REQUIRE(ifs.is_open());

                CHECK_NOTHROW(newParseFile(std::move(ifs)));
            }

            if (checkAst) {

                DYNAMIC_SECTION("Compare AST - " << fileName)
                {
                    auto astFile = folder / "ast"s / (fileName + ".ast"s);

                    bool astFileExists = std::filesystem::exists(astFile);
                    if (!astFileExists)
                        WARN("AST file for" << fileName << " does not exist. Expected to find file on path: " << astFile);
                    else
                        compareAst(entry.path(), astFile);
                }
            }
        }
    }
}


TEST_CASE("AQL parser tests", "[unit][cli][parser]")
{
    testFolder(std::filesystem::path(CMAKE_CURRENT_SOURCE_DIR) / "test-src/aql/basic"s, true);
    testFolder(std::filesystem::path(CMAKE_CURRENT_SOURCE_DIR) / "test-src/aql/tests"s, false);
}
