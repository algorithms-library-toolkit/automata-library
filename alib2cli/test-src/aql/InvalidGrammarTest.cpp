#include <catch2/catch.hpp>
#include <exception/CommonException.h>
#include <parser/Parser.h>

void newParseString(const std::string& str)
{
    std::istringstream iss(str);
    cli::Parser::parseStream(iss);
}

TEST_CASE("Invalid grammar - Scope")
{
    SECTION("Undeclare")
    {
        CHECK_THROWS_AS(newParseString("begin undeclare a (); end"), exception::CommonException);
        CHECK_NOTHROW(newParseString("undeclare a ()"));
    }

    SECTION("If")
    {
        CHECK_THROWS_AS(newParseString("if (1) then begin print 1; end"), exception::CommonException);
        CHECK_NOTHROW(newParseString("begin if (1) then print 1; end"));
    }


    SECTION("While")
    {
        CHECK_THROWS_AS(newParseString("while (1) do begin print 1; end"), exception::CommonException);
        CHECK_NOTHROW(newParseString("begin while (1) do print 1; end"));
    }

    SECTION("Break")
    {
        CHECK_THROWS_AS(newParseString("break"), exception::CommonException);
        CHECK_NOTHROW(newParseString("begin break; end"));
    }

    SECTION("Continue")
    {
        CHECK_THROWS_AS(newParseString("continue"), exception::CommonException);
        CHECK_NOTHROW(newParseString("begin continue; end"));
    }

    SECTION("Function")
    {
        CHECK_THROWS_AS(newParseString("begin function func () returning auto return 1; end"), exception::CommonException);
        CHECK_NOTHROW(newParseString("function func () returning auto return 1"));
    }

    SECTION("Procedure")
    {
        CHECK_THROWS_AS(newParseString("begin procedure func () print 1; end"), exception::CommonException);
        CHECK_NOTHROW(newParseString("procedure func () print 1"));
    }
}

TEST_CASE("Whitespace")
{
    SECTION("Two commands on same line")
    {
        CHECK_THROWS(newParseString("print 1 print 2"));
    }

    SECTION("Invalid print - 1")
    {
        CHECK_THROWS(newParseString("print\n"
                                    "a\n"
                                    "b"));
    }
}

TEST_CASE("Incomplete command")
{
    SECTION("introspect overload")
    {
        CHECK_THROWS(newParseString("introspect overloads"));
    }
}

TEST_CASE("Incomplete block")
{
    SECTION("basic begin")
    {
        CHECK_THROWS_AS(newParseString("begin"), cli::Parser::ContinueException);
    }
    SECTION("nested begin")
    {
        CHECK_THROWS_AS(newParseString("begin\nbegin\nendn"), cli::Parser::ContinueException);
    }
}


TEST_CASE("Incomplete string")
{
    SECTION("basic string")
    {
        CHECK_THROWS_AS(newParseString("print \""), cli::Parser::ContinueException);
    }
    SECTION("nested begin")
    {
        CHECK_THROWS_AS(newParseString("print IsSame \"test\" \"test"), cli::Parser::ContinueException);
    }
}
