#pragma once

#include <registry/InputFileRegistry.hpp>

namespace registration {

class InputFileRegister {
    std::string m_FileType;

public:
    InputFileRegister(std::string fileType, std::unique_ptr<abstraction::OperationAbstraction> (*callback)(const core::type_details& type))
        : m_FileType(std::move(fileType))
    {
        abstraction::InputFileRegistry::registerInputFileHandler(m_FileType, callback);
    }

    ~InputFileRegister()
    {
        abstraction::InputFileRegistry::unregisterInputFileHandler(m_FileType);
    }
};

} /* namespace registration */
