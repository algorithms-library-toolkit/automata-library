#include <ext/typeinfo>

#include "InputFileTypeRegistration.hpp"

#include <registry/XmlRegistry.h>

#include <abstraction/WrapperAbstraction.hpp>
#include <abstraction/XmlTokensParserAbstraction.hpp>

#include <registry/Registry.h>

namespace {

std::unique_ptr<abstraction::OperationAbstraction> dummy(const core::type_details&)
{
    ext::vector<std::string> templateParams;
    ext::vector<core::type_details> paramTypes{core::type_details::get<std::string>()};
    abstraction::AlgorithmCategories::AlgorithmCategory category = abstraction::AlgorithmCategories::AlgorithmCategory::NONE;
    ext::vector<abstraction::TypeQualifiers::TypeQualifierSet> paramTypeQualifiers{abstraction::TypeQualifiers::typeQualifiers<const std::string&>()};

    return abstraction::Registry::getAlgorithmAbstraction("xml::builtin::ReadFile", templateParams, paramTypes, paramTypeQualifiers, category);
}

std::unique_ptr<abstraction::OperationAbstraction> dummy2(const core::type_details&)
{
    ext::vector<std::string> templateParams;
    ext::vector<core::type_details> paramTypes{core::type_details::get<std::string>()};
    abstraction::AlgorithmCategories::AlgorithmCategory category = abstraction::AlgorithmCategories::AlgorithmCategory::NONE;
    ext::vector<abstraction::TypeQualifiers::TypeQualifierSet> paramTypeQualifiers{abstraction::TypeQualifiers::typeQualifiers<const std::string&>()};

    return abstraction::Registry::getAlgorithmAbstraction("cli::builtin::ReadFile", templateParams, paramTypes, paramTypeQualifiers, category);
}

auto xmlInputFileHandler = registration::InputFileRegister("xml", dummy);
auto fileInputFileHandler = registration::InputFileRegister("file", dummy2);

}
