#include "Autocomplete.h"

#include <grammar/completion/Scanner.h>
#include <limits>

using namespace cli::grammar::parser;
using namespace cli::grammar::lexer;

using namespace cli::grammar::completion;

std::vector<cli::Autocomplete::Suggestion> cli::Autocomplete::getSuggestions(const std::string& input, const std::string& context)
{

    antlr4::ANTLRInputStream inputStream{input};
    AltCliLexer lexer{&inputStream};
    antlr4::CommonTokenStream tokenStream(&lexer);
    AltCliParser parser(&tokenStream);
    antlr4::CodeCompletionCore completionCore{&parser};

    completionCore.ignoredTokens = {
        AltCliLexer::HASH_SIGN,
        AltCliLexer::AT_SIGN,
        AltCliLexer::LEFT_PAREN,
        AltCliLexer::RIGHT_PAREN,
        AltCliLexer::LEFT_BRACKET,
        AltCliLexer::RIGHT_BRACKET,
        AltCliLexer::LEFT_BRACE,
        AltCliLexer::RIGHT_BRACE,
        AltCliLexer::COLON_SIGN,
        AltCliLexer::DOLAR_SIGN,
        AltCliLexer::LESS_SIGN,
        AltCliLexer::MORE_SIGN,
        AltCliLexer::SEMICOLON,
        AltCliLexer::PIPE_SIGN,
        AltCliLexer::IMPL_SIGN,
        AltCliLexer::SLASH_SIGN,
        AltCliLexer::STAR_SIGN,
        AltCliLexer::PERCENTAGE_SIGN,
        AltCliLexer::PLUS_SIGN,
        AltCliLexer::MINUS_SIGN,
        AltCliLexer::TYLDE_SIGN,
        AltCliLexer::CARET_SIGN,
        AltCliLexer::AMPERSAND_SIGN,
        AltCliLexer::EXCLAMATION_SIGN,
        AltCliLexer::OR_OPERATOR,
        AltCliLexer::AND_OPERATOR,
        AltCliLexer::EQUAL_OPERATOR,
        AltCliLexer::NOT_EQUAL_OPERATOR,
        AltCliLexer::LESS_OR_EQUAL_OPERATOR,
        AltCliLexer::MORE_OR_EQUAL_OPERATOR,
        AltCliLexer::ASSIGN_OPERATOR,
        AltCliLexer::INC_OPERATOR,
        AltCliLexer::DEC_OPERATOR,
        AltCliLexer::COMMA,
        AltCliLexer::DOT,
        AltCliLexer::LESS_OPERATOR,
        AltCliLexer::MORE_OPERATOR,
        AltCliLexer::NEWLINE,
        AltCliLexer::STRING,
        AltCliLexer::INTEGER,
        AltCliLexer::DOUBLE,
        AltCliLexer::IDENTIFIER,
        AltCliLexer::EOF};

    completionCore.preferredRules = {
        AltCliParser::RuleVariable,
        AltCliParser::RuleBinding_name,
        AltCliParser::RuleBinding,
        AltCliParser::RuleAlgorithm,
        AltCliParser::RuleAlgorithm_group,
        AltCliParser::RuleDatatype_group};


    auto scanner = Scanner(&tokenStream);
    while (scanner.next())
        ;
    if (!context.empty())
        scanner.previous();

    size_t caretIndex = scanner.tokenIndex();
    if (caretIndex > 0 && !completionCore.ignoredTokens.contains(scanner.lookBack())) {
        --caretIndex;
    }

    lexer.removeErrorListeners();
    parser.removeErrorListeners();


    auto tree = parser.parse();
    auto candidates = completionCore.collectCandidates(caretIndex, tree);

    std::vector<Suggestion> data;

    for (const auto& candidate : candidates.tokens) {
        auto tokenName = lexer.getVocabulary().getDisplayName(candidate.first);

        if (tokenName[0] == '\'' && tokenName[tokenName.size() - 1] == '\'') {
            tokenName = tokenName.substr(1, tokenName.size() - 2);
        }
        if (context.empty() || tokenName.starts_with(context))
            data.emplace_back(Autocomplete::SuggestionKind::Keyword, tokenName);
    }

    for (const auto& candidate : candidates.rules) {
        switch (candidate.first) {
        case AltCliParser::RuleVariable:
            for (const auto& v : filter_completions(m_environment.getVariableNames(), context)) {
                data.emplace_back(Autocomplete::SuggestionKind::Variable, "$" + v);
            }
            break;
        case AltCliParser::RuleBinding_name:
            // Used only for introspect binding
            data.clear();
            for (const auto& v : filter_completions(m_environment.getBindingNames(), context)) {
                data.emplace_back(Autocomplete::SuggestionKind::Binding, v);
            }
            return data;

        case AltCliParser::RuleBinding:
            for (const auto& v : m_environment.getBindingNames()) {
                const auto binding = "#" + v;
                if (binding.starts_with(context))
                    data.emplace_back(Autocomplete::SuggestionKind::Binding, binding);
            }
            break;
        case AltCliParser::RuleAlgorithm_group:
            for (const auto& v : Autocomplete::fetchAlgorithmGroups(context)) {
                data.emplace_back(Autocomplete::SuggestionKind::Binding, v);
            }
            break;
        case AltCliParser::RuleDatatype_group:
            for (const auto& v : Autocomplete::fetchDatatypeGroups(context)) {
                data.emplace_back(Autocomplete::SuggestionKind::Binding, v);
            }
            break;
        case AltCliParser::RuleAlgorithm: {
            const auto fullyQualifiedName = Autocomplete::fetchAlgorithmsFullyQualifiedName(context);
            const auto lastSegmentName = Autocomplete::fetchAlgorithmsLastSegmentName(context);

            std::vector<std::string> algoNames;

            std::set_union(fullyQualifiedName.begin(), fullyQualifiedName.end(), lastSegmentName.begin(), lastSegmentName.end(), std::back_inserter(algoNames));

            for (const auto& algo : algoNames) {
                data.emplace_back(Autocomplete::SuggestionKind::Identifier, algo);
            }
            break;
        }
        default:
            auto tokenName = parser.getRuleNames()[candidate.first];
            data.emplace_back(Autocomplete::SuggestionKind::Other, tokenName);
            break;
        }
    }

    return data;
}
cli::Autocomplete::Autocomplete(cli::Environment& environment)
    : m_environment(environment)
{
}
std::set<std::string> cli::Autocomplete::fetchDatatypeGroups(const std::string& text)
{
    std::set<std::string> res;

    for (const std::string& dtt : abstraction::XmlRegistry::listDataTypes()) {
        std::set<std::string> groups = getGroups(dtt);
        res.insert(groups.begin(), groups.end());
    }

    return filter_completions(res, text);
}
std::set<std::string> cli::Autocomplete::fetchAlgorithmsFullyQualifiedName(const std::string& text)
{
    std::set<std::string> fullyQualifiedNames;

    for (const ext::pair<std::string, ext::vector<std::string>>& algo : abstraction::Registry::listAlgorithms()) {
        fullyQualifiedNames.insert(algo.first);
    }

    return filter_completions(fullyQualifiedNames, text);
}
std::set<std::string> cli::Autocomplete::fetchAlgorithmsLastSegmentName(const std::string& text)
{
    std::map<std::string, unsigned> collisions;

    for (const ext::pair<std::string, ext::vector<std::string>>& algo : abstraction::Registry::listAlgorithms()) {
        size_t pos = algo.first.find_last_of(':');
        if (pos != std::string::npos)
            collisions[algo.first.substr(pos + 1)] += 1;
    }

    std::set<std::string> res;
    for (const std::pair<const std::string, unsigned>& kv : collisions)
        if (kv.second == 1)
            res.insert(kv.first);

    return filter_completions(res, text);
}
std::set<std::string> cli::Autocomplete::fetchAlgorithmGroups(const std::string& text)
{
    std::set<std::string> res;

    for (const ext::pair<std::string, ext::vector<std::string>>& algo : abstraction::Registry::listAlgorithms()) {
        std::set<std::string> groups = getGroups(algo.first);
        res.insert(groups.begin(), groups.end());
    }

    return filter_completions(res, text);
}
std::set<std::string> cli::Autocomplete::getGroups(const std::string& qualified_name)
{
    std::set<std::string> res;

    unsigned template_level = 0;
    for (size_t i = 0; i < qualified_name.size(); i++) {
        if (qualified_name[i] == '<')
            template_level++;
        else if (qualified_name[i] == '>')
            template_level--;

        else if (template_level == 0 && i > 0 && qualified_name[i - 1] == ':' && qualified_name[i] == ':')
            res.insert(qualified_name.substr(0, i + 1));
    }

    return res;
}
std::set<std::string> cli::Autocomplete::filter_completions(const std::set<std::string>& choices, const std::string& text)
{
    std::pair<std::set<std::string>::const_iterator, std::set<std::string>::const_iterator> range;
    const std::string prefix = text;

    range = std::equal_range(choices.begin(), choices.end(), prefix, [&prefix](const std::string& a, const std::string& b) { return strncmp(a.c_str(), b.c_str(), prefix.size()) < 0; });

    return std::set<std::string>(range.first, range.second);
}
