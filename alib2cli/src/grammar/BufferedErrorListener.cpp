#include <AltCliLexer.h>
#include <ostream>
#include "BufferedErrorListener.h"

namespace cli::grammar {
void BufferedErrorListener::syntaxError(antlr4::Recognizer* r, antlr4::Token* token, size_t line, size_t charPositionInLine, const std::string& msg, std::exception_ptr)
{
    if (token == nullptr) {
        auto* lexer = dynamic_cast<cli::grammar::lexer::AltCliLexer*>(r);

        if (lexer != nullptr) {
            auto startOfToken = lexer->getText();
            if (startOfToken.starts_with("\"")) {
                messages.emplace_back(INCOMPLETE_STRING);
                return;
            }
        }
    }
    std::ostringstream iss;

    iss << "Line: " << line << ", Position: " << charPositionInLine << std::endl;
    iss << "Message: " << msg << std::endl
        << std::endl;

    messages.emplace_back(iss.str());
}


std::string BufferedErrorListener::getErrors() const
{
    std::ostringstream iss;

    for (const auto& msg : messages) {
        iss << msg << std::endl;
    }

    return iss.str();
}

bool BufferedErrorListener::any() const
{
    return !messages.empty();
}

bool BufferedErrorListener::isMissingClosingQuote() const
{
    return any() && messages[0] == INCOMPLETE_STRING;
}
} // cli
