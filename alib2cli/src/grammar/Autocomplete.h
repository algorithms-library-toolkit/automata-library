#pragma once

#include <algorithm>
#include <antlr4-runtime.h>

#include <AltCliLexer.h>
#include <AltCliParser.h>
#include <environment/Environment.h>
#include <grammar/completion/CodeCompletionCore.h>
#include <registry/XmlRegistry.h>
#include <unordered_set>

namespace cli {
using namespace cli::grammar::lexer;
class Autocomplete {
    static std::set<std::string> fetchDatatypeGroups(const std::string& text);

    static std::set<std::string> fetchAlgorithmsFullyQualifiedName(const std::string& text);

    static std::set<std::string> fetchAlgorithmsLastSegmentName(const std::string& text);

    static std::set<std::string> fetchAlgorithmGroups(const std::string& text);

    static std::set<std::string> getGroups(const std::string& qualified_name);

    static std::set<std::string> filter_completions(const std::set<std::string>& choices, const std::string& text);

    cli::Environment& m_environment;

public:
    explicit Autocomplete(Environment& environment);

    enum SuggestionKind {
        Variable,
        Binding,
        Identifier,
        Keyword,
        Other
    };

    typedef std::pair<cli::Autocomplete::SuggestionKind, std::string> Suggestion;

    std::vector<Suggestion> getSuggestions(const std::string& input, const std::string& context);
};
}
