#pragma once

#include <antlr4-runtime.h>

namespace cli::grammar {

class BufferedErrorListener : public antlr4::BaseErrorListener {
    std::vector<std::string> messages;

    void syntaxError(antlr4::Recognizer* recognizer, antlr4::Token* offendingSymbol, size_t line, size_t charPositionInLine, const std::string& msg, std::exception_ptr e) override;
    static inline char INCOMPLETE_STRING[] = "Missing closing quote";

public:
    std::string getErrors() const;
    bool any() const;

    bool isMissingClosingQuote() const;
};

} // cli
