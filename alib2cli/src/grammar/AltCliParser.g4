parser grammar AltCliParser;
options {
    tokenVocab = AltCliLexer;
}

// Initial rule
parse
    : NEWLINE* top_level_command (NEWLINE top_level_command?)* EOF
    | NEWLINE* EOF;

arg
    : binding
    | identifier
    ;

template_arg
    : AT_SIGN arg
    ;

file
    : binding
    | string
    ;

type
    : string
    | LEFT_PAREN type RIGHT_PAREN
    | identifier
    ;

in_redirect
    : LEFT_PAREN statement_list RIGHT_PAREN # StatementInRedirect
    | (LEFT_BRACKET arg RIGHT_BRACKET)? type_option? file # FileInRedirect
    ;

out_redirect
    : variable # ResultVariableStatement
    | (LEFT_BRACKET arg RIGHT_BRACKET)? file # ResultFileStatement
    ;

common
    : variable # VariableStatement
    | LESS_SIGN in_redirect # InRedirectStatement
    | string # StringImmediateStatement
    | INTEGER # IntegerImmediateStatement
    | DOUBLE # DoubleImmediateStatement
    | binding # ValueStatement
    | LEFT_BRACE param* RIGHT_BRACE # ContainerStatement
    ;

param
    : common # CommonParam
    | MINUS_SIGN # PreviousResultParam
    | identifier # ImmediateParam
    | LEFT_PAREN type RIGHT_PAREN param # CastParam
    ;

statement
    : common # CommonStatement
    | algorithm template_arg* category_option? (param)* # SingleStatement
    | LEFT_PAREN type RIGHT_PAREN statement # CastStatement
    ;

statement_list
    : statement statement_list_optional* # StatementList
    ;

// TODO: Is it ok?
statement_list_optional
    : PIPE_SIGN statement # StatementListPipe
    | MORE_SIGN out_redirect # StatementListRedirect
    ;

block
    : BEGIN semicolon_command* END
    ;

semicolon_command
    : block # BlockSemicolonCommand
    | command_if # IfSemicolonCommand
    | command_while # WhileSemicolonCommand
    | nested_level_command SEMICOLON # SemicolonCommand
    ;

introspect_cast_from_to
    : (COLON_SIGN (KW_FROM | KW_TO))* # IntrospectFromTo
    ;

introspect_command
    : KW_ALGORITHMS algorithm_group? #IntrospectAlgorithms
    | KW_OVERLOADS (arg | algorithm) template_arg* # IntrospectOverloads
    | KW_OPERATORS # IntrospectOperators
    | KW_DATATYPES datatype_group? # IntrospectDatatypes
    | KW_CASTS introspect_cast_from_to arg? # IntrospectCasts
    | KW_NORMALIZATIONS # IntrospectNormalizations
    | KW_DENORMALIZATIONS # IntrospectDeormalizations
    | KW_VARIABLES variable? # IntrospectVariables
    | KW_BINDINGS binding_name? # IntrospectBindings
    | KW_AST top_level_command # IntrospectAst
    | KW_AST file # IntrospectAstFile // TODO: use file redirection. I tried, but the abstraction is killing me.
    ;

batch
    : statement_list
    ;

expression_or_batch
    : KW_BATCH batch # ExpressionOrBatch
    | KW_EXPRESSION? expression # ExpressionOrBatch
    ;

batch_or_expression
    : KW_EXPRESSION expression # BatchOrExpression
    | KW_BATCH? batch # BatchOrExpression
    ;

qualified_type
    : KW_CONST? arg (AND_OPERATOR | AMPERSAND_SIGN)?
    ;

runnable_param
    : qualified_type DOLAR_SIGN arg
    ;

command_if
    : KW_IF LEFT_PAREN condition=expression_or_batch RIGHT_PAREN KW_THEN then_branch=semicolon_command
        (KW_ELSE else_branch=semicolon_command)?  # IfCommand
    ;
command_while
    : KW_WHILE LEFT_PAREN condition=expression_or_batch RIGHT_PAREN KW_DO body=semicolon_command # WhileCommand
    ;
    
command
    : KW_EXECUTE batch_or_expression # Execute
    | KW_PRINT batch_or_expression # Print
    | (KW_QUIT | KW_EXIT) batch_or_expression? # Quit
    | KW_RETURN expression_or_batch? # Return
    | KW_HELP arg? # Help
    | KW_INTROSPECT introspect_command # Introspect
    | KW_SET (param_int=INTEGER | param_id=identifier) (val_int=INTEGER | val_id=identifier | val_str=string) # Set
    | KW_SHOW KW_MEASUREMENTS KW_AS (KW_LIST | KW_TREE) # ShowMeasurement
    | KW_STOP KW_MEASUREMENT KW_FRAME # StopMeasurement
    | KW_CLEAR KW_MEASUREMENTS # ClearMeasurement
    | KW_START (KW_ROOT | KW_OVERALL | KW_INIT | KW_FINALIZE | KW_MAIN | KW_AUXILIARY | KW_PREPROCESS | KW_ALGORITHM) KW_MEASUREMENT KW_FRAME (INTEGER | identifier) # StartMeasurement
    | (KW_LOAD | KW_UNLOAD) string # Load
    | KW_CALC expression # Calc
    | block # BlockCommand
    | KW_EVAL (INTEGER | identifier | string) # Eval
    | KW_INTERPRET (identifier | string) # Interpret
    | KW_DECLARE qualified_type DOLAR_SIGN arg ASSIGN_OPERATOR expression # Declare // Should be top-level command?
    ;

nested_level_command
    : command # NestedLevelCommand
    | command_if # If
    | command_while # While
    | (KW_BREAK | KW_CONTINUE) # CycleControl
    ;

top_level_command
    : command # TopLevelCommand
    | KW_UNDECLARE (INTEGER | identifier | string) LEFT_PAREN (qualified_type (COMMA qualified_type)*)? RIGHT_PAREN # UndeclareFunction
    | KW_PROCEDURE (INTEGER | identifier | string) LEFT_PAREN ((runnable_param (COMMA runnable_param)*)?) RIGHT_PAREN nested_level_command # Procedure
    | KW_FUNCTION (INTEGER | identifier | string) LEFT_PAREN ((runnable_param (COMMA runnable_param)*)?) RIGHT_PAREN KW_RETURNING qualified_type nested_level_command # Function
    ;

expression
    : lhs=expression op=(STAR_SIGN | PERCENTAGE_SIGN | SLASH_SIGN) rhs=expression # BinaryExpression
    | lhs=expression op=(PLUS_SIGN | MINUS_SIGN) rhs=expression # BinaryExpression
    | lhs=expression op=(LESS_SIGN | LESS_OR_EQUAL_OPERATOR | MORE_SIGN | MORE_OR_EQUAL_OPERATOR) rhs=expression # BinaryExpression
    | lhs=expression op=(EQUAL_OPERATOR | NOT_EQUAL_OPERATOR) rhs=expression # BinaryExpression
    | lhs=expression op=CARET_SIGN rhs=expression # BinaryExpression
    | lhs=expression op=AMPERSAND_SIGN rhs=expression # BinaryExpression
    | lhs=expression op=PIPE_SIGN rhs=expression # BinaryExpression
    | lhs=expression op=AND_OPERATOR rhs=expression # BinaryExpression
    | lhs=expression op=OR_OPERATOR rhs=expression # BinaryExpression
    | <assoc=right> lhs=expression op=ASSIGN_OPERATOR rhs=expression # BinaryExpression
    | prefix_expression # PExpression
    ;

prefix_expression
    : KW_CAST LEFT_PAREN type RIGHT_PAREN prefix_expression # CastPrefixExpression
    | op=PLUS_SIGN prefix_expression # PrefixExpression
    | op=MINUS_SIGN prefix_expression # PrefixExpression
    | op=EXCLAMATION_SIGN prefix_expression # PrefixExpression
    | op=TYLDE_SIGN prefix_expression # PrefixExpression
    | op=INC_OPERATOR prefix_expression # PrefixExpression
    | op=DEC_OPERATOR prefix_expression # PrefixExpression
    | suffix_expression # SExpression
    ;

suffix_expression
    : atom DOT algorithm bracketed_expression_list # MethodCallExpression
    | atom op=(INC_OPERATOR | DEC_OPERATOR) # PostfixExpression
    | atom # SuffixExpression
    ;

atom
    : (KW_TYPE | KW_ACTUAL_TYPE) LEFT_PAREN expression RIGHT_PAREN # ActualTypeExpression
    | KW_DECLARED_TYPE LEFT_PAREN expression RIGHT_PAREN # ActualTypeExpression
    | algorithm bracketed_expression_list # FunctionCallExpression
    | variable # VariableExpression
    | LEFT_PAREN expression RIGHT_PAREN # AtomExpression
    | string # StringImmediateExpression
    | INTEGER # IntegerImmediateExpression
    | DOUBLE # DoubleImmediateExpression
    ;

bracketed_expression_list
    : LEFT_PAREN (expression (COMMA expression)*)? RIGHT_PAREN// # BracketedExpressionList
    ;

category_option
    : option # CategoryOption
    ;

type_option
    : option # TypeOption
    ;

option
    : (COLON_SIGN (identifier | INTEGER))
    ;

algorithm
    : IDENTIFIER
    ;

algorithm_group
    : arg
    ;

datatype_group
    : arg
    ;

variable
    : DOLAR_SIGN arg
    ;

binding
    : HASH_SIGN INTEGER
    | HASH_SIGN identifier
    ;

binding_name
    : identifier | INTEGER
    ;

string
    : STRING
    ;

// Allow keywords to be identifiers
identifier
    : IDENTIFIER | KW_AST | KW_FROM | KW_TO | KW_ALGORITHMS | KW_OVERLOADS | KW_OPERATORS | KW_DATATYPES | KW_CASTS
    | KW_NORMALIZATIONS | KW_DENORMALIZATIONS | KW_VARIABLES | KW_BINDINGS | KW_BATCH | KW_EXPRESSION | KW_CONST
    | KW_EXECUTE | KW_PRINT | KW_QUIT | KW_EXIT | KW_RUN | KW_RETURN | KW_BREAK | KW_HELP | KW_INTROSPECT | KW_SET
    | KW_SHOW | KW_MEASUREMENTS | KW_AS | KW_LIST | KW_TREE | KW_CLEAR | KW_START | KW_ROOT | KW_CURRENT | KW_OVERALL
    | KW_INIT | KW_FINALIZE | KW_MAIN | KW_AUXILIARY | KW_PREPROCESS | KW_ALGORITHM | KW_MEASUREMENT | KW_FRAME | KW_FUNCTION
    | KW_STOP | KW_LOAD | KW_UNLOAD | KW_CALC | KW_EVAL | KW_INTERPRET | KW_IF | KW_ELSE | KW_WHILE | KW_FOR | KW_THEN
    | KW_DO | KW_CONTINUE | KW_DECLARE | KW_UNDECLARE | KW_PROCEDURE | KW_CAST | KW_TYPE | KW_ACTUAL_TYPE | KW_DECLARED_TYPE
    | KW_RETURNING
    ;