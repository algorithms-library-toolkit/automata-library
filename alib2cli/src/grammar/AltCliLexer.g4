lexer grammar AltCliLexer;

@lexer::members {
size_t nestedLevel = 0;
}

HASH_SIGN: '#';
AT_SIGN: '@';
LEFT_PAREN: '(';
RIGHT_PAREN: ')';
LEFT_BRACKET: '[';
RIGHT_BRACKET: ']';
LEFT_BRACE: '{';
RIGHT_BRACE: '}';
COLON_SIGN: ':';
DOLAR_SIGN: '$';
LESS_SIGN: '<';
MORE_SIGN: '>';
SEMICOLON: ';';
PIPE_SIGN: '|';
IMPL_SIGN: '=>';
SLASH_SIGN: '/';
STAR_SIGN: '*';
PERCENTAGE_SIGN: '%';
PLUS_SIGN: '+';
MINUS_SIGN: '-';
TYLDE_SIGN: '~';
CARET_SIGN: '^';
AMPERSAND_SIGN: '&';
EXCLAMATION_SIGN: '!';

OR_OPERATOR: '||';
AND_OPERATOR: '&&';
EQUAL_OPERATOR: '==';
NOT_EQUAL_OPERATOR: '!=';
LESS_OPERATOR: LESS_SIGN;
MORE_OPERATOR: MORE_SIGN;
LESS_OR_EQUAL_OPERATOR: '<=';
MORE_OR_EQUAL_OPERATOR: '>=';
ASSIGN_OPERATOR: '=';
INC_OPERATOR: '++';
DEC_OPERATOR: '--';

COMMA: ',';
DOT: '.';

/* Special keyword */
BEGIN : 'begin' { nestedLevel++; };
END : 'end' { nestedLevel--; };

/* KEYWORDS */
KW_AST: 'ast';
KW_FROM: 'from';
KW_TO: 'to';
KW_ALGORITHMS: 'algorithms';
KW_OVERLOADS: 'overloads';
KW_OPERATORS: 'operators';
KW_DATATYPES: 'datatypes';
KW_CASTS: 'casts';
KW_NORMALIZATIONS: 'normalizations';
KW_DENORMALIZATIONS: 'denormalizations';
KW_VARIABLES: 'variables';
KW_BINDINGS: 'bindings';
KW_BATCH: 'batch';
KW_EXPRESSION: 'expression';
KW_CONST: 'const';
KW_EXECUTE: 'execute';
KW_PRINT: 'print';
KW_QUIT: 'quit';
KW_EXIT: 'exit';
KW_RUN: 'run';
KW_RETURN: 'return';
KW_BREAK: 'break';
KW_HELP: 'help';
KW_INTROSPECT: 'introspect';
KW_SET: 'set';
KW_SHOW: 'show';
KW_MEASUREMENTS: 'measurements';
KW_AS: 'as';
KW_LIST: 'list';
KW_TREE: 'tree';
KW_CLEAR: 'clear';
KW_START: 'start';
KW_ROOT: 'root';
KW_CURRENT: 'current';
KW_OVERALL: 'overall';
KW_INIT: 'init';
KW_FINALIZE: 'finalize';
KW_MAIN: 'main';
KW_AUXILIARY: 'auxiliary';
KW_PREPROCESS: 'preprocess';
KW_ALGORITHM: 'algorithm';
KW_MEASUREMENT: 'measurement';
KW_FRAME: 'frame';
KW_FUNCTION: 'function';
KW_STOP: 'stop';
KW_LOAD: 'load';
KW_UNLOAD: 'unload';
KW_CALC: 'calc';
KW_EVAL: 'eval';
KW_INTERPRET: 'interpret';
KW_IF: 'if';
KW_ELSE: 'else';
KW_WHILE: 'while';
KW_FOR: 'for';
KW_THEN: 'then';
KW_DO: 'do';
KW_CONTINUE: 'continue';
KW_DECLARE: 'declare';
KW_UNDECLARE: 'undeclare';
KW_PROCEDURE: 'procedure';
KW_CAST: 'cast';
KW_TYPE: 'type';
KW_ACTUAL_TYPE: 'actual_type';
KW_DECLARED_TYPE: 'declared_type';
KW_RETURNING: 'returning';

fragment DIGIT: [0-9];
INTEGER: DIGIT+;
DOUBLE: DIGIT+ '.' DIGIT* | DIGIT* '.' DIGIT+;

IDENTIFIER: ([a-z] | [A-Z] | '_') ([0-9] | [a-z] | [A-Z] | '_' | ':')*;

STRING: DQUOTE (STR_TEXT | EOL)* DQUOTE;

fragment STR_TEXT: (~["\r\n\\] | ESC_SEQ)+;
fragment ESC_SEQ: '\\' ([btnrf"\\] | EOF);
fragment DQUOTE: '"';
fragment EOL: '\r'? '\n';

ESCAPE_NEWLINE: '\\' EOL -> channel(HIDDEN);
// This new line is skiped if we are in block, where are statements ended by ';'
BLOCK_NEWLINE: EOL {nestedLevel>0}? -> channel(HIDDEN);
// Top level newline
NEWLINE: EOL;
// Ignore whitespace
WS: [ \t]+ -> channel(HIDDEN);

// Comments
COMMENT: '//' (~[\r\n] | '\\' EOL)* -> channel(HIDDEN);
MULTILINE_COMMENT: '/*' .*? '*/' -> channel(HIDDEN);
