#pragma once

#include <string>
#include <utility>

#include <readline/LineInterface.h>

namespace cli {

class StringLineInterface final : public cli::LineInterface {
    std::string m_string;

    bool readline(std::string& line, bool) override
    {
        if (m_string.empty())
            return false;

        line = std::exchange(m_string, "");
        return true;
    }

public:
    StringLineInterface(std::string string)
        : m_string(std::move(string))
    {
    }
};

} // namespace cli
