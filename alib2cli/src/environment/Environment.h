#pragma once

#include <istream>

#include <ext/concepts>
#include <ext/memory>
#include <ext/typeinfo>

#include <alib/map>
#include <alib/optional>
#include <alib/string>

#include <abstraction/Value.hpp>
#include <exception/CommonException.h>

#include <abstraction/OperationAbstraction.hpp>
#include <abstraction/ValueHolder.hpp>
#include <ast/command/CommandResult.h>

#include <abstraction/TemporariesHolder.h>

#include <common/ResultInterpret.h>

namespace cli {

class Command;

class Environment : public abstraction::TemporariesHolder {
    ext::map<std::string, std::string> m_bindings;
    ext::map<std::string, std::shared_ptr<abstraction::Value>> m_variables;
    std::shared_ptr<abstraction::Value> m_result;

    ext::optional_ref<Environment> m_upper;

    std::shared_ptr<abstraction::Value> getVariableInt(const std::string& name) const;

    void setVariableInt(std::string name, std::shared_ptr<abstraction::Value> value)
    {
        m_variables[std::move(name)] = std::move(value);
    }

public:
    Environment() = default;

    Environment(ext::optional_ref<Environment> upper)
        : m_upper(upper)
    {
    }

    std::string getBinding(const std::string& name) const;

    std::set<std::string> getBindingNames() const
    {
        std::set<std::string> res;

        for (const std::pair<const std::string, std::string>& kv : m_bindings) {
            res.insert(kv.first);
        }

        return res;
    }

    void setBinding(std::string name, std::string value)
    {
        m_bindings[std::move(name)] = std::move(value);
    }

    bool clearBinding(const std::string& name)
    {
        return m_bindings.erase(name) != 0;
    }

    std::shared_ptr<abstraction::Value> getVariable(const std::string& name) const
    {
        return getVariableInt(name);
    }

    template <class T>
    T getVariable(const std::string& name) const
    {
        return cli::ResultInterpret::value<T>(getVariableInt(name));
    }

    std::set<std::string> getVariableNames() const
    {
        std::set<std::string> res;

        for (const std::pair<const std::string, std::shared_ptr<abstraction::Value>>& kv : m_variables) {
            res.insert(kv.first);
        }

        return res;
    }

    void setVariable(std::string name, std::shared_ptr<abstraction::Value> value)
    {
        setVariableInt(std::move(name), std::move(value));
    }

    template <ext::same_as_decay<std::string> String, class T>
    void setVariable(String&& name, T&& value)
    {
        static_assert(!std::is_reference_v<T>, "The argument is expected to be an rvalue reference.");
        auto variable = std::make_shared<abstraction::ValueHolder<std::decay_t<T>>>(std::forward<T>(value), false);
        setVariableInt(std::forward<String>(name), variable);
    }

    bool clearVariable(const std::string& name)
    {
        return m_variables.erase(name) != 0u;
    }

    void setResult(std::shared_ptr<abstraction::Value> value)
    {
        m_result = std::move(value);
    }

    std::shared_ptr<abstraction::Value> getResult() const
    {
        return m_result;
    }

    cli::CommandResult execute(const std::string& input);

    cli::CommandResult execute(std::istream& input);

    cli::CommandResult execute(const std::unique_ptr<cli::Command>& command);

    Environment& getGlobalScope();
};

} /* namespace cli */
