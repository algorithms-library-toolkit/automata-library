#include <environment/Environment.h>

#include <ext/exception>

#include <parser/Parser.h>

#include <global/GlobalData.h>

namespace cli {

std::shared_ptr<abstraction::Value> Environment::getVariableInt(const std::string& name) const
{
    std::reference_wrapper<const Environment> env = *this;
    while (true) {
        auto res = env.get().m_variables.find(name);

        if (res != env.get().m_variables.end())
            return res->second;

        if (!env.get().m_upper)
            break;

        env = *env.get().m_upper;
    }
    throw exception::CommonException("Variable of name " + name + " not found.");
}

std::string Environment::getBinding(const std::string& name) const
{
    std::reference_wrapper<const Environment> env = *this;
    while (true) {
        auto res = m_bindings.find(name);

        if (res != m_bindings.end())
            return res->second;

        if (!env.get().m_upper)
            break;

        env = *env.get().m_upper;
    }
    throw exception::CommonException("Binded value of name " + name + " not found.");
}

Environment& Environment::getGlobalScope()
{
    if (!static_cast<bool>(m_upper))
        return *this;
    return m_upper->getGlobalScope();
}


cli::CommandResult Environment::execute(const std::unique_ptr<cli::Command>& command)
{
    try {
        const CommandResult res = command->run(*this);

        if (res == CommandResult::CONTINUE || res == CommandResult::BREAK)
            throw std::logic_error("There is no loop to continue/break.");

        return res;
    } catch (...) {
        alib::ExceptionHandler::handle(common::Streams::err);
        return CommandResult::EXCEPTION;
    }
}
cli::CommandResult Environment::execute(const std::string& input)
{
    return execute(Parser::parseString(input));
}
cli::CommandResult Environment::execute(std::istream& input)
{
    return execute(Parser::parseStream(input));
}

} /* namespace cli */
