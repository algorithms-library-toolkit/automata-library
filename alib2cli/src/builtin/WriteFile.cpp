#include <fstream>
#include <global/GlobalData.h>
#include <registration/AlgoRegistration.hpp>
#include "WriteFile.h"

#include <exception/CommonException.h>

namespace cli::builtin {

void WriteFile::write(const std::string& filename, const std::string& data)
{
    if (filename == "-")
        common::Streams::out << data;
    else {
        std::ofstream t(filename);
        if (!t.is_open()) {
            throw exception::CommonException("File could not be opened.");
        }
        t << data;
    }
}

auto WriteFileString = registration::AbstractRegister<WriteFile, void, const std::string&, const std::string&>(WriteFile::write, "filename", "data").setDocumentation("Writes some string into a file.\n\
\n\
@param filename the name of written file\n\
@param data the content of the file");

} /* namespace cli::builtin */
