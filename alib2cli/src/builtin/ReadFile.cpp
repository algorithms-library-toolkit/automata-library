#include <core/type_util.hpp>
#include <fstream>
#include <global/GlobalData.h>
#include <registration/AlgoRegistration.hpp>
#include <streambuf>
#include "ReadFile.h"

#include <exception/CommonException.h>

namespace cli::builtin {

std::string ReadFile::read(const std::string& filename)
{
    if (filename == "-") {
        common::Streams::in >> std::noskipws;
        return std::string((std::istreambuf_iterator<char>(common::Streams::in.get().rdbuf())), std::istreambuf_iterator<char>());
    } else {
        std::ifstream t(filename);
        if (!t.is_open()) {
            throw exception::CommonException("File could not be opened.");
        }
        t >> std::noskipws;
        return std::string((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());
    }
}

auto ReadFileString = registration::AbstractRegister<ReadFile, std::string, const std::string&>(ReadFile::read, "filename").setDocumentation("Reads the content of a file into a string.\n\
\n\
@param filename the name of read file\n\
@return the content of the file");

} /* namespace cli::builtin */
