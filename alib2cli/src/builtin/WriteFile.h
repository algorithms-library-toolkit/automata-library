#pragma once

#include <alib/string>

namespace cli {

namespace builtin {

/**
 * File writting command.
 *
 */
class WriteFile {
public:
    /**
     * Writes some string into a file.
     *
     * \param filename the name of written file
     * \param data the content of the file
     */
    static void write(const std::string& filename, const std::string& data);
};

} /* namespace builtin */

} /* namespace cli */
