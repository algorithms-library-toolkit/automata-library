#include <AltCliLexer.h>
#include <AltCliParser.h>
#include <antlr4-runtime.h>
#include <grammar/BufferedErrorListener.h>
#include <parser/AltVisitor.h>
#include "Parser.h"

namespace cli {
using namespace cli::grammar::parser;
using namespace cli::grammar::lexer;

std::unique_ptr<CommandList> Parser::parseString(const std::string& unit)
{
    antlr4::ANTLRInputStream inputStream(unit);
    return parse(inputStream);
}

std::unique_ptr<CommandList> Parser::parseStream(std::istream& istream)
{
    antlr4::ANTLRInputStream fileStream;
    fileStream.load(istream);

    return parse(fileStream);
}

std::unique_ptr<CommandList> Parser::parse(antlr4::CharStream& stream)
{
    auto listener = std::make_shared<cli::grammar::BufferedErrorListener>();

    AltCliLexer lexer(&stream);
    lexer.removeErrorListeners();
    lexer.addErrorListener(listener.get());
    antlr4::CommonTokenStream tokenStream(&lexer);
    AltCliParser parser(&tokenStream);
    parser.removeErrorListeners();
    parser.addErrorListener(listener.get());

    parser.setErrorHandler(std::make_shared<antlr4::DefaultErrorStrategy>());

    AltCliParser::ParseContext* tree = parser.parse();
    if (listener->any()) {
        if (lexer.nestedLevel > 0 || listener->isMissingClosingQuote()) {
            // Nested level or quote level is greater than 0, it means that we have unclosed block or string.
            throw ContinueException();
        }
        throw exception::CommonException(listener->getErrors());
    }

    if (tree == nullptr) {
        throw exception::CommonException("Invalid parse");
    }

    auto command = AltVisitor().transform(tree);

    return command;
}
} // cli
