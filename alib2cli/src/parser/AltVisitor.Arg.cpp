#include <parser/AltVisitor.h>

#include <ast/args/BindedArg.h>
#include <ast/args/ImmediateArg.h>

namespace cli {
std::any AltVisitor::visitArg(AltCliParser::ArgContext* ctx)
{
    if (ctx->binding() == nullptr) {
        return retPtr<Arg, ImmediateArg>(ctx->identifier()->getText());
    }

    return visitBinding(ctx->binding());
}

std::any AltVisitor::visitType(AltCliParser::TypeContext* ctx)
{
    if (ctx->type() != nullptr) {
        // Type is in original grammar defined recursively in lexer
        // In ANTLR4 the recursive nature is defined in parse, so we get the whole tree in one go here.
        return retPtr<Arg, ImmediateArg>(getRawText(ctx));
    }

    if (ctx->identifier() != nullptr)
        return retPtr<Arg, ImmediateArg>(ctx->identifier()->getText());

    // Old parser sets hint and lexes text with " included.
    if (ctx->string() != nullptr)
        return retPtr<Arg, ImmediateArg>(ctx->string()->getText());

    invalidParse("Invalid type");
}

std::any AltVisitor::visitVariable(AltCliParser::VariableContext* ctx)
{
    return visit(ctx->arg());
}

std::any AltVisitor::visitBinding(AltCliParser::BindingContext* ctx)
{
    std::string content;
    if (ctx->identifier() != nullptr)
        content = ctx->identifier()->getText();
    else if (ctx->INTEGER() != nullptr)
        content = ctx->INTEGER()->getText();

    return retPtr<Arg, BindedArg>(content);
}

std::any AltVisitor::visitAlgorithm_group(AltCliParser::Algorithm_groupContext* ctx)
{
    return visit(ctx->arg());
}

std::any AltVisitor::visitDatatype_group(AltCliParser::Datatype_groupContext* ctx)
{
    return visit(ctx->arg());
}

std::any AltVisitor::visitFile(AltCliParser::FileContext* ctx)
{
    if (ctx->binding() != nullptr)
        return visit(ctx->binding());

    if (ctx->string() != nullptr)
        return retPtr<Arg, ImmediateArg>(cast<std::string>(visit(ctx->string())));

    invalidParse("Invalid file name");
}

std::any AltVisitor::visitAlgorithm(AltCliParser::AlgorithmContext* ctx)
{
    return retPtr<Arg, ImmediateArg>(ctx->IDENTIFIER()->getText());
}
}