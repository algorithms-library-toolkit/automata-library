#include <parser/AltVisitor.h>

#include <ast/args/ImmediateArg.h>
#include <ast/command/AlgorithmsIntrospectionCommand.h>
#include <ast/command/AstIntrospectionCommand.h>
#include <ast/command/BindingsIntrospectionCommand.h>
#include <ast/command/BlockCommand.h>
#include <ast/command/BreakCommand.h>
#include <ast/command/CalcCommand.h>
#include <ast/command/CastsIntrospectionCommand.h>
#include <ast/command/ClearMeasurements.h>
#include <ast/command/ContinueCommand.h>
#include <ast/command/DataTypesIntrospectionCommand.h>
#include <ast/command/DeclareRunnableCommand.h>
#include <ast/command/EOTCommand.h>
#include <ast/command/EvalCommand.h>
#include <ast/command/ExecuteCommand.h>
#include <ast/command/HelpCommand.h>
#include <ast/command/IfCommand.h>
#include <ast/command/InterpretCommand.h>
#include <ast/command/LoadCommand.h>
#include <ast/command/NormalizeDenormalizeIntrospectionCommand.h>
#include <ast/command/OperatorsIntrospectionCommand.h>
#include <ast/command/OverloadsIntrospectionCommand.h>
#include <ast/command/PrintCommand.h>
#include <ast/command/QuitCommand.h>
#include <ast/command/ReturnCommand.h>
#include <ast/command/SetCommand.h>
#include <ast/command/ShowMeasurements.h>
#include <ast/command/StartMeasurementFrame.h>
#include <ast/command/StopMeasurementFrame.h>
#include <ast/command/UndeclareRunnableCommand.h>
#include <ast/command/UnloadCommand.h>
#include <ast/command/VarDeclareCommand.h>
#include <ast/command/VariablesIntrospectionCommand.h>
#include <ast/command/WhileCommand.h>

namespace cli {
using QualifedType = std::pair<abstraction::TypeQualifiers::TypeQualifierSet, std::unique_ptr<Arg>>;

std::any AltVisitor::visitIfCommand(AltCliParser::IfCommandContext* ctx)
{
    auto expression = castToUnique<Expression>(visit(ctx->condition));
    auto thenBranch = castToUnique<Command>(visit(ctx->then_branch));

    auto elseBranch = std::unique_ptr<Command>(nullptr);
    if (ctx->else_branch != nullptr) {
        elseBranch = castToUnique<Command>(visit(ctx->else_branch));
    }

    return retPtr<Command, IfCommand>(std::move(expression), std::move(thenBranch), std::move(elseBranch));
}

std::any AltVisitor::visitWhileCommand(AltCliParser::WhileCommandContext* ctx)
{
    auto condition = castToUnique<Expression>(visit(ctx->condition));
    auto body = castToUnique<Command>(visit(ctx->body));

    return retPtr<Command, WhileCommand>(std::move(condition), std::move(body));
}

std::any AltVisitor::visitParse(AltCliParser::ParseContext* ctx)
{
    ext::vector<std::unique_ptr<Command>> commands;

    if (ctx->top_level_command().empty()) {
        commands.emplace_back(new EOTCommand());
    } else {
        fillList<Command>(commands, ctx->top_level_command());
    }

    return new CommandList(std::move(commands));
}

std::any AltVisitor::visitPrint(AltCliParser::PrintContext* ctx)
{
    const auto anyExpression = visit(ctx->batch_or_expression());

    return retPtr<Command, PrintCommand>(castToUnique<Expression>(anyExpression));
}

std::any AltVisitor::visitBlock(AltCliParser::BlockContext* ctx)
{
    ext::vector<std::unique_ptr<Command>> list;
    fillList<Command>(list, ctx->semicolon_command());

    return retPtr<Command, CommandList>(std::move(list));
}

std::any AltVisitor::visitBlockSemicolonCommand(AltCliParser::BlockSemicolonCommandContext* ctx)
{
    return retPtr<Command, BlockCommand>(castToUnique<Command>(visit(ctx->block())));
}

std::any AltVisitor::visitBlockCommand(AltCliParser::BlockCommandContext* ctx)
{
    return retPtr<Command, BlockCommand>(castToUnique<Command>(visit(ctx->block())));
}

std::any AltVisitor::visitIfSemicolonCommand(AltCliParser::IfSemicolonCommandContext* ctx)
{
    return visit(ctx->command_if());
}

std::any AltVisitor::visitWhileSemicolonCommand(AltCliParser::WhileSemicolonCommandContext* ctx)
{
    return visit(ctx->command_while());
}

std::any AltVisitor::visitSemicolonCommand(AltCliParser::SemicolonCommandContext* ctx)
{
    return visit(ctx->nested_level_command());
}

std::any AltVisitor::visitQuit(AltCliParser::QuitContext* ctx)
{
    std::unique_ptr<Expression> expression;
    if (ctx->batch_or_expression() != nullptr)
        expression = castToUnique<Expression>(visit(ctx->batch_or_expression()));

    return retPtr<Command, QuitCommand>(std::move(expression));
}

std::any AltVisitor::visitReturn(AltCliParser::ReturnContext* ctx)
{
    std::unique_ptr<Expression> expression;
    if (ctx->expression_or_batch() != nullptr)
        expression = castToUnique<Expression>(visit(ctx->expression_or_batch()));

    return retPtr<Command, ReturnCommand>(std::move(expression));
}

std::any AltVisitor::visitCycleControl(AltCliParser::CycleControlContext* ctx)
{
    if (ctx->KW_BREAK() != nullptr)
        return retPtr<Command, BreakCommand>();

    if (ctx->KW_CONTINUE() != nullptr)
        return retPtr<Command, ContinueCommand>();

    invalidParse("Invalid cycle control");
}

std::any AltVisitor::visitDeclare(AltCliParser::DeclareContext* ctx)
{
    const auto qualifiedType = visitQualifiedType(ctx->qualified_type());
    auto name = castToUnique<Arg>(visit(ctx->arg()));
    auto expression = castToUnique<Expression>(visit(ctx->expression()));

    return retPtr<Command, VarDeclareCommand>(std::move(name), qualifiedType.first, std::move(expression));
}

QualifedType AltVisitor::visitQualifiedType(AltCliParser::Qualified_typeContext* ctx)
{
    auto typeQualifiers = abstraction::TypeQualifiers::TypeQualifierSet::NONE;

    if (ctx->KW_CONST() != nullptr)
        typeQualifiers = typeQualifiers | abstraction::TypeQualifiers::TypeQualifierSet::CONST;

    if (ctx->AND_OPERATOR() != nullptr)
        typeQualifiers = typeQualifiers | abstraction::TypeQualifiers::TypeQualifierSet::RREF;
    else if (ctx->AMPERSAND_SIGN() != nullptr)
        typeQualifiers = typeQualifiers | abstraction::TypeQualifiers::TypeQualifierSet::LREF;

    auto type = castToUnique<Arg>(visit(ctx->arg()));

    return std::make_pair(typeQualifiers, std::move(type));
}

std::vector<QualifedType> AltVisitor::visitRunnableParams(const std::vector<AltCliParser::Runnable_paramContext*>& ctx)
{
    std::vector<QualifedType> params;

    for (auto* paramCtx : ctx) {
        const auto qualifiedType = visitQualifiedType(paramCtx->qualified_type());
        auto name = castToUnique<Arg>(visit(paramCtx->arg()));

        params.emplace_back(qualifiedType.first, std::move(name));
    }

    return params;
}

std::any AltVisitor::visitExecute(AltCliParser::ExecuteContext* ctx)
{
    const auto expression = visit(ctx->batch_or_expression());

    return retPtr<Command, ExecuteCommand>(castToUnique<Expression>(expression));
}

std::any AltVisitor::visitHelp(AltCliParser::HelpContext* ctx)
{
    std::unique_ptr<Arg> arg;
    if (ctx->arg() != nullptr)
        arg = castToUnique<Arg>(visit(ctx->arg()));

    return retPtr<Command, HelpCommand>(std::move(arg));
}

std::any AltVisitor::visitFunction(AltCliParser::FunctionContext* ctx)
{
    std::string name;
    if (ctx->string() != nullptr)
        name = cast<std::string>(visit(ctx->string()));
    else if (ctx->identifier() != nullptr)
        name = ctx->identifier()->getText();
    else if (ctx->INTEGER() != nullptr)
        name = ctx->INTEGER()->getText();
    else
        invalidParse("Invalid function name");

    auto params = visitRunnableParams(ctx->runnable_param());
    auto retType = visitQualifiedType(ctx->qualified_type());
    auto body = castToUnique<Command>(visit(ctx->nested_level_command()));

    return retPtr<Command, DeclareRunnableCommand>(std::move(name), std::move(params), std::move(retType), std::move(body));
}

std::any AltVisitor::visitProcedure(AltCliParser::ProcedureContext* ctx)
{
    std::string name;
    if (ctx->string() != nullptr)
        name = cast<std::string>(visit(ctx->string()));
    else if (ctx->identifier() != nullptr)
        name = ctx->identifier()->getText();
    else if (ctx->INTEGER() != nullptr)
        name = ctx->INTEGER()->getText();
    else
        invalidParse("Invalid procedure name");

    auto params = visitRunnableParams(ctx->runnable_param());
    auto body = castToUnique<Command>(visit(ctx->nested_level_command()));

    return retPtr<Command, DeclareRunnableCommand>(std::move(name), std::move(params), std::move(body));
}

std::any AltVisitor::visitUndeclareFunction(AltCliParser::UndeclareFunctionContext* ctx)
{
    std::string name;
    if (ctx->identifier() != nullptr)
        name = ctx->identifier()->getText();
    else if (ctx->INTEGER() != nullptr)
        name = ctx->INTEGER()->getText();
    else if (ctx->string() != nullptr)
        name = cast<std::string>(visit(ctx->string()));
    else
        invalidParse("Invalid function name");

    std::vector<abstraction::TypeQualifiers::TypeQualifierSet> params;
    for (const auto& paramCtx : ctx->qualified_type()) {
        const QualifedType type = visitQualifiedType(paramCtx);
        params.emplace_back(type.first);
    }

    return retPtr<Command, UndeclareRunnableCommand>(name, params);
}

std::any AltVisitor::visitShowMeasurement(AltCliParser::ShowMeasurementContext* ctx)
{
    measurements::MeasurementFormat format;
    if (ctx->KW_LIST() != nullptr)
        format = measurements::MeasurementFormat::LIST;
    else if (ctx->KW_TREE() != nullptr)
        format = measurements::MeasurementFormat::TREE;
    else
        invalidParse("Invalid measurement format");

    return retPtr<Command, ShowMeasurements>(format);
}

std::any AltVisitor::visitStopMeasurement(AltCliParser::StopMeasurementContext*)
{
    return retPtr<Command, StopMeasurementFrame>();
}

std::any AltVisitor::visitClearMeasurement(AltCliParser::ClearMeasurementContext*)
{
    return retPtr<Command, ClearMeasurements>();
}

std::any AltVisitor::visitStartMeasurement(AltCliParser::StartMeasurementContext* ctx)
{
    std::string name;
    if (ctx->identifier() != nullptr)
        name = ctx->identifier()->getText();
    else if (ctx->INTEGER() != nullptr)
        name = ctx->INTEGER()->getText();
    else
        invalidParse("Invalid measurement name");

    measurements::Type type;
    if (ctx->KW_ROOT() != nullptr)
        type = measurements::Type::ROOT;
    else if (ctx->KW_OVERALL() != nullptr)
        type = measurements::Type::OVERALL;
    else if (ctx->KW_INIT() != nullptr)
        type = measurements::Type::INIT;
    else if (ctx->KW_FINALIZE() != nullptr)
        type = measurements::Type::FINALIZE;
    else if (ctx->KW_MAIN() != nullptr)
        type = measurements::Type::MAIN;
    else if (ctx->KW_AUXILIARY() != nullptr)
        type = measurements::Type::AUXILIARY;
    else if (ctx->KW_PREPROCESS() != nullptr)
        type = measurements::Type::PREPROCESS;
    else if (ctx->KW_ALGORITHM() != nullptr)
        type = measurements::Type::ALGORITHM;
    else
        invalidParse("Unknown measurement type");

    return retPtr<Command, StartMeasurementFrame>(std::move(name), type);
}

std::any AltVisitor::visitEval(AltCliParser::EvalContext* ctx)
{
    std::string code;
    if (ctx->identifier() != nullptr)
        code = ctx->identifier()->getText();
    else if (ctx->INTEGER() != nullptr)
        code = ctx->INTEGER()->getText();
    else if (ctx->string() != nullptr)
        code = cast<std::string>(visit(ctx->string()));
    else
        invalidParse("Invalid code");

    return retPtr<Command, EvalCommand>(std::move(code));
}

std::any AltVisitor::visitSet(AltCliParser::SetContext* ctx)
{
    std::string param;
    if (ctx->param_int != nullptr)
        param = ctx->param_int->getText();
    else if (ctx->param_id != nullptr)
        param = ctx->param_id->getText();
    else
        invalidParse("Invalid set parameter");

    std::string value;
    if (ctx->val_str != nullptr)
        value = cast<std::string>(visit(ctx->string()));
    else if (ctx->val_int != nullptr)
        value = ctx->val_int->getText();
    else if (ctx->val_id != nullptr)
        value = ctx->val_id->getText();
    else
        invalidParse("Invalid set value");


    return retPtr<Command, SetCommand>(param, value);
}

std::any AltVisitor::visitInterpret(AltCliParser::InterpretContext* ctx)
{
    std::string name;
    if (ctx->identifier() != nullptr)
        name = ctx->identifier()->getText();
    else if (ctx->string() != nullptr)
        name = cast<std::string>(visit(ctx->string()));
    else
        invalidParse("Invalid name");

    return retPtr<Command, InterpretCommand>(name);
}

std::any AltVisitor::visitLoad(AltCliParser::LoadContext* ctx)
{
    std::string name = cast<std::string>(visit(ctx->string()));

    if (ctx->KW_LOAD() != nullptr)
        return retPtr<Command, LoadCommand>(name);
    if (ctx->KW_UNLOAD() != nullptr)
        return retPtr<Command, UnloadCommand>(name);

    invalidParse("Invalid (un)load command");
}

std::any AltVisitor::visitCalc(AltCliParser::CalcContext* ctx)
{
    auto expression = castToUnique<Expression>(visit(ctx->expression()));

    return retPtr<Command, CalcCommand>(std::move(expression));
}

std::any AltVisitor::visitIntrospectAst(AltCliParser::IntrospectAstContext* ctx)
{
    auto command = castToUnique<Command>(visit(ctx->top_level_command()));

    return retPtr<Command, AstIntrospectionCommand>(std::move(command));
}

std::any AltVisitor::visitIntrospectAstFile(AltCliParser::IntrospectAstFileContext* ctx)
{
    auto fileName = castToUnique<Arg>(visit(ctx->file()));

    return retPtr<Command, AstIntrospectionCommand>(std::move(fileName));
}

std::any AltVisitor::visitIntrospectAlgorithms(AltCliParser::IntrospectAlgorithmsContext* ctx)
{
    std::unique_ptr<Arg> arg;
    if (ctx->algorithm_group() != nullptr)
        arg = castToUnique<Arg>(visit(ctx->algorithm_group()));

    return retPtr<Command, AlgorithmsIntrospectionCommand>(std::move(arg));
}

std::any AltVisitor::visitIntrospectOverloads(AltCliParser::IntrospectOverloadsContext* ctx)
{
    auto arg = castToUnique<Arg>(visit(ctx->arg()));
    ext::vector<std::unique_ptr<Arg>> templateParams;

    fillList<Arg>(templateParams, ctx->template_arg());

    return retPtr<Command, OverloadsIntrospectionCommand>(std::move(arg), std::move(templateParams));
}

std::any AltVisitor::visitIntrospectOperators(AltCliParser::IntrospectOperatorsContext*)
{
    return retPtr<Command, OperatorsIntrospectionCommand>();
}

std::any AltVisitor::visitIntrospectDatatypes(AltCliParser::IntrospectDatatypesContext* ctx)
{
    std::unique_ptr<Arg> arg;
    if (ctx->datatype_group() != nullptr)
        arg = castToUnique<Arg>(visit(ctx->datatype_group()));

    return retPtr<Command, DataTypesIntrospectionCommand>(std::move(arg));
}

std::any AltVisitor::visitIntrospectCasts(AltCliParser::IntrospectCastsContext* ctx)
{
    std::unique_ptr<Arg> arg;
    if (ctx->arg() != nullptr)
        arg = castToUnique<Arg>(visit(ctx->arg()));

    auto fromTo = cast<std::pair<bool, bool>>(visit(ctx->introspect_cast_from_to()));

    return retPtr<Command, CastsIntrospectionCommand>(std::move(arg), fromTo.first, fromTo.second);
}

std::any AltVisitor::visitIntrospectFromTo(AltCliParser::IntrospectFromToContext* ctx)
{
    const bool from = !ctx->KW_FROM().empty();
    const bool to = !ctx->KW_TO().empty();

    return std::make_pair(from, to);
}

std::any AltVisitor::visitIntrospectNormalizations(AltCliParser::IntrospectNormalizationsContext*)
{
    return retPtr<Command, NormalizeDenormalizeIntrospectionCommand>(NormalizeDenormalizeIntrospectionCommand::What::NORMALIZE);
}

std::any AltVisitor::visitIntrospectDeormalizations(AltCliParser::IntrospectDeormalizationsContext*)
{
    return retPtr<Command, NormalizeDenormalizeIntrospectionCommand>(NormalizeDenormalizeIntrospectionCommand::What::DENORMALIZE);
}

std::any AltVisitor::visitIntrospectVariables(AltCliParser::IntrospectVariablesContext* ctx)
{
    std::unique_ptr<Arg> arg;
    if (ctx->variable() != nullptr)
        arg = castToUnique<Arg>(visit(ctx->variable()));

    return retPtr<Command, VariablesIntrospectionCommand>(std::move(arg));
}

std::any AltVisitor::visitIntrospectBindings(AltCliParser::IntrospectBindingsContext* ctx)
{
    std::unique_ptr<Arg> arg;
    if (ctx->binding_name() != nullptr) {
        const auto bindCtx = ctx->binding_name();
        std::string content;
        if (bindCtx->identifier() != nullptr)
            content = bindCtx->identifier()->getText();
        else if (bindCtx->INTEGER() != nullptr)
            content = bindCtx->INTEGER()->getText();
        else
            invalidParse("Invalid binding name");

        arg = std::make_unique<ImmediateArg>(content);
    }

    return retPtr<Command, BindingsIntrospectionCommand>(std::move(arg));
}
}
