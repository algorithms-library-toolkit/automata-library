#include <parser/AltVisitor.h>

namespace cli {
std::any AltVisitor::visitTypeOption(AltCliParser::TypeOptionContext* ctx)
{
    const auto option = cast<std::string>(visit(ctx->option()));

    return retPtr<TypeOption, TypeOption>(option);
}

std::any AltVisitor::visitCategoryOption(AltCliParser::CategoryOptionContext* ctx)
{
    const auto option = std::any_cast<std::string>(visit(ctx->option()));

    return retPtr<CategoryOption, CategoryOption>(option);
}

std::any AltVisitor::visitOption(AltCliParser::OptionContext* ctx)
{
    if (ctx->identifier() != nullptr)
        return ctx->identifier()->getText();
    if (ctx->INTEGER() != nullptr)
        return ctx->INTEGER()->getText();

    invalidParse("Invalid option");
}
}
