#include <parser/AltVisitor.h>

#include <ast/statements/CastStatement.h>
#include <ast/statements/ContainerStatement.h>
#include <ast/statements/FileStatement.h>
#include <ast/statements/ImmediateStatement.h>
#include <ast/statements/PreviousResultStatement.h>
#include <ast/statements/ResultFileStatement.h>
#include <ast/statements/ResultVariableStatement.h>
#include <ast/statements/SingleStatement.h>
#include <ast/statements/ValueStatement.h>
#include <ast/statements/VariableStatement.h>

#include <ast/args/BindedArg.h>
#include <ast/args/ImmediateArg.h>

namespace cli {

std::any AltVisitor::visitSingleStatement(AltCliParser::SingleStatementContext* ctx)
{
    std::unique_ptr<Arg> name = castToUnique<Arg>(visit(ctx->algorithm()));
    ext::vector<std::unique_ptr<Arg>> templateArgs;
    ext::vector<std::shared_ptr<Statement>> statements;

    fillList<Arg>(templateArgs, ctx->template_arg());
    fillList<Statement>(statements, ctx->param());

    std::unique_ptr<CategoryOption> category = std::unique_ptr<CategoryOption>(nullptr);
    if (ctx->category_option() != nullptr)
        category = castToUnique<CategoryOption>(visit(ctx->category_option()));

    return retPtr<Statement, SingleStatement>(std::move(name), std::move(templateArgs), std::move(statements), std::move(category));
}

std::any AltVisitor::visitPreviousResultParam(AltCliParser::PreviousResultParamContext*)
{
    return retPtr<Statement, PreviousResultStatement>();
}

std::any AltVisitor::visitStatementInRedirect(AltCliParser::StatementInRedirectContext* ctx)
{
    return visit(ctx->statement_list());
}

std::any AltVisitor::visitFileInRedirect(AltCliParser::FileInRedirectContext* ctx)
{
    std::unique_ptr<Arg> fileType;
    if (ctx->arg() != nullptr) {
        fileType = castToUnique<Arg>(visit(ctx->arg()));
    }

    std::unique_ptr<TypeOption> type;
    if (ctx->type_option() != nullptr) {
        type = castToUnique<TypeOption>(visit(ctx->type_option()));
    }

    auto file = castToUnique<Arg>(visit(ctx->file()));
    return retPtr<Statement, FileStatement>(std::move(file), std::move(fileType), std::move(type));
}

std::any AltVisitor::visitCastStatement(AltCliParser::CastStatementContext* ctx)
{
    auto arg = castToUnique<Arg>(visit(ctx->type()));
    auto statement = castToShared<Statement>(visit(ctx->statement()));

    return retPtr<Statement, CastStatement>(std::move(arg), std::move(statement));
}

std::any AltVisitor::visitStatementList(AltCliParser::StatementListContext* ctx)
{
    ext::vector<std::shared_ptr<Statement>> list;

    list.emplace_back(castToShared<Statement>(visit(ctx->statement())));
    fillList<Statement>(list, ctx->statement_list_optional());

    return retPtr<Statement, StatementList>(std::move(list));
}

std::any AltVisitor::visitCommonStatement(AltCliParser::CommonStatementContext* ctx)
{
    return AltCliParserBaseVisitor::visitCommonStatement(ctx);
}

std::any AltVisitor::visitStringImmediateStatement(AltCliParser::StringImmediateStatementContext* ctx)
{
    return retPtr<Statement, ImmediateStatement<std::string>>(cast<std::string>(visit(ctx->string())));
}

std::any AltVisitor::visitIntegerImmediateStatement(AltCliParser::IntegerImmediateStatementContext* ctx)
{
    return retPtr<Statement, ImmediateStatement<int>>(visitInt(ctx->INTEGER()));
}

std::any AltVisitor::visitDoubleImmediateStatement(AltCliParser::DoubleImmediateStatementContext* ctx)
{
    return retPtr<Statement, ImmediateStatement<double>>(visitDouble(ctx->DOUBLE()));
}

std::any AltVisitor::visitImmediateParam(AltCliParser::ImmediateParamContext* ctx)
{
    const auto content = ctx->identifier()->getText();

    return retPtr<Statement, ImmediateStatement<std::string>>(content);
}

std::any AltVisitor::visitResultVariableStatement(AltCliParser::ResultVariableStatementContext* ctx)
{
    auto arg = castToUnique<Arg>(visit(ctx->variable()));

    return retPtr<Statement, ResultVariableStatement>(std::move(arg));
}

std::any AltVisitor::visitResultFileStatement(AltCliParser::ResultFileStatementContext* ctx)
{
    std::unique_ptr<Arg> fileType;
    if (ctx->arg() != nullptr) {
        fileType = castToUnique<Arg>(visit(ctx->arg()));
    }

    auto file = castToUnique<Arg>(visit(ctx->file()));

    return retPtr<Statement, ResultFileStatement>(std::move(file), std::move(fileType));
}

std::any AltVisitor::visitVariableStatement(AltCliParser::VariableStatementContext* ctx)
{
    auto arg = castToUnique<Arg>(visit(ctx->variable()));
    return retPtr<Statement, VariableStatement>(std::move(arg));
}

std::any AltVisitor::visitCastParam(AltCliParser::CastParamContext* ctx)
{
    auto resultType = castToUnique<Arg>(visit(ctx->type()));
    auto param = castToUnique<Statement>(visit(ctx->param()));

    return retPtr<Statement, CastStatement>(std::move(resultType), std::move(param));
}

std::any AltVisitor::visitValueStatement(AltCliParser::ValueStatementContext* ctx)
{
    auto binding = castToUnique<Arg>(visit(ctx->binding()));
    return retPtr<Statement, ValueStatement>(std::move(binding));
}

std::any AltVisitor::visitContainerStatement(AltCliParser::ContainerStatementContext* ctx)
{
    ext::vector<std::shared_ptr<Statement>> params;
    fillList<Statement>(params, ctx->param());

    return retPtr<Statement, ContainerStatement>("Set", std::move(params));
}
}