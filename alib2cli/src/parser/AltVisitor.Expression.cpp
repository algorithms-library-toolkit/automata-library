#include <parser/AltVisitor.h>

#include <ast/expression/ActualTypeExpression.h>
#include <ast/expression/BatchExpression.h>
#include <ast/expression/BinaryExpression.h>
#include <ast/expression/CastExpression.h>
#include <ast/expression/DeclaredTypeExpression.h>
#include <ast/expression/FunctionCallExpression.h>
#include <ast/expression/ImmediateExpression.h>
#include <ast/expression/MethodCallExpression.h>
#include <ast/expression/PostfixExpression.h>
#include <ast/expression/PrefixExpression.h>
#include <ast/expression/VariableExpression.h>

namespace cli {

std::any AltVisitor::visitBatchOrExpression(AltCliParser::BatchOrExpressionContext* ctx)
{
    if (ctx->KW_EXPRESSION() != nullptr) {
        return visit(ctx->expression());
    }

    return visit(ctx->batch());
}

std::any AltVisitor::visitExpressionOrBatch(AltCliParser::ExpressionOrBatchContext* ctx)
{
    if (ctx->KW_BATCH() != nullptr) {
        return visit(ctx->batch());
    }

    return visit(ctx->expression());
}

std::any AltVisitor::visitBatch(AltCliParser::BatchContext* ctx)
{
    std::any const statements = visit(ctx->statement_list());

    return retPtr<Expression, BatchExpression>(castToShared<Statement>(statements));
}

std::any AltVisitor::visitBinaryExpression(AltCliParser::BinaryExpressionContext* ctx)
{
    auto lhs = castToUnique<Expression>(visit(ctx->lhs));
    auto rhs = castToUnique<Expression>(visit(ctx->rhs));
    auto operation = binaryExpressionMap[ctx->op->getText()];

    return retPtr<Expression, BinaryExpression>(operation, std::move(lhs), std::move(rhs));
}

std::any AltVisitor::visitPostfixExpression(AltCliParser::PostfixExpressionContext* ctx)
{
    auto expression = castToUnique<Expression>(visit(ctx->atom()));
    auto operation = postfixExpressionMap[ctx->op->getText()];

    return retPtr<Expression, PostfixExpression>(operation, std::move(expression));
}

std::any AltVisitor::visitPrefixExpression(AltCliParser::PrefixExpressionContext* ctx)
{
    auto expression = castToUnique<Expression>(visit(ctx->prefix_expression()));
    auto operation = prefixExpressionMap[ctx->op->getText()];

    return retPtr<Expression, PrefixExpression>(operation, std::move(expression));
}

std::any AltVisitor::visitCastPrefixExpression(AltCliParser::CastPrefixExpressionContext* ctx)
{
    auto type = castToUnique<Arg>(visit(ctx->type()));
    auto expression = castToUnique<Expression>(visit(ctx->prefix_expression()));

    return retPtr<Expression, CastExpression>(std::move(type), std::move(expression));
}

std::any AltVisitor::visitActualTypeExpression(AltCliParser::ActualTypeExpressionContext* ctx)
{
    auto expression = castToUnique<Expression>(visit(ctx->expression()));

    return retPtr<Expression, ActualTypeExpression>(std::move(expression));
}

std::any AltVisitor::visitAtomExpression(AltCliParser::AtomExpressionContext* ctx)
{
    return visit(ctx->expression());
}

std::any AltVisitor::visitMethodCallExpression(AltCliParser::MethodCallExpressionContext* ctx)
{
    auto object = castToUnique<Expression>(visit(ctx->atom()));
    const auto methodName = ctx->algorithm()->getText();
    std::vector<std::unique_ptr<Expression>> list;
    // NOTE: It is not straightforward
    fillList<Expression>(list, ctx->bracketed_expression_list()->expression());

    return retPtr<Expression, MethodCallExpression>(std::move(object), methodName, std::move(list));
}

std::any AltVisitor::visitFunctionCallExpression(AltCliParser::FunctionCallExpressionContext* ctx)
{
    const auto procedureName = ctx->algorithm()->getText();
    std::vector<std::unique_ptr<Expression>> list;
    // NOTE: It is not straightforward
    fillList<Expression>(list, ctx->bracketed_expression_list()->expression());

    return retPtr<Expression, FunctionCallExpression>(procedureName, std::move(list));
}

std::any AltVisitor::visitVariableExpression(AltCliParser::VariableExpressionContext* ctx)
{
    auto name = castToUnique<Arg>(visit(ctx->variable()));

    return retPtr<Expression, VariableExpression>(std::move(name));
}

std::any AltVisitor::visitStringImmediateExpression(AltCliParser::StringImmediateExpressionContext* ctx)
{
    return retPtr<Expression, ImmediateExpression<std::string>>(cast<std::string>(visit(ctx->string())));
}

std::any AltVisitor::visitIntegerImmediateExpression(AltCliParser::IntegerImmediateExpressionContext* ctx)
{
    return retPtr<Expression, ImmediateExpression<int>>(visitInt(ctx->INTEGER()));
}

std::any AltVisitor::visitDoubleImmediateExpression(AltCliParser::DoubleImmediateExpressionContext* ctx)
{
    return retPtr<Expression, ImmediateExpression<double>>(visitDouble(ctx->DOUBLE()));
}
}
