#pragma once

#include <concepts>
#include <source_location>

#include <ext/algorithm>

#include <ast/Arg.h>
#include <ast/Expression.h>
#include <ast/Option.h>
#include <ast/Statement.h>

#include <ast/options/CategoryOption.h>
#include <ast/options/TypeOption.h>

#include <ast/command/CommandList.h>

#include <ast/statements/StatementList.h>

#include <exception/CommonException.h>

#include <AltCliParserBaseVisitor.h>
#include "global/GlobalData.h"

using namespace cli::grammar::parser;

namespace cli {
struct AltVisitor : public AltCliParserBaseVisitor {
    std::unique_ptr<CommandList> transform(AltCliParser::ParseContext* ctx);

    std::any visitParse(AltCliParser::ParseContext* ctx) override;

    std::any visitArg(AltCliParser::ArgContext* ctx) override;

    std::any visitType(AltCliParser::TypeContext* ctx) override;

    std::any visitPrint(AltCliParser::PrintContext* ctx) override;

    std::any visitBatchOrExpression(AltCliParser::BatchOrExpressionContext* ctx) override;

    std::any visitExpressionOrBatch(AltCliParser::ExpressionOrBatchContext* ctx) override;

    std::any visitStringImmediateStatement(AltCliParser::StringImmediateStatementContext* ctx) override;

    std::any visitIntegerImmediateStatement(AltCliParser::IntegerImmediateStatementContext* ctx) override;

    std::any visitDoubleImmediateStatement(AltCliParser::DoubleImmediateStatementContext* ctx) override;

    std::any visitCommonStatement(AltCliParser::CommonStatementContext* ctx) override;

    std::any visitStatementList(AltCliParser::StatementListContext* ctx) override;

    std::any visitBatch(AltCliParser::BatchContext* ctx) override;

    std::any visitBlock(AltCliParser::BlockContext* ctx) override;

    std::any visitBlockSemicolonCommand(AltCliParser::BlockSemicolonCommandContext* ctx) override;

    std::any visitIfSemicolonCommand(AltCliParser::IfSemicolonCommandContext* ctx) override;

    std::any visitWhileSemicolonCommand(AltCliParser::WhileSemicolonCommandContext* ctx) override;

    std::any visitSemicolonCommand(AltCliParser::SemicolonCommandContext* ctx) override;

    std::any visitQuit(AltCliParser::QuitContext* ctx) override;

    std::any visitSingleStatement(AltCliParser::SingleStatementContext* ctx) override;

    std::any visitTypeOption(AltCliParser::TypeOptionContext* ctx) override;

    std::any visitOption(AltCliParser::OptionContext* ctx) override;

    std::any visitCategoryOption(AltCliParser::CategoryOptionContext* ctx) override;

    std::any visitPreviousResultParam(AltCliParser::PreviousResultParamContext* ctx) override;

    std::any visitStatementInRedirect(AltCliParser::StatementInRedirectContext* ctx) override;

    std::any visitFileInRedirect(AltCliParser::FileInRedirectContext* ctx) override;

    std::any visitCastStatement(AltCliParser::CastStatementContext* ctx) override;

    std::any visitBlockCommand(AltCliParser::BlockCommandContext* ctx) override;

    std::any visitIfCommand(AltCliParser::IfCommandContext* ctx) override;

    std::any visitWhileCommand(AltCliParser::WhileCommandContext* ctx) override;

    std::any visitStringImmediateExpression(AltCliParser::StringImmediateExpressionContext* ctx) override;

    std::any visitIntegerImmediateExpression(AltCliParser::IntegerImmediateExpressionContext* ctx) override;

    std::any visitDoubleImmediateExpression(AltCliParser::DoubleImmediateExpressionContext* ctx) override;

    std::any visitPostfixExpression(AltCliParser::PostfixExpressionContext* ctx) override;

    std::any visitBinaryExpression(AltCliParser::BinaryExpressionContext* ctx) override;

    std::any visitPrefixExpression(AltCliParser::PrefixExpressionContext* ctx) override;

    std::any visitCastPrefixExpression(AltCliParser::CastPrefixExpressionContext* ctx) override;

    std::any visitActualTypeExpression(AltCliParser::ActualTypeExpressionContext* ctx) override;

    std::any visitAtomExpression(AltCliParser::AtomExpressionContext* ctx) override;

    std::any visitMethodCallExpression(AltCliParser::MethodCallExpressionContext* ctx) override;

    std::any visitFunctionCallExpression(AltCliParser::FunctionCallExpressionContext* ctx) override;

    std::any visitVariableExpression(AltCliParser::VariableExpressionContext* ctx) override;

    std::any visitDeclare(AltCliParser::DeclareContext* ctx) override;

    std::any visitCycleControl(AltCliParser::CycleControlContext* ctx) override;

    std::any visitExecute(AltCliParser::ExecuteContext* ctx) override;

    std::any visitReturn(AltCliParser::ReturnContext* ctx) override;

    std::any visitImmediateParam(AltCliParser::ImmediateParamContext* ctx) override;

    std::any visitResultVariableStatement(AltCliParser::ResultVariableStatementContext* ctx) override;

    std::any visitResultFileStatement(AltCliParser::ResultFileStatementContext* ctx) override;

    std::any visitVariableStatement(AltCliParser::VariableStatementContext* ctx) override;

    std::any visitCastParam(AltCliParser::CastParamContext* ctx) override;

    std::any visitHelp(AltCliParser::HelpContext* ctx) override;

    std::any visitFunction(AltCliParser::FunctionContext* ctx) override;

    std::any visitProcedure(AltCliParser::ProcedureContext* ctx) override;

    std::any visitShowMeasurement(AltCliParser::ShowMeasurementContext* ctx) override;

    std::any visitStopMeasurement(AltCliParser::StopMeasurementContext* ctx) override;

    std::any visitClearMeasurement(AltCliParser::ClearMeasurementContext* ctx) override;

    std::any visitStartMeasurement(AltCliParser::StartMeasurementContext* ctx) override;

    std::any visitEval(AltCliParser::EvalContext* ctx) override;

    std::any visitValueStatement(AltCliParser::ValueStatementContext* ctx) override;

    std::any visitUndeclareFunction(AltCliParser::UndeclareFunctionContext* ctx) override;

    std::any visitSet(AltCliParser::SetContext* ctx) override;

    std::any visitInterpret(AltCliParser::InterpretContext* ctx) override;

    std::any visitLoad(AltCliParser::LoadContext* ctx) override;

    std::any visitIntrospectAst(AltCliParser::IntrospectAstContext* ctx) override;

    std::any visitIntrospectAstFile(AltCliParser::IntrospectAstFileContext* ctx) override;

    std::any visitCalc(AltCliParser::CalcContext* ctx) override;

    std::any visitIntrospectFromTo(AltCliParser::IntrospectFromToContext* ctx) override;

    std::any visitIntrospectAlgorithms(AltCliParser::IntrospectAlgorithmsContext* ctx) override;

    std::any visitIntrospectOverloads(AltCliParser::IntrospectOverloadsContext* ctx) override;

    std::any visitIntrospectOperators(AltCliParser::IntrospectOperatorsContext* ctx) override;

    std::any visitIntrospectDatatypes(AltCliParser::IntrospectDatatypesContext* ctx) override;

    std::any visitIntrospectCasts(AltCliParser::IntrospectCastsContext* ctx) override;

    std::any visitIntrospectNormalizations(AltCliParser::IntrospectNormalizationsContext* ctx) override;

    std::any visitIntrospectDeormalizations(AltCliParser::IntrospectDeormalizationsContext* ctx) override;

    std::any visitIntrospectVariables(AltCliParser::IntrospectVariablesContext* ctx) override;

    std::any visitIntrospectBindings(AltCliParser::IntrospectBindingsContext* ctx) override;

    std::any visitVariable(AltCliParser::VariableContext* ctx) override;

    std::any visitBinding(AltCliParser::BindingContext* ctx) override;

    std::any visitAlgorithm_group(AltCliParser::Algorithm_groupContext* ctx) override;

    std::any visitDatatype_group(AltCliParser::Datatype_groupContext* ctx) override;

    std::any visitFile(AltCliParser::FileContext* ctx) override;

    std::any visitAlgorithm(AltCliParser::AlgorithmContext* ctx) override;

    std::any visitString(AltCliParser::StringContext* ctx) override;

    std::any visitContainerStatement(AltCliParser::ContainerStatementContext* ctx) override;

protected:
    std::any defaultResult() override;

    std::any aggregateResult(std::any previousResult, std::any nextResult) override;

private:
    std::pair<abstraction::TypeQualifiers::TypeQualifierSet, std::unique_ptr<Arg>>
    visitQualifiedType(AltCliParser::Qualified_typeContext* ctx);

    std::vector<std::pair<abstraction::TypeQualifiers::TypeQualifierSet, std::unique_ptr<Arg>>>
    visitRunnableParams(const std::vector<AltCliParser::Runnable_paramContext*>& ctx);

    static int visitInt(antlr4::tree::TerminalNode* node);

    static double visitDouble(antlr4::tree::TerminalNode* node);

    static std::string getRawText(antlr4::ParserRuleContext* ctx);

    template <typename T>
    T cast(const std::any& val, const std::source_location location = std::source_location::current()) const
    {
        try {
            return std::any_cast<T>(val);
        } catch (const std::bad_any_cast&) {
            std::ostringstream out;
            out << "Bad any cast: " << std::endl;
            out << val.type().name() << " with " << typeid(T).name() << std::endl;
            out << "At " << location.file_name() << ":" << location.line() << std::endl;
            out << "\t" << location.function_name() << std::endl;

            throw std::logic_error(out.str());
        }
    }

    template <typename T>
    std::shared_ptr<T> castToShared(
        const std::any& val,
        const std::source_location location = std::source_location::current()) const
    {
        return std::shared_ptr<T>(cast<T*>(val, location));
    }

    template <typename T>
    std::unique_ptr<T> castToUnique(
        const std::any& val,
        const std::source_location location = std::source_location::current()) const
    {
        return std::unique_ptr<T>(cast<T*>(val, location));
    }

    template <typename Base, std::derived_from<Base> T, class... Args>
    Base* retPtr(Args&&... args)
    {
        return new T(std::forward<Args>(args)...);
    }

    template <class Type, class OutVectorType, class InVectorType>
    void fillList(std::vector<OutVectorType>& outVector, const std::vector<InVectorType>& inVector, const std::source_location location = std::source_location::current())
    {
        for (auto& ctx : inVector) {
            auto any = visit(ctx);
            try {
                // If OutVectorType is std::shared_ptr<Type>, then std::unique_ptr is promoted
                outVector.emplace_back(castToUnique<Type>(any, location));
            } catch (const std::exception& ex) {
                throw;
            }
        }
    }

    [[noreturn]] static void invalidParse(const std::string& message, const std::source_location location = std::source_location::current());

    static std::map<std::string, abstraction::Operators::BinaryOperators> binaryExpressionMap;
    static std::map<std::string, abstraction::Operators::PostfixOperators> postfixExpressionMap;
    static std::map<std::string, abstraction::Operators::PrefixOperators> prefixExpressionMap;
};
}
