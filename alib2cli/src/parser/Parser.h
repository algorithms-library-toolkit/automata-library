#pragma once

#include <antlr4-runtime.h>
#include <ast/command/CommandList.h>
#include <istream>
#include <string>

namespace cli {

struct Parser {
    static std::unique_ptr<CommandList> parseString(const std::string& unit);
    static std::unique_ptr<CommandList> parseStream(std::istream& istream);

    class ContinueException final : public std::exception {
    };

private:
    static std::unique_ptr<CommandList> parse(antlr4::CharStream& stream);
};

} // cli
