#include <string>

#include <parser/AltVisitor.h>

using namespace cli::grammar::parser;
using namespace abstraction;

namespace cli {
std::map<std::string, Operators::BinaryOperators> AltVisitor::binaryExpressionMap = {
    {"&&", Operators::BinaryOperators::LOGICAL_AND},
    {"||", Operators::BinaryOperators::LOGICAL_OR},
    {"^", Operators::BinaryOperators::BINARY_XOR},
    {"&", Operators::BinaryOperators::BINARY_AND},
    {"|", Operators::BinaryOperators::BINARY_OR},
    {"+", Operators::BinaryOperators::ADD},
    {"-", Operators::BinaryOperators::SUB},
    {"*", Operators::BinaryOperators::MUL},
    {"%", Operators::BinaryOperators::MOD},
    {"/", Operators::BinaryOperators::DIV},
    {"==", Operators::BinaryOperators::EQUALS},
    {"!=", Operators::BinaryOperators::NOT_EQUALS},
    {"<", Operators::BinaryOperators::LESS},
    {"<=", Operators::BinaryOperators::LESS_OR_EQUAL},
    {">", Operators::BinaryOperators::MORE},
    {">=", Operators::BinaryOperators::MORE_OR_EQUAL},
    {"=", Operators::BinaryOperators::ASSIGN}};

std::map<std::string, Operators::PrefixOperators> AltVisitor::prefixExpressionMap = {
    {"+", Operators::PrefixOperators::PLUS},
    {"-", Operators::PrefixOperators::MINUS},
    {"!", Operators::PrefixOperators::LOGICAL_NOT},
    {"~", Operators::PrefixOperators::BINARY_NEG},
    {"++", Operators::PrefixOperators::INCREMENT},
    {"--", Operators::PrefixOperators::DECREMENT}};

std::map<std::string, Operators::PostfixOperators> AltVisitor::postfixExpressionMap = {
    {"--", Operators::PostfixOperators::DECREMENT},
    {"++", Operators::PostfixOperators::INCREMENT}};

std::unique_ptr<CommandList> AltVisitor::transform(AltCliParser::ParseContext* ctx)
{
    return castToUnique<CommandList>(visitParse(ctx));
}

int AltVisitor::visitInt(antlr4::tree::TerminalNode* node)
{
    return std::stoi(node->getText());
}

double AltVisitor::visitDouble(antlr4::tree::TerminalNode* node)
{
    return std::stod(node->getText());
}

std::any AltVisitor::visitString(AltCliParser::StringContext* ctx)
{
    std::string text = ctx->getText();
    if (text[0] == '"' && text[text.length() - 1] == '"' && text.size() > 1)
        text = text.substr(1, text.size() - 2);

    std::string res;
    auto iter = text.begin();
    while (iter != text.end()) {
        char curr = *iter++;
        if (curr == '\\' && iter != text.end()) {
            char escapeChar = *iter++;
            switch (escapeChar) {
            case '\\':
                curr = '\\';
                break;
            case '"':
                curr = '"';
                break;
            case 'n':
                curr = '\n';
                break;
            case 't':
                curr = '\t';
                break;
            // all other escapes
            default:
                curr = escapeChar;
                // invalid escape sequence
                break;
            }
        }
        res += curr;
    }

    return res;
}

std::any AltVisitor::defaultResult()
{
    return std::any();
}

std::any AltVisitor::aggregateResult(std::any previousResult, std::any nextResult)
{
    if (!previousResult.has_value())
        return nextResult;

    throw exception::CommonException("Panic: We cant merge two any values, please write appropriate visitor");
}

std::string AltVisitor::getRawText(antlr4::ParserRuleContext* ctx)
{
    if (ctx->start == nullptr || ctx->stop == nullptr)
        return ctx->getText(); // Fallback

    return ctx->start->getInputStream()->getText(antlr4::misc::Interval(ctx->start->getStartIndex(), ctx->stop->getStopIndex()));
}

[[noreturn]] void AltVisitor::invalidParse(const std::string& message, const std::source_location location)
{
    throw std::logic_error(message + " at " + std::string(location.file_name()) + ":" + std::to_string(location.line()));
}
}