#pragma once

#include <ast/Statement.h>

namespace cli {

class ResultVariableStatement final : public Statement {
    std::unique_ptr<cli::Arg> m_name;

public:
    ResultVariableStatement(std::unique_ptr<cli::Arg> name)
        : m_name(std::move(name))
    {
    }

    std::shared_ptr<abstraction::Value> translateAndEval(const std::shared_ptr<abstraction::Value>& prev, Environment& environment) const override
    {
        std::shared_ptr<abstraction::Value> res = prev->clone(abstraction::TypeQualifiers::TypeQualifierSet::NONE, false);
        environment.setVariable(m_name->eval(environment), res);
        return res;
    }

    std::ostream& print(std::ostream& out) const override
    {
        return out << "(ResultVariableStatement " << m_name << ")";
    }
};

} /* namespace cli */
