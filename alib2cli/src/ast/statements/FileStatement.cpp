#include <ast/Arg.h>
#include <ast/Option.h>
#include <ast/Statement.h>
#include <ast/statements/FileStatement.h>

#include <registry/InputFileRegistry.hpp>

#include <common/EvalHelper.h>

namespace cli {

FileStatement::FileStatement(std::unique_ptr<Arg> file, std::unique_ptr<Arg> fileType, std::unique_ptr<TypeOption> type)
    : m_file(std::move(file))
    , m_fileType(std::move(fileType))
    , m_type(std::move(type))
{
}

std::shared_ptr<abstraction::Value> FileStatement::translateAndEval(const std::shared_ptr<abstraction::Value>&, Environment& environment) const
{
    std::string filetype = "xml";
    if (m_fileType)
        filetype = m_fileType->eval(environment);

    core::type_details type = core::type_details::universal_type();
    if (m_type)
        type = core::type_details::as_type(m_type->getType());

    std::unique_ptr<abstraction::OperationAbstraction> res = abstraction::InputFileRegistry::getAbstraction(filetype, type);

    ext::vector<std::shared_ptr<abstraction::Value>> params;
    params.push_back(std::make_shared<abstraction::ValueHolder<object::Object>>(object::ObjectFactory<>::construct(m_file->eval(environment)), true));

    return abstraction::EvalHelper::evalAbstraction(environment, std::move(res), params);
}

std::ostream& FileStatement::print(std::ostream& out) const
{
    return out << "(FileStatement " << m_file << " " << m_fileType << " " << m_type << ")";
}

} /* namespace cli */
