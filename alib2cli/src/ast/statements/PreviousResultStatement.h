#pragma once

#include <ast/Statement.h>

namespace cli {

class PreviousResultStatement final : public Statement {

public:
    std::shared_ptr<abstraction::Value> translateAndEval(const std::shared_ptr<abstraction::Value>& prev, Environment&) const override
    {
        if (prev == nullptr)
            throw std::invalid_argument("There is no previous result to use.");
        return prev;
    }

    std::ostream& print(std::ostream& out) const override
    {
        return out << "(PreviousResultStatement)";
    }
};

} /* namespace cli */
