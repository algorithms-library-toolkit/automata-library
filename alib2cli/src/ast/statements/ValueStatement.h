#pragma once

#include <abstraction/ValueHolder.hpp>
#include <ast/Statement.h>

namespace cli {

class ValueStatement final : public Statement {
    std::unique_ptr<cli::Arg> m_arg;

public:
    ValueStatement(std::unique_ptr<cli::Arg> arg)
        : m_arg(std::move(arg))
    {
    }

    std::shared_ptr<abstraction::Value> translateAndEval(const std::shared_ptr<abstraction::Value>&, Environment& environment) const override
    {
        return std::make_shared<abstraction::ValueHolder<std::string>>(m_arg->eval(environment), true);
    }

    std::ostream& print(std::ostream& out) const override
    {
        return out << "(ValueStatement " << m_arg << ")";
    }
};

} /* namespace cli */
