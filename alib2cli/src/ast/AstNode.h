#pragma once

#include <concepts>
#include <memory>
#include <ostream>

namespace cli {

class AstNode {
protected:
    virtual std::ostream& print(std::ostream& out) const = 0;

public:
    virtual ~AstNode() = default;

    friend std::ostream& operator<<(std::ostream& out, const AstNode& node)
    {
        return node.print(out);
    }

    template <std::derived_from<AstNode> T>
    friend std::ostream& operator<<(std::ostream& out, std::shared_ptr<T> const& node)
    {
        if (node != nullptr)
            return node->print(out);
        return out << "(null)";
    }

    template <std::derived_from<AstNode> T>
    friend std::ostream& operator<<(std::ostream& out, std::unique_ptr<T> const& node)
    {
        if (node != nullptr)
            return node->print(out);
        return out << "(null)";
    }
};

} /* namespace cli */
