#pragma once

#include <memory>
#include <ostream>

#include <ast/AstNode.h>
#include <ast/command/CommandResult.h>

namespace cli {

class Environment;

class Command : public AstNode {
public:
    virtual CommandResult run(Environment& environment) const = 0;
};

} /* namespace cli */
