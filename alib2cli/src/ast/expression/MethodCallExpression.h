#pragma once

#include <ext/foreach>

#include <ast/Expression.h>
#include <environment/Environment.h>

namespace cli {

class MethodCallExpression : public Expression {
    std::unique_ptr<Expression> m_object;
    std::string m_name;
    std::vector<std::unique_ptr<Expression>> m_params;

public:
    MethodCallExpression(std::unique_ptr<Expression> object, std::string name, std::vector<std::unique_ptr<Expression>> params)
        : m_object(std::move(object))
        , m_name(std::move(name))
        , m_params(std::move(params))
    {
    }

    std::shared_ptr<abstraction::Value> translateAndEval(Environment& environment) const override
    {
        ext::vector<std::shared_ptr<abstraction::Value>> params;
        params.push_back(m_object->translateAndEval(environment));
        for (const std::unique_ptr<Expression>& param : m_params) {
            params.push_back(param->translateAndEval(environment));
        }

        std::string name = m_name;

        ext::vector<std::string> templateParams;
        /*		for ( const std::unique_ptr < cli::Arg > & templateParam : m_templateParams )
                                templateParams.push_back ( templateParam->eval ( environment ) );*/

        abstraction::AlgorithmCategories::AlgorithmCategory category = abstraction::AlgorithmCategories::AlgorithmCategory::NONE;
        /*		if ( m_category )
                                category = m_category->getCategory ( );*/

        return abstraction::EvalHelper::evalAlgorithm(environment, name, templateParams, params, category);
    }

    std::ostream& print(std::ostream& out) const override
    {
        out << "(MethodCallExpression " << m_object << " " << m_name;
        for (const auto& param : m_params) {
            out << " " << param;
        }
        return out << ")";
    }
};

} /* namespace cli */
