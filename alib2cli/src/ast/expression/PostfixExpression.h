#pragma once

#include <ast/Expression.h>

namespace cli {

class PostfixExpression final : public Expression {
    abstraction::Operators::PostfixOperators m_operation;
    std::unique_ptr<Expression> m_expr;

public:
    PostfixExpression(abstraction::Operators::PostfixOperators operation, std::unique_ptr<Expression> expr)
        : m_operation(operation)
        , m_expr(std::move(expr))
    {
    }

    std::shared_ptr<abstraction::Value> translateAndEval(Environment& environment) const override
    {
        ext::vector<std::shared_ptr<abstraction::Value>> params;
        params.push_back(m_expr->translateAndEval(environment));

        abstraction::AlgorithmCategories::AlgorithmCategory category = abstraction::AlgorithmCategories::AlgorithmCategory::NONE;

        return abstraction::EvalHelper::evalOperator(environment, m_operation, params, category);
    }

    std::ostream& print(std::ostream& out) const override
    {
        return out << "(PostfixExpression " << m_operation << " " << m_expr << ")";
    }
};

} /* namespace cli */
