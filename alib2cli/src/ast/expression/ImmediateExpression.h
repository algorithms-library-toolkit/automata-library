#pragma once

#include <ast/Expression.h>
#include <object/ObjectFactory.h>

namespace cli {

template <class Type>
class ImmediateExpression final : public Expression {
    Type m_value;

public:
    ImmediateExpression(Type value)
        : m_value(std::move(value))
    {
    }

    std::shared_ptr<abstraction::Value> translateAndEval(Environment&) const override
    {
        return std::make_shared<abstraction::ValueHolder<object::Object>>(object::ObjectFactory<>::construct(m_value), true);
    }
    std::ostream& print(std::ostream& out) const override
    {
        return out << "(ImmediateExpression " << m_value << ")";
    }
};

} /* namespace cli */
