#pragma once

#include <alib/string>
#include <ast/Expression.h>

namespace cli {

class VariableExpression final : public Expression {
    std::unique_ptr<cli::Arg> m_name;

public:
    VariableExpression(std::unique_ptr<cli::Arg> name)
        : m_name(std::move(name))
    {
    }

    std::shared_ptr<abstraction::Value> translateAndEval(Environment& environment) const override
    {
        return environment.getVariable(m_name->eval(environment));
    }

    std::ostream& print(std::ostream& out) const override
    {
        return out << "(VariableExpression " << m_name << ")";
    }
};

} /* namespace cli */
