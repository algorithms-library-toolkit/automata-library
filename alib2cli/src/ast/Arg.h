#pragma once

#include <memory>
#include <ostream>

#include <ast/Ast.h>
#include <environment/Environment.h>

namespace cli {

class Arg : public AstNode {
public:
    virtual std::string eval(Environment& environment) const = 0;
};

} /* namespace cli */
