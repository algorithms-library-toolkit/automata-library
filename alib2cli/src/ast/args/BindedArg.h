#pragma once

#include <alib/string>
#include <ast/Arg.h>

namespace cli {

class BindedArg final : public Arg {
    std::string m_bind;

public:
    BindedArg(std::string bind)
        : m_bind(std::move(bind))
    {
    }

    std::string eval(Environment& environment) const override
    {
        return environment.getBinding(m_bind);
    }

    std::ostream& print(std::ostream& out) const override
    {
        return out << "(BindedArg " << m_bind << ")";
    }
};

} /* namespace cli */
