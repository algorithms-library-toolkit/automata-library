#pragma once

#include <alib/string>
#include <ast/Arg.h>

namespace cli {

class ImmediateArg final : public Arg {
    std::string m_value;

public:
    ImmediateArg(std::string value)
        : m_value(std::move(value))
    {
    }

    std::string eval(Environment&) const override
    {
        return m_value;
    }

    std::ostream& print(std::ostream& out) const override
    {
        return out << "(ImmediateArg " << m_value << ")";
    }
};

} /* namespace cli */
