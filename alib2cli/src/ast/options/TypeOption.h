#pragma once

#include <ast/Option.h>
#include <exception/CommonException.h>

namespace cli {

class TypeOption final : public Option {
    std::string m_type;

public:
    TypeOption(std::string type)
        : m_type(std::move(type))
    {
    }

    const std::string& getType() const
    {
        return m_type;
    }

    std::ostream& print(std::ostream& out) const override
    {
        return out << "(TypeOption " << m_type << ")";
    }

    friend std::ostream& operator<<(std::ostream& out, std::shared_ptr<TypeOption> const& option)
    {
        if (option != nullptr)
            return option->print(out);
        return out << "(null)";
    }

    friend std::ostream& operator<<(std::ostream& out, std::unique_ptr<TypeOption> const& option)
    {
        if (option != nullptr)
            return option->print(out);
        return out << "(null)";
    }
};

} /* namespace cli */
