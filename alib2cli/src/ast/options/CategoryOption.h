#pragma once

#include <ast/Option.h>
#include <common/AlgorithmCategories.hpp>

namespace cli {

class CategoryOption final : public Option {
    std::string m_key;

public:
    CategoryOption(std::string key)
        : m_key(std::move(key))
    {
    }

    abstraction::AlgorithmCategories::AlgorithmCategory getCategory() const
    {
        return abstraction::AlgorithmCategories::algorithmCategory(m_key);
    }

    std::ostream& print(std::ostream& out) const override
    {
        return out << "(CategoryOption " << m_key << ")";
    }

    friend std::ostream& operator<<(std::ostream& out, std::shared_ptr<CategoryOption> const& option)
    {
        if (option != nullptr)
            return option->print(out);
        return out << "(null)";
    }

    friend std::ostream& operator<<(std::ostream& out, std::unique_ptr<CategoryOption> const& option)
    {
        if (option != nullptr)
            return option->print(out);
        return out << "(null)";
    }
};

} /* namespace cli */
