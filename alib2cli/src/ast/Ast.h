#pragma once

#include <ast/AstNode.h>

namespace cli {

class Option;
class Statement;
class Arg;

} /* namespace cli */
