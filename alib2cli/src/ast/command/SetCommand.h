#pragma once

#include <ast/Command.h>
#include <environment/Environment.h>

namespace cli {

class SetCommand : public Command {
    std::string m_param;
    std::string m_value;

public:
    SetCommand(std::string param, std::string value)
        : m_param(std::move(param))
        , m_value(std::move(value))
    {
    }

    CommandResult run(Environment&) const override;

    std::ostream& print(std::ostream& out) const override;
};

} /* namespace cli */
