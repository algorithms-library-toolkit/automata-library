#pragma once

#include <ast/Command.h>
#include <environment/Environment.h>

#include <iostream>
#include <readline/StringLineInterface.h>

namespace cli {

class EvalCommand : public Command {
    std::string m_code;

public:
    EvalCommand(std::string code)
        : m_code(std::move(code))
    {
    }

    CommandResult run(Environment& environment) const override
    {
        CommandResult state = environment.execute(m_code);

        if (state != cli::CommandResult::QUIT)
            state = cli::CommandResult::OK;

        return state;
    }

    std::ostream& print(std::ostream& out) const override
    {
        return out << "(EvalCommand " << m_code << ")";
    }
};

} /* namespace cli */
