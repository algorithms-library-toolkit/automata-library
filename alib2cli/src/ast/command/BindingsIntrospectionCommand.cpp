#include "BindingsIntrospectionCommand.h"

#include <global/GlobalData.h>

namespace cli {

CommandResult BindingsIntrospectionCommand::run(Environment& environment) const
{
    std::string param;
    if (m_param != nullptr)
        param = m_param->eval(environment);

    if (param.empty())
        for (const std::string& name : environment.getBindingNames())
            common::Streams::out << name << std::endl;
    else
        common::Streams::out << param << " " << environment.getBinding(param) << std::endl;

    return CommandResult::OK;
}

std::ostream& BindingsIntrospectionCommand::print(std::ostream& out) const
{
    return out << "(BindingsIntrospectionCommand " << m_param << ")";
}

} /* namespace cli */
