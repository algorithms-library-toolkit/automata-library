#include "DeclareRunnableCommand.h"

#include <ext/foreach>

#include <ast/Statement.h>
#include <ast/args/ImmediateArg.h>
#include <common/CastHelper.h>
#include <registration/AlgoRegistration.hpp>

namespace cli {

DeclareRunnableCommand::DeclareRunnableCommand(std::string name, std::vector<std::pair<abstraction::TypeQualifiers::TypeQualifierSet, std::unique_ptr<Arg>>> params, std::pair<abstraction::TypeQualifiers::TypeQualifierSet, std::unique_ptr<Arg>> result, std::shared_ptr<Command> body)
    : m_name(std::move(name))
    , m_params(std::move(params))
    , m_result(std::move(result))
    , m_body(std::move(body))
{
}

DeclareRunnableCommand::DeclareRunnableCommand(std::string name, std::vector<std::pair<abstraction::TypeQualifiers::TypeQualifierSet, std::unique_ptr<Arg>>> params, std::shared_ptr<Command> body)
    : DeclareRunnableCommand(std::move(name), std::move(params), std::make_pair(abstraction::TypeQualifiers::TypeQualifierSet::NONE, std::make_unique<ImmediateArg>("void")), std::move(body))
{
}

CommandResult DeclareRunnableCommand::run(Environment& environment) const
{
    std::vector<std::pair<abstraction::TypeQualifiers::TypeQualifierSet, std::string>> params;
    ext::vector<ext::tuple<core::type_details, abstraction::TypeQualifiers::TypeQualifierSet, std::string>> paramSpecs;

    for (const std::pair<abstraction::TypeQualifiers::TypeQualifierSet, std::unique_ptr<Arg>>& param : m_params) {
        params.emplace_back(param.first, param.second->eval(environment));
        paramSpecs.emplace_back(core::type_details::universal_type(), param.first, param.second->eval(environment));
    }

    ext::pair<core::type_details, abstraction::TypeQualifiers::TypeQualifierSet> resultSpec = ext::make_pair(core::type_details::as_type(m_result.second->eval(environment)), m_result.first);
    std::shared_ptr<Command> body = m_body;

    std::function<std::shared_ptr<abstraction::Value>(const std::vector<std::shared_ptr<abstraction::Value>>&)> callback = [=](const std::vector<std::shared_ptr<abstraction::Value>>& actualParams) {
        Environment newEnvironment;
        newEnvironment.setResult(std::make_shared<abstraction::Void>());
        for (const ext::tuple<const std::shared_ptr<abstraction::Value>&, const std::pair<abstraction::TypeQualifiers::TypeQualifierSet, std::string>&>& params_bind : ext::make_tuple_foreach(actualParams, params)) {
            std::shared_ptr<abstraction::Value> res = std::get<0>(params_bind)->clone(std::get<1>(params_bind).first, false);
            newEnvironment.setVariable(std::get<1>(params_bind).second, res);
        }

        body->run(newEnvironment);
        std::shared_ptr<abstraction::Value> result = newEnvironment.getResult();

        if (resultSpec.first == core::type_details::void_type() && result->getDeclaredType() != core::type_details::void_type())
            throw std::logic_error("Non void type returned from void routine");

        if (result->getDeclaredType() != core::type_details::void_type()) {
            bool isResultLvalueRef = abstraction::TypeQualifiers::isLvalueRef(result->getTypeQualifiers());
            result = result->clone(resultSpec.second, !isResultLvalueRef);
        }

        return result;
    };

    abstraction::AlgorithmRegistry::registerRaw(m_name, callback, resultSpec, paramSpecs);
    return CommandResult::OK;
}

std::ostream& DeclareRunnableCommand::print(std::ostream& out) const
{
    out << "(DeclareRunnableCommand " << m_name;

    for (const auto& param : m_params) {
        out << " (Param " << param.first << " " << param.second << ")";
    }

    return out << " (Result " << m_result.first << " " << m_result.second << ") " << m_body << ")";
}

} /* namespace cli */
