#pragma once

#include <ast/Command.h>
#include <environment/Environment.h>

#include <alib/measure>

namespace cli {

class ShowMeasurements : public Command {
    measurements::MeasurementFormat m_format;

public:
    ShowMeasurements(measurements::MeasurementFormat format)
        : m_format(format)
    {
    }

    CommandResult run(Environment&) const override;

    std::ostream& print(std::ostream& out) const override;
};

} /* namespace cli */
