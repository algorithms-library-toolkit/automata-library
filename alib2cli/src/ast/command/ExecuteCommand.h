#pragma once

#include <ast/Command.h>
#include <ast/Expression.h>
#include <environment/Environment.h>

namespace cli {

class ExecuteCommand : public Command {
    std::unique_ptr<Expression> m_expr;

public:
    ExecuteCommand(std::unique_ptr<Expression> expr)
        : m_expr(std::move(expr))
    {
    }

    CommandResult run(Environment& environment) const override
    {
        m_expr->translateAndEval(environment);
        return CommandResult::OK;
    }


    std::ostream& print(std::ostream& out) const override
    {
        return out << "(ExecuteCommand " << m_expr << ")";
    }
};

} /* namespace cli */
