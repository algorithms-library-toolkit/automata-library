#pragma once

#include <ast/Command.h>
#include <ast/Expression.h>
#include <environment/Environment.h>

namespace cli {

class QuitCommand : public Command {
    std::unique_ptr<Expression> m_expr;

public:
    QuitCommand(std::unique_ptr<Expression> expr)
        : m_expr(std::move(expr))
    {
    }

    CommandResult run(Environment& environment) const override
    {
        if (m_expr)
            environment.setResult(m_expr->translateAndEval(environment));

        return CommandResult::QUIT;
    }

    std::ostream& print(std::ostream& out) const override
    {
        return out << "(QuitCommand " << m_expr << ")";
    }
};

} /* namespace cli */
