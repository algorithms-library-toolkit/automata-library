#pragma once

#include <ast/Arg.h>
#include <ast/Command.h>
#include <environment/Environment.h>

namespace cli {

class OverloadsIntrospectionCommand : public Command {
    std::unique_ptr<cli::Arg> m_param;
    ext::vector<std::unique_ptr<cli::Arg>> m_templateParams;

    static void typePrint(const ext::pair<core::type_details, abstraction::TypeQualifiers::TypeQualifierSet>& result, ext::ostream& os);

public:
    OverloadsIntrospectionCommand(std::unique_ptr<cli::Arg> param, ext::vector<std::unique_ptr<cli::Arg>> templateParams)
        : m_param(std::move(param))
        , m_templateParams(std::move(templateParams))
    {
    }

    CommandResult run(Environment& environment) const override;

    std::ostream& print(std::ostream& out) const override;
};

} /* namespace cli */
