#pragma once

#include <ast/Command.h>
#include <ast/Expression.h>
#include <environment/Environment.h>

namespace cli {

class ReturnCommand : public Command {
    std::unique_ptr<Expression> m_command;

public:
    ReturnCommand(std::unique_ptr<Expression> command)
        : m_command(std::move(command))
    {
    }

    CommandResult run(Environment& environment) const override
    {
        if (m_command)
            environment.setResult(m_command->translateAndEval(environment));

        return CommandResult::RETURN;
    }

    std::ostream& print(std::ostream& out) const override
    {
        return out << "(ReturnCommand " << m_command << ")";
    }
};

} /* namespace cli */
