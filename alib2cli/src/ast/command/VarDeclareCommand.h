#pragma once

#include <ast/Command.h>
#include <environment/Environment.h>

namespace cli {

class VarDeclareCommand : public Command {
    std::unique_ptr<cli::Arg> m_name;
    abstraction::TypeQualifiers::TypeQualifierSet m_typeQualifiers;
    std::unique_ptr<Expression> m_expr;

public:
    VarDeclareCommand(std::unique_ptr<cli::Arg> name, abstraction::TypeQualifiers::TypeQualifierSet typeQualifiers, std::unique_ptr<Expression> expr)
        : m_name(std::move(name))
        , m_typeQualifiers(typeQualifiers)
        , m_expr(std::move(expr))
    {
    }

    CommandResult run(Environment& environment) const override
    {
        std::shared_ptr<abstraction::Value> value = m_expr->translateAndEval(environment);
        std::shared_ptr<abstraction::Value> res = value->clone(m_typeQualifiers, false);
        environment.setVariable(m_name->eval(environment), res);
        return CommandResult::OK;
    }

    std::ostream& print(std::ostream& out) const override
    {
        return out << "(VarDeclareCommand " << m_name << " "
                   << m_typeQualifiers << " " << m_expr << ")";
    }
};

} /* namespace cli */
