#pragma once

#include <ast/Arg.h>
#include <ast/Command.h>
#include <environment/Environment.h>

namespace cli {

class DeclareRunnableCommand : public Command {
    std::string m_name;
    std::vector<std::pair<abstraction::TypeQualifiers::TypeQualifierSet, std::unique_ptr<Arg>>> m_params;
    std::pair<abstraction::TypeQualifiers::TypeQualifierSet, std::unique_ptr<Arg>> m_result;
    std::shared_ptr<Command> m_body;

public:
    DeclareRunnableCommand(std::string name, std::vector<std::pair<abstraction::TypeQualifiers::TypeQualifierSet, std::unique_ptr<Arg>>> params, std::pair<abstraction::TypeQualifiers::TypeQualifierSet, std::unique_ptr<Arg>> result, std::shared_ptr<Command> body);

    DeclareRunnableCommand(std::string name, std::vector<std::pair<abstraction::TypeQualifiers::TypeQualifierSet, std::unique_ptr<Arg>>> params, std::shared_ptr<Command> body);

    CommandResult run(Environment& environment) const override;

    std::ostream& print(std::ostream& out) const override;
};

} /* namespace cli */
