#pragma once

namespace cli {

enum class CommandResult {
    OK,
    QUIT,
    RETURN,
    CONTINUE,
    BREAK,
    EXCEPTION,
    ERROR,
    EOT
};

} /* namespace cli */
