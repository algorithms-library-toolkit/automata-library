#include "UndeclareRunnableCommand.h"

#include <ext/foreach>

#include <ast/Statement.h>
#include <common/CastHelper.h>
#include <registration/AlgoRegistration.hpp>

namespace cli {

UndeclareRunnableCommand::UndeclareRunnableCommand(std::string name, std::vector<abstraction::TypeQualifiers::TypeQualifierSet> params)
    : m_name(std::move(name))
    , m_params(std::move(params))
{
}

CommandResult UndeclareRunnableCommand::run(Environment&) const
{
    ext::vector<ext::pair<core::type_details, abstraction::TypeQualifiers::TypeQualifierSet>> paramSpecs;

    for (const abstraction::TypeQualifiers::TypeQualifierSet& param : m_params)
        paramSpecs.emplace_back(core::type_details::universal_type(), param);

    abstraction::AlgorithmRegistry::unregisterRaw(m_name, paramSpecs);
    return CommandResult::OK;
}

std::ostream& UndeclareRunnableCommand::print(std::ostream& out) const
{
    out << "(UndeclareRunnableCommand " << m_name;
    for (const auto& param : m_params) {
        out << " " << param;
    }
    return out << ")";
}

} /* namespace cli */
