#pragma once

#include <ast/Command.h>
#include <environment/Environment.h>

#include <fstream>
#include <readline/IstreamLineInterface.h>

namespace cli {

class InterpretCommand : public Command {
    std::string m_fileName;

public:
    InterpretCommand(std::string libraryName)
        : m_fileName(std::move(libraryName))
    {
    }

    CommandResult run(Environment& environment) const override
    {
        std::ifstream ifs(m_fileName);

        if (!ifs.is_open())
            throw exception::CommonException("File '" + m_fileName + "' not found.");

        CommandResult state = environment.execute(ifs);

        if (state != cli::CommandResult::QUIT && state != cli::CommandResult::RETURN)
            state = cli::CommandResult::OK;

        return state;
    }

    std::ostream& print(std::ostream& out) const override
    {
        return out << "(InterpretCommand " << m_fileName << ")";
    }
};

} /* namespace cli */
