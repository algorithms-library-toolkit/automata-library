#pragma once

#include <ast/Command.h>
#include <environment/Environment.h>

#include <alib/measure>

namespace cli {

class StopMeasurementFrame : public Command {
public:
    CommandResult run(Environment&) const override
    {
        measurements::end();
        return CommandResult::OK;
    }

    std::ostream& print(std::ostream& out) const override
    {
        return out << "(StopMeasurementFrame)";
    }
};

} /* namespace cli */
