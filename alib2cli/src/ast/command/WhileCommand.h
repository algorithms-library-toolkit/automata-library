#pragma once

#include <ast/Command.h>
#include <ast/Statement.h>
#include <common/CastHelper.h>
#include <common/ResultInterpret.h>
#include <environment/Environment.h>

namespace cli {

class WhileCommand : public Command {
    std::unique_ptr<Expression> m_condition;
    std::unique_ptr<Command> m_body;

public:
    WhileCommand(std::unique_ptr<Expression> condition, std::unique_ptr<Command> body)
        : m_condition(std::move(condition))
        , m_body(std::move(body))
    {
    }

    CommandResult run(Environment& environment) const override
    {
        CommandResult res = cli::CommandResult::OK;
        while (res == cli::CommandResult::OK) {
            std::shared_ptr<abstraction::Value> conditionResult = m_condition->translateAndEval(environment);

            std::shared_ptr<abstraction::Value> castedResult = abstraction::CastHelper::eval(environment, conditionResult, core::type_details::as_type("bool"));

            if (!cli::ResultInterpret::value<bool>(castedResult))
                break;

            res = m_body->run(environment);

            if (res == CommandResult::CONTINUE)
                res = CommandResult::OK;

            if (res == CommandResult::BREAK) {
                res = CommandResult::OK;
                break;
            }
        }

        return res;
    }


    std::ostream& print(std::ostream& out) const override
    {
        return out << "(WhileCommand " << m_condition << " " << m_body << ")";
    }
};

} /* namespace cli */
