#pragma once

#include <ast/Command.h>
#include <ast/Expression.h>
#include <environment/Environment.h>

namespace cli {

class PrintCommand : public Command {
    std::unique_ptr<Expression> m_expr;

public:
    PrintCommand(std::unique_ptr<Expression> expr)
        : m_expr(std::move(expr))
    {
    }

    CommandResult run(Environment& environment) const override;

    std::ostream& print(std::ostream& out) const override;
};

} /* namespace cli */
