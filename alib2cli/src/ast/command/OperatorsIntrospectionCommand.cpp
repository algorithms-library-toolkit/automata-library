#include "OperatorsIntrospectionCommand.h"

#include <global/GlobalData.h>
#include <registry/Registry.h>

namespace cli {

void OperatorsIntrospectionCommand::typePrint(const ext::pair<core::type_details, abstraction::TypeQualifiers::TypeQualifierSet>& result, ext::ostream& os)
{
    if (abstraction::TypeQualifiers::isConst(std::get<1>(result)))
        os << "const ";

    os << std::get<0>(result);

    if (abstraction::TypeQualifiers::isLvalueRef(std::get<1>(result)))
        os << " &";

    if (abstraction::TypeQualifiers::isRvalueRef(std::get<1>(result)))
        os << " &&";
}

template <class Operators>
void OperatorsIntrospectionCommand::printOperators(const ext::list<ext::pair<Operators, abstraction::AlgorithmFullInfo>>& overloads)
{
    for (const ext::pair<Operators, abstraction::AlgorithmFullInfo>& overload : overloads) {

        typePrint(std::get<1>(overload).getResult(), common::Streams::out);

        common::Streams::out << " operator ";
        common::Streams::out << abstraction::Operators::toString(std::get<0>(overload));
        common::Streams::out << " (";

        for (size_t i = 0; i < std::get<1>(overload).getParams().size(); ++i) {
            if (i != 0)
                common::Streams::out << ",";

            common::Streams::out << " ";

            typePrint(std::get<1>(overload).getParams()[i], common::Streams::out);

            common::Streams::out << " " << std::get<1>(overload).getParamNames()[i];
        }
        common::Streams::out << " )" << std::endl;
    }
}

CommandResult OperatorsIntrospectionCommand::run(Environment& /* environment */) const
{
    printOperators(abstraction::Registry::listBinaryOperators());
    printOperators(abstraction::Registry::listPrefixOperators());
    printOperators(abstraction::Registry::listPostfixOperators());
    return CommandResult::OK;
}

std::ostream& OperatorsIntrospectionCommand::print(std::ostream& out) const
{
    return out << "(OperatorsIntrospectionCommand)";
}

} /* namespace cli */
