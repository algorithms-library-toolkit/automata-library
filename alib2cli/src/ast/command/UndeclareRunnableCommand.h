#pragma once

#include <ast/Arg.h>
#include <ast/Command.h>
#include <environment/Environment.h>

namespace cli {

class UndeclareRunnableCommand : public Command {
    std::string m_name;
    std::vector<abstraction::TypeQualifiers::TypeQualifierSet> m_params;

public:
    UndeclareRunnableCommand(std::string name, std::vector<abstraction::TypeQualifiers::TypeQualifierSet>);

    CommandResult run(Environment& environment) const override;

    std::ostream& print(std::ostream& out) const override;
};

} /* namespace cli */
