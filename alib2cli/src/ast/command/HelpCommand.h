#pragma once

#include <ast/Arg.h>
#include <ast/Command.h>
#include <environment/Environment.h>

namespace cli {

class HelpCommand : public Command {
    std::unique_ptr<cli::Arg> m_command;

public:
    HelpCommand(std::unique_ptr<cli::Arg> command)
        : m_command(std::move(command))
    {
    }

    CommandResult run(Environment& environment) const override;

    std::ostream& print(std::ostream& out) const override;
};

} /* namespace cli */
