#pragma once

#include <ast/Command.h>

namespace cli {

class BlockCommand final : public Command {
    std::unique_ptr<Command> m_innerCommand;

public:
    BlockCommand(std::unique_ptr<Command> innerCommand)
        : m_innerCommand(std::move(innerCommand))
    {
    }

    CommandResult run(Environment& environment) const override
    {
        Environment newEnvironment{ext::optional_ref<Environment>(environment)};
        CommandResult res = m_innerCommand->run(newEnvironment);
        if (newEnvironment.getResult())
            environment.setResult(newEnvironment.getResult());
        return res;
    }

    std::ostream& print(std::ostream& out) const override
    {
        return out << "(BlockCommand " << m_innerCommand << ")";
    }
};

} /* namespace cli */
