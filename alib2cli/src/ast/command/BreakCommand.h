#pragma once

#include <ast/Command.h>
#include <environment/Environment.h>

namespace cli {

class BreakCommand : public Command {
public:
    CommandResult run(Environment& /* environment */) const override
    {
        return CommandResult::BREAK;
    }


    std::ostream& print(std::ostream& out) const override
    {
        return out << "(BreakCommand)";
    }
};

} /* namespace cli */
