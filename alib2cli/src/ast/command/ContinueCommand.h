#pragma once

#include <ast/Command.h>
#include <environment/Environment.h>

namespace cli {

class ContinueCommand : public Command {
public:
    CommandResult run(Environment& /* environment */) const override
    {
        return CommandResult::CONTINUE;
    }

    std::ostream& print(std::ostream& out) const override
    {
        return out << "(ContinueCommand)";
    }
};

} /* namespace cli */
