#pragma once

#include <ast/Command.h>
#include <ast/Expression.h>
#include <environment/Environment.h>
#include <global/GlobalData.h>
#include <optional>
#include <parser/Parser.h>

namespace cli {

class AstIntrospectionCommand : public Command {
    std::unique_ptr<Command> m_command = nullptr;
    std::unique_ptr<Arg> m_file = nullptr;

public:
    AstIntrospectionCommand(std::unique_ptr<Command> command)
        : m_command(std::move(command))
    {
    }


    AstIntrospectionCommand(std::unique_ptr<Arg> file)
        : m_file(std::move(file))
    {
    }

    CommandResult run(Environment& environment) const override
    {
        if (m_command != nullptr)
            common::Streams::out << m_command << std::endl;
        else if (m_file != nullptr) {
            std::ifstream file(m_file->eval(environment));
            auto cmd = Parser::parseStream(file);

            common::Streams::out << cmd << std::endl;
        } else
            throw std::runtime_error("AstIntrospectionCommand::run: no command or file specified");

        return CommandResult::OK;
    }

    std::ostream& print(std::ostream& out) const override
    {
        if (m_command != nullptr)
            return out << "(AstIntrospectionCommand " << m_command << ")";
        else
            return out << "(AstIntrospectionCommand " << m_file << ")";
    }
};

} // cli
