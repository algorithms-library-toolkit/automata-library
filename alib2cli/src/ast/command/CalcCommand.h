#pragma once

#include <ast/Command.h>
#include <ast/Expression.h>
#include <environment/Environment.h>

namespace cli {

class CalcCommand : public Command {
    std::unique_ptr<cli::Expression> m_expr;

public:
    CalcCommand(std::unique_ptr<cli::Expression> expr)
        : m_expr(std::move(expr))
    {
    }

    CommandResult run(Environment& environment) const override
    {
        m_expr->translateAndEval(environment);

        return cli::CommandResult::OK;
    }

    std::ostream& print(std::ostream& out) const override
    {
        return out << "(CalcCommand " << m_expr << ")";
    }
};

} /* namespace cli */
