#include "VariablesIntrospectionCommand.h"

#include <global/GlobalData.h>

namespace cli {

CommandResult VariablesIntrospectionCommand::run(Environment& environment) const
{
    std::string param;
    if (m_param != nullptr)
        param = m_param->eval(environment);

    if (param.empty())
        for (const std::string& name : environment.getVariableNames())
            common::Streams::out << name << std::endl;
    else
        common::Streams::out << param << " " << environment.getVariable(param)->getActualType() << std::endl;

    return CommandResult::OK;
}

std::ostream& VariablesIntrospectionCommand::print(std::ostream& out) const
{
    return out << "(VariablesIntrospectionCommand " << m_param << ")";
}

} /* namespace cli */
