#pragma once

#include <ast/Command.h>
#include <environment/Environment.h>

#include <common/LibraryLoader.h>

namespace cli {

class LoadCommand : public Command {
    std::string m_libraryName;

public:
    LoadCommand(std::string libraryName)
        : m_libraryName(std::move(libraryName))
    {
    }

    CommandResult run(Environment&) const override
    {
        cli::LibraryLoader::load(m_libraryName);
        return CommandResult::OK;
    }

    std::ostream& print(std::ostream& out) const override
    {
        return out << "(LoadCommand " << m_libraryName << ")";
    }
};

} /* namespace cli */
