#include "DataTypesIntrospectionCommand.h"

#include <global/GlobalData.h>
#include <registry/XmlRegistry.h>

namespace cli {

void DataTypesIntrospectionCommand::printTypes(const ext::set<std::string>& types)
{
    for (const std::string& type : types)
        common::Streams::out << type << std::endl;
}

CommandResult DataTypesIntrospectionCommand::run(Environment& environment) const
{
    std::string param;
    if (m_param != nullptr)
        param = m_param->eval(environment);

    if (param.empty()) {
        printTypes(abstraction::XmlRegistry::listDataTypes());
    } else if (param.find("::", param.size() - 2) != std::string::npos) {
        printTypes(abstraction::XmlRegistry::listDataTypeGroup(param));
    } else {
        throw exception::CommonException("Invalid DataType introspection param");
    }
    return CommandResult::OK;
}

std::ostream& DataTypesIntrospectionCommand::print(std::ostream& out) const
{
    return out << "(DataTypesIntrospectionCommand " << m_param << ")";
}

} /* namespace cli */
