#include <registry/OutputFileRegistry.hpp>

namespace abstraction {

ext::map<std::string, std::unique_ptr<OutputFileRegistry::Entry>>& OutputFileRegistry::getEntries()
{
    static ext::map<std::string, std::unique_ptr<Entry>> inputFileHandlers;
    return inputFileHandlers;
}

void OutputFileRegistry::registerOutputFileHandler(const std::string& fileType, std::unique_ptr<abstraction::OperationAbstraction> (*callback)(const core::type_details& typehint))
{
    auto iter = getEntries().insert(std::make_pair(fileType, std::unique_ptr<Entry>(new EntryImpl(callback))));
    if (!iter.second)
        throw std::invalid_argument("Entry " + iter.first->first + " already registered.");
}

void OutputFileRegistry::unregisterOutputFileHandler(const std::string& fileType)
{
    if (getEntries().erase(fileType) == 0u)
        throw std::invalid_argument("Entry " + fileType + " not registered.");
}

std::unique_ptr<abstraction::OperationAbstraction> OutputFileRegistry::getAbstraction(const std::string& fileType, const core::type_details& typehint)
{
    auto res = getEntries().find(fileType);
    if (res == getEntries().end())
        throw exception::CommonException("Entry " + fileType + " not available.");

    return res->second->getAbstraction(typehint);
}

std::unique_ptr<abstraction::OperationAbstraction> OutputFileRegistry::EntryImpl::getAbstraction(const core::type_details& typehint) const
{
    return m_callback(typehint);
}

} /* namespace abstraction */
