#pragma once

#include <alib/map>
#include <alib/string>
#include <functional>
#include <memory>

#include <abstraction/OperationAbstraction.hpp>
#include <exception/CommonException.h>

namespace abstraction {

class OutputFileRegistry {
    class Entry {
    public:
        virtual std::unique_ptr<abstraction::OperationAbstraction> getAbstraction(const core::type_details& typehint) const = 0;

        virtual ~Entry() = default;
    };

    class EntryImpl : public Entry {
        std::function<std::unique_ptr<abstraction::OperationAbstraction>(const core::type_details& typehint)> m_callback;

    public:
        explicit EntryImpl(std::unique_ptr<abstraction::OperationAbstraction> (*callback)(const core::type_details& typehint))
            : m_callback(callback)
        {
        }

        std::unique_ptr<abstraction::OperationAbstraction> getAbstraction(const core::type_details& typehint) const override;
    };

    static ext::map<std::string, std::unique_ptr<Entry>>& getEntries();

public:
    static void registerOutputFileHandler(const std::string& fileType, std::unique_ptr<abstraction::OperationAbstraction> (*callback)(const core::type_details& typehint));

    static void unregisterOutputFileHandler(const std::string& fileType);

    static std::unique_ptr<abstraction::OperationAbstraction> getAbstraction(const std::string& fileType, const core::type_details& typehint);
};

} /* namespace abstraction */
