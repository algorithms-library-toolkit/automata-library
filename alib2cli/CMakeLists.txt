project(alt-libcli VERSION ${CMAKE_PROJECT_VERSION})

option(ALIB2CLI_COVERAGE "Collects coverage data for alib2cli" OFF)


find_package(LibXml2 REQUIRED)
find_package(antlr4-generator 4.11 REQUIRED)
find_package(antlr4-runtime 4.11 REQUIRED)

if(DEFINED ENV{ANTLR4_JAR_LOCATION})
    SET(ANTLR4_JAR_LOCATION $ENV{ANTLR4_JAR_LOCATION})
    MESSAGE("ANTLR4_JAR_LOCATION ${ANTLR4_JAR_LOCATION}")
endif()

antlr4_generate(AltCliGrammarLexer
		${CMAKE_CURRENT_SOURCE_DIR}/src/grammar/AltCliLexer.g4
		LEXER
		FALSE # listener
		FALSE # visitor
		cli::grammar::lexer
		)

antlr4_generate(AltCliGrammarParser
		${CMAKE_CURRENT_SOURCE_DIR}/src/grammar/AltCliParser.g4
		PARSER
		FALSE # listener
		TRUE # visitor
		cli::grammar::parser
		${ANTLR4_TOKEN_FILES_AltCliGrammarLexer}
		${ANTLR4_TOKEN_DIRECTORY_AltCliGrammarLexer}
		)

alt_library(alib2cli
	DEPENDS alib2xml alib2common alib2abstraction alib2measure alib2std ${CMAKE_DL_LIBS}
	TEST_DEPENDS LibXml2::LibXml2 stdc++fs alib2str
)

# These files are automatically generated, so we don't want to see warnings
target_sources(alib2cli PRIVATE ${ANTLR4_SRC_FILES_AltCliGrammarLexer} ${ANTLR4_SRC_FILES_AltCliGrammarParser})
set_source_files_properties(
	${ANTLR4_SRC_FILES_AltCliGrammarLexer} ${ANTLR4_SRC_FILES_AltCliGrammarParser}
	PROPERTIES
		GENERATED TRUE
		COMPILE_OPTIONS "-w")

target_include_directories(alib2cli PUBLIC ${ANTLR4_INCLUDE_DIR_AltCliGrammarLexer} ${ANTLR4_INCLUDE_DIR_AltCliGrammarParser})
target_include_directories(alib2cli SYSTEM PUBLIC ${ANTLR4_INCLUDE_DIR})
target_link_libraries(alib2cli INTERFACE antlr4_shared)

# Enable coverage report
if (ALIB2CLI_COVERAGE)
	target_compile_options(alib2cli PUBLIC -fprofile-arcs -ftest-coverage)
	target_link_options(alib2cli PUBLIC -fprofile-arcs -lgcov)
	target_compile_options(test_alib2cli PUBLIC -fprofile-arcs -ftest-coverage)
	target_link_options(test_alib2cli PUBLIC -fprofile-arcs -lgcov)
endif ()

configure_file(
		${CMAKE_CURRENT_SOURCE_DIR}/test-src/aql/configure_tests.hpp.in
		${CMAKE_CURRENT_BINARY_DIR}/test-src/aql/configure_tests.hpp
)

target_include_directories(test_alib2cli PRIVATE ${CMAKE_CURRENT_BINARY_DIR}/test-src/aql/)
