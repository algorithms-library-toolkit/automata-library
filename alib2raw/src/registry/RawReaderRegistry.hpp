#pragma once

#include <ext/memory>
#include <ext/typeinfo>

#include <alib/map>
#include <alib/string>

#include <abstraction/OperationAbstraction.hpp>

namespace abstraction {

class RawReaderRegistry {
public:
    class Entry {
    public:
        virtual std::unique_ptr<abstraction::OperationAbstraction> getAbstraction() const = 0;

        virtual ~Entry() = default;
    };

private:
    template <class Return>
    class EntryImpl : public Entry {
    public:
        std::unique_ptr<abstraction::OperationAbstraction> getAbstraction() const override;
    };

    static ext::map<core::type_details, std::unique_ptr<Entry>>& getEntries();

public:
    static void unregisterRawReader(const core::type_details& type);

    template <class ReturnType>
    static void unregisterRawReader()
    {
        core::type_details type = core::type_details::get<ReturnType>();
        unregisterRawReader(type);
    }

    static void registerRawReader(core::type_details type, std::unique_ptr<Entry> entry);

    template <class ReturnType>
    static void registerRawReader(core::type_details type)
    {
        registerRawReader(std::move(type), std::unique_ptr<Entry>(new EntryImpl<ReturnType>()));
    }

    template <class ReturnType>
    static void registerRawReader()
    {
        core::type_details type = core::type_details::get<ReturnType>();
        registerRawReader<ReturnType>(std::move(type));
    }

    static std::unique_ptr<abstraction::OperationAbstraction> getAbstraction(const core::type_details& type);
};

} /* namespace abstraction */

#include <abstraction/RawReaderAbstraction.hpp>

namespace abstraction {

template <class Return>
std::unique_ptr<abstraction::OperationAbstraction> RawReaderRegistry::EntryImpl<Return>::getAbstraction() const
{
    return std::make_unique<abstraction::RawReaderAbstraction<Return>>();
}

} /* namespace abstraction */
