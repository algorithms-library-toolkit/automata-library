#include <exception/CommonException.h>
#include <registry/RawReaderRegistry.hpp>

namespace abstraction {

ext::map<core::type_details, std::unique_ptr<RawReaderRegistry::Entry>>& RawReaderRegistry::getEntries()
{
    static ext::map<core::type_details, std::unique_ptr<Entry>> readers;
    return readers;
}

void RawReaderRegistry::unregisterRawReader(const core::type_details& type)
{
    if (getEntries().erase(type) == 0u)
        throw std::invalid_argument("Entry " + ext::to_string(type) + " not registered.");
}

void RawReaderRegistry::registerRawReader(core::type_details type, std::unique_ptr<Entry> entry)
{
    auto iter = getEntries().insert(std::make_pair(std::move(type), std::move(entry)));
    if (!iter.second)
        throw std::invalid_argument("Entry " + ext::to_string(iter.first->first) + " already registered.");
}

std::unique_ptr<abstraction::OperationAbstraction> RawReaderRegistry::getAbstraction(const core::type_details& type)
{
    auto iter = std::find_if(getEntries().begin(), getEntries().end(), [&](const std::pair<const core::type_details, std::unique_ptr<Entry>>& entry) { return entry.first.compatible_with(type); });
    if (iter == getEntries().end()) {
        throw exception::CommonException("Entry " + ext::to_string(type) + " not available.");
    } else {
        return iter->second->getAbstraction();
    }
}

} /* namespace abstraction */
