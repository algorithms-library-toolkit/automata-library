#pragma once

#include <abstraction/NaryOperationAbstraction.hpp>
#include <abstraction/ValueOperationAbstraction.hpp>

#include <factory/RawDataFactory.hpp>

namespace abstraction {

template <class ReturnType>
class RawReaderAbstraction : virtual public NaryOperationAbstraction<std::string&&>, virtual public ValueOperationAbstraction<ReturnType> {
public:
    std::shared_ptr<abstraction::Value> run() const override
    {
        const std::shared_ptr<abstraction::Value>& param = std::get<0>(this->getParams());
        return std::make_shared<abstraction::ValueHolder<ReturnType>>(factory::RawDataFactory::fromString(abstraction::retrieveValue<std::string&&>(param)), true);
    }
};

} /* namespace abstraction */
