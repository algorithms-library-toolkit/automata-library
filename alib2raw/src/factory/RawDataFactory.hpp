#pragma once

#include <ext/fstream>
#include <ext/sstream>

#include <alib/set>
#include <exception/CommonException.h>
#include <global/GlobalData.h>

#include <core/rawApi.hpp>

namespace factory {

class RawDataFactory {
public:
    class fromFile {
        const std::string& filename;

    public:
        explicit fromFile(const std::string& file)
            : filename(file)
        {
        }

        template <class T>
        operator T()
        {
            ext::ifstream fileStream(filename);

            return fromStream(fileStream);
        }
    };

    class fromRaw {
        const std::string& string;

    public:
        explicit fromRaw(const std::string& str)
            : string(str)
        {
        }

        template <class T>
        operator T()
        {
            ext::istringstream stringStream(string);

            return fromStream(stringStream);
        }
    };

    class fromString {
        const std::string& string;

    public:
        explicit fromString(const std::string& str)
            : string(str)
        {
        }

        template <class T>
        operator T()
        {
            ext::istringstream stringStream(string);

            return fromStream(stringStream);
        }
    };

    class fromStdin {
    public:
        template <class T>
        operator T()
        {
            return fromStream(common::Streams::in);
        }
    };

    class fromStream {
        ext::istream& in;

    public:
        explicit fromStream(ext::istream& i)
            : in(i)
        {
        }

        template <class T>
        operator T()
        {
            if (in.peek() == EOF)
                throw exception::CommonException("Empty stream");

            T res = core::rawApi<T>::parse(in);

            while (isspace(in.peek()))
                in.get();

            if (in.peek() != EOF)
                throw exception::CommonException("Unexpected characters at the end of the stream");

            return res;
        }
    };

    template <class T>
    static void toFile(const T& data, const std::string& filename)
    {
        ext::ofstream fileStream(filename);
        toStream<T>(data, fileStream);
    }

    template <class T>
    static std::string toString(const T& data)
    {
        ext::ostringstream stringStream;
        toStream<T>(data, stringStream);
        return stringStream.str();
    }

    template <class T>
    static void toStdout(const T& data)
    {
        toStream<T>(data, common::Streams::out.get());
    }

    template <class T>
    static void toStream(const T& data, ext::ostream& out)
    {
        core::rawApi<T>::compose(out, data);
    }
};

} /* namespace factory */
