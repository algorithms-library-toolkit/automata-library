#include "Trim.h"

#include <automaton/FSM/CompactNFA.h>
#include <automaton/FSM/DFA.h>
#include <automaton/FSM/EpsilonNFA.h>
#include <automaton/FSM/ExtendedNFA.h>
#include <automaton/FSM/NFA.h>

#include <registration/AlgoRegistration.hpp>
#include "automaton/Automaton.h"

namespace {

auto TrimDFA = registration::AbstractRegister<automaton::simplify::efficient::Trim, automaton::DFA<>, const automaton::DFA<>&>(automaton::simplify::efficient::Trim::trim, abstraction::AlgorithmCategories::AlgorithmCategory::EFFICIENT);
auto TrimNFA = registration::AbstractRegister<automaton::simplify::efficient::Trim, automaton::NFA<>, const automaton::NFA<>&>(automaton::simplify::efficient::Trim::trim, abstraction::AlgorithmCategories::AlgorithmCategory::EFFICIENT);
auto TrimMultiInitialStateNFA = registration::AbstractRegister<automaton::simplify::efficient::Trim, automaton::MultiInitialStateNFA<>, const automaton::MultiInitialStateNFA<>&>(automaton::simplify::efficient::Trim::trim, abstraction::AlgorithmCategories::AlgorithmCategory::EFFICIENT);
auto TrimEpsilonNFA = registration::AbstractRegister<automaton::simplify::efficient::Trim, automaton::EpsilonNFA<>, const automaton::EpsilonNFA<>&>(automaton::simplify::efficient::Trim::trim, abstraction::AlgorithmCategories::AlgorithmCategory::EFFICIENT);
auto TrimCompactNFA = registration::AbstractRegister<automaton::simplify::efficient::Trim, automaton::CompactNFA<>, const automaton::CompactNFA<>&>(automaton::simplify::efficient::Trim::trim, abstraction::AlgorithmCategories::AlgorithmCategory::EFFICIENT);
auto TrimExtendedNFA = registration::AbstractRegister<automaton::simplify::efficient::Trim, automaton::ExtendedNFA<>, const automaton::ExtendedNFA<>&>(automaton::simplify::efficient::Trim::trim, abstraction::AlgorithmCategories::AlgorithmCategory::EFFICIENT);

} /* namespace */
