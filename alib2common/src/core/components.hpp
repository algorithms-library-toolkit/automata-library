#pragma once

namespace component {

class GeneralAlphabet;
class InputAlphabet;
class OutputAlphabet;
class TapeAlphabet;
class CallAlphabet;
class ReturnAlphabet;
class LocalAlphabet;
class ConstantAlphabet;
class NonlinearAlphabet;
class PushdownStoreAlphabet;
class States;
class FinalStates;
class InitialState;
class InitialStates;
class BlankSymbol;
class BottomOfTheStackSymbol;
class TerminalAlphabet;
class NonterminalAlphabet;
class InitialSymbol;
class WildcardSymbol;
class TerminatingSymbol;
class SubtreeWildcardSymbol;
class NodeWildcardSymbol;
class NodeWildcardSymbols;
class BarSymbol;
class BarSymbols;
class VariablesBarSymbol;
class SubtreeGapSymbol;

} /* namespace component */
