#include <alib/set>

#include <registry/CastRegistry.hpp>
#include <registry/ContainerRegistry.hpp>
#include <registry/ValuePrinterRegistry.hpp>

#include <object/Object.h>
#include <object/ObjectFactory.h>

#include <exception/CommonException.h>
#include <ext/exception>

#include <alib/map>
#include <alib/pair>
#include <alib/set>
#include <alib/trie>
#include <alib/vector>

#include <registration/OperatorRegistration.hpp>

namespace {

class PrimitiveRegistrator {
public:
    PrimitiveRegistrator()
    {
        alib::ExceptionHandler::addHandler<3>([](alib::ExceptionHandler::NestedExceptionContainer& exceptions, const exception::CommonException& exception) {
            exceptions.push_back("Common exception", ext::to_string(exception));
        });

        abstraction::CastRegistry::registerCast<double, int>();
        abstraction::CastRegistry::registerCast<int, double>();

        abstraction::CastRegistry::registerCastAlgorithm<std::string, int>(ext::to_string);
        abstraction::CastRegistry::registerCastAlgorithm<int, std::string>(static_cast<int (*)(const std::string&)>(ext::from_string<int>));

        abstraction::CastRegistry::registerCast<bool, int>();
        abstraction::CastRegistry::registerCast<char, int>();
        abstraction::CastRegistry::registerCastAlgorithm<bool, std::string>(static_cast<bool (*)(const std::string&)>(ext::from_string<bool>));
        abstraction::CastRegistry::registerCastAlgorithm<unsigned, std::string>(static_cast<unsigned (*)(const std::string&)>(ext::from_string<unsigned>));
        abstraction::CastRegistry::registerCastAlgorithm<double, std::string>(static_cast<double (*)(const std::string&)>(ext::from_string<double>));

        abstraction::CastRegistry::registerCast<size_t, int>();
        abstraction::CastRegistry::registerCast<int, size_t>();
        abstraction::CastRegistry::registerCast<unsigned, int>();

        abstraction::CastRegistry::registerCast<long, int>();

        abstraction::ValuePrinterRegistry::registerValuePrinter<int>();
        abstraction::ValuePrinterRegistry::registerValuePrinter<unsigned>();
        abstraction::ValuePrinterRegistry::registerValuePrinter<double>();
        abstraction::ValuePrinterRegistry::registerValuePrinter<std::string>();
        abstraction::ValuePrinterRegistry::registerValuePrinter<void>();
        abstraction::ValuePrinterRegistry::registerValuePrinter<bool>();

        abstraction::ValuePrinterRegistry::registerValuePrinter<object::Object>();
        abstraction::ValuePrinterRegistry::registerValuePrinter<ext::set<object::Object>>();
        abstraction::ValuePrinterRegistry::registerValuePrinter<ext::trie<object::Object, object::Object>>();
        abstraction::ValuePrinterRegistry::registerValuePrinter<ext::vector<object::Object>>();
        abstraction::ValuePrinterRegistry::registerValuePrinter<ext::pair<object::Object, object::Object>>();
        abstraction::ValuePrinterRegistry::registerValuePrinter<ext::map<object::Object, object::Object>>();
        abstraction::ContainerRegistry::registerSet();
    }

    ~PrimitiveRegistrator()
    {
        abstraction::CastRegistry::unregisterCast<double, int>();
        abstraction::CastRegistry::unregisterCast<int, double>();

        abstraction::CastRegistry::unregisterCast<std::string, int>();
        abstraction::CastRegistry::unregisterCast<int, std::string>();

        abstraction::CastRegistry::unregisterCast<bool, int>();
        abstraction::CastRegistry::unregisterCast<char, int>();
        abstraction::CastRegistry::unregisterCast<bool, std::string>();
        abstraction::CastRegistry::unregisterCast<unsigned, std::string>();
        abstraction::CastRegistry::unregisterCast<double, std::string>();

        abstraction::CastRegistry::unregisterCast<size_t, int>();
        abstraction::CastRegistry::unregisterCast<int, size_t>();
        abstraction::CastRegistry::unregisterCast<unsigned, int>();

        abstraction::CastRegistry::unregisterCast<long, int>();

        abstraction::ValuePrinterRegistry::unregisterValuePrinter<int>();
        abstraction::ValuePrinterRegistry::unregisterValuePrinter<unsigned>();
        abstraction::ValuePrinterRegistry::unregisterValuePrinter<double>();
        abstraction::ValuePrinterRegistry::unregisterValuePrinter<std::string>();
        abstraction::ValuePrinterRegistry::unregisterValuePrinter<void>();
        abstraction::ValuePrinterRegistry::unregisterValuePrinter<bool>();

        abstraction::ValuePrinterRegistry::unregisterValuePrinter<object::Object>();
        abstraction::ValuePrinterRegistry::unregisterValuePrinter<ext::set<object::Object>>();
        abstraction::ValuePrinterRegistry::unregisterValuePrinter<ext::trie<object::Object, object::Object>>();
        abstraction::ValuePrinterRegistry::unregisterValuePrinter<ext::vector<object::Object>>();
        abstraction::ValuePrinterRegistry::unregisterValuePrinter<ext::pair<object::Object, object::Object>>();
        abstraction::ValuePrinterRegistry::unregisterValuePrinter<ext::map<object::Object, object::Object>>();
        abstraction::ContainerRegistry::unregisterSet();
    }
};

auto primitiveRegistrator = PrimitiveRegistrator();

auto addInts = registration::BinaryOperatorRegister<abstraction::Operators::BinaryOperators::ADD, int, int>();
auto subInts = registration::BinaryOperatorRegister<abstraction::Operators::BinaryOperators::SUB, int, int>();
auto modInts = registration::BinaryOperatorRegister<abstraction::Operators::BinaryOperators::MOD, int, int>();
auto mulInts = registration::BinaryOperatorRegister<abstraction::Operators::BinaryOperators::MUL, int, int>();
auto divInts = registration::BinaryOperatorRegister<abstraction::Operators::BinaryOperators::DIV, int, int>();

auto equalsInts = registration::BinaryOperatorRegister<abstraction::Operators::BinaryOperators::EQUALS, int, int>();
auto notEqualsInts = registration::BinaryOperatorRegister<abstraction::Operators::BinaryOperators::NOT_EQUALS, int, int>();

auto lessInts = registration::BinaryOperatorRegister<abstraction::Operators::BinaryOperators::LESS, int, int>();
auto lessOrEqualInts = registration::BinaryOperatorRegister<abstraction::Operators::BinaryOperators::LESS_OR_EQUAL, int, int>();
auto moreInts = registration::BinaryOperatorRegister<abstraction::Operators::BinaryOperators::MORE, int, int>();
auto moreOrEqualInts = registration::BinaryOperatorRegister<abstraction::Operators::BinaryOperators::MORE_OR_EQUAL, int, int>();

auto andInts = registration::BinaryOperatorRegister<abstraction::Operators::BinaryOperators::BINARY_AND, int, int>();
auto orInts = registration::BinaryOperatorRegister<abstraction::Operators::BinaryOperators::BINARY_OR, int, int>();
auto xorInts = registration::BinaryOperatorRegister<abstraction::Operators::BinaryOperators::BINARY_XOR, int, int>();

auto plusInt = registration::PrefixOperatorRegister<abstraction::Operators::PrefixOperators::PLUS, int>();
auto minusInt = registration::PrefixOperatorRegister<abstraction::Operators::PrefixOperators::MINUS, int>();

auto negInt = registration::PrefixOperatorRegister<abstraction::Operators::PrefixOperators::BINARY_NEG, int>();

auto postIncrementInt = registration::PostfixOperatorRegister<abstraction::Operators::PostfixOperators::INCREMENT, int&>();
auto postDecrementInt = registration::PostfixOperatorRegister<abstraction::Operators::PostfixOperators::DECREMENT, int&>();

auto prefixIncrementInt = registration::PrefixOperatorRegister<abstraction::Operators::PrefixOperators::INCREMENT, int&>();
auto prefixDecrementInt = registration::PrefixOperatorRegister<abstraction::Operators::PrefixOperators::DECREMENT, int&>();

auto assignInt = registration::BinaryOperatorRegister<abstraction::Operators::BinaryOperators::ASSIGN, int&, int>();


auto andBools = registration::BinaryOperatorRegister<abstraction::Operators::BinaryOperators::LOGICAL_AND, bool, bool>();
auto orBools = registration::BinaryOperatorRegister<abstraction::Operators::BinaryOperators::LOGICAL_OR, bool, bool>();

auto notBools = registration::PrefixOperatorRegister<abstraction::Operators::PrefixOperators::LOGICAL_NOT, bool>();

auto equalsBools = registration::BinaryOperatorRegister<abstraction::Operators::BinaryOperators::EQUALS, bool, bool>();
auto notEqualsBools = registration::BinaryOperatorRegister<abstraction::Operators::BinaryOperators::NOT_EQUALS, bool, bool>();

} /* namespace */
