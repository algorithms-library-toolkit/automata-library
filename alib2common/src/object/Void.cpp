#include <object/Void.h>

#include <registration/ValuePrinterRegistration.hpp>

namespace object {

Void::Void() = default;

std::ostream& operator<<(std::ostream& out, const Void&)
{
    return out << "(Void)";
}

Void Void::VOID = Void();

} /* namespace object */

namespace core {

object::Void type_util<object::Void>::denormalize(object::Void&& object)
{
    return object;
}

object::Void type_util<object::Void>::normalize(object::Void&& object)
{
    return object;
}

std::unique_ptr<type_details_base> type_util<object::Void>::type(const object::Void&)
{
    return std::make_unique<type_details_type>("object::Void");
}

std::unique_ptr<type_details_base> type_details_retriever<object::Void>::get()
{
    return std::make_unique<type_details_type>("object::Void");
}

} /* namespace core */

namespace {

auto valuePrinter = registration::ValuePrinterRegister<object::Void>();

} /* namespace */
