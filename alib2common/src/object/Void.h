/*
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <alib/string>

#include <core/type_util.hpp>

namespace object {

/**
 * \brief
 * Represents void object.
 */
class Void {
public:
    /**
     * \brief
     * Creates a new instance of void object
     */
    explicit Void();

    /**
     * The three way comparison implementation
     *
     * \param other the other instance
     *
     * \returns the actual relation between two by type same objects
     */
    std::strong_ordering operator<=>(const Void&) const
    {
        return std::strong_ordering::equal;
    }

    /**
     * The equality comparison implementation.
     *
     * \param other the other object to compare with.
     *
     * \returns true if this and other objects are equal, false othervise
     */
    bool operator==(const Void&) const
    {
        return true;
    }

    /**
     * Print this object as raw representation to ostream.
     *
     * \param os ostream where to print
     * \param instance object to print
     *
     * \returns modified output stream
     */
    friend std::ostream& operator<<(std::ostream& out, const Void&);

    /**
     * \brief
     * Singleton instance of the void object.
     */
    static Void VOID;
};

} /* namespace object */

namespace core {

template <>
struct type_util<object::Void> {
    static object::Void denormalize(object::Void&& object);

    static object::Void normalize(object::Void&& object);

    static std::unique_ptr<type_details_base> type(const object::Void& arg);
};

template <>
struct type_details_retriever<object::Void> {
    static std::unique_ptr<type_details_base> get();
};

} /* namespace core */
