#include <cstdlib>
#include "GlobalData.h"

#include <ext/iostream>

namespace common {

bool GlobalData::verbose = false;

bool GlobalData::measure = false;

bool GlobalData::optimizeXml = true;

bool GlobalData::exitOnError = false;

int GlobalData::argc = 0;

char** GlobalData::argv = nullptr;

ext::reference_wrapper<ext::istream> Streams::in = ext::cin;
ext::reference_wrapper<ext::ostream> Streams::out = ext::cout;
ext::reference_wrapper<ext::ostream> Streams::err = ext::cerr;
ext::reference_wrapper<ext::ostream> Streams::log = ext::clog;
ext::reference_wrapper<ext::ostream> Streams::measure = ext::cmeasure;

} /* common */

namespace ext {

std::ostream& operator<<(ext::reference_wrapper<std::ostream>& os, std::ostream& (*const func)(std::ostream&))
{
    os.get() << func;
    return os;
}

std::ostream& operator<<(ext::reference_wrapper<std::ostream>& os, std::ios_base& (*const func)(std::ios_base&))
{
    os.get() << func;
    return os;
}

ext::ostream& operator<<(ext::reference_wrapper<ext::ostream>& os, std::ostream& (*const func)(std::ostream&))
{
    os.get() << func;
    return os;
}

ext::ostream& operator<<(ext::reference_wrapper<ext::ostream>& os, std::ios_base& (*const func)(std::ios_base&))
{
    os.get() << func;
    return os;
}

std::istream& operator>>(ext::reference_wrapper<std::istream>& is, std::istream& (*const func)(std::istream&))
{
    is.get() >> func;
    return is;
}

std::istream& operator>>(ext::reference_wrapper<std::istream>& is, std::ios_base& (*const func)(std::ios_base&))
{
    is.get() >> func;
    return is;
}

ext::istream& operator>>(ext::reference_wrapper<ext::istream>& is, std::istream& (*const func)(std::istream&))
{
    is.get() >> func;
    return is;
}

ext::istream& operator>>(ext::reference_wrapper<ext::istream>& is, std::ios_base& (*const func)(std::ios_base&))
{
    is.get() >> func;
    return is;
}

} /* namespace ext */
