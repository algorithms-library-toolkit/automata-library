#pragma once

#include <exception>
#include <ostream>

#include <alib/string>

namespace exception {

/**
 * \brief
 * Basic exception from which all other exceptions are derived.
 *
 * Extends standard exception and additionally contains cause, backtrace (if conputed), and copy of command line string used to execute the program.
 */
class CommonException : public std::exception {
private:
    /**
     * \brief
     * Programmer supplied identification of the exception cause.
     */
    std::string m_cause;

    /**
     * \brief
     * Overiden string representation of the backtrace from the position of the throw.
     */
    std::string m_backtrace;

    /**
     * \brief
     * A copy of the command line string resulting in the exception.
     */
    std::string m_command;

public:
    /**
     * \brief
     * Exception constructor with specified cause. The backtrace is internally computed and the command line is also retrieved from global data registry.
     */
    explicit CommonException(std::string cause);

    /**
     * \return reason why the exception occurred
     */
    const char* what() const noexcept override;

    /**
     * \return reason why the exception occurred
     */
    const std::string& getCause() const;

    /**
     * \return reason why the exception occurred
     */
    const std::string& getBacktrace() const;

    /**
     * \return reason why the exception occurred
     */
    const std::string& getCommand() const;

    /**
     * Print this object as raw representation to ostream.
     *
     * \param os ostream where to print
     * \param instance object to print
     *
     * \returns modified output stream
     */
    friend std::ostream& operator<<(std::ostream& os, const CommonException&);
};

} /* namespace exception */
